//package com.quyang.voice.cache.redis;
//
//import com.alibaba.fastjson.JSONObject;
//import com.google.gson.Gson;
//import com.quyang.voice.utils.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class MyRedisUtil {
//
//    private static Logger _logger = LoggerFactory.getLogger(MyRedisUtil.class);
//
//    @Autowired
//    private Gson gson;
//
//    /**
//     * 创建索引（用户提供key与数据列表）
//     * @param redisClient
//     * @param key
//     * @param list
//     * @param clazz
//     * @return
//     */
//    public String buildIndex(IRedisClient redisClient, String key, List list, Class clazz) {
//
//        StringBuilder sb = new StringBuilder();
//        int m = 0;
//        for (Object instance : list) {
//            // json for instance.
//            String json = this.getJsonFromObject(instance, clazz);
//            // convert json string to json object
//            JSONObject jsonObject = JSONObject.parseObject(json);
//            // get object id
//            String uid = jsonObject.get("uid").toString();
//            // set object to redis.
//            redisClient.set(uid, json);
//            //build index.
//            if (m > 0) {
//                sb.append(",");
//            }
//            sb.append(uid);
//            m++;
//        }
//
//        String result = sb.toString();
//        String json = redisClient.get(key);
//
//        if (StringUtils.isNotBlank(json)) {
//            result = json + "," + result;
//        }
//        redisClient.set(key, result);
//        return result;
//
//    }
//
//    public String getJsonFromObject(Object instance, Class clazz) {
//        String json = null;
//        if (instance != null) {
//            json = gson.toJson(instance, clazz);
//        } else {
//            _logger.debug("instance is null. {}", instance.getClass().getName());
//        }
//        return json;
//    }
//
//
//    /**
//     * 从Redis中直接返回对象
//     * @param redisClient
//     * @param key
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    public <T> T getObjectByKey(IRedisClient redisClient, String key, Class clazz) {
//        T result = null;
//        String json = redisClient.get(key);
//        if (StringUtils.isNotBlank(json)) {
//           result = this.getObjectFromJson(json, clazz);
//        }
//        return result;
//    }
//
//
//    /**
//     * 将对象直接设置进缓存
//     * @param redisClient
//     * @param key
//     * @param obj
//     * @param clazz
//     */
//    public void setObjectToCache(IRedisClient redisClient, String key, Object obj, Class clazz) {
//        String json = this.getJsonFromObject(obj, clazz);
//        if (StringUtils.isNotBlank(json)) {
//            redisClient.set(key, json);
//        }
//    }
//
//    /**
//     * 将对象直接设置进缓存
//     * @param redisClient
//     * @param key
//     * @param obj
//     * @param clazz
//     */
//    public void setObjectToCacheWithTimeout(IRedisClient redisClient, String key, Object obj, Class clazz, int seconds) {
//        String json = this.getJsonFromObject(obj, clazz);
//        if (StringUtils.isNotBlank(json)) {
//            redisClient.setTimeout(key, json, seconds);
//        }
//    }
//
//    /**
//     * 可直接将Json转化为对应的PO
//     * @param json
//     * @return
//     */
//    public  <T> T getObjectFromJson (String json, Class clazz) {
//        T result = null;
//        if (StringUtils.isBlank(json)) {
//            _logger.debug("json value is null: {}", clazz);
//        } else {
//            result = (T) gson.fromJson(json, clazz);
//        }
//        return result;
//    }
//
//
//    /**
//     * 从缓存中读取一组数据
//     * @param redisClient
//     * @param keys
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//
//    public <T> List<T> getObjectsFromCache(IRedisClient redisClient, String[] keys, Class clazz) {
//        List<T> result = new ArrayList<T>();
//        List<String> values = redisClient.mget(keys);
//        for (String value : values) {
//            T instance = (T) this.getObjectFromJson(value, clazz);
//            if(null!=instance)
//            	result.add(instance);
//        }
//        return result;
//    }
//
//
//    /**
//     * 从当前的索引中很移除Ids. 键值被很移除后，不再补充.
//     * @param redisClient
//     * @param key
//     * @param ids
//     * @return
//     */
//    public String removeKeysFromIndex(IRedisClient redisClient, String key, String... ids) {
//
//        String json = redisClient.get(key);
//
//        //若无索引，则返回空值
//        if (StringUtils.isBlank(json)) {
//            _logger.trace("indexes are null.. {}", key);
//            return null;
//        }
//
//        String[] values = json.split(",");
//        StringBuilder sb = new StringBuilder();
//        for (String id : ids) {
//            int i = 0;
//            for (String value : values) {
//                if (i > 0)
//                    sb.append(",");
//
//                //判断是否在移除列表中
//                boolean removable = false;
//                if (id.equalsIgnoreCase(value)) {
//                    removable = true;
//                }
//
//                if (!removable) {
//                    sb.append(id);
//                    i ++;
//                }
//            }
//        }
//
//        //重新设置indexes值
//        String result = sb.toString();
//        redisClient.set(key, result);
//
//        return result;
//    }
//
//    /**
//     * 将新值添加到索引前端，并限制索引值的大小
//     * @param redisClient
//     * @param key
//     * @param max  当max=0时，不限制索引大小.
//     * @param ids
//     * @return
//     */
//    public String insertKeysToHeadWithLimited(IRedisClient redisClient, String key, int max, String...ids) {
//
//        String value = StringUtils.strArrayToStringWithSeperator(ids, ",");
//        String json = redisClient.get(key);
//        int total = ids.length;
//        if (StringUtils.isBlank(json)) {
//            json = value;
//        } else {
//
//            //去掉在json中重复的Id.
//            StringBuilder sb = new StringBuilder();
//            int m = 0;
//            for (String id : ids) {
//                if (json.indexOf(id)<0) {
//                    if (m==0) {
//                        sb.append(id);
//                    } else {
//                        sb.append(","+id);
//                    }
//                    m++;
//                }
//            }
//            value = sb.toString();
//
//            String[] keys = json.split(",");
//            total = total + keys.length;
//            json = StringUtils.addStringOnTop(value, keys, 0, 0).toString();
//        }
//
//        if (max > 0) {
//            //判断是否超过数量，若超过，则将后面的key删了
//            if (total > max) {
//                String[] keys = json.split(",");
//                json = StringUtils.addStringOnTop("", keys, 0, max).toString();
//            }
//        }
//
//        return json;
//
//    }
//
//    public StringBuilder getIndexList(IRedisClient redisClient, String key, int start, int count) {
//
//        StringBuilder result = null;
//        int max = start + count;
//
//        String values = redisClient.get(key);
//        if (StringUtils.isBlank(values)) {
//            _logger.debug("object is null");
//        } else {
//            String[] ids = values.split(",");
//            int total = ids.length;
//            if (total >= max) {
//                _logger.trace("object count enough to fetch out.");
//            } else {
//                max = total;
//            }
//
//            //起始记录小于等于最大记录
//            if (start <= max) {
//                result = new StringBuilder();
//                int m=0;
//                for (int i = start; i < max; i++) {
//                    if (m==0) {
//                        result.append(ids[i]);
//                    } else {
//                        result.append(","+ids[i]);
//                    }
//                }
//            } else {
//                _logger.debug("the start value more than max value : {}, {}", start, max);
//            }
//        }
//        return result;
//    }
//
//    public void setGson(Gson gson) {
//        this.gson = gson;
//    }
//
//}
