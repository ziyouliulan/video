package com.quyang.voice.cache.redis;

import com.quyang.voice.utils.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.SortingParams;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

enum RedisActionEnum {
	
    SET_VALUE,
    SET_VALUE_NOT_EX,
    SET_VALUE_WITH_TIMEOUT,
    APPEND_VALUE,
    SET_VALUE_EXPIRED,
    M_GET,
    DEL_VALUE_WITH_MULTI_KEYS,
    GET_VALUE_BY_KEY,
    GET_KEY_WITH_PATTEN,
    GET_KEYS_WITH_PATTEN,
    DEL_KEYS,
    INCREASE,
    INCREASE_BY,
    DECR,
    DECR_BY,
    
    H_INCREASE,
    H_GETALL,
    H_MGET,
    H_DEL,
    H_EXISTS,
    H_GET,
    H_INCRBY,
    H_MSET,
    EXISTS,
    S_CARD,
    S_ADD,
    S_ADD_WITH_TIMEOUT,
    S_DEL,
    S_IS_MEMBER,
    S_MEMBERS,
    
    R_PUSH,
    L_POP,
    L_LEN,
    L_RANGE,
    L_REM,
    L_TRIM,
    SORT,
    SORT_DESC
}

public abstract class BaseRedisClient {

    private Logger _logger = LoggerFactory.getLogger(BaseRedisClient.class);

    protected abstract int getDB();
    private static String server;
    private static short port;

    private JedisPool clientPool = null;

    public BaseRedisClient() {}

    protected BaseRedisClient(String server, short port) {

        if(clientPool == null)
        {
            _logger.debug("ready to connect to redis server {} on {}", server, port);
            try {
                this.server = server;
                this.port = port;
                JedisPoolConfig config = new JedisPoolConfig();
                _logger.trace("redis max active connections:{}", config.getMaxTotal());
                clientPool = new JedisPool(config, server, port);
                _logger.trace("connected");
            } catch (Exception e) {
                _logger.error("exception raised with message: {}", e.getMessage());
            }
        }
        else
        {
            _logger.trace("redis client has connected to server!");
        }
    }

    private Jedis getRedisClient() {

        Jedis client;
        client = clientPool.getResource();

        if (client.getDB() > 0 || client.getDB() != this.getDB()) {
            _logger.trace("switch to new db, {}, {}", client.getDB(), this.getDB());
            client.select(this.getDB());
        } else {
            _logger.debug("using existence db.");
        }

        if (client==null) {
            _logger.error("redis client is null.");
        }

        return client;
    }

    private void returnResource(Jedis client) {
        if (client != null) {
            client.close();
            _logger.trace("return resource to pool." );
        } else {
            _logger.error("redis client is null...");
        }
    }

    private int getIntValueFromParamters(int index, String...params) {
        int result = 0;
        if (params == null) {
            _logger.error("parameters is empty.");
        } else {
            String str = params[index];
            result = Integer.valueOf(str).intValue();
        }
        return result;
    }

    private float getFloatValueFromParameters(int index, String...params) {
        float result = 0;
        if (params == null) {
            _logger.error("parameters is empty.");
        } else {
            String str = params[index];
            str = str.replace("\\n", "");
            result = Float.valueOf(str).floatValue();
        }
        return result;
    }


    private long getLongValueFromParameters(int index, String...params) {
        long result = 0;
        if (params == null) {
            _logger.error("parameters is empty.");
        } else {
            String str = params[index];
            str = str.replace("\\n", "");
            result = Long.valueOf(str);
        }
        return result;
    }



    private String getStringValueFromParameters(int index, String...params) {
        return params[index];
    }

    protected  Object execute(RedisActionEnum action, String key, String value, String...params) {
        Object result = null;
        Jedis client = null;
        try {
            client = this.getRedisClient();
            //_logger.debug("action: {}, set {} value {}", action, key, value);

            if (client == null) {
                return result;
            }

            switch (action) {
                case SET_VALUE: {
                    client.set(key, value);
                    break;
                }

                case SET_VALUE_NOT_EX :{
                    result = client.setnx(key, value).longValue() == 1 ? true : false ;
                }

                case SET_VALUE_WITH_TIMEOUT: {
                    int seconds = this.getIntValueFromParamters(0, params);
                    client.setex(key, seconds, value);
                    break;
                }

                case APPEND_VALUE:
                    client.append(key, value);
                    break;

                case SET_VALUE_EXPIRED: {
                    int seconds = this.getIntValueFromParamters(0, params);
                    client.expire(key, seconds);
                    break;
                }

                case M_GET: {
                    result = client.mget(params);
                    break;
                }

                case DEL_VALUE_WITH_MULTI_KEYS: {
                    client.del(params);
                    break;
                }

                case GET_VALUE_BY_KEY: {
                    result = client.get(key);
                    break;
                }

                case GET_KEY_WITH_PATTEN: {
                    String patten = this.getStringValueFromParameters(0, params);
                    result = client.keys(patten);
                    break;
                }

                case GET_KEYS_WITH_PATTEN: {
                    String patten = this.getStringValueFromParameters(0, params);
                    result = client.keys(patten);
                    break;
                }

                case DEL_KEYS: {
                    String patten = this.getStringValueFromParameters(0, params);
                    Set<String> keys = client.keys(patten);
                    if (null != keys) {
                        for (String k: keys) {
                            client.del(k);
                        }
                    }
                    break;
                }
                case S_ADD: {
                    String member = this.getStringValueFromParameters(0, params);
                    client.sadd(key, member);
                    break;
                }
                case S_ADD_WITH_TIMEOUT: {
                    String member = this.getStringValueFromParameters(0, params);
                    int seconds = this.getIntValueFromParamters(1, params);
                    client.sadd(key, member);
                    client.expire(key, seconds);
                    break;
                }

                //Remove the specified member from the set value stored at key.
                case S_DEL: {
                    String member = this.getStringValueFromParameters(0, params);
                    client.srem(key, member);
                    break;
                }

                case S_IS_MEMBER: {
                    String member = this.getStringValueFromParameters(0, params);
                    result = client.sismember(key, member);
                    break;
                }

                case S_MEMBERS: {
                    result = client.smembers(key);
                    break;
                }
                case INCREASE: {
                	result =  client.incr(key);
                    break;
                }
                case INCREASE_BY: {
                    client.incrBy(key, this.getLongValueFromParameters(0, params));
                }
                case DECR: {
                    client.decr(key);
                    break;
                }
                case DECR_BY: {
                    client.decrBy(key, this.getLongValueFromParameters(0, params));
                    break;
                }
                case H_INCREASE:{
                    String field = this.getStringValueFromParameters(0, params);
                    float increaseValue = this.getFloatValueFromParameters(1, params);
                    client.hincrBy(key, field, (long) increaseValue);
                    break;
                }
                case H_GETALL: {
                    result = client.hgetAll(key);
                    break;
                }
                case H_MGET: {
                    result = client.hmget(key, params);
                    break;
                }
                case H_DEL: {
                    client.hdel(key, params);
                    break;
                }
                case H_EXISTS: {
                	String field = this.getStringValueFromParameters(0, params);
                	result = client.hexists(key, field);
                    break;
                }
                case H_GET: {
                	String field = this.getStringValueFromParameters(0, params);
                	result = client.hget(key, field);
                    break;
                }
                case H_INCRBY: {
                	String field = this.getStringValueFromParameters(0, params);
                	long hincrbyValue = Long.parseLong(value);
                	result = client.hincrBy(key, field, hincrbyValue);
                    break;
                }
                case H_MSET: {
                	Map<String, String> map = JsonUtil.stringToTObj(value, (new HashMap<String, String>()).getClass());
                    client.hmset(key, map);
                    break;
                }
                case S_CARD: {
                	result = client.scard(key);
                    break;
                }
                case EXISTS: {
                	result = client.exists(key);
                    break;
                }
                case R_PUSH:{
                    client.rpush(key, value);
                    break;
                }
                case L_POP: {
                    result = client.lpop(key);
                    break;
                }
                case L_LEN:{
                	result = client.llen(key);
                    break;
                }
                case L_REM: {
                	String value2 = this.getStringValueFromParameters(0, params);
                    result = client.lrem(key, Long.parseLong(value), value2);
                    break;
                }
                case L_RANGE:{
                	long value2 = this.getLongValueFromParameters(0, params);
                	result = client.lrange(key, Long.parseLong(value), value2);
                    break;
                }
                case L_TRIM: {
                	long value2 = this.getLongValueFromParameters(0, params);
                    result = client.ltrim(key, Long.parseLong(value), value2);
                    break;
                }
                case SORT: {
                    result = client.sort(key);
                    break;
                }
                case SORT_DESC: {
                	SortingParams sortingParams = new SortingParams();
                    sortingParams.desc();
                    result = client.sort(key,sortingParams);
                    break;
                }

                default:
                    _logger.debug("no action.");
            }
        }
        catch(Exception e){
            _logger.error("exception raised: host->{} db->{}, key->{}, msg-> {}"+ e.getMessage());
        }
        finally {
            this.returnResource(client);
        }

        return result;
    }
}
