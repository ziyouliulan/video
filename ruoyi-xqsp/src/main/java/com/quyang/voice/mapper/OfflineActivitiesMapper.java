package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.OfflineActivities;

/**
 * @Entity com.quyang.voice.model.OfflineActivities
 */
public interface OfflineActivitiesMapper extends BaseMapper<OfflineActivities> {

}




