package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.GroupConfig;

/**
* @author lpden
* @description 针对表【group_config(参数配置表)】的数据库操作Mapper
* @createDate 2022-04-19 14:56:06
* @Entity com.quyang.voice.model.GroupConfig
*/
public interface GroupConfigMapper extends BaseMapper<GroupConfig> {

}




