package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.LitemallGoods;

/**
 * @Entity com.quyang.voice.model.LitemallGoods
 */
public interface LitemallGoodsMapper extends BaseMapper<LitemallGoods> {

}




