package com.quyang.voice.mapper;

import com.quyang.voice.model.ClientConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【client_config(参数配置表)】的数据库操作Mapper
* @createDate 2022-04-24 13:50:30
* @Entity com.quyang.voice.model.ClientConfig
*/
public interface ClientConfigMapper extends BaseMapper<ClientConfig> {

}




