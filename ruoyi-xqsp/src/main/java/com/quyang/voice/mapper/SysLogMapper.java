package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.SysLog;

/**
 * @Entity com.quyang.voice.model.SysLog
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}




