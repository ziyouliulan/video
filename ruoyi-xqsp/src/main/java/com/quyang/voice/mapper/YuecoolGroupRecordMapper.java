package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.YuecoolGroupRecord;

/**
* @author lpden
* @description 针对表【yuecool_group_record(消费记录)】的数据库操作Mapper
* @createDate 2021-12-30 17:33:12
* @Entity com.quyang.voice.model.YuecoolGroupRecord
*/
public interface YuecoolGroupRecordMapper extends BaseMapper<YuecoolGroupRecord> {

}




