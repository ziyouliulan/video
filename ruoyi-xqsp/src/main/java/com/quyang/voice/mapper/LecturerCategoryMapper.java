package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.LecturerCategory;

/**
 * @Entity com.quyang.voice.model.LecturerCategory
 */
public interface LecturerCategoryMapper extends BaseMapper<LecturerCategory> {

}




