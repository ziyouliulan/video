package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.CourseDetails;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

/**
 * @Entity com.quyang.voice.model.CourseDetails
 */
@Mapper
public interface CourseDetailsMapper extends BaseMapper<CourseDetails> {

    public List<CourseDetails> selectByDept(HashMap map);


    public HashMap<String,String> selectByUserId(Integer userId);


    public List<CourseDetails> learningRecords(Integer userId);




}




