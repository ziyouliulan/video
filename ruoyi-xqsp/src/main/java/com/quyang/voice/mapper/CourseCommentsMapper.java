package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.CourseComments;

/**
 * @Entity com.quyang.voice.model.CourseComments
 */
public interface CourseCommentsMapper extends BaseMapper<CourseComments> {

}




