package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.IntegralTrendChart;

/**
 * @Entity com.quyang.voice.model.IntegralTrendChart
 */
public interface IntegralTrendChartMapper extends BaseMapper<IntegralTrendChart> {

}




