package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.YuecoolCarType;

/**
 * @Entity com.quyang.voice.model.YuecoolCarType
 */
public interface YuecoolCarTypeMapper extends BaseMapper<YuecoolCarType> {

}




