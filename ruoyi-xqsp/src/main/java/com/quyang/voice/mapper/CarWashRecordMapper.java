package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.CarWashRecord;

/**
 * @Entity com.quyang.voice.model.CarWashRecord
 */
public interface CarWashRecordMapper extends BaseMapper<CarWashRecord> {

}




