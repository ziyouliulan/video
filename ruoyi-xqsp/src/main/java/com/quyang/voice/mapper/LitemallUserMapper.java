package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.LitemallUser;

/**
 * @Entity com.quyang.voice.model.LitemallUser
 */
public interface LitemallUserMapper extends BaseMapper<LitemallUser> {

}




