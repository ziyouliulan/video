package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.GroupUpgrade;

/**
 * @Entity com.quyang.voice.model.GroupUpgrade
 */
public interface GroupUpgradeMapper extends BaseMapper<GroupUpgrade> {

}




