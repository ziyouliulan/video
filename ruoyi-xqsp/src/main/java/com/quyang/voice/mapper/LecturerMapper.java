package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Lecturer;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.quyang.voice.model.Lecturer
 */
@Mapper
public interface LecturerMapper extends BaseMapper<Lecturer> {

}




