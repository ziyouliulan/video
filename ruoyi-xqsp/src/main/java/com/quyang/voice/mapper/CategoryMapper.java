package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.quyang.voice.model.Category
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {

}




