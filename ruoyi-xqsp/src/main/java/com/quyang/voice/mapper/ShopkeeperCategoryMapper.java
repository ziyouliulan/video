package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.ShopkeeperCategory;

/**
 * @Entity com.quyang.voice.model.ShopkeeperCategory
 */
public interface ShopkeeperCategoryMapper extends BaseMapper<ShopkeeperCategory> {

}




