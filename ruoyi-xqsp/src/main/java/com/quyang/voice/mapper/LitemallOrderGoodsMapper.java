package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.LitemallOrderGoods;

/**
 * @Entity com.quyang.voice.model.LitemallOrderGoods
 */
public interface LitemallOrderGoodsMapper extends BaseMapper<LitemallOrderGoods> {

}




