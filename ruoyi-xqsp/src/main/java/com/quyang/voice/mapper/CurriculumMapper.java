package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Curriculum;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.quyang.voice.model.Curriculum
 */
@Mapper
public interface CurriculumMapper extends BaseMapper<Curriculum> {

}




