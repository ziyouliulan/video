package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.YuecoolNotice;
import org.apache.ibatis.annotations.Mapper;

/**
* @author lpden
* @description 针对表【yuecool_notice】的数据库操作Mapper
* @createDate 2021-12-15 15:23:34
* @Entity com.quyang.voice.model.YuecoolNotice
*/
@Mapper
public interface YuecoolNoticeMapper extends BaseMapper<YuecoolNotice> {
    YuecoolNotice getNticeById(Long id);
}




