package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Shopkeeper;

/**
* @author lpden
* @description 针对表【yuecool_shopkeeper(讲师)】的数据库操作Mapper
* @createDate 2021-12-12 16:40:11
* @Entity com.quyang.voice.model.Shopkeeper
*/
public interface ShopkeeperMapper extends BaseMapper<Shopkeeper> {

}




