package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Yinhangka;

/**
* @author lpden
* @description 针对表【yuecool_yinhangka】的数据库操作Mapper
* @createDate 2022-01-10 17:22:38
* @Entity com.quyang.voice.model.Yinhangka
*/
public interface YinhangkaMapper extends BaseMapper<Yinhangka> {

}




