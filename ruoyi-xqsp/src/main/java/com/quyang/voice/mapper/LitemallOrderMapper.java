package com.quyang.voice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.LitemallOrder;

/**
 * @Entity com.quyang.voice.model.LitemallOrder
 */
public interface LitemallOrderMapper extends BaseMapper<LitemallOrder> {

}




