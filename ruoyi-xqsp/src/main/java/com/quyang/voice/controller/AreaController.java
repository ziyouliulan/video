
package com.quyang.voice.controller;

import com.quyang.voice.dao.AreaDao;
import com.quyang.voice.model.Area;
import com.quyang.voice.model.AreaExample;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
* <p>说明： 地区API接口层</P>
* @version: V1.0
* @author: liuhaotian
* @time    2020年12月14日
*
*/
@Api(value = "地区城市",tags="地区城市" )
@RestController
@RequestMapping("/api/area")
public class AreaController {

	private static final Logger logger = LoggerFactory.getLogger(AreaController.class);




	@Autowired
	AreaDao areaDao;

	/**
	 * @explain 查询对象
	 * @author  liuhaotian
	 * @time    2020年12月14日
	 */
	@GetMapping("/getCity")
	@ApiOperation(value = "获得城市信息", notes = "获得城市信息")
	@ApiImplicitParams({
			@ApiImplicitParam(dataType = "String", name = "areaCode",value = "省份的地区id")
	})
	public ResponseUtil<List<Area>> getAreaById(String areaCode ){
		logger.info("request param,areaCode is "+areaCode);

		AreaExample areaExample = new AreaExample();

		areaExample.createCriteria().andLayerEqualTo(2).andParentCodeEqualTo(areaCode);

		return ResponseUtil.suc(areaDao.selectByExample(areaExample));
	}

	/**
	 * @explain 查询对象
	 * @author  liuhaotian
	 * @time    2020年12月14日
	 */
	@GetMapping("/getProvince")
	@ApiOperation(value = "获得省份列表", notes = "获得省份列表")
	public ResponseUtil<List<Area>> getAreaById(){

		AreaExample areaExample = new AreaExample();

		areaExample.createCriteria().andLayerEqualTo(1);

		return ResponseUtil.suc(areaDao.selectByExample(areaExample));
	}



}
