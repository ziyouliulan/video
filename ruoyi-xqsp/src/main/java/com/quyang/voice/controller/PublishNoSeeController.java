
package com.quyang.voice.controller;

import com.quyang.voice.model.PublishNoSee;
import com.quyang.voice.service.IPublishNoSeeService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 不让用户看朋友圈表API接口层</P>
* @version: V1.0
* @author: liuhaotian
* @time    2021年02月01日
*
*/
@Api(value = "不让用户看朋友圈",tags="不让用户看朋友圈" )
@RestController
@RequestMapping("/api/publishNoSee")
public class PublishNoSeeController {

	private static final Logger logger = LoggerFactory.getLogger(PublishNoSeeController.class);

     @Autowired
     IPublishNoSeeService publishNoSeeService;

	/**
	 * @explain 查询对象
	 * @author  liuhaotian
	 * @time    2021年02月01日
	 */
	@GetMapping("/getPublishNoSeeById/{id}")
	@ApiOperation(value = "查询不让用户看朋友圈表", notes = "查询不让用户看朋友圈表")
	@ApiImplicitParam(paramType="path", name = "id", value = "不让用户看朋友圈表id", required = true, dataType = "Long")
	public ResponseUtil<PublishNoSee> getPublishNoSeeById(@PathVariable("id")Long id){
		logger.info("request param,id is "+id);
		return publishNoSeeService.getPublishNoSeeById(id);
	}

	/**
	 * @explain 删除对象
	 * @author  liuhaotian
	 * @time    2021年02月01日
	 */
	@PostMapping("/deletePublishNoSeeById")
	@ApiOperation(value = "删除不让用户看朋友圈表", notes = "删除不让用户看朋友圈表")
	@ApiImplicitParam(paramType="query", name = "id", value = "不让用户看朋友圈表id", required = true, dataType = "Long")
	public ReturnJson deletePublishNoSeeById(Long id){
		logger.info("request param,id is "+id);
		return publishNoSeeService.deletePublishNoSeeById(id);
	}

	/**
	 * @explain 添加对象
	 * @author  liuhaotian
	 * @time    2021年02月01日
	 */
	@PostMapping("/addPublishNoSee")
	@ApiOperation(value = "新增不让用户看朋友圈表", notes = "新增不让用户看朋友圈表")
	@ApiImplicitParams(
		@ApiImplicitParam(dataType = "PublishNoSee",name = "publishNoSee")
	)
	public ReturnJson insertPublishNoSee(@ApiIgnore PublishNoSee publishNoSee){
		logger.info("request param,PublishNoSee is "+publishNoSee);
		return publishNoSeeService.insertPublishNoSee(publishNoSee);
	}

	/**
	 * @explain 修改对象
	 * @author  liuhaotian
	 * @time    2021年02月01日
	 */
	@PostMapping("/updatePublishNoSee")
	@ApiOperation(value = "修改不让用户看朋友圈表", notes = "修改不让用户看朋友圈表")
	@ApiImplicitParams(
		@ApiImplicitParam(dataType = "PublishNoSee",name = "publishNoSee")
	)
	public ReturnJson updatePublishNoSee(@ApiIgnore PublishNoSee publishNoSee){
		logger.info("request param,PublishNoSee is "+publishNoSee);
		return publishNoSeeService.updatePublishNoSee(publishNoSee);
	}

}
