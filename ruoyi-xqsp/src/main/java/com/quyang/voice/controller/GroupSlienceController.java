
package com.quyang.voice.controller;

import com.quyang.voice.model.GroupSlience;
import com.quyang.voice.model.vo.UserBaseVo;
import com.quyang.voice.service.IGroupSlienceService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


/**
* <p>说明： 群禁言API接口层</P>
* @version: V1.0
* @author: wlx
* @time    2021年02月03日
*
*/
@Api(value = "群禁言",tags="群禁言" )
@RestController
@RequestMapping("/api/groupSlience")
public class GroupSlienceController {

	private static final Logger logger = LoggerFactory.getLogger(GroupSlienceController.class);

     @Autowired
     IGroupSlienceService groupSlienceService;




	/**
	 * @explain 查询群禁言列表
	 * @author  wlx
	 * @time    2021年02月03日
	 */
	@GetMapping("/getGroupSlienceById")
	@ApiOperation(value = "查询群禁言", notes = "查询群禁言")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "clusterId", value = "群Id", required = true, dataType = "Long")
	})
	public ResponseUtil<List<UserBaseVo>> getGroupSlienceById(Long clusterId){
		logger.info("request param,clusterId is "+clusterId);
		return groupSlienceService.getGroupSlienceById(clusterId);
	}

	/**
	 * @explain 删除对象
	 * @author  wlx
	 * @time    2021年02月03日
	 */
	@PostMapping("/deleteGroupSlienceById")
	@ApiOperation(value = "解除个人群禁言", notes = "解除个人群禁言")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "clusterId", value = "群Id", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "userId", value = "被禁言人id", required = true, dataType = "Long")
		})
	public ReturnJson deleteGroupSlience(Long clusterId,Long userId){
		logger.info("request param,id is "+clusterId+",userId is "+userId);
		return groupSlienceService.deleteGroupSlienceById(clusterId,userId);
	}


	/**
	 * @explain 添加对象
	 * @author  wlx
	 * @time    2021年02月03日
	 */
	@PostMapping("/addGroupSlience")
	@ApiOperation(value = "新增个人群禁言", notes = "新增个人群禁言")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "clusterId", value = "群Id", required = true, dataType = "Long"),
		@ApiImplicitParam(name = "userId", value = "被禁言人id", required = true, dataType = "Long")
	})
	public ReturnJson insertGroupSlience(@ApiIgnore GroupSlience groupSlience){
		logger.info("request param,GroupSlience is "+groupSlience);
		return groupSlienceService.insertGroupSlience(groupSlience);
	}



}
