
package com.quyang.voice.controller;

import com.github.pagehelper.PageInfo;
import com.quyang.voice.model.Publishs;
import com.quyang.voice.model.PublishsLike;
import com.quyang.voice.model.vo.PublishCommentsVo;
import com.quyang.voice.model.vo.PublishInfoVo;
import com.quyang.voice.model.vo.PublishsListVo;
import com.quyang.voice.model.vo.PublishsVo;
import com.quyang.voice.service.IPublishCommentsReplyService;
import com.quyang.voice.service.IPublishsService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import com.ruoyi.xqsp.domain.ToolLocalStorage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


/**
* <p>说明： 世界动态API接口层</P>
* @version: V1.0
* @author: wlx
* @time    2020年12月01日
*
*/
@Api(value = "朋友圈",tags="朋友圈" )
@RestController
@RequestMapping("/xqsp/publishs")
public class PublishsController {

	private static final Logger logger = LoggerFactory.getLogger(PublishsController.class);

     @Autowired
     IPublishsService publishsService;

     @Autowired
	IPublishCommentsReplyService publishCommentsReplyService;

	/**
	 * @explain 查询对象
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@GetMapping("/getPublishsById")
	@ApiOperation(value = "查询动态详情", notes = "查询动态详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "checkUserId", value = "查看的用户id", required = true, dataType = "Integer"),
		@ApiImplicitParam(name = "id", value = "动态id", required = true, dataType = "Long")
	})
	public ResponseUtil<PublishInfoVo> getPublishsById(Long id,Integer checkUserId){
		logger.info("request param,id is "+id+"param,checkUserId is "+checkUserId);
		return publishsService.getPublishsById(id,checkUserId);
	}


	/**
	 * @explain 查询对象
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@GetMapping("/getPublishsList")
	@ApiOperation(value = "分页查询动态列表", notes = "分页查询动态列表")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "checkUserId", value = "查看的用户id", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer")
	})
	public ResponseUtil<PageInfo<PublishsVo>> getPublishsList(Integer checkUserId,Integer pageNum, Integer pageSize ){
		logger.info("request param,checkUserId is "+checkUserId+",pageNum is "+pageNum+"param,pageSize is "+pageSize);
		return publishsService.getPublishsList(checkUserId,pageNum,pageSize);
	}

	/**
	 * @explain 删除对象
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@PostMapping("/deletePublishsById")
	@ApiOperation(value = "删除动态", notes = "删除世界动态")
	@ApiImplicitParam(name = "ids", value = "世界动态ids，删除多个用逗号隔开", required = true, dataType = "String")
	public ReturnJson deletePublishsById(String ids){
		logger.info("request param,ids is "+ids);

		return publishsService.deletePublishsById(ids);
	}



	/**
	 * @explain 添加对象
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@PostMapping("/addUploadPublishs")
	@ApiOperation(value = "新增动态", notes = "新增动态")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "发布的用户id", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "content", value = "动态内容", required = true, dataType = "String"),
			@ApiImplicitParam(name = "type", value = "1 文字+图片、2 文字+视频 3：纯文字", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "picUrl", value = "图片url，多张已逗号分隔,图片和视频只能传一种", required = false, dataType = "String"),
			@ApiImplicitParam(name = "videoUrl", value = "视频url,图片和视频只能传一种", required = false, dataType = "String"),
			@ApiImplicitParam(name = "videoCoverUrl", value = "视频封面url,只要有视频url，这个字段必须有", required = false, dataType = "String"),
			@ApiImplicitParam(name = "noSeeStatus", value = "知识付费的时候参数为4", required = false, dataType = "Integer"),
	})
	public ReturnJson addUploadPublishs(@ApiIgnore Publishs publishs, MultipartFile[] file){
//		List<ToolLocalStorage> toolLocalStorages = storageService.create(null, file);

		logger.info("request param,Publishs is "+publishs);

//		switch (publishs.getType()){
//			case 1:
//				String[] strings = new String[toolLocalStorages.size()];
//				for (int i = 0; i < toolLocalStorages.size(); i++) {
//					strings[i] = toolLocalStorages.get(i).getImgUrl();
//				}
//				publishs.setPicUrl(StringUtils.join(strings,","));
//				break;
//			case 2:
//				publishs.setVideoUrl(toolLocalStorages.get(0).getImgUrl());
//				publishs.setVideoCoverUrl(toolLocalStorages.get(1).getImgUrl());
//
//				break;
//		}

		return publishsService.insertPublishs(publishs);
	}

	/**
	 * @explain 添加对象
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@PostMapping("/addPublishs")
	@ApiOperation(value = "新增动态", notes = "新增动态")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "发布的用户id", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "content", value = "动态内容", required = true, dataType = "String"),
			@ApiImplicitParam(name = "type", value = "1 文字+图片、2 文字+视频 3：纯文字", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "picUrl", value = "图片url，多张已逗号分隔,图片和视频只能传一种", required = false, dataType = "String"),
			@ApiImplicitParam(name = "videoUrl", value = "视频url,图片和视频只能传一种", required = false, dataType = "String"),
			@ApiImplicitParam(name = "videoCoverUrl", value = "视频封面url,只要有视频url，这个字段必须有", required = false, dataType = "String"),
			@ApiImplicitParam(name = "noSeeStatus", value = "知识付费的时候参数为4", required = false, dataType = "Integer"),
	})
	public ReturnJson insertPublishs(@ApiIgnore Publishs publishs){
		logger.info("request param,Publishs is "+publishs);
		return publishsService.insertPublishs(publishs);
	}

	/**
	 * @explain 查询对象
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@GetMapping("/getMyPublishList")
	@ApiOperation(value = "分页查询动态列表(我的作品列表)", notes = "分页查询动态列表(我的作品列表)")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "noSeeStatus", value = "状态", required = true, dataType = "Integer"),
	})
	public ResponseUtil<PageInfo<PublishsListVo>> getMyPublishList(Integer pageNum, Integer pageSize, Long userId,Integer noSeeStatus){
		logger.info("request param,userId is "+userId);
		return publishsService.getMyPublishList(pageNum,pageSize,userId,noSeeStatus);

	}

	/**
	 * @explain 分页条件查询
	 * @author  liuhaotian
	 * @time    2020年10月23日
	 */
	@GetMapping("/getPublishsByUser")
	@ApiOperation(value = "根据userId获取该用户发布的动态", notes = "分页查询返回")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "toUserId", value = "查看用户ID", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer")
	})
	public ResponseUtil<PageInfo<PublishsVo>> getPublishsByUser(Integer pageNum, Integer pageSize, Integer userId,Integer toUserId){
		logger.info("request param,pageNum is "+pageNum+",pageSize is "+pageSize+",userId is"+userId);
		return publishsService.getPublishsByUser(pageNum ,pageSize,userId,toUserId);
	}

	/**
	 * @explain 分页条件查询
	 * @author  liuhaotian
	 * @time    2020年10月23日
	 */
	@GetMapping("/getPublishsByUserId")
	@ApiOperation(value = "根据userId获取该用户发布的动态  不区分好友", notes = "分页查询返回")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "noSeeStatus", value = "1： 朋友圈和短视频  4知识付费", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
	})
	public ResponseUtil<PageInfo<PublishsVo>> getPublishsByUserId(Integer pageNum, Integer pageSize, Integer userId,Integer noSeeStatus){
		logger.info("request param,pageNum is "+pageNum+",pageSize is "+pageSize+",userId is"+userId);
		return publishsService.getPublishsByUserId(pageNum ,pageSize,userId,noSeeStatus);
	}

	/**
	 * @explain 点击点赞
	 * 点击一下喜欢，再点一下取消
	 * @author  wlx
	 * @time    2020年12月01日
	 */
	@PostMapping("/postPublishsLike")
	@ApiOperation(value = "动态点赞/取消点赞(收藏)", notes = "动态点赞/取消点赞(收藏)")
	@ApiImplicitParams({
			@ApiImplicitParam(value = "动态id", name = "publishId", required = true, dataType = "Long"),
			@ApiImplicitParam(value = "点赞的用户id", name = "likedPostId", required = true, dataType = "Long"),
			@ApiImplicitParam(value = "点赞状态，0取消，1点赞", name = "status", required = true, dataType = "Integer"),
	})
	public ReturnJson postPublishsLike(@ApiIgnore PublishsLike publishsLike){
		logger.info("request param,postPublishsLike is "+ publishsLike);
		return publishsService.postPublishsLike(publishsLike);
	}


//	/**
//	 * @explain 添加对象
//	 * @author  wulixiang
//	 * @time    2020年12月01日
//	 */
//	@PostMapping("/addPublishComments")
//	@ApiOperation(value = "新增动态评论", notes = "新增动态评论")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "publishId", value = "动态id", required = true, dataType = "Long"),
//			@ApiImplicitParam(name = "userId", value = "评论用户id", required = true,dataType = "Long"),
//			@ApiImplicitParam(name = "isAuthor", value = "是否是作者：0不是 1是", required = true,dataType = "Integer"),
//			@ApiImplicitParam(name = "comment", value = "评论内容", required = true, dataType = "String"),
//			@ApiImplicitParam(name = "commentPic", value = "评论图片url", dataType = "String")
//	})
//	public ReturnJson insertPublishComments(@ApiIgnore PublishComments publishComments){
//		logger.info("request param,PublishComments is "+publishComments);
//		return publishsService.insertPublishComments(publishComments);
//	}
//
//
//
//	/**
//	 * @explain 删除对象
//	 * @author  wulixiang
//	 * @time    2020年9月10日
//	 */
//	@PostMapping("/deletePublishCommentsById")
//	@ApiOperation(value = "删除动态评论", notes = "删除动态评论")
//	@ApiImplicitParam(paramType="query", name = "id", value = "动态评论id", required = true, dataType = "Long")
//	public ReturnJson deletePublishCommentsById(Long id){
//		logger.info("request param,id is "+id);
//		return publishsService.deletePublishCommentsById(id);
//	}


	/**
	 * @explain 分页查询某个动态的评论
	 * @author  wlx
	 * @time    2020年12月02日
	 */
	@GetMapping("/queryPublishComments")
	@ApiOperation(value = "分页查询某个动态的评论", notes = "分页查询某个动态的评论")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "id", value = "世界动态id", required = true, dataType = "Long")
	})
	public ResponseUtil<PageInfo<PublishCommentsVo>> queryPublishComments(Integer pageNum, Integer pageSize,Long id){
		logger.info("request param,id is "+id);
		return publishsService.queryPublishComments(pageNum,pageSize,id);
	}


}
