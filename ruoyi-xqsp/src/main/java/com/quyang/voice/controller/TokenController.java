//package com.quyang.voice.controller;
//
//import cn.hutool.core.util.RandomUtil;
//import com.quyang.voice.model.MissuUsers;
//import com.quyang.voice.model.Token;
//import com.quyang.voice.quartz.RedisUtils;
//import com.quyang.voice.quartz.SpringContextHolder;
//import com.quyang.voice.service.IMissuUsersService;
//import com.quyang.voice.utils.ResponseUtil;
//import com.quyang.voice.utils.realPersonAuthentication.InitFaceVerify;
//import io.agora.media.RtcTokenBuilder;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.models.auth.In;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//@Api(value = "用户管理",tags="Token" )
//@RestController
//@RequestMapping("api/token")
//@CrossOrigin
//public class TokenController {
//
//
//    @Value("${rtc.appid}")
//    String appId;
//
//    @Value("${rtc.appcertificate}")
//    String appCertificate;
//
//
//    @Autowired
//    IMissuUsersService missuUsersService;
//
//    @Autowired
//    RedisUtils redisUtils;
//
//    @Autowired
//    InitFaceVerify initFaceVerify;
////
////
//////    static String channelName = "你好啊";
//////    static String userAccount = "2082341273";
//////    static int uid = 2082341273;
////
//
//    @PostMapping("generate")
//    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "String", name = "channelName",value = "频道号",required = true),
//            @ApiImplicitParam(dataType = "Integer",defaultValue = "0" ,name = "uid",value = "如果填 0，则表示不对 uid 鉴权",required = false),
//            @ApiImplicitParam(dataType = "Integer",defaultValue = "3600", name = "expirationTimeInSeconds",value = "token服务过期时间",required = false),
//    })
//    public ResponseEntity generate(String channelName,
//                                           Integer uid,Integer expirationTimeInSeconds){
//
//
//        System.out.println(appId);
//        RtcTokenBuilder token = new RtcTokenBuilder();
//        int timestamp = (int)(System.currentTimeMillis() / 1000 + 3600);
////        String result = token.buildTokenWithUserAccount(appId, appCertificate,
////                channelName, userAccount, RtcTokenBuilder.Role.Role_Publisher, timestamp);
////        System.out.println(result);
//
//        String result = token.buildTokenWithUid(appId, appCertificate,
//                channelName, 0, RtcTokenBuilder.Role.Role_Publisher, timestamp);
//        System.out.println(result);
//      return new ResponseEntity(ResponseUtil.suc(result),HttpStatus.OK);
//    }
//
//
//    @PostMapping("generates")
//    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "String", name = "channelName",value = "频道号",required = true),
//            @ApiImplicitParam(dataType = "Integer",defaultValue = "0" ,name = "uid",value = "如果填 0，则表示不对 uid 鉴权",required = false),
//            @ApiImplicitParam(dataType = "Integer",defaultValue = "3600", name = "expirationTimeInSeconds",value = "token服务过期时间",required = false),
//            @ApiImplicitParam(dataType = "Integer",defaultValue = "3600", name = "refresh",value = "刷新",required = false),
//    })
//    public ResponseEntity generates(String gid,
//                                   Integer uid,Integer expirationTimeInSeconds,Integer refresh){
//
//        String randomString = RandomUtil.randomString(10);
//        HashMap map = new HashMap();
//        switch (refresh){
//            case 1:
//                System.out.println(appId);
//                RtcTokenBuilder token = new RtcTokenBuilder();
//                int timestamp = (int)(System.currentTimeMillis() / 1000 + 3600);
//
//                String result = token.buildTokenWithUid(appId, appCertificate,
//                        randomString, 0, RtcTokenBuilder.Role.Role_Publisher, timestamp);
//                System.out.println(result);
//
//                map.put("token",result);
//                map.put("channelName",randomString);
//                redisUtils.set(gid,map);
//                break;
//            case 2:
//                map = (HashMap) redisUtils.get(gid);
//                break;
//        }
//
//        return new ResponseEntity(ResponseUtil.suc(map),HttpStatus.OK);
//    }
//
//
//
//
//    @PostMapping("systemTime")
//    public ResponseEntity systemTime(){
//
//        return new ResponseEntity(ResponseUtil.suc(System.currentTimeMillis()),HttpStatus.OK);
//    }
//
//
//
//    @GetMapping("sdkToken")
//    public ResponseUtil sdkToken(){
//        String sdkToken = "";
//
//        try {
//            Map<String, String> map = new HashMap<>();
//            // Token 的权限，取值包括 TokenRole.Admin，TokenRole.Writer，TokenRole.Reader。
//            map.put("role", Token.TokenRole.Reader.getValue());
//
//            sdkToken = Token.sdkToken(
//                    "zLFWY5grBhrW9a7G", // 将 Your AK 替换成你从控制台获取的 AK。
//                    "5fuIsDfDRMV7rApUKK0Fd7A8UAXzuSTD", // 将 Your SK 替换成你从控制台获取的 SK。
//                    1000 * 60 * 10, // Token 有效时间，单位为毫秒。设为 0 时，表示永不过期。
//                    map);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        return ResponseUtil.suc(sdkToken);
//    }
//
//    @GetMapping("getCertifyId")
//    public ResponseUtil getCertifyId(String metaInfo,Integer userId,String realName,String idCardNo){
//
//        String certifyId = initFaceVerify.getCertifyId(metaInfo,userId,realName,idCardNo);
//        redisUtils.set(certifyId,userId,10, TimeUnit.MINUTES);
//        return ResponseUtil.suc(certifyId);
//    }
//
//    @GetMapping("bindIDCard")
//    public ResponseUtil bindIDCard(MissuUsers missuUsers){
//
//        missuUsersService.updateMissuUsers(missuUsers);
//
//        return ResponseUtil.suc();
//    }
//
//
//
//    public static void main(String[] args) throws Exception {
//        Map<String, String> map = new HashMap<>();
//        // Token 的权限，可选值包括 TokenRole.Admin，TokenRole.Writer，TokenRole.Reader。
//        map.put("role", Token.TokenRole.Reader.getValue());
//        // 填入你的房间 UUID，可通过调用服务端创建房间 API 或获取房间列表 API 获取。
//        map.put("uuid", "房间的 UUID");
//
//        String roomToken = Token.roomToken(
//                "zLFWY5grBhrW9a7G", // 将 Your AK 替换成你从控制台获取的 AK。
//                "5fuIsDfDRMV7rApUKK0Fd7A8UAXzuSTD", // 将 Your SK 替换成你从控制台获取的 SK。
//                1000 * 60 * 10, // Token 有效时间，单位为毫秒。设为 0 时，表示永不过期。
//                map);
//
//        System.out.println(roomToken);
//    }
//}
