
package com.quyang.voice.controller;

import com.quyang.voice.model.PublishComments;
import com.quyang.voice.service.IPublishCommentsService;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 动态评论API接口层</P>
* @version: V1.0
* @author: wulixiang
* @time    2020年9月10日
*
*/
@Api(value = "publishComments", tags = "动态评论管理")
@RestController
@RequestMapping("/api/publishComments")
public class PublishCommentsController {

	private static final Logger logger = LoggerFactory.getLogger(PublishCommentsController.class);

     @Autowired
     IPublishCommentsService publishCommentsService;

	/**publishCommentsService
	 * @explain 删除对象
	 * @author  wulixiang
	 * @time    2020年9月10日
	 */
	@PostMapping("/deletePublishCommentsById")
	@ApiOperation(value = "删除动态评论", notes = "删除动态评论")
	@ApiImplicitParam(paramType="query", name = "id", value = "动态评论id", required = true, dataType = "Long")
	public ReturnJson deletePublishCommentsById(Long id){
		logger.info("request param,id is "+id);
		return publishCommentsService.deletePublishCommentsById(id);
	}

	/**
	 * @explain 添加对象
	 * @author  wulixiang
	 * @time    2020年9月10日
	 */
	@PostMapping("/addPublishComments")
	@ApiOperation(value = "新增动态评论", notes = "新增动态评论")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "publishId", value = "动态id", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "userId", value = "评论用户id", required = true,dataType = "Long"),
			@ApiImplicitParam(name = "userName", value = "评论用户名", required = true, dataType = "String"),
			@ApiImplicitParam(name = "comment", value = "评论内容", required = true, dataType = "String"),
			@ApiImplicitParam(name = "commentPic", value = "评论图片url", dataType = "String")
	})
	public ReturnJson insertPublishComments(@ApiIgnore PublishComments publishComments){
		logger.info("request param,PublishComments is "+publishComments);
		return publishCommentsService.insertPublishComments(publishComments);
	}

}
