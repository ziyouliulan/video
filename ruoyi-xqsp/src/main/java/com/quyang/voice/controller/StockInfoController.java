package com.quyang.voice.controller;

import com.github.pagehelper.PageInfo;
import com.quyang.voice.dao.StockInfoDao;
import com.quyang.voice.model.StockInfo;
import com.quyang.voice.model.StockInfoExample;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 股票咨讯
 * @author zx
 */
@RestController
@RequestMapping("/api/stockInfo")
@Api(value = "stockInfo", tags = "股票咨讯")
public class StockInfoController {

    private static final Logger logger = LoggerFactory.getLogger(StockInfoController.class);

    @Autowired
    StockInfoDao stockInfoDao;


    @GetMapping("/list")
    @ApiOperation(value = "咨讯列表", notes = "咨讯列表", consumes = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "咨讯名称", required = false, dataType = "String"),
            @ApiImplicitParam(name = "category", value = "咨讯分类", required = false, dataType = "Integer"),
            @ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer")
    })
    public ResponseUtil<PageInfo<StockInfo>> list(String name, Integer category , Long pageNum, Integer pageSize) {

        logger.info("request param,name is ===> " + name);
        StockInfoExample stockInfoExample = new StockInfoExample();
        stockInfoExample.createCriteria().andNameEqualTo(name);
        stockInfoExample.createCriteria().andCategoryEqualTo(category);
        stockInfoExample.setOffset(pageNum);
        stockInfoExample.setLimit(pageSize);

        List<StockInfo> stockInfos = stockInfoDao.selectByExample(stockInfoExample);
        return ResponseUtil.suc(stockInfos);

    }



    @GetMapping("/getById")
    @ApiOperation(value = "咨讯详情", notes = "咨讯详情", consumes = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "id", name = "咨讯Id", required = true, dataType = "Integer")
    })
    public ResponseUtil<StockInfo> detail(Integer id) {
        StockInfoExample stockInfoExample = new StockInfoExample();

        StockInfo stockInfo = stockInfoDao.selectByPrimaryKey(id);
        logger.info("request param,id is ===> " + id);

        return ResponseUtil.suc(stockInfo
        );

    }
}
