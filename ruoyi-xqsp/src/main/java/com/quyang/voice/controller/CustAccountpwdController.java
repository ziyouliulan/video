
package com.quyang.voice.controller;

import com.quyang.voice.model.CustAccountpwd;
import com.quyang.voice.service.ICustAccountpwdService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 用户钱包表API接口层</P>
* @version: V1.0
* @author: wlx
* @time    2021年02月19日
*
*/
@Api(value = "用户支付密码",tags="用户支付密码" )
@RestController
@RequestMapping("/api/custAccountpwd")
public class CustAccountpwdController {

	private static final Logger logger = LoggerFactory.getLogger(CustAccountpwdController.class);

     @Autowired
     ICustAccountpwdService cust_accountpwdService;

	/**
	 * @explain 查询对象
	 * @author  wlx
	 * @time    2021年02月19日
	 */
	@GetMapping("/getCustAccountpwdById/{id}")
	@ApiOperation(value = "getCustAccountpwd", notes = "查询用户钱包表")
	@ApiImplicitParam(paramType="path", name = "id", value = "用户钱包表id", required = true, dataType = "Long")
	public ResponseUtil<CustAccountpwd> getCustAccountpwdById(@PathVariable("id")Long id){
		logger.info("request param,id is "+id);
		return cust_accountpwdService.getCustAccountpwdById(id);
	}

	/**
	 * @explain 删除对象
	 * @author  wlx
	 * @time    2021年02月19日
	 */
	@PostMapping("/deleteCustAccountpwdById")
	@ApiOperation(value = "deleteCustAccountpwd", notes = "删除用户钱包表")
	@ApiImplicitParam(paramType="query", name = "id", value = "用户钱包表id", required = true, dataType = "Long")
	public ReturnJson deleteCustAccountpwdById(Long id){
		logger.info("request param,id is "+id);
		return cust_accountpwdService.deleteCustAccountpwdById(id);
	}

	/**
	 * @explain 添加对象
	 * @author  wlx
	 * @time    2021年02月19日
	 */
	@PostMapping("/addCustAccountpwd")
	@ApiOperation(value = "addCustAccountpwd", notes = "新增用户钱包表")
	@ApiImplicitParams(
		@ApiImplicitParam(dataType = "CustAccountpwd",name = "cust_accountpwd")
	)
	public ReturnJson insertCustAccountpwd(@ApiIgnore CustAccountpwd cust_accountpwd){
		logger.info("request param,CustAccountpwd is "+cust_accountpwd);
		return cust_accountpwdService.insertCustAccountpwd(cust_accountpwd);
	}

	/**
	 * @explain 修改对象
	 * @author  wlx
	 * @time    2021年02月19日
	 */
	@PostMapping("/updateCustAccountpwd")
	@ApiOperation(value = "updateCustAccountpwd", notes = "修改用户钱包表")
	@ApiImplicitParams(
		@ApiImplicitParam(dataType = "CustAccountpwd",name = "cust_accountpwd")
	)
	public ReturnJson updateCustAccountpwd(@ApiIgnore CustAccountpwd cust_accountpwd){
		logger.info("request param,CustAccountpwd is "+cust_accountpwd);
		return cust_accountpwdService.updateCustAccountpwd(cust_accountpwd);
	}

}
