
package com.quyang.voice.controller;

import com.quyang.voice.service.IMissuUsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
* <p>说明： 用户表API接口层</P>
* @version: V1.0
* @author: liuhaotian
* @time    2021年02月02日
*
*/
//@Api(value = "missuUsers",tags="missuUsers" )
@RestController
@RequestMapping("/api/missuUsers")
public class MissuUsersController {

	private static final Logger logger = LoggerFactory.getLogger(MissuUsersController.class);

     @Autowired
     IMissuUsersService missuUsersService;

	/**
	 * @explain 查询对象
	 * @author  liuhaotian
	 * @time    2021年02月02日
	 */
//	@GetMapping("/getMissuUsersById/{id}")
//	@ApiOperation(value = "getMissuUsers", notes = "查询用户表")
//	@ApiImplicitParam(paramType="path", name = "id", value = "用户表id", required = true, dataType = "Long")
//	public ResponseUtil<MissuUsers> getMissuUsersById(@PathVariable("id")Long id){
//		logger.info("request param,id is "+id);
//		return missuUsersService.getMissuUsersById(id);
//	}
//
//	/**
//	 * @explain 删除对象
//	 * @author  liuhaotian
//	 * @time    2021年02月02日
//	 */
//	@PostMapping("/deleteMissuUsersById")
//	@ApiOperation(value = "deleteMissuUsers", notes = "删除用户表")
//	@ApiImplicitParam(paramType="query", name = "id", value = "用户表id", required = true, dataType = "Long")
//	public ReturnJson deleteMissuUsersById(Long id){
//		logger.info("request param,id is "+id);
//		return missuUsersService.deleteMissuUsersById(id);
//	}
//
//	/**
//	 * @explain 添加对象
//	 * @author  liuhaotian
//	 * @time    2021年02月02日
//	 */
//	@PostMapping("/addMissuUsers")
//	@ApiOperation(value = "addMissuUsers", notes = "新增用户表")
//	@ApiImplicitParams(
//		@ApiImplicitParam(dataType = "MissuUsers",name = "missuUsers")
//	)
//	public ReturnJson insertMissuUsers(@ApiIgnore MissuUsers missuUsers){
//		logger.info("request param,MissuUsers is "+missuUsers);
//		return missuUsersService.insertMissuUsers(missuUsers);
//	}
//
//	/**
//	 * @explain 修改对象
//	 * @author  liuhaotian
//	 * @time    2021年02月02日
//	 */
//	@PostMapping("/updateMissuUsers")
//	@ApiOperation(value = "updateMissuUsers", notes = "修改用户表")
//	@ApiImplicitParams(
//		@ApiImplicitParam(dataType = "MissuUsers",name = "missuUsers")
//	)
//	public ReturnJson updateMissuUsers(@ApiIgnore MissuUsers missuUsers){
//		logger.info("request param,MissuUsers is "+missuUsers);
//		return missuUsersService.updateMissuUsers(missuUsers);
//	}

}
