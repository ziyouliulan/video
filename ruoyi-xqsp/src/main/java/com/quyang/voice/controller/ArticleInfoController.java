package com.quyang.voice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.quyang.voice.dao.ArticleInfoDao;
import com.quyang.voice.dao.DeptDao;
import com.quyang.voice.dao.DictDataDao;
import com.quyang.voice.mapper.CategoryMapper;
import com.quyang.voice.model.*;
import com.quyang.voice.service.LecturerCategoryService;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Api(value = "文章管理" ,tags = "文章管理")
@RequestMapping("api/article")
public class ArticleInfoController {


    @Autowired
    ArticleInfoDao  articleInfoDao;


    @Autowired
    DeptDao deptDao;


    @Autowired
    DictDataDao dictDataDao;

    @Autowired
    CategoryMapper categoryMapper;



    @Autowired
    LecturerCategoryService lecturerCategoryService;


    @PostMapping("getArticleInfo")
    @ApiOperation("文章分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "category",
                    value = "分类0:推荐,1:头条,2:牛人解盘,3:实战干货,4:基金学院,5:服务",required = true),
            @ApiImplicitParam(dataType = "Integer", name = "id",value = "用户账户",required = true),
            @ApiImplicitParam(dataType = "String", name = "name",value = "用户账户",required =false),
            @ApiImplicitParam(dataType = "Long", name = "pageNum",value = "用户账户",required = true),
            @ApiImplicitParam(dataType = "Integer", name = "pageSize",value = "用户账户",required = true),
    })

    public ResponseUtil getArticleInfo(Integer category,Integer id,String name,Integer pageNum, Integer pageSize){

        ArticleInfoExample  articleInfoExample = new ArticleInfoExample();
        PageHelper.startPage(pageNum, pageSize);
//        articleInfoExample.setOffset(pageNum);
//        articleInfoExample.setLimit(pageSize);
        ArticleInfoExample.Criteria criteria = articleInfoExample.createCriteria();


        if (category !=null){

            if (category ==0 ){
                criteria.andIsTopEqualTo(1);
            }else {
                criteria.andCategoryEqualTo(category);
            }
        }

        if (id !=null){
            criteria.andIdEqualTo(id);
        }
        if (name !=null){
            criteria.andNameLike("%"+name+"%");
        }


        return ResponseUtil.suc(articleInfoDao.selectByExample(articleInfoExample));
    }


    @GetMapping("getDept")
    @ApiOperation("获取科室部门")
    public ResponseUtil getDept(Long id){

        DeptExample deptExample = new DeptExample();
        deptExample.createCriteria().andParentIdEqualTo(id);
        List<Dept> depts = deptDao.selectByExample(deptExample);
        return ResponseUtil.suc(depts);
    }

    @GetMapping("searchDept")
    @ApiOperation("科室搜索")
    public ResponseUtil searchDept(String deptName){

        DeptExample deptExample = new DeptExample();
        DeptExample.Criteria criteria = deptExample.createCriteria();
        criteria.andDeptNameLike("%"+deptName+"%");
        List<Dept> depts = deptDao.selectByExample(deptExample);
        List<Dept>  deptList= new ArrayList<>();
        for (Dept d
                :depts
             ) {

            System.out.println();
            if (d.getAncestors().split(",").length ==3){
                deptList.add(d);
            }

        }

        return ResponseUtil.suc(deptList);
    }



    @GetMapping("getArticleCategory")
    @ApiOperation("获取文章分类")
    public ResponseUtil getArticleCategory(){

        DictDataExample dictDataExample = new DictDataExample();

        dictDataExample.createCriteria().andDictTypeEqualTo("article_category");

        return ResponseUtil.suc(dictDataDao.selectByExample(dictDataExample));
    }

    @PostMapping("/instructorCategory")
    @ApiOperation(value = "讲师分类", notes = "讲师分类")
    public ResponseUtil instructorCategory(){

        return ResponseUtil.suc(lecturerCategoryService.list(null));

    }


    @GetMapping("getCategory")
    @ApiOperation("知识付费分类")
    public ResponseUtil getCategory(Long id){
        QueryWrapper<Category> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id",id);
        return ResponseUtil.suc(categoryMapper.selectList(wrapper));
    }




}
