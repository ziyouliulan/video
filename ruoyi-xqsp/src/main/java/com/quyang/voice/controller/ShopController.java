package com.quyang.voice.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quyang.voice.model.DoctorExample;
import com.quyang.voice.model.LitemallUser;
import com.quyang.voice.model.Shopkeeper;
import com.quyang.voice.model.ShopkeeperCategory;
import com.quyang.voice.service.LitemallUserService;
import com.quyang.voice.service.ShopkeeperCategoryService;
import com.quyang.voice.service.ShopkeeperService;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "商城管理" ,tags = "商城管理")
@RequestMapping("api/shop")
public class ShopController {


    @Autowired
    ShopkeeperService shopkeeperService;




    @Autowired
    ShopkeeperCategoryService shopkeeperCategoryService;

    @PostMapping("addShopkeeper")
    @ApiOperation(value = "店铺认证", notes = "店铺认证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer")
    })
    public ResponseUtil addShopkeeper(Shopkeeper shopkeeper) {


        ShopkeeperCategory byId = shopkeeperCategoryService.getById(shopkeeper.getPositionId());
        if (byId == null){
            return ResponseUtil.suc("分类不存在");
        }


        DoctorExample doctorExample = new DoctorExample();
        doctorExample.createCriteria().andUserIdEqualTo(shopkeeper.getUserId());
        QueryWrapper<Shopkeeper> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",shopkeeper.getUserId());

        Shopkeeper one = shopkeeperService.getOne(wrapper);

        if (!StrUtil.isBlankIfStr(one)){
            switch (one.getStatus()){
                case "1":
                    return ResponseUtil.suc("正在认证中");
                case "2":
                    shopkeeper.setStatus("1");
                    shopkeeper.setInquiryManagement("0");
                    shopkeeper.setId( one.getId());
                    boolean b = shopkeeperService.updateById(shopkeeper);
                    return ResponseUtil.suc();
                case "3":
                    return ResponseUtil.suc("已经认证成功，无需再次认证");
            }
        }

        shopkeeper.setStatus("1");
        shopkeeperService.save(shopkeeper);

        shopkeeper.setPositionName(byId.getName());


        return ResponseUtil.suc(shopkeeper);
    }




    @PostMapping("queryShopkeeperList")
    @ApiOperation(value = "店铺列表", notes = "店铺列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "positionId", value = "分类id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "name", value = "店铺名称", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "isRecommend", value = "是否推荐  1是  0否", required = true, dataType = "Integer"),
    })
    public ResponseUtil queryShopkeeperList(Integer userId, Integer positionId, String name,String isRecommend) {



        return shopkeeperService.getShopkeepers(userId,positionId,name,isRecommend);
    }




    @PostMapping("queryShopkeeper")
    @ApiOperation(value = "店铺详情", notes = "店铺详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer"),
    })
    public ResponseUtil queryShopkeeper(Integer userId) {



        return shopkeeperService.getShopkeeper(userId);
    }


    @Autowired
    LitemallUserService litemallUserService;


    @PostMapping("testDate")
    @ApiOperation(value = "testDate", notes = "测试")
    public ResponseUtil testDate() {


        List<LitemallUser> list = litemallUserService.list(null);
        return ResponseUtil.suc(list);
    }






//
//    @PostMapping("Shopkeeper")
//    @ApiOperation(value = "", notes = "")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer")
//    })
//    public ResponseUtil queryShopkeeper(Shopkeeper shopkeeper) {
//
//        return ResponseUtil.suc();
//    }





    }
