package com.quyang.voice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quyang.voice.dao.MissuUserMsgsCollectDao;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.model.YuecoolGroupRecord;
import com.quyang.voice.service.IMissuUsersService;
import com.quyang.voice.service.YuecoolGroupRecordService;
import com.quyang.voice.service.YuecoolNoticeService;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@Api(value = "消息管理",tags="消息管理" )
@RestController
@RequestMapping("/api/msg")
public class MsgController {


    @Autowired
    MissuUserMsgsCollectDao missuUserMsgsCollectDao;

    @Autowired
    YuecoolNoticeService noticeService;

    @Autowired
    YuecoolGroupRecordService groupRecordService;

    @Autowired
    IMissuUsersService missuUsersService;

    /*
     * @Author:
     * @Date:  2021/7/6 13:44
     * @Description: TODO 查询最后一条已读消息  这是为了漫游消息保证已读未读的正确性  暂时没有
     * @param userId:用户id
     * @param toUserId:需要查询的用户id
     * @return: com.quyang.voice.utils.ResponseUtil
    */
    @PostMapping("/lastMsg")
    @ApiOperation(value = "查询最后一条已读消息", notes = "查询最后一条已读消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer") ,
            @ApiImplicitParam(name = "toUserId", value = "会话框Id", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "osType", value = "类型 1：安卓,2:ios,3 :web", required = true, dataType = "Integer"),
            })
    public ResponseUtil getLastMsg(Integer userId, Integer toUserId ,Integer osType){

        switch (osType){
            case 1:
                HashMap hashMap = new HashMap();
                hashMap.put("userId",userId);
                hashMap.put("toUserId",toUserId);
                hashMap.put("osType",osType);

                return ResponseUtil.suc(missuUserMsgsCollectDao.getFingerprint(hashMap));
            case 2:
                break;
            case 3:
                break;

        }

        return ResponseUtil.suc();

    }


    @PostMapping("/systemNotificationDetails")
    @ApiOperation(value = "查询系统通知详情", notes = "查询最后一条已读消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", required = true, dataType = "Integer") ,
    })
    public ResponseUtil getNotice(Long id){

        return ResponseUtil.suc( noticeService.getNtice(id));
    }


    @PostMapping("/addGroupRecord")
    @ApiOperation(value = "新增群记录", notes = "查询最后一条已读消息")
    public ResponseUtil addGroupRecord(YuecoolGroupRecord groupRecord){

        String[] split = groupRecord.getToUserids().split(",");
        for (String str: split
             ) {
            groupRecord.setToUserid(Integer.valueOf(str));
            groupRecordService.save(groupRecord);
        }

        return ResponseUtil.suc( );
    }


    @PostMapping("/queryGroupRecord")
    @ApiOperation(value = "查询群记录", notes = "查询群记录")
    public ResponseUtil queryGroupRecord(Integer gid,String type,Integer userId){



        QueryWrapper<YuecoolGroupRecord> wrapper = new QueryWrapper();
        wrapper.eq("g_id",gid);
        if (type != null){
            wrapper.eq("type",type);
        }

//        if (userId != null){
            wrapper.eq("to_userId",userId);
            MissuUsers missuUsers = missuUsersService.getById(userId);
//        }
        wrapper.orderByDesc("create_time");
        YuecoolGroupRecord groupRecord = groupRecordService.getOne(wrapper);

        if (groupRecord != null){
            MissuUsers byId = missuUsersService.getById(groupRecord.getUserId());
            if (byId != null){
                groupRecord.setUserName(byId.getNickname());
            }
            groupRecord.setToUserName(missuUsers.getNickname());

        }





        return ResponseUtil.suc( groupRecord);
    }



    @PostMapping("/queryGroupRecord2")
    @ApiOperation(value = "查询群记录", notes = "查询群记录")
    public ResponseUtil queryGroupRecord2(Integer gid){



        QueryWrapper<YuecoolGroupRecord> wrapper = new QueryWrapper();
        wrapper.eq("g_id",gid);
        wrapper.isNotNull("type");
        wrapper.orderByDesc("create_time");
        List<YuecoolGroupRecord> list = groupRecordService.list(wrapper);

        for (YuecoolGroupRecord groupRecord:list
             ) {

                MissuUsers users1 = missuUsersService.getById(groupRecord.getUserId());
                MissuUsers users2 = missuUsersService.getById(groupRecord.getToUserid());
                if (users1 != null){
                    groupRecord.setUserName(users1.getNickname());
                }

                if (users2 != null) {
                    groupRecord.setToUserName(users2.getNickname());
                }

                String str = "";
                switch (groupRecord.getType()){
                    case 1:
                        str = groupRecord.getUserName()+"邀请"+groupRecord.getToUserName()+"加入群聊";
                        break;
                    case 2:
                        str = groupRecord.getUserName()+"同意"+groupRecord.getToUserName()+"加入群聊";
                        break;
                    case 3:
                        str = groupRecord.getToUserName()+"退出了群聊";
                        break;
                    case 4:
                        str = groupRecord.getUserName()+"把"+groupRecord.getToUserName()+"踢出了群聊";
                        break;

                }

            groupRecord.setDescs(str);

            }







        return ResponseUtil.suc( list);
    }




}
