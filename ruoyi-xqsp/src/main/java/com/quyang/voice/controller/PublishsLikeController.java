
package com.quyang.voice.controller;

import com.quyang.voice.model.PublishsLike;
import com.quyang.voice.service.IPublishsLikeService;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 动态点赞API接口层</P>
* @version: V1.0
* @author: liuhaotian
* @time    2020年9月8日
*
*/
@Api(value = "publishsLike", tags = "动态点赞管理")
@RestController
@RequestMapping("/api/publishsLike")
public class PublishsLikeController {

     @Autowired
     IPublishsLikeService publishsLikeService;


	private static final Logger logger = LoggerFactory.getLogger(PublishsLikeController.class);



	/**
	 * @explain 点击点赞
	 * 点击一下喜欢，再点一下取消
	 * @author  liuhaotian
	 * @time    2020年9月9日
	 */
	@PostMapping("/postPublishsLike")
	@ApiOperation(value = "点击动态点赞", notes = "点击动态点赞")
	@ApiImplicitParams({
			@ApiImplicitParam(value = "动态id", name = "publishId", required = true, dataType = "Long"),
			@ApiImplicitParam(value = "被点赞的用户id", name = "likedUserId", required = true, dataType = "Long"),
			@ApiImplicitParam(value = "点赞的用户id", name = "likedPostId", required = true, dataType = "Long"),
			@ApiImplicitParam(value = "点赞状态，0取消，1点赞", name = "status", required = true, dataType = "Integer"),
	})
	public ResponseUtil postPublishsLike(@ApiIgnore PublishsLike publishsLike){
		logger.info("request param,postPublishsLike is "+ publishsLike);
		return publishsLikeService.postPublishsLike(publishsLike);
	}

}
