
package com.quyang.voice.controller;

import com.quyang.voice.model.PrivacyConfig;
import com.quyang.voice.model.vo.PrivateConfigVo;
import com.quyang.voice.service.IPrivacyConfigService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 用户隐私设置API接口层</P>
* @version: V1.0
* @author: liuhaotian
* @time    2021年02月01日
*
*/
@Api(value = "用户隐私设置",tags="用户隐私设置" )
@RestController
@RequestMapping("/api/privacyConfig")
public class PrivacyConfigController {

	private static final Logger logger = LoggerFactory.getLogger(PrivacyConfigController.class);

     @Autowired
     IPrivacyConfigService privacyConfigService;

	/**
	 * @explain 查询对象
	 * @author  liuhaotian
	 * @time    2021年02月01日
	 */
	@GetMapping("/getPrivacyConfigById/{id}")
	@ApiOperation(value = "查询用户隐私设置", notes = "查询用户隐私设置")
	@ApiImplicitParam(paramType="query", name = "userId", value = "用户id", required = true, dataType = "Integer")
	public ResponseUtil<PrivateConfigVo> getPrivacyConfigById(Integer userId){
		logger.info("request param,userId is "+userId);
		return privacyConfigService.getPrivacyConfigById(userId);
	}


	/**
	 * @explain 修改对象
	 * @author  liuhaotian
	 * @time    2021年02月01日
	 */
	@PostMapping("/updatePrivacyConfig")
	@ApiOperation(value = "修改用户隐私设置", notes = "修改用户隐私设置")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userUid", value = "用户id", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "isSerachPhone", value = "是否允许搜索手机号 允许：0 不允许：1", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isSerachUid", value = "是否允许搜索账户  允许：0 不允许：1", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isQr", value = "是否允许通过二维码添加  允许：0 不允许：1", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isGroup", value = "是否允许通过群聊添加  允许：0 不允许：1", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isCard", value = "是否允许通过名片添加  允许：0 不允许：1", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isTemporary", value = "是否允许通过名片添加  允许：0 不允许：1", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isPublishLimt", value = "动态可见时间 0：最近半年  1：最近一个月  2：最近三天 3：全部", required = false, dataType = "Integer"),
			@ApiImplicitParam(name = "isNotSeeMe", value = "不让他看我动态  用户id", required = false, dataType = "String"),
			@ApiImplicitParam(name = "isNotSeeHe", value = "不看他动态 用户id", required = false, dataType = "String"),
			@ApiImplicitParam(name = "black", value = "黑名单 用户id", required = false, dataType = "String"),
	})
	public ReturnJson updatePrivacyConfig(@ApiIgnore PrivacyConfig privacyConfig){
		logger.info("request param,PrivacyConfig is "+privacyConfig);
		return privacyConfigService.updatePrivacyConfig(privacyConfig);
	}

}
