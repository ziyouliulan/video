//
//package com.quyang.voice.controller;
//
//import com.github.pagehelper.PageInfo;
//import com.quyang.voice.dao.ToolLocalStorageMusicDao;
//import com.quyang.voice.model.Publishs;
//import com.quyang.voice.model.PublishsLike;
//import com.quyang.voice.model.ToolLocalStorageMusicExample;
//import com.quyang.voice.model.dto.ShortVideoDto;
//import com.quyang.voice.model.vo.PublishCommentsVo;
//import com.quyang.voice.model.vo.PublishInfoVo;
//import com.quyang.voice.model.vo.PublishsListVo;
//import com.quyang.voice.model.vo.PublishsVo;
//import com.quyang.voice.service.IPublishCommentsReplyService;
//import com.quyang.voice.service.ShortVideoService;
//import com.quyang.voice.utils.ResponseUtil;
//import com.quyang.voice.utils.ReturnJson;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import io.swagger.annotations.ApiOperation;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import springfox.documentation.annotations.ApiIgnore;
//
//import java.util.Map;
//
//
//@Api(value = "短视频",tags="短视频" )
//@RestController
//@RequestMapping("/api/ShortVideo")
//public class ShortVideoController {
//
//	private static final Logger logger = LoggerFactory.getLogger(ShortVideoController.class);
//
//     @Autowired
//	 ShortVideoService shortVideoService;
//
//
//     @Autowired
//     ToolLocalStorageMusicDao toolLocalStorageMusicDao;
//
//     @Autowired
//	IPublishCommentsReplyService publishCommentsReplyService;
//
//	/**
//	 * @explain 查询对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@GetMapping("/getPublishsById")
//	@ApiOperation(value = "查询短视频详情", notes = "查询短视频详情")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer")
//	})
//	public ResponseUtil<PublishInfoVo> getPublishsById(Long id,Integer checkUserId){
//		logger.info("request param,id is "+id+"param,checkUserId is "+checkUserId);
//		return shortVideoService.getPublishsById(id,checkUserId);
//	}
//
//
//	/**
//	 * @explain 查询对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@GetMapping("/getMusicList")
//	@ApiOperation(value = "查询音乐", notes = "查询音乐")
//	public ResponseUtil getMusicList(Long pageNum, Integer pageSize){
//		ToolLocalStorageMusicExample toolLocalStorageMusicExample = new ToolLocalStorageMusicExample();
//
//		toolLocalStorageMusicExample.setOffset(pageNum);
//		toolLocalStorageMusicExample.setLimit(pageSize);
//
//		return ResponseUtil.suc(toolLocalStorageMusicDao.selectByExample(toolLocalStorageMusicExample));
//	}
//
//
//
//	/**
//	 * @explain 查询对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@GetMapping("/getPublishsList")
//	@ApiOperation(value = "分页查询短视频列表", notes = "分页查询短视频列表")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "checkUserId", value = "查看的用户id", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "regionCode", value = "地区编码", required = false, dataType = "String")
//	})
//	public ResponseUtil<PageInfo<PublishsVo>> getPublishsList(Integer checkUserId,Integer pageNum, Integer pageSize, String regionCode){
//		logger.info("request param,checkUserId is "+checkUserId+",pageNum is "+pageNum+"param,pageSize is "+pageSize);
//		return shortVideoService.getPublishsList(checkUserId,pageNum,pageSize,regionCode);
//	}
//
//
//
//	/**
//	 * @explain 查询对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@GetMapping("/getPublishsLists")
//	@ApiOperation(value = "分页查询短视频列表22", notes = "分页查询短视频列表22")
//	public ResponseUtil<PageInfo<PublishsVo>> getPublishsList(ShortVideoDto shortVideoDto){
//		return shortVideoService.getPublishsList(shortVideoDto);
//	}
//
//	/**
//	 * @explain 删除对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@PostMapping("/deletePublishsById")
//	@ApiOperation(value = "删除短视频", notes = "删除短视频")
//	@ApiImplicitParam(name = "ids", value = "短视频ids，删除多个用逗号隔开", required = true, dataType = "String")
//	public ReturnJson deletePublishsById(String ids){
//		logger.info("request param,ids is "+ids);
//
//		return shortVideoService.deletePublishsById(ids);
//	}
//
//
//	/**
//	 * @explain 添加对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@PostMapping("/addPublishs")
//	@ApiOperation(value = "新增短视频", notes = "新增短视频")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "userId", value = "发布的用户id", required = true, dataType = "Long"),
//			@ApiImplicitParam(name = "content", value = "动态内容", required = true, dataType = "String"),
//			@ApiImplicitParam(name = "type", value = "1 文字+图片、2 文字+视频 3：纯文字", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "picUrl", value = "图片url，多张已逗号分隔,图片和视频只能传一种", required = false, dataType = "String"),
//			@ApiImplicitParam(name = "videoUrl", value = "视频url,图片和视频只能传一种", required = false, dataType = "String"),
//			@ApiImplicitParam(name = "videoCoverUrl", value = "视频封面url,只要有视频url，这个字段必须有", required = false, dataType = "String"),
//			@ApiImplicitParam(name = "regionCode", value = "地区编码", required = false, dataType = "String"),
//			@ApiImplicitParam(name = "regionName", value = "地区名称", required = false, dataType = "String"),
//			@ApiImplicitParam(name = "goodsId", value = "商品id", required = false, dataType = "String"),
//	})
//	public ReturnJson insertPublishs(@ApiIgnore Publishs publishs){
//		logger.info("request param,Publishs is "+publishs);
//		return shortVideoService.insertPublishs(publishs);
//	}
//
//	/**
//	 * @explain 查询对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@GetMapping("/getMyPublishList")
//	@ApiOperation(value = "分页查询短视频列表(我的作品列表)", notes = "分页查询短视频列表(我的作品列表)")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Long")
//	})
//	public ResponseUtil<PageInfo<PublishsListVo>> getMyPublishList(Integer pageNum, Integer pageSize, Long userId){
//		logger.info("request param,userId is "+userId);
//		return shortVideoService.getMyPublishList(pageNum,pageSize,userId);
//
//	}
//
//
//	/**
//	 * @explain 查询对象
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@GetMapping("/getOtherPublishList")
//	@ApiOperation(value = "分页查询短视频列表(他人作品列表)", notes = "分页查询短视频列表(他人作品列表)")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Long")
//	})
//	public ResponseUtil<PageInfo<PublishsListVo>> getOtherPublishList(Integer pageNum, Integer pageSize, Long userId){
//		logger.info("request param,userId is "+userId);
//		return shortVideoService.getOtherPublishList(pageNum,pageSize,userId);
//
//	}
//
//	/**
//	 * @explain 分页条件查询
//	 * @author  liuhaotian
//	 * @time    2020年10月23日
//	 */
//	@GetMapping("/getPublishsByUser")
//	@ApiOperation(value = "根据userId获取该用户发布的短视频", notes = "分页查询返回")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "toUserId", value = "查看用户ID", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer")
//	})
//	public ResponseUtil<PageInfo<PublishsVo>> getPublishsByUser(Integer pageNum, Integer pageSize, Integer userId,Integer toUserId){
//		logger.info("request param,pageNum is "+pageNum+",pageSize is "+pageSize+",userId is"+userId);
//		return shortVideoService.getPublishsByUser(pageNum ,pageSize,userId,toUserId);
//	}
//
//	/**
//	 * @explain 点击点赞
//	 * 点击一下喜欢，再点一下取消
//	 * @author  wlx
//	 * @time    2020年12月01日
//	 */
//	@PostMapping("/postPublishsLike")
//	@ApiOperation(value = "短视频点赞/取消点赞(收藏)", notes = "短视频点赞/取消点赞(收藏)")
//	@ApiImplicitParams({
//			@ApiImplicitParam(value = "短视频id", name = "publishId", required = true, dataType = "Long"),
//			@ApiImplicitParam(value = "点赞的用户id", name = "likedPostId", required = true, dataType = "Long"),
//			@ApiImplicitParam(value = "点赞状态，0取消，1点赞", name = "status", required = true, dataType = "Integer"),
//	})
//	public ReturnJson postPublishsLike(@ApiIgnore PublishsLike publishsLike){
//		logger.info("request param,postPublishsLike is "+ publishsLike);
//		return shortVideoService.postPublishsLike(publishsLike);
//	}
//
//
////	/**
////	 * @explain 添加对象
////	 * @author  wulixiang
////	 * @time    2020年12月01日
////	 */
////	@PostMapping("/addPublishComments")
////	@ApiOperation(value = "新增动态评论", notes = "新增动态评论")
////	@ApiImplicitParams({
////			@ApiImplicitParam(name = "publishId", value = "动态id", required = true, dataType = "Long"),
////			@ApiImplicitParam(name = "userId", value = "评论用户id", required = true,dataType = "Long"),
////			@ApiImplicitParam(name = "isAuthor", value = "是否是作者：0不是 1是", required = true,dataType = "Integer"),
////			@ApiImplicitParam(name = "comment", value = "评论内容", required = true, dataType = "String"),
////			@ApiImplicitParam(name = "commentPic", value = "评论图片url", dataType = "String")
////	})
////	public ReturnJson insertPublishComments(@ApiIgnore PublishComments publishComments){
////		logger.info("request param,PublishComments is "+publishComments);
////		return shortVideoService.insertPublishComments(publishComments);
////	}
////
////
////
////	/**
////	 * @explain 删除对象
////	 * @author  wulixiang
////	 * @time    2020年9月10日
////	 */
////	@PostMapping("/deletePublishCommentsById")
////	@ApiOperation(value = "删除动态评论", notes = "删除动态评论")
////	@ApiImplicitParam(paramType="query", name = "id", value = "动态评论id", required = true, dataType = "Long")
////	public ReturnJson deletePublishCommentsById(Long id){
////		logger.info("request param,id is "+id);
////		return shortVideoService.deletePublishCommentsById(id);
////	}
//
//
//	/**
//	 * @explain 分页查询某个动态的评论
//	 * @author  wlx
//	 * @time    2020年12月02日
//	 */
//	@GetMapping("/queryPublishComments")
//	@ApiOperation(value = "分页查询某个短视频的评论", notes = "分页查询某个短视频的评论")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "Integer"),
//			@ApiImplicitParam(name = "id", value = "短视频id", required = true, dataType = "Long")
//	})
//	public ResponseUtil<PageInfo<PublishCommentsVo>> queryPublishComments(Integer pageNum, Integer pageSize,Long id){
//		logger.info("request param,id is "+id);
//		return shortVideoService.queryPublishComments(pageNum,pageSize,id);
//	}
//
//	@GetMapping("/queryPublishDetails")
//	@ApiOperation(value = "查询短视频作者主页", notes = "查询短视频作者主页")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer")
//	})
//	public ResponseUtil<Map> queryPublishDetails(Integer userId){
//
//		return shortVideoService.queryPublishDetails(userId);
//	}
//
//
//}
