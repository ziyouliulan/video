
package com.quyang.voice.controller;

import com.quyang.voice.model.PublishCommentsReply;
import com.quyang.voice.service.IPublishCommentsReplyService;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 动态评论回复表API接口层</P>
* @version: V1.0
* @author: liuhaotian
* @time    2020年9月10日
*
*/
@Api(value = "publishCommentsReply", tags = "动态评论回复管理")
@RestController
@RequestMapping("/api/publishCommentsReply")
public class PublishCommentsReplyController {

	private static final Logger logger = LoggerFactory.getLogger(PublishCommentsReplyController.class);

     @Autowired
     IPublishCommentsReplyService publishCommentsReplyService;

	
	/**
	 * @explain 删除对象
	 * @author  liuhaotian
	 * @time    2020年9月10日
	 */
	@PostMapping("/deletePublishCommentsReplyById")
	@ApiOperation(value = "删除动态评论回复", notes = "删除动态评论回复")
	@ApiImplicitParam(paramType = "query", name = "id", value = "动态评论回复表id", required = true, dataType = "Long")
	public ReturnJson deletePublishCommentsReplyById(Long id){
		logger.info("request param,id is "+id);
		return publishCommentsReplyService.deletePublishCommentsReplyById(id);
	}
	
	/**
	 * @explain 添加对象
	 * @author  liuhaotian
	 * @time    2020年9月10日
	 */
	@PostMapping("/addPublishCommentsReply")
	@ApiOperation(value = "新增动态评论回复", notes = "新增动态评论回复")
	@ApiImplicitParams({
			@ApiImplicitParam(value = "评论ID", name = "commentsId", required = true, dataType = "Long"),
			@ApiImplicitParam(value = "用户ID", required = true, name = "uid", dataType = "Long"),
			@ApiImplicitParam(value = "目标用户ID 给谁回复", required = true, name = "touid", dataType = "Long"),
			@ApiImplicitParam(value = "用户名", required = true, name = "uname", dataType = "String"),
			@ApiImplicitParam(value = "目标用户名", required = true, name = "touname", dataType = "String"),
			@ApiImplicitParam(value = "回复内容", required = true, name = "replyComment", dataType = "String"),
			@ApiImplicitParam(value = "回复图片", name = "replyCommentPic", dataType = "String")
	})
	public ReturnJson insertPublishCommentsReply(@ApiIgnore PublishCommentsReply publishCommentsReply){
		logger.info("request param,PublishCommentsReply is "+publishCommentsReply);
		return publishCommentsReplyService.insertPublishCommentsReply(publishCommentsReply);
	}

}
