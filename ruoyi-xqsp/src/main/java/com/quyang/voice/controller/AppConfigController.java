
package com.quyang.voice.controller;

import cn.hutool.core.util.RandomUtil;
import com.google.gson.Gson;
import com.quyang.voice.dao.EsysSnDao;
import com.quyang.voice.dao.MissuRosterDao;
import com.quyang.voice.model.AppConfig;
import com.quyang.voice.model.EsysSn;
import com.quyang.voice.model.MissuRoster;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.service.ClientConfigService;
import com.quyang.voice.service.IAppConfigService;
import com.quyang.voice.service.IMissuUsersService;
import com.quyang.voice.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;


/**
* <p>说明： app配置API接口层</P>
* @version: V1.0
* @author: wlx
* @time    2021年02月03日
*
*/
@Api(value = "app配置",tags="app配置" )
@RestController
@RequestMapping("/api/appConfig")
public class AppConfigController {
     @Autowired
     IAppConfigService appConfigService;

	@Autowired
	IMissuUsersService missuUsersService;


     @Autowired
	 EsysSnDao esysSnDao;

	 @Autowired
	 ClientConfigService configService;

     @Autowired
     MissuRosterDao missuRosterDao;


	/**
	 * @explain 查询对象
	 * @author  wlx
	 * @time    2021年02月03日
	 */
	@GetMapping("/getAppConfig")
	@ApiOperation(value = "查询app配置", notes = "查询app配置")
	public ResponseUtil<AppConfig> getAppConfigById(){
		return appConfigService.getAppConfigById(1l);
	}




	@PostMapping("/createUser")
	@ApiOperation(value = "创建用户", notes = "创建用户")
	@CrossOrigin
	public ResponseUtil createUser(){



		//生成用户
		MissuUsers missuUsers = new MissuUsers();

		missuUsers.setUserStatus(1);
		missuUsers.setUserLevelId(1);

		EsysSn user_cornet = esysSnDao.selectByPrimaryKey("user_cornet");

		missuUsers.setUserCornet(user_cornet.getCurrentVal().intValue());
		user_cornet.setCurrentVal(user_cornet.getCurrentVal()+1);
		esysSnDao.updateByPrimaryKeySelective(user_cornet);
		missuUsers.setUserMail(missuUsers.getUserCornet()+"");
		Date date = new Date();
		missuUsers.setRegisterTime(date);
//		String format = DateUtil.format(date, "yyyyMMddHHmm");
		missuUsers.setUserPsw(encyptPassword("123456",""));
//            missuUsers.setUserAvatarFileName(list.get(i).getAvatarUrl());
		missuUsers.setNickname("游客"+RandomUtil.randomInt(10,99));
		missuUsers.setMaxFriend(200);
		EsysSn invitation_code = esysSnDao.selectByPrimaryKey("invitation_code");
		missuUsers.setMyCommunicationNumber(invitation_code.getCurrentVal().intValue());
		invitation_code.setCurrentVal(invitation_code.getCurrentVal() +1);
		esysSnDao.updateByPrimaryKeySelective(invitation_code);
		missuUsersService.insertMissuUsers(missuUsers);

		Integer userId = missuUsers.getUserUid();
		MissuRoster missuRoster = new MissuRoster();

		missuRoster.setUserUid(400071);
		missuRoster.setFriendUserUid(userId);
		missuRoster.setAddTime(new Date());
		missuRosterDao.insert(missuRoster);


		missuRoster.setFriendUserUid(400071);
		missuRoster.setUserUid(userId);
		missuRoster.setAddTime(new Date());
		missuRosterDao.insert(missuRoster);




		return ResponseUtil.suc(missuUsers);

	}



	@PostMapping("/sync")
	@ApiOperation(value = "同步商城数据", notes = "同步商城数据")
	@CrossOrigin
	public ResponseUtil sync(){


		return ResponseUtil.suc();
	}



	@GetMapping("config")
	@ApiOperation("oss多文件上传")
	@CrossOrigin
	public ResponseEntity config( ){

//        List<ToolLocalStorage> toolLocalStorages = storageService.aliyunUploads(null, file);
		String[] imgs = {".png", ".jpg", ".jpeg", ".gif", ".bmp"};
		Gson gson = new Gson();
		HashMap map = new HashMap();
		map.put("imageUrl","http://localhost:8092/api/storage/upload");
		map.put("imagePath","");
		map.put("imageFieldName","");
		map.put("imageMaxSize",2048);
		map.put("imageAllowFiles",imgs);

		return new ResponseEntity(gson.toJson(map), HttpStatus.OK);

	}





	public static void main(String[] args) {
//		int i = ;
//		System.out.println(i);
	}


	public static  String encyptPassword(String originalPsw, String DEFAULT_PASSWORD) {
		if (originalPsw == null) {
			return null;
		} else {
			if (!DEFAULT_PASSWORD.equals(originalPsw)) {
				try {
					return byteToHexStringForMD5(MessageDigest.getInstance("MD5").digest(originalPsw.getBytes()));
				} catch (Exception var3) {
					var3.printStackTrace();
				}
			}

			return originalPsw;
		}
	}


	public static String byteToHexStringForMD5(byte[] MD5Bytes) {
		char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
		char[] str = new char[32];
		int k = 0;

		for(int i = 0; i < 16; ++i) {
			byte byte0 = MD5Bytes[i];
			str[k++] = hexDigits[byte0 >>> 4 & 15];
			str[k++] = hexDigits[byte0 & 15];
		}

		String s = new String(str);
		return s;
	}



}
