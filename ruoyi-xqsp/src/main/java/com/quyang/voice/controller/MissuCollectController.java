
package com.quyang.voice.controller;

import com.quyang.voice.model.MissuCollect;
import com.quyang.voice.model.vo.MissuCollectVo;
import com.quyang.voice.service.IMissuCollectService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


/**
* <p>说明： 用户消息收藏API接口层</P>
* @version: V1.0
* @author: sky
* @time    2021年03月02日
*
*/
@Api(value = "用户消息收藏",tags="用户消息收藏" )
@RestController
@RequestMapping("/api/missuCollect")
public class MissuCollectController {

	private static final Logger logger = LoggerFactory.getLogger(MissuCollectController.class);

     @Autowired
     IMissuCollectService missuCollectService;

	/**
	 * @explain 查询对象
	 * @author  sky
	 * @time    2021年03月02日
	 */
	@GetMapping("/getMissuCollectById/{id}")
	@ApiOperation(value = "查询用户消息收藏详情", notes = "查询用户消息收藏详情")
	@ApiImplicitParam(paramType="path", name = "id", value = "用户消息收藏id", required = true, dataType = "Long")
	public ResponseUtil<MissuCollectVo> getMissuCollectById(@PathVariable("id")Long id){
		logger.info("request param,id is "+id);
		return missuCollectService.getMissuCollectById(id);
	}

	/**
	 * @explain 删除对象
	 * @author  sky
	 * @time    2021年03月02日
	 */
	@PostMapping("/deleteMissuCollectById")
	@ApiOperation(value = "删除用户消息收藏", notes = "删除用户消息收藏")
	@ApiImplicitParam(paramType="query", name = "id", value = "用户消息收藏id", required = true, dataType = "Long")
	public ReturnJson deleteMissuCollectById(Long id){
		logger.info("request param,id is "+id);
		return missuCollectService.deleteMissuCollectById(id);
	}

	/**
	 * @explain 添加对象
	 * @author  sky
	 * @time    2021年03月02日
	 */
	@PostMapping("/addMissuCollect")
	@ApiOperation(value = "新增用户消息收藏", notes = "新增用户消息收藏")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "发出收藏申请用户的id", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "content", value = "收藏内容", required = true, dataType = "String"),
			@ApiImplicitParam(name = "type", value = "收藏类型 0:纯文字   1:图片   2:视频  3:音频", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "fromUserId", value = "消息发送用户id", required = true, dataType = "Integer"),
	}
	)
	public ReturnJson insertMissuCollect(@ApiIgnore MissuCollect missuCollect){
		logger.info("request param,MissuCollect is "+missuCollect);
		return missuCollectService.insertMissuCollect(missuCollect);
	}

	/**
	 * @explain 查询对象
	 * @author  sky
	 * @time    2021年03月02日
	 */
	@GetMapping("/getMissuCollectList")
	@ApiOperation(value = "查询用户消息收藏列表", notes = "查询用户消息收藏列表")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "userId", value = "发出收藏申请用户的id", required = true, dataType = "Integer")
	}
	)
	public ResponseUtil<MissuCollectVo> getMissList(Integer userId){
		logger.info("request param,userId is "+userId);
		return missuCollectService.getMissuColletList(userId);
	}




	/**
	 * @explain 修改对象
	 * @author  sky
	 * @time    2021年03月02日
	 */
	@PostMapping("/updateMissuCollect")
	@ApiOperation(value = "修改用户消息收藏", notes = "修改用户消息收藏")
	@ApiImplicitParams(
		@ApiImplicitParam(dataType = "MissuCollect",name = "missuCollect")
	)
	public ReturnJson updateMissuCollect(@ApiIgnore MissuCollect missuCollect){
		logger.info("request param,MissuCollect is "+missuCollect);
		return missuCollectService.updateMissuCollect(missuCollect);
	}

}
