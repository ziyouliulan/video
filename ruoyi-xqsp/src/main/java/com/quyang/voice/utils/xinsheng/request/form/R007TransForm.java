package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class R007TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"cardNo","holderName","cardAvailableDate","cvv2","mobileNo","identityType","identityCode","userId","merUserIp"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode",  "errorCode","ncountOrderId"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String cardNo;//支付银行卡卡号
    private String holderName;//持卡人姓名
    private String cardAvailableDate;//信用卡有效期
    private String cvv2;//信用卡CVV2
    private String mobileNo;//银行签约手机号
    private String identityType;//证件类型
    private String identityCode;//证件号码
    private String userId;//用户ID
    private String merUserIp;//商户用户IP

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
