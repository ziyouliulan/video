package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T008TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"ncountOrderId","smsCode","merUserIp","paymentTerminalInfo","receiverTerminalInfo","deviceInfo","businessType","feeType","divideAcctDtl","feeAmountUser"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode" ,"errorCode", "ncountOrderId","tranAmount","checkDate","submitTime","tranFinishTime","bankCode","cardType","shortCardNo","bindCardAgrNo"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String ncountOrderId;//新账通订单号
    private String smsCode;//短信验证码
    private String merUserIp;//商户用户IP
    private String paymentTerminalInfo;//付款方终端信息
    private String receiverTerminalInfo;//收款方终端信息
    private String deviceInfo;//设备信息
    private String businessType;//业务类型
    private String feeType;//手续费内扣外扣
    private String divideAcctDtl;//分账明细
    private String feeAmountUser;//手续费承担方id

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
