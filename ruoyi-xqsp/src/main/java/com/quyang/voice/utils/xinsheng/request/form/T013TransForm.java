package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T013TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"tranAmount","currencyType","payType","receiveUserId","notifyFrontUrl","notifyServerUrl","riskExpand","goodsInfo","orderSubject","orderDesc","merchantId","merUserIp","payLimit"};
    public static final String[] verifyArr = new String[]{};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String tranAmount;//交易金额
    private String currencyType;//币种
    private String payType;//付款方支付方式
    private String receiveUserId;//收款方 ID
    private String notifyFrontUrl;//商户回调地址
    private String notifyServerUrl;//商户通知地址
    private String riskExpand;//分控扩展信息
    private String goodsInfo;//商品信息
    private String orderSubject;//订单标题
    private String orderDesc;//订单描述
    private String merchantId;//客户机构号
    private String merUserIp;//商户用户IP
    private String payLimit;//限制信用卡支付

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
