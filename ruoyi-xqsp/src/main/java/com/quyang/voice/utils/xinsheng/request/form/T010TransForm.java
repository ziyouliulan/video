package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T010TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"merUserIp","goodsInfo","tranAmount","payType","bankCode","currencyType","directFlag","notifyFrontUrl","notifyServerUrl","receiveUserId","paymentTerminalInfo","receiverTerminalInfo","deviceInfo","businessType","feeType","divideAcctDtl","feeAmountUser"};
    public static final String[] verifyArr = new String[]{};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String merUserIp;//用户浏览器IP
    private String goodsInfo;//商品信息
    private String tranAmount;//交易金额
    private String payType;//付款方支付方式
    private String bankCode;//网银机构代码
    private String currencyType;//币种
    private String directFlag;//是否直连
    private String notifyFrontUrl;//商户回调地址
    private String notifyServerUrl;//商户通知地址
    private String receiveUserId;//收款方 ID
    private String paymentTerminalInfo;//付款方终端信息
    private String receiverTerminalInfo;//收款方终端信息
    private String deviceInfo;//设备信息
    private String businessType;//业务类型
    private String feeType;//手续费内扣外扣
    private String divideAcctDtl;//分账明细
    private String feeAmountUser;//手续费承担方id

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
