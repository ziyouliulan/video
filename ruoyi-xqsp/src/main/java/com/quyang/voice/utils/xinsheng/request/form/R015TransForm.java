package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class R015TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"cardNo","userId","mobileNo","frontUrl","notifyUrl","merUserIp","pageStyle"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "merAttach", "charset", "signType", "resultCode" ,"errorCode", "errorMsg", "bindCardAgrNo", "bankCode",
		"cardType", "shortCardNo"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String cardNo;//支付银行卡卡号
    private String userId;//用户编号
    private String mobileNo;//银行签约手机号
    private String frontUrl;//商户回调地址
    private String notifyUrl;//异步通知地址
    private String merUserIp;//商户用户IP
    private String pageStyle;//页面样式

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
