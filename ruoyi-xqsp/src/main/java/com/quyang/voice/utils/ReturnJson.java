package com.quyang.voice.utils;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel("ReturnJson")
public class ReturnJson {

    @ApiModelProperty("状态码")
    private Integer status;

    @ApiModelProperty("描述")
    private String msg;

    public ReturnJson(Integer code, String msg) {
        this.status = code;
        this.msg = msg;
    }

    public static ReturnJson fail(String msg) {
        ReturnJson json = new ReturnJson(-1, msg);
        return json;
    }

    public static ReturnJson fail(String msg,int code) {
        ReturnJson json = new ReturnJson(code, msg);
        return json;
    }

    public static ReturnJson failCustom(int code, String msg) {
        ReturnJson json = new ReturnJson(code, msg);
        return json;
    }
    public static ReturnJson sucCustom(int code , String msg) {
        ReturnJson json = new ReturnJson(code, msg);
        return json;
    }

    public static ReturnJson suc(String msg) {
        ReturnJson json = new ReturnJson(200, msg);
        return json;
    }

    public static ReturnJson suc(ResultCode code) {
        ReturnJson json = new ReturnJson(code.code(), code.message());
        return json;
    }

    public static ReturnJson fail(ResultCode code) {
        ReturnJson json = new ReturnJson(code.code(), code.message());
        return json;
    }


}

