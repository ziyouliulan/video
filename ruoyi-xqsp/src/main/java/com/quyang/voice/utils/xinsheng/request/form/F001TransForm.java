package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class F001TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"orgMerOrderId","divideId","divideDtlId"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode" ,"errorCode", "ncountOrderId"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "charset"};

    private String orgMerOrderId;//原商户订单号
    private String divideId;//分账主订单号
    private String divideDtlId;//分账明细订单号

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
