package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class R004TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"userId","certFileType"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode", "errorCode", "userId","certFileType"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String userId;//用户编号
    private String certFileType;//证件资料类型

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
