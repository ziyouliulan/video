package com.quyang.voice.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalSerializer extends JsonSerializer<BigDecimal> {

    /**
     * TODO 使用方法      @JsonSerialize(using = CustomerDoubleSerialize.class)
     * @Author:lipeng
     * @Date:2022/1/14 14:15
     * @param: aDouble
     * @param: jsonGenerator
     * @param: serializerProvider
     * @return:void
     */
    @Override
    public void serialize(BigDecimal bigDecimal, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        if (bigDecimal !=null){
            BigDecimal bigDecimal1 = bigDecimal.setScale(2);

            jsonGenerator.writeNumber(bigDecimal1);
        }else {
            jsonGenerator.writeNumber(bigDecimal);
        }

    }
}
