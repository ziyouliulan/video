package com.quyang.voice.utils.xinsheng.request.init;

import com.quyang.voice.utils.xinsheng.request.form.*;

import java.util.Map;


public class InitForm {

    // 接口请求提交地址
    public static final String R012_SUBMIT_URL = "https://ncount.hnapay.com/api/r012.htm";
    public static final String R013_SUBMIT_URL = "https://ncount.hnapay.com/api/r013.htm";
    public static final String R014_SUBMIT_URL = "https://ncount.hnapay.com/api/r014.htm";
    public static final String Q006_SUBMIT_URL = "https://ncount.hnapay.com/api/q006.htm";
    public static final String Q007_SUBMIT_URL = "https://ncount.hnapay.com/api/q007.htm";
    public static final String Q008_SUBMIT_URL = "https://ncount.hnapay.com/api/q008.htm";
    public static final String Q009_SUBMIT_URL = "https://ncount.hnapay.com/api/q009.htm";
    public static final String Q001_SUBMIT_URL = "https://ncount.hnapay.com/api/q001.htm";
    public static final String Q002_SUBMIT_URL = "https://ncount.hnapay.com/api/q002.htm";
    public static final String Q004_SUBMIT_URL = "https://ncount.hnapay.com/api/q004.htm";
    public static final String R001_SUBMIT_URL = "https://ncount.hnapay.com/api/r001.htm";
    public static final String R002_SUBMIT_URL = "https://ncount.hnapay.com/api/r002.htm";
    public static final String R003_SUBMIT_URL = "https://ncount.hnapay.com/api/r003.htm";
    public static final String R004_SUBMIT_URL = "https://ncount.hnapay.com/api/r004.htm";
    public static final String R007_SUBMIT_URL = "https://ncount.hnapay.com/api/r007.htm";
    public static final String R008_SUBMIT_URL = "https://ncount.hnapay.com/api/r008.htm";
    public static final String R009_SUBMIT_URL = "https://ncount.hnapay.com/api/r009.htm";//个人用户解绑接口
    public static final String R010_SUBMIT_URL = "https://ncount.hnapay.com/api/r010.htm";
    public static final String R011_SUBMIT_URL = "https://ncount.hnapay.com/api/r011.htm";
    public static final String T002_SUBMIT_URL = "https://ncount.hnapay.com/api/t002.htm";
    public static final String T003_SUBMIT_URL = "https://ncount.hnapay.com/api/t003.htm";
    public static final String T004_SUBMIT_URL = "https://ncount.hnapay.com/api/t004.htm";
    public static final String T005_SUBMIT_URL = "https://ncount.hnapay.com/api/t005.htm";
    public static final String T006_SUBMIT_URL = "https://ncount.hnapay.com/api/t006.htm";
    public static final String T007_SUBMIT_URL = "https://ncount.hnapay.com/api/t007.htm";//快捷支付下单接口
    public static final String T008_SUBMIT_URL = "https://ncount.hnapay.com/api/t008.htm";//快捷支付确认接口
    public static final String R015_SUBMIT_URL = "https://ncount.hnapay.com/api/r015.htm";
    public static final String T009_SUBMIT_URL = "https://ncount.hnapay.com/api/t009.htm";
    public static final String T010_SUBMIT_URL = "https://ncount.hnapay.com/api/t010.htm";
    public static final String T011_SUBMIT_URL = "https://ncount.hnapay.com/api/t011.htm";
    public static final String T012_SUBMIT_URL = "https://ncount.hnapay.com/api/t012.htm";
    public static final String T013_SUBMIT_URL = "https://ncount.hnapay.com/api/t013.htm";
    public static final String F001_SUBMIT_URL = "https://ncount.hnapay.com/api/f001.htm";


    public BaseTransForm getFormByTranCode(String tranCode) {
        BaseTransForm baseTransForm = null;
        switch (tranCode) {
            case "R012":
                baseTransForm = getR012TransForm();
                break;
            case "R013":
                baseTransForm = getR013TransForm();
                break;
            case "R014":
                baseTransForm = getR014TransForm();
                break;
            case "Q006":
                baseTransForm = getQ006TransForm();
                break;
            case "Q007":
                baseTransForm = getQ007TransForm();
                break;
            case "Q008":
                baseTransForm = getQ008TransForm();
                break;
            case "Q009":
                baseTransForm = getQ009TransForm();
                break;
            case "Q001":
                baseTransForm = getQ001TransForm();
                break;
            case "Q002":
                baseTransForm = getQ002TransForm();
                break;
            case "Q004":
                baseTransForm = getQ004TransForm();
                break;
            case "R001":
                baseTransForm = getR001TransForm();
                break;
            case "R002":
                baseTransForm = getR002TransForm();
                break;
            case "R003":
                baseTransForm = getR003TransForm();
                break;
            case "R004":
                baseTransForm = getR004TransForm();
                break;
            case "R007":
                baseTransForm = getR007TransForm();
                break;
            case "R008":
                baseTransForm = getR008TransForm();
                break;
            case "R009":
                baseTransForm = getR009TransForm();
                break;
            case "R010":
                baseTransForm = getR010TransForm();
                break;
            case "R011":
                baseTransForm = getR011TransForm();
                break;
            case "T002":
                baseTransForm = getT002TransForm();
                break;
            case "T003":
                baseTransForm = getT003TransForm();
                break;
            case "T004":
                baseTransForm = getT004TransForm();
                break;
            case "T005":
                baseTransForm = getT005TransForm();
                break;
            case "T006":
                baseTransForm = getT006TransForm();
                break;
            case "T007":
                baseTransForm = getT007TransForm();
                break;
            case "T008":
                baseTransForm = getT008TransForm();
                break;
            case "R015":
                baseTransForm = getR015TransForm();
                break;
            case "T009":
                baseTransForm = getT009TransForm();
                break;
            case "T010":
                baseTransForm = getT010TransForm();
                break;
            case "T011":
                baseTransForm = getT011TransForm();
                break;
            case "T012":
                baseTransForm = getT012TransForm();
                break;
            case "T013":
                baseTransForm = getT013TransForm();
                break;
            case "F001":
                baseTransForm = getF001TransForm();
                break;
            default:
                break;
        }
        return baseTransForm;
    }

    private R012TransForm getR012TransForm() {

        R012TransForm transForm = new R012TransForm();
        transForm.setTranCode("R012");
        transForm.initCommonParams("R012");
        transForm.setSubmitUrl(R012_SUBMIT_URL);
        transForm.setUserId("102302320");
        transForm.setChoiceAgreement("01");
        return transForm;
    }

    private R013TransForm getR013TransForm() {

        R013TransForm transForm = new R013TransForm();
        transForm.setTranCode("R013");
        transForm.initCommonParams("R013");
        transForm.setSubmitUrl(R013_SUBMIT_URL);
        transForm.setBizType("00");
        transForm.setUserId("102302320");
        transForm.setEnpAcct("21341242423423423");
        transForm.setTransAmt("");
        return transForm;
    }

    private R014TransForm getR014TransForm() {

        R014TransForm transForm = new R014TransForm();
        transForm.setTranCode("R014");
        transForm.initCommonParams("R014");
        transForm.setSubmitUrl(R014_SUBMIT_URL);
        transForm.setEnpAcct("21341242423423423");
        return transForm;
    }

    private Q006TransForm getQ006TransForm() {

        Q006TransForm transForm = new Q006TransForm();
        transForm.setTranCode("Q006");
        transForm.initCommonParams("Q006");
        transForm.setSubmitUrl(Q006_SUBMIT_URL);
        transForm.setUserId("1231243124");
        transForm.setEnpAcct("21341242423423423");
        return transForm;
    }

    private Q007TransForm getQ007TransForm() {
        Q007TransForm transForm = new Q007TransForm();
        transForm.setTranCode("Q007");
        transForm.initCommonParams("Q007");
        transForm.setSubmitUrl(Q007_SUBMIT_URL);
        transForm.setUserId("1231243124");
        transForm.setEnpAcct("21341242423423423");
        transForm.setStartDate("20200729");
        transForm.setEndDate("20200729");
        transForm.setPageSize("10");
        transForm.setPageNum("1");
        return transForm;
    }
    private Q008TransForm getQ008TransForm() {

        Q008TransForm transForm = new Q008TransForm();
        transForm.setTranCode("Q008");
        transForm.initCommonParams("Q008");
        transForm.setSubmitUrl(Q008_SUBMIT_URL);
        transForm.setOriChannelSerialNo("1231243124");
        return transForm;
    }
    private Q009TransForm getQ009TransForm() {

        Q009TransForm transForm = new Q009TransForm();
        transForm.setTranCode("Q009");
        transForm.initCommonParams("Q009");
        transForm.setSubmitUrl(Q009_SUBMIT_URL);
        transForm.setEnpAcct("21341242423423423");
        transForm.setIdentityType("2");
        transForm.setIdentityCode("1231241234324");
        return transForm;
    }
    private Q001TransForm getQ001TransForm() {

        Q001TransForm transForm = new Q001TransForm();
        transForm.setTranCode("Q001");
        transForm.initCommonParams("Q001");
        transForm.setSubmitUrl(Q001_SUBMIT_URL);
        transForm.setUserId("100011747621");
        return transForm;
    }
    private Q002TransForm getQ002TransForm() {

        Q002TransForm transForm = new Q002TransForm();
        transForm.setTranCode("Q002");
        transForm.initCommonParams("Q002");
        transForm.setSubmitUrl(Q002_SUBMIT_URL);
        transForm.setTranMerOrderId("T007_20210316181722");
        transForm.setQueryType("RECV");
        return transForm;
    }
    private Q004TransForm getQ004TransForm() {

        Q004TransForm transForm = new Q004TransForm();
        transForm.setTranCode("Q004");
        transForm.initCommonParams("Q004");
        transForm.setSubmitUrl(Q004_SUBMIT_URL);
        transForm.setUserId("11000000111");
        transForm.setAcctType("1");
        transForm.setStartDate("20181230");
        transForm.setEndDate("20181231");
        transForm.setPageNum("1");
        return transForm;
    }
    private R001TransForm getR001TransForm() {

        R001TransForm transForm = new R001TransForm();
        transForm.setTranCode("R001");
        transForm.initCommonParams("R001");
        transForm.setSubmitUrl(R001_SUBMIT_URL);
        transForm.setMerUserId("11000000111");
        transForm.setUserName("某某公司");
        transForm.setMobile("13211111111");
        transForm.setCertType("2");
        transForm.setCertNo("112312431241231");
        transForm.setCertValidate("20201020");
        transForm.setCardNo("320102000");
        transForm.setBankCode("ICBC");
        transForm.setProvince("北京");
        transForm.setCity("北京市");
        transForm.setBranch("北京支行");
        return transForm;
    }
    private R002TransForm getR002TransForm() {

        R002TransForm transForm = new R002TransForm();
        transForm.setTranCode("R002");
        transForm.initCommonParams("R002");
        transForm.setSubmitUrl(R002_SUBMIT_URL);
        transForm.setUserId("11000000111");
        transForm.setBindCardAgrNo("10000000000000000000");
        transForm.setMobile("13211111111");
        transForm.setCardNo("320102000");
        transForm.setBankCode("ICBC");
        transForm.setProvince("北京");
        transForm.setCity("北京市");
        transForm.setBranch("北京支行");
        return transForm;
    }
    private R003TransForm getR003TransForm() {

        R003TransForm transForm = new R003TransForm();
        transForm.setTranCode("R003");
        transForm.initCommonParams("R003");
        transForm.setSubmitUrl(R003_SUBMIT_URL);
        transForm.setUserId("11000000111");
        transForm.setCertFileType("1");
        return transForm;
    }
    private R004TransForm getR004TransForm() {

        R004TransForm transForm = new R004TransForm();
        transForm.setTranCode("R004");
        transForm.initCommonParams("R004");
        transForm.setSubmitUrl(R004_SUBMIT_URL);
        transForm.setUserId("11000000111");
        transForm.setCertFileType("1");
        return transForm;
    }
    private R007TransForm getR007TransForm() {

        R007TransForm transForm = new R007TransForm();
        transForm.setTranCode("R007");
        transForm.initCommonParams("R007");
        transForm.setSubmitUrl(R007_SUBMIT_URL);
        transForm.setCardNo("6236682340014285547");
        transForm.setHolderName("朱宝龙");
        transForm.setCardAvailableDate("");
        transForm.setCvv2("");
        transForm.setMobileNo("16621066608");
        transForm.setIdentityType("1");
        transForm.setIdentityCode("410823198501050534");
        transForm.setUserId("100011747621");//2021031614434422
        transForm.setMerUserIp("211.12.38.88");
        return transForm;
    }
    private R008TransForm getR008TransForm() {

        R008TransForm transForm = new R008TransForm();
        transForm.setTranCode("R008");
        transForm.initCommonParams("R008");
        transForm.setSubmitUrl(R008_SUBMIT_URL);
        transForm.setNcountOrderId("2021031917659030");//签约订单号，绑卡接口返回的订单号
        transForm.setSmsCode("053771");//签约短信验证码
        transForm.setMerUserIp("211.12.38.88");//用户IP地址
        return transForm;
    }
    private R009TransForm getR009TransForm() {

        R009TransForm transForm = new R009TransForm();
        transForm.setTranCode("R009");
        transForm.initCommonParams("R009");
        transForm.setSubmitUrl(R009_SUBMIT_URL);
        transForm.setOriBindCardAgrNo("2017032312244422");//绑卡协议号
        transForm.setUserId("11000000111");//新生支付用户ID
        return transForm;
    }
    private R010TransForm getR010TransForm() {

        R010TransForm transForm = new R010TransForm();
        transForm.setTranCode("R010");
        transForm.initCommonParams("R010");
        transForm.setSubmitUrl(R010_SUBMIT_URL);
        transForm.setMerUserId("2021031614434422");//用户唯一标识
        transForm.setMobile("16621066608");//手机号
        transForm.setUserName("朱宝龙");//姓名
        transForm.setCertNo("410823198501050534");//身份证号码
        return transForm;
    }
    private R011TransForm getR011TransForm() {

        R011TransForm transForm = new R011TransForm();
        transForm.setTranCode("R011");
        transForm.initCommonParams("R011");
        transForm.setSubmitUrl(R011_SUBMIT_URL);
        transForm.setUserId("2017032312244422");
        transForm.setCertNo("13212121212");
        transForm.setUserName("公司名称");
        transForm.setMobile("18888888888");
        transForm.setCertValidate("20200107");
        return transForm;
    }
    private T002TransForm getT002TransForm() {

        T002TransForm transForm = new T002TransForm();
        transForm.setTranCode("T002");
        transForm.initCommonParams("T002");
        transForm.setSubmitUrl(T002_SUBMIT_URL);
        transForm.setTranAmount("1");//提现金额
        transForm.setUserId("100011747621");
        transForm.setBindCardAgrNo("202103190001288976");//用户绑卡时的协议号
        transForm.setNotifyUrl("http://www.xxx.com/response.do");//异步通知地址
        transForm.setPaymentTerminalInfo("01|10001");
        transForm.setDeviceInfo("192.168.0.1|E1E2E3E4E5E6|359836049182979|20000|12345678901234567890|H1H2H3H4H5H6|AABBCC");
        return transForm;
    }
    private T003TransForm getT003TransForm() {

        T003TransForm transForm = new T003TransForm();
        transForm.setTranCode("T003");
        transForm.initCommonParams("T003");
        transForm.setSubmitUrl(T003_SUBMIT_URL);
        transForm.setPayUserId("300010620023");//付款方ID  商户ID：
        transForm.setReceiveUserId("100011747621");//收款方ID
        transForm.setTranAmount("1.00");//金额
        transForm.setBusinessType("02");//默认：02 转账
        return transForm;
    }
    private T004TransForm getT004TransForm() {

        T004TransForm transForm = new T004TransForm();
        transForm.setTranCode("T004");
        transForm.initCommonParams("T004");
        transForm.setSubmitUrl(T004_SUBMIT_URL);
        transForm.setTranAmount("100.01");
        transForm.setBankCode("PBOC");
        transForm.setCurrencyType("1");
        transForm.setRemark("");
        transForm.setReceiveUserId("11000000111");
        transForm.setMerUserIp("211.12.38.88");
        transForm.setNotifyUrl("http://www.xxx.com/response.do");
        transForm.setBusinessType("01");
        return transForm;
    }
    private T005TransForm getT005TransForm() {

        T005TransForm transForm = new T005TransForm();
        transForm.setTranCode("T005");
        transForm.initCommonParams("T005");
        transForm.setSubmitUrl(T005_SUBMIT_URL);
        transForm.setSerialId("1231234124123");
        transForm.setOrgMerOrderId("1232134325423");
        transForm.setOrgSubmitTime("20200101121212");
        transForm.setOrderAmount("12.12");
        transForm.setRefundSource("1");
        transForm.setDestType("1");
        transForm.setRefundType("1");
        transForm.setRefundAmount("12.12");
        transForm.setNotifyUrl("http://www.xxx.com/response.do");
        transForm.setRemark("备注信息");
        return transForm;
    }
    private T006TransForm getT006TransForm() {

        T006TransForm transForm = new T006TransForm();
        transForm.setTranCode("T006");
        transForm.initCommonParams("T006");
        transForm.setSubmitUrl(T006_SUBMIT_URL);
        transForm.setTranAmount("12.12");
        transForm.setOrgCode("WECHATPAY");
        transForm.setCurrencyType("1");
        transForm.setRemark("备注信息");
        transForm.setNotifyServerUrl("http://www.xxx.com/response.do");
        transForm.setMerUserIp("211.12.38.88");
        transForm.setExpireTime("120");
        transForm.setRiskExpand("111");
        transForm.setGoodsInfo("111");
        transForm.setOrderSubject("111");
        transForm.setOrderDesc("1212");
        transForm.setPayLimit("1");
        transForm.setAppId("1231241434");
        transForm.setOpenId("123123424");
        transForm.setAliAppId("");
        transForm.setBuyerLogonId("");
        transForm.setBuyerId("备注信息");
        transForm.setMerchantId("111accSSVV");
        transForm.setReceiveUserId("213124234");
        transForm.setBusinessType("03");
        transForm.setFeeType("1");
        transForm.setDivideAcctDtl("[{\"ledgerUserId \": \"220000001390\",\"amount\": \"50\"},{\"ledgerUserId\":\"220000001408\",\"amount\":\"50\"}]");
        transForm.setFeeAmountUser("");
        return transForm;
    }
    private T007TransForm getT007TransForm() {

        T007TransForm transForm = new T007TransForm();
        transForm.setTranCode("T007");
        transForm.initCommonParams("T007");
        transForm.setSubmitUrl(T007_SUBMIT_URL);
        transForm.setTranAmount("32.81");//金额66.19
        transForm.setPayType("2");//
        transForm.setCardNo("6236682340014285547"); //银行卡号
        transForm.setHolderName("朱宝龙");
        transForm.setCardAvailableDate(""); //银行卡可为空，信用卡不可为空
        transForm.setCvv2("");//银行卡可为空，信用卡不可为空
        transForm.setMobileNo("16621066608");//绑卡手机号
        transForm.setIdentityType("1");//1、身份证类型
        transForm.setIdentityCode("410823198501050534");//身份证号
        transForm.setBindCardAgrNo("");//绑卡协议号
        transForm.setNotifyUrl("http://www.xxx.com/response.do");//商户异步通知地址
        transForm.setOrderExpireTime("");
        transForm.setUserId("");//用户ID
        transForm.setReceiveUserId("300010620023");//收款方ID，商户号ID
        transForm.setMerUserIp("211.12.38.88");
        transForm.setRiskExpand("");
        transForm.setGoodsInfo("demo商品信息");
        transForm.setSubMerchantId("2102101047542945028");//商户渠道进件 ID
        transForm.setDivideFlag("0");
        transForm.setDivideDetail("");
        return transForm;
    }
    private T008TransForm getT008TransForm() {

        T008TransForm transForm = new T008TransForm();
        transForm.setTranCode("T008");
        transForm.initCommonParams("T008","T007_20210316181722");
        transForm.setSubmitUrl(T008_SUBMIT_URL);
        transForm.setNcountOrderId("2021031618274006");//新生支付下单接口返回的平台的订单号
        transForm.setSmsCode("135870");//短信验证码
        transForm.setMerUserIp("211.12.38.88");
        transForm.setPaymentTerminalInfo("02|10001"); //交易终端电脑01  手机02
        transForm.setReceiverTerminalInfo("02|00001|CN|110000");//收款方终端信息
        transForm.setDeviceInfo("192.168.0.1|E1E2E3E4E5E6|359836049182979|20000|12345678901234567890|H1H2H3H4H5H6|AABBCC");//绑卡设备信息
        transForm.setBusinessType("03");
        transForm.setFeeType("");
        transForm.setDivideAcctDtl("[{\"ledgerUserId \": \"220000001390\",\"amount\": \"50\"},{\"ledgerUserId\":\"220000001408\",\"amount\":\"50\"}]");
        transForm.setFeeAmountUser("");
        return transForm;
    }
    private R015TransForm getR015TransForm() {

        R015TransForm transForm = new R015TransForm();
        transForm.setTranCode("R015");
        transForm.initCommonParams("R015");
        transForm.setSubmitUrl(R015_SUBMIT_URL);
        transForm.setCardNo("6230200017171717");
        transForm.setUserId("100003830757");
        transForm.setMobileNo("13212121212");
        transForm.setFrontUrl("http://www.xxx.com/response.do");
        transForm.setNotifyUrl("https://www.baidu.com ");
        transForm.setMerUserIp("211.12.38.88");
        transForm.setPageStyle("1");
        return transForm;
    }
    private T009TransForm getT009TransForm() {

        T009TransForm transForm = new T009TransForm();
        transForm.setTranCode("T009");
        transForm.initCommonParams("T009");
        transForm.setSubmitUrl(T009_SUBMIT_URL);
        transForm.setTranAmount("12.12");
        transForm.setOrgCode("WECHATPAY");
        transForm.setCurrencyType("1");
        transForm.setRemark("备注信息");
        transForm.setNotifyServerUrl("http://www.xxx.com/response.do");
        transForm.setMerUserIp("211.12.38.88");
        transForm.setExpireTime("120");
        transForm.setRiskExpand("111");
        transForm.setGoodsInfo("111");
        transForm.setOrderSubject("111");
        transForm.setOrderDesc("1212");
        transForm.setPayLimit("1");
        transForm.setMerchantId("111accSSVV");
        transForm.setReceiveUserId("213124234");
        transForm.setBusinessType("03");
        transForm.setFeeType("1");
        transForm.setDivideAcctDtl("[{\"ledgerUserId \": \"220000001390\",\"amount\": \"50\"},{\"ledgerUserId\":\"220000001408\",\"amount\":\"50\"}]");
        transForm.setFeeAmountUser("");
        return transForm;
    }
    private T010TransForm getT010TransForm() {

        T010TransForm transForm = new T010TransForm();
        transForm.setTranCode("T010");
        transForm.initCommonParams("T010");
        transForm.setSubmitUrl(T010_SUBMIT_URL);
        transForm.setMerUserIp("211.12.38.88");
        transForm.setGoodsInfo("111");
        transForm.setTranAmount("12.12");
        transForm.setPayType("BANK_B2C");
        transForm.setBankCode("ICBC");
        transForm.setCurrencyType("1");
        transForm.setDirectFlag("1");
        transForm.setNotifyFrontUrl("http://www.xxx.com/response.do");
        transForm.setNotifyServerUrl("http://www.xxx.com/response.do");
        transForm.setReceiveUserId("213124234");
        transForm.setPaymentTerminalInfo("01|10001");
        transForm.setReceiverTerminalInfo("01|00001|CN|110000");
        transForm.setDeviceInfo("192.168.0.1|E1E2E3E4E5E6|12456789012345|20000|12345678901234567890|H1H2H3H4H5H6|AABBCC");
        transForm.setBusinessType("03");
        transForm.setFeeType("1");
        transForm.setDivideAcctDtl("[{\"ledgerUserId \": \"220000001390\",\"amount\": \"50\"},{\"ledgerUserId\":\"220000001408\",\"amount\":\"50\"}]");
        transForm.setFeeAmountUser("");
        return transForm;
    }
    private T011TransForm getT011TransForm() {

        T011TransForm transForm = new T011TransForm();
        transForm.setTranCode("T011");
        transForm.initCommonParams("T011");
        transForm.setSubmitUrl(T011_SUBMIT_URL);
        transForm.setNcountOrderId("1231242345");
        transForm.setBusinessType("03");
        transForm.setOrgTranCode("T010");
        return transForm;
    }
    private T012TransForm getT012TransForm() {

        T012TransForm transForm = new T012TransForm();
        transForm.setTranCode("T012");
        transForm.initCommonParams("T012");
        transForm.setSubmitUrl(T012_SUBMIT_URL);
        transForm.setColDate("20191224");
        return transForm;
    }
    private T013TransForm getT013TransForm() {

        T013TransForm transForm = new T013TransForm();
        transForm.setTranCode("T013");
        transForm.initCommonParams("T013");
        transForm.setSubmitUrl(T013_SUBMIT_URL);
        transForm.setTranAmount("12.12");
        transForm.setCurrencyType("1");
        transForm.setPayType("BANK_B2C");
        transForm.setReceiveUserId("213124234");
        transForm.setNotifyFrontUrl("http://www.xxx.com/response.do");
        transForm.setNotifyServerUrl("http://www.xxx.com/response.do");
        transForm.setRiskExpand("111");
        transForm.setGoodsInfo("111");
        transForm.setOrderSubject("111");
        transForm.setOrderDesc("1212");
        transForm.setMerchantId("111accSSVV");
        transForm.setMerUserIp("211.12.38.88");
        transForm.setPayLimit("1");
        return transForm;
    }
    private F001TransForm getF001TransForm() {

        F001TransForm transForm = new F001TransForm();
        transForm.setTranCode("F001");
        transForm.initCommonParams("F001");
        transForm.setSubmitUrl(F001_SUBMIT_URL);
        transForm.setOrgMerOrderId("T006_1021345435");
        transForm.setDivideId("123456789");
        transForm.setDivideDtlId("223456789");
        return transForm;
    }

    public String getVerifyDataByTranCode(String tranCode, Map<String, Object> responseMap) {

        StringBuffer sb = new StringBuffer();

        String[] verifyFields = getVerifyArrByTranCode(tranCode);

        for (int i = 0; i < verifyFields.length; i++) {
            sb.append(verifyFields[i]);
            sb.append("=[");
            sb.append(responseMap.get(verifyFields[i]) == null ? "" : responseMap.get(verifyFields[i]));
            sb.append("]");
        }

        return sb.toString();

    }

    private String[] getVerifyArrByTranCode(String tranCode) {
        switch (tranCode) {
            case "R012":
                return R012TransForm.verifyArr;
            case "R013":
                return R013TransForm.verifyArr;
            case "R014":
                return R014TransForm.verifyArr;
            case "Q006":
                return Q006TransForm.verifyArr;
            case "Q007":
                return Q007TransForm.verifyArr;
            case "Q008":
                return Q008TransForm.verifyArr;
            case "Q009":
                return Q009TransForm.verifyArr;
            case "Q001":
                return Q001TransForm.verifyArr;
            case "Q002":
                return Q002TransForm.verifyArr;
            case "Q004":
                return Q004TransForm.verifyArr;
            case "R001":
                return R001TransForm.verifyArr;
            case "R002":
                return R002TransForm.verifyArr;
            case "R003":
                return R003TransForm.verifyArr;
            case "R004":
                return R004TransForm.verifyArr;
            case "R007":
                return R007TransForm.verifyArr;
            case "R008":
                return R008TransForm.verifyArr;
            case "R009":
                return R009TransForm.verifyArr;
            case "R010":
                return R010TransForm.verifyArr;
            case "R011":
                return R011TransForm.verifyArr;
            case "T002":
                return T002TransForm.verifyArr;
            case "T003":
                return T003TransForm.verifyArr;
            case "T004":
                return T004TransForm.verifyArr;
            case "T005":
                return T005TransForm.verifyArr;
            case "T006":
                return T006TransForm.verifyArr;
            case "T007":
                return T007TransForm.verifyArr;
            case "T008":
                return T008TransForm.verifyArr;
            case "R015":
                return R015TransForm.verifyArr;
            case "T009":
                return T009TransForm.verifyArr;
            case "T010":
                return T010TransForm.verifyArr;
            case "T011":
                return T011TransForm.verifyArr;
            case "T012":
                return T012TransForm.verifyArr;
            case "T013":
                return T013TransForm.verifyArr;
            case "F001":
                return F001TransForm.verifyArr;
            default:
                return null;
        }
    }

}
