package com.quyang.voice.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 二倍均值法随机生成红包金额
 */
public class RedPacketUtil {
    /**
     *
     * @param totalAmount 红包总钱数
     * @param totalPeople 抢红包的总人数
     * @return
     */
    public static List<Integer> dividedRedPackage(Integer totalAmount,Integer totalPeople){
        List<Integer> list=new ArrayList<>();
        if(totalAmount>0 && totalPeople>0){
            Integer restAmount=totalAmount;
            Integer restPeople=totalPeople;
            Random random=new Random();

            for(int i=0;i<totalPeople-1;i++){
                int amount=random.nextInt(restAmount/restPeople*2-1)+1;

                restAmount-=amount;
                restPeople--;

                list.add(amount);
            }
            list.add(restAmount);
        }
        return list;
    }

    public static int validPackage(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).sum();
    }

}
