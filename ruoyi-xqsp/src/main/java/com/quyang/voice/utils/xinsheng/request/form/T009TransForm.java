package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T009TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"tranAmount","orgCode","currencyType","remark","notifyServerUrl","merUserIp","expireTime","riskExpand","goodsInfo","orderSubject","orderDesc","payLimit","merchantId","receiveUserId","businessType","feeType","divideAcctDtl","feeAmountUser"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode" ,"errorCode", "ncountOrderId","qrCodeUrl"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset","tranAmount","orgCode","currencyType","remark","notifyServerUrl","merUserIp","expireTime","riskExpand","goodsInfo","orderSubject","orderDesc","payLimit","merchantId","receiveUserId"};

    private String tranAmount;//交易金额
    private String orgCode;//目标资金机构代码
    private String currencyType;//币种
    private String remark;//备注
    private String notifyServerUrl;//异步通知地址
    private String merUserIp;//用户浏览器IP
    private String expireTime;//订单过期时间
    private String riskExpand;//分控扩展信息
    private String goodsInfo;//商品信息
    private String orderSubject;//订单标题
    private String orderDesc;//订单描述
    private String payLimit;//限制信用卡支付
    private String merchantId;//客户机构号
    private String receiveUserId;//收款方 ID
    private String businessType;//业务类型
    private String feeType;//手续费内扣外扣
    private String divideAcctDtl;//分账明细
    private String feeAmountUser;//手续费承担方id

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
