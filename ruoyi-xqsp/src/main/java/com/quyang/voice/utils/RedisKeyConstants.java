package com.quyang.voice.utils;

import java.io.Serializable;

public class RedisKeyConstants implements Serializable {

	private static final long serialVersionUID = 8381256875914544364L;
	
	public static final String R_VALIDATECODE_KEY = "R_VALIDATECODE_KEY_";//验证码
	public static final String R_VALIDATECODE_WX_KEY = "R_VALIDATECODE_WX_KEY_";//验证码
	public static final String R_USER_KEY = "R_USER_KEY_";//用户
	public static final String R_PUBLISHS_LIKE_KEY = "R_PUBLISHS_LIKE_KEY";//点赞功能
	public static final String R_PUBLISHS_LIKE_USER = "R_PUBLISHS_LIKE_USER";//点赞用户
	public static final String R_PUBLISHS_LIKE_MSG = "R_PUBLISHS_LIKE_MSG";//点赞动态id
	public static final String R_PUBLISHS_LIKE_COUNT = "R_PUBLISHS_LIKE_COUNT";//动态被点赞总数


	
}
