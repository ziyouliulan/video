package com.quyang.voice.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("Result")
public class ResponseUtil<T> {

    /**
     * 1.status状态值：代表本次请求response的状态结果。
     */
    @ApiModelProperty("状态码")
    private Integer status;
    /**
     * 2.response描述：对本次状态码的描述。
     */
    @ApiModelProperty("描述")
    private String msg;
    /**
     * 3.data数据：本次返回的数据。
     */
    @ApiModelProperty("数据")
    private T data;

    /**
     * 成功，创建ResResult：没data数据
     */
    public static ResponseUtil suc() {
        ResponseUtil result = new ResponseUtil();
        result.setResultCode(ResultCode.SUCCESS);
        return result;
    }

    /**
     * 成功，创建ResResult：有data数据
     */
    public static ResponseUtil suc(Object data) {
        ResponseUtil result = new ResponseUtil();
        result.setResultCode(ResultCode.SUCCESS);
        result.setData(data);
        return result;
    }

    /**
     * 失败，指定status、desc
     */
    public static ResponseUtil fail(Integer status, String desc) {
        ResponseUtil result = new ResponseUtil();
        result.setStatus(status);
        result.setMsg(desc);
        return result;
    }

    /**
     * 失败，指定ResultCode枚举
     */
    public static ResponseUtil fail(ResultCode resultCode) {
        ResponseUtil result = new ResponseUtil();
        result.setResultCode(resultCode);
        return result;
    }

    /**
     * 把ResultCode枚举转换为ResResult
     */
    private void setResultCode(ResultCode code) {
        this.status = code.code();
        this.msg = code.message();
    }
}
