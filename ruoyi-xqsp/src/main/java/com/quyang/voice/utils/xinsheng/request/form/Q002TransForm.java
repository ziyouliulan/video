package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class Q002TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"tranMerOrderId","queryType"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode", "errorCode", "tranMerOrderId", "ncountOrderId","orderStatus","tranAmount"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String tranMerOrderId;//商户订单号
    private String queryType;//交易大类

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
