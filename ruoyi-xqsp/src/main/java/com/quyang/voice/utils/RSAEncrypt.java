package com.quyang.voice.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

public class RSAEncrypt {
	private static Map<Integer, String> keyMap = new HashMap<Integer, String>();  //用于封装随机产生的公钥与私钥
	public static void main(String[] args) throws Exception {
		//生成公钥和私钥
		genKeyPair();
		//加密字符串
		String message = "df723820";
		System.out.println("随机生成的公钥为:" + keyMap.get(0));
		System.out.println("随机生成的私钥为:" + keyMap.get(1));

		String pub = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1QcxtOMPceci2V0xNsgpxAQAFhGC8An/ylE5jWBENSkYmdn1IKY7xXW0p6ULLedylQ1S1GVfVrPLXNR0TcIjAVeoiWBrLbkaf0O29BEj0r+8VYCNq1P005xwvaqPV92AEq3vKfrnyMDwDJsRi/FvePfP4rF7i90ONrpakzUYQWwIDAQAB";
		String pri = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALVBzG04w9x5yLZXTE2yCnEBAAWEYLwCf/KUTmNYEQ1KRiZ2fUgpjvFdbSnpQst53KVDVLUZV9Ws8tc1HRNwiMBV6iJYGstuRp/Q7b0ESPSv7xVgI2rU/TTnHC9qo9X3YASre8p+ufIwPAMmxGL8W9498/isXuL3Q42ulqTNRhBbAgMBAAECgYAS3iSPggcxXiP8hx3UQKJBnR8Jc/w11fTHpZR7VuF/hqnCuzbTWjAdA+KnKDbhw2+qbFXW/CpefcBRkp/SNDan0YYPaIKKajQN8MjCSyjDiNJ9irPjHT21D2e8W3NY98nh7wK51k680zalNU6k3LQ9m/KUQJqgSGOxk5A1uQ2XQQJBAPbhE4fhdxGRngXMsy6o6d/w7cIf6arYUp7lWzYuREz2vsioW2jpH7MUQUiXa4edI3UhRTqG5LFUO9yb/g/LajECQQC79BVlgL5ht8dMCnuKTxxj18o1aeGjZWqvAS0YoFUhtfxglk9zKHCMfFME1SZ2bgmoBMCvM+XX6Rd1THvp3DRLAkEAl9VhX6ZoxmNaXpLAeKfl/pZ8g3GBElOsrJC+ghn+Y581Qs9YOLN3jXmdlPmbZnJcRkNKAm7H48+8aTd/D1O5YQJALubP2A5yc6wKnZ0YCwGG0httKDsNxVpvbtBaUoTf+ZOSMQARP2ld/JChntkZyf/XhQf6lT3cIy7VXb4CRe9fVQJBAMD9Z+8yezt77KgaWkytW8YsoE/Zx4QXnpAF2Wfe7KgTeGWsYf3cXkFEmjur/IB6Vh5NiuefSdRfyQn63dhz98I=";

		String messageEn = encrypt(message,pub);
		System.out.println(message + "\t加密后的字符串为:" + messageEn);
		String messageDe = decrypt(messageEn,pri);
		System.out.println("还原后的字符串为:" + messageDe);
	}

	/**
	 * 随机生成密钥对
	 * @throws NoSuchAlgorithmException
	 */
	public static void genKeyPair() throws NoSuchAlgorithmException {
		// KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
		// 初始化密钥对生成器，密钥大小为96-1024位
		keyPairGen.initialize(1024,new SecureRandom());
		// 生成一个密钥对，保存在keyPair中
		KeyPair keyPair = keyPairGen.generateKeyPair();
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();   // 得到私钥
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();  // 得到公钥
		String publicKeyString = new String(Base64.encodeBase64(publicKey.getEncoded()));
		// 得到私钥字符串
		String privateKeyString = new String(Base64.encodeBase64((privateKey.getEncoded())));
		// 将公钥和私钥保存到Map
		keyMap.put(0,publicKeyString);  //0表示公钥
		keyMap.put(1,privateKeyString);  //1表示私钥
	}
	/**
	 * RSA公钥加密
	 *
	 * @param str
	 *            加密字符串
	 * @param publicKey
	 *            公钥
	 * @return 密文
	 * @throws Exception
	 *             加密过程中的异常信息
	 */
	public static String encrypt( String str, String publicKey ) throws Exception{
		//base64编码的公钥
		byte[] decoded = Base64.decodeBase64(publicKey);
		RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
		//RSA加密
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		String outStr = Base64.encodeBase64String(cipher.doFinal(str.getBytes("UTF-8")));
		return outStr;
	}

	/**
	 * RSA私钥解密
	 *
	 * @param str
	 *            加密字符串
	 * @param privateKey
	 *            私钥
	 * @return 铭文
	 * @throws Exception
	 *             解密过程中的异常信息
	 */
	public static String decrypt(String str, String privateKey) throws Exception{
		//64位解码加密后的字符串
		byte[] inputByte = Base64.decodeBase64(str.getBytes("UTF-8"));
		//base64编码的私钥
		byte[] decoded = Base64.decodeBase64(privateKey);
		RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
		//RSA解密
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, priKey);
		String outStr = new String(cipher.doFinal(inputByte));
		return outStr;
	}


}

