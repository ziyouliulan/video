//
//package com.quyang.voice.utils;
//
//import com.google.common.base.Predicates;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.bind.annotation.RequestMethod;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.ResponseMessageBuilder;
//import springfox.documentation.schema.ModelRef;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.Contact;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//
//@Configuration
//public class SwaggerConfig {
//
//    @Value("${swagger.enabled}")
//    private Boolean enabled;
//
//    @Bean
//    public Docket createAdminApi() {
//
//        //添加全局响应状态码
//        List responseMessageList = new ArrayList<>();
//        Arrays.stream(ResultCode.values()).forEach(resultCode -> {
//            responseMessageList.add(
//                    new ResponseMessageBuilder().code(resultCode.code()).message(resultCode.message()).responseModel(
//                            new ModelRef(resultCode.message())).build()
//            );
//        });
//
//        return new Docket(DocumentationType.SWAGGER_2)
//                .globalResponseMessage(RequestMethod.GET, responseMessageList)
//                .globalResponseMessage(RequestMethod.POST, responseMessageList)
//                .globalResponseMessage(RequestMethod.PUT, responseMessageList)
//                .globalResponseMessage(RequestMethod.DELETE, responseMessageList)
//                .enable(enabled)
//                .apiInfo(apiInfo())
//                .select()
//                .paths(PathSelectors.any())
//                //不显示错误的接口地址
//                .paths(Predicates.not(PathSelectors.regex("/error.*")))//错误路径不监控
//                .build()
//                .groupName("悦酷app接口");
//    }
//
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .description("悦酷app")
//                .title("悦酷app 接口文档")
//                .version("1.0")
//                .contact(new Contact("quyang","www.quyang.com","www.quyang.com"))
//                .build();
//    }
//}
