/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * www.hnapay.com
 */

package com.quyang.voice.utils.xinsheng.utils;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JSON操作工具类
 *
 */
public class JsonUtils {
    /**
     * 将JSON串转为Map
     *
     * @param json
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> jsonToMap(String json) throws Exception {
        if (StringUtils.isBlank(json)) {
            return null;
        }

        Map<String, Object> model = new HashMap<String, Object>();
        ObjectMapper mapper = new ObjectMapper();
        System.out.println("************:"+json);
        model = mapper.readValue(json, Map.class);

        return model;
    }

    /**
     * 将对象解析为json串
     *
     * @param obj
     * @return
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static String toJson(Object obj) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(obj);

        return json;
    }

    /**
     * JSON数组转LIST
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> jsonArrayToList(String json,Class<T> clazz){
        return JSONArray.parseArray(json,clazz);

    }

}
