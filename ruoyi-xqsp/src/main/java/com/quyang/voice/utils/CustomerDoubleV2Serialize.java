package com.quyang.voice.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

public class CustomerDoubleV2Serialize extends JsonSerializer<Double> {

    /**
     * TODO 使用方法      @JsonSerialize(using = CustomerDoubleSerialize.class)
     * @Author:lipeng
     * @Date:2022/1/14 14:15
     * @param: aDouble
     * @param: jsonGenerator
     * @param: serializerProvider
     * @return:void
     */
    @Override
    public void serialize(Double aDouble, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        BigDecimal bigDecimal = new BigDecimal(String.valueOf(aDouble));
        BigDecimal bigDecimal1 = bigDecimal.setScale(0, BigDecimal.ROUND_DOWN);

        jsonGenerator.writeNumber(bigDecimal1);
    }
}
