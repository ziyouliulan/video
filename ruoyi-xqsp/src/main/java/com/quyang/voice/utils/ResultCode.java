package com.quyang.voice.utils;

public enum ResultCode {


    SERVE_ERROR(404, "资源，服务未找到"),

    /* 成功状态码 */
    SUCCESS(200, "成功"),

    /* 失败状态码 */
    FAIL(-1, "失败"),
    NO_DATA(-11, "暂无数据"),
    ERR_TIME(-1111, "未到时间"),
    RED_PACKET_EXPIRE(-1113, "红包已到期"),
    RED_PACKET_OVER(-1112, "红包已领完"),
    RED_PACKET_UNEXPIRED(-1116, "房间内已有红包"),
    YIJIAN_LOGIN_FAIL(-111, "一键登录失败"),
    NO_LOTTERY(-1114, "未抽到奖"),
    USER_IS_INVALID(-1115, "该账号被封，请联系客服！"),
    HAVE_APPLE(-1117, "已经申请过，请勿多次申请"),
    NO_PUBLISH(-1118, "动态已经删除"),
    /*授权失败*/
    AUTH_ERROR(403, "访问受限，授权过期"),
    REPEAT_ERROR(405, "无法重复操作"),
    UNAUTHORIZED(10401, "签名验证失败"),
    ALIWITHOUTPAYFAIL(-1201,"支付宝提现失败，请确认填写信息完整，或联系平台客服"),


    /* 系统500错误*/
    SYSTEM_ERROR(500, "系统异常，请稍后重试"),
//    TEST(500, "测试异常"),

    /* 参数错误：400 */
    PARAM_IS_INVALID(400, "参数列表错误（缺少，格式不匹配）"),

    /* 用户错误：1001-1999*/
    USER_HAS_EXISTED(1004, "账号已存在"),
    USER_NOT_FIND(1001, "账号不存在"),
    USER_PWD_ERR(1002, "用户名或密码错误"),
    USER_PWD_NULL(1009, "未设置密码"),
    VCODE_ERR(1003, "验证码错误"),
    PHONE_HAS_EXISTED(1005, "用户名或手机号已存在"),
    SAFEMODE_PWD_ERROR(1006, "青少年模式密码错误"),
    SAFEMODE_OFF_ERROR(1007, "青少年模式未打开"),
    NOT_SUFFICIENT_FUNDS(1008, "余额不足,请充值"),
    USER_LOGOUT(1009, "登出账号成功"),
    PHONE_BIND_SUC(1010, "换绑/绑定手机成功"),
    INVITE_CODE_IS_NOT_EXIST(1011,"邀请码不存在"),
    TODAAY_NO_NUMBER_WITHDRAW(1012,"已达今日提现额度上限"),
    TODAAY_NO_COUNT_WITHDRAW(1013,"已达今日提现次数上限"),
    NO_MAX_WITHDRAW(1014,"超过单次提现上限"),
    NO_MIN_WITHDRAW(1015,"输入的提现金额太小啦"),
    OVER_NUMBER_CHECK(1016,"申请的提现金额过大，请等待后台审核哦~"),
    LIMIT_DAY(1017,"不在提现日期内 "),
    NO_PAY_KEY(10018,"未设置支付密码"),
    NO_CHECK_OLD_PAY_KEY(10019,"原密码错误"),
    MAX_TRANSFER_AMOUNT(10020,"单笔最大转账金额"),
    MAX_TRANSFER_AMOUNT_COUNT(10021,"单日最大转账总额"),
    MAXIMUM_AMOUNT_OF_A_SINGLE_RED_ENVELOPE(10022,"单笔红包最大金额"),
    MAXIMUM_AMOUNT_OF_RED_ENVELOPES_IN_A_SINGLE_DAY(10023,"单日红包最大金额"),
    LACK_OF_INTEGRAL(10024,"积分余额不足"),





    /*群:2001-2999*/
    OUT_INDEX_ADMIN(2002,"群管理员数量超过3个"),

    COACH_BANNED(2001,"教练已被封号"),
    NOT_IN_GROUP(2003,"用户不在群内"),
    NOT_ALERALY_GROUP(2004,"该群不存在"),
    WX_NO_BILD(3001,"请先绑定微信"),
    ALI_NO_BILD(3002,"请先绑定支付宝"),
    ROB_ALARY(3003,"已经抢过该红包"),
    PROHIBIT_RECHARGE_WITHDRAWAL(3004,"当前时间段不能充值和提现"),












    /*VIP：6001-6999*/
    IS_NOT_VIP(6001, "用户不是VIP"),
    AGENT_FULL(6002, "可添加下属数量不足"),
    CAN_NOT_ADD_AGENT(6003, "二级代理不可添加下属"),


    /*房间9000-9999*/
    EXIST_MUSIC(9003, "已经存在该音乐"),
    /*房间9000-9999*/
    USER_IS_BAN(9000, "您已被该房间设为黑名单，3小时候可以再次进入"),
    NO_ROOM_POWER(9001, "暂无权限"),
    HAS_VOTE(9002, "已投票"),
    PK_NOT_FINISHED(9004, "当前PK还未结束或PK间隔未到期，请稍后再试"),
    ROOM_IS_BAN(9005, "房间已被封禁，无法进入"),
    ROOM_END_TIME(9006, "该房间已经到期，无法进入"),
    ROOM_NO＿VIP(9007, "亲，该房间只能VIP用户进入哦"),


    /*实名认证状态8000-8001*/
    CERTIFICATION_SUC(8000, "已实名认证"),
    CERTIFICATION_FAIL(8001, "未实名认证");




    private Integer code;



    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

}

