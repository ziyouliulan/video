package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class Q009TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"enpAcct","identityType","identityCode"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode",  "errorCode","enpAcct","enpAcctStatus","totalAmt"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String enpAcct;//企业号
    private String identityType;//企业证件类型
    private String identityCode;//企业证件号

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
