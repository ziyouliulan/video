package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T003TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"payUserId","receiveUserId","tranAmount","businessType"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode" ,"errorCode"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String payUserId;//付款方用户编号
    private String receiveUserId;//收款方ID
    private String tranAmount;//转账金额
    private String businessType;//业务类型

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
