package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class R001TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"merUserId","userName","mobile","certType","certNo","certValidate","cardNo","bankCode","province","city","branch"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode", "errorCode", "userId","bindCardAgrNo"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String merUserId;//商户用户唯一标识
    private String userName;//用户名称
    private String mobile;//手机号
    private String certType;//证件类型
    private String certNo;//证件号
    private String certValidate;//证件有效期
    private String cardNo;//银行卡号
    private String bankCode;//银行简码
    private String province;//开户行所属省份
    private String city;//开户行所属城市
    private String branch;//开户支行

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
