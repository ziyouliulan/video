package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T005TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"serialId","orgMerOrderId","orgSubmitTime","orderAmount","refundSource","destType","refundType","refundAmount","notifyUrl","remark"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode" ,"errorCode","orgMerOrderId","orderAmount","refundAmount"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String serialId;//请求序列号
    private String orgMerOrderId;//商户原始订单号
    private String orgSubmitTime;//原订单支付下单请求时间
    private String orderAmount;//原订单金额
    private String refundSource;//退款资金来源
    private String destType;//退款目的地类型
    private String refundType;//退款类型
    private String refundAmount;//商户退款金额
    private String notifyUrl;//异步通知地址
    private String remark;//备注

    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
