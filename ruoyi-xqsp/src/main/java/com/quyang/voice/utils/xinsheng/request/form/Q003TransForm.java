package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * Created by gaoyantao on 16/4/10.
 * Q003 - 开户结果查询
 */
@Data
public class Q003TransForm extends BaseTransForm {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
    private String origMerOrderId;
	public static final String[] encryptArr = new String[]{"origMerOrderId"};
	public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode", "errorCode", "userId","bindCardAgrNoList"};
	public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};



	@Override
	public String getEncryptJsonStr() {
		return getJsonStr(this,encryptArr);
	}

	@Override
	public String getVerifyJsonStr() {
		return getJsonStr(this,verifyArr);
	}

	@Override
	public String getSubmitJsonStr() {
		return getJsonStr(this,submitArr);
	}
}
