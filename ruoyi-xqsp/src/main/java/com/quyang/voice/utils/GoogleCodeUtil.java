package com.quyang.voice.utils;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class GoogleCodeUtil {

    /*
     * @Author: 李鹏
     * @Date:  2021/6/11 10:01
     * @Description: TODO 生成SecretKey
     * @return: java.lang.String
    */
    public static String generateSecretKey() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(Base64.decodeBase64("yueku"));
        byte[] buffer = sr.generateSeed(10);
        Base32 codec = new Base32();
        byte[] bEncodedKey = codec.encode(buffer);
        return new String(bEncodedKey);
    }
    /*
     * @Author: 李鹏
     * @Date:  2021/6/11 10:02
     * @Description: TODO 生成SecretKey
     * @param seed:
     * @return: java.lang.String
    */
    public static String generateSecretKey(String seed) throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(Base64.decodeBase64(seed));
        byte[] buffer = sr.generateSeed(10);
        Base32 codec = new Base32();
        byte[] bEncodedKey = codec.encode(buffer);
        return new String(bEncodedKey);
    }
    /*
     * @Author: 李鹏
     * @Date:  2021/6/11 10:02
     * @Description: TODO 生成二维码链接
     * @param user:
     * @param secret:
     * @return: java.lang.String
    */
    public static String getQRBarcode(String user, String secret) {
        String format = "otpauth://totp/%s?secret=%s";
        return String.format(format, user, secret);
    }

    public static int generateCode(byte[] key, long t) throws NoSuchAlgorithmException, InvalidKeyException {
        byte[] data = new byte[8];
        long value = t;
        for (int i = 8; i-- > 0; value >>>= 8) {
            data[i] = (byte) value;
        }
        SecretKeySpec signKey = new SecretKeySpec(key, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signKey);
        byte[] hash = mac.doFinal(data);
        int offset = hash[20 - 1] & 0xF;
        // We're using a long because Java hasn't got unsigned int.
        long truncatedHash = 0;
        for (int i = 0; i < 4; ++i) {
            truncatedHash <<= 8;
            // We are dealing with signed bytes:
            // we just keep the first byte.
            truncatedHash |= (hash[offset + i] & 0xFF);
        }
        truncatedHash &= 0x7FFFFFFF;
        truncatedHash %= 1000000;
        return (int) truncatedHash;
    }


    /**
     * 最多可偏移的时间
     * default 3 - max 17
     */
    static int window_size = 3;
    /*
     * @Author: 李鹏
     * @Date:  2021/6/11 14:02
     * @Description: TODO 验证谷歌验证码
     * @param secret: key
     * @param code: code
     * @param timeMsec:
     * @return: boolean
    */
    public static boolean check_code(String secret, long code, long timeMsec) {
        Base32 codec = new Base32();
        byte[] decodedKey = codec.decode(secret);
        long t = (timeMsec / 1000L) / 30L;
        for (int i = -window_size; i <= window_size; ++i) {
            long hash;
            try {
                hash = generateCode(decodedKey, t + i);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
            if (hash == code) {
                return true;
            }
        }
        return false;
    }

}
