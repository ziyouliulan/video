package com.quyang.voice.utils.xinsheng.request.form;

import lombok.Data;

/**
 * @Author
 * @Version V1.0
 * @Description
 * @Date 2020-10-19
 **/
@Data
public class T007TransForm extends BaseTransForm {

    private static final long serialVersionUID = 1L;

    public static final String[] encryptArr = new String[]{"tranAmount","payType","cardNo","holderName","cardAvailableDate","cvv2","mobileNo","identityType","identityCode","bindCardAgrNo","notifyUrl","orderExpireTime","userId","receiveUserId","merUserIp","riskExpand","goodsInfo","subMerchantId","divideFlag","divideDetail"};
    public static final String[] verifyArr = new String[]{"version", "tranCode", "merOrderId", "merId", "charset", "signType", "resultCode" ,"errorCode", "ncountOrderId","submitTime"};
    public static final String[] submitArr = new String[]{"version", "tranCode", "merId", "merOrderId", "submitTime", "msgCiphertext", "signType", "signValue", "merAttach", "charset"};

    private String tranAmount;//支付金额
    private String payType;//支付方式
    private String cardNo;//支付银行卡卡号
    private String holderName;//持卡人姓名
    private String cardAvailableDate;//信用卡有效期
    private String cvv2;//信用卡CVV2
    private String mobileNo;//银行签约手机号
    private String identityType;//证件类型
    private String identityCode;//证件号码
    private String bindCardAgrNo;//商户异步通知地址
    private String notifyUrl;//备注
    private String orderExpireTime;//订单过期时长
    private String userId;//用户编号
    private String receiveUserId;//收款方ID
    private String merUserIp;//商户用户IP
    private String riskExpand;//风控扩展信息
    private String goodsInfo;//商品信息
    private String subMerchantId;//商户渠道进件ID
    private String divideFlag;//是否分账
    private String divideDetail;//分账明细信息


    @Override
    public String getEncryptJsonStr() {
        return getJsonStr(this,encryptArr);
    }

    @Override
    public String getVerifyJsonStr() {
        return getJsonStr(this,verifyArr);
    }

    @Override
    public String getSubmitJsonStr() {
        return getJsonStr(this,submitArr);
    }
}
