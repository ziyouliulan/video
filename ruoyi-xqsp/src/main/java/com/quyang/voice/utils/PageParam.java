
package com.quyang.voice.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * app分页组件
 * @author bianP
 * @version $Id: AppPage.java, v 0.1 2018年06月20日 下午2:31:23 
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("Page")
public class PageParam implements Serializable{
	
	private static final long serialVersionUID = -7248374800878487522L;

    @ApiModelProperty(value = "当前页" , example = "1")
    private Integer pageNum=1;

    @ApiModelProperty(value = "每页记录数" , example = "1")
    private Integer pageSize=10;

    
}
