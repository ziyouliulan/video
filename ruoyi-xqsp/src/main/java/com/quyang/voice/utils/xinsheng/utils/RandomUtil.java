package com.quyang.voice.utils.xinsheng.utils;


import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 随机字符工具
 */
@Slf4j
public class RandomUtil {
//    private Logger log = Logger.getLogger(this.getClass());
    private RandomUtil() {
    }
    public static RandomUtil getInstance() {
        return new RandomUtil();
    }

    public static final String ALLCHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String LETTERCHAR = "abcdefghijkllmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String NUMBERCHAR = "0123456789";
    public static final String DAXIEZIMU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


    /***
     *  生成固定字母加数组组合
     */
    public String getZiMuShuZi(Integer shuliang){
        Random random = new Random();
        StringBuffer sb = new StringBuffer();

        for(int i=0;i<shuliang;i++){
            //首字母不能为0
            System.out.println("*******"+DAXIEZIMU.charAt(random.nextInt(DAXIEZIMU.length())));
            sb.append(DAXIEZIMU.charAt(random.nextInt(DAXIEZIMU.length())));
            sb.append((random.nextInt(9)+1)+"");
        }
        return sb.toString();
    }

    /***
     *  生成数字组合
     */
    public String getShuZi(Integer shuliang){
        Random random = new Random();
        String result="";
        for(int i=0;i<shuliang;i++){
            //首字母不能为0
            result += (random.nextInt(9)+1);
        }
        return result;
    }

    /***
     *  生成字母组合
     */
    public String getZiMu(Integer shuliang){
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<shuliang;i++){
            //首字母不能为0
            sb.append(DAXIEZIMU.charAt(random.nextInt(DAXIEZIMU.length())));
        }
        return sb.toString();
    }


    /***
     * 生成uid 6位数字
     */
    public String generateUID(){
        Random random = new Random();
        String result="";
        for(int i=0;i<6;i++){
            //首字母不能为0
            result += (random.nextInt(9)+1);
        }
        return result;
    }



    public String generateUIDYQM(){
        Random random = new Random();
        String result="";
        for(int i=0;i<10;i++){
            //首字母不能为0
            result += (random.nextInt(9)+1);
        }
        return result;
    }



    /***
     * 生成uid 10位数字
     */
    public String generateUIDYQM(Map<String, Integer> dataMap){
        if(dataMap == null){
            dataMap = new HashMap<String,Integer>();
        }
        Random random = new Random();
        String result="";
        while(result==null || result.length()<=0 || dataMap.get(result)!=null){
            result = "";
            for(int i=0;i<10;i++){
                //首字母不能为0
                result += (random.nextInt(9)+1);
            }
        }
        return result;
    }

    /***
     * 生成uid 位数字
     */
    public String generateUIDYQMQI(){
        String result="";
        Random random = new Random();

        for(int i=0;i<7;i++){
            //首字母不能为0
            result += (random.nextInt(9)+1);
        }

        return result;
    }

    /**
     * 返回一个定长的随机字符串(只包含大小写字母、数字)
     *
     * @param length
     *            随机字符串长度
     * @return 随机字符串
     */
    public String generateString(int length) {
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(ALLCHAR.charAt(random.nextInt(ALLCHAR.length())));
        }
        return sb.toString();
    }

    /**
     * 返回一个定长的随机纯字母字符串(只包含大小写字母)
     *
     * @param length
     *            随机字符串长度
     * @return 随机字符串
     */
    public String generateMixString(int length) {
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(ALLCHAR.charAt(random.nextInt(LETTERCHAR.length())));
        }
        return sb.toString();
    }

    /**
     * 返回一个定长的随机纯大写字母字符串(只包含大小写字母)
     *
     * @param length
     *            随机字符串长度
     * @return 随机字符串
     */
    public String generateLowerString(int length) {
        return generateMixString(length).toLowerCase();
    }

    /**
     * 返回一个定长的随机纯小写字母字符串(只包含大小写字母)
     *
     * @param length
     *            随机字符串长度
     * @return 随机字符串
     */
    public String generateUpperString(int length) {
        return generateMixString(length).toUpperCase();
    }

    /**
     * 生成一个定长的纯0字符串
     *
     * @param length
     *            字符串长度
     * @return 纯0字符串
     */
    public String generateZeroString(int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            sb.append('0');
        }
        return sb.toString();
    }

    /**
     * 根据数字生成一个定长的字符串，长度不够前面补0
     *
     * @param num
     *            数字
     * @param fixdlenth
     *            字符串长度
     * @return 定长的字符串
     */
    public String toFixdLengthString(long num, int fixdlenth) {
        StringBuffer sb = new StringBuffer();
        String strNum = String.valueOf(num);
        if (fixdlenth - strNum.length() >= 0) {
            sb.append(generateZeroString(fixdlenth - strNum.length()));
        } else {
            throw new RuntimeException("将数字" + num + "转化为长度为" + fixdlenth
                    + "的字符串发生异常！");
        }
        sb.append(strNum);
        return sb.toString();
    }

    /**
     * 每次生成的len位数都不相同
     *
     * @param param
     * @return 定长的数字
     */
    public int getNotSimple(int[] param, int len) {
        Random rand = new Random();
        for (int i = param.length; i > 1; i--) {
            int index = rand.nextInt(i);
            int tmp = param[index];
            param[index] = param[i - 1];
            param[i - 1] = tmp;
        }
        int result = 0;
        for (int i = 0; i < len; i++) {
            result = result * 10 + param[i];
        }
        return result;
    }

    /**
     * 生成ACCID
     * @return
     */
    public String getAccID() {
        Random random = new Random();
        int ends = random.nextInt(99);
        String result = new Date().getTime() + String.format("%02d",ends);
        return result;
    }

    public String getTxQunCode() {
        Random random = new Random();
        int ends = random.nextInt(99);
        String quncode = ""+new Date().getTime();

        String result =  generateUID() + quncode.substring(8);
        return result;
    }



//    public static void main(String[] args){
//    	RandomUtil ranUtils = new RandomUtil();
//    	String uid = ranUtils.generateUID();
//    	System.out.println(uid);
//    }

    public static void main(String[] args) {
        System.out.println(RandomUtil.getInstance().getTxQunCode());
//        System.out.println(RandomUtil.getInstance().getAccID());
//
//        System.out.println("返回一个定长的随机字符串(只包含大小写字母、数字):" + RandomUtil.getInstance().generateString(50));
//        System.out
//                .println("返回一个定长的随机纯字母字符串(只包含大小写字母):" + RandomUtil.getInstance().generateMixString(10));
//        System.out.println("返回一个定长的随机纯大写字母字符串(只包含大小写字母):"
//                + RandomUtil.getInstance().generateLowerString(10));
//        System.out.println("返回一个定长的随机纯小写字母字符串(只包含大小写字母):"
//                + RandomUtil.getInstance().generateUpperString(10));
//        System.out.println("生成一个定长的纯0字符串:" + RandomUtil.getInstance().generateZeroString(10));
//        System.out.println("根据数字生成一个定长的字符串，长度不够前面补0:"
//                + RandomUtil.getInstance().toFixdLengthString(123, 10));
//        int[] in = { 1, 2, 3, 4, 5, 6, 7 };
//        System.out.println("每次生成的len位数都不相同:" + RandomUtil.getInstance().getNotSimple(in, 3));
    }
}
