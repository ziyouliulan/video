package com.quyang.voice.utils;



public class Constants {

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

	public static final String xmlDtd1 = "&lt;";
	public static final String xmlDtd2 = "&gt;";
	public static final String xmlDtd3 = "&amp;";
	public static final String xmlDtd4 = "&apos;";
	public static final String xmlDtd5 = "&quot;";
	
	public static final String QQRELOGIN = "6001";
	
    public class ConstError {
    	
        /**
         * 系统错误
         */
        public static final String ERR_ACTION_NOT_FOUND = "101,Http请求参数中未找到相关方法";
        public static final String HANDLER_NOT_FOUND = "102,在Spring容器中未找到相关处理器[%s]";
        public static final String METHOD_NOT_FOUND = "103,在类[%s]中未找到方法[%s]";
        public static final String METHOD_INVOKE_EXCEPTION = "104,调用类[%s]的[%s]方法出现异常";

        /**
         * 逻辑错误
         */
        public static final String HTTP_SERVLET_PARAMTERS_NOT_MATCH = "105,请求参数不匹配";
        public static final String HTTP_SERVLET_PARAMTERS_IS_NULL = "1001,参数错误,[%s]不能为空";
        public static final String ENTITY_NOT_FOUND = "107,实体类[%s]未找到";
        
        public static final String FAIL = "108,[%s]";
        
        //公共开始
        public static final String NAME_IS_EXISTS_ERROR = "2001,名称已存在";
        public static final String EMAIL_CODE_ERROR = "2002,邮箱格式不正确";
        public static final String BUMBER_CODE_ERROR = "2003,必须为数字";
        public static final String HTTP_CODE_ERROR = "2004,HTTP调用失败";
        //公共结束
        
        //手机短信
        public static final String MOBILE_GET_CODE_ERROR = "-1,获取验证码失败";
        public static final String MOBILE_CODE_ERROR = "3005,验证码错误，不匹配";
        public static final String MOBILE_OVER_CODE_ERROR = "3004,该验证码已经失效";
        public static final String MOBILE_NO_NULL_ERROR = "3003,未设置手机，请联系客服";
        public static final String MOBILE_NO_REGISTER_ERROR = "3002,用户不存在";
        public static final String MOBILE_IS_REGISTER_ERROR = "114,手机号码已注册";        
        public static final String MOBILE_NO_EXISTS_ERROR = "115,请输入正确的机号码";   
        public static final String NO_Longin_ERROR = "116,请重新登录";
        public static final String Longin_Status_ERROR = "118,用户未被激活";

        public static final String LONGIN_PWD_ERROR = "3006,密码错误";
        public static final String REQUEST_VALIDATION_ERROR = "3007,请求验证失败";
        public static final String TOKEN_INVALID_ERROR = "3008,无效token";
        public static final String SAMEQUOTE_LIMITTIME_ERROR = "3010,在10分钟内不能重复发布相同报价";
        public static final String NO_QUOTE_AUTHORITY_ERROR = "3011,您的权限不够，交易员才能发报价";
        public static final String USER_UNFIND_ERROR = "3012,未找到该DM帐号的用户信息";
        public static final String UNEQUAL_OPENID = "3013,亲，此账号已被其他微信号授权，请重新输入";
        public static final String USER_STATUS_ERROR = "3014,用户未审核";
        
        //众包群组
        public static final String GROUP_SHARERIGHTS_ERROR = "1106,您的个人共享数据源已被暂时停用，请联系客服4000-918-000";
        public static final String GROUP_REGISTERED_ERROR = "1104,请到设置里绑定QQ后，才能共享网友的报价源";
        public static final String GROUP_QQONLINESTATUS_ERROR = "1103,请先点击旁边的QQ图标进行登录，QQ登录后再来点击共享数据源图标";
        public static final String GROUP_NO_ERROR = "3001,未找到可以分享的群，请等待数据同步后稍后重试";

        //XMPP
        public static final String XMPP_LOGIN_ERROR = "3100,服务器异常，请联系客服或稍后再试";
        public static final String XMPP_REPORT_ERROR = "3101,服务器数据异常，请联系客服或稍后再试";

        //登录时查询QQRegister信息
        public static final String QQREGISTER_NOT_FOUND = "3200,未找到用户的QQ注册信息";
        
//      1107 获取状态失败QQ 不在线 (陶老师用了)
//      3000 数据校验错误  (陶老师用了)
        
        /***
         * 自定义报价类型
         */
        public static final String USERNDTYPE_NAME_IS_EXISTS_ERROR = "4000,自定义名称已存在";
        public static final String USERNDTYPE_TYPE_EXCEEDING_ERROR = "4001,最多支持20个自定义类型";
        
    }


    public class ConstResult {

        public static final String TOKEN_NOT_EXISTS = "-9";//未找到token
        public static final String XMPP_LOGIN_FAILURE = "-10";//XMPP登录失败
        public static final String XMPP_REPORT_FAILURE = "-11";//XMPP上报失败
        public static final String REQUEST_UNVALIDATED = "-99";//请求验证失败

        /**
         * 登录相关
         */
        public static final String UNCORRECT_USERNAME_OR_PWD = "1";//用户名或密码错误
        public static final String LOGOUT_SUCCESS = "3";//登出成功
        public static final String UNCORRECT_USERNAME = "4";//用户名不正确
        public static final String UNCORRECT_PWD = "5";//密码错误
        public static final String UNEQUAL_OPENID = "6";//openid不一致
        


        /**
         * 发送邮件相关
         */
        public static final String SEND_EMAIL_SUCCESS = "1";//成功
        public static final String SEND_EMAIL_FAILURE = "0";//失败

        /**
         * 设置用户自定义属性
         */
        public static final String USER_DEFINED_SUCCESS = "1";//成功
        public static final String USER_DEFINED_FAILURE = "0";//失败

        /**
         * 设置众包群组
         */
        public static final String QQSGROUP_SET_SUCCESS  = "1";//成功
        public static final String QQSGROUP_SET_FAILURE  = "0";//失败
        public static final String QQSGROUP_UNFINDUSER  = "2";//失败
    }

    public static class Const {

        /**
         * 钱包相关
         */
        //交易类型
        //充值金币
        public static final int  TRADE_CHARGEJINBI = 8;
        //礼物收益
        public static final int  TRADE_GIFTINCOME_INCOME = 9;
        //二级礼物收益
        public static final int  TRADE_GIFTINCOME_INCOME2 = 11;
        //礼物支出
        public static final int  TRADE_GIFTINCOME_PAY = 10;
//        public static final int  TRADE_REDPACK_OUT = 10;
//        public static final int  TRADE_REDPACK_IN = 3;
        //课程提现
        public static final int  TRADE_WITHTHDRAW_COURSE = 3;
        public static final int  TRADE_WITHTHDRAW_AG = 18;
        //购买课程
        public static final int  TRADE_BUYCOURSE =0;
        //首次购买会员
        public static final int  TRADE_BUYVIPONCE =1;
        //匹配
        public static final int  TRADE_PIPEI =2;
        //视频/语音
        public static final int  VIDEOANDVOICE = 4;

        //退款
        public static final int  TUIKUAN = 6;
        //购买会员（复购）
        public static final int  BUYVIP = 7;

        //分销收益
        public static final int  FERNXIAO = 14;

        //直系收益
        public static final int  ONEIN = 5;

        //二级收益
        public static final int  TWOIN = 11;

        public static final int  COURSE_IN = 17;

        //交易货币
        public static final int  CURRENCY_RMB = 0;
        public static final int  CURRENCY_JINBI = 1;
        public static final int  CURRENCY_ZUANSHI = 2;


        //收支类型
        //收入
        public static final int TRADE_TYPE_IN = 0;
        public static final int TRADE_TYPE_OUT = 1;

        //交易状态
        //通过
        public static final int TRADE_STATUS_PASS = 1;
        //待审核
        public static final int TRADE_STATUS_PROCESSE = 0;
        //被拒绝
        public static final int TRADE_STATUS_BAN = 2;
        //不需要审核
        public static final int TRADE_NO_CHECK = 3;

        //账号类型
        //银行
        public static final int PAY_ACCOUNT_BAN = 0;
        //微信
        public static final int PAY_ACCOUNT_WEIXIN = 1;
        //支付宝
        public static final int PAY_ACCOUNT_ALIPAY = 2;
        //苹果内购
        public static final int PAY_ACCOUNT_APPLEPAY = 2;


        //支付手机渠道
        public static final int PAY_PHONE_TYPE_ANDROID = 0;
        //微信
        public static final int PAY_PHONE_TYPE_APPLE = 1;





    }



}
