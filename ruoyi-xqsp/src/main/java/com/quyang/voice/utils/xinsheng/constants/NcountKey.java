package com.quyang.voice.utils.xinsheng.constants;

/**
 * 常量配置
 */
public class NcountKey {
    //新生签名使用的算法
    public static String ALGORITHM = "RSA";

    //类型
    public static String SIGN_ALGORITHM = "SHA1withRSA";

    //前置系统公钥
    //public static String NCOUNT_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnXwplvkCxvJKBx6O/Caa3Nztix5SVlum5YlvKyUOlcQZE1sLYoKJd9WE6jNt8o4HQGcJEeWGhDPmnppdBAYwuilSKSatRWaTygHidI8QVIk8r4zByz2PX0/855LlyM60YcgIHJkaKXZgB3si+FnavxO7cFQtjFFhx62+L0/TL/wIDAQAB";

    public static String NCOUNT_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9vGvYjivDF5uPBNDXMtoAtjYQ2YPSsfareduDG6kHL/N3A05rFHA11Dbr+UON82Y4V0RFKAQeZFPWcTLjcy6ntZVI8XoYLpuVQBPsb0Ya+PwbzR8/TmUdUf91ru8APtJgqkULgPVrO1hhzZ1tQMznosNLTOqbknMnnMcwzB5yYwIDAQAB";

    public static String MER_PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIkzfQfU2sHzJ07o3mrSy3NbsjAyc9skjckWSuJvu1MfVS583Cgy93jkyxZS1FQx4MM202WExmkkWV2cN5HwV6KTVEDxf8nNjlZmCQDPG2M6PeXMxyNErENgYhXOGyHRyBRjyblwsVARgHsSyoIi6D/vq7dlfR1nXE0GXxkNX7KFAgMBAAECgYBvlLtDij2a7Jh0WXjBocuqdtGU1wNYHwNcnc+rFjlfoaTSGOOnocOXw2LTNevOyX3E5arI7lG6osA3HFwpSTDLKQW3p1cabswt9etz6a/yWwkTnVDyKEWBpJNqxzLtlILsSyKOeFNcuBLkeVYKNZFuSdJhLuDJvPCbIvnO3YpZ4QJBANSr+99UNXcejzOL6qq0NFTiEO8UMvpWut4em3gZjf8SV8KtpnSHFp13O+8UMpCan2zQ5PCfRC0mphrqaxdFDm0CQQClJ0oElCJdlKhMZ3REiVLHqLxJZWF+T1tcAHpUBONIiDp9QVuPcCq14jHi/O7QqdtN+W1oZxgMXzyhG+Zi6sV5AkEAuTmdIhJeioPkJvnqhYhzt6Y5J3EJQT1+fWxc7cocARcBtn21ZvItATJS0dRegdEZnxWFAAlGwKhTrUeTw+2SNQJAfjbhfVBMVyI14HYrx/j+l3Gfu6PL90DkfBItaqp1nEfoEzNwqtU4/tmH8IYGaNd2r0eQ0nn/Se06o5Fz52KuWQJBAIQM/X4Qlk9sElU+qUF2ro2Ft6+fkkEAmW4TvdVe7uHusv8IIhEMf9HSvXs2erpPEJ6hTHItoYRBULKkmX8JSV4=";

    //测试环境商户ID
    public static String MER_ID = "300029616020";
    public static String SubMerchantId = "2112201719582960208";

//    public static final String ZhiFuNotifyUrl = "http://47.118.58.157/appchongzhi/zhifuhuidiao";
    public static final String ZhiFuNotifyUrl = "http://api.jiujian123.com/api/realPersonAuthentication/paymentCallback";
    public static final String TiXianNotifyUrl = "http://api.jiujian123.com/api/realPersonAuthentication/withdrawalCallback";

}
