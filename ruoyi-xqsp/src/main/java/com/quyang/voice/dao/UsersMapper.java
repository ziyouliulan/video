package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Users;
import com.quyang.voice.model.vo.UserBaseVo;
import com.quyang.voice.model.vo.UserInformationBodyVo;
import com.quyang.voice.model.vo.UsersVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UsersMapper extends BaseMapper<Users> {

    @Select("SELECT * from t_user where id = #{userId}")
    Users selectByUserId(Long userId);

    @Select("SELECT * from t_user where phone = #{phone}")
    Users selectByPhone(String phone);

    @Select("SELECT * from t_user where phone = #{account} or sys_id = #{account}")
    Users selectByPhoneOrSysid(String account);

    @Select("SELECT * from t_user where id = #{userId}")
    Users selectByPrimaryKey(Long userId);

    @Update("update t_user set passwd = #{passwd} where id = #{id}")
    int updatePasswdById(Long id, String passwd);

    @Update("update t_user set status=#{status} where id=#{id}")
    int getMyStatus(Long id, int status);

    @Update("update t_user set phone=#{phone} where id=#{id}")
    int changePhone(Long id, String phone);

    @Update("update t_user set safemode_status=#{safemodeStatus} where id=#{id}")
    int changeSafeModeStatus(Long id, Integer safemodeStatus);

    @Update("update t_user set safemode_pwd=#{safemodePwd} where id=#{id}")
    int changeSafeModePwd(Long id, String safemodePwd);

    @Update("update t_user set nickname=#{nickname},avatar=#{avatar}," +
            "sex=#{sex},birthday=#{birthday},sign=#{sign}" +
            " where id=${id}")
    int updateProfile(Users users);

    @Select("select * from t_user where wx=#{openId}")
    Users getWxUser(String openId);

    @Select("select * from t_user where qq=#{openId}")
    Users getQQUser(String openId);

    //根据用户id查询是否可跟随
    @Select("SELECT follow FROM t_user_details WHERE user_id=#{userId}")
    Integer getFollowStatus(Long userId);

    //新增用户意见反馈
    @Insert("INSERT INTO t_user_feedback (user_id,views,create_time) VALUES(#{userId},#{views},NOW())")
    int userFeedback(Long userId, String views);

    @Select("SELECT * from t_user where sys_id = #{sysId}")
    Users getUsersByASysId(String sysId);


    /**
     * 获得用户基本信息
     * @param sysId
     * @return
     */

    @Select("\tSELECT\n" +
            "\tu.id AS id,\n" +
            "\tu.`nickname` AS `name`,\n" +
            "\tu.sysid AS sysid,\n" +
            "\tu.sex AS sex,\n" +
            "\tu.avatar AS avatar\n" +
            "FROM\n" +
            "\tt_user u\n" +
            "\tWHERE\n" +
            "\tu.sysid = #{sysid}")
    UserBaseVo getUserBaseVoBysysId(String sysId);


    @Select("SELECT u.id userId,u.sys_id,u.birthday,u.avatar\n" +
            " FROM t_user u\n" +
            " WHERE u.id=#{userId}")
    UserInformationBodyVo getInformationBody(@Param("userId") Long userId);


    //根据用户id查询用户的昵称，账号，性别，头像，生日,手机 remark
    UsersVo getUserBase(Long id);
}
