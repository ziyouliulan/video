
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.GroupTab;
import com.quyang.voice.model.vo.GroupTabVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GroupTabMapper extends BaseMapper<GroupTab> {



    @Select("    SELECT" +

            "    gt.id AS id," +
            "    gt.NAME AS NAME," +
            "    gt.address_url AS address_url," +
            "    gt.img_url AS img_url," +
            "    ugt.group_id AS group_id," +
            "    IFNULL(ugt.group_tab_on,0) AS group_tab_on" +
            "    FROM" +
            "    l_group_tab gt" +
            "    LEFT JOIN l_user_group_tab ugt ON gt.id = ugt.group_tab_id and ugt.group_id = #{clusterId} and ugt.status = 1")
    public List<GroupTabVo> findByGroupId(String clusterId);


}
