
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.PublishCommentsReply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PublishCommentsReplyMapper extends BaseMapper<PublishCommentsReply> {

    /**
     * 动态回复表
     * @param commentId
     * @return
     */
    List<PublishCommentsReply> getPublishCommentsReply(Long commentId,Integer userId);

}
