package com.quyang.voice.dao;

import com.quyang.voice.model.MissuUserMsgsCollect;
import com.quyang.voice.model.MissuUserMsgsCollectExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface MissuUserMsgsCollectDao {
    long countByExample(MissuUserMsgsCollectExample example);

    int deleteByExample(MissuUserMsgsCollectExample example);

    int deleteByPrimaryKey(Integer collectId);

    int insert(MissuUserMsgsCollect record);

    int insertSelective(MissuUserMsgsCollect record);

    List<MissuUserMsgsCollect> selectByExample(MissuUserMsgsCollectExample example);

    MissuUserMsgsCollect selectByPrimaryKey(Integer collectId);

    int updateByExampleSelective(@Param("record") MissuUserMsgsCollect record, @Param("example") MissuUserMsgsCollectExample example);

    int updateByExample(@Param("record") MissuUserMsgsCollect record, @Param("example") MissuUserMsgsCollectExample example);

    int updateByPrimaryKeySelective(MissuUserMsgsCollect record);

    int updateByPrimaryKey(MissuUserMsgsCollect record);


    String getFingerprint(HashMap map);
}
