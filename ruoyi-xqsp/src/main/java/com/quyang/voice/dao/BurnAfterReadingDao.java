package com.quyang.voice.dao;

import com.quyang.voice.model.BurnAfterReading;
import com.quyang.voice.model.BurnAfterReadingExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BurnAfterReadingDao {
    long countByExample(BurnAfterReadingExample example);

    int deleteByExample(BurnAfterReadingExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BurnAfterReading record);

    int insertSelective(BurnAfterReading record);

    List<BurnAfterReading> selectByExample(BurnAfterReadingExample example);

    BurnAfterReading selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BurnAfterReading record, @Param("example") BurnAfterReadingExample example);

    int updateByExample(@Param("record") BurnAfterReading record, @Param("example") BurnAfterReadingExample example);

    int updateByPrimaryKeySelective(BurnAfterReading record);

    int updateByPrimaryKey(BurnAfterReading record);
}
