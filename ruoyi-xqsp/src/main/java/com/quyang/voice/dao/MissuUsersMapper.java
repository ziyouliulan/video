
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.model.vo.MissuUsersBaseVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MissuUsersMapper extends BaseMapper<MissuUsers> {





    @Select("SELECT mu.*,  ugt.group_id AS group_id, IFNULL( ugt.group_tab_on, 0 ) AS group_tab_on" +
            " from missu_users mu LEFT JOIN l_user_group_tab ugt ON mu.user_uid = ugt.group_tab_id AND ugt.group_id = #{clusterId}   and  ugt.status  = 2  where mu.user_type = 3")
    public List<MissuUsersBaseVo> findByGroupId(String clusterId);

}

