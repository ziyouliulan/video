
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.UsermoenyLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UsermoenyLogMapper extends BaseMapper<UsermoenyLog> {

}
