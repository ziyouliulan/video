package com.quyang.voice.dao;

import com.quyang.voice.model.Doctor;
import com.quyang.voice.model.Patient;
import com.quyang.voice.model.PatientInformation;
import com.quyang.voice.model.PatientInformationExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public interface PatientInformationDao {
    long countByExample(PatientInformationExample example);

    int deleteByExample(PatientInformationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PatientInformation record);

    int insertSelective(PatientInformation record);

    List<PatientInformation> selectByExample(PatientInformationExample example);

    Doctor selectByUserId(Integer useid);


    Patient selectByPatientId(Integer id);


    PatientInformation selectByPrimaryKey(Integer id);


    List<PatientInformation> selectByMap(HashMap map);

    int updateByExampleSelective(@Param("record") PatientInformation record, @Param("example") PatientInformationExample example);

    int updateByExample(@Param("record") PatientInformation record, @Param("example") PatientInformationExample example);

    int updateByPrimaryKeySelective(PatientInformation record);

    int updateByPrimaryKey(PatientInformation record);
}
