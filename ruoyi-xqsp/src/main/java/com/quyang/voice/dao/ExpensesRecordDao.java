package com.quyang.voice.dao;

import com.quyang.voice.model.ExpensesRecord;
import com.quyang.voice.model.ExpensesRecordExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpensesRecordDao {
    long countByExample(ExpensesRecordExample example);

    int deleteByExample(ExpensesRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ExpensesRecord record);

    int insertSelective(ExpensesRecord record);

    List<ExpensesRecord> selectByExample(ExpensesRecordExample example);

    ExpensesRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ExpensesRecord record, @Param("example") ExpensesRecordExample example);

    int updateByExample(@Param("record") ExpensesRecord record, @Param("example") ExpensesRecordExample example);

    int updateByPrimaryKeySelective(ExpensesRecord record);

    int updateByPrimaryKey(ExpensesRecord record);
}
