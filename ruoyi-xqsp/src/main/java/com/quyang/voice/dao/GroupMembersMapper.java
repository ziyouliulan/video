
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.GroupMembers;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface GroupMembersMapper extends BaseMapper<GroupMembers> {

}
