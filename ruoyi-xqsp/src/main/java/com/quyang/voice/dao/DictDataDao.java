package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.ConsumeRecord;
import com.quyang.voice.model.DictData;
import com.quyang.voice.model.DictDataExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictDataDao extends BaseMapper<DictData> {
    long countByExample(DictDataExample example);

    int deleteByExample(DictDataExample example);

    int deleteByPrimaryKey(Long dictCode);

    int insert(DictData record);

    int insertSelective(DictData record);

    List<DictData> selectByExample(DictDataExample example);

    DictData selectByPrimaryKey(Long dictCode);

    int updateByExampleSelective(@Param("record") DictData record, @Param("example") DictDataExample example);

    int updateByExample(@Param("record") DictData record, @Param("example") DictDataExample example);

    int updateByPrimaryKeySelective(DictData record);

    int updateByPrimaryKey(DictData record);
}
