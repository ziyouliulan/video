package com.quyang.voice.dao;

import com.quyang.voice.model.EsysSn;
import com.quyang.voice.model.EsysSnExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EsysSnDao {
    long countByExample(EsysSnExample example);

    int deleteByExample(EsysSnExample example);

    int deleteByPrimaryKey(String snName);

    int insert(EsysSn record);

    int insertSelective(EsysSn record);

    List<EsysSn> selectByExample(EsysSnExample example);

    EsysSn selectByPrimaryKey(String snName);

    int updateByExampleSelective(@Param("record") EsysSn record, @Param("example") EsysSnExample example);

    int updateByExample(@Param("record") EsysSn record, @Param("example") EsysSnExample example);

    int updateByPrimaryKeySelective(EsysSn record);

    int updateByPrimaryKey(EsysSn record);
}
