package com.quyang.voice.dao;

import com.quyang.voice.model.MissuOfflineHistory;
import com.quyang.voice.model.MissuOfflineHistoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface MissuOfflineHistoryDao {
    long countByExample(MissuOfflineHistoryExample example);

    int deleteByExample(MissuOfflineHistoryExample example);

    int deleteByPrimaryKey(Integer historyId);

    int insert(MissuOfflineHistory record);

    int insertSelective(MissuOfflineHistory record);

    List<MissuOfflineHistory> selectByExample(MissuOfflineHistoryExample example);

    MissuOfflineHistory selectByPrimaryKey(Integer historyId);

    int updateByExampleSelective(@Param("record") MissuOfflineHistory record, @Param("example") MissuOfflineHistoryExample example);

    int updateByExample(@Param("record") MissuOfflineHistory record, @Param("example") MissuOfflineHistoryExample example);
    int updateByExampleRed(HashMap map);

    int updateByPrimaryKeySelective(MissuOfflineHistory record);

    int updateByPrimaryKey(MissuOfflineHistory record);
}
