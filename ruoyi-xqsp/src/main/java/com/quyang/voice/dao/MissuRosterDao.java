package com.quyang.voice.dao;

import com.quyang.voice.model.MissuRoster;
import com.quyang.voice.model.MissuRosterExample;
import com.quyang.voice.model.MissuRosterKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MissuRosterDao {
    long countByExample(MissuRosterExample example);

    int deleteByExample(MissuRosterExample example);

    int deleteByPrimaryKey(MissuRosterKey key);

    int insert(MissuRoster record);

    int insertSelective(MissuRoster record);

    List<MissuRoster> selectByExample(MissuRosterExample example);

    MissuRoster selectByPrimaryKey(MissuRosterKey key);

    int updateByExampleSelective(@Param("record") MissuRoster record, @Param("example") MissuRosterExample example);

    int updateByExample(@Param("record") MissuRoster record, @Param("example") MissuRosterExample example);

    int updateByPrimaryKeySelective(MissuRoster record);

    int updateByPrimaryKey(MissuRoster record);
}
