package com.quyang.voice.dao;

import com.quyang.voice.model.CarouselIndicators;
import com.quyang.voice.model.CarouselIndicatorsExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarouselIndicatorsDao {
    long countByExample(CarouselIndicatorsExample example);

    int deleteByExample(CarouselIndicatorsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CarouselIndicators record);

    int insertSelective(CarouselIndicators record);

    List<CarouselIndicators> selectByExample(CarouselIndicatorsExample example);

    CarouselIndicators selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CarouselIndicators record, @Param("example") CarouselIndicatorsExample example);

    int updateByExample(@Param("record") CarouselIndicators record, @Param("example") CarouselIndicatorsExample example);

    int updateByPrimaryKeySelective(CarouselIndicators record);

    int updateByPrimaryKey(CarouselIndicators record);
}
