
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.GroupSlience;
import com.quyang.voice.model.vo.UserBaseVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GroupSlienceMapper extends BaseMapper<GroupSlience> {

    /**
     * 获得被禁言用户id
     * @param clusterId
     * @return
     */
    List<UserBaseVo> getSilenceUserList(Long clusterId);

}
