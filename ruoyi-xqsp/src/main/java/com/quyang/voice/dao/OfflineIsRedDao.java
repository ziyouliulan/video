package com.quyang.voice.dao;

import com.quyang.voice.model.OfflineIsRed;
import com.quyang.voice.model.OfflineIsRedExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfflineIsRedDao {
    long countByExample(OfflineIsRedExample example);

    int deleteByExample(OfflineIsRedExample example);

    int insert(OfflineIsRed record);

    int insertSelective(OfflineIsRed record);

    List<OfflineIsRed> selectByExample(OfflineIsRedExample example);

    int updateByExampleSelective(@Param("record") OfflineIsRed record, @Param("example") OfflineIsRedExample example);

    int updateByExample(@Param("record") OfflineIsRed record, @Param("example") OfflineIsRedExample example);
}
