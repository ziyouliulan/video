
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.ConsumeRecord;
import com.quyang.voice.model.vo.ConsumeRecordSimpleVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ConsumeRecordMapper extends BaseMapper<ConsumeRecord> {

    /**
     * 获得
     * @param userId
     * @param type
     * @param year
     * @param month
     */
    List<ConsumeRecordSimpleVo> getConsumRecord(Integer userId, Integer type,Integer gId, Integer year, Integer month);


    /**
     * 按时间查询收入支出信息
     *
     * @param userId
     * @param year
     * @param month
     * @param type
     * @return
     */
    Double getCharge(Integer userId,Integer year, Integer month, Integer type,Integer gId);

    /**
     * 当天支出的最大值
     *
     * @param userId
     * @param type
     * @return
     */
    Double getSumCharge(Long userId, Integer type);
    /**
     * 当天支出的最大值
     *
     * @param userId
     * @param type
     * @return
     */
    Double getSumCharges(Long userId, Integer[] type);

    /**
     * 当天支出的最大值
     *
     * @param userId
     * @param type
     * @return
     */
    Integer getSumCount(Long userId, Integer type);

}
