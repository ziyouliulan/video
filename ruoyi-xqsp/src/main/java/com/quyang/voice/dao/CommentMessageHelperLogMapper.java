
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.CommentMessageHelperLog;
import com.quyang.voice.model.vo.CommentMessageVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CommentMessageHelperLogMapper extends BaseMapper<CommentMessageHelperLog> {
    //评论消息页面
    List<CommentMessageVo> getCommentMessage(Long userId);
    //查询未读条数;ID:查看者id
    Integer notReadCount(Long id);
    //清空该用户所有评论未读
    int allToRead(Long userId);
}
