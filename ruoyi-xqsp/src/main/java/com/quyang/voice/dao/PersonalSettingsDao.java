package com.quyang.voice.dao;

import com.quyang.voice.model.PersonalSettings;
import com.quyang.voice.model.PersonalSettingsExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonalSettingsDao {
    long countByExample(PersonalSettingsExample example);

    int deleteByExample(PersonalSettingsExample example);

    int deleteByPrimaryKey(Integer userId);

    int insert(PersonalSettings record);

    int insertSelective(PersonalSettings record);

    List<PersonalSettings> selectByExample(PersonalSettingsExample example);

    PersonalSettings selectByPrimaryKey(Integer userId);

    int updateByExampleSelective(@Param("record") PersonalSettings record, @Param("example") PersonalSettingsExample example);

    int updateByExample(@Param("record") PersonalSettings record, @Param("example") PersonalSettingsExample example);

    int updateByPrimaryKeySelective(PersonalSettings record);

    int updateByPrimaryKey(PersonalSettings record);
}
