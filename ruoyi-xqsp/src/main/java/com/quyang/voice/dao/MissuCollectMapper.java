
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.MissuCollect;
import com.quyang.voice.model.vo.MissuCollectVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MissuCollectMapper extends BaseMapper<MissuCollect> {

    /**
     * 根据id查询收藏信息
     * @param id
     * @return
     */
    MissuCollectVo getMissuCollectById(Long id);

    /**
     * 根据userId查询收藏信息
     * @param userId
     * @return
     */
    List<MissuCollectVo> getMisscCollectVoList(Integer userId);

}
