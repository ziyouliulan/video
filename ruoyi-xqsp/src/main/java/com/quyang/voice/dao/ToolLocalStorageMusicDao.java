package com.quyang.voice.dao;

import com.quyang.voice.model.ToolLocalStorageMusic;
import com.quyang.voice.model.ToolLocalStorageMusicExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToolLocalStorageMusicDao {
    long countByExample(ToolLocalStorageMusicExample example);

    int deleteByExample(ToolLocalStorageMusicExample example);

    int deleteByPrimaryKey(Long storageId);

    int insert(ToolLocalStorageMusic record);

    int insertSelective(ToolLocalStorageMusic record);

    List<ToolLocalStorageMusic> selectByExample(ToolLocalStorageMusicExample example);

    ToolLocalStorageMusic selectByPrimaryKey(Long storageId);

    int updateByExampleSelective(@Param("record") ToolLocalStorageMusic record, @Param("example") ToolLocalStorageMusicExample example);

    int updateByExample(@Param("record") ToolLocalStorageMusic record, @Param("example") ToolLocalStorageMusicExample example);

    int updateByPrimaryKeySelective(ToolLocalStorageMusic record);

    int updateByPrimaryKey(ToolLocalStorageMusic record);
}
