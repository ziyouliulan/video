
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.Dynamic;
import com.quyang.voice.model.Publishs;
import com.quyang.voice.model.UserBlack;
import com.quyang.voice.model.dto.ShortVideoDto;
import com.quyang.voice.model.vo.PublishCommentsReplyVo;
import com.quyang.voice.model.vo.PublishCommentsVo;
import com.quyang.voice.model.vo.PublishsListVo;
import com.quyang.voice.model.vo.PublishsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Mapper
@Repository
public interface PublishsMapper extends BaseMapper<Publishs> {

    PublishsVo selectPublishInfo(Long id);

    Integer selectCommentNum(Long id);

    Integer selectGoodNum(Long id);

    List<PublishsVo> getPublishsListByUserId(
            @Param("noSeeUserIds") List<Dynamic> noSeeUserIds, @Param("noSeeList") List<Dynamic> noSeeUserId,
            @Param("userBlackList1") List<UserBlack> userBlackList1 , @Param("userBlackList2") List<UserBlack> userBlackList2,
            @Param("userId") Integer checkUserId, @Param("types") ArrayList<Integer> types, @Param("noSeeStatus") Integer noSeeStatus);



    List<PublishsVo> getPublishsList(
            @Param("noSeeUserIds") List<Dynamic> noSeeUserIds, @Param("noSeeList") List<Dynamic> noSeeUserId,
            @Param("userBlackList1") List<UserBlack> userBlackList1 , @Param("userBlackList2") List<UserBlack> userBlackList2,
            @Param("userId") Integer checkUserId, @Param("types") ArrayList<Integer> types, @Param("toUserId") Integer toUserId);



    List<PublishsVo> getPublishsOtherList(
            @Param("noSeeUserIds") List<Dynamic> noSeeUserIds, @Param("noSeeList") List<Dynamic> noSeeUserId,
            @Param("userBlackList1") List<UserBlack> userBlackList1 , @Param("userBlackList2") List<UserBlack> userBlackList2,
            @Param("userId") Integer checkUserId, @Param("types") ArrayList<Integer> types, @Param("toUserId") Integer toUserId,String regionCode);


    List<PublishsVo> getPublishsOtherLists(
            @Param("types") ArrayList<Integer> types,@Param("shortVideoDto") ShortVideoDto shortVideoDto);


    List<PublishsVo> getPublishsOtherLiskLists(
            @Param("types") ArrayList<Integer> types,@Param("shortVideoDto") ShortVideoDto shortVideoDto);


    /**
     * 查找指定账户
     * @param userId
     * @return
     */
    List<PublishsVo> getPublishsListByUser(@Param("UserId") Integer userId);

    List<PublishCommentsVo> queryPublishComments(Long id);

    List<PublishCommentsReplyVo> queryPublishCommentsReply(Long id);

    List<PublishsListVo> getMyPublishList(Long userId,Integer noSeeStatus);

    List<PublishsListVo> getMyShortVideoList(Long userId);
    List<PublishsListVo> getOtherShortVideoList(Long userId);

    List<PublishsListVo> getMyPublishList1(Long userId);

    /**
     * 查询该用户对该动态是否点了赞
     * @param userId
     * @param publishId
     * @param likedUserId
     * @return
     */
    List<Long> getPublishsLike(Integer userId, Long publishId, Long likedUserId);

    /**
     * 查询该用户对该动态是否点了赞
     * @param userId
     * @param publishId
     * @param likedUserId
     * @return
     */
    Boolean getPublishsLikes(@Param("userId") Integer userId,@Param("publishId") Long publishId);

    /**
     * 查询获赞数
     * @param userId
     * @return
     */
    Integer getPraise(Integer userId);

    List<Publishs> getLikes(Integer userId);




}
