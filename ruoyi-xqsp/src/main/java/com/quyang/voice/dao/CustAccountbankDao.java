package com.quyang.voice.dao;

import com.quyang.voice.model.CustAccountbank;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface CustAccountbankDao {
    int deleteByPrimaryKey(Long id);

    int insert(CustAccountbank record);

    int insertSelective(CustAccountbank record);

    CustAccountbank selectByPrimaryKey(Long id);


    List<CustAccountbank> selectByUserId(Integer id);

    int updateByPrimaryKeySelective(CustAccountbank record);

    int updateByPrimaryKey(CustAccountbank record);
}