package com.quyang.voice.dao;

import com.quyang.voice.model.EvaluateLabel;
import com.quyang.voice.model.EvaluateLabelExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvaluateLabelDao {
    long countByExample(EvaluateLabelExample example);

    int deleteByExample(EvaluateLabelExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EvaluateLabel record);

    int insertSelective(EvaluateLabel record);

    List<EvaluateLabel> selectByExample(EvaluateLabelExample example);

    EvaluateLabel selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EvaluateLabel record, @Param("example") EvaluateLabelExample example);

    int updateByExample(@Param("record") EvaluateLabel record, @Param("example") EvaluateLabelExample example);

    int updateByPrimaryKeySelective(EvaluateLabel record);

    int updateByPrimaryKey(EvaluateLabel record);
}
