package com.quyang.voice.dao;

import com.quyang.voice.model.DoctorComments;
import com.quyang.voice.model.DoctorCommentsExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorCommentsDao {
    long countByExample(DoctorCommentsExample example);

    int deleteByExample(DoctorCommentsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(DoctorComments record);

    int insertSelective(DoctorComments record);

    List<DoctorComments> selectByExample(DoctorCommentsExample example);

    DoctorComments selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") DoctorComments record, @Param("example") DoctorCommentsExample example);

    int updateByExample(@Param("record") DoctorComments record, @Param("example") DoctorCommentsExample example);

    int updateByPrimaryKeySelective(DoctorComments record);

    int updateByPrimaryKey(DoctorComments record);
}
