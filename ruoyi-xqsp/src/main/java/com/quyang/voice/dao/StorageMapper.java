package com.quyang.voice.dao;

import com.quyang.voice.model.TStorage;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface StorageMapper {

    @Select("select * from storage where file_name = #{key}")
    TStorage findByKey(String key);

    @Insert("insert into storage (file_name, name," +
            "      type, size, url," +
            "      add_time, update_time) " +
            "values (#{fileName}, #{name}," +
            "      #{type}, #{size}, #{url}," +
            "      #{addTime}, #{updateTime})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    int add(TStorage tStorage);

    @Delete("delete from storage where file_name=#{key}")
    int delete(String key);

    @Select("select * from storage where file_name = #{fileName}")
    TStorage downloadFileByName(String fileName);

    @Select("select * from storage where id = #{id}")
    TStorage downloadById(Long id);
}
