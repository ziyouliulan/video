
package com.quyang.voice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quyang.voice.model.PublishComments;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PublishCommentsMapper extends BaseMapper<PublishComments> {

    String getUserAvatar(Long userId);

    /**
     * 查询指定天数内某动态的评论数量
     * @return
     * @param publishId
     * @param day
     */
    Integer countCommentByTime(@Param("publishId")Long publishId, @Param("day")Integer day);


    /**
     * 获得动态下的评论列表
     * @param publishId
     * @return
     */
    List<PublishComments> getPublishCommentsMapper(Long publishId);
}
