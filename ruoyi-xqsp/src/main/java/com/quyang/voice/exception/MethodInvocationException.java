package com.quyang.voice.exception;

import com.quyang.voice.utils.Constants;
import com.quyang.voice.utils.ResponseUtil;

public class MethodInvocationException extends  BaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1895439802072919903L;

	public MethodInvocationException(String handler, String method) {
        super(handler + "/" +method);
        String[] messages = Constants.ConstError.METHOD_INVOKE_EXCEPTION.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1])+",handler:"+handler+",method:"+method);

        
    }
}
