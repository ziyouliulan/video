package com.quyang.voice.exception;


import com.quyang.voice.utils.Constants;
import com.quyang.voice.utils.ResponseUtil;

public class HttpServletParamtersNotMatchException extends BaseException {

	private static final long serialVersionUID = -5101311233548923012L;

	public HttpServletParamtersNotMatchException() {
        super("HttpServlet参数未找到.");
        String[] messages = Constants.ConstError.HTTP_SERVLET_PARAMTERS_NOT_MATCH.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1]));
        
    }

}
