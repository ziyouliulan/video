package com.quyang.voice.exception;


import com.quyang.voice.utils.ResponseUtil;

public class MessageCodeException  extends BaseException {


	private static final long serialVersionUID = -7118805686640085616L;

    public MessageCodeException(String message) {
        super(message + "未找到.");
        String[] messages = message.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1]));

    }


}