package com.quyang.voice.exception;

import com.quyang.voice.utils.Constants;
import com.quyang.voice.utils.ResponseUtil;

public class EntityNotFoundException extends BaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6075305586444176225L;

	public EntityNotFoundException(String entity) {
        super(entity + "未找到.");
        String[] messages = Constants.ConstError.ENTITY_NOT_FOUND.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1], entity));
    }
}
