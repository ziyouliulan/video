package com.quyang.voice.exception;

import com.quyang.voice.utils.Constants;
import com.quyang.voice.utils.ResponseUtil;

public class HandlerNotFoundException extends BaseException {


    /**
	 * 
	 */
	private static final long serialVersionUID = -7618805686640085656L;

    public HandlerNotFoundException(String handler) {
        super(handler);
        String[] messages = Constants.ConstError.HANDLER_NOT_FOUND.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1])+",handler:"+handler);

        
    }


}
