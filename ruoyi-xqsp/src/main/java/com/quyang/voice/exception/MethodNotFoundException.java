package com.quyang.voice.exception;

import com.quyang.voice.utils.Constants;
import com.quyang.voice.utils.ResponseUtil;

public class MethodNotFoundException extends  BaseException {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1468348354506855700L;

	public MethodNotFoundException(String handlerName, String actionName) {
        super(handlerName+"/"+actionName+"/未找到");
        String[] messages = Constants.ConstError.METHOD_NOT_FOUND.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1])+",handlerName:"+handlerName+",actionName:"+actionName);

    }


}
