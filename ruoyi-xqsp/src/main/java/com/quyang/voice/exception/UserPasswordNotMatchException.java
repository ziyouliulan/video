package com.quyang.voice.exception;

/**
 * 用户密码不正确或不符合规范异常类
 *
 * @author quyang
 */
public class UserPasswordNotMatchException extends Exception {
    private static final long serialVersionUID = 1L;

    public UserPasswordNotMatchException(String message) {
        super(message);
    }
}
