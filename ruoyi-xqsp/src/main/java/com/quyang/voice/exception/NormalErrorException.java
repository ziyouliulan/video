package com.quyang.voice.exception;


import com.quyang.voice.utils.ResponseUtil;

public class NormalErrorException extends  BaseException {


    private static final long serialVersionUID = -1625512040584809496L;

    public NormalErrorException(String message) {
        super(message);
        String[] messages = message.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),messages[1]);
    }


}
