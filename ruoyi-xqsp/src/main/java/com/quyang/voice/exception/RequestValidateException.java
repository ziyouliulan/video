package com.quyang.voice.exception;


import com.quyang.voice.utils.ResponseUtil;

/**
 * @author Zhaoyicong
 * @Description
 * @date 2015/10/23
 * @time 10:40
 * Copyright © 2015 DealingMatrix.cn. All Rights Reserved.
 */
public class RequestValidateException extends BaseException {


    private static final long serialVersionUID = 507385609088207925L;

    public RequestValidateException(String msg) {
        super(msg);
        String[] messages = msg.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1]));

    }
}
