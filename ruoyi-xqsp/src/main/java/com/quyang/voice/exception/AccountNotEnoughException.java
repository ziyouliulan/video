package com.quyang.voice.exception;

/**
 * @author Zhaoyicong
 * @Description
 * @date 2015/4/29
 * @time 11:37
 * Copyright © 2014-2015 iYouou.com. All Rights Reserved.
 */
public class AccountNotEnoughException extends Exception {

    private static final long serialVersionUID = -1L;

    public AccountNotEnoughException(String message) {
        super(message);
    }


}
