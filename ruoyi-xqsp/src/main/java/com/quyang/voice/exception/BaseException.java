package com.quyang.voice.exception;


import com.quyang.voice.utils.ResponseUtil;

public abstract class BaseException extends Exception {


    /**
	 * 
	 */
	private static final long serialVersionUID = -6665005104159595734L;
	protected ResponseUtil errorMsg;
    private String handlerName;
    private String actionName;

    public BaseException(String msg) {
        super(msg);
    }

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public ResponseUtil getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(ResponseUtil errorMsg) {
        this.errorMsg = errorMsg;
    }


}
