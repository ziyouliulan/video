package com.quyang.voice.exception;

import com.quyang.voice.utils.Constants;
import com.quyang.voice.utils.ResponseUtil;

public class HttpServletParamIsNullException extends BaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6152650242430603657L;

	public HttpServletParamIsNullException(String parameterName) {
        super(parameterName+"为空.");
        String[] messages = Constants.ConstError.HTTP_SERVLET_PARAMTERS_IS_NULL.split(",");
        ResponseUtil.fail(Integer.valueOf(messages[0]),String.format(messages[1])+",parameterName:"+parameterName);

        
    }

}
