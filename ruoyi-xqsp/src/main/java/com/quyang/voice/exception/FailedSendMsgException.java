package com.quyang.voice.exception;

public class FailedSendMsgException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1568406172676918926L;

	public FailedSendMsgException(String message) {
        super(message);
    }

}
