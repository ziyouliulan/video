package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.Curriculum;

/**
 *
 */
public interface CurriculumService extends IService<Curriculum> {

}
