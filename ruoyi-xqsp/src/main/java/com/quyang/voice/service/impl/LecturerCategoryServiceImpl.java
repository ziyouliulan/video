package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.LecturerCategoryMapper;
import com.quyang.voice.model.LecturerCategory;
import com.quyang.voice.service.LecturerCategoryService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class LecturerCategoryServiceImpl extends ServiceImpl<LecturerCategoryMapper, LecturerCategory>
    implements LecturerCategoryService{

}




