package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.OfflineActivities;

/**
 *
 */
public interface OfflineActivitiesService extends IService<OfflineActivities> {

}
