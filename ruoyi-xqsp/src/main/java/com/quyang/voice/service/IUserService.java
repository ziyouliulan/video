
package com.quyang.voice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.Users;
import com.quyang.voice.model.vo.MissuUsersBaseVo;
import com.quyang.voice.model.vo.UsersVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

import java.util.List;

public interface IUserService extends IService<Users> {
    ResponseUtil<UsersVo> getUsersById(Long userId,Long toUserId);

    ReturnJson updateUsers(Users user);

    //换绑微信
    ReturnJson updateUserWx(Long userId,String phone,String vCode,String openId);
    //解绑微信
    ReturnJson removeUserWx(Long userId,String phone,String vCode);

    ReturnJson updPwd(Long id, String oldPwd, String newPwd);

    ReturnJson changePhone(Long id, String phone, String vCode);
    ResponseUtil<IPage<Users>> getUsersPages(Integer pageNum,Integer pageSize);
    //首页搜索用户
//    ResponseUtil<PageInfo<IndexSearchUsersVo>> searchUser(Integer pageNum, Integer pageSize,Long userId, String userName);

    ReturnJson forgetPwd(String phone, String newPwd, String vCode);

    ReturnJson inviteUser(Long id, String inviteCode);

    //黑名单--------------------------------------
    //拉黑/取消拉黑用户
    ReturnJson  blackUser(Integer userId,Integer blackUserId,Integer type);    //拉黑/取消拉黑用户

//    查询单个用户黑名单
    ResponseUtil  blackUserDetail(Integer userId,Integer blackUserId);

    //我的黑名单列表
    ResponseUtil<List<MissuUsersBaseVo>> getMyBlackUser(Integer userId);

    //拉黑我的人列表
    ResponseUtil<List<MissuUsersBaseVo>> getBlackUser(Integer userId);


    //阅后即焚 ------------------------------------------
    //拉黑/取消拉黑用户  burnAfterReading
    ReturnJson  burnAfterReadingUser(Integer userId,Integer blackUserId,Integer type);    //拉黑/取消拉黑用户

    //    查询单个用户黑名单
    ResponseUtil  burnAfterReadingUserDetail(Integer userId,Integer blackUserId);

    //我的黑名单列表
    ResponseUtil<List<MissuUsersBaseVo>> getMyBurnAfterReadingUser(Integer userId,Integer burnAfterReadingId);

    //拉黑我的人列表
    ResponseUtil<List<MissuUsersBaseVo>> getBurnAfterReadingUser(Integer userId,Integer burnAfterReadingId);






    //朋友圈-------------------------------------

    //拉黑/取消拉黑用户
    ReturnJson  dynamic(Integer userId,String blackUserId,Integer type,Integer dynamicType);    //拉黑/取消拉黑用户

    //    查询单个用户黑名单
    ResponseUtil  dynamicDetail(Integer userId,Integer blackUserId,Integer dynamicType);

    //我的黑名单列表
    ResponseUtil <List<MissuUsersBaseVo>> getMyDynamic(Integer userId,Integer dynamicType);

    //拉黑我的人列表
    ResponseUtil <List<MissuUsersBaseVo>> getDynamicUser(Integer userId,Integer dynamicType);

}
