package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.LecturerCategory;

/**
 *
 */
public interface LecturerCategoryService extends IService<LecturerCategory> {

}
