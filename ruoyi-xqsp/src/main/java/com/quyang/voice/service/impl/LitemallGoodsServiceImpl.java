package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.LitemallGoodsMapper;
import com.quyang.voice.model.LitemallGoods;
import com.quyang.voice.service.LitemallGoodsService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class LitemallGoodsServiceImpl extends ServiceImpl<LitemallGoodsMapper, LitemallGoods>
    implements LitemallGoodsService{

}




