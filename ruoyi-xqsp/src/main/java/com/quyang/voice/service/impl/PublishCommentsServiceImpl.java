
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.PublishCommentsMapper;
import com.quyang.voice.dao.PublishCommentsReplyMapper;
import com.quyang.voice.dao.UsersMapper;
import com.quyang.voice.model.PublishComments;
import com.quyang.voice.model.PublishCommentsReply;
import com.quyang.voice.service.IPublishCommentsReplyService;
import com.quyang.voice.service.IPublishCommentsService;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import com.quyang.voice.utils.SafeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("publishCommentsService")
public class PublishCommentsServiceImpl extends ServiceImpl<PublishCommentsMapper, PublishComments> implements IPublishCommentsService {

    private static final Logger logger = LoggerFactory.getLogger(PublishCommentsServiceImpl.class);

    @Autowired
    IPublishCommentsService publishCommentsService;


    @Autowired
    PublishCommentsMapper publishCommentsMapper;

    @Autowired
    IPublishCommentsReplyService publishCommentsReplyService;

    @Autowired
    PublishCommentsReplyMapper publishCommentsReplyMapper;


    @Autowired
    UsersMapper usersMapper;


    @Override
    public ReturnJson deletePublishCommentsById(Long id){
        try {
            boolean rsg = publishCommentsService.removeById(id);
            QueryWrapper<PublishCommentsReply> queryWrapper =new QueryWrapper<PublishCommentsReply>();
            queryWrapper.eq("comments_id",id);
            publishCommentsReplyMapper.delete(queryWrapper);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("OrganizesServiceImpl.deleteOrganizesById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public boolean deletePublishComments(Long id) {
        try {
            boolean rsg = publishCommentsService.removeById(id);
            publishCommentsReplyService.deletePublishCommentsReplyByCommentId(id);
            if (rsg) {
                return true;
            }else {
                return false;
            }
        }catch (Exception e){
            logger.error("OrganizesServiceImpl.deleteOrganizesById", e);
            return false;
        }
    }

    @Override
    public ReturnJson insertPublishComments(PublishComments publishComments){
        try {
            if (null != publishComments) {
                if(publishComments.getCommentPic() != null && publishComments.getCommentPic().length() != 0){
                    String picName = publishComments.getCommentPic().split("/")[publishComments.getCommentPic().split("/").length-1];
                    publishComments.setCommentPicName(picName);
                }
                String userAvatar = publishCommentsMapper.getUserAvatar(publishComments.getUserId());
                publishComments.setUserAvatar(userAvatar);
                publishComments.setCreateTime(SafeUtils.getNowDate());

                boolean rsg = publishCommentsService.save(publishComments);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("OrganizesServiceImpl.insertOrganizes", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

}
