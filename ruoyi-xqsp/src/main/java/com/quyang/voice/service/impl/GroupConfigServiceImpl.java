package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.GroupConfigMapper;
import com.quyang.voice.model.GroupConfig;
import com.quyang.voice.service.GroupConfigService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
* @author lpden
* @description 针对表【group_config(参数配置表)】的数据库操作Service实现
* @createDate 2022-04-19 14:56:06
*/
@Service
public class GroupConfigServiceImpl extends ServiceImpl<GroupConfigMapper, GroupConfig>
    implements GroupConfigService{

    @Override
    public HashMap configToMap() {
        List<GroupConfig> list = list(null);
        HashMap map = new HashMap();
        for (GroupConfig groupConfig: list
             ) {

            map.put(groupConfig.getConfigKey(),groupConfig.getConfigValue());

        }
        return map;
    }

}




