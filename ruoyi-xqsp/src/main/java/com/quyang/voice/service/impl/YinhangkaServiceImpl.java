package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.YinhangkaMapper;
import com.quyang.voice.model.Yinhangka;
import com.quyang.voice.service.YinhangkaService;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【yuecool_yinhangka】的数据库操作Service实现
* @createDate 2022-01-07 15:47:01
*/
@Service
public class YinhangkaServiceImpl extends ServiceImpl<YinhangkaMapper, Yinhangka>
    implements YinhangkaService{

}




