
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.PublishComments;
import com.quyang.voice.utils.ReturnJson;

public interface IPublishCommentsService extends IService<PublishComments> {

    ReturnJson deletePublishCommentsById(Long id);

    boolean deletePublishComments(Long id);

    ReturnJson insertPublishComments(PublishComments publishComments);

}