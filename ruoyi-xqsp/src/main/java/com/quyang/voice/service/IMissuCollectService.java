
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.MissuCollect;
import com.quyang.voice.model.vo.MissuCollectVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IMissuCollectService extends IService<MissuCollect> {
    ResponseUtil<MissuCollectVo> getMissuCollectById(Long id);

    ReturnJson deleteMissuCollectById(Long id);

    ReturnJson insertMissuCollect(MissuCollect missuCollect);

    ReturnJson updateMissuCollect(MissuCollect missuCollect);

    /**
     * 查询收藏消息列表
     * @param userId
     * @return
     */
    ResponseUtil<MissuCollectVo> getMissuColletList(Integer userId);
}
