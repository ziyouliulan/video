package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.GroupUpgrade;

/**
 *
 */
public interface GroupUpgradeService extends IService<GroupUpgrade> {

}
