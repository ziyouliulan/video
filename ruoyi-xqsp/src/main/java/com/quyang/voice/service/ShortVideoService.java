
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.quyang.voice.model.PublishComments;
import com.quyang.voice.model.PublishCommentsReply;
import com.quyang.voice.model.Publishs;
import com.quyang.voice.model.PublishsLike;
import com.quyang.voice.model.dto.ShortVideoDto;
import com.quyang.voice.model.vo.PublishCommentsVo;
import com.quyang.voice.model.vo.PublishInfoVo;
import com.quyang.voice.model.vo.PublishsListVo;
import com.quyang.voice.model.vo.PublishsVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public interface ShortVideoService extends IService<Publishs> {
    ResponseUtil<PublishInfoVo> getPublishsById(Long id, Integer checkUserId);

    ReturnJson deletePublishsById(String ids);

    ReturnJson insertPublishs(Publishs publishs);

    ReturnJson postPublishsLike(PublishsLike publishsLike);

    ReturnJson insertPublishComments(PublishComments publishComments);

    ReturnJson insertPublishCommentsReply(PublishCommentsReply publishCommentsReply,Long publishId);

    ReturnJson deletePublishCommentsReplyById(Long id);

    ReturnJson deletePublishCommentsById(Long id);

    ResponseUtil<PageInfo<PublishsVo>> getPublishsList(Integer checkUserId,Integer pageNum, Integer pageSize);
    ResponseUtil<PageInfo<PublishsVo>> getPublishsList(Integer checkUserId,Integer pageNum, Integer pageSize,String regionCode);
    ResponseUtil<PageInfo<PublishsVo>> getPublishsList(ShortVideoDto shortVideoDto);

    ResponseUtil<PageInfo<PublishCommentsVo>> queryPublishComments(Integer pageNum, Integer pageSize,Long id);

    ResponseUtil<PageInfo<PublishsListVo>> getMyPublishList(Integer pageNum, Integer pageSize, Long userId);
    ResponseUtil<PageInfo<PublishsListVo>> getOtherPublishList(Integer pageNum, Integer pageSize, Long userId);

    /**
     * 查询用户发布的动态
     * @param pageNum
     * @param pageSize
     * @param userId
     * @return
     */
    ResponseUtil<PageInfo<PublishsVo>> getPublishsByUser(Integer pageNum, Integer pageSize, Integer userId,Integer toUserId);

    /**
     * TODO 查询用户短视频获取的点赞数以及详情
     * @Author:lipeng
     * @Date:2021/9/8 18:00
     * @param: userId
     * @return:com.quyang.voice.utils.ResponseUtil<java.util.HashMap>
     */
    public ResponseUtil<Map> queryPublishDetails(Integer userId);
}
