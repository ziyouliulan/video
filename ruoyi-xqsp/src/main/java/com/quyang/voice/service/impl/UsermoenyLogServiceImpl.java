
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.UsermoenyLogMapper;
import com.quyang.voice.manage.IUsermoenyLogManage;
import com.quyang.voice.model.UsermoenyLog;
import com.quyang.voice.service.IUsermoenyLogService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service("usermoenyLogService")
public class UsermoenyLogServiceImpl extends ServiceImpl<UsermoenyLogMapper, UsermoenyLog> implements IUsermoenyLogService {

    private static final Logger logger = LoggerFactory.getLogger(UsermoenyLogServiceImpl.class);

    @Autowired
    IUsermoenyLogService usermoenyLogService;

    @Autowired
    IUsermoenyLogManage usermoenyLogManage;

    @Autowired
    UsermoenyLogMapper usermoenyLogMapper;

    @Override
    public ResponseUtil<UsermoenyLog> getUsermoenyLogById(@PathVariable("id")Long id){
        try {
            UsermoenyLog obj=usermoenyLogService.getById(id);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("UsermoenyLogServiceImpl.getUsermoenyLogById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteUsermoenyLogById(Long id){
        try {
            boolean rsg = usermoenyLogService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("UsermoenyLogServiceImpl.deleteUsermoenyLogById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertUsermoenyLog(UsermoenyLog usermoenyLog){
        try {
            if (null != usermoenyLog) {
                boolean rsg = usermoenyLogService.save(usermoenyLog);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("UsermoenyLogServiceImpl.insertUsermoenyLog", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateUsermoenyLog(UsermoenyLog usermoenyLog){
        try {
            if (null != usermoenyLog) {
                boolean rsg = usermoenyLogService.updateById(usermoenyLog);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("UsermoenyLogServiceImpl.updateUsermoenyLog", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
