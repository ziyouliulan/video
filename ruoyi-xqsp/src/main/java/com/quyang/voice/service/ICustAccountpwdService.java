
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.CustAccountpwd;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface ICustAccountpwdService extends IService<CustAccountpwd> {
    ResponseUtil<CustAccountpwd> getCustAccountpwdById(Long id);

    ReturnJson deleteCustAccountpwdById(Long id);

    ReturnJson insertCustAccountpwd(CustAccountpwd cust_accountpwd);

    ReturnJson updateCustAccountpwd(CustAccountpwd cust_accountpwd);
}
