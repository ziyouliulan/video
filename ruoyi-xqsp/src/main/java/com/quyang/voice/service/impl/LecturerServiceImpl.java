package com.quyang.voice.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.LecturerMapper;
import com.quyang.voice.model.Lecturer;
import com.quyang.voice.model.LecturerCategory;
import com.quyang.voice.model.dto.LecturerDto;
import com.quyang.voice.service.LecturerCategoryService;
import com.quyang.voice.service.LecturerService;
import com.quyang.voice.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class LecturerServiceImpl extends ServiceImpl<LecturerMapper, Lecturer>
    implements LecturerService{


    @Autowired
    LecturerCategoryService lecturerCategoryService;
    @Override
    public ResponseUtil getLecturer(Integer userId) {

        QueryWrapper<Lecturer> wrapper = new QueryWrapper();
        wrapper.eq("user_id",userId);

        Lecturer lecturer = getOne(wrapper);
        if (lecturer == null){
            return ResponseUtil.suc();
        }
        LecturerCategory byId = lecturerCategoryService.getById(lecturer.getDeptId());
        if (byId !=null){
            lecturer.setDeptName(byId.getName());
        }

        return ResponseUtil.suc(lecturer);

    }

    @Override
    public ResponseUtil getLecturers(LecturerDto lecturerDto) {

        QueryWrapper<Lecturer> wrapper = new QueryWrapper();
        wrapper.eq("status","3");

        if (!StrUtil.isEmptyIfStr(lecturerDto.getCategoryId()) && lecturerDto.getCategoryId() != 0){
            wrapper.eq("dept_id",lecturerDto.getCategoryId());
        }

        if (!StrUtil.isEmptyIfStr(lecturerDto.getName() )){
            wrapper.like("name",lecturerDto.getName());
        }


        if (!StrUtil.isEmptyIfStr(lecturerDto.getIsRecommend() )){
            wrapper.eq("is_recommend",lecturerDto.getIsRecommend());
        }


        if (!StrUtil.isEmptyIfStr(lecturerDto.getUserId() )){
            wrapper.like("user_id",lecturerDto.getUserId());
        }


        wrapper.orderByDesc("create_time");
        List<Lecturer> lecturers = list(wrapper);
        return ResponseUtil.suc(lecturers);

    }


}




