
package com.quyang.voice.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quyang.voice.dao.ConsumeRecordMapper;
import com.quyang.voice.manage.IConsumeRecordManage;
import com.quyang.voice.model.ConsumeRecord;
import com.quyang.voice.model.vo.ConsumRecordIndexVo;
import com.quyang.voice.model.vo.ConsumeRecordSimpleVo;
import com.quyang.voice.service.IConsumeRecordService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;

@Service("consume_recordService")
public class ConsumeRecordServiceImpl extends ServiceImpl<ConsumeRecordMapper, ConsumeRecord> implements IConsumeRecordService {

    private static final Logger logger = LoggerFactory.getLogger(ConsumeRecordServiceImpl.class);

    @Autowired
    IConsumeRecordService consumeRecordService;

    @Autowired
    IConsumeRecordManage consumeRecordManage;

    @Autowired
    ConsumeRecordMapper consumeRecordMapper;

    @Override
    public ResponseUtil<ConsumeRecord> getConsumeRecordById(@PathVariable("id")Long id){
        try {
            ConsumeRecord obj=consumeRecordService.getById(id);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("ConsumeRecordServiceImpl.getConsumeRecordById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteConsumeRecordById(Long id){
        try {
            boolean rsg = consumeRecordService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("ConsumeRecordServiceImpl.deleteConsumeRecordById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertConsumeRecord(ConsumeRecord consume_record){
        try {
            if (null != consume_record) {
                boolean rsg = consumeRecordService.save(consume_record);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("ConsumeRecordServiceImpl.insertConsumeRecord", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateConsumeRecord(ConsumeRecord consume_record){
        try {
            if (null != consume_record) {
                boolean rsg = consumeRecordService.updateById(consume_record);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("ConsumeRecordServiceImpl.updateConsumeRecord", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<ConsumeRecord> getConsumeRecordByUserId(Integer userId, Integer type, Integer changeType,Integer pageNum,Integer pageSize) {
        QueryWrapper<ConsumeRecord> consumeRecordQueryWrapper = new QueryWrapper<>();
        consumeRecordQueryWrapper.eq("user_id",userId);

        if (changeType != null) {
            consumeRecordQueryWrapper.eq("change_type", changeType);
        }
        consumeRecordQueryWrapper.in("type","1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,20,29,31,41,43,48,50");
        consumeRecordQueryWrapper.orderByDesc("time");
        PageHelper.startPage(pageNum, pageSize);
        List<ConsumeRecord> consumeRecordList = consumeRecordService.list(consumeRecordQueryWrapper);
        PageInfo<ConsumeRecord> pageInfo = new PageInfo<>(consumeRecordList);
        return ResponseUtil.suc(pageInfo);
    }

    @Override
    public ResponseUtil<ConsumRecordIndexVo> getConsumRecordList(Integer userId, Integer type, Integer year, Integer month,Integer pageNum,Integer pageSize) {

        ConsumRecordIndexVo vo = new ConsumRecordIndexVo();

        try {
            if (year == null || month == null){
                year = DateUtil.year(new Date());
                month = DateUtil.month(new Date());
                PageHelper.startPage(pageNum, pageSize);
                List<ConsumeRecordSimpleVo> consumeRecordSimpleVoList =  consumeRecordMapper.getConsumRecord(userId,type,null,year,month);
                vo.setInCharge(consumeRecordMapper.getCharge(userId,null,year,month,1));
                vo.setOutCharge(consumeRecordMapper.getCharge(userId,null,year,month,2));
                vo.setConsumeRecordSimpleVoList(consumeRecordSimpleVoList);
                return ResponseUtil.suc(vo);
            }else {
                vo.setInCharge(consumeRecordMapper.getCharge(userId,null, year,month,1));
                vo.setOutCharge(consumeRecordMapper.getCharge(userId,null,year,month,2));
                PageHelper.startPage(pageNum, pageSize);
                List<ConsumeRecordSimpleVo> consumeRecordSimpleVoList =  consumeRecordMapper.getConsumRecord(userId,type,null,year,month);
                vo.setConsumeRecordSimpleVoList(consumeRecordSimpleVoList);
                return ResponseUtil.suc(vo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<ConsumRecordIndexVo> getConsumRecordList(Integer userId, Integer type,Integer gId, Integer year, Integer month,Integer pageNum,Integer pageSize) {

        ConsumRecordIndexVo vo = new ConsumRecordIndexVo();

        try {
            if (year == null || month == null){
                year = DateUtil.year(new Date());
                month = DateUtil.month(new Date());
                PageHelper.startPage(pageNum, pageSize);
                List<ConsumeRecordSimpleVo> consumeRecordSimpleVoList =  consumeRecordMapper.getConsumRecord(userId,type,gId,year,month);
                vo.setInCharge(consumeRecordMapper.getCharge(userId,year,month,1,gId));
                vo.setOutCharge(consumeRecordMapper.getCharge(userId,year,month,2,gId));
                vo.setConsumeRecordSimpleVoList(consumeRecordSimpleVoList);
                return ResponseUtil.suc(vo);
            }else {
                vo.setInCharge(consumeRecordMapper.getCharge(userId,year,month,1,gId));
                vo.setOutCharge(consumeRecordMapper.getCharge(userId,year,month,2,gId));
                PageHelper.startPage(pageNum, pageSize);
                List<ConsumeRecordSimpleVo> consumeRecordSimpleVoList =  consumeRecordMapper.getConsumRecord(userId,type,gId,year,month);
                vo.setConsumeRecordSimpleVoList(consumeRecordSimpleVoList);
                return ResponseUtil.suc(vo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

//    @Override
//    public ResponseUtil<ConsumeRecordDetailVo> getConsumeRecord(String tradeNo) {
//
//        ConsumeRecord consumeRecord = consumeRecordService.getById(tradeNo);
//
//        ConsumeRecordDetailVo consumeRecordDetailVo = new ConsumeRecordDetailVo();
//
//
//
//
//
//        return null;
//    }



}
