package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.CourseDetails;
import com.quyang.voice.model.dto.CourseDetailsDto;
import com.quyang.voice.utils.ResponseUtil;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
public interface CourseDetailsService extends IService<CourseDetails> {

    public List<CourseDetails> selectByDept(HashMap map);


    public CourseDetailsDto getCurriculumDetails(Integer userId, Integer id);



    public ResponseUtil purchaseCourse(Integer userId, Integer id);


    public ResponseUtil learningRecords(Integer userId);



}
