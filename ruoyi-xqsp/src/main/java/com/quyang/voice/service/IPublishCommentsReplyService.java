
package com.quyang.voice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.PublishCommentsReply;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

import java.util.List;

public interface IPublishCommentsReplyService extends IService<PublishCommentsReply> {
    ResponseUtil<PublishCommentsReply> getPublishCommentsReplyById(Long id);

    ReturnJson deletePublishCommentsReplyById(Long id);

    ReturnJson insertPublishCommentsReply(PublishCommentsReply publishCommentsReply);

    ReturnJson updatePublishCommentsReply(PublishCommentsReply publishCommentsReply);

    /**
     * 根据首层评论查询下级回复
     * @param commentId
     * @return
     */
    List<PublishCommentsReply>   getPublishCommentsReplyByCommentId(Long commentId,Integer userId);

    /**
     * 根据首层回复id删除下级所有的回复
     * @param commentId
     * @return
     */
    Boolean deletePublishCommentsReplyByCommentId(Long commentId);

    ResponseUtil<IPage<PublishCommentsReply>> getPublishCommentsReplyPages(Integer pageNum,Integer pageSize);
}
