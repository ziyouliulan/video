
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.PublishsLikeMapper;
import com.quyang.voice.dao.UsersMapper;
import com.quyang.voice.model.PublishsLike;
import com.quyang.voice.service.IPublishsLikeService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;

import static com.quyang.voice.utils.ResultCode.SERVE_ERROR;


@Service("publishsLikeService")
public class PublishsLikeServiceImpl extends ServiceImpl<PublishsLikeMapper, PublishsLike> implements IPublishsLikeService {

    private static final Logger logger = LoggerFactory.getLogger(PublishsLikeServiceImpl.class);

    @Autowired
    IPublishsLikeService publishsLikeService;


    @Autowired
    PublishsLikeMapper publishsLikeMapper;


    @Autowired
    UsersMapper usersMapper;

    @Override
    public ResponseUtil<PublishsLike> getPublishsLikeById(@PathVariable("id")Long id){
        PublishsLike obj=publishsLikeService.getById(id);
        if (null != obj ) {
           return ResponseUtil.suc(obj);
        }else {
           return ResponseUtil.fail(1,"查询对象不存在！");
       }
    }

    @Override
    public ReturnJson deletePublishsLikeById(Long id){
        boolean rsg = publishsLikeService.removeById(id);
        if (rsg) {
            return ReturnJson.suc("删除成功");
        }else {
            return ReturnJson.fail("删除失败");
        }
    }

    @Override
    public ReturnJson insertPublishsLike(PublishsLike publishsLike){
        if (null != publishsLike) {
            publishsLike.setCreateTime(new Date());
            boolean rsg = publishsLikeService.save(publishsLike);
            if (rsg) {
                return ReturnJson.suc("添加成功");
            }else {
                return ReturnJson.fail("添加失败");
            }
        }else {
            return ReturnJson.fail("请输入正确参数");
        }
    }

    @Override
    public ReturnJson updatePublishsLike(PublishsLike publishsLike){
        if (null != publishsLike) {
            boolean rsg = publishsLikeService.updateById(publishsLike);
            if (rsg) {
                return ReturnJson.suc("修改成功");
            }else {
                return ReturnJson.fail("修改失败");
            }
        }else {
            return ReturnJson.fail("请输入正确参数");
        }
    }

    @Override
    public ResponseUtil postPublishsLike(PublishsLike publishsLike) {
        try {
            //点赞状态，true为是，false为否
            QueryWrapper<PublishsLike> queryWrapper = new QueryWrapper<PublishsLike>();
            queryWrapper.eq("publish_id",publishsLike.getPublishId());
            queryWrapper.eq("liked_post_id",publishsLike.getLikedPostId());
            PublishsLike thePublishsLike = publishsLikeMapper.selectOne(queryWrapper);
            //当数据表中有它的点赞数据时候
            if (thePublishsLike != null){
                //清除掉之前点赞的信息
                publishsLikeMapper.delete(queryWrapper);
                return ResponseUtil.suc(false);
            }
            //当数据库中不存在时候
            else{
                publishsLike.setStatus(1);
                publishsLike.setCreateTime(new Date());
                publishsLikeMapper.insert(publishsLike);
                return ResponseUtil.suc(true);
            }
        } catch (Exception e) {
            logger.info(String.valueOf(e));
            e.printStackTrace();
            return ResponseUtil.fail(SERVE_ERROR);
        }
    }

    @Override
    public ReturnJson removePublishsLike(Long publishId, Long likedPostId) {
        return null;
    }

    @Override
    public Boolean getPublishsLike(Long userId, Long publishId, Long likedUserId) {
        Boolean flag = false;
        QueryWrapper<PublishsLike> queryWrapper = new QueryWrapper<PublishsLike>();
        queryWrapper.eq("publish_id",publishId);
        queryWrapper.eq("liked_post_id",userId);
        PublishsLike publishsLike = publishsLikeMapper.selectOne(queryWrapper);
        //当数据库中能查到
        if (publishsLike != null){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public ResponseUtil<IPage<PublishsLike>> getPublishsLikePages(Integer pageNum,Integer pageSize){
        Page<PublishsLike> page=new Page<PublishsLike>(pageNum,pageSize);
        QueryWrapper<PublishsLike> queryWrapper =new QueryWrapper<PublishsLike>();
        //分页数据
        IPage<PublishsLike> pageData=publishsLikeService.page(page, queryWrapper);
        return ResponseUtil.suc(pageData);
    }

}
