package com.quyang.voice.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.ShopkeeperMapper;
import com.quyang.voice.model.Shopkeeper;
import com.quyang.voice.model.ShopkeeperCategory;
import com.quyang.voice.service.ShopkeeperCategoryService;
import com.quyang.voice.service.ShopkeeperService;
import com.quyang.voice.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ShopkeeperServiceImpl extends ServiceImpl<ShopkeeperMapper, Shopkeeper>
        implements ShopkeeperService{

    @Autowired
    ShopkeeperCategoryService shopkeeperCategoryService;

    @Override
    public ResponseUtil getShopkeeper(Integer userId) {

        QueryWrapper<Shopkeeper> wrapper = new QueryWrapper();
        wrapper.eq("user_id",userId);

        Shopkeeper shopkeeper = getOne(wrapper);

        if (shopkeeper == null){
            return ResponseUtil.suc();
        }
        ShopkeeperCategory byId = shopkeeperCategoryService.getById(shopkeeper.getPositionId());

        if (byId !=null){
            shopkeeper.setPositionName(byId.getName());
        }

        return ResponseUtil.suc(shopkeeper);
    }

    @Override
    public ResponseUtil getShopkeepers(Integer userId, Integer positionId, String name, String isRecommend) {

        QueryWrapper<Shopkeeper> wrapper = new QueryWrapper();
        wrapper.eq("status","3");

        if (!StrUtil.isEmptyIfStr(positionId) && positionId != 0){
            wrapper.eq("position_id",positionId);
        }

        if (!StrUtil.isEmptyIfStr(name)){
            wrapper.like("name",name);
        }


        if (!StrUtil.isEmptyIfStr(isRecommend )){
            wrapper.eq("is_recommend",isRecommend);
        }


        if (!StrUtil.isEmptyIfStr(userId )){
            wrapper.like("user_id",userId);
        }
        wrapper.orderByDesc("create_time");

        return ResponseUtil.suc(list(wrapper));

    }
}




