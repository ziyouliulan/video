package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.CarWashRecord;
import com.quyang.voice.utils.ResponseUtil;

/**
 *
 */
public interface CarWashRecordService extends IService<CarWashRecord> {

    ResponseUtil payForCarWash(Integer shopId , Integer userId, String password, Integer type);


    public ResponseUtil carWashRecord(Integer userId,Integer type,Integer pageNum, Integer pageSize);

}
