
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.AppConfig;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IAppConfigService extends IService<AppConfig> {
    ResponseUtil<AppConfig> getAppConfigById(Long id);

    ReturnJson deleteAppConfigById(Long id);

    ReturnJson insertAppConfig(AppConfig appConfig);

    ReturnJson updateAppConfig(AppConfig appConfig);
}
