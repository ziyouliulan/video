package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.LitemallGoods;

/**
 *
 */
public interface LitemallGoodsService extends IService<LitemallGoods> {

}
