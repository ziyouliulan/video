package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.YuecoolNotice;

/**
* @author lpden
* @description 针对表【yuecool_notice】的数据库操作Service
* @createDate 2021-12-15 15:23:34
*/
public interface YuecoolNoticeService extends IService<YuecoolNotice> {

    YuecoolNotice getNtice(Long id);


}
