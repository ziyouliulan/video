
package com.quyang.voice.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.GroupSlienceMapper;
import com.quyang.voice.manage.IGroupSlienceManage;
import com.quyang.voice.model.GroupSlience;
import com.quyang.voice.model.vo.UserBaseVo;
import com.quyang.voice.service.IGroupSlienceService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("groupSlienceService")
public class GroupSlienceServiceImpl extends ServiceImpl<GroupSlienceMapper, GroupSlience> implements IGroupSlienceService {

    private static final Logger logger = LoggerFactory.getLogger(GroupSlienceServiceImpl.class);

    @Autowired
    IGroupSlienceService groupSlienceService;

    @Autowired
    IGroupSlienceManage groupSlienceManage;

    @Autowired
    GroupSlienceMapper groupSlienceMapper;

    @Override
    public ResponseUtil<List<UserBaseVo>> getGroupSlienceById(Long id){
        try {
            List<UserBaseVo> userBaseVoList = groupSlienceMapper.getSilenceUserList(id);
            return ResponseUtil.suc(userBaseVoList);
        }catch (Exception e){
            logger.error("GroupSlienceServiceImpl.getGroupSlienceById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteGroupSlienceById(Long clusterId,Long userId){
        try {
            QueryWrapper<GroupSlience> personalClusterSilenceQueryWrapper = new QueryWrapper<>();
            personalClusterSilenceQueryWrapper.eq("user_id",userId);
            personalClusterSilenceQueryWrapper.eq("cluster_id",clusterId);
            boolean rsg = groupSlienceService.remove(personalClusterSilenceQueryWrapper);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("PersonalClusterSilenceServiceImpl.deletePersonalClusterSilenceById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertGroupSlience(GroupSlience groupSlience){
        try {
            QueryWrapper<GroupSlience> groupSlienceQueryWrapper = new QueryWrapper<>();
            groupSlienceQueryWrapper.eq("cluster_id",groupSlience.getClusterId()).eq("user_id",groupSlience.getUserId());
            GroupSlience groupSlienceR = groupSlienceService.getOne(groupSlienceQueryWrapper);
            if (null != groupSlience) {
                Date date = new Date();
                DateTime newDate2 = DateUtil.offsetDay(date, 3);
                groupSlience.setBanTime(newDate2);
                groupSlience.setCreateTime(date);
                boolean rsg = false;
                if (groupSlienceR != null) {
                    groupSlienceR.setBanTime(groupSlience.getBanTime());
                    rsg = groupSlienceService.updateById(groupSlienceR);
                }else {
                    rsg = groupSlienceService.save(groupSlience);

                }
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("GroupSlienceServiceImpl.insertGroupSlience", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
