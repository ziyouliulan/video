package com.quyang.voice.service;

import com.quyang.voice.model.Doctor;
import com.quyang.voice.model.PatientInformation;
import com.quyang.voice.model.dto.DoctorDto;
import com.quyang.voice.utils.ResponseUtil;

import java.util.List;

public interface DoctorService {



    public List<Doctor> getDoctor(DoctorDto doctorDto);


    public List<Doctor> searchDoctor(DoctorDto doctorDto);


    public List<Doctor> getDoctorInformation(DoctorDto doctorDto);

    public PatientInformation insertPatientInformation(PatientInformation patientInformation);

    public ResponseUtil insertPatientInformation(Long userId, Long toUserId, Double money);


    int updateByPrimaryKeySelective(PatientInformation record);

    public ResponseUtil prescriptionPayment(Long userId, Long toUserId, Double money,Integer id);

}
