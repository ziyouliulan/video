
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.CustAccountbase;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface ICustAccountbaseService extends IService<CustAccountbase> {

    ResponseUtil<Map> getCustAccountbaseById(Integer id);


    /**
     * 充值
     * @param request
     * @param response
     * @return
     */
    ReturnJson recharge(HttpServletRequest request, HttpServletResponse response);

    /**
     * 充值
     * @param request
     * @param response
     * @return
     */
    ReturnJson shopPayment(HttpServletRequest request, HttpServletResponse response);


    /**
     * 更新支付密码
     * @param userId
     * @param oldPayKey
     * @param newPayKey
     * @return
     */
    ReturnJson updatePayKey(Integer userId,String oldPayKey,String newPayKey);

    /**
     * 更新支付密码
     * @param userId
     * @param payKey
     * @param newPayKey
     * @return
     */
    ResponseUtil updatePayKey2(String userId,String payKey,String vCode);

    /**
     * 查看是否有支付密码
     * @param userId
     * @return
     */
    ResponseUtil checkPayKeyIsExist(Integer userId);

    /**
     *
     * @param userId
     * @param payKey
     * @return
     */
    ResponseUtil checkPayKey(Integer userId,String payKey);

}
