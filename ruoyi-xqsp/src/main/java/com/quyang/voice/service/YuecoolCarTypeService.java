package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.YuecoolCarType;

/**
 *
 */
public interface YuecoolCarTypeService extends IService<YuecoolCarType> {

}
