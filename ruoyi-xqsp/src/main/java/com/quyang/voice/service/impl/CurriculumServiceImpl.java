package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.CurriculumMapper;
import com.quyang.voice.model.Curriculum;
import com.quyang.voice.service.CurriculumService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class CurriculumServiceImpl extends ServiceImpl<CurriculumMapper, Curriculum>
    implements CurriculumService{

}




