
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.MissuCollectMapper;
import com.quyang.voice.manage.IMissuCollectManage;
import com.quyang.voice.model.MissuCollect;
import com.quyang.voice.model.vo.MissuCollectVo;
import com.quyang.voice.service.IMissuCollectService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;

@Service("missuCollectService")
public class MissuCollectServiceImpl extends ServiceImpl<MissuCollectMapper, MissuCollect> implements IMissuCollectService {

    private static final Logger logger = LoggerFactory.getLogger(MissuCollectServiceImpl.class);

    @Autowired
    IMissuCollectService missuCollectService;

    @Autowired
    IMissuCollectManage missuCollectManage;

    @Autowired
    MissuCollectMapper missuCollectMapper;

    @Override
    public ResponseUtil<MissuCollectVo> getMissuCollectById(@PathVariable("id")Long id){
        try {
            MissuCollectVo obj=missuCollectMapper.getMissuCollectById(id);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("MissuCollectServiceImpl.getMissuCollectById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteMissuCollectById(Long id){
        try {
            boolean rsg = missuCollectService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("MissuCollectServiceImpl.deleteMissuCollectById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertMissuCollect(MissuCollect missuCollect){
        try {
            if (null != missuCollect) {
                missuCollect.setCreateTime(new Date());
                boolean rsg = missuCollectService.save(missuCollect);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("MissuCollectServiceImpl.insertMissuCollect", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateMissuCollect(MissuCollect missuCollect){
        try {
            if (null != missuCollect) {
                boolean rsg = missuCollectService.updateById(missuCollect);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("MissuCollectServiceImpl.updateMissuCollect", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<MissuCollectVo> getMissuColletList(Integer userId) {
        try {
            List<MissuCollectVo> obj=missuCollectMapper.getMisscCollectVoList(userId);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("MissuCollectServiceImpl.getMissuCollectById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
