package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.CourseComments;
import com.quyang.voice.utils.ResponseUtil;

/**
 *
 */
public interface CourseCommentsService extends IService<CourseComments> {

    public ResponseUtil addCourseComments(CourseComments courseComments);


    public ResponseUtil courseComments(Integer userId,Integer courseId);

}
