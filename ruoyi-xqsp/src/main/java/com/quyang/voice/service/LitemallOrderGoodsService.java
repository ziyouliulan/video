package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.LitemallOrderGoods;

/**
 *
 */
public interface LitemallOrderGoodsService extends IService<LitemallOrderGoods> {

}
