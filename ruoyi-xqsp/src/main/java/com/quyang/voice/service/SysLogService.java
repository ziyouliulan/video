package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.SysLog;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.scheduling.annotation.Async;

/**
 *
 */
public interface SysLogService extends IService<SysLog> {


    @Async
    void save(String username, String browser, String ip, ProceedingJoinPoint joinPoint, SysLog log);


}
