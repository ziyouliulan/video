package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.ShopkeeperCategory;

/**
 *
 */
public interface ShopkeeperCategoryService extends IService<ShopkeeperCategory> {

}
