package com.quyang.voice.service;

import com.quyang.voice.model.ClientConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
* @author lpden
* @description 针对表【client_config(参数配置表)】的数据库操作Service
* @createDate 2022-04-24 13:50:30
*/
public interface ClientConfigService extends IService<ClientConfig> {
    HashMap configToMap();
}
