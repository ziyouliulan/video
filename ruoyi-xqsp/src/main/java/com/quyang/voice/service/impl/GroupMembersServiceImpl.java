
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.GroupMembersMapper;
import com.quyang.voice.manage.IGroupMembersManage;
import com.quyang.voice.model.GroupMembers;
import com.quyang.voice.service.IGroupMembersService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service("groupMembersService")
public class GroupMembersServiceImpl extends ServiceImpl<GroupMembersMapper, GroupMembers> implements IGroupMembersService {

    private static final Logger logger = LoggerFactory.getLogger(GroupMembersServiceImpl.class);

    @Autowired
    IGroupMembersService groupMembersService;

    @Autowired
    IGroupMembersManage groupMembersManage;

    @Autowired
    GroupMembersMapper groupMembersMapper;

    @Override
    public ResponseUtil<GroupMembers> getGroupMembersById(@PathVariable("id")Long id){
        try {
            GroupMembers obj=groupMembersService.getById(id);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("GroupMembersServiceImpl.getGroupMembersById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteGroupMembersById(Long id){
        try {
            boolean rsg = groupMembersService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("GroupMembersServiceImpl.deleteGroupMembersById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertGroupMembers(GroupMembers groupMembers){
        try {
            if (null != groupMembers) {
                boolean rsg = groupMembersService.save(groupMembers);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("GroupMembersServiceImpl.insertGroupMembers", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateGroupMembers(GroupMembers groupMembers){
        try {
            if (null != groupMembers) {
                boolean rsg = groupMembersService.updateById(groupMembers);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("GroupMembersServiceImpl.updateGroupMembers", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateGroupAdmin(String userIds, String clusterId, Integer type) {

        try {
            String[] ids = userIds.split(",");
            QueryWrapper<GroupMembers> groupMembersQueryWrapper = new QueryWrapper<>();
            groupMembersQueryWrapper.eq("is_admin",0);
            groupMembersQueryWrapper.eq("g_id",clusterId);
            List<GroupMembers> groupMembersList = groupMembersService.list(groupMembersQueryWrapper);
            if ( type.equals(0) && groupMembersList.size() + ids.length > 10){
                return ReturnJson.fail(ResultCode.OUT_INDEX_ADMIN);
            }else {
                GroupMembers groupMembers = new GroupMembers();
                groupMembers.setIsAdmin(0);
                UpdateWrapper<GroupMembers> groupMembersUpdateWrapper = new UpdateWrapper<>();
//                groupMembersUpdateWrapper.in("user_uid",ids);
                groupMembersUpdateWrapper.in("user_uid",ids);
                groupMembersUpdateWrapper.eq("g_id",clusterId);
                GroupMembers groupMembersR = new GroupMembers();
                groupMembersR.setIsAdmin(type);
                groupMembersService.update(groupMembersR,groupMembersUpdateWrapper);
            }
            return ReturnJson.suc(ResultCode.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("updateGroupAdmin:",e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);

        }
    }
}
