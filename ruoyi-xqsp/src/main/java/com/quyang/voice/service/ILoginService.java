package com.quyang.voice.service;

import com.quyang.voice.token.LoginUser;
import com.quyang.voice.utils.ResponseUtil;

public interface ILoginService {

    ResponseUtil getLoginByPwd(String account, String passwd,String loginLot,String loginLat);

    ResponseUtil loginByVerifyCode(String phone, String vCode,String loginLot,String loginLat);

    ResponseUtil loginByWeChat(String openId,String loginLot,String loginLat);

    ResponseUtil loginByQQ(String openId,String loginLot,String loginLat);

    ResponseUtil oneClickLogin(String phone,String loginLot,String loginLat);

    ResponseUtil<LoginUser> getPhoneByYoumengToken(String youmengToken,String loginLot,String loginLat);

}
