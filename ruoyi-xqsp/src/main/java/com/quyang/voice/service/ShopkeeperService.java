package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.Shopkeeper;
import com.quyang.voice.utils.ResponseUtil;

/**
* @author lpden
* @description 针对表【yuecool_shopkeeper(讲师)】的数据库操作Service
* @createDate 2021-12-12 16:40:11
*/
public interface ShopkeeperService extends IService<Shopkeeper> {



    public ResponseUtil getShopkeeper(Integer id);

    public ResponseUtil getShopkeepers(Integer id, Integer positionId, String name,String isRecommend);
}
