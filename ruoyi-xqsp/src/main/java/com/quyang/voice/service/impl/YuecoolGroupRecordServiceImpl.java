package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.YuecoolGroupRecordMapper;
import com.quyang.voice.model.YuecoolGroupRecord;
import com.quyang.voice.service.YuecoolGroupRecordService;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【yuecool_group_record(消费记录)】的数据库操作Service实现
* @createDate 2021-12-30 17:33:12
*/
@Service
public class YuecoolGroupRecordServiceImpl extends ServiceImpl<YuecoolGroupRecordMapper, YuecoolGroupRecord>
    implements YuecoolGroupRecordService{

}




