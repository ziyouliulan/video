package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.ShopkeeperCategoryMapper;
import com.quyang.voice.model.ShopkeeperCategory;
import com.quyang.voice.service.ShopkeeperCategoryService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ShopkeeperCategoryServiceImpl extends ServiceImpl<ShopkeeperCategoryMapper, ShopkeeperCategory>
    implements ShopkeeperCategoryService{

}




