package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.YuecoolNoticeMapper;
import com.quyang.voice.model.YuecoolNotice;
import com.quyang.voice.service.YuecoolNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【yuecool_notice】的数据库操作Service实现
* @createDate 2021-12-15 15:23:34
*/
@Service
public class YuecoolNoticeServiceImpl extends ServiceImpl<YuecoolNoticeMapper, YuecoolNotice>
    implements YuecoolNoticeService{

    @Autowired
    YuecoolNoticeMapper yuecoolNoticeMapper;

    @Override
    public YuecoolNotice getNtice(Long id){

        return yuecoolNoticeMapper.getNticeById(id);

    }
}




