package com.quyang.voice.service;

import com.quyang.voice.model.PatientInformation;
import com.quyang.voice.model.PatientInformationExample;

import java.util.List;

public interface PatientInformationService {


    List<PatientInformation> selectByExample(PatientInformationExample example);

}
