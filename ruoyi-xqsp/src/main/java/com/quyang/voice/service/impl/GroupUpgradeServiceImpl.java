package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.GroupUpgradeMapper;
import com.quyang.voice.model.GroupUpgrade;
import com.quyang.voice.service.GroupUpgradeService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class GroupUpgradeServiceImpl extends ServiceImpl<GroupUpgradeMapper, GroupUpgrade>
    implements GroupUpgradeService{

}




