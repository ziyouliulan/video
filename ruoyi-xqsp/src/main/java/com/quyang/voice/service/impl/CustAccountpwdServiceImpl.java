
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.CustAccountpwdMapper;
import com.quyang.voice.manage.ICustAccountpwdManage;
import com.quyang.voice.model.CustAccountpwd;
import com.quyang.voice.service.ICustAccountpwdService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service("cust_accountpwdService")
public class CustAccountpwdServiceImpl extends ServiceImpl<CustAccountpwdMapper, CustAccountpwd> implements ICustAccountpwdService {

    private static final Logger logger = LoggerFactory.getLogger(CustAccountpwdServiceImpl.class);

    @Autowired
    ICustAccountpwdService cust_accountpwdService;

    @Autowired
    ICustAccountpwdManage cust_accountpwdManage;

    @Autowired
    CustAccountpwdMapper cust_accountpwdMapper;

    @Override
    public ResponseUtil<CustAccountpwd> getCustAccountpwdById(@PathVariable("id")Long id){
        try {
            CustAccountpwd obj=cust_accountpwdService.getById(id);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("CustAccountpwdServiceImpl.getCustAccountpwdById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteCustAccountpwdById(Long id){
        try {
            boolean rsg = cust_accountpwdService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("CustAccountpwdServiceImpl.deleteCustAccountpwdById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertCustAccountpwd(CustAccountpwd cust_accountpwd){
        try {
            if (null != cust_accountpwd) {
                boolean rsg = cust_accountpwdService.save(cust_accountpwd);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("CustAccountpwdServiceImpl.insertCustAccountpwd", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateCustAccountpwd(CustAccountpwd cust_accountpwd){
        try {
            if (null != cust_accountpwd) {
                boolean rsg = cust_accountpwdService.updateById(cust_accountpwd);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("CustAccountpwdServiceImpl.updateCustAccountpwd", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
