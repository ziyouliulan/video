
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.PublishNoSee;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IPublishNoSeeService extends IService<PublishNoSee> {
    ResponseUtil<PublishNoSee> getPublishNoSeeById(Long id);

    ReturnJson deletePublishNoSeeById(Long id);

    ReturnJson insertPublishNoSee(PublishNoSee publishNoSee);

    ReturnJson updatePublishNoSee(PublishNoSee publishNoSee);
}
