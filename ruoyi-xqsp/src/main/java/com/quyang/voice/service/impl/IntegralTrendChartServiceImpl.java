package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.IntegralTrendChartMapper;
import com.quyang.voice.model.IntegralTrendChart;
import com.quyang.voice.service.IntegralTrendChartService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class IntegralTrendChartServiceImpl extends ServiceImpl<IntegralTrendChartMapper, IntegralTrendChart>
    implements IntegralTrendChartService{

}




