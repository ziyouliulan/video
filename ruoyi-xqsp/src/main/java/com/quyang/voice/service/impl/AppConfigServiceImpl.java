
package com.quyang.voice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.quyang.voice.dao.AppConfigMapper;
import com.quyang.voice.dao.MissuUsersMapper;
import com.quyang.voice.manage.IAppConfigManage;
import com.quyang.voice.model.AppConfig;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.service.ClientConfigService;
import com.quyang.voice.service.IAppConfigService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service("appConfigService")
public class AppConfigServiceImpl extends ServiceImpl<AppConfigMapper, AppConfig> implements IAppConfigService {

    private static final Logger logger = LoggerFactory.getLogger(AppConfigServiceImpl.class);

    @Autowired
    IAppConfigService appConfigService;

    @Autowired
    IAppConfigManage appConfigManage;

    @Autowired
    AppConfigMapper appConfigMapper;

    @Autowired
    MissuUsersMapper missuUsersMapper;

    @Autowired
    ClientConfigService configService;

    @Override
    public ResponseUtil<AppConfig> getAppConfigById(@PathVariable("id")Long id){
        try {
            AppConfig obj=appConfigService.getById(id);
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.select("user_uid");
            wrapper.eq("user_type",3);
            List<MissuUsers> list = missuUsersMapper.selectList(wrapper);

            List<Integer> integerList = new ArrayList<>();
            for (MissuUsers missuUsers:list
                 ) {
                integerList.add(missuUsers.getUserUid());
            }
            obj.setCustomerServiceIdentification(Joiner.on("#").join(integerList));
            String jsonString = JSONObject.toJSONString(obj);
            HashMap map = JSONObject.parseObject(jsonString, HashMap.class);
            HashMap map1 = configService.configToMap();
            map.putAll(map1);
            return ResponseUtil.suc(map);
        }catch (Exception e){
            logger.error("AppConfigServiceImpl.getAppConfigById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deleteAppConfigById(Long id){
        try {
            boolean rsg = appConfigService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("AppConfigServiceImpl.deleteAppConfigById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertAppConfig(AppConfig appConfig){
        try {
            if (null != appConfig) {
                boolean rsg = appConfigService.save(appConfig);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("AppConfigServiceImpl.insertAppConfig", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateAppConfig(AppConfig appConfig){
        try {
            if (null != appConfig) {
                boolean rsg = appConfigService.updateById(appConfig);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("AppConfigServiceImpl.updateAppConfig", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }


    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(12312);
        list.add(435);
        list.add(543);
        list.add(12312);
        String join = Joiner.on("#").join(list);
        System.out.println(join);

        List<String> strings = Splitter.on("#").trimResults().splitToList(join);
        System.out.println(strings);
        System.out.println(strings.get(2));
    }
}
