package com.quyang.voice.service;


import com.quyang.voice.model.TStorage;
import com.quyang.voice.utils.ResponseUtil;

import com.ruoyi.xqsp.domain.ToolLocalStorage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface IStorageService {

    ResponseUtil<TStorage> downloadFileByName(String fileName) throws UnsupportedEncodingException;

    ResponseUtil<TStorage> downloadById(Long id);



    ToolLocalStorage create(String name, MultipartFile file);
    List<ToolLocalStorage> create(String name, MultipartFile[] file);



}


