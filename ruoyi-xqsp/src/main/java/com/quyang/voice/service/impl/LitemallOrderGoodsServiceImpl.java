package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.LitemallOrderGoodsMapper;
import com.quyang.voice.model.LitemallOrderGoods;
import com.quyang.voice.service.LitemallOrderGoodsService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class LitemallOrderGoodsServiceImpl extends ServiceImpl<LitemallOrderGoodsMapper, LitemallOrderGoods>
    implements LitemallOrderGoodsService{

}




