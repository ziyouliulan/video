
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.PrivacyConfig;
import com.quyang.voice.model.vo.PrivateConfigVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IPrivacyConfigService extends IService<PrivacyConfig> {
    ResponseUtil<PrivateConfigVo> getPrivacyConfigById(Integer id);

    ReturnJson updatePrivacyConfig(PrivacyConfig privacyConfig);
}
