
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.PublishCommentsMapper;
import com.quyang.voice.dao.PublishCommentsReplyMapper;
import com.quyang.voice.model.PublishComments;
import com.quyang.voice.model.PublishCommentsReply;
import com.quyang.voice.service.IPublishCommentsReplyService;
import com.quyang.voice.service.IStorageService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import com.quyang.voice.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;

@Service("publishCommentsReplyService")
public class PublishCommentsReplyServiceImpl extends ServiceImpl<PublishCommentsReplyMapper, PublishCommentsReply> implements IPublishCommentsReplyService {

    private static final Logger logger = LoggerFactory.getLogger(PublishCommentsReplyServiceImpl.class);

    @Autowired
    IPublishCommentsReplyService publishCommentsReplyService;


    @Autowired
    PublishCommentsReplyMapper publishCommentsReplyMapper;

    @Autowired
    PublishCommentsMapper publishCommentsMapper;


    @Override
    public ResponseUtil<PublishCommentsReply> getPublishCommentsReplyById(@PathVariable("id")Long id){
        try {
            PublishCommentsReply obj=publishCommentsReplyService.getById(id);
            if (null != obj ) {
                return ResponseUtil.suc(obj);
            }else {
                return ResponseUtil.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("OrganizesServiceImpl.getOrganizesById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deletePublishCommentsReplyById(Long id){
        try {
            PublishCommentsReply reply = publishCommentsReplyService.getById(id);
            if(reply.getReplyCommentPic()!=null){
//                storageService.delete(reply.getReplyCommentPicName());
            }
            boolean rsg = publishCommentsReplyService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("OrganizesServiceImpl.deleteOrganizesById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertPublishCommentsReply(PublishCommentsReply publishCommentsReply){
        try {
            if (null != publishCommentsReply) {
                publishCommentsReply.setCreateDate(new Date());
                if(publishCommentsReply.getReplyCommentPic() != null && publishCommentsReply.getReplyCommentPic().length() != 0){
                    String picName = publishCommentsReply.getReplyCommentPic().split("/")[publishCommentsReply.getReplyCommentPic().split("/").length-1];
                    publishCommentsReply.setReplyCommentPicName(picName);
                }

                PublishComments publishComments = publishCommentsMapper.selectById(publishCommentsReply.getCommentsId());

                if (publishComments != null && publishComments.getUserId() != publishCommentsReply.getTouid()){
                    publishCommentsReply.setIsCommentUser(1);
                }
                boolean rsg = publishCommentsReplyService.save(publishCommentsReply);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("OrganizesServiceImpl.insertOrganizes", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updatePublishCommentsReply(PublishCommentsReply publishCommentsReply){
        try {
            if (null != publishCommentsReply) {
                boolean rsg = publishCommentsReplyService.updateById(publishCommentsReply);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("OrganizesServiceImpl.updateOrganizes", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<PublishCommentsReply> getPublishCommentsReplyByCommentId(Long commentId,Integer userId) {
        try {
//            QueryWrapper<PublishCommentsReply> queryWrapper = new QueryWrapper<>();
//            queryWrapper.eq("comments_id",commentId);
//            queryWrapper.orderByAsc("create_date");
            return publishCommentsReplyMapper.getPublishCommentsReply(commentId, userId);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString());
            return  null;
        }
    }

    @Override
    public Boolean deletePublishCommentsReplyByCommentId(Long commentId) {
        try {
            QueryWrapper<PublishCommentsReply> queryWrapper =new QueryWrapper<PublishCommentsReply>();
            queryWrapper.eq("comments_id",commentId);
            List<PublishCommentsReply> repl = publishCommentsReplyMapper.selectList(queryWrapper);
            for(PublishCommentsReply publishCommentsReply:repl){
                if(!StringUtils.isEmpty(publishCommentsReply.getReplyCommentPic())){
//                    storageService.delete(publishCommentsReply.getReplyCommentPicName());
                }
            }
            publishCommentsReplyMapper.delete(queryWrapper);
            logger.info("删除评论下的回复");
            return true;
        } catch (Exception e) {
            logger.info("删除评论下的回复失败"+e);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ResponseUtil<IPage<PublishCommentsReply>> getPublishCommentsReplyPages(Integer pageNum,Integer pageSize){
        try {
            Page<PublishCommentsReply> page=new Page<PublishCommentsReply>(pageNum,pageSize);
            QueryWrapper<PublishCommentsReply> queryWrapper =new QueryWrapper<PublishCommentsReply>();
            //分页数据
            IPage<PublishCommentsReply> pageData=publishCommentsReplyService.page(page, queryWrapper);
            return ResponseUtil.suc(pageData);
        } catch (Exception e) {
            logger.error("OrganizesServiceImpl.getOrganizesPages", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
