
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.model.vo.MissuUsersBaseVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IMissuUsersService extends IService<MissuUsers> {
    ReturnJson deleteMissuUsersById(Long id);

    ReturnJson insertMissuUsers(MissuUsers missuUsers);

    ReturnJson updateMissuUsers(MissuUsers missuUsers);

    ReturnJson checkRepeat(String username, String phone);


    /**
     * 根据用户Uid查询用户基本信息
     * @param userUid
     * @return
     */
    MissuUsersBaseVo getUserBase(Integer userUid);

}
