
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.UsermoenyLog;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IUsermoenyLogService extends IService<UsermoenyLog> {
    ResponseUtil<UsermoenyLog> getUsermoenyLogById(Long id);

    ReturnJson deleteUsermoenyLogById(Long id);

    ReturnJson insertUsermoenyLog(UsermoenyLog usermoenyLog);

    ReturnJson updateUsermoenyLog(UsermoenyLog usermoenyLog);
}
