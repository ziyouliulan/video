package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.YuecoolGroupRecord;

/**
* @author lpden
* @description 针对表【yuecool_group_record(消费记录)】的数据库操作Service
* @createDate 2021-12-30 17:33:12
*/
public interface YuecoolGroupRecordService extends IService<YuecoolGroupRecord> {

}
