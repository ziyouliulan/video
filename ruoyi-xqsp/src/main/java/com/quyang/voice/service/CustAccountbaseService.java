package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.CustAccountbase;
import org.springframework.stereotype.Service;

/**
 *
 */
public interface CustAccountbaseService extends IService<CustAccountbase> {

}
