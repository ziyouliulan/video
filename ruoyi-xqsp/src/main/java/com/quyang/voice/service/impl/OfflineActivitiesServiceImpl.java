package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.OfflineActivitiesMapper;
import com.quyang.voice.model.OfflineActivities;
import com.quyang.voice.service.OfflineActivitiesService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class OfflineActivitiesServiceImpl extends ServiceImpl<OfflineActivitiesMapper, OfflineActivities>
    implements OfflineActivitiesService{

}




