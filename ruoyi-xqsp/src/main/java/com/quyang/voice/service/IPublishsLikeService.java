
package com.quyang.voice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.PublishsLike;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IPublishsLikeService extends IService<PublishsLike> {
    ResponseUtil<PublishsLike> getPublishsLikeById(Long id);

    ReturnJson deletePublishsLikeById(Long id);

    ReturnJson insertPublishsLike(PublishsLike publishsLike);

    ReturnJson updatePublishsLike(PublishsLike publishsLike);

    /**
     * 进行点赞
     * @param publishsLike
     * @return
     */
    ResponseUtil postPublishsLike(PublishsLike publishsLike);

    /**
     * 根据动态id和操作人id取消
     * @param publishId
     * @param likedPostId
     * @return
     */
    ReturnJson removePublishsLike(Long publishId,Long likedPostId);

    /**
     * 查询该用户对该动态是否点了赞
     * @param userId
     * @param publishId
     * @param likedUserId
     * @return
     */
    Boolean getPublishsLike(Long userId, Long publishId, Long likedUserId);


    ResponseUtil<IPage<PublishsLike>> getPublishsLikePages(Integer pageNum,Integer pageSize);
}