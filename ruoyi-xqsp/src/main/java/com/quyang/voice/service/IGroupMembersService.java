
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.GroupMembers;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IGroupMembersService extends IService<GroupMembers> {
    ResponseUtil<GroupMembers> getGroupMembersById(Long id);

    ReturnJson deleteGroupMembersById(Long id);

    ReturnJson insertGroupMembers(GroupMembers groupMembers);

    ReturnJson updateGroupMembers(GroupMembers groupMembers);


    /**
     * 更新任命群管理
     * @param userIds
     * @param clusterId
     * @param type
     * @return
     */
    ReturnJson updateGroupAdmin(String userIds,String clusterId,Integer type);
}
