
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.GroupSlience;
import com.quyang.voice.model.vo.UserBaseVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

import java.util.List;

public interface IGroupSlienceService extends IService<GroupSlience> {
    ResponseUtil<List<UserBaseVo>> getGroupSlienceById(Long id);

    ReturnJson deleteGroupSlienceById(Long clusterId,Long userId);

    ReturnJson insertGroupSlience(GroupSlience groupSlience);
}
