package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.LitemallUserMapper;
import com.quyang.voice.model.LitemallUser;
import com.quyang.voice.service.LitemallUserService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class LitemallUserServiceImpl extends ServiceImpl<LitemallUserMapper, LitemallUser>
    implements LitemallUserService{

}




