package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.mapper.YuecoolCarTypeMapper;
import com.quyang.voice.model.YuecoolCarType;
import com.quyang.voice.service.YuecoolCarTypeService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class YuecoolCarTypeServiceImpl extends ServiceImpl<YuecoolCarTypeMapper, YuecoolCarType>
    implements YuecoolCarTypeService{

}




