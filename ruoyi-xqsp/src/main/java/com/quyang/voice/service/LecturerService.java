package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.Lecturer;
import com.quyang.voice.model.dto.LecturerDto;
import com.quyang.voice.utils.ResponseUtil;

/**
 *
 */
public interface LecturerService extends IService<Lecturer> {

    public ResponseUtil getLecturer(Integer userId);
    public ResponseUtil getLecturers(LecturerDto lecturerDto);

}
