package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.GroupConfig;

import java.util.HashMap;

/**
* @author lpden
* @description 针对表【group_config(参数配置表)】的数据库操作Service
* @createDate 2022-04-19 14:56:06
*/
public interface GroupConfigService extends IService<GroupConfig> {

    HashMap configToMap();

}
