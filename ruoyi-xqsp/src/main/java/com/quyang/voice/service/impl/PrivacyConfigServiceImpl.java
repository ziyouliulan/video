
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.PrivacyConfigMapper;
import com.quyang.voice.manage.IPrivacyConfigManage;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.model.PrivacyConfig;
import com.quyang.voice.model.PublishNoSee;
import com.quyang.voice.model.vo.MissuUsersBaseVo;
import com.quyang.voice.model.vo.PrivateConfigVo;
import com.quyang.voice.service.IMissuUsersService;
import com.quyang.voice.service.IPrivacyConfigService;
import com.quyang.voice.service.IPublishNoSeeService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("privacyConfigService")
public class PrivacyConfigServiceImpl extends ServiceImpl<PrivacyConfigMapper, PrivacyConfig> implements IPrivacyConfigService {

    private static final Logger logger = LoggerFactory.getLogger(PrivacyConfigServiceImpl.class);

    @Autowired
    IPrivacyConfigService privacyConfigService;

    @Autowired
    IPrivacyConfigManage privacyConfigManage;

    @Autowired
    PrivacyConfigMapper privacyConfigMapper;

    @Autowired
    IPublishNoSeeService publishNoSeeService;

    @Autowired
    IMissuUsersService missuUsersService;

    @Override
    public ResponseUtil<PrivateConfigVo> getPrivacyConfigById(Integer userId){
        try {
            QueryWrapper<PrivacyConfig> privacyConfigQueryWrapper = new QueryWrapper<>();
            privacyConfigQueryWrapper.eq("user_id",userId);
            PrivacyConfig obj=privacyConfigService.getOne(privacyConfigQueryWrapper);

            PrivateConfigVo vo = new PrivateConfigVo();
            //当没有该数据则创建
            if (obj == null){
                PrivacyConfig privacyConfig = new PrivacyConfig();
                privacyConfig.setUserUid(userId);
                privacyConfigService.save(privacyConfig);
                obj = privacyConfig;
            }
            vo.setPrivacyConfig(obj);

            //获得用户不看的人的用户信息
            List<String> noSeeHeIds = Arrays.asList(obj.getIsNotSeeHe().split(","));
            QueryWrapper<MissuUsers> missuUsersQueryWrapper = new QueryWrapper<>();
            missuUsersQueryWrapper.in("user_uid",noSeeHeIds);
            List<MissuUsers> missuUsersList = missuUsersService.list(missuUsersQueryWrapper);
            List<MissuUsersBaseVo> noSeeHeUser = new ArrayList<>();
            for (MissuUsers missuUsers: missuUsersList){
                MissuUsersBaseVo missuUsersBaseVo = new MissuUsersBaseVo();
                missuUsersBaseVo.setNickname(missuUsers.getNickname());
                missuUsersBaseVo.setUserType(missuUsers.getUserType());
                missuUsersBaseVo.setUserUid(missuUsers.getUserUid());
                missuUsersBaseVo.setUserAvatarFileName(missuUsers.getUserAvatarFileName());
                noSeeHeUser.add(missuUsersBaseVo);
            }


            //获得不让看我的人的用户信息
            List<String> noSeeMeIds = Arrays.asList(obj.getIsNotSeeHe().split(","));
            QueryWrapper<MissuUsers> missuUsersQueryWrapperR = new QueryWrapper<>();
            missuUsersQueryWrapperR.in("user_uid",noSeeMeIds);
            List<MissuUsers> missuUsersListR = missuUsersService.list(missuUsersQueryWrapperR);
            List<MissuUsersBaseVo> noSeeMeUser = new ArrayList<>();

            for (MissuUsers missuUsers: missuUsersList){
                MissuUsersBaseVo missuUsersBaseVo = new MissuUsersBaseVo();
                missuUsersBaseVo.setNickname(missuUsers.getNickname());
                missuUsersBaseVo.setUserType(missuUsers.getUserType());
                missuUsersBaseVo.setUserUid(missuUsers.getUserUid());
                missuUsersBaseVo.setUserAvatarFileName(missuUsers.getUserAvatarFileName());
                noSeeMeUser.add(missuUsersBaseVo);
            }

            vo.setIsNotSeeHeUser(noSeeHeUser);


            return ResponseUtil.suc(vo);
        }catch (Exception e){
            logger.error("PrivacyConfigServiceImpl.getPrivacyConfigById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updatePrivacyConfig(PrivacyConfig privacyConfig){
        try {
            if (null != privacyConfig) {
                //如果屏蔽用户不为空，则更新对应表
                if (privacyConfig.getIsNotSeeMe() != null && !privacyConfig.getIsNotSeeMe().equals("")){
                    List<String> toUserIdList = Arrays.asList(privacyConfig.getIsNotSeeMe().split(",").clone());
                    List<PublishNoSee> publishNoSeeList = new ArrayList<>();
                    QueryWrapper<PublishNoSee> publishNoSeeQueryWrapper = new QueryWrapper<>();
                    for (String toUserIdStr :  toUserIdList){
                        PublishNoSee publishNoSee = new PublishNoSee();
                        publishNoSee.setUserId(privacyConfig.getUserUid());
                        publishNoSee.setToUserId(Integer.valueOf(toUserIdStr));
                        publishNoSeeList.add(publishNoSee);
                    }
                    publishNoSeeQueryWrapper.eq("user_id",privacyConfig.getUserUid());
                    publishNoSeeService.remove(publishNoSeeQueryWrapper);
                    publishNoSeeService.saveOrUpdateBatch(publishNoSeeList);
                }
                boolean rsg = privacyConfigService.updateById(privacyConfig);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("PrivacyConfigServiceImpl.updatePrivacyConfig", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
