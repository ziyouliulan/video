package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.Category;

/**
 *
 */
public interface CategoryService extends IService<Category> {

}
