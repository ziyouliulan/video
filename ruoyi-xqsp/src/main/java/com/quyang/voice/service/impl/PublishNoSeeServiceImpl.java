
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.dao.PublishNoSeeMapper;
import com.quyang.voice.manage.IPublishNoSeeManage;
import com.quyang.voice.model.PublishNoSee;
import com.quyang.voice.service.IPublishNoSeeService;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ResultCode;
import com.quyang.voice.utils.ReturnJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service("publishNoSeeService")
public class PublishNoSeeServiceImpl extends ServiceImpl<PublishNoSeeMapper, PublishNoSee> implements IPublishNoSeeService {

    private static final Logger logger = LoggerFactory.getLogger(PublishNoSeeServiceImpl.class);

    @Autowired
    IPublishNoSeeService publishNoSeeService;

    @Autowired
    IPublishNoSeeManage publishNoSeeManage;

    @Autowired
    PublishNoSeeMapper publishNoSeeMapper;

    @Override
    public ResponseUtil<PublishNoSee> getPublishNoSeeById(@PathVariable("id")Long id){
        try {
            PublishNoSee obj=publishNoSeeService.getById(id);
            return ResponseUtil.suc(obj);
        }catch (Exception e){
            logger.error("PublishNoSeeServiceImpl.getPublishNoSeeById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deletePublishNoSeeById(Long id){
        try {
            boolean rsg = publishNoSeeService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("PublishNoSeeServiceImpl.deletePublishNoSeeById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertPublishNoSee(PublishNoSee publishNoSee){
        try {
            if (null != publishNoSee) {
                boolean rsg = publishNoSeeService.save(publishNoSee);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("PublishNoSeeServiceImpl.insertPublishNoSee", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updatePublishNoSee(PublishNoSee publishNoSee){
        try {
            if (null != publishNoSee) {
                boolean rsg = publishNoSeeService.updateById(publishNoSee);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("PublishNoSeeServiceImpl.updatePublishNoSee", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }
}
