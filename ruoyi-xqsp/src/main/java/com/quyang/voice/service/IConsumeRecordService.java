
package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.ConsumeRecord;
import com.quyang.voice.model.vo.ConsumRecordIndexVo;
import com.quyang.voice.utils.ResponseUtil;
import com.quyang.voice.utils.ReturnJson;

public interface IConsumeRecordService extends IService<ConsumeRecord> {
    ResponseUtil<ConsumeRecord> getConsumeRecordById(Long id);

    ReturnJson deleteConsumeRecordById(Long id);

    ReturnJson insertConsumeRecord(ConsumeRecord consume_record);

    ReturnJson updateConsumeRecord(ConsumeRecord consume_record);


    /**
     * 查询用户收支
     * @param userId
     * @param type
     * @param changeType
     * @return
     */
    ResponseUtil<ConsumeRecord> getConsumeRecordByUserId(Integer userId,Integer type,Integer changeType,Integer pageNum,Integer pageSize);

    /**
     * 查询用户交易记录
     * @param userId
     * @param type
     * @param year
     * @param month
     * @return
     */
    ResponseUtil<ConsumRecordIndexVo> getConsumRecordList(Integer userId, Integer type, Integer year, Integer month,Integer pageNum,Integer pageSize);
    ResponseUtil<ConsumRecordIndexVo> getConsumRecordList(Integer userId, Integer type,Integer gId, Integer year, Integer month,Integer pageNum,Integer pageSize);

    /**
     * 根据id获取订单详情
     * @param tradeNo
     * @return
     */
//    ResponseUtil<ConsumeRecordDetailVo> getConsumeRecord(String tradeNo);






}
