package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.LitemallUser;

/**
 *
 */
public interface LitemallUserService extends IService<LitemallUser> {

}
