
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quyang.voice.dao.*;
import com.quyang.voice.manage.IPublishsManage;
import com.quyang.voice.model.*;
import com.quyang.voice.model.dto.ShortVideoDto;
import com.quyang.voice.model.vo.*;
import com.quyang.voice.service.*;
import com.quyang.voice.utils.*;
import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.service.ISpUsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.quyang.voice.utils.ResultCode.SERVE_ERROR;

@Service("ShortVideoService")
public class ShortVideoServiceImpl extends ServiceImpl<PublishsMapper, Publishs> implements ShortVideoService {

    private static final Logger logger = LoggerFactory.getLogger(ShortVideoServiceImpl.class);

    @Autowired
    IPublishsService publishsService;

    @Autowired
    IPublishsManage publishsManage;

    @Autowired
    PublishsMapper publishsMapper;

    @Autowired
    PublishsLikeMapper publishsLikeMapper;

    @Autowired
    PublishCommentsMapper publishCommentsMapper;

    @Autowired
    PublishCommentsReplyMapper publishCommentsReplyMapper;


    @Autowired
    PublishsBrowsingMapper publishsBrowsingMapper;


    @Autowired
    CommentMessageHelperLogMapper commentMessageHelperLogMapper;

//    @Autowired
//    UsersMapper usersMapper;

    @Autowired
    IPrivacyConfigService privacyConfigService;

    @Autowired
    IPublishNoSeeService publishNoSeeService;

    @Autowired
    IPublishCommentsReplyService publishCommentsReplyService;

//    @Autowired
//    MissuUsersMapper missuUsersMapper;
//

    @Autowired
    ISpUsersService usersService;

    @Autowired
    LitemallGoodsService goodsService;


    @Autowired
    UserBlackMapper userBlackMapper;

    @Autowired
    ISpUsersService spUsersService;

    @Autowired
    DynamicDao dynamicDao;
    @Override
    public ResponseUtil<PublishInfoVo> getPublishsById(Long id, Integer checkUserId) {
        try {
            PublishInfoVo publishInfoVo = new PublishInfoVo();
            PublishsVo obj = publishsMapper.selectPublishInfo(id);
            if(obj==null){
                return ResponseUtil.fail(ResultCode.NO_PUBLISH);
            }

            //这条动态是不是自己的
            //如果动态是本人所发，状态为2
            if (checkUserId.longValue() == obj.getUserId().longValue()){
                obj.setIsFocus(2);
            }

            //加入这条动态的评论量
            Integer commentNum = publishsMapper.selectCommentNum(id);
            obj.setCommentNum(commentNum);
            //加入这条动态的点赞量
            Integer goodNum = publishsMapper.selectGoodNum(id);
            obj.setLikeNum(goodNum);

            //查看者是否喜欢
            PublishsLike isLike = publishsLikeMapper.selectOne(new QueryWrapper<PublishsLike>().eq("publish_id",id)
                    .eq("liked_post_id",checkUserId).eq("status",1));
            if(StringUtils.isEmpty(isLike)){
                obj.setIsLike(false);
            }else{
                if (isLike.getStatus() ==1){
                    obj.setIsLike(true);
                }else {
                    obj.setIsLike(false);
                }

            }

            //加一天查看的记录到t_publishs_browsing
            PublishsBrowsing publishsBrowsing = new PublishsBrowsing();
            publishsBrowsing.setUpdateTime(new Date());
            publishsBrowsing.setUserId(checkUserId);
            publishsBrowsing.setPublishId(id);

            //如果已经有浏览记录则直接更新浏览时间
            QueryWrapper<PublishsBrowsing> publishsBrowsingQueryWrapper = new QueryWrapper<>();
            publishsBrowsingQueryWrapper.eq("publish_id",id);
            publishsBrowsingQueryWrapper.eq("user_id",checkUserId);
            List<PublishsBrowsing> publishsBrowsingList  = publishsBrowsingMapper.selectList(publishsBrowsingQueryWrapper);
            if (publishsBrowsingList != null && publishsBrowsingList.size()> 0){
                UpdateWrapper<PublishsBrowsing> publishsBrowsingUpdateWrapper = new UpdateWrapper<>();
                publishsBrowsingUpdateWrapper.eq("publish_id",id);
                publishsBrowsingUpdateWrapper.eq("user_id",checkUserId);
                publishsBrowsingMapper.update(publishsBrowsing,publishsBrowsingUpdateWrapper);
            }else {
                publishsBrowsingMapper.insert(publishsBrowsing);
            }

            //查询动态的评论和评论回复

            //得到所有评论
            List<PublishComments> publishComments=publishCommentsMapper.getPublishCommentsMapper(id);
            //装下所有评论的List
            List<PublishCommentsVo> topicNodes = new ArrayList<>();
            if (publishComments.size() !=0 ) {
                publishInfoVo.setIsComments(1);
                for(PublishComments temp : publishComments){
                    String userAvatar = publishCommentsMapper.getUserAvatar(temp.getUserId());
                    temp.setUserAvatar(userAvatar);
                    PublishCommentsVo PublishCommentsVo = new PublishCommentsVo();
                    PublishCommentsVo.setPublishComments(temp);
                    Long ad = temp.getId();
                    //找到是这个评论的所有回复
                    List<PublishCommentsReply> prpl = publishCommentsReplyService.getPublishCommentsReplyByCommentId(ad,checkUserId);
                    PublishCommentsVo.setReplays(prpl);
                    topicNodes.add(PublishCommentsVo);
                }
                publishInfoVo.setPublishCommentsVo(topicNodes);
            }else{
                publishInfoVo.setIsComments(0);
            }
            publishInfoVo.setPublishsVo(obj);

            return ResponseUtil.suc(publishInfoVo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.getPublishsById", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deletePublishsById(String ids){
        try {
//            Long [] bbb = (Long[]) ConvertUtils.convert(ids.split(","),Long.class);
            String[] split = ids.split(",");
            for(String aaa:split){
                boolean rsg = publishsService.removeById(aaa);
                if (rsg) {
                    List<PublishComments> coms = publishCommentsMapper.selectList(new QueryWrapper<PublishComments>()
                            .eq("publish_id",Long.valueOf(aaa)));
                    //删除动态评论回复
                    for(PublishComments com:coms){
                        publishCommentsReplyMapper.delete(new QueryWrapper<PublishCommentsReply>().eq("comments_id",com.getId()));
                    }
                    //删除动态的评论
                    publishCommentsMapper.delete(new QueryWrapper<PublishComments>().eq("publish_id",aaa));

                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }
            return ReturnJson.suc(ResultCode.SUCCESS);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.deletePublishsById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertPublishs(Publishs publishs){
        try {
            if (null != publishs) {
                //判断动态类型
                //文字+图片
//                if (publishs.getPicUrl() != null && !publishs.getPicUrl().equals("") && publishs.getContent() != null && !publishs.getContent().equals("") ){
//                    publishs.setType(1);
//                }else if (publishs.getVideoCoverUrl() != null && !publishs.getVideoCoverUrl().equals("") && publishs.getContent() != null && !publishs.getContent().equals("")){
//                    publishs.setType(2);
//                }
                publishs.setType(4);
                publishs.setCreateTime(new Date());
                boolean rsg = publishsService.save(publishs);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("PublishsServiceImpl.insertPublishs", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson postPublishsLike(PublishsLike publishsLike) {
        try {
            //点赞状态，true为是，false为否
            QueryWrapper<PublishsLike> queryWrapper = new QueryWrapper<PublishsLike>();
            queryWrapper.eq("publish_id", publishsLike.getPublishId());
            queryWrapper.eq("liked_post_id", publishsLike.getLikedPostId());
            PublishsLike thePublishsLike = publishsLikeMapper.selectOne(queryWrapper);
            if (thePublishsLike == null) {
                //当数据库中不存在时候,新增一条记录
                publishsLike.setCreateTime(new Date());
                publishsLikeMapper.insert(publishsLike);
                return ReturnJson.suc(ResultCode.SUCCESS);
            }
            else {
                //当数据表中有它的点赞的记录时候, 修改之前点赞的信息
                publishsLike.setCreateTime(new Date());
                publishsLike.setId(thePublishsLike.getId());
                publishsLikeMapper.updateById(publishsLike);
                return ReturnJson.suc(ResultCode.SUCCESS);
            }
        } catch (Exception e) {
            logger.info(String.valueOf(e));
            e.printStackTrace();
            return ReturnJson.fail(SERVE_ERROR);
        }
    }

    @Override
    public ReturnJson insertPublishComments(PublishComments publishComments) {
        try {
            if (null != publishComments) {
                if(publishComments.getCommentPic() != null && publishComments.getCommentPic().length() != 0){
                    String picName = publishComments.getCommentPic().split("/")[publishComments.getCommentPic().split("/").length-1];
                    publishComments.setCommentPicName(picName);
                    SpUsers spUsers = spUsersService.getById(publishComments.getUserId());
                    publishComments.setUserName(spUsers.getUserName());
                    publishComments.setUserAvatar(spUsers.getUserImg());

                }


                publishComments.setCreateTime(SafeUtils.getNowDate());
                int rsg = publishCommentsMapper.insert(publishComments);
                if (rsg>0) {

                    //插入评论消息助手日志
//                    CommentMessageHelperLog commentMessageHelperLog = new CommentMessageHelperLog();
//                    commentMessageHelperLog.setCreateTime(new Date());
//                    MissuUsers users = missuUsersMapper.selectById(publishComments.getUserId());//发表评论的用户
//                    Publishs publishs = publishsMapper.selectById(publishComments.getPublishId());//该动态信息
//                    commentMessageHelperLog.setHandlerId(publishs.getUserId());
//                    if(publishs.getContent().length()>=5){
//                        commentMessageHelperLog.setMessage(users.getNickname()+"评论了你的动态 "+publishs.getContent().substring(0,5));
//                    }else{
//                        commentMessageHelperLog.setMessage(users.getNickname()+"评论了你的动态 "+publishs.getContent());
//                    }
////                    commentMessageHelperLog.setMsgType();
//                    commentMessageHelperLog.setPublishId(publishs.getId());
//                    commentMessageHelperLog.setStatus(0);
//                    commentMessageHelperLog.setUserId(users.getUserUid().longValue());
//                    commentMessageHelperLogMapper.insert(commentMessageHelperLog);

                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("PublishsServiceImpl.insertOrganizes", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertPublishCommentsReply(PublishCommentsReply publishCommentsReply,Long publishId) {
        try {
            if (null != publishCommentsReply) {
                publishCommentsReply.setCreateDate(new Date());
                if(publishCommentsReply.getReplyCommentPic() != null && publishCommentsReply.getReplyCommentPic().length() != 0){
                    String picName = publishCommentsReply.getReplyCommentPic().split("/")[publishCommentsReply.getReplyCommentPic().split("/").length-1];
                    publishCommentsReply.setReplyCommentPicName(picName);
                }

                Publishs publishs = publishsMapper.selectById(publishId);
                //判断被回复用户是不是作者
//                if (publishs != null && publishs.getUserId() != publishCommentsReply.getTouid()){
//                    publishCommentsReply.setIsCommentTouid(1);
//                }
//                //判断回复用户是不是楼主作者
//                if (publishs != null && publishs.getUserId() != publishCommentsReply.getUid()){
//                    publishCommentsReply.setIsCommentUid(1);
//                }
                int rsg = publishCommentsReplyMapper.insert(publishCommentsReply);
                if (rsg>0) {

                    //插入评论消息助手日志
                    CommentMessageHelperLog commentMessageHelperLog = new CommentMessageHelperLog();
                    commentMessageHelperLog.setCreateTime(new Date());
//                    Users users = usersMapper.selectByUserId(publishCommentsReply.getUid());//发表回复的用户
                    SpUsers users = usersService.getById(publishCommentsReply.getUid());
                    commentMessageHelperLog.setHandlerId(publishCommentsReply.getTouid());
                    commentMessageHelperLog.setMessage(users.getUserNickname()+"回复了你");
//                    commentMessageHelperLog.setMsgType();
                    commentMessageHelperLog.setPublishId(publishId);
                    commentMessageHelperLog.setStatus(0);
                    commentMessageHelperLog.setUserId(publishCommentsReply.getUid());
                    commentMessageHelperLogMapper.insert(commentMessageHelperLog);

                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("PublishsServiceImpl.insertOrganizes", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deletePublishCommentsReplyById(Long id) {

        try {
            PublishCommentsReply reply = publishCommentsReplyMapper.selectById(id);
            if(!StringUtils.isEmpty(reply.getReplyCommentPic())){
//                storageService.delete(reply.getReplyCommentPicName());
            }
            int rsg = publishCommentsReplyMapper.deleteById(id);
            if (rsg==1) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("PublishsServiceImpl.deleteOrganizesById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson deletePublishCommentsById(Long id) {

        try {
            int rsg = publishCommentsMapper.deleteById(id);
            if (rsg==1) {
                QueryWrapper<PublishCommentsReply> queryWrapper =new QueryWrapper<PublishCommentsReply>();
                queryWrapper.eq("comments_id",id);
                publishCommentsReplyMapper.delete(queryWrapper);
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("PublishsServiceImpl.deleteOrganizesById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishsVo>> getPublishsList(Integer checkUserId, Integer pageNum, Integer pageSize) {
        try {

            //查找我不看的人的动态，已经将我拉黑的
            List<Integer> list = new ArrayList<>();
            //1:不看他人动态 2：不让别人看我的动态

            //查看我不想看的人
            DynamicExample dynamicExample = new DynamicExample();
            dynamicExample.createCriteria().andUserIdEqualTo(checkUserId).andTypeEqualTo(1);
            List<Dynamic> dynamics = dynamicDao.selectByExample(dynamicExample);
            //查看不想让我看的人

            dynamicExample = new DynamicExample();
            dynamicExample.createCriteria().andBlackIdEqualTo(checkUserId).andTypeEqualTo(2);
            List<Dynamic> dynamic = dynamicDao.selectByExample(dynamicExample);

            List<UserBlack> blackList1 = userBlackMapper.selectList
                    (new QueryWrapper<UserBlack>().eq("user_id",checkUserId));


            List<UserBlack> blackList2 = userBlackMapper.selectList
                    (new QueryWrapper<UserBlack>().eq("black_id",checkUserId));


//            List<PublishNoSee> publishNoSeeList = publishNoSeeService.list(new QueryWrapper<PublishNoSee>().eq("user_id",checkUserId));
            ArrayList<Integer> types = new ArrayList<>();
            types.add(4);
            PageHelper.startPage(pageNum, pageSize);
            List<PublishsVo> obj = publishsMapper.getPublishsOtherList(dynamics,dynamic, blackList1,blackList2,checkUserId,types,null,null);
            for (PublishsVo pa: obj){

                List<Long> isLike = publishsMapper.getPublishsLike(checkUserId,pa.getId(),pa.getUserId());
                if (isLike!= null && isLike.size()>0){
                    pa.setIsLike(true);
                }
                Integer goodNum = publishsMapper.selectGoodNum(pa.getId());
                Integer commentNum = publishsMapper.selectCommentNum(pa.getId());

                if (pa.getPicUrl() != null && !pa.getPicUrl().equals("")){
                    List<String> picList = Arrays.asList(pa.getPicUrl().split(","));
                    pa.setPicUrlList(picList);
                }
                pa.setLikeNum(goodNum);
                pa.setCommentNum(commentNum);
            }
            PageInfo<PublishsVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.getPublishsList", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishsVo>> getPublishsList(Integer checkUserId, Integer pageNum, Integer pageSize, String regionCode) {
        try {

            //查找我不看的人的动态，已经将我拉黑的
            List<Integer> list = new ArrayList<>();
            //1:不看他人动态 2：不让别人看我的动态

            //查看我不想看的人
            DynamicExample dynamicExample = new DynamicExample();
            dynamicExample.createCriteria().andUserIdEqualTo(checkUserId).andTypeEqualTo(1);
            List<Dynamic> dynamics = dynamicDao.selectByExample(dynamicExample);
            //查看不想让我看的人

            dynamicExample = new DynamicExample();
            dynamicExample.createCriteria().andBlackIdEqualTo(checkUserId).andTypeEqualTo(2);
            List<Dynamic> dynamic = dynamicDao.selectByExample(dynamicExample);

            List<UserBlack> blackList1 = userBlackMapper.selectList
                    (new QueryWrapper<UserBlack>().eq("user_id",checkUserId));


            List<UserBlack> blackList2 = userBlackMapper.selectList
                    (new QueryWrapper<UserBlack>().eq("black_id",checkUserId));


//            List<PublishNoSee> publishNoSeeList = publishNoSeeService.list(new QueryWrapper<PublishNoSee>().eq("user_id",checkUserId));
            ArrayList<Integer> types = new ArrayList<>();
            types.add(4);
            PageHelper.startPage(pageNum, pageSize);
            List<PublishsVo> obj = publishsMapper.getPublishsOtherList(dynamics,dynamic, blackList1,blackList2,checkUserId,types,null,regionCode);
            for (PublishsVo pa: obj){

                if (pa.getGoodsId() !=null){
                    LitemallGoods goods = goodsService.getById(pa.getGoodsId());
                    if (goods != null){

                        pa.setGallery(goods.getGallery());
                        pa.setShopId(goods.getUserId());
                        pa.setCounterPrice(goods.getCounterPrice());
                        pa.setRetailPrice(goods.getRetailPrice());
                        pa.setVipPrice(goods.getVipPrice());
                        pa.setProductName(goods.getName());
                    }


                }

                Boolean publishsLikes = publishsMapper.getPublishsLikes(checkUserId, pa.getId());
                if (publishsLikes  != null){
                    pa.setIsLike(publishsLikes);
                }else {
                    pa.setIsLike(false);
                }

//                if (isLike!= null && isLike.size()>0){
//                    pa.setIsLike(isLike.get(0).intValue()== 1?true:false);
//                }else {
//                    pa.setIsLike(false);
//                }
                Integer goodNum = publishsMapper.selectGoodNum(pa.getId());
                Integer commentNum = publishsMapper.selectCommentNum(pa.getId());

                if (pa.getPicUrl() != null && !pa.getPicUrl().equals("")){
                    List<String> picList = Arrays.asList(pa.getPicUrl().split(","));
                    pa.setPicUrlList(picList);
                }
                pa.setLikeNum(goodNum);
                pa.setCommentNum(commentNum);
            }
            PageInfo<PublishsVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.getPublishsList", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishsVo>> getPublishsList(ShortVideoDto shortVideoDto) {
        try {
//            List<PublishNoSee> publishNoSeeList = publishNoSeeService.list(new QueryWrapper<PublishNoSee>().eq("user_id",checkUserId));
            ArrayList<Integer> types = new ArrayList<>();
            types.add(4);
            PageHelper.startPage(shortVideoDto.getPageNum(),shortVideoDto.getPageSize());
            List<PublishsVo> obj = new ArrayList<PublishsVo>();
            if ("1".equals(shortVideoDto.getType())){
                obj = publishsMapper.getPublishsOtherLiskLists(types,shortVideoDto);
            }else {
                obj = publishsMapper.getPublishsOtherLists(types,shortVideoDto);
            }

            for (PublishsVo pa: obj){





                Boolean publishsLikes = publishsMapper.getPublishsLikes(shortVideoDto.getCheckUserId(), pa.getId());
                if (publishsLikes  != null){
                    pa.setIsLike(publishsLikes);
                }else {
                    pa.setIsLike(false);
                }


                Integer goodNum = publishsMapper.selectGoodNum(pa.getId());
                Integer commentNum = publishsMapper.selectCommentNum(pa.getId());

                if (pa.getPicUrl() != null && !pa.getPicUrl().equals("")){
                    List<String> picList = Arrays.asList(pa.getPicUrl().split(","));
                    pa.setPicUrlList(picList);
                }
                pa.setLikeNum(goodNum);
                pa.setCommentNum(commentNum);
            }
            PageInfo<PublishsVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.getPublishsList", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishCommentsVo>> queryPublishComments(Integer pageNum, Integer pageSize,Long id) {
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<PublishCommentsVo> obj = publishsMapper.queryPublishComments(id);

            if (obj.size() !=0 ) {
                for(PublishCommentsVo temp : obj){

                    List<PublishCommentsReplyVo> reply = publishsMapper.queryPublishCommentsReply(temp.getId());
                    temp.setReplyVos(reply);
                    //加入这条动态的评论量
                    Integer commentNum = publishsMapper.selectCommentNum(id);

                }
            }
            PageInfo<PublishCommentsVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.queryPublishComments", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishsListVo>> getMyPublishList(Integer pageNum, Integer pageSize, Long userId) {
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<PublishsListVo> obj = publishsMapper.getMyShortVideoList(userId);
            PageInfo<PublishsListVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.queryPublishComments", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishsListVo>> getOtherPublishList(Integer pageNum, Integer pageSize, Long userId) {
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<PublishsListVo> obj = publishsMapper.getMyShortVideoList(userId);
            PageInfo<PublishsListVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.queryPublishComments", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ResponseUtil<PageInfo<PublishsVo>> getPublishsByUser(Integer pageNum, Integer pageSize, Integer userId,Integer toUserId) {
        try {
            //查找我不看的人的动态，已经将我拉黑的
//            List<Integer> list = new ArrayList<>();
//            List<PublishNoSee> publishNoSeeList = publishNoSeeService.list(new QueryWrapper<PublishNoSee>().eq("user_id",userId));

            //查看我不想看的人
            DynamicExample dynamicExample = new DynamicExample();
            dynamicExample.createCriteria().andUserIdEqualTo(userId).andTypeEqualTo(1);
            List<Dynamic> dynamics = dynamicDao.selectByExample(dynamicExample);


            //查看不想让我看的人

            dynamicExample = new DynamicExample();
            dynamicExample.createCriteria().andBlackIdEqualTo(userId).andTypeEqualTo(2);
            List<Dynamic> dynamic = dynamicDao.selectByExample(dynamicExample);


            List<UserBlack> blackList1 = userBlackMapper.selectList
                    (new QueryWrapper<UserBlack>().eq("user_id",userId));


            List<UserBlack> blackList2 = userBlackMapper.selectList
                    (new QueryWrapper<UserBlack>().eq("black_id",userId));
            ArrayList<Integer> types = new ArrayList<>();
            types.add(4);

            PageHelper.startPage(pageNum, pageSize);
            List<PublishsVo> obj = publishsMapper.getPublishsList(dynamics,dynamic,blackList1,blackList2,userId,types,toUserId );
            for (PublishsVo pa: obj){
//
//                List<Long> isLike = publishsMapper.getPublishsLike(userId,pa.getId(),pa.getUserId());
//                if (isLike!= null && isLike.size()>0){
//                    pa.setIsLike(true);
//                }
//

                Boolean publishsLikes = publishsMapper.getPublishsLikes(userId, pa.getId());
                if (publishsLikes  != null){
                    pa.setIsLike(publishsLikes);
                }else {
                    pa.setIsLike(false);
                }



                Integer goodNum = publishsMapper.selectGoodNum(pa.getId());
                Integer commentNum = publishsMapper.selectCommentNum(pa.getId());

                pa.setCommentNum(goodNum);
                pa.setCommentNum(commentNum);
            }
            PageInfo<PublishsVo> pageInfo = new PageInfo<>(obj);
            return ResponseUtil.suc(pageInfo);
        }catch (Exception e){
            logger.error("PublishsServiceImpl.getPublishsList", e);
            return ResponseUtil.fail(ResultCode.SYSTEM_ERROR);
        }
    }


    @Override
    public ResponseUtil<Map> queryPublishDetails(Integer userId) {

//        MissuUsers missuUsers = missuUsersMapper.selectById(userId);

        SpUsers users = usersService.getById(userId);

        Integer praise = publishsMapper.getPraise(userId);
        List<Publishs> likes = publishsMapper.getLikes(userId);
        QueryWrapper<Publishs> queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userId);
        List<Publishs> publishs = publishsMapper.selectList(queryWrapper);


        HashMap map = new HashMap();
        map.put("nickname",users.getUserNickname());
        map.put("accountNumber",users.getUserName());
        map.put("userDesc","");
        map.put("age","");
        map.put("sex",null);
        map.put("province","");
        map.put("city","");
        map.put("region","");
        map.put("praise",praise);
//        map.put("likes",likes);
//        map.put("works",publishs);


        return ResponseUtil.suc(map);
    }
}
