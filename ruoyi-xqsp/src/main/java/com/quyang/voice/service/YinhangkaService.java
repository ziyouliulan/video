package com.quyang.voice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.quyang.voice.model.Yinhangka;

/**
* @author lpden
* @description 针对表【yuecool_yinhangka】的数据库操作Service
* @createDate 2022-01-07 15:47:01
*/
public interface YinhangkaService extends IService<Yinhangka> {

}
