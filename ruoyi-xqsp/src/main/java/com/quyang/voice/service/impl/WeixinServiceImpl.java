package com.quyang.voice.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.google.gson.Gson;
import com.quyang.voice.model.AppConfig;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.service.IAppConfigService;
import com.quyang.voice.service.IMissuUsersService;
import com.quyang.voice.service.WeixinService;
import com.quyang.voice.utils.HttpURLConnectionUtil;
import lombok.Data;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.util.*;

import static com.quyang.voice.utils.MD5Util.byteToHexStringForMD5;

@Service
public class WeixinServiceImpl implements WeixinService {


    @Autowired
    IMissuUsersService missuUsersService;

    @Autowired
    IAppConfigService appConfigService;


    final String CODE_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?";

    final String USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?";

    /**
     * 获取 微信用户 openId
     *
     * @return
     */
    public String getWxOpenId(String code) {
        AppConfig byId = appConfigService.getById(1L);
//        appid=APPID&
//                secret=SECRET&
//                code=CODE&
//                grant_type=authorization_code

        TreeMap<String, String> params = new TreeMap<>();
        params.put("grant_type", "authorization_code");
        params.put("appid", byId.getAppId());
        params.put("secret",  byId.getAppSecret());
        params.put("code", code);
//        CODE_URL + createLinkStringByGet(params)
//        https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
//        String param="appid="+AppID+"&secret="+AppSecret+"&code="+code+"&grant_type=authorization_code";
        String json = HttpURLConnectionUtil.doGet(CODE_URL+createLinkStringByGet(params));

        return json;
    }
//    {
//        "access_token": "ACCESS_TOKEN",
//            "expires_in": 7200,
//            "refresh_token": "REFRESH_TOKEN",
//            "openid": "OPENID",
//            "scope": "SCOPE"
//    }

    @Data
    public class WeixinModel{
        String access_token;//	调用接口凭证
        String expires_in;//	access_token 接口调用凭证超时时间，单位（秒）
        String refresh_token;//用户刷新 access_token
        String openid;//普通用户的标识，对当前开发者帐号唯一
        String scope;//用户授权的作用域，使用逗号（,）分隔
        String errcode;
        String errmsg;
        String nickname;//	普通用户昵称
        Integer sex;//普通用户性别，1 为男性，2 为女性
        String province;//普通用户个人资料填写的省份
        String city;//普通用户个人资料填写的城市
        String country;//	国家，如中国为 CN
        String headimgurl;//	用户头像，最后一个数值代表正方形头像大小（有 0、46、64、96、132 数值可选，0 代表 640*640 正方形头像），用户没有头像时该项为空
        String[] privilege;//用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
        String unionid;//用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的

//        {"openid":"of3ml5k7rh8Ji0Iir6_sHH95TKxk",
//                "nickname":"莫宰羊",
//                "sex":0,"language":"zh_CN"
//                ,"city":"","province":"","country":"",
//                "headimgurl":"https:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/wDYorU27jz79uF858Fuy2ht2ksLibcqg2biaqficjwpZmKWeN0mu3X3VtgticoZB1Xicm7c94210WhC89v46aAN1Wow\/132",
//                "privilege":[],"unionid":"o2uci6NLD0D5kxJMb_ohq89iTfj4"}

    }



    public static String createLinkStringByGet(Map<String, String> params) {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = null;
        try {
            prestr = "";
            for (int i = 0; i < keys.size(); i++) {
                String key = keys.get(i);
                String value = params.get(key);
//                value = URLEncoder.encode(value, "UTF-8");
                if (i == keys.size() - 1) {//拼接时，不包括最后一个&字符
                    prestr = prestr + key + "=" + value;
                } else {
                    prestr = prestr + key + "=" + value + "&";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prestr;
    }

    // 不带秘钥加密
    public static String md52(String text) throws Exception {
        // 加密后的字符串
        String md5str = DigestUtils.md5Hex(text);
//        System.out.println("MD52加密后的字符串为:" + md5str + "\t长度：" + md5str.length());
        return md5str;
    }
    public static  String encyptPassword(String originalPsw, String DEFAULT_PASSWORD) {
        if (originalPsw == null) {
            return null;
        } else {
            if (!DEFAULT_PASSWORD.equals(originalPsw)) {
                try {
                    return byteToHexStringForMD5(MessageDigest.getInstance("MD5").digest(originalPsw.getBytes()));
                } catch (Exception var3) {
                    var3.printStackTrace();
                }
            }

            return originalPsw;
        }
    }
    public static void main(String[] args) {
   /*     WeixinServiceImpl weixinService = new WeixinServiceImpl();
        MissuUsers ds = weixinService.crateUser("ds");
        System.out.println(ds);*/
//        ArrayList arrayList = new ArrayList();
//
//        HashMap map = new HashMap();
//        map.put("name","姓名");
//        map.put("phone","手机号");
//        arrayList.add(map);
//        arrayList.add(map);
//        Gson gson = new Gson();
//        String s = gson.toJson(arrayList);
//        System.out.println(s);
        String s = convertMD5("8c7dc2b4da66bea74155a26b8f8ec2a0");
        System.out.println(s);


    }


    /**
     * 加密解密算法 执行一次加密，两次解密
     */
    public static String convertMD5(String inStr){

        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++){
            a[i] = (char) (a[i] ^ 't');
        }
        String s = new String(a);
        return s;

    }
}
