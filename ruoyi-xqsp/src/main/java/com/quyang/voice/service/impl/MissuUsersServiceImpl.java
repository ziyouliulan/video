
package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.quyang.voice.dao.MissuUsersMapper;
import com.quyang.voice.manage.IMissuUsersManage;
import com.quyang.voice.model.MissuUsers;
import com.quyang.voice.model.vo.MissuUsersBaseVo;
import com.quyang.voice.service.IMissuUsersService;
import com.quyang.voice.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("missuUsersService")
public class MissuUsersServiceImpl extends ServiceImpl<MissuUsersMapper, MissuUsers> implements IMissuUsersService {

    private static final Logger logger = LoggerFactory.getLogger(MissuUsersServiceImpl.class);

    @Autowired
    IMissuUsersService missuUsersService;

    @Autowired
    IMissuUsersManage missuUsersManage;

    @Autowired
    MissuUsersMapper missuUsersMapper;



    @Override
    public ReturnJson deleteMissuUsersById(Long id){
        try {
            boolean rsg = missuUsersService.removeById(id);
            if (rsg) {
                return ReturnJson.suc(ResultCode.SUCCESS);
            }else {
                return ReturnJson.fail(ResultCode.FAIL);
            }
        }catch (Exception e){
            logger.error("MissuUsersServiceImpl.deleteMissuUsersById", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson insertMissuUsers(MissuUsers missuUsers){
        try {
            if (null != missuUsers) {
                boolean rsg = missuUsersService.save(missuUsers);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("MissuUsersServiceImpl.insertMissuUsers", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson updateMissuUsers(MissuUsers missuUsers){
        try {
            if (null != missuUsers) {
                boolean rsg = missuUsersService.updateById(missuUsers);
                if (rsg) {
                    return ReturnJson.suc(ResultCode.SUCCESS);
                }else {
                    return ReturnJson.fail(ResultCode.FAIL);
                }
            }else {
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        } catch (Exception e) {
            logger.error("MissuUsersServiceImpl.updateMissuUsers", e);
            return ReturnJson.fail(ResultCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ReturnJson checkRepeat(String username, String phone) {
        if(StringUtils.isNull(username)||StringUtils.isBlank(username)){
            if (StringUtils.isNull(phone)||StringUtils.isBlank(phone)){
                return ReturnJson.fail(ResultCode.PARAM_IS_INVALID);
            }
        }
        QueryWrapper wrapper=new QueryWrapper();
        if (StringUtils.isNotNull(username)&&StringUtils.isNotBlank(username)){
            wrapper.eq("user_mail",username);
        }
        if (StringUtils.isNotNull(phone)&&StringUtils.isNotBlank(phone)){
            wrapper.eq("user_phone",phone);
        }
        int count=missuUsersMapper.selectCount(wrapper);
        if (count>0){
            return ReturnJson.fail(ResultCode.PHONE_HAS_EXISTED);
        }else {
            return ReturnJson.suc(ResultCode.SUCCESS);
        }
    }



    @Override
    public MissuUsersBaseVo getUserBase(Integer userUid) {
        try {
            QueryWrapper<MissuUsers> usersQueryWrapper = new QueryWrapper<>();
            usersQueryWrapper.eq("user_uid",userUid);
            MissuUsers user=missuUsersMapper.selectOne(usersQueryWrapper);
            MissuUsersBaseVo missuUsersBaseVo = new MissuUsersBaseVo();
                missuUsersBaseVo.setUserUid(user.getUserUid());
                missuUsersBaseVo.setUserAvatarFileName(user.getUserAvatarFileName());
                missuUsersBaseVo.setNickname(user.getNickname());
                missuUsersBaseVo.setUserType(user.getUserType());
                missuUsersBaseVo.setUserPhone(user.getUserPhone());
            return missuUsersBaseVo;
        } catch (Exception e) {
            logger.error("UsersServiceImpl.getUserBaseById", e);
            return null;
        }
    }


}
