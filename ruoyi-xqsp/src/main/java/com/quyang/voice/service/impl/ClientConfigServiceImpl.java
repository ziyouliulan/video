package com.quyang.voice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quyang.voice.model.ClientConfig;
import com.quyang.voice.model.GroupConfig;
import com.quyang.voice.service.ClientConfigService;
import com.quyang.voice.mapper.ClientConfigMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
* @author lpden
* @description 针对表【client_config(参数配置表)】的数据库操作Service实现
* @createDate 2022-04-24 13:50:30
*/
@Service
public class ClientConfigServiceImpl extends ServiceImpl<ClientConfigMapper, ClientConfig>
    implements ClientConfigService{


    @Override
    public HashMap configToMap() {
        List<ClientConfig> list = list(null);
        HashMap map = new HashMap();
        for (ClientConfig groupConfig: list
        ) {

            map.put(groupConfig.getConfigKey(),groupConfig.getConfigValue());

        }
        return map;
    }

}




