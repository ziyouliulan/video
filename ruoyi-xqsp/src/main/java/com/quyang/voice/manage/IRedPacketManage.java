
package com.quyang.voice.manage;

import com.quyang.voice.model.dto.RedPacketDto;

public interface IRedPacketManage {

    /**
     * 将红包存入redis
     * @param orderId
     * @param redPacketDto
     * @return
     */
    Boolean setRedPack(String orderId, RedPacketDto redPacketDto);

    /**
     * 抢红包
     *
     * @param userId
     * @param orderId
     * @return
     */
     Integer robRedPack(Integer userId, String orderId);

    /**
     * 校验是否抢完
     * @param orderId
     * @return
     */
    Boolean check(String orderId);

    /**
     * 获得已经被抢的红包数量
     * @param orderId
     * @return
     */
    Long getRobNum(String orderId);

    /**
     * 判断是否已经抢过
     * @param orderId
     * @param userId
     * @return
     */
    Boolean checkRob(String orderId,Integer userId);
}