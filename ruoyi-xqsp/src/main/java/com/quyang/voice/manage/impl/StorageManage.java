package com.quyang.voice.manage.impl;

import com.quyang.voice.dao.StorageMapper;
import com.quyang.voice.manage.IStorageManage;
import com.quyang.voice.model.TStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("storageManage")
public class StorageManage extends BaseManage implements IStorageManage {

    @Autowired
    private StorageMapper storageMapper;

    @Override
    public TStorage findByKey(String key) {
        return storageMapper.findByKey(key);
    }

    @Override
    public int add(TStorage tStorage) {
        return storageMapper.add(tStorage);
    }

    @Override
    public int delete(String key) {
        return storageMapper.delete(key);
    }

    @Override
    public TStorage downloadFileByName(String fileName) {
        return storageMapper.downloadFileByName(fileName);
    }

    @Override
    public TStorage downloadById(Long id) {
        return storageMapper.downloadById(id);
    }
}
