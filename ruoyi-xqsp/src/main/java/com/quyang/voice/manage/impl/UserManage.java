//package com.quyang.voice.manage.impl;
//
//import com.quyang.voice.cache.redis.IRedisClient;
//import com.quyang.voice.dao.UsersMapper;
//import com.quyang.voice.manage.IUserManage;
//import com.quyang.voice.model.Users;
//import com.quyang.voice.utils.RedisKeyConstants;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//@Repository("userManage")
//public class UserManage extends BaseManage implements IUserManage {
//
////    @Autowired
////    IRedisClient userManageClient;
//
//    @Autowired
//    UsersMapper usersMapper;
//
////	public void setUserToRedis(String phone, Users user) {
////		userManageClient.setTimeout(RedisKeyConstants.R_USER_KEY + phone, myRedisUtil.getJsonFromObject(user, user.getClass()), 60*60*48);
////	}
//
//	@Override
//	public int setUser(Users user) {
//		return usersMapper.insert(user);
//	}
//
//	@Override
//	public int updatePasswd(String passwd, Long id) {
//		return usersMapper.updatePasswdById(id,passwd);
//	}
//
//	@Override
//	public Users getUsersById(Long id) {
//		return usersMapper.selectByPrimaryKey(id);
//	}
//
//	@Override
//	public Users getUsersByASysId(String sysId) {
//		return usersMapper.getUsersByASysId(sysId);
//	}
//
//
//    @Override
//    public Users getUserByPhone(String phone) {
//        return usersMapper.selectByPhone(phone);
//    }
//
////	@Override
////	public Users getUserByPhoneFromRedis(String phone) {
////		return myRedisUtil.getObjectByKey(userManageClient, RedisKeyConstants.R_USER_KEY+phone, Users.class);
////	}
//
//	@Override
//	public Users getUserByPhoneOrSysid(String account) {
//		return usersMapper.selectByPhoneOrSysid(account);
//	}
//
//	@Override
//	public int getMyStatus(Long id, int status) {
//		return usersMapper.getMyStatus(id,status);
//	}
//
//	@Override
//	public int changePhone(Long id, String phone) {
//		return usersMapper.changePhone(id, phone);
//	}
//
//	@Override
//	public int changeSafeModeStatus(Long id, Integer safeModeStatus) {
//		return usersMapper.changeSafeModeStatus(id, safeModeStatus);
//	}
//
//	@Override
//	public int changeSafeModePwd(Long id, String safeModePwd) {
//		return usersMapper.changeSafeModePwd(id,safeModePwd);
//	}
//
//	@Override
//	public int updateProfile(Users users) {
//		return usersMapper.updateProfile(users);
//	}
//
//}
