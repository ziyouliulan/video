//package com.quyang.voice.manage;
//
//import com.quyang.voice.model.Users;
//
//public interface IUserManage {
//
//    void setUserToRedis(String phone, Users user);
//
//    int setUser(Users user);
//
//    int updatePasswd(String passwd, Long id);
//
//    Users getUsersById(Long id);
//
//    Users getUserByPhone(String phone);
//
//    Users getUserByPhoneFromRedis(String phone);
//
//    Users getUserByPhoneOrSysid(String account);
//
//    int getMyStatus(Long id, int status);
//
//    int changePhone(Long id, String phone);
//
//    int changeSafeModeStatus(Long id, Integer safeModeStatus);
//
//    int changeSafeModePwd(Long id, String safeModePwd);
//
//    int updateProfile(Users users);
//
//    Users getUsersByASysId(String sysId);
//}
