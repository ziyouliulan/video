package com.quyang.voice.manage;

import com.quyang.voice.model.TStorage;

public interface IStorageManage {

    TStorage findByKey(String key);

    int add(TStorage tStorage);

    int delete(String key);

    TStorage downloadFileByName(String fileName);

    TStorage downloadById(Long id);
}
