package com.quyang.voice.manage.impl;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseManage {

//    @Autowired
//    protected MyRedisUtil myRedisUtil;

    @Autowired
    protected Gson gson;
//
//    public void setMyRedisUtil(MyRedisUtil myRedisUtil) {
//        this.myRedisUtil = myRedisUtil;
//    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
