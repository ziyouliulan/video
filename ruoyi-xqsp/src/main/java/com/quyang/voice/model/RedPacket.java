
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(红包实体类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("TRedPacket")
@TableName(value = "red_packet")
@AllArgsConstructor
@NoArgsConstructor
public class RedPacket implements Serializable {

	private static final long serialVersionUID = 1613613409994L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Long id;

	@ApiModelProperty(name = "orderId", value = "红包的唯一标识")
	private String orderId;

    @ApiModelProperty(name = "userId", value = "用户id")
	private Integer userId;

    @ApiModelProperty(name = "groupGid", value = "发送到那个群")
	private Integer groupGid;

    @ApiModelProperty(name = "toUserId", value = "发送给哪个用户")
	private Integer toUserId;

    @ApiModelProperty(name = "greetings", value = "祝福语")
	private String greetings;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "sendTime", value = "发送时间")
	private Date sendTime;

    @ApiModelProperty(name = "type", value = "1：普通红包  2：拼手气红包  3:口令红包")
	private Integer type;

    @ApiModelProperty(name = "count", value = "红包个数")
	private Integer count;


    @ApiModelProperty(name = "receiveCount", value = "已领取个数")
	private Integer receiveCount;

    @ApiModelProperty(name = "money", value = "红包金额")
	@JsonSerialize(using = CustomerDoubleSerialize.class)
	private Double money;

    @ApiModelProperty(name = "over", value = "红包剩余金额")
	@JsonSerialize(using = CustomerDoubleSerialize.class)
	private Double over;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "outTime", value = "超时时间")
	private Date outTime;

    @ApiModelProperty(name = "status", value = "1 ：发出   2：已领完       -1：已退款       3:未领完退款")
	private Integer status;

    @ApiModelProperty(name = "userIds", value = "领取该红包的 userId 字符串")
	private String userIds;

	@ApiModelProperty(name = "word", value = "红包口令")
	private String word;


	@ApiModelProperty(name = "coverUrl", value = "红包封面")
	private String coverUrl;


	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "outTime", value = "超时时间")
	private Date updateTime;


}
