package com.quyang.voice.model;


import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * 【请填写功能名称】对象 missu_user_remark
 *
 * @author yuecool
 * @date 2021-04-19
 */

public class MissuUserRemark implements Serializable
{
    private static final long serialVersionUID = 1L;


    private Long id;

    /** $column.columnComment */

    private Long userId;

    /** $column.columnComment */

    private Long friendId;



    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setFriendId(Long friendId)
    {
        this.friendId = friendId;
    }

    public Long getFriendId()
    {
        return friendId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("friendId", getFriendId())
            .append("remark")
            .toString();
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
