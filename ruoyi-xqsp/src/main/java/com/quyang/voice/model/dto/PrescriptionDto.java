package com.quyang.voice.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PrescriptionDto {

    private Integer id;

    /**
     * 病情描述
     */
    @ApiModelProperty(value="病情描述")
    private String desc;

    /**
     * 处方照片
     */
    @ApiModelProperty(value="处方照片")
    private String imagaUrl;

    /**
     * 处方照片
     */
    @ApiModelProperty(value="处方照片")
    private String[] imagaUrls;

    /**
     * 补充
     */
    @ApiModelProperty(value="补充")
    private String supplement;

    private static final long serialVersionUID = 1L;

}
