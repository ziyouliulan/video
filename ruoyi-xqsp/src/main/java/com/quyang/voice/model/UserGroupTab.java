package com.quyang.voice.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-05-08
 */

@Setter
@Getter
@ToString
public class UserGroupTab  implements Serializable {

	private static final long serialVersionUID =  6781192476350896324L;

	/**
	 * id
	 */

	private Long id;

	/**
	 * 群id
	 */

	private String groupId;

	/**
	 * 群标签id
	 */

	private Long groupTabId;

	/**
	 * 1:开启 0：关闭
	 */

	private String groupTabOn;

	/**
	 * 1:群页签
	 */

	private String status;

}
