//package com.quyang.voice.model;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//
//import java.io.Serializable;
//import java.time.LocalDateTime;
//
///**
// * 会员分类
// * @TableName yuecool_vip_category
// */
//@TableName(value ="yuecool_vip_category")
//@Data
//public class VipCategory implements Serializable {
//    /**
//     * id
//     */
//    @TableId(type = IdType.AUTO)
//    private Integer id;
//
//    /**
//     * 名称
//     */
//    private String name;
//
//    /**
//     * 金额
//     */
//    private Double money;
//
//    /**
//     * 多少个月
//     */
//    private Integer time;
//
//    /**
//     * 父id
//     */
//    private Integer pId;
//
//    /**
//     * 是否删除
//     */
//    private Boolean deleted;
//
//    /**
//     * 洗车次数
//     */
//    private Double number;
//
//    /**
//     * 类型：1：年付会员 2:次数会员
//     */
//    private Integer status;
//
//    /**
//     * 内容
//     */
//    private String content;
//
//    /**
//     * 语音播报
//     */
//    private String voiceBroadcast;
//
//    /**
//     * 分给商家的钱
//     */
//    private Double dividedInto;
//
//    /**
//     * 分类id
//     */
//    private Long categoryId;
//
//    /**
//     * 创建时间
//     */
//    private LocalDateTime createTime;
//
//    /**
//     * 更新时间
//     */
//    private LocalDateTime updateTime;
//
//    @TableField(exist = false)
//    private static final long serialVersionUID = 1L;
//}
