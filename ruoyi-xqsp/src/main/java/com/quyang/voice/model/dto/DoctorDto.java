package com.quyang.voice.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DoctorDto {

    @ApiModelProperty("医生Id")
    Integer userId;


    @ApiModelProperty("医生名称")
    String name;


    @ApiModelProperty("是否开方")
    String positionId;



    @ApiModelProperty("科室名称")
    String deptName;


    @ApiModelProperty("科室")
    String deptId;

    @ApiModelProperty("擅长")
    String beGoodAt;

    @ApiModelProperty("省")
    String areaId;
    @ApiModelProperty("市")
    String cityId;

    @ApiModelProperty("是否推荐医生  1： 是  0：否")
    String isRecommend;


    @ApiModelProperty("是否在线  1： 是  0：否")
    String isOnLine;


    @ApiModelProperty(" 是否开启预约问诊  1： 是  0：否")
    String appointmentManagement;




}
