package com.quyang.voice.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(钱包消息VO类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */

@Data
@ApiModel("WalletMessageVo")
@AllArgsConstructor
@NoArgsConstructor
public class WalletMessageVo implements Serializable {

    private static final long serialVersionUID = 5645649823489789L;

    @ApiModelProperty(name = "orderId",value = "订单id")
    private Long orderId;

    @ApiModelProperty(name = "coursesId",value = "课程id")
    private Long coursesId;

    @ApiModelProperty(name = "coursesId",value = "课程状态(1待上课，2已上完)")
    private Integer status;

    @ApiModelProperty(name = "coursesName",value = "课程名称")
    private String coursesName;

    @ApiModelProperty(name = "msgType",value = "消息类型(1购买课程，2发起提现，3申请退款)")
    private Integer msgType;

    @ApiModelProperty(name = "message",value = "消息")
    private String message;


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime",value = "交易时间")
    private Date createTime;
}
