package com.quyang.voice.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ShortVideoDto {


    @ApiModelProperty(name = "checkUserId", value = "当前用户id")
    Integer checkUserId;

    @ApiModelProperty(name = "pageNum", value = "当前页")
    Integer pageNum;

    @ApiModelProperty(name = "pageSize", value = "每页记录数")
    Integer pageSize;

    @ApiModelProperty(name = "regionCode", value = "地区编码")
    String regionCode;

    @ApiModelProperty(name = "nickname", value = "查询的用户名")
    String nickname;

    @ApiModelProperty(name = "name", value = "标题")
    String name;

    @ApiModelProperty(name = "type", value = "1:喜欢 2其他")
    String type;

    @ApiModelProperty(name = "toUserId", value = "需要查询的用户id")
    String toUserId;
}
