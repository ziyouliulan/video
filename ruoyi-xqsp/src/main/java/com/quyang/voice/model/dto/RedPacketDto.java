package com.quyang.voice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RedPacketDto {
    private Integer userId;

    //指定多少人抢

    private Integer count;

    //指定总金额-单位为分

    private Double money;


    private Integer moneyR;

    private Integer groupGid;

    private Integer toUserId;

    private String greetings;

    private String coverUrl;

    private Integer type;

    private String word;
    private Boolean administratorRedEnvelope = false;

}
