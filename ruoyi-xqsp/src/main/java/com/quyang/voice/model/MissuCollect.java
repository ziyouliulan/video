
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(用户消息收藏实体类)
 *
 * @version: V1.0
 * @author: sky
 *
 */
@Data
@ApiModel("TMissuCollect")
@TableName(value = "missu_collect")
@AllArgsConstructor
@NoArgsConstructor
public class MissuCollect implements Serializable {

	private static final long serialVersionUID = 1614649532756L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "id", hidden = true)
	private Long id;

    @ApiModelProperty(name = "userId", value = "收藏人id")
	private Integer userId;

    @ApiModelProperty(name = "content", value = "收藏内容")
	private String content;

    @ApiModelProperty(name = "type", value = "收藏类型 0:纯文字   1:图片   2:音频 3:赠送的礼物 4：索要礼物信息 5：文件信息  6：视频 7：名片 8：位置信息 10:发红包信息 11：撤回消息 12：禁言  90：系统消息或提示信息 ")
	private Integer type;

    @ApiModelProperty(name = "fromUserId", value = "消息发送用户id")
	private Integer fromUserId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
	private Date createTime;


}
