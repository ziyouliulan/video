package com.quyang.voice.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description:TODO(用户个人资料bodyVO类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("UserInformationBodyVo")
@AllArgsConstructor
@NoArgsConstructor
public class UserInformationBodyVo implements Serializable {

    @ApiModelProperty(name = "userId", value = "用户id")
    private Long userId;

    @ApiModelProperty(name = "sysid", value = "用户账号")
    private String sysid;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(name = "birthday", value = "用户生日")
    private Date birthday;

    @ApiModelProperty(name = "sign", value = "个性签名")
    private String sign;

    @ApiModelProperty(name = "avatar", value = "头像")
    private String avatar;

    @ApiModelProperty(name = "wealth", value = "财富值")
    private Long wealth;

    @ApiModelProperty(name = "wealthGradeUrl", value = "财富等级图片")
    private String wealthGradeUrl;

    @ApiModelProperty(name = "charm", value = "魅力值")
    private Long charm;

    @ApiModelProperty(name = "charmGradeUrl", value = "魅力等级图片")
    private String charmGradeUrl;


    @ApiModelProperty(name = "userGameRoleVoList", value = "用户游戏角色列表")
    private List<UserGameRoleVo> userGameRoleVoList;

}
