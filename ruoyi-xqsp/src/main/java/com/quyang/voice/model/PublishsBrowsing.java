
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(世界动态浏览实体类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("TPublishsBrowsing")
@TableName(value = "t_publishs_browsing")
@AllArgsConstructor
@NoArgsConstructor
public class PublishsBrowsing implements Serializable {

	private static final long serialVersionUID = 1606799289242L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Long id;

    @ApiModelProperty(name = "userId", value = "用户id")
	private Integer userId;

    @ApiModelProperty(name = "publishId", value = "动态id")
	private Long publishId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "updateTime", value = "浏览时间")
	private Date updateTime;


}
