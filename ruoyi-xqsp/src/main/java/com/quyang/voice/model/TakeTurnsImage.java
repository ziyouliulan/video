
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(首页轮播图实体类)
 *
 * @version: V1.0
 * @author: liuhaotian
 *
 */
@Data
@ApiModel("TakeTurnsImage")
@TableName(value = "t_take_turns_image")
@AllArgsConstructor
@NoArgsConstructor
public class TakeTurnsImage implements Serializable {

	private static final long serialVersionUID = 1609814190057L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
	private Long id;

    @ApiModelProperty(name = "imageUrl", value = "图片url")
	private String imageUrl;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
	private Date createTime;

    @ApiModelProperty(name = "status", value = "状态（0启用，1禁用）")
	private Integer status;

    @ApiModelProperty(name = "belong", value = "图片所属0首页，1交流圈")
	private Integer belong;

    @ApiModelProperty(name = "isH5", value = "是否是H5: 0是 1不是")
	private Integer isH5;

    @ApiModelProperty(name = "h5Url", value = "H5连接")
	private String h5Url;


}
