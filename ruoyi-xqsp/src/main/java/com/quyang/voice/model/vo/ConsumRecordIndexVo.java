package com.quyang.voice.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@ApiModel("ConsumRecordIndexVo")
@AllArgsConstructor
@NoArgsConstructor
public class ConsumRecordIndexVo {

    @ApiModelProperty(name = "consumeRecordSimpleVoList", value = "交易记录")
    List<ConsumeRecordSimpleVo> consumeRecordSimpleVoList;

    @ApiModelProperty(name = "out", value = "支出")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    Double outCharge;

    @ApiModelProperty(name = "inCharge", value = "收入")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    Double inCharge;
}
