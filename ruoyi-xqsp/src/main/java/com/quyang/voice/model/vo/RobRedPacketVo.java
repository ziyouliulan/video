package com.quyang.voice.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel("RobRedPacketVo")
@AllArgsConstructor
@NoArgsConstructor
public class RobRedPacketVo implements Serializable {

    private static final long serialVersionUID = 1606664166975L;

    @ApiModelProperty(name = "userId", value = "发红包的用户id")
    private  Integer robUserId;

    @ApiModelProperty(name = "nickName", value = "发红包用户的昵称")
    private  String  robNickName;

    @ApiModelProperty(name = "avatarUrl", value = "发送红包用户的头像")
    private String robAvatarUrl;

    @ApiModelProperty(name = "robMoney", value = "领取的金额")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double robMoney;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "领取时间")
    private Date createTime;
}
