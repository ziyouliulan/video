package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(用户财富魅力排名Vo类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("UserWealthCharmRankingVo")
@AllArgsConstructor
@NoArgsConstructor
public class UserWealthCharmRankingVo implements Serializable {


    @ApiModelProperty(name = "userId", value = "用户id")
    private Long userId;

    @ApiModelProperty(name = "userName", value = "用户昵称")
    private String userName;

    @ApiModelProperty(name = "headUrl", value = "用户头像url")
    private String headUrl;

    @ApiModelProperty(name = "sex", value = "性别：0 女，1 男")
    private Integer sex;

    //---------以上是首页财富魅力榜属性---------------------------------
    @ApiModelProperty(name = "position", value = "用户职位")
    private String position;

    @ApiModelProperty(name = "isVip", value = "是否是vip")
    private Boolean isVip;

    @ApiModelProperty(name = "grade", value = "用户等级")
    private Integer grade;
    //---------以上是房间财富魅力榜属性---------------------------------
}
