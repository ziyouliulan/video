package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-05-07
 */

@Setter
@Getter
@ToString
@TableName( "l_group_tab" )
public class GroupTab  implements Serializable {

	private static final long serialVersionUID =  6021726964939728048L;

	/**
	 * id
	 */


	private Long id;

	/**
	 * 标题
	 */

	private String name;

	/**
	 * 链接地址
	 */

	private String addressUrl;

	/**
	 * 图片地址
	 */

	private String imgUrl;
}
