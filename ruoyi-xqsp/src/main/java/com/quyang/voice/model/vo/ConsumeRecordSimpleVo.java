package com.quyang.voice.model.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ApiModel("ConsumeRecordSimpleVo")
@AllArgsConstructor
@NoArgsConstructor
public class ConsumeRecordSimpleVo {


    @ApiModelProperty(name = "id", value = "id")
    private Long id;

    @ApiModelProperty(name = "tradeNo", value = "交易单号")
    private String tradeNo;

    @ApiModelProperty(name = "userId", value = "用户ID")
    private Long userId;

    @ApiModelProperty(name = "toUserId", value = " 对方用户Id： 接受转账时 为 转账人的ID， 发送转账时 为 接受放的 ID")
    private Long toUserId;

    @ApiModelProperty(name = "money", value = "金额")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double money;

    @ApiModelProperty(name = "type", value = "类型： 1:用户充值, 2:用户提现, 3:后台充值, 4:发红包, 5:领取红包, 6:红包退款  7:转账   8:接受转账   9:转账退回 ")
    private Integer type;

    @ApiModelProperty(name = "descs", value = "消费备注")
    private String descs;

    @ApiModelProperty(name = "payType", value = " 1：支付宝支付 , 2：微信支付, 3：余额支付, 4:系统支付")
    private Integer payType;

    @ApiModelProperty(name = "status", value = "0：创建  1：支付完成  2：交易完成  -1：交易关闭")
    private Integer status;

    @ApiModelProperty(name = "changeType", value = " 1 收入  2支出")
    private Integer changeType;

    @ApiModelProperty(name = "avatar", value = " 对方头像")
    private  String toUserAvatar;

    @ApiModelProperty(name = "avatar", value = " 红包转账id")
    private  String orderId;

    @ApiModelProperty(name = "toNickName", value = " 对方昵称")
    private  String toNickName;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "timestamp", value = "到账时间")
    private Date timestamp;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "time", value = "时间")
    private Date time;






}
