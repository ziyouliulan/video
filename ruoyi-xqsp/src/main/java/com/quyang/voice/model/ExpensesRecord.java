package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_expenses_record
 * @author
 */
@ApiModel(value="com.quyang.voice.model.ExpensesRecord消费记录")
@Data
public class ExpensesRecord implements Serializable {
    private Long id;

    /**
     * 交易单号
     */
    @ApiModelProperty(value="交易单号")
    private String tradeNo;

    /**
     * 用户ID
     */
    @ApiModelProperty(value="用户ID")
    private Long userId;

    /**
     *  对方用户Id：
 接受转账时 为 转账人的ID，
 发送转账时 为 接受放的 ID
     */
    @ApiModelProperty(value=" 对方用户Id： 接受转账时 为 转账人的ID， 发送转账时 为 接受放的 ID")
    private Long toUserId;

    /**
     * 金额
     */
    @ApiModelProperty(value="金额")
    private Double money;

    /**
     * 类型：
1:预约问诊
     */
    @ApiModelProperty(value="类型： 1:预约问诊 ")
    private Integer type;

    /**
     * 消费备注
     */
    @ApiModelProperty(value="消费备注")
    private String descs;

    /**
     *  1：支付宝支付 , 2：微信支付, 3：余额支付, 4:系统支付
     */
    @ApiModelProperty(value=" 1：支付宝支付 , 2：微信支付, 3：余额支付, 4:系统支付")
    private Integer payType;

    /**
     * 0：创建  1：支付完成  2：交易完成  -1：交易关闭 3提现成功，4提现失败
     */
    @ApiModelProperty(value="0：创建  1：支付完成  2：交易完成  -1：交易关闭 3提现成功，4提现失败")
    private Integer status;

    /**
     * 红包id
     */
    @ApiModelProperty(value="红包id")
    private String redPacketId;

    /**
     *  1 收入  2支出
     */
    @ApiModelProperty(value=" 1 收入  2支出")
    private Integer changeType;

    /**
     * 超过一定金额需要后台审核：1.审核成功 ，-1.审核失败
     */
    @ApiModelProperty(value="超过一定金额需要后台审核：1.审核成功 ，-1.审核失败")
    private Integer manualPayStatus;

    /**
     * 手续费
     */
    @ApiModelProperty(value="手续费")
    private Double serviceCharge;

    /**
     * 当前余额
     */
    @ApiModelProperty(value="当前余额")
    private Double currentBalance;

    /**
     * 实际操作金额
     */
    @ApiModelProperty(value="实际操作金额")
    private Double operationAmount;

    /**
     * 时间
     */
    @ApiModelProperty(value="时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    /**
     * 到账时间
     */
    @ApiModelProperty(value="到账时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date timestamp;

    /**
     * 订单号
     */
    @ApiModelProperty(value="订单号")
    private String order;

    private static final long serialVersionUID = 1L;
}
