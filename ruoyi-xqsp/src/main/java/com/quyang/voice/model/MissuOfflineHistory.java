package com.quyang.voice.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * missu_offline_history
 * @author
 */
@Data
public class MissuOfflineHistory implements Serializable {
    /**
     * 记录ID
     */
    private Integer historyId;

    /**
     * 发起人的用户id
     */
    private Integer userUid;

    /**
     * 好友的用户id
     */
    private Integer friendUserUid;

    /**
     * 聊天消息类型：
0 普通文本消息
1 图片消息
2 语音留言（就是类似于微信的对讲机功能）
3 赠送的礼品
4 索取礼品消息
5 文件消息
6 短视频消息
7 名片
8 位置消息
10 发红包消息（wlx新增）
11 撤回消息 (wlx新增)
12 禁言 (wlx新增)
13 抢红包 (wlx新增)
90 系统消息或提示信息

     */
    private Integer historyType;

    /**
     * 聊天模式类型：
0 正常聊天
1 临时聊天(陌生人聊天)
2 普通群聊或世界频道（当groupid=-1时就是世界频道聊天）

     */
    private Integer chatType;

    /**
     * 聊天消息内容
1）当消息类型是文本消息时，本字段存放的是文本消息内容。
2）当消息类型是图片消息时，本字段存放的是暂存于服务端的图片原文件（非缩略图）的文件名。
3）当消息类型是语音留言消息时，本字段存放的是暂存于服务端的语音文件的文件名。
4）当消息类型是红包时，消息内容是红包--红包id
     */
    private String historyContent;

    /**
     * 消息内容
本字段为备用字段。
字段用途：
1）自2013-12-19日起：正式聊天中的普通文本、图片消息的离线消息存放在本字段的是QoS的指纹码。

     */
    private String historyContent2;

    /**
     * 发生时间
     */
    private Date historyTime;

    /**
     * 发生时间戳

     */
    private String historyTime2;

    /**
     * 对应群组id
     */
    private String groupId;

    /**
     * 安卓消息读取状态：1已读 0 未读
     */
    private Integer isRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer iosIsRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer webIsRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer isReadSync;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer iosIsReadSync;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer webIsReadSync;

    /**
     * 已读 未读1已读 0 未读
     */
    private Integer isReads;

    private static final long serialVersionUID = 1L;
}
