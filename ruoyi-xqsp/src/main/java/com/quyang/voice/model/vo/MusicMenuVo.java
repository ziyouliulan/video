
package com.quyang.voice.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**   
 * @Description:TODO(歌单表实体类)
 * 
 * @version: V1.0
 * @author: wlx
 * 
 */
@Data
@ApiModel("MusicMenuVo")
@AllArgsConstructor
@NoArgsConstructor
public class MusicMenuVo implements Serializable {

	private static final long serialVersionUID = 1605244722818L;
	
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Long id;
    
    @ApiModelProperty(name = "name", value = "歌单名字")
	private String name;
    
    @ApiModelProperty(name = "avatarId", value = "歌单头像id")
	private Long avatarId;

	@ApiModelProperty(name = "avatarUrl", value = "歌单头像Url")
	private String avatarUrl;

    @ApiModelProperty(name = "userId", value = "创建者id")
	private Long userId;

	@ApiModelProperty(name = "singNums", value = "歌单内歌曲数量")
	private Integer singNums;

}
