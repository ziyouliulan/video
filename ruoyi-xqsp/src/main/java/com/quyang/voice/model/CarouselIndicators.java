package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * yuecool_carousel_indicators
 * @author
 */
@ApiModel(value="com.quyang.voice.model.CarouselIndicators轮播管理")
@Data
public class CarouselIndicators implements Serializable {
    private Integer id;

    private String imgUrl;

    /**
     * 1文章轮播 2首页轮播
     */
    @ApiModelProperty(value="1文章轮播 2首页轮播")
    private String state;



    /**
     * 轮播图地址
     */
    @ApiModelProperty(value="轮播图地址")
    private String address;

    /**
     * 轮播名称
     */
    @ApiModelProperty(value="轮播名称")
    private String name;

    private static final long serialVersionUID = 1L;
}
