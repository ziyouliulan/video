package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-05-06
 */

@Setter
@Getter
@ToString
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class UserLevel  implements Serializable {

	private static final long serialVersionUID =  2760618567361620690L;

	/**
	 * id
	 */


	private Long id;

	/**
	 * 等级
	 */

	private String level;

	/**
	 * 等级名称
	 */

	private String levelName;

	/**
	 * 加群
	 */

	private String addGroup;

	/**
	 * 发红包
	 */

	private String sendRedpacket;

	/**
	 * 加好友
	 */

	private String addFriend;

	/**
	 * 创建群
	 */

	private String createGroup;

	/**
	 * 强制加好友
	 */

	private String addFriendSuper;

	/**
	 * 强制加群
	 */

	private String addGroupSuper;

	/**
	 * 强制拉人加群
	 */

	private String addPersonSuper;

	/**
	 * 群内撤回他人消息
	 */

	private String groupDelMsg;

	/**
	 * 查看云消息记录
	 */

	private String seeYunMsg;

	/**
	 * 消息开关
	 */

	private String msgTab;

	/**
	 * 联系人开关
	 */

	private String friendTab;

	/**
	 * 朋友圈开关
	 */

	private String circleTab;

	/**
	 * 我的钱包开关
	 */

	private String walletTab;

	/**
	 * 收藏开关
	 */

	private String collectionTab;

	/**
	 * 发现开关
	 */

	private String findTab;

}
