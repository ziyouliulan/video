package com.quyang.voice.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("RedPacketDetailVo")
@AllArgsConstructor
@NoArgsConstructor
public class RedPacketDetailVo   implements Serializable {

    private static final long serialVersionUID = 1606794166999L;

    @ApiModelProperty(name = "userId", value = "发红包的用户id")
    private  Integer userId;

    @ApiModelProperty(name = "nickName", value = "发红包用户的昵称")
    private  String  nickName;

    @ApiModelProperty(name = "avatarUrl", value = "发送红包用户的头像")
    private String avatarUrl;

    @ApiModelProperty(name = "greetings", value = "祝福语")
    private String greetings;

    @ApiModelProperty(name = "total", value = "总计红包数")
    private  Integer total;

    @ApiModelProperty(name = "total", value = "总计红包数")
    private  String totalDuration;

    @ApiModelProperty(name = "robNum", value = "已经被领取的红包数")
    private  Integer robNum;

    @ApiModelProperty(name = "totalMoney", value = "总计多少钱")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private  Double totalMoney;

    @ApiModelProperty(name = "robToMoney", value = "已经被领取的钱")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private  Double robToMoney;

    @ApiModelProperty(name = "meRobMoney", value = "我抢到的钱")
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private  Double meRobMoney;

    @ApiModelProperty(name = "status", value = "领取状态 0：未领取  1：已领取 2：本人未领取且红包已领完  " )
    private Integer status;

    @ApiModelProperty(name = "redPackVoList", value = "已经领取名单" )
    private List<RobRedPacketVo> redPackVoList;







}
