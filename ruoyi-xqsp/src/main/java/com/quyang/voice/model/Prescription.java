package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_prescription
 * @author
 */
@ApiModel(value="com.quyang.voice.model.Prescription")
@Data
public class Prescription implements Serializable {
    private Integer id;

    /**
     * 病情描述
     */
    @ApiModelProperty(value="病情描述")
    private String desc;

    /**
     * 地址id
     */
    @ApiModelProperty(value="地址id")
    private String addressId;

    private LitemallAddress address;

    /**
     * 处方照片
     */
    @ApiModelProperty(value="处方照片")
    private String imagaUrl;

    /**
     * 补充
     */
    @ApiModelProperty(value="补充")
    private String supplement;

    /**
     * 逻辑删除
     */
    @ApiModelProperty(value="逻辑删除")
    private Boolean deleted;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    /**
     * 订单编号
     */
    @ApiModelProperty(value="订单编号")
    private String orderId;

    /**
     * 开药价格
     */
    @ApiModelProperty(value="开药价格")
    private String money;

    /**
     * 订单状态1：待审核  2 待支付, 3 已完成 4已取消
     */
    @ApiModelProperty(value="订单状态1：待审核  2 待支付, 3 已完成 4已取消")
    private Integer status;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    private static final long serialVersionUID = 1L;
}
