package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 消费记录
 * @TableName yuecool_group_record
 */
@TableName(value ="yuecool_group_record")
@Data
public class YuecoolGroupRecord implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 群id
     */
    private Integer gId;

    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 用户id
     */
    @TableField(exist = false)
    private String userName;

    /**
     *
     */
    private Integer toUserid;
    /**
     *
     */
    @TableField(exist = false)
    private String toUserName;

    /**
     *
     */

    @TableField(exist = false)
    private String toUserids;

    /**
     * 类型：1邀请入群 2自主加群  3自主退群 4 被踢
     */
    private Integer type;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;

    /**
     * 更新日期
     */
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String descs;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
