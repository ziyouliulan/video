package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("RedPackVo")
@AllArgsConstructor
@NoArgsConstructor
public class RedPackVo implements Serializable {

    private static final long serialVersionUID = 1606794166975L;

    @ApiModelProperty(name = "orderId", value = "红包id")
    private String orderId;

    @ApiModelProperty(name = "nickNme", value = "发送人昵称")
    private String nickName;

    @ApiModelProperty(name = "coverUrl")
    private String coverUrl;

    @ApiModelProperty(name = "greetings", value = "祝福语")
    private String greetings;

    @ApiModelProperty(name = "avatarUrl", value = "用户头像")
    private String avatarUrl;

    @ApiModelProperty(name = "status", value = "红包领取状态 0：未领取  1：已领取")
    private Integer status;



}
