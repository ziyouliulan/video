
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(技能卡实体类)
 * @version: V1.0
 * @author: Z-H
 */
@Data
@ApiModel("TSkillCard")
@TableName(value = "t_skill_card")
@AllArgsConstructor
@NoArgsConstructor
public class SkillCard implements Serializable {

    private static final long serialVersionUID = 1603077708443L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
    private Long id;

    @ApiModelProperty(name = "userId", value = "用户id")
    private Long userId;

    @ApiModelProperty(name = "name", value = "房间类型名称")
    private String name;

    @ApiModelProperty(name = "roomTypeId", value = "房间类型id")
    private Long roomTypeId;

    @ApiModelProperty(name = "img", value = "图片")
    private String img;
}
