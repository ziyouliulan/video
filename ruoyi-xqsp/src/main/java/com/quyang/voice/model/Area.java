package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * sys_area
 * @author
 */
@ApiModel(value="com.quyang.voice.model.Area行政区划")
@Data
public class Area implements Serializable {
    /**
     * 区域id
     */
    @ApiModelProperty(value="区域id")
    private Long areaId;

    /**
     * 行政区划代码
     */
    @ApiModelProperty(value="行政区划代码")
    private String areaCode;

    /**
     * 父级id
     */
    @ApiModelProperty(value="父级id")
    private String parentCode;

    /**
     * 地区名称
     */
    @ApiModelProperty(value="地区名称")
    private String name;

    /**
     * 层级
     */
    @ApiModelProperty(value="层级")
    private Integer layer;

    /**
     * 排序号,1:省级,2:地市,3:区县
     */
    @ApiModelProperty(value="排序号,1:省级,2:地市,3:区县")
    private Integer orderNum;

    /**
     * 显示,1:显示,0:隐藏
     */
    @ApiModelProperty(value="显示,1:显示,0:隐藏")
    private Byte status;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date gmtCreate;

    /**
     * 修改时间
     */
    @ApiModelProperty(value="修改时间")
    private Date gmtModified;

    private static final long serialVersionUID = 1L;
}
