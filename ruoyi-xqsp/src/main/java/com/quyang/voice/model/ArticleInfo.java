package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * article_info
 * @author
 */
@ApiModel(value="com.quyang.voice.model.ArticleInfo商品基本信息表")
@Data
public class ArticleInfo implements Serializable {
    private Integer id;

    /**
     * 咨讯名称，标题
     */
    @ApiModelProperty(value="咨讯名称，标题")
    private String name;

    /**
     * 所属类目,1头条，2牛人解盘
     */
    @ApiModelProperty(value="所属类目,1头条，2牛人解盘")
    private Integer category;

    /**
     * 发布方
     */
    @ApiModelProperty(value="发布方")
    private String keywords;

    /**
     * 简介
     */
    @ApiModelProperty(value="简介")
    private String brief;

    private Short sortOrder;

    /**
     * 封面图片url
     */
    @ApiModelProperty(value="封面图片url")
    private String picUrl;

    /**
     * 详细介绍，是富文本格式
     */
    @ApiModelProperty(value="详细介绍，是富文本格式")
    private String detail;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="创建时间")
    private Date addTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    /**
     * 是否置顶 1：置顶
     */
    @ApiModelProperty(value="是否置顶 1：置顶    ")
    private Integer isTop;

    private static final long serialVersionUID = 1L;
}
