
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(用户余额更新记录实体类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("TUsermoenyLog")
@TableName(value = "usermoeny_log")
@AllArgsConstructor
@NoArgsConstructor
public class UsermoenyLog implements Serializable {

	private static final long serialVersionUID = 1613626845231L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Long id;

    @ApiModelProperty(name = "orderId", value = "订单号")
	private String orderId;

    @ApiModelProperty(name = "description", value = "记录描述")
	private String description;

    @ApiModelProperty(name = "toUserId", value = "对方用户id")
	private Long toUserId;

    @ApiModelProperty(name = "businessId", value = "业务ID")
	private String businessId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
	private Date createTime;

    @ApiModelProperty(name = "beforeMoeny", value = "操作前余额")
	private Double beforeMoeny;

    @ApiModelProperty(name = "moeny", value = "改变余额")
	private Double moeny;

    @ApiModelProperty(name = "changeType", value = "类型：1 增加余额  2减少余额")
	private Integer changeType;

    @ApiModelProperty(name = "endMoeny", value = "操作后余额")
	private Double endMoeny;

    @ApiModelProperty(name = "logType", value = "1:发送/支出 操作 2:领取/收入 操作 3:退款操作 5:锁定余额操作 6:取消锁定余额操作 -1:异常回滚操作")
	private Integer logType;

    @ApiModelProperty(name = "extra", value = "自定义参数")
	private String extra;


}
