package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 讲师
 * @TableName yuecool_shopkeeper
 */
@TableName(value ="litemall.yuecool_shopkeeper")
@Data
public class Shopkeeper implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 店铺名称
     */
    private String name;

    /**
     * 企业名称
     */
    private String enterpriseName;

    /**
     * 所属分类
     */
    private String hospital;

    /**
     * 0:男 1：女
     */
    private String sex;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 电话
     */
    private String phone;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 店铺简介
     */
    private String userDesc;

    /**
     * 店铺分类
     */
    private String positionId;
    /**
     * 店铺分类
     */
    @TableField(exist = false)
    private String positionName;

    /**
     * 响应时间
     */
    private Double responseTime;

    /**
     * 店铺等级
     */
    private String shopLevel;

    /**
     * 好评率
     */
    private Double favorableRate;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 是否推荐1是 0 否
     */
    private String isRecommend;

    /**
     * 地区id
     */
    private String areaId;

    /**
     * 地区id 市
     */
    private String cityId;

    /**
     * 详细地址
     */
    private String detailedAddress;

    /**
     * 照片
     */
    private String doctorPhotos;

    /**
     * 营业执照
     */
    private String qualificationCertificate;

    /**
     * 身份证正面
     */
    private String frontOfIdCard;

    /**
     * 身份证反面
     */
    private String reverseSideOfIdCard;

    /**
     * 1:个人  2企业 3自营
     */
    private String inquiryManagement;

    /**
     * 状态1： 认证中  2认证失败 3 认证成功 4封禁 5删除
     */
    private String status;

    /**
     * 经度
     */
    private String lot;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 是否会员商城
     */
    private Boolean isVip;

    /**
     * 是否支持洗车
     */
    private Boolean isCarWash;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
