package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @TableName yuecool_car_type
 */
@TableName(value ="yuecool_car_type")
@Data
public class YuecoolCarType implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 车型
     */
    private String name;

    /**
     * 收费多少
     */
    private Double num;

    /**
     * 是否删除
     */
    private Byte deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
