package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-05-07
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("UserGroupTabVo")
public class UserGroupTabVo  implements Serializable {

	private static final long serialVersionUID =  9012765422624716503L;

   	@ApiModelProperty(name = "id" , value = "id")
	private Long id;

   	@ApiModelProperty(name = "group_id" , value = "群id")
	private String groupId;

   	@ApiModelProperty(name = "group_tab_id" , value = "群标签id")
	private Long groupTabId;

   	@ApiModelProperty(name = "group_tab_on" , value = "1:开启 0：关闭")
	private String groupTabOn;

}
