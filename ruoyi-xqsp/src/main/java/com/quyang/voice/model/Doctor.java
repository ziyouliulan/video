package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_doctor
 * @author
 */
@ApiModel(value="com.quyang.voice.model.Doctor")
@Data
public class Doctor implements Serializable {
    private Integer id;

    /**
     * 医师名称
     */
    @ApiModelProperty(value="医师名称")
    private String name;

    /**
     * 所属医院
     */
    @ApiModelProperty(value="所属医院")
    private String hospital;

    /**
     * 0:男 1：女
     */
    @ApiModelProperty(value="0:男 1：女")
    private String sex;

    /**
     * 生日
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(value="生日")
    private String birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="电话")
    private String phone;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    /**
     * 简介
     */
    @ApiModelProperty(value="简介")
    private String userDesc;

    /**
     * 职位id
     */
    @ApiModelProperty(value="职位id")
    private String positionId;

    /**
     * 擅长
     */
    @ApiModelProperty(value="擅长")
    private String beGoodAt;

    /**
     * 科室id
     */
    @ApiModelProperty(value="科室id")
    private String deptId;
    /**
     * 科室id
     */
    @ApiModelProperty(value="科室id")
    private String deptName;

    /**
     * 图文收费
     */
    @ApiModelProperty(value="图文收费")
    private String imageText;

    private Integer imageTextTime;

    /**
     * 音频收费
     */
    @ApiModelProperty(value="音频收费")
    private String audio;

    private Integer audioTime;

    /**
     * 视频收费
     */
    @ApiModelProperty(value="视频收费")
    private String video;

    private Integer videoTime;

    /**
     * 响应时间
     */
    @ApiModelProperty(value="响应时间")
    private Double responseTime;

    /**
     * 是否开方
     */
    @ApiModelProperty(value="是否开方")
    private Integer prescription;

    /**
     * 医院等级
     */
    @ApiModelProperty(value="医院等级")
    private String hospitalLevel;

    /**
     * 接诊人数
     */
    @ApiModelProperty(value="接诊人数")
    private Integer numberOfPatients;

    /**
     * 好评率
     */
    @ApiModelProperty(value="好评率")
    private Double favorableRate;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 是否推荐医师
     */
    @ApiModelProperty(value="是否推荐医师")
    private String isRecommend;

    /**
     * 地区id
     */
    @ApiModelProperty(value="地区id")
    private String areaId;
    /**
     * 地区id
     */
    @ApiModelProperty(value="地区id")
    private String areaName;

    /**
     * 地区id 市
     */
    @ApiModelProperty(value="地区id 市")
    private String cityId;

    /**
     * 地区id 市
     */
    @ApiModelProperty(value="地区id 市")
    private String cityName;

    /**
     * 医生照片
     */
    @ApiModelProperty(value="医生照片")
    private String doctorPhotos;

    /**
     * 资格证书
     */
    @ApiModelProperty(value="资格证书")
    private String qualificationCertificate;

    /**
     * 身份证正面
     */
    @ApiModelProperty(value="身份证正面")
    private String frontOfIdCard;

    /**
     * 身份证反面
     */
    @ApiModelProperty(value="身份证反面")
    private String reverseSideOfIdCard;

    /**
     * 预约管理 0关闭 1：开启
     */
    @ApiModelProperty(value="预约管理 0关闭 1：开启")
    private String appointmentManagement;

    /**
     * 问诊管理 0关闭 1：开启
     */
    @ApiModelProperty(value="问诊管理 0关闭 1：开启")
    private String inquiryManagement;

    /**
     * 医生状态
     */
    @ApiModelProperty(value="医生状态")
    private String status;

    private static final long serialVersionUID = 1L;
}
