package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("MyJoinedClusterVo")
@AllArgsConstructor
@NoArgsConstructor
public class MyJoinedClusterVo implements Serializable {
    private static final long serialVersionUID = 1600833164692L;

    @ApiModelProperty(name = "id", value = "群或讨论组id")
    private Long id;

    @ApiModelProperty(name = "type", value = "群类型（1：公会群，2：个人群，3：讨论组）")
    private Integer type;

    @ApiModelProperty(name = "name", value = "昵称")
    private String name;

    @ApiModelProperty(name = "url", value = "头像")
    private String url;

    @ApiModelProperty(name = "isMasterCluster", value = "是否总群")
    private Boolean isMasterCluster;

    @ApiModelProperty(name = "isMasterCluster", value = "是否屏蔽")
    private Boolean isShield;

    @ApiModelProperty(name = "masterClusterId", value = "总群id")
    private Long masterClusterId;

    @ApiModelProperty(name = "isDisturb", value = "是否免打扰")
    private Boolean isDisturb;


}
