package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * stock_info
 * @author
 */
@ApiModel(value="com.quyang.voice.model.StockInfo商品基本信息表")
@Data
public class StockInfo implements Serializable {
    private Integer id;

    /**
     * 咨讯名称，标题
     */
    @ApiModelProperty(value="咨讯名称，标题")
    private String name;

    /**
     * 所属类目,1头条，2牛人解盘
     */
    @ApiModelProperty(value="所属类目,1头条，2牛人解盘")
    private Integer category;

    /**
     * 发布方
     */
    @ApiModelProperty(value="发布方")
    private String keywords;

    /**
     * 简介
     */
    @ApiModelProperty(value="简介")
    private String brief;

    private Short sortOrder;

    /**
     * 封面图片url
     */
    @ApiModelProperty(value="封面图片url")
    private String picUrl;

    /**
     * 详细介绍，是富文本格式
     */
    @ApiModelProperty(value="详细介绍，是富文本格式")
    private String detail;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date addTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}
