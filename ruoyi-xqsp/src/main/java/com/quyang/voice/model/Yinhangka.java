package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @TableName yuecool_yinhangka
 */
@TableName(value ="yuecool_yinhangka")
@Data
public class Yinhangka implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户账号
     */
    private String usercode;

    /**
     * 手机号
     */
    private String userphone;

    /**
     * 新生用户ID
     */
    private String xsuserid;

    /**
     * 持卡人姓名
     */
    private String yhxingming;

    /**
     * 身份证号
     */
    private String shenfenzhenghao;

    /**
     * 银行卡号
     */
    private String yinhangkahao;

    /**
     * 银行签约手机号
     */
    private String yhphone;

    /**
     * 签约订单号(绑卡确认接口使用)
     */
    private String ordercode;

    /**
     * 银行ID
     */
    private Integer bankid;

    /**
     * 银行编码
     */
    private String bankcode;

    /**
     * 银行名
     */
    private String bankname;

    /**
     * 银行logo
     */
    private String banklogo;

    /**
     * 绑卡协议号
     */
    private String bindcardagrno;

    /**
     * 1.成功绑定 2.未绑定成功
     */
    private Boolean state;

    /**
     * 创建时间
     */
    private LocalDateTime createdate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
