package com.quyang.voice.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


@Data
@ApiModel("TMissuCollectVo")
@AllArgsConstructor
@NoArgsConstructor
public class MissuCollectVo implements Serializable {

    private static final long serialVersionUID = 1614649532756L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "id", hidden = true)
    private Long id;

    @ApiModelProperty(name = "userId", value = "收藏人id")
    private Integer userId;

    @ApiModelProperty(name = "content", value = "收藏内容")
    private String content;


    @ApiModelProperty(name = "nickname", value = "发送人的昵称")
    private String nickname;

    @ApiModelProperty(name = "type", value = "收藏类型 0:纯文字   1:图片   2:视频  3:音频")
    private Integer type;

    @ApiModelProperty(name = "fromUserId", value = "消息发送用户id")
    private Integer fromUserId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private Date createTime;
}
