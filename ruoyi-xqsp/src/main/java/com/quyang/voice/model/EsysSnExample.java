package com.quyang.voice.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EsysSnExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public EsysSnExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSnNameIsNull() {
            addCriterion("sn_name is null");
            return (Criteria) this;
        }

        public Criteria andSnNameIsNotNull() {
            addCriterion("sn_name is not null");
            return (Criteria) this;
        }

        public Criteria andSnNameEqualTo(String value) {
            addCriterion("sn_name =", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameNotEqualTo(String value) {
            addCriterion("sn_name <>", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameGreaterThan(String value) {
            addCriterion("sn_name >", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameGreaterThanOrEqualTo(String value) {
            addCriterion("sn_name >=", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameLessThan(String value) {
            addCriterion("sn_name <", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameLessThanOrEqualTo(String value) {
            addCriterion("sn_name <=", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameLike(String value) {
            addCriterion("sn_name like", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameNotLike(String value) {
            addCriterion("sn_name not like", value, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameIn(List<String> values) {
            addCriterion("sn_name in", values, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameNotIn(List<String> values) {
            addCriterion("sn_name not in", values, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameBetween(String value1, String value2) {
            addCriterion("sn_name between", value1, value2, "snName");
            return (Criteria) this;
        }

        public Criteria andSnNameNotBetween(String value1, String value2) {
            addCriterion("sn_name not between", value1, value2, "snName");
            return (Criteria) this;
        }

        public Criteria andCurrentValIsNull() {
            addCriterion("current_val is null");
            return (Criteria) this;
        }

        public Criteria andCurrentValIsNotNull() {
            addCriterion("current_val is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentValEqualTo(Long value) {
            addCriterion("current_val =", value, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValNotEqualTo(Long value) {
            addCriterion("current_val <>", value, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValGreaterThan(Long value) {
            addCriterion("current_val >", value, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValGreaterThanOrEqualTo(Long value) {
            addCriterion("current_val >=", value, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValLessThan(Long value) {
            addCriterion("current_val <", value, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValLessThanOrEqualTo(Long value) {
            addCriterion("current_val <=", value, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValIn(List<Long> values) {
            addCriterion("current_val in", values, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValNotIn(List<Long> values) {
            addCriterion("current_val not in", values, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValBetween(Long value1, Long value2) {
            addCriterion("current_val between", value1, value2, "currentVal");
            return (Criteria) this;
        }

        public Criteria andCurrentValNotBetween(Long value1, Long value2) {
            addCriterion("current_val not between", value1, value2, "currentVal");
            return (Criteria) this;
        }

        public Criteria andMinValIsNull() {
            addCriterion("min_val is null");
            return (Criteria) this;
        }

        public Criteria andMinValIsNotNull() {
            addCriterion("min_val is not null");
            return (Criteria) this;
        }

        public Criteria andMinValEqualTo(Long value) {
            addCriterion("min_val =", value, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValNotEqualTo(Long value) {
            addCriterion("min_val <>", value, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValGreaterThan(Long value) {
            addCriterion("min_val >", value, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValGreaterThanOrEqualTo(Long value) {
            addCriterion("min_val >=", value, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValLessThan(Long value) {
            addCriterion("min_val <", value, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValLessThanOrEqualTo(Long value) {
            addCriterion("min_val <=", value, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValIn(List<Long> values) {
            addCriterion("min_val in", values, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValNotIn(List<Long> values) {
            addCriterion("min_val not in", values, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValBetween(Long value1, Long value2) {
            addCriterion("min_val between", value1, value2, "minVal");
            return (Criteria) this;
        }

        public Criteria andMinValNotBetween(Long value1, Long value2) {
            addCriterion("min_val not between", value1, value2, "minVal");
            return (Criteria) this;
        }

        public Criteria andMaxValIsNull() {
            addCriterion("max_val is null");
            return (Criteria) this;
        }

        public Criteria andMaxValIsNotNull() {
            addCriterion("max_val is not null");
            return (Criteria) this;
        }

        public Criteria andMaxValEqualTo(Long value) {
            addCriterion("max_val =", value, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValNotEqualTo(Long value) {
            addCriterion("max_val <>", value, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValGreaterThan(Long value) {
            addCriterion("max_val >", value, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValGreaterThanOrEqualTo(Long value) {
            addCriterion("max_val >=", value, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValLessThan(Long value) {
            addCriterion("max_val <", value, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValLessThanOrEqualTo(Long value) {
            addCriterion("max_val <=", value, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValIn(List<Long> values) {
            addCriterion("max_val in", values, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValNotIn(List<Long> values) {
            addCriterion("max_val not in", values, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValBetween(Long value1, Long value2) {
            addCriterion("max_val between", value1, value2, "maxVal");
            return (Criteria) this;
        }

        public Criteria andMaxValNotBetween(Long value1, Long value2) {
            addCriterion("max_val not between", value1, value2, "maxVal");
            return (Criteria) this;
        }

        public Criteria andDigitIsNull() {
            addCriterion("digit is null");
            return (Criteria) this;
        }

        public Criteria andDigitIsNotNull() {
            addCriterion("digit is not null");
            return (Criteria) this;
        }

        public Criteria andDigitEqualTo(Short value) {
            addCriterion("digit =", value, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitNotEqualTo(Short value) {
            addCriterion("digit <>", value, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitGreaterThan(Short value) {
            addCriterion("digit >", value, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitGreaterThanOrEqualTo(Short value) {
            addCriterion("digit >=", value, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitLessThan(Short value) {
            addCriterion("digit <", value, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitLessThanOrEqualTo(Short value) {
            addCriterion("digit <=", value, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitIn(List<Short> values) {
            addCriterion("digit in", values, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitNotIn(List<Short> values) {
            addCriterion("digit not in", values, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitBetween(Short value1, Short value2) {
            addCriterion("digit between", value1, value2, "digit");
            return (Criteria) this;
        }

        public Criteria andDigitNotBetween(Short value1, Short value2) {
            addCriterion("digit not between", value1, value2, "digit");
            return (Criteria) this;
        }

        public Criteria andHeadStrIsNull() {
            addCriterion("head_str is null");
            return (Criteria) this;
        }

        public Criteria andHeadStrIsNotNull() {
            addCriterion("head_str is not null");
            return (Criteria) this;
        }

        public Criteria andHeadStrEqualTo(String value) {
            addCriterion("head_str =", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrNotEqualTo(String value) {
            addCriterion("head_str <>", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrGreaterThan(String value) {
            addCriterion("head_str >", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrGreaterThanOrEqualTo(String value) {
            addCriterion("head_str >=", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrLessThan(String value) {
            addCriterion("head_str <", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrLessThanOrEqualTo(String value) {
            addCriterion("head_str <=", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrLike(String value) {
            addCriterion("head_str like", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrNotLike(String value) {
            addCriterion("head_str not like", value, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrIn(List<String> values) {
            addCriterion("head_str in", values, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrNotIn(List<String> values) {
            addCriterion("head_str not in", values, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrBetween(String value1, String value2) {
            addCriterion("head_str between", value1, value2, "headStr");
            return (Criteria) this;
        }

        public Criteria andHeadStrNotBetween(String value1, String value2) {
            addCriterion("head_str not between", value1, value2, "headStr");
            return (Criteria) this;
        }

        public Criteria andBzIsNull() {
            addCriterion("bz is null");
            return (Criteria) this;
        }

        public Criteria andBzIsNotNull() {
            addCriterion("bz is not null");
            return (Criteria) this;
        }

        public Criteria andBzEqualTo(String value) {
            addCriterion("bz =", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzNotEqualTo(String value) {
            addCriterion("bz <>", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzGreaterThan(String value) {
            addCriterion("bz >", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzGreaterThanOrEqualTo(String value) {
            addCriterion("bz >=", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzLessThan(String value) {
            addCriterion("bz <", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzLessThanOrEqualTo(String value) {
            addCriterion("bz <=", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzLike(String value) {
            addCriterion("bz like", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzNotLike(String value) {
            addCriterion("bz not like", value, "bz");
            return (Criteria) this;
        }

        public Criteria andBzIn(List<String> values) {
            addCriterion("bz in", values, "bz");
            return (Criteria) this;
        }

        public Criteria andBzNotIn(List<String> values) {
            addCriterion("bz not in", values, "bz");
            return (Criteria) this;
        }

        public Criteria andBzBetween(String value1, String value2) {
            addCriterion("bz between", value1, value2, "bz");
            return (Criteria) this;
        }

        public Criteria andBzNotBetween(String value1, String value2) {
            addCriterion("bz not between", value1, value2, "bz");
            return (Criteria) this;
        }

        public Criteria andGenTimeIsNull() {
            addCriterion("gen_time is null");
            return (Criteria) this;
        }

        public Criteria andGenTimeIsNotNull() {
            addCriterion("gen_time is not null");
            return (Criteria) this;
        }

        public Criteria andGenTimeEqualTo(Date value) {
            addCriterion("gen_time =", value, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeNotEqualTo(Date value) {
            addCriterion("gen_time <>", value, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeGreaterThan(Date value) {
            addCriterion("gen_time >", value, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("gen_time >=", value, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeLessThan(Date value) {
            addCriterion("gen_time <", value, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeLessThanOrEqualTo(Date value) {
            addCriterion("gen_time <=", value, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeIn(List<Date> values) {
            addCriterion("gen_time in", values, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeNotIn(List<Date> values) {
            addCriterion("gen_time not in", values, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeBetween(Date value1, Date value2) {
            addCriterion("gen_time between", value1, value2, "genTime");
            return (Criteria) this;
        }

        public Criteria andGenTimeNotBetween(Date value1, Date value2) {
            addCriterion("gen_time not between", value1, value2, "genTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}