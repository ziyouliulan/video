package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * yuecool_evaluate_label
 * @author
 */
@ApiModel(value="com.quyang.voice.model.EvaluateLabel")
@Data
public class EvaluateLabel implements Serializable {
    private Integer id;

    private String name;

    private String isDel;

    private static final long serialVersionUID = 1L;
}
