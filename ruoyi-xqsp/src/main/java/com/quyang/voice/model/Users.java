package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data//生成getter,setter等函数
@ApiModel("TUser")
@AllArgsConstructor//生成全参数构造函数
@NoArgsConstructor//生成无参构造函数
@TableName(value = "t_user")
public class Users implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID", hidden = true)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "姓名")
    private String realName;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "性别：0 女，1 男")
    private Integer sex;

    @ApiModelProperty(value = "个人介绍")
    private String introduce;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(value = "生日")
    private Date birthday;

    @ApiModelProperty(value = "密码")
    private String passwd;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "系统用户id")
    private String sysId;

    @ApiModelProperty(value = "青少年模式开关：0关闭  1开启")
    private int safemodeStatus;

    @ApiModelProperty(value = "青少年模式密码")
    private String safemodePwd;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "爱好")
    private String hobby;

    @ApiModelProperty(value = "职业")
    private String occupation;

    @ApiModelProperty(value = "身高")
    private Integer height;

    @ApiModelProperty(value = "体重")
    private Integer weight;

    @ApiModelProperty(value = "身份证号")
    private String idCard;

    @ApiModelProperty(value = "邀请码")
    private String inviteCode;

    @ApiModelProperty(value = "微信openId")
    private String wx;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "区")
    private String area;

    @ApiModelProperty(value = "登陆经度")
    private String loginLot;

    @ApiModelProperty(value = "登陆纬度")
    private String loginLat;

    @ApiModelProperty(value = "QQopenId")
    private String qq;

    @TableField(exist = false)
    @ApiModelProperty(value = "是否关注")
    private Boolean isFollow;

    @TableField(exist = false)
    @ApiModelProperty(value = "是否教练")
    private Boolean isCoach;

    @TableField(exist = false)
    @ApiModelProperty(value = "教练名字")
    private String CoachName;

}
