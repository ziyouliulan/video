
package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**   
 * @Description:TODO(动态评论实体类)
 * 
 * @version: V1.0
 * @author: wulixiang
 * 
 */
@Data
@ApiModel("TPublishCommentsInfo")
@AllArgsConstructor
@NoArgsConstructor
public class PublishInfoVo implements Serializable {

	private static final long serialVersionUID = 1599710372539L;

	@ApiModelProperty(name = "publishsVo", value = "动态")
	private PublishsVo publishsVo;

	@ApiModelProperty(name = "isComments", value = "是否有评论：0没有，1有")
	private Integer isComments;

	@ApiModelProperty(name = "publishCommentsVo", value = "动态评论")
	private List<PublishCommentsVo> publishCommentsVo;

}
