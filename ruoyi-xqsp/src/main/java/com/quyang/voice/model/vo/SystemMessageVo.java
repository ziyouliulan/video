package com.quyang.voice.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(系统消息VO类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("SystemMessageVo")
@AllArgsConstructor
@NoArgsConstructor
public class SystemMessageVo implements Serializable {


    @ApiModelProperty(name = "name", value = "模块名称")
    private String name;

    @ApiModelProperty(name = "content", value = "最新消息内容")
    private String content;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(name = "updateTime", value = "最新消息时间")
    private Date updateTime;

    @ApiModelProperty(name = "notReadNum", value = "未读条数")
    private Integer notReadNum;
}
