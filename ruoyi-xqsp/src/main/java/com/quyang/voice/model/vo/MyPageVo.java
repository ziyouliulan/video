package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(用户Vo类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("MyPageVo")
@AllArgsConstructor
@NoArgsConstructor
public class MyPageVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID",name = "用户id")
    private Long id;

    @ApiModelProperty(name = "nickname",value = "昵称")
    private String nickName;

    @ApiModelProperty(name = "sysId",value = "账号")
    private String sysId;

    @ApiModelProperty(name = "headUrl",value = "头像")
    private String headUrl;

    @ApiModelProperty(value = "是否VIP")
    private Boolean isVIP;

//    @ApiModelProperty(value = "是否实名认证")
//    private Boolean isVerify;

    @ApiModelProperty(value = "是否代理")
    private Boolean isAgent;

    @ApiModelProperty(value = "关注")
    private Integer follow;

    @ApiModelProperty(value = "粉丝")
    private Integer fans;

    @ApiModelProperty(value = "收藏")
    private Integer collect;

    @ApiModelProperty(value = "微信openId")
    private String wx;


}
