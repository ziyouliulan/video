
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(用户黑名单实体类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("blacklist")
@TableName(value = "yuecool_blacklist")
@AllArgsConstructor
@NoArgsConstructor
public class UserBlack implements Serializable {

	private static final long serialVersionUID = 1606726284903L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
	private Integer id;

    @ApiModelProperty(name = "userId", value = "用户id")
	private Integer userId;

    @ApiModelProperty(name = "blackId", value = "被拉黑用户id")
	private Integer blackId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "拉黑时间")
	private Date createTime;


}
