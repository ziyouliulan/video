package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_patient
 * @author
 */
@ApiModel(value="com.quyang.voice.model.Patient")
@Data
public class Patient implements Serializable {
    /**
     * id
     */
    @ApiModelProperty(value="id")
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    /**
     * 关系
     */
    @ApiModelProperty(value="关系")
    private String relationship;

    /**
     * 姓名
     */
    @ApiModelProperty(value="姓名")
    private String name;

    /**
     * 0:男 1：女
     */
    @ApiModelProperty(value="0:男 1：女")
    private String sex;

    /**
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(value="出生日期")
    private Date birthday;

    /**
     * 手机号
     */
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 身高
     */
    @ApiModelProperty(value="身高")
    private Integer height;

    /**
     * 体重
     */
    @ApiModelProperty(value="体重")
    private Double weight;

    /**
     * 血型
     */
    @ApiModelProperty(value="血型")
    private String bloodType;

    /**
     * 0:未婚 ，1：已婚
     */
    @ApiModelProperty(value="0:未婚 ，1：已婚")
    private String maritalStatus;

    private static final long serialVersionUID = 1L;
}
