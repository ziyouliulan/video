package com.quyang.voice.model;

import java.util.ArrayList;
import java.util.List;

public class OfflineIsRedExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public OfflineIsRedExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOfflineIdIsNull() {
            addCriterion("offline_id is null");
            return (Criteria) this;
        }

        public Criteria andOfflineIdIsNotNull() {
            addCriterion("offline_id is not null");
            return (Criteria) this;
        }

        public Criteria andOfflineIdEqualTo(Integer value) {
            addCriterion("offline_id =", value, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdNotEqualTo(Integer value) {
            addCriterion("offline_id <>", value, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdGreaterThan(Integer value) {
            addCriterion("offline_id >", value, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("offline_id >=", value, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdLessThan(Integer value) {
            addCriterion("offline_id <", value, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdLessThanOrEqualTo(Integer value) {
            addCriterion("offline_id <=", value, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdIn(List<Integer> values) {
            addCriterion("offline_id in", values, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdNotIn(List<Integer> values) {
            addCriterion("offline_id not in", values, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdBetween(Integer value1, Integer value2) {
            addCriterion("offline_id between", value1, value2, "offlineId");
            return (Criteria) this;
        }

        public Criteria andOfflineIdNotBetween(Integer value1, Integer value2) {
            addCriterion("offline_id not between", value1, value2, "offlineId");
            return (Criteria) this;
        }

        public Criteria andUserUidIsNull() {
            addCriterion("user_uid is null");
            return (Criteria) this;
        }

        public Criteria andUserUidIsNotNull() {
            addCriterion("user_uid is not null");
            return (Criteria) this;
        }

        public Criteria andUserUidEqualTo(Integer value) {
            addCriterion("user_uid =", value, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidNotEqualTo(Integer value) {
            addCriterion("user_uid <>", value, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidGreaterThan(Integer value) {
            addCriterion("user_uid >", value, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_uid >=", value, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidLessThan(Integer value) {
            addCriterion("user_uid <", value, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidLessThanOrEqualTo(Integer value) {
            addCriterion("user_uid <=", value, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidIn(List<Integer> values) {
            addCriterion("user_uid in", values, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidNotIn(List<Integer> values) {
            addCriterion("user_uid not in", values, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidBetween(Integer value1, Integer value2) {
            addCriterion("user_uid between", value1, value2, "userUid");
            return (Criteria) this;
        }

        public Criteria andUserUidNotBetween(Integer value1, Integer value2) {
            addCriterion("user_uid not between", value1, value2, "userUid");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNull() {
            addCriterion("group_id is null");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNotNull() {
            addCriterion("group_id is not null");
            return (Criteria) this;
        }

        public Criteria andGroupIdEqualTo(String value) {
            addCriterion("group_id =", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotEqualTo(String value) {
            addCriterion("group_id <>", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThan(String value) {
            addCriterion("group_id >", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThanOrEqualTo(String value) {
            addCriterion("group_id >=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThan(String value) {
            addCriterion("group_id <", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThanOrEqualTo(String value) {
            addCriterion("group_id <=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLike(String value) {
            addCriterion("group_id like", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotLike(String value) {
            addCriterion("group_id not like", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdIn(List<String> values) {
            addCriterion("group_id in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotIn(List<String> values) {
            addCriterion("group_id not in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdBetween(String value1, String value2) {
            addCriterion("group_id between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotBetween(String value1, String value2) {
            addCriterion("group_id not between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andIsReadIsNull() {
            addCriterion("is_read is null");
            return (Criteria) this;
        }

        public Criteria andIsReadIsNotNull() {
            addCriterion("is_read is not null");
            return (Criteria) this;
        }

        public Criteria andIsReadEqualTo(Integer value) {
            addCriterion("is_read =", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotEqualTo(Integer value) {
            addCriterion("is_read <>", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadGreaterThan(Integer value) {
            addCriterion("is_read >", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_read >=", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadLessThan(Integer value) {
            addCriterion("is_read <", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadLessThanOrEqualTo(Integer value) {
            addCriterion("is_read <=", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadIn(List<Integer> values) {
            addCriterion("is_read in", values, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotIn(List<Integer> values) {
            addCriterion("is_read not in", values, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadBetween(Integer value1, Integer value2) {
            addCriterion("is_read between", value1, value2, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotBetween(Integer value1, Integer value2) {
            addCriterion("is_read not between", value1, value2, "isRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadIsNull() {
            addCriterion("ios_is_read is null");
            return (Criteria) this;
        }

        public Criteria andIosIsReadIsNotNull() {
            addCriterion("ios_is_read is not null");
            return (Criteria) this;
        }

        public Criteria andIosIsReadEqualTo(Integer value) {
            addCriterion("ios_is_read =", value, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadNotEqualTo(Integer value) {
            addCriterion("ios_is_read <>", value, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadGreaterThan(Integer value) {
            addCriterion("ios_is_read >", value, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadGreaterThanOrEqualTo(Integer value) {
            addCriterion("ios_is_read >=", value, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadLessThan(Integer value) {
            addCriterion("ios_is_read <", value, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadLessThanOrEqualTo(Integer value) {
            addCriterion("ios_is_read <=", value, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadIn(List<Integer> values) {
            addCriterion("ios_is_read in", values, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadNotIn(List<Integer> values) {
            addCriterion("ios_is_read not in", values, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_read between", value1, value2, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andIosIsReadNotBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_read not between", value1, value2, "iosIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadIsNull() {
            addCriterion("web_is_read is null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadIsNotNull() {
            addCriterion("web_is_read is not null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadEqualTo(Integer value) {
            addCriterion("web_is_read =", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadNotEqualTo(Integer value) {
            addCriterion("web_is_read <>", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadGreaterThan(Integer value) {
            addCriterion("web_is_read >", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadGreaterThanOrEqualTo(Integer value) {
            addCriterion("web_is_read >=", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadLessThan(Integer value) {
            addCriterion("web_is_read <", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadLessThanOrEqualTo(Integer value) {
            addCriterion("web_is_read <=", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadIn(List<Integer> values) {
            addCriterion("web_is_read in", values, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadNotIn(List<Integer> values) {
            addCriterion("web_is_read not in", values, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read between", value1, value2, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadNotBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read not between", value1, value2, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncIsNull() {
            addCriterion("is_read_sync is null");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncIsNotNull() {
            addCriterion("is_read_sync is not null");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncEqualTo(Integer value) {
            addCriterion("is_read_sync =", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncNotEqualTo(Integer value) {
            addCriterion("is_read_sync <>", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncGreaterThan(Integer value) {
            addCriterion("is_read_sync >", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_read_sync >=", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncLessThan(Integer value) {
            addCriterion("is_read_sync <", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncLessThanOrEqualTo(Integer value) {
            addCriterion("is_read_sync <=", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncIn(List<Integer> values) {
            addCriterion("is_read_sync in", values, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncNotIn(List<Integer> values) {
            addCriterion("is_read_sync not in", values, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncBetween(Integer value1, Integer value2) {
            addCriterion("is_read_sync between", value1, value2, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncNotBetween(Integer value1, Integer value2) {
            addCriterion("is_read_sync not between", value1, value2, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncIsNull() {
            addCriterion("ios_is_read_sync is null");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncIsNotNull() {
            addCriterion("ios_is_read_sync is not null");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncEqualTo(Integer value) {
            addCriterion("ios_is_read_sync =", value, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncNotEqualTo(Integer value) {
            addCriterion("ios_is_read_sync <>", value, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncGreaterThan(Integer value) {
            addCriterion("ios_is_read_sync >", value, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncGreaterThanOrEqualTo(Integer value) {
            addCriterion("ios_is_read_sync >=", value, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncLessThan(Integer value) {
            addCriterion("ios_is_read_sync <", value, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncLessThanOrEqualTo(Integer value) {
            addCriterion("ios_is_read_sync <=", value, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncIn(List<Integer> values) {
            addCriterion("ios_is_read_sync in", values, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncNotIn(List<Integer> values) {
            addCriterion("ios_is_read_sync not in", values, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_read_sync between", value1, value2, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsReadSyncNotBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_read_sync not between", value1, value2, "iosIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncIsNull() {
            addCriterion("web_is_read_sync is null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncIsNotNull() {
            addCriterion("web_is_read_sync is not null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncEqualTo(Integer value) {
            addCriterion("web_is_read_sync =", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncNotEqualTo(Integer value) {
            addCriterion("web_is_read_sync <>", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncGreaterThan(Integer value) {
            addCriterion("web_is_read_sync >", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncGreaterThanOrEqualTo(Integer value) {
            addCriterion("web_is_read_sync >=", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncLessThan(Integer value) {
            addCriterion("web_is_read_sync <", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncLessThanOrEqualTo(Integer value) {
            addCriterion("web_is_read_sync <=", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncIn(List<Integer> values) {
            addCriterion("web_is_read_sync in", values, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncNotIn(List<Integer> values) {
            addCriterion("web_is_read_sync not in", values, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read_sync between", value1, value2, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncNotBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read_sync not between", value1, value2, "webIsReadSync");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}