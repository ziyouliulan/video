
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(用户隐私设置实体类)
 *
 * @version: V1.0
 * @author: liuhaotian
 *
 */
@Data
@ApiModel("TPrivacyConfig")
@TableName(value = "privacy_config")
@AllArgsConstructor
@NoArgsConstructor
public class PrivacyConfig implements Serializable {

	private static final long serialVersionUID = 1612161303904L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Integer id;

    @ApiModelProperty(name = "userUid", value = "用户id")
	private Integer userUid;

    @ApiModelProperty(name = "isSerachPhone", value = "是否允许搜索手机号 允许：0 不允许：1")
	private Integer isSerachPhone;

    @ApiModelProperty(name = "isSerachUid", value = "是否允许搜索账户  允许：0 不允许：1")
	private Integer isSerachUid;

    @ApiModelProperty(name = "isQr", value = "是否允许通过二维码添加  允许：0 不允许：1")
	private Integer isQr;

    @ApiModelProperty(name = "isGroup", value = "是否允许通过群聊添加  允许：0 不允许：1")
	private Integer isGroup;

    @ApiModelProperty(name = "isCard", value = "是否允许通过名片添加  允许：0 不允许：1")
	private Integer isCard;

    @ApiModelProperty(name = "isTemporary", value = "是否允许临时会话  允许：0 不允许：1")
	private Integer isTemporary;

    @ApiModelProperty(name = "isPublishLimt", value = "动态可见时间 0：最近半年  1：最近一个月  2：最近三天 3：全部")
	private Integer isPublishLimt;

    @ApiModelProperty(name = "isNotSeeMe", value = "不让他看我动态  用户id ")
	private String isNotSeeMe;

    @ApiModelProperty(name = "isNotSeeHe", value = "不看他动态 用户id")
	private String isNotSeeHe;

    @ApiModelProperty(name = "black", value = "黑名单 用户id")
	private String black;


}
