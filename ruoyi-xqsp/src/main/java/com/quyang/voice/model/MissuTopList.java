package com.quyang.voice.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-04-29
 */

@Setter
@Getter
@ToString
public class MissuTopList implements Serializable {

	private static final long serialVersionUID =  4155473949431608657L;



	@ApiModelProperty(value = "ID", hidden = true)

	private Long id;

	/**
	 * 用户
	 */

	private String userId;

	/**
	 * 置顶id
	 */

	private String topId;

	/**
	 * 排序编号
	 */

	private Long sort;

}
