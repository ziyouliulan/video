
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**   
 * @Description:TODO(评论消息助手日志实体类)
 * 
 * @version: V1.0
 * @author: H-T-C
 * 
 */
@Data
@ApiModel("TCommentMessageHelperLog")
@TableName(value = "t_comment_message_helper_log")
@AllArgsConstructor
@NoArgsConstructor
public class CommentMessageHelperLog implements Serializable {

	private static final long serialVersionUID = 1606978044970L;
	
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "id", hidden = true)
	private Long id;
    
    @ApiModelProperty(name = "msgType", value = "消息类型")
	private Integer msgType;
    
    @ApiModelProperty(name = "userId", value = "用户id")
	private Long userId;
    
    @ApiModelProperty(name = "handlerId", value = "处理者id(查看者id)")
	private Long handlerId;

	@ApiModelProperty(name = "publishId", value = "评论的动态id")
	private Long publishId;
    
    @ApiModelProperty(name = "message", value = "处理消息")
	private String message;
    
    @ApiModelProperty(name = "status", value = "阅读情况：0未读 1已读")
	private Integer status;
    
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
	private Date createTime;
    

}
