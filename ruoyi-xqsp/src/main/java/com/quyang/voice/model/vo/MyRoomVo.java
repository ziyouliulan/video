package com.quyang.voice.model.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:TODO(公会房间实体类)
 *
 * @version: V1.0
 * @author: liuhaotian
 *
 */
@Data
@ApiModel("TMyRoomVo")
@AllArgsConstructor
@NoArgsConstructor
public class MyRoomVo {
    private static final long serialVersionUID = 1599614918529L;

    @ApiModelProperty(name = "id", value = "房间id")
    private Long id;

    @ApiModelProperty(name = "roomName", value = "房间名称")
    private String roomName;

    @ApiModelProperty(name = "roomNum", value = "房间号")
    private String roomNum;

    @ApiModelProperty(name = "romPicUrl", value = "房间头像")
    private String romPicUrl;

    @ApiModelProperty(name = "onlineNum", value = "在线人数")
    private Integer onlineNum;

    @ApiModelProperty(name = "isWillEndTime", value = "房间是否即将到期 0:无提示  1： 将要到期  2：已经到期")
    private Integer isEndTime;

    @ApiModelProperty(name = "isLock", value = "是否上锁:0未开启   1开启")
    private Integer isLock;

}
