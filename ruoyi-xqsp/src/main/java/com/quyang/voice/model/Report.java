
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(举报实体类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("TReport")
@TableName(value = "t_report")
@AllArgsConstructor
@NoArgsConstructor
public class Report implements Serializable {

	private static final long serialVersionUID = 1606963212778L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
	private Long id;

    @ApiModelProperty(name = "userId", value = "发起举报的用户id")
	private Long userId;

    @ApiModelProperty(name = "reportId", value = "被举报的信息id-- 用户、房间、课程、动态")
	private Long reportId;

    @ApiModelProperty(name = "type", value = "举报种类：0 用户  1房间   2课程  3动态  4音乐   5教练")
	private Integer type;

    @ApiModelProperty(name = "message", value = "举报描述信息")
	private String message;

    @ApiModelProperty(name = "reportType", value = "举报类型（1泄露隐私，2人身攻击，3淫秽色情，4垃圾广告，5敏感信息，6侵权，7其它）")
	private Integer reportType;

    @ApiModelProperty(name = "imageUrl", value = "举报证据图片url（多张逗号分隔）")
	private String imageUrl;

    @ApiModelProperty(name = "result", value = "举报处理结果（0：未处理，1：已处理，2：已驳回")
	private Integer result;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "举报时间")
	private Date createTime;


}
