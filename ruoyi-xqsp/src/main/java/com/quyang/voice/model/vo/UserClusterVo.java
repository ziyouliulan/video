package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(用户公会群Vo类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("UserClusterVo")
@AllArgsConstructor
@NoArgsConstructor
public class UserClusterVo implements Serializable {

    @ApiModelProperty(name = "clusterName", value = "群名称")
    private String clusterName;


    @ApiModelProperty(name = "identity", value = "用户身份（0普通成员，1群主,2管理员）")
    private Integer identity;

    @ApiModelProperty(name = "stopTalk", value = "用户是否被禁言（0无，1禁）")
    private Integer stopTalk;

    @ApiModelProperty(name = "status", value = "用户是否还在群内 0：在  1：不在")
    private Integer status;

    @ApiModelProperty(name = "isAdmin", value = "是否为群管理员  0：是  1：不是")
    private Integer isAdmin;

    @ApiModelProperty(name = "allowPrivateChat", value = "群内是否允许私人聊天 0：允许  1：不允许")
    private Integer allowPrivateChat;

}
