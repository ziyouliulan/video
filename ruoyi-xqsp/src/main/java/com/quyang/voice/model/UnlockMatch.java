
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(解锁匹配实体类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("TUnlockMatch")
@TableName(value = "t_unlock_match")
@AllArgsConstructor
@NoArgsConstructor
public class UnlockMatch implements Serializable {

	private static final long serialVersionUID = 1607328182908L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
	private Long id;

    @ApiModelProperty(name = "userId", value = "用户id")
	private Long userId;

    @ApiModelProperty(name = "matchTypeId", value = "匹配类型id")
	private Long matchTypeId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "解锁时间")
	private Date createTime;


}
