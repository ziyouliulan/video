package com.quyang.voice.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(评论消息VO类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("CommentMessageVo")
@AllArgsConstructor
@NoArgsConstructor
public class CommentMessageVo implements Serializable {

    @ApiModelProperty(name = "userId",value = "用户id")
    private Long userId;

    @ApiModelProperty(name = "userName",value = "用户昵称")
    private String userName;

    @ApiModelProperty(name = "headUrl",value = "用户头像")
    private String headUrl;

    @ApiModelProperty(name = "msgType",value = "消息类型")
    private Integer msgType;

    @ApiModelProperty(name = "publishId",value = "动态id")
    private Long publishId;

    @ApiModelProperty(name = "message",value = "消息")
    private String message;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime",value = "评论时间")
    private Date createTime;
}
