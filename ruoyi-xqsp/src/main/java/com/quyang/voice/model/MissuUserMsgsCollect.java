package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * missu_user_msgs_collect
 * @author
 */
@ApiModel(value="com.quyang.voice.model.MissuUserMsgsCollect" +
        "用户聊天消息收集表 用户聊天记录表。目前默认存储15天的聊天记录" +
        "（具体的存储时长，以及如何清理，由RainbowChat服务端的Java定时器实现），" +
        "，超时后将自动转储到消息记录存档表。 收集用户消息的目的是有助于分析用户行为，" +
        "用户消息本身对公司而言没有多大意义且违背用户隐私条款，目前先这样吧，以后或需停止收集。")
@Data
public class MissuUserMsgsCollect implements Serializable {
    /**
     * 记录ID
     */
    @ApiModelProperty(value="记录ID")
    private Integer collectId;

    /**
     * 发送人的ID
     */
    @ApiModelProperty(value="发送人的ID")
    private String srcUid;

    /**
     * 接收人的ID
     */
    @ApiModelProperty(value="接收人的ID")
    private String destUid;

    /**
     * 聊天模式类型
0 正式一对一聊天；
1 临时一对一聊天；
2 群聊或世界频道聊天（当为本类型时，dest_uid字段不可为空，且当dest_uid=-1时表示群聊！）

 注：当聊天模式为群聊时，dest_uid字段值为消息发生的群组id哦。
     */
    @ApiModelProperty(value="聊天模式类型 0 正式一对一聊天； 1 临时一对一聊天； " +
            "2 群聊或世界频道聊天（当为本类型时，dest_uid字段不可为空，且当dest_uid=-1时表示群聊！）" +
            " 注：当聊天模式为群聊时，dest_uid字段值为消息发生的群组id哦。")
    private Integer chatType;

    /**
     * 聊天消息类型：
0 普通文本消息
1 图片消息
2 语音留言（就是类似于微信的对讲机功能）
3 赠送的礼品
4 索取礼品消息
5 文件消息
6 短视频消息
7 名片
8 位置消息
10 发红包消息（wlx新增）
11 撤回消息 (wlx新增)
12 禁言 (wlx新增)
13 抢红包 (wlx新增)
90 系统消息或提示信息

     */
    @ApiModelProperty(value="聊天消息类型： " +
            "0 普通文本消息\n" +
            "1 图片消息\n" +
            "2 语音留言（就是类似于微信的对讲机功能）\n" +
            "3 赠送的礼品\n" +
            "4 索取礼品消息\n" +
            "5 文件消息\n" +
            "6 短视频消息\n" +
            "7 名片\n" +
            "8 位置消息\n" +
            "10 发红包消息（wlx新增）\n" +
            "11 撤回消息 (wlx新增)\n" +
            "12 禁言 (wlx新增)\n" +
            "13 抢红包 (wlx新增)\n" +
            "90 系统消息或提示信息")
    private Integer msgType;

    /**
     * 消息内容
1）当消息类型是文本消息时，本字段存放的是文本消息内容。
2）当消息类型是图片消息时，本字段存放的是暂存于服务端的图片原文件（非缩略图）的文件名。
3）当消息类型是语音留言消息时，本字段存放的是暂存于服务端的语音文件的文件名。
4）口令红包+id、普通红包+id、手气红包+id
     */
    @ApiModelProperty(value="消息内容\n" +
            "1）当消息类型是文本消息时，本字段存放的是文本消息内容。\n" +
            "2）当消息类型是图片消息时，本字段存放的是暂存于服务端的图片原文件（非缩略图）的文件名。\n" +
            "3）当消息类型是语音留言消息时，本字段存放的是暂存于服务端的语音文件的文件名。\n" +
            "4）口令红包+id、普通红包+id、手气红包+id")
    private String msgContent;

    /**
     * 发生时间
     */
    @ApiModelProperty(value="发生时间")
    private Date msgTime;

    /**
     * 发生时间戳
     */
    @ApiModelProperty(value="发生时间戳")
    private Long msgTime2;

    /**
     * 消息发送时的在线人数：
数据可助于分析消息推送时的服务端压力等。
     */
    @ApiModelProperty(value="消息发送时的在线人数：\n" +
            "数据可助于分析消息推送时的服务端压力等。")
    private Integer onlineCount;

    /**
     * 消息指纹码(唯一id)
     */
    @ApiModelProperty(value="消息指纹码(唯一id)")
    private String fingerprint;

    /**
     * 消息读取状态：1已读 0 未读
     */
    @ApiModelProperty(value="消息读取状态：1已读 0 未读")
    private Integer isRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    @ApiModelProperty(value="消息读取状态：1已读 0 未读")
    private Integer iosIsRed;

    /**
     * 消息读取状态：1已读 0 未读
     */
    @ApiModelProperty(value="消息读取状态：1已读 0 未读")
    private Integer webIsRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    @ApiModelProperty(value="消息读取状态：1已读 0 未读")
    private Integer isReadSync;

    /**
     * 消息读取状态：1已读 0 未读
     */
    @ApiModelProperty(value="消息读取状态：1已读 0 未读")
    private Integer iosIsRedsYnc;

    /**
     * 消息读取状态：1已读 0 未读
     */
    @ApiModelProperty(value="消息读取状态：1已读 0 未读")
    private Integer webIsReadSync;

    private static final long serialVersionUID = 1L;
}
