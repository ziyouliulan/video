package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * missu_roster
 * @author
 */
@ApiModel(value="com.quyang.voice.model.MissuRoster好友列表")
@Data
public class MissuRoster extends MissuRosterKey implements Serializable {
    /**
     * 对好友的备注
     */
    @ApiModelProperty(value="对好友的备注")
    private String remark;

    /**
     * 好友添加时间
     */
    @ApiModelProperty(value="好友添加时间")
    private Date addTime;

    private static final long serialVersionUID = 1L;
}
