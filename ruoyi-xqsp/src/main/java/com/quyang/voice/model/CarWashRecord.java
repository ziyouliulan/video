package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 *
 * @TableName yuecool_car_wash_record
 */
@TableName(value ="yuecool_car_wash_record")
@Data
public class CarWashRecord implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 商家id
     */
    private Integer shopId;

    /**
     * 商家名称
     */
    private String shopName;

    /**
     * 商家名称
     */
    @TableField(exist = false)
    private String shopAvatar;

    /**
     * 用户名称
     */
    private String userName;

    @TableField(exist = false)
    private String userAvatar;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 车牌号
     */
    private String carCode;

    /**
     * 创建日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 1年费会员 2次数会员
     */
    private Integer status;

    /**
     * 洗车次数
     */
    private Double num;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
