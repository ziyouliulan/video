package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_dynamic
 * @author
 */
@ApiModel(value="com.quyang.voice.model.Dynamic动态权限设置")
@Data
public class Dynamic implements Serializable {
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    /**
     * 黑名单id
     */
    @ApiModelProperty(value="黑名单id")
    private Integer blackId;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 1:不看他人动态 2：不让别人看我的动态
     */
    @ApiModelProperty(value="1:不看他人动态 2：不让别人看我的动态")
    private Integer type;

    private static final long serialVersionUID = 1L;
}
