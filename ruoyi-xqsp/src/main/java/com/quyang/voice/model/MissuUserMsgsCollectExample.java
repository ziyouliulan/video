package com.quyang.voice.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MissuUserMsgsCollectExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public MissuUserMsgsCollectExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCollectIdIsNull() {
            addCriterion("collect_id is null");
            return (Criteria) this;
        }

        public Criteria andCollectIdIsNotNull() {
            addCriterion("collect_id is not null");
            return (Criteria) this;
        }

        public Criteria andCollectIdEqualTo(Integer value) {
            addCriterion("collect_id =", value, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdNotEqualTo(Integer value) {
            addCriterion("collect_id <>", value, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdGreaterThan(Integer value) {
            addCriterion("collect_id >", value, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("collect_id >=", value, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdLessThan(Integer value) {
            addCriterion("collect_id <", value, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdLessThanOrEqualTo(Integer value) {
            addCriterion("collect_id <=", value, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdIn(List<Integer> values) {
            addCriterion("collect_id in", values, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdNotIn(List<Integer> values) {
            addCriterion("collect_id not in", values, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdBetween(Integer value1, Integer value2) {
            addCriterion("collect_id between", value1, value2, "collectId");
            return (Criteria) this;
        }

        public Criteria andCollectIdNotBetween(Integer value1, Integer value2) {
            addCriterion("collect_id not between", value1, value2, "collectId");
            return (Criteria) this;
        }

        public Criteria andSrcUidIsNull() {
            addCriterion("src_uid is null");
            return (Criteria) this;
        }

        public Criteria andSrcUidIsNotNull() {
            addCriterion("src_uid is not null");
            return (Criteria) this;
        }

        public Criteria andSrcUidEqualTo(String value) {
            addCriterion("src_uid =", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidNotEqualTo(String value) {
            addCriterion("src_uid <>", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidGreaterThan(String value) {
            addCriterion("src_uid >", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidGreaterThanOrEqualTo(String value) {
            addCriterion("src_uid >=", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidLessThan(String value) {
            addCriterion("src_uid <", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidLessThanOrEqualTo(String value) {
            addCriterion("src_uid <=", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidLike(String value) {
            addCriterion("src_uid like", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidNotLike(String value) {
            addCriterion("src_uid not like", value, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidIn(List<String> values) {
            addCriterion("src_uid in", values, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidNotIn(List<String> values) {
            addCriterion("src_uid not in", values, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidBetween(String value1, String value2) {
            addCriterion("src_uid between", value1, value2, "srcUid");
            return (Criteria) this;
        }

        public Criteria andSrcUidNotBetween(String value1, String value2) {
            addCriterion("src_uid not between", value1, value2, "srcUid");
            return (Criteria) this;
        }

        public Criteria andDestUidIsNull() {
            addCriterion("dest_uid is null");
            return (Criteria) this;
        }

        public Criteria andDestUidIsNotNull() {
            addCriterion("dest_uid is not null");
            return (Criteria) this;
        }

        public Criteria andDestUidEqualTo(String value) {
            addCriterion("dest_uid =", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidNotEqualTo(String value) {
            addCriterion("dest_uid <>", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidGreaterThan(String value) {
            addCriterion("dest_uid >", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidGreaterThanOrEqualTo(String value) {
            addCriterion("dest_uid >=", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidLessThan(String value) {
            addCriterion("dest_uid <", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidLessThanOrEqualTo(String value) {
            addCriterion("dest_uid <=", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidLike(String value) {
            addCriterion("dest_uid like", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidNotLike(String value) {
            addCriterion("dest_uid not like", value, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidIn(List<String> values) {
            addCriterion("dest_uid in", values, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidNotIn(List<String> values) {
            addCriterion("dest_uid not in", values, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidBetween(String value1, String value2) {
            addCriterion("dest_uid between", value1, value2, "destUid");
            return (Criteria) this;
        }

        public Criteria andDestUidNotBetween(String value1, String value2) {
            addCriterion("dest_uid not between", value1, value2, "destUid");
            return (Criteria) this;
        }

        public Criteria andChatTypeIsNull() {
            addCriterion("chat_type is null");
            return (Criteria) this;
        }

        public Criteria andChatTypeIsNotNull() {
            addCriterion("chat_type is not null");
            return (Criteria) this;
        }

        public Criteria andChatTypeEqualTo(Integer value) {
            addCriterion("chat_type =", value, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeNotEqualTo(Integer value) {
            addCriterion("chat_type <>", value, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeGreaterThan(Integer value) {
            addCriterion("chat_type >", value, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("chat_type >=", value, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeLessThan(Integer value) {
            addCriterion("chat_type <", value, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeLessThanOrEqualTo(Integer value) {
            addCriterion("chat_type <=", value, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeIn(List<Integer> values) {
            addCriterion("chat_type in", values, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeNotIn(List<Integer> values) {
            addCriterion("chat_type not in", values, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeBetween(Integer value1, Integer value2) {
            addCriterion("chat_type between", value1, value2, "chatType");
            return (Criteria) this;
        }

        public Criteria andChatTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("chat_type not between", value1, value2, "chatType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeIsNull() {
            addCriterion("msg_type is null");
            return (Criteria) this;
        }

        public Criteria andMsgTypeIsNotNull() {
            addCriterion("msg_type is not null");
            return (Criteria) this;
        }

        public Criteria andMsgTypeEqualTo(Integer value) {
            addCriterion("msg_type =", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeNotEqualTo(Integer value) {
            addCriterion("msg_type <>", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeGreaterThan(Integer value) {
            addCriterion("msg_type >", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_type >=", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeLessThan(Integer value) {
            addCriterion("msg_type <", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeLessThanOrEqualTo(Integer value) {
            addCriterion("msg_type <=", value, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeIn(List<Integer> values) {
            addCriterion("msg_type in", values, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeNotIn(List<Integer> values) {
            addCriterion("msg_type not in", values, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeBetween(Integer value1, Integer value2) {
            addCriterion("msg_type between", value1, value2, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_type not between", value1, value2, "msgType");
            return (Criteria) this;
        }

        public Criteria andMsgContentIsNull() {
            addCriterion("msg_content is null");
            return (Criteria) this;
        }

        public Criteria andMsgContentIsNotNull() {
            addCriterion("msg_content is not null");
            return (Criteria) this;
        }

        public Criteria andMsgContentEqualTo(String value) {
            addCriterion("msg_content =", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentNotEqualTo(String value) {
            addCriterion("msg_content <>", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentGreaterThan(String value) {
            addCriterion("msg_content >", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentGreaterThanOrEqualTo(String value) {
            addCriterion("msg_content >=", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentLessThan(String value) {
            addCriterion("msg_content <", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentLessThanOrEqualTo(String value) {
            addCriterion("msg_content <=", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentLike(String value) {
            addCriterion("msg_content like", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentNotLike(String value) {
            addCriterion("msg_content not like", value, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentIn(List<String> values) {
            addCriterion("msg_content in", values, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentNotIn(List<String> values) {
            addCriterion("msg_content not in", values, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentBetween(String value1, String value2) {
            addCriterion("msg_content between", value1, value2, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgContentNotBetween(String value1, String value2) {
            addCriterion("msg_content not between", value1, value2, "msgContent");
            return (Criteria) this;
        }

        public Criteria andMsgTimeIsNull() {
            addCriterion("msg_time is null");
            return (Criteria) this;
        }

        public Criteria andMsgTimeIsNotNull() {
            addCriterion("msg_time is not null");
            return (Criteria) this;
        }

        public Criteria andMsgTimeEqualTo(Date value) {
            addCriterion("msg_time =", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeNotEqualTo(Date value) {
            addCriterion("msg_time <>", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeGreaterThan(Date value) {
            addCriterion("msg_time >", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("msg_time >=", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeLessThan(Date value) {
            addCriterion("msg_time <", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeLessThanOrEqualTo(Date value) {
            addCriterion("msg_time <=", value, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeIn(List<Date> values) {
            addCriterion("msg_time in", values, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeNotIn(List<Date> values) {
            addCriterion("msg_time not in", values, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeBetween(Date value1, Date value2) {
            addCriterion("msg_time between", value1, value2, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTimeNotBetween(Date value1, Date value2) {
            addCriterion("msg_time not between", value1, value2, "msgTime");
            return (Criteria) this;
        }

        public Criteria andMsgTime2IsNull() {
            addCriterion("msg_time2 is null");
            return (Criteria) this;
        }

        public Criteria andMsgTime2IsNotNull() {
            addCriterion("msg_time2 is not null");
            return (Criteria) this;
        }

        public Criteria andMsgTime2EqualTo(Long value) {
            addCriterion("msg_time2 =", value, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2NotEqualTo(Long value) {
            addCriterion("msg_time2 <>", value, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2GreaterThan(Long value) {
            addCriterion("msg_time2 >", value, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2GreaterThanOrEqualTo(Long value) {
            addCriterion("msg_time2 >=", value, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2LessThan(Long value) {
            addCriterion("msg_time2 <", value, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2LessThanOrEqualTo(Long value) {
            addCriterion("msg_time2 <=", value, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2In(List<Long> values) {
            addCriterion("msg_time2 in", values, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2NotIn(List<Long> values) {
            addCriterion("msg_time2 not in", values, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2Between(Long value1, Long value2) {
            addCriterion("msg_time2 between", value1, value2, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andMsgTime2NotBetween(Long value1, Long value2) {
            addCriterion("msg_time2 not between", value1, value2, "msgTime2");
            return (Criteria) this;
        }

        public Criteria andOnlineCountIsNull() {
            addCriterion("online_count is null");
            return (Criteria) this;
        }

        public Criteria andOnlineCountIsNotNull() {
            addCriterion("online_count is not null");
            return (Criteria) this;
        }

        public Criteria andOnlineCountEqualTo(Integer value) {
            addCriterion("online_count =", value, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountNotEqualTo(Integer value) {
            addCriterion("online_count <>", value, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountGreaterThan(Integer value) {
            addCriterion("online_count >", value, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("online_count >=", value, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountLessThan(Integer value) {
            addCriterion("online_count <", value, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountLessThanOrEqualTo(Integer value) {
            addCriterion("online_count <=", value, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountIn(List<Integer> values) {
            addCriterion("online_count in", values, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountNotIn(List<Integer> values) {
            addCriterion("online_count not in", values, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountBetween(Integer value1, Integer value2) {
            addCriterion("online_count between", value1, value2, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andOnlineCountNotBetween(Integer value1, Integer value2) {
            addCriterion("online_count not between", value1, value2, "onlineCount");
            return (Criteria) this;
        }

        public Criteria andFingerprintIsNull() {
            addCriterion("fingerprint is null");
            return (Criteria) this;
        }

        public Criteria andFingerprintIsNotNull() {
            addCriterion("fingerprint is not null");
            return (Criteria) this;
        }

        public Criteria andFingerprintEqualTo(String value) {
            addCriterion("fingerprint =", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintNotEqualTo(String value) {
            addCriterion("fingerprint <>", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintGreaterThan(String value) {
            addCriterion("fingerprint >", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintGreaterThanOrEqualTo(String value) {
            addCriterion("fingerprint >=", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintLessThan(String value) {
            addCriterion("fingerprint <", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintLessThanOrEqualTo(String value) {
            addCriterion("fingerprint <=", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintLike(String value) {
            addCriterion("fingerprint like", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintNotLike(String value) {
            addCriterion("fingerprint not like", value, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintIn(List<String> values) {
            addCriterion("fingerprint in", values, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintNotIn(List<String> values) {
            addCriterion("fingerprint not in", values, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintBetween(String value1, String value2) {
            addCriterion("fingerprint between", value1, value2, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andFingerprintNotBetween(String value1, String value2) {
            addCriterion("fingerprint not between", value1, value2, "fingerprint");
            return (Criteria) this;
        }

        public Criteria andIsReadIsNull() {
            addCriterion("is_read is null");
            return (Criteria) this;
        }

        public Criteria andIsReadIsNotNull() {
            addCriterion("is_read is not null");
            return (Criteria) this;
        }

        public Criteria andIsReadEqualTo(Integer value) {
            addCriterion("is_read =", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotEqualTo(Integer value) {
            addCriterion("is_read <>", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadGreaterThan(Integer value) {
            addCriterion("is_read >", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_read >=", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadLessThan(Integer value) {
            addCriterion("is_read <", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadLessThanOrEqualTo(Integer value) {
            addCriterion("is_read <=", value, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadIn(List<Integer> values) {
            addCriterion("is_read in", values, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotIn(List<Integer> values) {
            addCriterion("is_read not in", values, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadBetween(Integer value1, Integer value2) {
            addCriterion("is_read between", value1, value2, "isRead");
            return (Criteria) this;
        }

        public Criteria andIsReadNotBetween(Integer value1, Integer value2) {
            addCriterion("is_read not between", value1, value2, "isRead");
            return (Criteria) this;
        }

        public Criteria andIosIsRedIsNull() {
            addCriterion("ios_is_red is null");
            return (Criteria) this;
        }

        public Criteria andIosIsRedIsNotNull() {
            addCriterion("ios_is_red is not null");
            return (Criteria) this;
        }

        public Criteria andIosIsRedEqualTo(Integer value) {
            addCriterion("ios_is_red =", value, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedNotEqualTo(Integer value) {
            addCriterion("ios_is_red <>", value, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedGreaterThan(Integer value) {
            addCriterion("ios_is_red >", value, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedGreaterThanOrEqualTo(Integer value) {
            addCriterion("ios_is_red >=", value, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedLessThan(Integer value) {
            addCriterion("ios_is_red <", value, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedLessThanOrEqualTo(Integer value) {
            addCriterion("ios_is_red <=", value, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedIn(List<Integer> values) {
            addCriterion("ios_is_red in", values, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedNotIn(List<Integer> values) {
            addCriterion("ios_is_red not in", values, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_red between", value1, value2, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andIosIsRedNotBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_red not between", value1, value2, "iosIsRed");
            return (Criteria) this;
        }

        public Criteria andWebIsReadIsNull() {
            addCriterion("web_is_read is null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadIsNotNull() {
            addCriterion("web_is_read is not null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadEqualTo(Integer value) {
            addCriterion("web_is_read =", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadNotEqualTo(Integer value) {
            addCriterion("web_is_read <>", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadGreaterThan(Integer value) {
            addCriterion("web_is_read >", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadGreaterThanOrEqualTo(Integer value) {
            addCriterion("web_is_read >=", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadLessThan(Integer value) {
            addCriterion("web_is_read <", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadLessThanOrEqualTo(Integer value) {
            addCriterion("web_is_read <=", value, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadIn(List<Integer> values) {
            addCriterion("web_is_read in", values, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadNotIn(List<Integer> values) {
            addCriterion("web_is_read not in", values, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read between", value1, value2, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andWebIsReadNotBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read not between", value1, value2, "webIsRead");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncIsNull() {
            addCriterion("is_read_sync is null");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncIsNotNull() {
            addCriterion("is_read_sync is not null");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncEqualTo(Integer value) {
            addCriterion("is_read_sync =", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncNotEqualTo(Integer value) {
            addCriterion("is_read_sync <>", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncGreaterThan(Integer value) {
            addCriterion("is_read_sync >", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_read_sync >=", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncLessThan(Integer value) {
            addCriterion("is_read_sync <", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncLessThanOrEqualTo(Integer value) {
            addCriterion("is_read_sync <=", value, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncIn(List<Integer> values) {
            addCriterion("is_read_sync in", values, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncNotIn(List<Integer> values) {
            addCriterion("is_read_sync not in", values, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncBetween(Integer value1, Integer value2) {
            addCriterion("is_read_sync between", value1, value2, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIsReadSyncNotBetween(Integer value1, Integer value2) {
            addCriterion("is_read_sync not between", value1, value2, "isReadSync");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncIsNull() {
            addCriterion("ios_is_reds_ync is null");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncIsNotNull() {
            addCriterion("ios_is_reds_ync is not null");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncEqualTo(Integer value) {
            addCriterion("ios_is_reds_ync =", value, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncNotEqualTo(Integer value) {
            addCriterion("ios_is_reds_ync <>", value, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncGreaterThan(Integer value) {
            addCriterion("ios_is_reds_ync >", value, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncGreaterThanOrEqualTo(Integer value) {
            addCriterion("ios_is_reds_ync >=", value, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncLessThan(Integer value) {
            addCriterion("ios_is_reds_ync <", value, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncLessThanOrEqualTo(Integer value) {
            addCriterion("ios_is_reds_ync <=", value, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncIn(List<Integer> values) {
            addCriterion("ios_is_reds_ync in", values, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncNotIn(List<Integer> values) {
            addCriterion("ios_is_reds_ync not in", values, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_reds_ync between", value1, value2, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andIosIsRedsYncNotBetween(Integer value1, Integer value2) {
            addCriterion("ios_is_reds_ync not between", value1, value2, "iosIsRedsYnc");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncIsNull() {
            addCriterion("web_is_read_sync is null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncIsNotNull() {
            addCriterion("web_is_read_sync is not null");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncEqualTo(Integer value) {
            addCriterion("web_is_read_sync =", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncNotEqualTo(Integer value) {
            addCriterion("web_is_read_sync <>", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncGreaterThan(Integer value) {
            addCriterion("web_is_read_sync >", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncGreaterThanOrEqualTo(Integer value) {
            addCriterion("web_is_read_sync >=", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncLessThan(Integer value) {
            addCriterion("web_is_read_sync <", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncLessThanOrEqualTo(Integer value) {
            addCriterion("web_is_read_sync <=", value, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncIn(List<Integer> values) {
            addCriterion("web_is_read_sync in", values, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncNotIn(List<Integer> values) {
            addCriterion("web_is_read_sync not in", values, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read_sync between", value1, value2, "webIsReadSync");
            return (Criteria) this;
        }

        public Criteria andWebIsReadSyncNotBetween(Integer value1, Integer value2) {
            addCriterion("web_is_read_sync not between", value1, value2, "webIsReadSync");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}