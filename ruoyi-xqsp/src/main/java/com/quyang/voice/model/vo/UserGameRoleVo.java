package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("UserGameRoleVo")
@AllArgsConstructor
@NoArgsConstructor
public class UserGameRoleVo implements Serializable {


    @ApiModelProperty(name = "id", value = "该记录id")
    private Long id;

    @ApiModelProperty(name = "userId", value = "用户id")
    private Long userId;

    @ApiModelProperty(name = "gameId", value = "游戏id")
    private Long gameId;

    @ApiModelProperty(name = "rankId", value = "段位id")
    private Long rankId;

    @ApiModelProperty(name = "areaId", value = "区服id")
    private Long areaId;

    @ApiModelProperty(name = "gameName", value = "游戏名称")
    private String gameName;

    @ApiModelProperty(name = "nickName", value = "昵称")
    private String nickName;

    @ApiModelProperty(name = "rankName", value = "段位名称")
    private String rankName;

    @ApiModelProperty(name = "areaName", value = "区服名称")
    private String areaName;

    @ApiModelProperty(name = "imageUrl", value = "游戏图片")
    private String imageUrl;

    @ApiModelProperty(name = "styleIds", value = "游戏风格id字符串")
    private String styleIds;



}
