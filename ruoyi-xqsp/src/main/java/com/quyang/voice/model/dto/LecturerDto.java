package com.quyang.voice.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LecturerDto {


    @ApiModelProperty("讲师名称")
    String name;


    @ApiModelProperty("讲师id")
    String userId;


    @ApiModelProperty("是否推荐讲师  1： 是  0：否")
    String isRecommend;


    @ApiModelProperty("讲师类别")
    Integer categoryId;


}
