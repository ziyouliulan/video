
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(群成员实体类)
 *
 * @version: V1.0
 * @author: sky
 *
 */
@Data
@ApiModel("TGroupMembers")
@TableName(value = "group_members")
@AllArgsConstructor
@NoArgsConstructor
public class GroupMembers implements Serializable {

	private static final long serialVersionUID = 1614326198030L;

    @ApiModelProperty(name = "userRecordId", value = "群成员记录id： 是指存放于数据库中的此条数据记录唯一id。")
	private Integer userRecordId;

    @ApiModelProperty(name = "userUid", value = "用户uid")
	private Integer userUid;

    @ApiModelProperty(name = "gId", value = "群id")
	private String gId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "joinTime", value = "加入本群的时间")
	private Date joinTime;

    @ApiModelProperty(name = "beInviteUserId", value = "邀请人id： 本人被谁邀请进群的（非邀请的情况，本字段则可为空）。")
	private Integer beInviteUserId;

	@ApiModelProperty(name = "isAdmin", value = "是否为管理员 0：是  1：否")
	private Integer isAdmin;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "beOwnerTime", value = "何时成为群主： * 创建群时，创建者自动成为群主，本时间就是创建群的时间； * 进行群转让人，本字段填的就是最近一次被转让时的时间。")
	private Date beOwnerTime;

    @ApiModelProperty(name = "nicknameIngroup", value = "本群昵称")
	private String nicknameIngroup;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "msgTimeStart", value = "消息起始时间")
	private Date msgTimeStart;


}
