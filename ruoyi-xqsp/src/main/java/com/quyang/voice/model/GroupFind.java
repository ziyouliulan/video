package com.quyang.voice.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-05-11
 */

@Setter
@Getter
@ToString

public class GroupFind  implements Serializable {

	private static final long serialVersionUID =  1372962522119012543L;

	/**
	 * id
	 */


	private Long id;

	/**
	 * 标题
	 */

	private String name;

	/**
	 * 链接地址
	 */

	private String addressUrl;

	/**
	 * 图片地址
	 */

	private String imgUrl;

	/**
	 * 备注
	 */

	private String remark;

}
