package com.quyang.voice.model;

import java.util.ArrayList;
import java.util.List;

public class PersonalSettingsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public PersonalSettingsExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneIsNull() {
            addCriterion("is_search_phone is null");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneIsNotNull() {
            addCriterion("is_search_phone is not null");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneEqualTo(Integer value) {
            addCriterion("is_search_phone =", value, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneNotEqualTo(Integer value) {
            addCriterion("is_search_phone <>", value, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneGreaterThan(Integer value) {
            addCriterion("is_search_phone >", value, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_search_phone >=", value, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneLessThan(Integer value) {
            addCriterion("is_search_phone <", value, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneLessThanOrEqualTo(Integer value) {
            addCriterion("is_search_phone <=", value, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneIn(List<Integer> values) {
            addCriterion("is_search_phone in", values, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneNotIn(List<Integer> values) {
            addCriterion("is_search_phone not in", values, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneBetween(Integer value1, Integer value2) {
            addCriterion("is_search_phone between", value1, value2, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchPhoneNotBetween(Integer value1, Integer value2) {
            addCriterion("is_search_phone not between", value1, value2, "isSearchPhone");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameIsNull() {
            addCriterion("is_search_username is null");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameIsNotNull() {
            addCriterion("is_search_username is not null");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameEqualTo(Integer value) {
            addCriterion("is_search_username =", value, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameNotEqualTo(Integer value) {
            addCriterion("is_search_username <>", value, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameGreaterThan(Integer value) {
            addCriterion("is_search_username >", value, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_search_username >=", value, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameLessThan(Integer value) {
            addCriterion("is_search_username <", value, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameLessThanOrEqualTo(Integer value) {
            addCriterion("is_search_username <=", value, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameIn(List<Integer> values) {
            addCriterion("is_search_username in", values, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameNotIn(List<Integer> values) {
            addCriterion("is_search_username not in", values, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameBetween(Integer value1, Integer value2) {
            addCriterion("is_search_username between", value1, value2, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchUsernameNotBetween(Integer value1, Integer value2) {
            addCriterion("is_search_username not between", value1, value2, "isSearchUsername");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdIsNull() {
            addCriterion("is_search_id is null");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdIsNotNull() {
            addCriterion("is_search_id is not null");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdEqualTo(Integer value) {
            addCriterion("is_search_id =", value, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdNotEqualTo(Integer value) {
            addCriterion("is_search_id <>", value, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdGreaterThan(Integer value) {
            addCriterion("is_search_id >", value, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_search_id >=", value, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdLessThan(Integer value) {
            addCriterion("is_search_id <", value, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdLessThanOrEqualTo(Integer value) {
            addCriterion("is_search_id <=", value, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdIn(List<Integer> values) {
            addCriterion("is_search_id in", values, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdNotIn(List<Integer> values) {
            addCriterion("is_search_id not in", values, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdBetween(Integer value1, Integer value2) {
            addCriterion("is_search_id between", value1, value2, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andIsSearchIdNotBetween(Integer value1, Integer value2) {
            addCriterion("is_search_id not between", value1, value2, "isSearchId");
            return (Criteria) this;
        }

        public Criteria andScanCodeIsNull() {
            addCriterion("scan_code is null");
            return (Criteria) this;
        }

        public Criteria andScanCodeIsNotNull() {
            addCriterion("scan_code is not null");
            return (Criteria) this;
        }

        public Criteria andScanCodeEqualTo(Integer value) {
            addCriterion("scan_code =", value, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeNotEqualTo(Integer value) {
            addCriterion("scan_code <>", value, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeGreaterThan(Integer value) {
            addCriterion("scan_code >", value, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeGreaterThanOrEqualTo(Integer value) {
            addCriterion("scan_code >=", value, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeLessThan(Integer value) {
            addCriterion("scan_code <", value, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeLessThanOrEqualTo(Integer value) {
            addCriterion("scan_code <=", value, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeIn(List<Integer> values) {
            addCriterion("scan_code in", values, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeNotIn(List<Integer> values) {
            addCriterion("scan_code not in", values, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeBetween(Integer value1, Integer value2) {
            addCriterion("scan_code between", value1, value2, "scanCode");
            return (Criteria) this;
        }

        public Criteria andScanCodeNotBetween(Integer value1, Integer value2) {
            addCriterion("scan_code not between", value1, value2, "scanCode");
            return (Criteria) this;
        }

        public Criteria andGroupChatIsNull() {
            addCriterion("group_chat is null");
            return (Criteria) this;
        }

        public Criteria andGroupChatIsNotNull() {
            addCriterion("group_chat is not null");
            return (Criteria) this;
        }

        public Criteria andGroupChatEqualTo(Integer value) {
            addCriterion("group_chat =", value, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatNotEqualTo(Integer value) {
            addCriterion("group_chat <>", value, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatGreaterThan(Integer value) {
            addCriterion("group_chat >", value, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatGreaterThanOrEqualTo(Integer value) {
            addCriterion("group_chat >=", value, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatLessThan(Integer value) {
            addCriterion("group_chat <", value, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatLessThanOrEqualTo(Integer value) {
            addCriterion("group_chat <=", value, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatIn(List<Integer> values) {
            addCriterion("group_chat in", values, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatNotIn(List<Integer> values) {
            addCriterion("group_chat not in", values, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatBetween(Integer value1, Integer value2) {
            addCriterion("group_chat between", value1, value2, "groupChat");
            return (Criteria) this;
        }

        public Criteria andGroupChatNotBetween(Integer value1, Integer value2) {
            addCriterion("group_chat not between", value1, value2, "groupChat");
            return (Criteria) this;
        }

        public Criteria andBusinessCardIsNull() {
            addCriterion("business_card is null");
            return (Criteria) this;
        }

        public Criteria andBusinessCardIsNotNull() {
            addCriterion("business_card is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessCardEqualTo(Integer value) {
            addCriterion("business_card =", value, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardNotEqualTo(Integer value) {
            addCriterion("business_card <>", value, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardGreaterThan(Integer value) {
            addCriterion("business_card >", value, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardGreaterThanOrEqualTo(Integer value) {
            addCriterion("business_card >=", value, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardLessThan(Integer value) {
            addCriterion("business_card <", value, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardLessThanOrEqualTo(Integer value) {
            addCriterion("business_card <=", value, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardIn(List<Integer> values) {
            addCriterion("business_card in", values, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardNotIn(List<Integer> values) {
            addCriterion("business_card not in", values, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardBetween(Integer value1, Integer value2) {
            addCriterion("business_card between", value1, value2, "businessCard");
            return (Criteria) this;
        }

        public Criteria andBusinessCardNotBetween(Integer value1, Integer value2) {
            addCriterion("business_card not between", value1, value2, "businessCard");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionIsNull() {
            addCriterion("temporary_session is null");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionIsNotNull() {
            addCriterion("temporary_session is not null");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionEqualTo(Integer value) {
            addCriterion("temporary_session =", value, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionNotEqualTo(Integer value) {
            addCriterion("temporary_session <>", value, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionGreaterThan(Integer value) {
            addCriterion("temporary_session >", value, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionGreaterThanOrEqualTo(Integer value) {
            addCriterion("temporary_session >=", value, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionLessThan(Integer value) {
            addCriterion("temporary_session <", value, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionLessThanOrEqualTo(Integer value) {
            addCriterion("temporary_session <=", value, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionIn(List<Integer> values) {
            addCriterion("temporary_session in", values, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionNotIn(List<Integer> values) {
            addCriterion("temporary_session not in", values, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionBetween(Integer value1, Integer value2) {
            addCriterion("temporary_session between", value1, value2, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andTemporarySessionNotBetween(Integer value1, Integer value2) {
            addCriterion("temporary_session not between", value1, value2, "temporarySession");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeIsNull() {
            addCriterion("dynamic_display_time is null");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeIsNotNull() {
            addCriterion("dynamic_display_time is not null");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeEqualTo(Integer value) {
            addCriterion("dynamic_display_time =", value, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeNotEqualTo(Integer value) {
            addCriterion("dynamic_display_time <>", value, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeGreaterThan(Integer value) {
            addCriterion("dynamic_display_time >", value, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("dynamic_display_time >=", value, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeLessThan(Integer value) {
            addCriterion("dynamic_display_time <", value, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeLessThanOrEqualTo(Integer value) {
            addCriterion("dynamic_display_time <=", value, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeIn(List<Integer> values) {
            addCriterion("dynamic_display_time in", values, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeNotIn(List<Integer> values) {
            addCriterion("dynamic_display_time not in", values, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeBetween(Integer value1, Integer value2) {
            addCriterion("dynamic_display_time between", value1, value2, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andDynamicDisplayTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("dynamic_display_time not between", value1, value2, "dynamicDisplayTime");
            return (Criteria) this;
        }

        public Criteria andIsReadDelIsNull() {
            addCriterion("is_read_del is null");
            return (Criteria) this;
        }

        public Criteria andIsReadDelIsNotNull() {
            addCriterion("is_read_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsReadDelEqualTo(Integer value) {
            addCriterion("is_read_del =", value, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelNotEqualTo(Integer value) {
            addCriterion("is_read_del <>", value, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelGreaterThan(Integer value) {
            addCriterion("is_read_del >", value, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_read_del >=", value, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelLessThan(Integer value) {
            addCriterion("is_read_del <", value, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_read_del <=", value, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelIn(List<Integer> values) {
            addCriterion("is_read_del in", values, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelNotIn(List<Integer> values) {
            addCriterion("is_read_del not in", values, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelBetween(Integer value1, Integer value2) {
            addCriterion("is_read_del between", value1, value2, "isReadDel");
            return (Criteria) this;
        }

        public Criteria andIsReadDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_read_del not between", value1, value2, "isReadDel");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}