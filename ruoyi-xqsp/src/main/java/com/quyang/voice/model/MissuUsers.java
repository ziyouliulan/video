
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(用户表实体类)
 *
 * @version: V1.0
 * @author: liuhaotian
 *
 */
@Data
@ApiModel("TMissuUsers")
@TableName(value = "missu_users")
@AllArgsConstructor
@NoArgsConstructor
public class MissuUsers implements Serializable {

	private static final long serialVersionUID = 1612233089986L;

    @ApiModelProperty(name = "userUid", value = "id")
	@TableId(value = "user_uid" ,type = IdType.AUTO)
	private Integer userUid;

    @ApiModelProperty(name = "userMail", value = "新：用户名（唯一）  老：邮箱")
	private String userMail;

    @ApiModelProperty(name = "age", value = "年龄")
	private Integer age;

    @ApiModelProperty(name = "nickname", value = "昵称")
	private String nickname;

    @ApiModelProperty(name = "userPsw", value = "登陆密码")
	private String userPsw;

    @ApiModelProperty(name = "userSex", value = "1 男，0 女  默认是1")
	private Integer userSex;

    @ApiModelProperty(name = "registerIp", value = "注册时IP")
	private String registerIp;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "registerTime", value = "注册时间")
	private Date registerTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "latestLoginTime", value = "最近一次登陆时间：此时间是服务器的默认时区时间（当前是北京时间）。")
	private Date latestLoginTime;

    @ApiModelProperty(name = "latestLoginTime2", value = "最近一次登陆时间：时间戳")
	private String latestLoginTime2;

    @ApiModelProperty(name = "latestLoginIp", value = "最近一次登陆IP")
	private String latestLoginIp;

    @ApiModelProperty(name = "userStatus", value = " 用户状态：表示该用户处于禁用状（0用户不能允许登陆）， 1表示正常登陆状态。默认值是1。")
	private Integer userStatus;

    @ApiModelProperty(name = "isOnline", value = "是否在线：0 表示不在线，1 表示在线。")
	private Integer isOnline;

    @ApiModelProperty(name = "verificationCode", value = "忘记密码验证码：8位数字字母随机数")
	private String verificationCode;

    @ApiModelProperty(name = "verificationTime", value = "忘记密码操作时间：半小时内验证码有效，否则失效，默认getdate()")
	private String verificationTime;

    @ApiModelProperty(name = "userAvatarFileName", value = "用户头像文件名")
	private String userAvatarFileName;

    @ApiModelProperty(name = "whatSUp", value = "用户心情：仿微信哦，60个字符（微信就是这么多）。")
	private String whatSUp;

    @ApiModelProperty(name = "maxFriend", value = "允许的最多好友个数")
	private Integer maxFriend;

    @ApiModelProperty(name = "userDesc", value = "个人说明")
	private String userDesc;

    @ApiModelProperty(name = "userType", value = "用户类型")
	private Integer userType;

    @ApiModelProperty(name = "userRegieon", value = "用户所属地区")
	private String userRegieon;

    @ApiModelProperty(name = "iosDeviceToken", value = "ios设备token")
	private String iosDeviceToken;

    @ApiModelProperty(name = "userPhone", value = "用户手机号")
	private String userPhone;

    @ApiModelProperty(name = "balance", value = "余额")
	private Double balance;

    @ApiModelProperty(name = "loginLot", value = "登陆经度")
	private String loginLot;

    @ApiModelProperty(name = "loginLat", value = "登陆纬度")
	private String loginLat;


	@ApiModelProperty(name = "UserLevel", value = "等级id")
	private Integer UserLevelId;

	@ApiModelProperty(name = "reCommunicationNumber", value = "邀请人的")
	private Integer reCommunicationNumber;


	@ApiModelProperty(name = "myCommunicationNumber", value = "我的邀请码")
	private Integer myCommunicationNumber;

	private String latestLoginAddres;
	private String registerAddres;

	@ApiModelProperty(name = "userCornet", value = "短号")
	private Integer userCornet;



	@ApiModelProperty(name = "googleSecret",value = "谷歌验证码")
	private String googleSecret;


	@ApiModelProperty(name = "openId",value = "微信id")
	private String openId;


	private String region;

	private String province;

	private String city;
	private Double distributionBonus;

	private String carCode;


	private String realName;


	private String idCardNo;

	private Boolean realPersonAuthentication;





}
