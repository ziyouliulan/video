package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_burn_after_reading
 * @author
 */
@ApiModel(value="com.quyang.voice.model.BurnAfterReading阅后即焚名单")
@Data
public class BurnAfterReading implements Serializable {
    private Integer id;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    /**
     * 阅后即焚id
     */
    @ApiModelProperty(value="阅后即焚id")
    private Integer readUserId;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 时间戳
     */
    @ApiModelProperty(value="时间戳")
    private Integer time;

    private static final long serialVersionUID = 1L;
}
