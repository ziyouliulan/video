package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * yuecool_personal_settings
 * @author
 */
@ApiModel(value="com.quyang.voice.model.PersonalSettings")
@Data
public class PersonalSettings implements Serializable {
    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    /**
     * 手机号搜索 1:开启  0关闭
     */
    @ApiModelProperty(value="手机号搜索 1:开启  0关闭")
    private Integer isSearchPhone;

    /**
     * 账号搜索 1:开启  0关闭
     */
    @ApiModelProperty(value="账号搜索 1:开启  0关闭")
    private Integer isSearchUsername;

    /**
     * id号搜索1:开启  0关闭
     */
    @ApiModelProperty(value="id号搜索1:开启  0关闭")
    private Integer isSearchId;

    /**
     * 二维码添加 1:开启  0关闭
     */
    @ApiModelProperty(value="二维码添加 1:开启  0关闭")
    private Integer scanCode;

    /**
     * 群聊添加 1:开启  0关闭
     */
    @ApiModelProperty(value="群聊添加 1:开启  0关闭")
    private Integer groupChat;

    /**
     * 名片添加：1:开启  0关闭
     */
    @ApiModelProperty(value="名片添加：1:开启  0关闭")
    private Integer businessCard;

    /**
     * 临时会话添加：1:开启  0关闭
     */
    @ApiModelProperty(value="临时会话添加：1:开启  0关闭")
    private Integer temporarySession;

    /**
     * 动态可见时间：1全部  2最近半年 3最近一个月 4 最近三天
     */
    @ApiModelProperty(value="动态可见时间：1全部  2最近半年 3最近一个月 4 最近三天")
    private Integer dynamicDisplayTime;

    /**
     * 阅后即焚：1开启，2关闭
     */
    @ApiModelProperty(value="阅后即焚：1开启，2关闭")
    private Integer isReadDel;

    private static final long serialVersionUID = 1L;
}
