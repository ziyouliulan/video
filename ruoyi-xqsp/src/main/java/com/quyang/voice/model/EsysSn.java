package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * esys_sn
 * @author
 */
@ApiModel(value="com.quyang.voice.model.EsysSn序列号生成规则配置表。 生成规则为：头辅助信息＋当前值[规范化为设定位数的字符串，不够的前面补")
@Data
public class EsysSn implements Serializable {
    /**
     * sn_name
     */
    @ApiModelProperty(value="sn_name")
    private String snName;

    /**
     * 当前值
     */
    @ApiModelProperty(value="当前值")
    private Long currentVal;

    /**
     * 最小值
     */
    @ApiModelProperty(value="最小值")
    private Long minVal;

    /**
     * 最大值
     */
    @ApiModelProperty(value="最大值")
    private Long maxVal;

    /**
     * 位数
     */
    @ApiModelProperty(value="位数")
    private Short digit;

    /**
     * 头辅助信息
     */
    @ApiModelProperty(value="头辅助信息")
    private String headStr;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String bz;

    /**
     * 建立时间
     */
    @ApiModelProperty(value="建立时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date genTime;

    private static final long serialVersionUID = 1L;
}
