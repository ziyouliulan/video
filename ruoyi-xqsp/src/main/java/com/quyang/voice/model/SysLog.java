package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统日志
 * @TableName sys_log
 */
@TableName(value ="sys_log")
@Data
public class SysLog implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long logId;

    /**
     * 接口描述
     */
    private String description;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 方法名称
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 返回值
     */
    private String result;

    /**
     * 用户ip
     */
    private String requestIp;

    /**
     * 运行时间
     */
    private Long time;

    /**
     * 用户名
     */
    private String username;

    /**
     * 地址
     */
    private String address;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 错误日志
     */
    private String exceptionDetail;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


    public SysLog(String logType, Long time) {
        this.logType = logType;
        this.time = time;
    }
}
