
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(群信息实体类)
 *
 * @version: V1.0
 * @author: sky
 *
 */
@Data
@ApiModel("TGroupBase")
@TableName(value = "group_base")
@AllArgsConstructor
@NoArgsConstructor
public class GroupBase implements Serializable {

	private static final long serialVersionUID = 1614325395249L;

	@TableId
    @ApiModelProperty(name = "gId", value = "群id")
	private String gId;

    @ApiModelProperty(name = "gStatus", value = "群状态： -1 已删除，0 封禁，1 正常。 删除功能由群主操作； 封禁为后台保留能力； 只有处于“正常”状态的群，才具体所有正常属性。")
	private Integer gStatus;

    @ApiModelProperty(name = "gName", value = "群名字： 首次生成时，直接生成默认的群名。 参考微信，限定30个字符。")
	private String gName;

    @ApiModelProperty(name = "gOwnerUserUid", value = "群主id")
	private Integer gOwnerUserUid;

    @ApiModelProperty(name = "gNotice", value = "群公告： 群公告只能由群主编辑和修改。 每次编辑群公告，保存时主动@所有人并发送最新公告。")
	private String gNotice;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gNoticeUpdatetime", value = "群公告更新时间")
	private Date gNoticeUpdatetime;

    @ApiModelProperty(name = "gNoticeUpdateuid", value = "群公告更新者id")
	private Integer gNoticeUpdateuid;

    @ApiModelProperty(name = "maxMemberCount", value = "群人数：默认200")
	private Integer maxMemberCount;

    @ApiModelProperty(name = "gMemberCount", value = "当前群员数")
	private Integer gMemberCount;

    @ApiModelProperty(name = "createUserUid", value = "群首次建立者id")
	private Integer createUserUid;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
	private Date createTime;

    @ApiModelProperty(name = "forbidUserUid", value = "用于记录封禁群的后台人员id。")
	private Integer forbidUserUid;

	@ApiModelProperty(name = "allow_private_chat", value = "是否允许群内成员私聊，0：允许  1：不允许")
	private Integer allowPrivateChat;


	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "forbidTime", value = "用于记录封禁群的时间。")
	private Date forbidTime;

    @ApiModelProperty(name = "firbidCause", value = "用于记录封禁群的理由。")
	private String firbidCause;

    @ApiModelProperty(name = "delUserUid", value = "删除用户")
	private Integer delUserUid;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "delTime", value = "删除时间")
	private Date delTime;

    @ApiModelProperty(name = "avatarIncludeCnt", value = "头像人数拼接")
	private Integer avatarIncludeCnt;



	@ApiModelProperty(name = "gTopContent", value = "头像人数拼接")
	private String gTopContent;

	@ApiModelProperty(name = "groupCornet", value = "群短号id")
	private String groupCornet;


	@ApiModelProperty(name = "Gmute", value = "群短号id")
	private Integer GMute;


	@ApiModelProperty(name = "invite", value = "群邀请")
	private Integer invite;


	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "expirationTime", value = "群过期时间")
	private Date expirationTime;

	public Integer administratorRedEnvelope;

}
