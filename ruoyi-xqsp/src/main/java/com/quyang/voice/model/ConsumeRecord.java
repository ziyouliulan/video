
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(消费记录实体类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("TConsumeRecord")
@TableName(value = "consume_record")
@AllArgsConstructor
@NoArgsConstructor
public class ConsumeRecord implements Serializable {

	private static final long serialVersionUID = 1613629986456L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
	private Long id;

    @ApiModelProperty(name = "tradeNo", value = "交易单号")
	private String tradeNo;

    @ApiModelProperty(name = "tradeNo", value = "商户订单号")
	private String ncountOrderId;

    @ApiModelProperty(name = "bankCardId", value = "银行卡id")
	private Integer bankCardId;

    @ApiModelProperty(name = "userId", value = "用户ID")
	private Long userId;

    @ApiModelProperty(name = "toUserId", value = " 对方用户Id： 接受转账时 为 转账人的ID， 发送转账时 为 接受放的 ID")
	private Long toUserId;

    @ApiModelProperty(name = "money", value = "金额")
	private Double money;

    @ApiModelProperty(name = "type", value = "类型： 1:用户充值, 2:用户提现, 3:后台充值, 4:发红包, 5:领取红包, 6:红包退款  7:转账   8:接受转账   9:转账退回 ")
	private Integer type;

    @ApiModelProperty(name = "descs", value = "消费备注")
	private String descs;

    @ApiModelProperty(name = "payType", value = " 1：支付宝支付 , 2：微信支付, 3：余额支付, 4:系统支付")
	private Integer payType;

    @ApiModelProperty(name = "status", value = "0：创建  1：支付完成  2：交易完成  -1：交易关闭")
	private Integer status;

    @ApiModelProperty(name = "redPacketId", value = "红包id")
	private String redPacketId;

    @ApiModelProperty(name = "changeType", value = " 1 收入  2支出")
	private Integer changeType;

    @ApiModelProperty(name = "manualPayStatus", value = "超过一定金额需要后台审核：1.审核成功 ，-1.审核失败")
	private Integer manualPayStatus;

    @ApiModelProperty(name = "serviceCharge", value = "手续费")
	private Double serviceCharge;

    @ApiModelProperty(name = "currentBalance", value = "当前余额")
	private Double currentBalance;

    @ApiModelProperty(name = "operationAmount", value = "实际操作金额")
	private Double operationAmount;


	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "time", value = "时间")
	private Date time;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "timestamp", value = "时间戳")
	private Date timestamp;


	/**
	 * 商品购买优惠
	 */
	private Double shopServiceCharge;

	/**
	 * 积分获得率
	 */
	private Double integralServiceCharge;

	/**
	 * 推荐抽成
	 */

	private Double recommendServiceCharge;

	/**
	 * 平台抽成
	 */
	private Double systemServiceCharge;


	private Integer gid;

	private Boolean administratorRedEnvelope;



}
