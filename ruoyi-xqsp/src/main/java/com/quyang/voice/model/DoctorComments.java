package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_doctor_comments
 * @author
 */
@ApiModel(value="com.quyang.voice.model.DoctorComments动态评论表")
@Data
public class DoctorComments implements Serializable {

    @ApiModelProperty(hidden = true)
    private Long id;

    /**
     * 医生id
     */
    @ApiModelProperty(value="医生id")
    private Integer doctorId;

    /**
     * 订单id
     */
    @ApiModelProperty(value="订单id")
    private Integer orderId;

    /**
     * 评论用户id
     */
    @ApiModelProperty(value="评论用户id")
    private Long userId;

    /**
     * 评论用户头像
     */
    @ApiModelProperty(value="评论用户头像")
    private String userName;

    /**
     * 评论用户名
     */
    @ApiModelProperty(value="评论用户名")
    private String userAvatar;

    /**
     * 评论内容
     */
    @ApiModelProperty(value="评论内容")
    private String comment;

    /**
     * 评论图片url
     */
    @ApiModelProperty(value="评论图片url")
    private String commentPic;

    /**
     * 评论图片文件名
     */
    @ApiModelProperty(value="评论图片文件名")
    private String commentPicName;

    /**
     * 评论时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="评论时间")
    private Date createTime;

    /**
     * 好评
     */
    @ApiModelProperty(value="好评")
    private Integer praiseRate;

    private static final long serialVersionUID = 1L;
}
