
package com.quyang.voice.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description:TODO(世界动态Vo类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("PublishsVo")
@AllArgsConstructor
@NoArgsConstructor
public class PublishsVo implements Serializable {

	private static final long serialVersionUID = 1606794166973L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "id", hidden = true)
	private Long id;

    @ApiModelProperty(name = "content", value = "动态内容")
	private String content;

    @ApiModelProperty(name = "userId", value = "发布的用户ID")
	private Long userId;

	@ApiModelProperty(name = "nickname", value = "发布的用户名")
	private String nickname;

	@ApiModelProperty(name = "nickname", value = "发布的用户头像")
	private String avatarUrl;

    @ApiModelProperty(name = "type", value = "动态类型：1 文字+图片、2 文字+视频、3 纯文字")
	private Integer type;

	@ApiModelProperty(name = "isLike", value = "是否已经点赞")
	private Boolean isLike;

	@ApiModelProperty(name = "isFocus", value = "是否关注：0未关注   1已关注    2本人")
	private Integer isFocus;

	@ApiModelProperty(name = "commentNum", value = "评论的数量")
	private Integer commentNum;

	@ApiModelProperty(name = "likeNum", value = "点赞的数量")
	private Integer likeNum;

    @ApiModelProperty(name = "picUrl", value = "图片url，多张已逗号分隔")
	private String picUrl;

    @ApiModelProperty(name = "videoUrl", value = "视频url")
	private String videoUrl;

    @ApiModelProperty(name = "videoCoverUrl", value = "视频封面url")
	private String videoCoverUrl;

	@ApiModelProperty(name = "picUrlList", value = "图片url集合")
	private List<String> picUrlList;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "发布时间")
	private Date createTime;

	@ApiModelProperty(name = "goodsId", value = "商品id")
	private Integer goodsId;

	@ApiModelProperty(name = "gallery", value = "商品轮播图")
	private String[] gallery;

	@ApiModelProperty(name = "productName", value = "商品名称")
	private String productName;



	@ApiModelProperty(name = "counterPrice", value = "原价")
	private BigDecimal counterPrice;


	@ApiModelProperty(name = "retailPrice", value = "优惠价")
	private BigDecimal retailPrice;


	@ApiModelProperty(name = "vipPrice", value = "会员价")
	private BigDecimal vipPrice;

	@ApiModelProperty(name = "shopId", value = "店铺id")
	private Integer shopId;




	@ApiModelProperty(name = "regionCode", value = "地区编码")
	private String regionCode;

	@ApiModelProperty(name = "regionName", value = "地区名称")
	private String regionName;





}
