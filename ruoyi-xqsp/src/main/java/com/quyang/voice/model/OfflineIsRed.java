package com.quyang.voice.model;

import lombok.Data;

import java.io.Serializable;

/**
 * offline_is_red
 * @author
 */
@Data
public class OfflineIsRed implements Serializable {
    /**
     * 消息id
     */
    private Integer offlineId;

    /**
     * 用户id
     */
    private Integer userUid;

    /**
     * 群id
     */
    private String groupId;

    /**
     * 安卓消息读取状态：1已读 0 未读
     */
    private Integer isRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer iosIsRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer webIsRead;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer isReadSync;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer iosIsReadSync;

    /**
     * 消息读取状态：1已读 0 未读
     */
    private Integer webIsReadSync;

    private static final long serialVersionUID = 1L;
}
