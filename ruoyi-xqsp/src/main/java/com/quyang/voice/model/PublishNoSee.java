
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(不让用户看朋友圈表实体类)
 *
 * @version: V1.0
 * @author: liuhaotian
 *
 */
@Data
@ApiModel("TPublishNoSee")
@TableName(value = "publish_no_see")
@AllArgsConstructor
@NoArgsConstructor
public class PublishNoSee implements Serializable {

	private static final long serialVersionUID = 1612164655429L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键id", hidden = true)
	private Long id;

    @ApiModelProperty(name = "userId", value = "用户id")
	private Integer userId;

    @ApiModelProperty(name = "toUserId", value = "被屏蔽用户id")
	private Integer toUserId;


}
