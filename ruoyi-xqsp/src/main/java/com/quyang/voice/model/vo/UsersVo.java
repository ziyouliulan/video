package com.quyang.voice.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description:TODO(用户Vo类)
 *
 * @version: V1.0
 * @author: H-T-C
 *
 */
@Data
@ApiModel("UsersVo")
@AllArgsConstructor
@NoArgsConstructor
public class UsersVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID",name = "用户id")
    private Long id;

    @ApiModelProperty(name = "nickname",value = "昵称")
    private String nickName;

    @ApiModelProperty(name = "sysId",value = "账号")
    private String sysId;

    @ApiModelProperty(name = "headUrl",value = "头像")
    private String headUrl;

    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "性别：0 女，1 男")
    private Integer sex;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(value = "生日")
    private Date birthday;

    @ApiModelProperty(value = "是否会员")
    private Boolean isVIP;

    @ApiModelProperty(value = "是否本人")
    private Boolean isMe;

    @ApiModelProperty(value = "是否关注")
    private Boolean isFollow;

//    @ApiModelProperty(value = "是否实名认证")
//    private Boolean isVerify;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "职业")
    private String occupation;

    @ApiModelProperty(value = "身高")
    private Integer height;

    @ApiModelProperty(value = "体重")
    private Integer weight;


    @ApiModelProperty(value = "个人介绍")
    private String introduce;

    @ApiModelProperty(value = "与我距离")
    private String distance;

    @ApiModelProperty(value = "邀请码")
    private String inviteCode;

    @ApiModelProperty(value = "是否被投票")
    private Boolean isVoted;

    @ApiModelProperty(value = "是否被拉黑")
    private Boolean isBlack;

    @ApiModelProperty(value = "我的作品")
    private List<PublishsListVo> publishs;
}
