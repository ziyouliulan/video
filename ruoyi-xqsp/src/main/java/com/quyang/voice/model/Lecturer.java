package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @TableName yuecool_lecturer
 */
@TableName(value ="yuecool_lecturer")
@Data
public class Lecturer implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 医师名称
     */
    private String name;

    /**
     * 所属分类
     */
    private String hospital;

    /**
     * 0:男 1：女
     */
    private String sex;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 电话
     */
    private String phone;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 简介
     */
    private String userDesc;

    /**
     * 职位id
     */
    private String positionId;

    /**
     * 擅长
     */
    private String beGoodAt;

    /**
     * 科室id
     */
    private String deptId;

    /**
     * 科室id
     */
    @TableField(exist = false )
    private String deptName;


    /**
     * 医院等级
     */
    private String hospitalLevel;

    /**
     * 接诊人数
     */
    private Integer numberOfPatients;

    /**
     * 好评率
     */
    private Double favorableRate;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty(hidden = true)
    private Date createTime;

    /**
     * 是否推荐讲师
     */
    private String isRecommend;

    /**
     * 地区id
     */
    private String areaId;

    /**
     * 地区id 市
     */
    private String cityId;

    /**
     * 医生照片
     */
    private String doctorPhotos;

    /**
     * 资格证书
     */
    private String qualificationCertificate;

    /**
     * 身份证正面
     */
    private String frontOfIdCard;

    /**
     * 身份证反面
     */
    private String reverseSideOfIdCard;

    /**
     * 预约管理 0关闭 1：开启
     */
    private String appointmentManagement;

    /**
     * 问诊管理 0关闭 1：开启
     */
    private String inquiryManagement;

    /**
     * 医生状态1： 认证中  2认证失败 3 认证成功
     */
    private String status;

    /**
     * 是否分校1:是 0否
     */
    private Integer branchSchool;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Lecturer other = (Lecturer) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getHospital() == null ? other.getHospital() == null : this.getHospital().equals(other.getHospital()))
            && (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
            && (this.getBirthday() == null ? other.getBirthday() == null : this.getBirthday().equals(other.getBirthday()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getUserDesc() == null ? other.getUserDesc() == null : this.getUserDesc().equals(other.getUserDesc()))
            && (this.getPositionId() == null ? other.getPositionId() == null : this.getPositionId().equals(other.getPositionId()))
            && (this.getBeGoodAt() == null ? other.getBeGoodAt() == null : this.getBeGoodAt().equals(other.getBeGoodAt()))
            && (this.getDeptId() == null ? other.getDeptId() == null : this.getDeptId().equals(other.getDeptId()))

            && (this.getHospitalLevel() == null ? other.getHospitalLevel() == null : this.getHospitalLevel().equals(other.getHospitalLevel()))
            && (this.getNumberOfPatients() == null ? other.getNumberOfPatients() == null : this.getNumberOfPatients().equals(other.getNumberOfPatients()))
            && (this.getFavorableRate() == null ? other.getFavorableRate() == null : this.getFavorableRate().equals(other.getFavorableRate()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getIsRecommend() == null ? other.getIsRecommend() == null : this.getIsRecommend().equals(other.getIsRecommend()))
            && (this.getAreaId() == null ? other.getAreaId() == null : this.getAreaId().equals(other.getAreaId()))
            && (this.getCityId() == null ? other.getCityId() == null : this.getCityId().equals(other.getCityId()))
            && (this.getDoctorPhotos() == null ? other.getDoctorPhotos() == null : this.getDoctorPhotos().equals(other.getDoctorPhotos()))
            && (this.getQualificationCertificate() == null ? other.getQualificationCertificate() == null : this.getQualificationCertificate().equals(other.getQualificationCertificate()))
            && (this.getFrontOfIdCard() == null ? other.getFrontOfIdCard() == null : this.getFrontOfIdCard().equals(other.getFrontOfIdCard()))
            && (this.getReverseSideOfIdCard() == null ? other.getReverseSideOfIdCard() == null : this.getReverseSideOfIdCard().equals(other.getReverseSideOfIdCard()))
            && (this.getAppointmentManagement() == null ? other.getAppointmentManagement() == null : this.getAppointmentManagement().equals(other.getAppointmentManagement()))
            && (this.getInquiryManagement() == null ? other.getInquiryManagement() == null : this.getInquiryManagement().equals(other.getInquiryManagement()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getHospital() == null) ? 0 : getHospital().hashCode());
        result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
        result = prime * result + ((getBirthday() == null) ? 0 : getBirthday().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getUserDesc() == null) ? 0 : getUserDesc().hashCode());
        result = prime * result + ((getPositionId() == null) ? 0 : getPositionId().hashCode());
        result = prime * result + ((getBeGoodAt() == null) ? 0 : getBeGoodAt().hashCode());
        result = prime * result + ((getDeptId() == null) ? 0 : getDeptId().hashCode());
        result = prime * result + ((getHospitalLevel() == null) ? 0 : getHospitalLevel().hashCode());
        result = prime * result + ((getNumberOfPatients() == null) ? 0 : getNumberOfPatients().hashCode());
        result = prime * result + ((getFavorableRate() == null) ? 0 : getFavorableRate().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getIsRecommend() == null) ? 0 : getIsRecommend().hashCode());
        result = prime * result + ((getAreaId() == null) ? 0 : getAreaId().hashCode());
        result = prime * result + ((getCityId() == null) ? 0 : getCityId().hashCode());
        result = prime * result + ((getDoctorPhotos() == null) ? 0 : getDoctorPhotos().hashCode());
        result = prime * result + ((getQualificationCertificate() == null) ? 0 : getQualificationCertificate().hashCode());
        result = prime * result + ((getFrontOfIdCard() == null) ? 0 : getFrontOfIdCard().hashCode());
        result = prime * result + ((getReverseSideOfIdCard() == null) ? 0 : getReverseSideOfIdCard().hashCode());
        result = prime * result + ((getAppointmentManagement() == null) ? 0 : getAppointmentManagement().hashCode());
        result = prime * result + ((getInquiryManagement() == null) ? 0 : getInquiryManagement().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", hospital=").append(hospital);
        sb.append(", sex=").append(sex);
        sb.append(", birthday=").append(birthday);
        sb.append(", phone=").append(phone);
        sb.append(", userId=").append(userId);
        sb.append(", userDesc=").append(userDesc);
        sb.append(", positionId=").append(positionId);
        sb.append(", beGoodAt=").append(beGoodAt);
        sb.append(", deptId=").append(deptId);
        sb.append(", hospitalLevel=").append(hospitalLevel);
        sb.append(", numberOfPatients=").append(numberOfPatients);
        sb.append(", favorableRate=").append(favorableRate);
        sb.append(", createTime=").append(createTime);
        sb.append(", isRecommend=").append(isRecommend);
        sb.append(", areaId=").append(areaId);
        sb.append(", cityId=").append(cityId);
        sb.append(", doctorPhotos=").append(doctorPhotos);
        sb.append(", qualificationCertificate=").append(qualificationCertificate);
        sb.append(", frontOfIdCard=").append(frontOfIdCard);
        sb.append(", reverseSideOfIdCard=").append(reverseSideOfIdCard);
        sb.append(", appointmentManagement=").append(appointmentManagement);
        sb.append(", inquiryManagement=").append(inquiryManagement);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
