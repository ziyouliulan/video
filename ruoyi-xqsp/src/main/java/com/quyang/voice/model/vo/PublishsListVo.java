
package com.quyang.voice.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(世界动态Vo类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("PublishsListVo")
@AllArgsConstructor
@NoArgsConstructor
public class PublishsListVo implements Serializable {

	private static final long serialVersionUID = 1606794166973L;

    @ApiModelProperty(name = "id", value = "动态ID")
	private Long id;

    @ApiModelProperty(name = "content", value = "动态内容")
	private String content;

    @ApiModelProperty(name = "type", value = "动态类型：1 文字+图片、2 文字+视频")
	private Integer type;

	@ApiModelProperty(name = "browseNum", value = "浏览的数量")
	private Integer browseNum;

    @ApiModelProperty(name = "picUrl", value = "图片url，多张已逗号分隔")
	private String picUrl;

    @ApiModelProperty(name = "videoUrl", value = "视频url")
	private String videoUrl;

    @ApiModelProperty(name = "videoCoverUrl", value = "视频封面url")
	private String videoCoverUrl;

    @ApiModelProperty(name = "region", value = "地区编码")
	private String region;

    @ApiModelProperty(name = "region", value = "状态")
	private Integer noSeeStatus;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "发布时间")
	private Date createTime;

}
