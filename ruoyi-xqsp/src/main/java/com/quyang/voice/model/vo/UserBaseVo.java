package com.quyang.voice.model.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("UserBaseVo")
@AllArgsConstructor
@NoArgsConstructor
public class UserBaseVo implements Serializable {

    private static final long serialVersionUID = 1599614913201L;

    @ApiModelProperty(name = "id", value = "用户id")
    private Long id;

    @ApiModelProperty(name = "sysid", value = "用户账号")
    private String sysid;

    @ApiModelProperty(name = "avatar", value = "用户头像")
    private String avatar;

    @ApiModelProperty(name = "name", value = "用户名称")
    private String name;

    @ApiModelProperty(name = "sign", value = "用户签名")
    private String sign;

//    @ApiModelProperty(name = "isAdmin", value = "是否为管理员（0：不是管理员  1：是管理员）")
//    private Integer isAdmin;

    @ApiModelProperty(name = "sex", value = "性别 0：女  1：男")
    private Integer sex;

    @ApiModelProperty(name = "status", value = "用户状态  0：封禁中  1：正常")
    private String status;
//
//    @ApiModelProperty(name = "isBothFollow", value = "是否互关")
//    private boolean isBothFollow;
//
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
//    @ApiModelProperty(name = "updateTime", value = "时间")
//    private Date updateTime;

}
