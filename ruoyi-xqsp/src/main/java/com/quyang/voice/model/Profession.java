
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:TODO(职业表实体类)
 *
 * @version: V1.0
 * @author: SKY
 *
 */
@Data
@ApiModel("TProfession")
@TableName(value = "t_profession")
@AllArgsConstructor
@NoArgsConstructor
public class Profession implements Serializable {

	private static final long serialVersionUID = 1606806899799L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Long id;

    @ApiModelProperty(name = "IndustryId", value = "行业id")
	private Long IndustryId;

    @ApiModelProperty(name = "name", value = "职业名")
	private String name;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "创建时间")
	private Date createTime;


}
