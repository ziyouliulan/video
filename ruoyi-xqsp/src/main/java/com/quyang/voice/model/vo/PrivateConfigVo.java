package com.quyang.voice.model.vo;


import com.quyang.voice.model.PrivacyConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Description:TODO(公会房间实体类)
 *
 * @version: V1.0
 * @author: liuhaotian
 *
 */
@Data
@ApiModel("PrivateConfigVo")
@AllArgsConstructor
@NoArgsConstructor
public class PrivateConfigVo implements Serializable {

    private static final long serialVersionUID = 1612161303955L;

    @ApiModelProperty(name = "privacyConfig", value = "隐私基本参数 ")
    private PrivacyConfig privacyConfig;

    @ApiModelProperty(name = "isNotSeeMeUser", value = "不让他看我动态的用户基本信息 ")
    private List<MissuUsersBaseVo> isNotSeeMeUser;

    @ApiModelProperty(name = "isNotSeeHe", value = "不看他动态用户基本信息")
    private List<MissuUsersBaseVo> isNotSeeHeUser;



}
