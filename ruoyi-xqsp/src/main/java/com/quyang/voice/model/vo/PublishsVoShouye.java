
package com.quyang.voice.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**   
 * @Description:TODO(世界动态Vo类)
 * 
 * @version: V1.0
 * @author: wlx
 * 
 */
@Data
@ApiModel("PublishsVoShouye")
@AllArgsConstructor
@NoArgsConstructor
public class PublishsVoShouye implements Serializable {

	private static final long serialVersionUID = 1606794166973L;
	
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "id", hidden = true)
	private Long id;
    
    @ApiModelProperty(name = "content", value = "动态内容")
	private String content;
    
    @ApiModelProperty(name = "userId", value = "发布的用户ID")
	private Long userId;

	@ApiModelProperty(name = "nickname", value = "发布的用户名")
	private String nickname;

	@ApiModelProperty(name = "nickname", value = "发布的用户头像")
	private String avatarUrl;

    @ApiModelProperty(name = "type", value = "动态类型：1 文字+图片、2 文字+视频")
	private Integer type;

	@ApiModelProperty(name = "browseNum", value = "浏览的数量")
	private Integer browseNum;

    @ApiModelProperty(name = "picUrl", value = "图片url，多张已逗号分隔")
	private String picUrl;
    
    @ApiModelProperty(name = "videoUrl", value = "视频url")
	private String videoUrl;
    
    @ApiModelProperty(name = "videoCoverUrl", value = "视频封面url")
	private String videoCoverUrl;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "createTime", value = "发布时间")
	private Date createTime;
    

}
