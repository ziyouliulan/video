package com.quyang.voice.model.vo;

import com.quyang.voice.model.UserLevel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("MissuUsersBaseVo")
@AllArgsConstructor
@NoArgsConstructor
public class MissuUsersBaseVo implements Serializable {

    @ApiModelProperty(name = "userUid", value = "id")
    private Integer userUid;

    @ApiModelProperty(name = "nickname", value = "昵称")
    private String nickname;

    @ApiModelProperty(name = "userAvatarFileName", value = "用户头像文件名")
    private String userAvatarFileName;

    @ApiModelProperty(name = "userType", value = "用户类型")
    private Integer userType;

    @ApiModelProperty(name = "userPhone", value = "用户手机")
    private String userPhone;

    @ApiModelProperty(name = "balance", value = "余额")
    private Double balance;

    @ApiModelProperty(name = "UserLevel", value = "等级")
    private UserLevel userLevel;



    @ApiModelProperty(name = "group_tab_on" , value = "1:开启 0：关闭")
    private String groupTabOn;


    @ApiModelProperty(name = "group_id" , value = "群id")
    private String groupId;



    @ApiModelProperty(name = "reCommunicationNumber", value = "邀请人的")
    private Integer reCommunicationNumber;


    @ApiModelProperty(name = "myCommunicationNumber", value = "我的邀请码")
    private Integer myCommunicationNumber;



    @ApiModelProperty(name = "userCornet", value = "短号")
    private Integer userCornet;




    @ApiModelProperty(name = "age", value = "年龄")
    private Integer age;


    @ApiModelProperty(name = "googleSecret",value = "谷歌验证码")
    private String googleSecret;
    private String latestLoginAddres;
    private String registerAddres;
    private String latestLoginIp;
    private String registerIp;
    private Integer time;
    private String region;

    private String province;

    private String city;
    private String carCode;
    private String realName;


    private String idCardNo;

    private Boolean realPersonAuthentication;

}
