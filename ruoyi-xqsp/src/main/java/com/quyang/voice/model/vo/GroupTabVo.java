package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description
 * @Author  Hunter
 * @Date 2021-05-07
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("GroupTabVo")
public class GroupTabVo  implements Serializable {

	private static final long serialVersionUID =  8985886717367639905L;

   	@ApiModelProperty(name = "id" , value = "id")
	private Long id;

   	@ApiModelProperty(name = "name" , value = "标题")
	private String name;

   	@ApiModelProperty(name = "address_url" , value = "链接地址")
	private String addressUrl;

   	@ApiModelProperty(name = "img_url" , value = "图片地址")
	private String imgUrl;

	@ApiModelProperty(name = "group_tab_on" , value = "1:开启 0：关闭")
	private String groupTabOn;


	@ApiModelProperty(name = "group_id" , value = "群id")
	private String groupId;


}
