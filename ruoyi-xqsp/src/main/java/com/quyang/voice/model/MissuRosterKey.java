package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * missu_roster
 * @author
 */
@ApiModel(value="com.quyang.voice.model.MissuRosterKey好友列表")
@Data
public class MissuRosterKey implements Serializable {
    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userUid;

    /**
     * 好友的id
     */
    @ApiModelProperty(value="好友的id")
    private Integer friendUserUid;

    private static final long serialVersionUID = 1L;
}
