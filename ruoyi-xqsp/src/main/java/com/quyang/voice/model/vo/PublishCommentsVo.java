
package com.quyang.voice.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.quyang.voice.model.PublishComments;
import com.quyang.voice.model.PublishCommentsReply;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description:TODO(动态评论实体类)
 *
 * @version: V1.0
 * @author: wulixiang
 *
 */
@Data
@ApiModel("TPublishCommentsVo")
@AllArgsConstructor
@NoArgsConstructor
public class PublishCommentsVo implements Serializable {

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(name = "id", value = "", hidden = true)
	private Long id;

	@ApiModelProperty(name = "publishId", value = "动态id")
	private Long publishId;

	@ApiModelProperty(name = "userId", value = "评论用户id")
	private Long userId;

	@ApiModelProperty(name = "userName", value = "评论用户名")
	private String userName;

	@ApiModelProperty(name = "nickname", value = "动态所属用户名")
	private String nickname;


	@ApiModelProperty(name = "avatarUrl", value = "动态所属用户头像")
	private String avatarUrl;

	@ApiModelProperty(name = "userAvatar", value = "评论用户头像")
	private String userAvatar;

	@ApiModelProperty(name = "comment", value = "评论内容")
	private String comment;

	@ApiModelProperty(name = "commentNum", value = "评论数量")
	private Integer commentNum;

	@ApiModelProperty(name = "commentPic", value = "评论图片url")
	private String commentPic;

	@ApiModelProperty(name = "isAuthor", value = "是否是作者：0不是 1是")
	private Integer isAuthor;

	@ApiModelProperty(name = "commentPicName", value = "评论图片文件名")
	private String commentPicName;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "createTime", value = "评论时间")
	private Date createTime;


	private static final long serialVersionUID = 1599710372539L;

	@ApiModelProperty(name = "publishComments", value = "动态评论")
	private PublishComments publishComments;

	@ApiModelProperty(name = "replays", value = "动态评论回复")
	private List<PublishCommentsReply> replays = new ArrayList<>();
	private List<PublishCommentsReplyVo> replyVos = new ArrayList<>();


}
