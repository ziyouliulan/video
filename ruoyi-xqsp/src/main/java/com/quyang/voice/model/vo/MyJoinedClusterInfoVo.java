package com.quyang.voice.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("MyJoinedClusterInfoVo")
@AllArgsConstructor
@NoArgsConstructor
public class MyJoinedClusterInfoVo implements Serializable {
    private static final long serialVersionUID = 1600833164692L;

    @ApiModelProperty(name = "id", value = "群或讨论组id")
    private Long id;

    /*@ApiModelProperty(name = "type", value = "群类型（1：公会群，2：个人群，3：讨论组）")
    private Integer type;*/

    @ApiModelProperty(name = "name", value = "昵称")
    private String name;

    @ApiModelProperty(name = "url", value = "头像")
    private String url;

    @ApiModelProperty(name = "isTop", value = "是否置顶（公会群无此属性）")
    private Boolean isTop;

    @ApiModelProperty(name = "isShield", value = "是否屏蔽（公会群无此属性）")
    private Boolean isShield;

    @ApiModelProperty(name = "isNoDisturbing", value = "是否免打扰（公会群无此属性）")
    private Boolean isNoDisturbing;


}
