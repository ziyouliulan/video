package com.quyang.voice.model;

import cn.hutool.core.util.StrUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DoctorExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    private String online;

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public DoctorExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("`name` is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("`name` is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("`name` =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("`name` <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("`name` >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("`name` >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("`name` <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("`name` <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("`name` like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("`name` not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("`name` in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("`name` not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("`name` between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("`name` not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andHospitalIsNull() {
            addCriterion("hospital is null");
            return (Criteria) this;
        }

        public Criteria andHospitalIsNotNull() {
            addCriterion("hospital is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalEqualTo(String value) {
            addCriterion("hospital =", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotEqualTo(String value) {
            addCriterion("hospital <>", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalGreaterThan(String value) {
            addCriterion("hospital >", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalGreaterThanOrEqualTo(String value) {
            addCriterion("hospital >=", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalLessThan(String value) {
            addCriterion("hospital <", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalLessThanOrEqualTo(String value) {
            addCriterion("hospital <=", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalLike(String value) {
            addCriterion("hospital like", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotLike(String value) {
            addCriterion("hospital not like", value, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalIn(List<String> values) {
            addCriterion("hospital in", values, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotIn(List<String> values) {
            addCriterion("hospital not in", values, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalBetween(String value1, String value2) {
            addCriterion("hospital between", value1, value2, "hospital");
            return (Criteria) this;
        }

        public Criteria andHospitalNotBetween(String value1, String value2) {
            addCriterion("hospital not between", value1, value2, "hospital");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("sex is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("sex is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("sex =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("sex <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("sex >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("sex >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("sex <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("sex <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("sex like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("sex not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("sex in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("sex not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("sex between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("sex not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNull() {
            addCriterion("birthday is null");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNotNull() {
            addCriterion("birthday is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdayEqualTo(String value) {
            addCriterion("birthday =", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotEqualTo(String value) {
            addCriterion("birthday <>", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThan(String value) {
            addCriterion("birthday >", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThanOrEqualTo(String value) {
            addCriterion("birthday >=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThan(String value) {
            addCriterion("birthday <", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThanOrEqualTo(String value) {
            addCriterion("birthday <=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLike(String value) {
            addCriterion("birthday like", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotLike(String value) {
            addCriterion("birthday not like", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayIn(List<String> values) {
            addCriterion("birthday in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotIn(List<String> values) {
            addCriterion("birthday not in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayBetween(String value1, String value2) {
            addCriterion("birthday between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotBetween(String value1, String value2) {
            addCriterion("birthday not between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserDescIsNull() {
            addCriterion("user_desc is null");
            return (Criteria) this;
        }

        public Criteria andUserDescIsNotNull() {
            addCriterion("user_desc is not null");
            return (Criteria) this;
        }

        public Criteria andUserDescEqualTo(String value) {
            addCriterion("user_desc =", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescNotEqualTo(String value) {
            addCriterion("user_desc <>", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescGreaterThan(String value) {
            addCriterion("user_desc >", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescGreaterThanOrEqualTo(String value) {
            addCriterion("user_desc >=", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescLessThan(String value) {
            addCriterion("user_desc <", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescLessThanOrEqualTo(String value) {
            addCriterion("user_desc <=", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescLike(String value) {
            addCriterion("user_desc like", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescNotLike(String value) {
            addCriterion("user_desc not like", value, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescIn(List<String> values) {
            addCriterion("user_desc in", values, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescNotIn(List<String> values) {
            addCriterion("user_desc not in", values, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescBetween(String value1, String value2) {
            addCriterion("user_desc between", value1, value2, "userDesc");
            return (Criteria) this;
        }

        public Criteria andUserDescNotBetween(String value1, String value2) {
            addCriterion("user_desc not between", value1, value2, "userDesc");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNull() {
            addCriterion("position_id is null");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNotNull() {
            addCriterion("position_id is not null");
            return (Criteria) this;
        }

        public Criteria andPositionIdEqualTo(String value) {
            addCriterion("position_id =", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotEqualTo(String value) {
            addCriterion("position_id <>", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThan(String value) {
            addCriterion("position_id >", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThanOrEqualTo(String value) {
            addCriterion("position_id >=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThan(String value) {
            addCriterion("position_id <", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThanOrEqualTo(String value) {
            addCriterion("position_id <=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLike(String value) {
            addCriterion("position_id like", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotLike(String value) {
            addCriterion("position_id not like", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIn(List<String> values) {
            addCriterion("position_id in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotIn(List<String> values) {
            addCriterion("position_id not in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdBetween(String value1, String value2) {
            addCriterion("position_id between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotBetween(String value1, String value2) {
            addCriterion("position_id not between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtIsNull() {
            addCriterion("be_good_at is null");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtIsNotNull() {
            addCriterion("be_good_at is not null");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtEqualTo(String value) {
            addCriterion("be_good_at =", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtNotEqualTo(String value) {
            addCriterion("be_good_at <>", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtGreaterThan(String value) {
            addCriterion("be_good_at >", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtGreaterThanOrEqualTo(String value) {
            addCriterion("be_good_at >=", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtLessThan(String value) {
            addCriterion("be_good_at <", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtLessThanOrEqualTo(String value) {
            addCriterion("be_good_at <=", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtLike(String value) {
            addCriterion("be_good_at like", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtNotLike(String value) {
            addCriterion("be_good_at not like", value, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtIn(List<String> values) {
            addCriterion("be_good_at in", values, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtNotIn(List<String> values) {
            addCriterion("be_good_at not in", values, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtBetween(String value1, String value2) {
            addCriterion("be_good_at between", value1, value2, "beGoodAt");
            return (Criteria) this;
        }

        public Criteria andBeGoodAtNotBetween(String value1, String value2) {
            addCriterion("be_good_at not between", value1, value2, "beGoodAt");
            return (Criteria) this;
        }



        public DoctorExample.Criteria andFIND_IN_SET_DEPT(String value) {
            addCriterion("FIND_IN_SET("+value+",yd.dept_id)");
            return (DoctorExample.Criteria) this;
        }

        public DoctorExample.Criteria andFIND_IN_SET_DEPTS(List<Dept>   depts) {
            String values  = "";
            for (Dept dept:depts
                 ) {
                if (StrUtil.isBlankIfStr(values)){
                    values = "FIND_IN_SET("+dept.getDeptId()+",yd.dept_id)";
                }else {
                    values += "  or  " +"FIND_IN_SET("+dept.getDeptId()+",yd.dept_id)";
                }
            }

            if (!StrUtil.isBlankIfStr(values)){
                addCriterion(values);
            }

            return (DoctorExample.Criteria) this;
        }



        public Criteria andDeptIdIsNull() {
            addCriterion("yd.dept_id is null");
            return (Criteria) this;
        }

        public Criteria andDeptIdIsNotNull() {
            addCriterion("yd.dept_id is not null");
            return (Criteria) this;
        }

        public Criteria andDeptIdEqualTo(String value) {
            addCriterion("yd.dept_id =", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotEqualTo(String value) {
            addCriterion("yd.dept_id <>", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdGreaterThan(String value) {
            addCriterion("yd.dept_id >", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdGreaterThanOrEqualTo(String value) {
            addCriterion("yd.dept_id >=", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLessThan(String value) {
            addCriterion("yd.dept_id <", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLessThanOrEqualTo(String value) {
            addCriterion("yd.dept_id <=", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLike(String value) {
            addCriterion("yd.dept_id like", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotLike(String value) {
            addCriterion("yd.dept_id not like", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdIn(List<String> values) {
            addCriterion("yd.dept_id in", values, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotIn(List<String> values) {
            addCriterion("yd.dept_id not in", values, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdBetween(String value1, String value2) {
            addCriterion("yd.dept_id between", value1, value2, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotBetween(String value1, String value2) {
            addCriterion("yd.dept_id not between", value1, value2, "deptId");
            return (Criteria) this;
        }

        public Criteria andImageTextIsNull() {
            addCriterion("image_text is null");
            return (Criteria) this;
        }

        public Criteria andImageTextIsNotNull() {
            addCriterion("image_text is not null");
            return (Criteria) this;
        }

        public Criteria andImageTextEqualTo(String value) {
            addCriterion("image_text =", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextNotEqualTo(String value) {
            addCriterion("image_text <>", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextGreaterThan(String value) {
            addCriterion("image_text >", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextGreaterThanOrEqualTo(String value) {
            addCriterion("image_text >=", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextLessThan(String value) {
            addCriterion("image_text <", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextLessThanOrEqualTo(String value) {
            addCriterion("image_text <=", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextLike(String value) {
            addCriterion("image_text like", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextNotLike(String value) {
            addCriterion("image_text not like", value, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextIn(List<String> values) {
            addCriterion("image_text in", values, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextNotIn(List<String> values) {
            addCriterion("image_text not in", values, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextBetween(String value1, String value2) {
            addCriterion("image_text between", value1, value2, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextNotBetween(String value1, String value2) {
            addCriterion("image_text not between", value1, value2, "imageText");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeIsNull() {
            addCriterion("image_text_time is null");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeIsNotNull() {
            addCriterion("image_text_time is not null");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeEqualTo(Integer value) {
            addCriterion("image_text_time =", value, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeNotEqualTo(Integer value) {
            addCriterion("image_text_time <>", value, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeGreaterThan(Integer value) {
            addCriterion("image_text_time >", value, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_text_time >=", value, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeLessThan(Integer value) {
            addCriterion("image_text_time <", value, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeLessThanOrEqualTo(Integer value) {
            addCriterion("image_text_time <=", value, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeIn(List<Integer> values) {
            addCriterion("image_text_time in", values, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeNotIn(List<Integer> values) {
            addCriterion("image_text_time not in", values, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeBetween(Integer value1, Integer value2) {
            addCriterion("image_text_time between", value1, value2, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andImageTextTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("image_text_time not between", value1, value2, "imageTextTime");
            return (Criteria) this;
        }

        public Criteria andAudioIsNull() {
            addCriterion("audio is null");
            return (Criteria) this;
        }

        public Criteria andAudioIsNotNull() {
            addCriterion("audio is not null");
            return (Criteria) this;
        }

        public Criteria andAudioEqualTo(String value) {
            addCriterion("audio =", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioNotEqualTo(String value) {
            addCriterion("audio <>", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioGreaterThan(String value) {
            addCriterion("audio >", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioGreaterThanOrEqualTo(String value) {
            addCriterion("audio >=", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioLessThan(String value) {
            addCriterion("audio <", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioLessThanOrEqualTo(String value) {
            addCriterion("audio <=", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioLike(String value) {
            addCriterion("audio like", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioNotLike(String value) {
            addCriterion("audio not like", value, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioIn(List<String> values) {
            addCriterion("audio in", values, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioNotIn(List<String> values) {
            addCriterion("audio not in", values, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioBetween(String value1, String value2) {
            addCriterion("audio between", value1, value2, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioNotBetween(String value1, String value2) {
            addCriterion("audio not between", value1, value2, "audio");
            return (Criteria) this;
        }

        public Criteria andAudioTimeIsNull() {
            addCriterion("audio_time is null");
            return (Criteria) this;
        }

        public Criteria andAudioTimeIsNotNull() {
            addCriterion("audio_time is not null");
            return (Criteria) this;
        }

        public Criteria andAudioTimeEqualTo(Integer value) {
            addCriterion("audio_time =", value, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeNotEqualTo(Integer value) {
            addCriterion("audio_time <>", value, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeGreaterThan(Integer value) {
            addCriterion("audio_time >", value, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("audio_time >=", value, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeLessThan(Integer value) {
            addCriterion("audio_time <", value, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeLessThanOrEqualTo(Integer value) {
            addCriterion("audio_time <=", value, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeIn(List<Integer> values) {
            addCriterion("audio_time in", values, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeNotIn(List<Integer> values) {
            addCriterion("audio_time not in", values, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeBetween(Integer value1, Integer value2) {
            addCriterion("audio_time between", value1, value2, "audioTime");
            return (Criteria) this;
        }

        public Criteria andAudioTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("audio_time not between", value1, value2, "audioTime");
            return (Criteria) this;
        }

        public Criteria andVideoIsNull() {
            addCriterion("video is null");
            return (Criteria) this;
        }

        public Criteria andVideoIsNotNull() {
            addCriterion("video is not null");
            return (Criteria) this;
        }

        public Criteria andVideoEqualTo(String value) {
            addCriterion("video =", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotEqualTo(String value) {
            addCriterion("video <>", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoGreaterThan(String value) {
            addCriterion("video >", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoGreaterThanOrEqualTo(String value) {
            addCriterion("video >=", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoLessThan(String value) {
            addCriterion("video <", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoLessThanOrEqualTo(String value) {
            addCriterion("video <=", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoLike(String value) {
            addCriterion("video like", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotLike(String value) {
            addCriterion("video not like", value, "video");
            return (Criteria) this;
        }

        public Criteria andVideoIn(List<String> values) {
            addCriterion("video in", values, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotIn(List<String> values) {
            addCriterion("video not in", values, "video");
            return (Criteria) this;
        }

        public Criteria andVideoBetween(String value1, String value2) {
            addCriterion("video between", value1, value2, "video");
            return (Criteria) this;
        }

        public Criteria andVideoNotBetween(String value1, String value2) {
            addCriterion("video not between", value1, value2, "video");
            return (Criteria) this;
        }

        public Criteria andVideoTimeIsNull() {
            addCriterion("video_time is null");
            return (Criteria) this;
        }

        public Criteria andVideoTimeIsNotNull() {
            addCriterion("video_time is not null");
            return (Criteria) this;
        }

        public Criteria andVideoTimeEqualTo(Integer value) {
            addCriterion("video_time =", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeNotEqualTo(Integer value) {
            addCriterion("video_time <>", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeGreaterThan(Integer value) {
            addCriterion("video_time >", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("video_time >=", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeLessThan(Integer value) {
            addCriterion("video_time <", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeLessThanOrEqualTo(Integer value) {
            addCriterion("video_time <=", value, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeIn(List<Integer> values) {
            addCriterion("video_time in", values, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeNotIn(List<Integer> values) {
            addCriterion("video_time not in", values, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeBetween(Integer value1, Integer value2) {
            addCriterion("video_time between", value1, value2, "videoTime");
            return (Criteria) this;
        }

        public Criteria andVideoTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("video_time not between", value1, value2, "videoTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeIsNull() {
            addCriterion("response_time is null");
            return (Criteria) this;
        }

        public Criteria andResponseTimeIsNotNull() {
            addCriterion("response_time is not null");
            return (Criteria) this;
        }

        public Criteria andResponseTimeEqualTo(Double value) {
            addCriterion("response_time =", value, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeNotEqualTo(Double value) {
            addCriterion("response_time <>", value, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeGreaterThan(Double value) {
            addCriterion("response_time >", value, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeGreaterThanOrEqualTo(Double value) {
            addCriterion("response_time >=", value, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeLessThan(Double value) {
            addCriterion("response_time <", value, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeLessThanOrEqualTo(Double value) {
            addCriterion("response_time <=", value, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeIn(List<Double> values) {
            addCriterion("response_time in", values, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeNotIn(List<Double> values) {
            addCriterion("response_time not in", values, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeBetween(Double value1, Double value2) {
            addCriterion("response_time between", value1, value2, "responseTime");
            return (Criteria) this;
        }

        public Criteria andResponseTimeNotBetween(Double value1, Double value2) {
            addCriterion("response_time not between", value1, value2, "responseTime");
            return (Criteria) this;
        }

        public Criteria andPrescriptionIsNull() {
            addCriterion("prescription is null");
            return (Criteria) this;
        }

        public Criteria andPrescriptionIsNotNull() {
            addCriterion("prescription is not null");
            return (Criteria) this;
        }

        public Criteria andPrescriptionEqualTo(Integer value) {
            addCriterion("prescription =", value, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionNotEqualTo(Integer value) {
            addCriterion("prescription <>", value, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionGreaterThan(Integer value) {
            addCriterion("prescription >", value, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionGreaterThanOrEqualTo(Integer value) {
            addCriterion("prescription >=", value, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionLessThan(Integer value) {
            addCriterion("prescription <", value, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionLessThanOrEqualTo(Integer value) {
            addCriterion("prescription <=", value, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionIn(List<Integer> values) {
            addCriterion("prescription in", values, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionNotIn(List<Integer> values) {
            addCriterion("prescription not in", values, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionBetween(Integer value1, Integer value2) {
            addCriterion("prescription between", value1, value2, "prescription");
            return (Criteria) this;
        }

        public Criteria andPrescriptionNotBetween(Integer value1, Integer value2) {
            addCriterion("prescription not between", value1, value2, "prescription");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelIsNull() {
            addCriterion("hospital_level is null");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelIsNotNull() {
            addCriterion("hospital_level is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelEqualTo(String value) {
            addCriterion("hospital_level =", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotEqualTo(String value) {
            addCriterion("hospital_level <>", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelGreaterThan(String value) {
            addCriterion("hospital_level >", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelGreaterThanOrEqualTo(String value) {
            addCriterion("hospital_level >=", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelLessThan(String value) {
            addCriterion("hospital_level <", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelLessThanOrEqualTo(String value) {
            addCriterion("hospital_level <=", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelLike(String value) {
            addCriterion("hospital_level like", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotLike(String value) {
            addCriterion("hospital_level not like", value, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelIn(List<String> values) {
            addCriterion("hospital_level in", values, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotIn(List<String> values) {
            addCriterion("hospital_level not in", values, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelBetween(String value1, String value2) {
            addCriterion("hospital_level between", value1, value2, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andHospitalLevelNotBetween(String value1, String value2) {
            addCriterion("hospital_level not between", value1, value2, "hospitalLevel");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsIsNull() {
            addCriterion("number_of_patients is null");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsIsNotNull() {
            addCriterion("number_of_patients is not null");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsEqualTo(Integer value) {
            addCriterion("number_of_patients =", value, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsNotEqualTo(Integer value) {
            addCriterion("number_of_patients <>", value, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsGreaterThan(Integer value) {
            addCriterion("number_of_patients >", value, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsGreaterThanOrEqualTo(Integer value) {
            addCriterion("number_of_patients >=", value, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsLessThan(Integer value) {
            addCriterion("number_of_patients <", value, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsLessThanOrEqualTo(Integer value) {
            addCriterion("number_of_patients <=", value, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsIn(List<Integer> values) {
            addCriterion("number_of_patients in", values, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsNotIn(List<Integer> values) {
            addCriterion("number_of_patients not in", values, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsBetween(Integer value1, Integer value2) {
            addCriterion("number_of_patients between", value1, value2, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andNumberOfPatientsNotBetween(Integer value1, Integer value2) {
            addCriterion("number_of_patients not between", value1, value2, "numberOfPatients");
            return (Criteria) this;
        }

        public Criteria andFavorableRateIsNull() {
            addCriterion("favorable_rate is null");
            return (Criteria) this;
        }

        public Criteria andFavorableRateIsNotNull() {
            addCriterion("favorable_rate is not null");
            return (Criteria) this;
        }

        public Criteria andFavorableRateEqualTo(Double value) {
            addCriterion("favorable_rate =", value, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateNotEqualTo(Double value) {
            addCriterion("favorable_rate <>", value, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateGreaterThan(Double value) {
            addCriterion("favorable_rate >", value, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateGreaterThanOrEqualTo(Double value) {
            addCriterion("favorable_rate >=", value, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateLessThan(Double value) {
            addCriterion("favorable_rate <", value, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateLessThanOrEqualTo(Double value) {
            addCriterion("favorable_rate <=", value, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateIn(List<Double> values) {
            addCriterion("favorable_rate in", values, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateNotIn(List<Double> values) {
            addCriterion("favorable_rate not in", values, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateBetween(Double value1, Double value2) {
            addCriterion("favorable_rate between", value1, value2, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andFavorableRateNotBetween(Double value1, Double value2) {
            addCriterion("favorable_rate not between", value1, value2, "favorableRate");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andIsRecommendIsNull() {
            addCriterion("is_recommend is null");
            return (Criteria) this;
        }

        public Criteria andIsRecommendIsNotNull() {
            addCriterion("is_recommend is not null");
            return (Criteria) this;
        }

        public Criteria andIsRecommendEqualTo(String value) {
            addCriterion("is_recommend =", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendNotEqualTo(String value) {
            addCriterion("is_recommend <>", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendGreaterThan(String value) {
            addCriterion("is_recommend >", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendGreaterThanOrEqualTo(String value) {
            addCriterion("is_recommend >=", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendLessThan(String value) {
            addCriterion("is_recommend <", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendLessThanOrEqualTo(String value) {
            addCriterion("is_recommend <=", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendLike(String value) {
            addCriterion("is_recommend like", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendNotLike(String value) {
            addCriterion("is_recommend not like", value, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendIn(List<String> values) {
            addCriterion("is_recommend in", values, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendNotIn(List<String> values) {
            addCriterion("is_recommend not in", values, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendBetween(String value1, String value2) {
            addCriterion("is_recommend between", value1, value2, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andIsRecommendNotBetween(String value1, String value2) {
            addCriterion("is_recommend not between", value1, value2, "isRecommend");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNull() {
            addCriterion("area_id is null");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNotNull() {
            addCriterion("area_id is not null");
            return (Criteria) this;
        }

        public Criteria andAreaIdEqualTo(String value) {
            addCriterion("area_id =", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotEqualTo(String value) {
            addCriterion("area_id <>", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThan(String value) {
            addCriterion("area_id >", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("area_id >=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThan(String value) {
            addCriterion("area_id <", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThanOrEqualTo(String value) {
            addCriterion("area_id <=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLike(String value) {
            addCriterion("area_id like", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotLike(String value) {
            addCriterion("area_id not like", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdIn(List<String> values) {
            addCriterion("area_id in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotIn(List<String> values) {
            addCriterion("area_id not in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdBetween(String value1, String value2) {
            addCriterion("area_id between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotBetween(String value1, String value2) {
            addCriterion("area_id not between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("city_id is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("city_id is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("city_id =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("city_id <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("city_id >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("city_id >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("city_id <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("city_id <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("city_id like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("city_id not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("city_id in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("city_id not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("city_id between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("city_id not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosIsNull() {
            addCriterion("doctor_photos is null");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosIsNotNull() {
            addCriterion("doctor_photos is not null");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosEqualTo(String value) {
            addCriterion("doctor_photos =", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosNotEqualTo(String value) {
            addCriterion("doctor_photos <>", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosGreaterThan(String value) {
            addCriterion("doctor_photos >", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosGreaterThanOrEqualTo(String value) {
            addCriterion("doctor_photos >=", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosLessThan(String value) {
            addCriterion("doctor_photos <", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosLessThanOrEqualTo(String value) {
            addCriterion("doctor_photos <=", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosLike(String value) {
            addCriterion("doctor_photos like", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosNotLike(String value) {
            addCriterion("doctor_photos not like", value, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosIn(List<String> values) {
            addCriterion("doctor_photos in", values, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosNotIn(List<String> values) {
            addCriterion("doctor_photos not in", values, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosBetween(String value1, String value2) {
            addCriterion("doctor_photos between", value1, value2, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andDoctorPhotosNotBetween(String value1, String value2) {
            addCriterion("doctor_photos not between", value1, value2, "doctorPhotos");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateIsNull() {
            addCriterion("qualification_certificate is null");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateIsNotNull() {
            addCriterion("qualification_certificate is not null");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateEqualTo(String value) {
            addCriterion("qualification_certificate =", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateNotEqualTo(String value) {
            addCriterion("qualification_certificate <>", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateGreaterThan(String value) {
            addCriterion("qualification_certificate >", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("qualification_certificate >=", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateLessThan(String value) {
            addCriterion("qualification_certificate <", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateLessThanOrEqualTo(String value) {
            addCriterion("qualification_certificate <=", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateLike(String value) {
            addCriterion("qualification_certificate like", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateNotLike(String value) {
            addCriterion("qualification_certificate not like", value, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateIn(List<String> values) {
            addCriterion("qualification_certificate in", values, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateNotIn(List<String> values) {
            addCriterion("qualification_certificate not in", values, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateBetween(String value1, String value2) {
            addCriterion("qualification_certificate between", value1, value2, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andQualificationCertificateNotBetween(String value1, String value2) {
            addCriterion("qualification_certificate not between", value1, value2, "qualificationCertificate");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardIsNull() {
            addCriterion("front_of_id_card is null");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardIsNotNull() {
            addCriterion("front_of_id_card is not null");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardEqualTo(String value) {
            addCriterion("front_of_id_card =", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardNotEqualTo(String value) {
            addCriterion("front_of_id_card <>", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardGreaterThan(String value) {
            addCriterion("front_of_id_card >", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardGreaterThanOrEqualTo(String value) {
            addCriterion("front_of_id_card >=", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardLessThan(String value) {
            addCriterion("front_of_id_card <", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardLessThanOrEqualTo(String value) {
            addCriterion("front_of_id_card <=", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardLike(String value) {
            addCriterion("front_of_id_card like", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardNotLike(String value) {
            addCriterion("front_of_id_card not like", value, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardIn(List<String> values) {
            addCriterion("front_of_id_card in", values, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardNotIn(List<String> values) {
            addCriterion("front_of_id_card not in", values, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardBetween(String value1, String value2) {
            addCriterion("front_of_id_card between", value1, value2, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andFrontOfIdCardNotBetween(String value1, String value2) {
            addCriterion("front_of_id_card not between", value1, value2, "frontOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardIsNull() {
            addCriterion("reverse_side_of_id_card is null");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardIsNotNull() {
            addCriterion("reverse_side_of_id_card is not null");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardEqualTo(String value) {
            addCriterion("reverse_side_of_id_card =", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardNotEqualTo(String value) {
            addCriterion("reverse_side_of_id_card <>", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardGreaterThan(String value) {
            addCriterion("reverse_side_of_id_card >", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardGreaterThanOrEqualTo(String value) {
            addCriterion("reverse_side_of_id_card >=", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardLessThan(String value) {
            addCriterion("reverse_side_of_id_card <", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardLessThanOrEqualTo(String value) {
            addCriterion("reverse_side_of_id_card <=", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardLike(String value) {
            addCriterion("reverse_side_of_id_card like", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardNotLike(String value) {
            addCriterion("reverse_side_of_id_card not like", value, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardIn(List<String> values) {
            addCriterion("reverse_side_of_id_card in", values, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardNotIn(List<String> values) {
            addCriterion("reverse_side_of_id_card not in", values, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardBetween(String value1, String value2) {
            addCriterion("reverse_side_of_id_card between", value1, value2, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andReverseSideOfIdCardNotBetween(String value1, String value2) {
            addCriterion("reverse_side_of_id_card not between", value1, value2, "reverseSideOfIdCard");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementIsNull() {
            addCriterion("appointment_management is null");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementIsNotNull() {
            addCriterion("appointment_management is not null");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementEqualTo(String value) {
            addCriterion("appointment_management =", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementNotEqualTo(String value) {
            addCriterion("appointment_management <>", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementGreaterThan(String value) {
            addCriterion("appointment_management >", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementGreaterThanOrEqualTo(String value) {
            addCriterion("appointment_management >=", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementLessThan(String value) {
            addCriterion("appointment_management <", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementLessThanOrEqualTo(String value) {
            addCriterion("appointment_management <=", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementLike(String value) {
            addCriterion("appointment_management like", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementNotLike(String value) {
            addCriterion("appointment_management not like", value, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementIn(List<String> values) {
            addCriterion("appointment_management in", values, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementNotIn(List<String> values) {
            addCriterion("appointment_management not in", values, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementBetween(String value1, String value2) {
            addCriterion("appointment_management between", value1, value2, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andAppointmentManagementNotBetween(String value1, String value2) {
            addCriterion("appointment_management not between", value1, value2, "appointmentManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementIsNull() {
            addCriterion("inquiry_management is null");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementIsNotNull() {
            addCriterion("inquiry_management is not null");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementEqualTo(String value) {
            addCriterion("inquiry_management =", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementNotEqualTo(String value) {
            addCriterion("inquiry_management <>", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementGreaterThan(String value) {
            addCriterion("inquiry_management >", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementGreaterThanOrEqualTo(String value) {
            addCriterion("inquiry_management >=", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementLessThan(String value) {
            addCriterion("inquiry_management <", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementLessThanOrEqualTo(String value) {
            addCriterion("inquiry_management <=", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementLike(String value) {
            addCriterion("inquiry_management like", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementNotLike(String value) {
            addCriterion("inquiry_management not like", value, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementIn(List<String> values) {
            addCriterion("inquiry_management in", values, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementNotIn(List<String> values) {
            addCriterion("inquiry_management not in", values, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementBetween(String value1, String value2) {
            addCriterion("inquiry_management between", value1, value2, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andInquiryManagementNotBetween(String value1, String value2) {
            addCriterion("inquiry_management not between", value1, value2, "inquiryManagement");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("`status` is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("`status` is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("`status` =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("`status` <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("`status` >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("`status` >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("`status` <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("`status` <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("`status` like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("`status` not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("`status` in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("`status` not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("`status` between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("`status` not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}