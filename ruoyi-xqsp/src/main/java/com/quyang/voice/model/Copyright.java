package com.quyang.voice.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Copyright implements Serializable {


    private String version;

    private String tranCode;

    private String merId;

    private String merOrderId;

    private String submitTime;

    private String msgCiphertext;

    private String signType;

    private String signValue;

    private String merAttach;

    private String charset;

}
