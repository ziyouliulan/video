package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.quyang.voice.model.dto.CourseDetailsDto;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @TableName yuecool_course_details
 */
@TableName(value ="yuecool_course_details")
@Data
public class CourseDetails extends CourseDetailsDto implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 简介内容
     */
    private String content;


    @TableField(exist = false)
    private String positionName;

    /**
     * 作者
     */
    private String author;

    /**
     * 现价
     */
    private BigDecimal presentPrice;

    /**
     * 优惠价
     */
    private BigDecimal concessionalPrice;

    /**
     * 视频介绍
     */
    private String videoIntroduction;

    /**
     * 是否展示
     */
    private String showOrNot;

    /**
     * 是否删除
     */
    private Integer deleted;

    /**
     * 是否推荐
     */
    private Integer isRecommend;

    /**
     * 封面
     */
    private String cover;

    /**
     * 类别
     */
    private Integer categoryId;

    /**
     * 是否收费
     */
    private Integer chargeOrNot;

    /**
     * 用户id
     */
    private Integer userId;

    @TableField(exist = false)
    private Integer courseHour;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CourseDetails other = (CourseDetails) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getAuthor() == null ? other.getAuthor() == null : this.getAuthor().equals(other.getAuthor()))
            && (this.getPresentPrice() == null ? other.getPresentPrice() == null : this.getPresentPrice().equals(other.getPresentPrice()))
            && (this.getConcessionalPrice() == null ? other.getConcessionalPrice() == null : this.getConcessionalPrice().equals(other.getConcessionalPrice()))
            && (this.getVideoIntroduction() == null ? other.getVideoIntroduction() == null : this.getVideoIntroduction().equals(other.getVideoIntroduction()))
            && (this.getShowOrNot() == null ? other.getShowOrNot() == null : this.getShowOrNot().equals(other.getShowOrNot()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getIsRecommend() == null ? other.getIsRecommend() == null : this.getIsRecommend().equals(other.getIsRecommend()))
            && (this.getCover() == null ? other.getCover() == null : this.getCover().equals(other.getCover()))
            && (this.getCategoryId() == null ? other.getCategoryId() == null : this.getCategoryId().equals(other.getCategoryId()))
            && (this.getChargeOrNot() == null ? other.getChargeOrNot() == null : this.getChargeOrNot().equals(other.getChargeOrNot()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getAuthor() == null) ? 0 : getAuthor().hashCode());
        result = prime * result + ((getPresentPrice() == null) ? 0 : getPresentPrice().hashCode());
        result = prime * result + ((getConcessionalPrice() == null) ? 0 : getConcessionalPrice().hashCode());
        result = prime * result + ((getVideoIntroduction() == null) ? 0 : getVideoIntroduction().hashCode());
        result = prime * result + ((getShowOrNot() == null) ? 0 : getShowOrNot().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getIsRecommend() == null) ? 0 : getIsRecommend().hashCode());
        result = prime * result + ((getCover() == null) ? 0 : getCover().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        result = prime * result + ((getChargeOrNot() == null) ? 0 : getChargeOrNot().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", content=").append(content);
        sb.append(", author=").append(author);
        sb.append(", presentPrice=").append(presentPrice);
        sb.append(", concessionalPrice=").append(concessionalPrice);
        sb.append(", videoIntroduction=").append(videoIntroduction);
        sb.append(", showOrNot=").append(showOrNot);
        sb.append(", deleted=").append(deleted);
        sb.append(", isRecommend=").append(isRecommend);
        sb.append(", cover=").append(cover);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", chargeOrNot=").append(chargeOrNot);
        sb.append(", userId=").append(userId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
