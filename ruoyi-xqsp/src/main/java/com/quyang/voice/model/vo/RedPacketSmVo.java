package com.quyang.voice.model.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("RedPacketDetailVo")
@AllArgsConstructor
@NoArgsConstructor
public class RedPacketSmVo  implements Serializable {
    private static final long serialVersionUID = 1606794166955L;

    @ApiModelProperty(name = "userId", value = "发红包的用户id")
    private  Integer userId;

    @ApiModelProperty(name = "orderId", value = "红包的唯一标识")
    private String orderId;

    @ApiModelProperty(name = "greetings", value = "祝福语")
    private String greetings;

    @ApiModelProperty(name = "type", value = "1：普通红包  2：拼手气红包  3:口令红包")
    private Integer type;

    @ApiModelProperty(name = "userIds", value = "领取该红包的 userId 字符串")
    private String userIds;

    @ApiModelProperty(name = "word", value = "红包口令")
    private String word;


    @ApiModelProperty(name = "coverUrl", value = "红包封面")
    private String coverUrl;
}
