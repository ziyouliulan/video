package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户钱包表
 * @TableName cust_accountbase
 */
@TableName(value ="cust_accountbase")
@Data
public class CustAccountbase implements Serializable {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 余额
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double amount = 0D;

    /**
     * 冻结金额
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double frozen = 0D;

    /**
     * 推荐金额
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double recommendAmount = 0D;

    /**
     * 支付密码
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private String payKey;

    /**
     * 积分
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double integral = 0D;

    /**
     * 冻结积分
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double integralFrozen = 0D;

    /**
     * 商家余额
     */
    @JsonSerialize(using = CustomerDoubleSerialize.class)
    private Double shopAmount = 0D;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
