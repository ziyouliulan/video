
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description:TODO(用户账户密码实体类)
 *
 * @version: V1.0
 * @author: wlx
 *
 */
@Data
@ApiModel("TCustAccountpwd")
@TableName(value = "cust_accountpwd")
@AllArgsConstructor
@NoArgsConstructor
public class CustAccountpwd implements Serializable {

	private static final long serialVersionUID = 1613703184579L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", hidden = true)
	private Long id;

    @ApiModelProperty(name = "userId", value = "用户id ")
	private Integer userId;

    @ApiModelProperty(name = "pwdType", value = "密码类型 0：支付密码")
	private Integer pwdType;

    @ApiModelProperty(name = "accountpwd", value = "账户密码")
	private String accountpwd;


}
