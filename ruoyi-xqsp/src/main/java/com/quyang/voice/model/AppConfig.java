
package com.quyang.voice.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalTime;

/**
 * @Description:TODO(配置实体类)
 *
 * @version: V1.0
 * @author: sky
 *
 */
@Data
@ApiModel("TAppConfig")
@TableName(value = "app_config")
@AllArgsConstructor
@NoArgsConstructor
public class AppConfig implements Serializable {

	private static final long serialVersionUID = 1615179995585L;

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "", hidden = true)
	private Long id;

    @ApiModelProperty(name = "registerType", value = "0：用户名注册   1：手机号注册")
	private Integer registerType;

    @ApiModelProperty(name = "maxFriend", value = "最大好友数量")
	private Integer maxFriend;

    @ApiModelProperty(name = "verificationCode", value = "是否开启注册验证码：1开启  2不开启")
	private Integer verificationCode;

    @ApiModelProperty(name = "addFriends", value = "是否添加好友：1是 2否")
	private Integer addFriends;

    @ApiModelProperty(name = "createGroup", value = "是否创建群聊：1是 2否")
	private Integer createGroup;

    @ApiModelProperty(name = "locationServices", value = "定位服务：1是 2否")
	private Integer locationServices;

    @ApiModelProperty(name = "voiceCall", value = "语音通话：1是 2否")
	private Integer voiceCall;

    @ApiModelProperty(name = "videoCall", value = "视频通话：1是 2否")
	private Integer videoCall;

    @ApiModelProperty(name = "momentsFunction", value = "是否开启朋友圈：1是  2否")
	private Integer momentsFunction;

    @ApiModelProperty(name = "webUrl", value = "官网地址")
	private String webUrl;

    @ApiModelProperty(name = "privacyPolicyUrl", value = "隐私政策url")
	private String privacyPolicyUrl;

    @ApiModelProperty(name = "androidNewUrl", value = "安卓最新版本")
	private String androidNewUrl;

    @ApiModelProperty(name = "androidDownloadUrl", value = "安卓版本下载地址")
	private String androidDownloadUrl;

    @ApiModelProperty(name = "appNotice", value = "app公告")
	private String appNotice;

    @ApiModelProperty(name = "iosDownloadUrl", value = "ios下载地址")
	private String iosDownloadUrl;

    @ApiModelProperty(name = "iosNewUrl", value = "ios最新版本")
	private String iosNewUrl;

    @ApiModelProperty(name = "iosUpdateUrl", value = "ios跟新说明")
	private String iosUpdateUrl;

    @ApiModelProperty(name = "androidUpdateUrl", value = "安卓跟新说明")
	private String androidUpdateUrl;


    private String fileUrl;

    private String imfileUrl;


    /** 用户短号：1开启 2关闭 */
    @ApiModelProperty(name = "userCornet", value = "用户短号：1开启 2关闭")
    private Integer userCornet;

    /** 邀请码：1开启 2关闭 */
    @ApiModelProperty(name = "invitationCode", value = "邀请码：1开启 2关闭")
    private Integer invitationCode;

    /** 短号搜索：1开启2关闭 */
    @ApiModelProperty(name = "searchCornet", value = "短号搜索：1开启2关闭")
    private Long searchCornet;

    /** 手机号搜索1开启2 关闭 */
    @ApiModelProperty(name = "searchPhone", value = "手机号搜索1开启2 关闭")
    private Long searchPhone;

    /** 账号搜搜 1：开启2关闭 */
    @ApiModelProperty(name = "searchName", value = "账号搜搜 1：开启2关闭")
    private Long searchName;


    /**红包功能1：开启2关闭 */
    @ApiModelProperty(name = "isOpenRedpacket", value = "红包功能1：开启2关闭")
    private Long isOpenRedpacket;


    /** 钱包功能1：开启2关闭*/
    @ApiModelProperty(name = "isOpenWallet", value = "钱包功能1：开启2关闭")
    private Long isOpenWallet;

    /** 钱包功能1：开启2关闭*/
    @ApiModelProperty(name = "msgInterval", value = "发送消息间隔")
    private int msgInterval;

    /** 钱包功能1：开启2关闭*/
    @ApiModelProperty(name = "groupNickname", value = "群昵称1：开启2关闭")
    private Long groupNickname;

    /** 钱包功能1：开启2关闭*/
    @ApiModelProperty(name = "groupVideo", value = "群视频：1开启2关闭")
    private Long groupVideo;


    /** 菜单1：开启2关闭*/
    @ApiModelProperty(name = "isMenu", value = "菜单：1开启2关闭")
    private Long isMenu;


    /** 日历：开启2关闭*/
    @ApiModelProperty(name = "isCalendar", value = "日历：1开启2关闭")
    private Long isCalendar;


    /** 群踢人拉人 是否通知普通群员：1开启 2关闭：1开启 2关闭*/
    @ApiModelProperty(name = "groupNotice", value = "群踢人拉人 是否通知普通群员：1开启 2关闭：1开启 2关闭")
    private Long groupNotice;



    /** 位置功能：1开启 2关闭*/
    @ApiModelProperty(name = "position", value = "位置功能：1开启 2关闭")
    private Long position;



    /** 位置功能：1开启 2关闭*/
    @ApiModelProperty(name = "isDelGroupMsg", value = "退出群是否删除消息：1开启 2关闭")
    private Long isDelGroupMsg;



    /** 位置功能：1开启 2关闭*/
    @ApiModelProperty(name = "recharge", value = "充值：1开启 2关闭")
    private Long recharge;

    /** 位置功能：1开启 2关闭*/
    @ApiModelProperty(name = "serviceCharge", value = "手续费")
    private Double serviceCharge;


    @ApiModelProperty(name = "maxTransferAmount", value = "单笔最大转账金额")
    private Double maxTransferAmount;
    @ApiModelProperty(name = "maxTransferAmountCount", value = "单日最大转账总额")
    private Double maxTransferAmountCount;
    @ApiModelProperty(name = "dayMaxRechargeAmount", value = "单笔最大充值金额")
    private Double dayMaxRechargeAmount;
    @ApiModelProperty(name = "dayMaxRechargeAmountCount", value = "单日最大充值总额")
    private Double dayMaxRechargeAmountCount;
    @ApiModelProperty(name = "singleMaximumWithdrawalAmount", value = "单笔最大提现金额")
    private Double singleMaximumWithdrawalAmount;

    @ApiModelProperty(name = "singleMaximumWithdrawalAmount", value = "单笔最小提现金额")
    private Double singleMinimumWithdrawalAmount;

    @ApiModelProperty(name = "dayMaxWithdrawAmount", value = "单日最大提现总额")
    private Double dayMaxWithdrawAmount;
    @ApiModelProperty(name = "myChangeWithdrawRate", value = "零钱提现费率")
    private Double myChangeWithdrawRate;
    @ApiModelProperty(name = "withdrawCode", value = "用户提现方式")
    private Double withdrawCode;
    @ApiModelProperty(name = "dayMaxWithdrawAmountCount", value = "单日提现最大次数")
    private Double dayMaxWithdrawAmountCount;
    @ApiModelProperty(name = "maximumAmountOfASingleRedEnvelope", value = "单笔红包最大金额")
    private Double maximumAmountOfASingleRedEnvelope;
    @ApiModelProperty(name = "maximumAmountOfRedEnvelopesInASingleDay", value = "单日红包最大金额")
    private Double maximumAmountOfRedEnvelopesInASingleDay;

    @ApiModelProperty(name = "wxpayImg", value = "微信收款码")
    private String wxpayImg;

    @ApiModelProperty(name = "alipayImg", value = "支付宝收款码")
    private String alipayImg;

    @ApiModelProperty(name = "isWxLogin", value = "是否开启微信登录")
    private String isWxLogin;

    @ApiModelProperty(name = "isUserPhone", value = "是否开启通讯录")
    private String isUserPhone;


    @ApiModelProperty(name = "appId", value = "appId")
    private String appId;


    @ApiModelProperty(name = "appSecret", value = "appSecret")
    private String appSecret;


    /** 邀请开关：1开启 2关闭*/
    @ApiModelProperty(name = "invite", value = "invite：1开启 2关闭")
    private Long invite;




    /** 消息漫游：1开启 2关闭*/
    @ApiModelProperty(name = "roamingMsg", value = "消息漫游：1开启 2关闭")
    private Long roamingMsg;

    private Integer isReadDel;


    /**
     * 分成比例
     */
    private Double distributionBonus;

    /**
     * 商品购买优惠
     */
    private Double shopServiceCharge;

    /**
     * 积分获得率
     */
    private Double integralServiceCharge;

    /**
     * 推荐抽成
     */
    private Double recommendServiceCharge;

    /**
     * 平台抽成
     */
    private Double systemServiceCharge;

    /**
     * 平台抽成
     */
    private Double specificShare;


    private String customerServiceNumber;

    private String customerServiceIdentification;
    private String customerServiceHotline;

    /**
     * 禁止充值和提现开始
     */
    private LocalTime prohibitRechargeWithdrawalStart;



    /**
     * 禁止充值和提现结束
     */
    private LocalTime prohibitRechargeWithdrawalEnd;

    private String customProduction;
    private String helpFarmers;

    public static void main(String[] args) {

        LocalTime of1 = LocalTime.of(06, 00);
        LocalTime of2 = LocalTime.of(21, 00);

        LocalTime of = LocalTime.of(20, 00);
        LocalTime now = LocalTime.now();
        System.out.println(of.compareTo(of1) != -1);
        System.out.println(of.compareTo(of2) != 1);
        int i = LocalTime.now().compareTo(of1);
        if (of.compareTo(of1) != -1  && of.compareTo(of2) != 1){
            System.out.println("测试");
        }
//        System.out.println(i);
    }
}
