package com.quyang.voice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * yuecool_patient_information
 * @author
 */
@ApiModel(value="com.quyang.voice.model.PatientInformation问诊记录")
@Data
public class PatientInformation implements Serializable {
    private Integer id;

    /**
     * 医生id
     */
    @ApiModelProperty(value="医生id")
    private Integer doctorId;


    //医生
    private Doctor doctor ;

    //患者
    private Patient patient;

    /**
     * 1:上午 2：中午 3：晚上
     */
    @ApiModelProperty(value="1:上午 2：中午 3：晚上")
    private String time;

    /**
     * 用户id
     */
    @ApiModelProperty(value="用户id")
    private Integer userId;

    /**
     * 患者id
     */
    @ApiModelProperty(value="患者id")
    private Integer patientId;

    /**
     * 问诊类型 1：图文  2音频  3视频
     */
    @ApiModelProperty(value="问诊类型 1：图文  2音频  3视频")
    private Integer typeOfConsultation;

    /**
     * 患者名称
     */
    @ApiModelProperty(value="患者名称")
    private String name;

    /**
     * 身份证
     */
    @ApiModelProperty(value="身份证")
    private String idCard;

    /**
     * 电话
     */
    @ApiModelProperty(value="电话")
    private String phone;

    /**
     * 病情描述
     */
    @ApiModelProperty(value="病情描述")
    private String desc;

    /**
     * 病例图片
     */
    @ApiModelProperty(value="病例图片")
    private String imageUrl;

    /**
     * 状态1:预约中  2预约确认  3取消预约 4：已完成
     */
    @ApiModelProperty(value="状态1:预约中  2预约确认  3取消预约 4：已完成")
    private Integer status;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="创建时间")
    private Date createTime;

    /**
     * 订单号
     */
    @ApiModelProperty(value="订单号")
    private String order;


    /**
     * 订单号
     */
    @ApiModelProperty(value="是否评论" ,hidden = true)
    private Boolean isComment;


    /**
     * 预约时间
     */
    @ApiModelProperty(value="预约时间")
    private String appointment;

    /**
     * 聊天开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="聊天开始日期")
    private Date startTime;

    /**
     * 聊天结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="聊天结束日期")
    private Date endTime;

    /**
     * 金额
     */
    @ApiModelProperty(value="金额")
    private Double money;

    /**
     * 总时间
     */
    @ApiModelProperty(value="总时间")
    private Long totalTime;

    /**
     * 类型1：已付费 2：未付费
     */
    @ApiModelProperty(value="类型1：已付费 2：未付费")
    private Integer type;

    /**
     * 预约时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value="预约时间")
    private Date appointmentTime;

    private static final long serialVersionUID = 1L;
}
