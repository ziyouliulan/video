package com.quyang.voice.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * tool_local_storage_music
 * @author
 */
@ApiModel(value="com.quyang.voice.model.ToolLocalStorageMusic本地存储")
@Data
public class ToolLocalStorageMusic implements Serializable {
    /**
     * ID
     */
    @ApiModelProperty(value="ID")
    private Long storageId;

    /**
     * 文件真实的名称
     */
    @ApiModelProperty(value="文件真实的名称")
    private String realName;

    /**
     * 文件名
     */
    @ApiModelProperty(value="文件名")
    private String name;

    /**
     * 后缀
     */
    @ApiModelProperty(value="后缀")
    private String suffix;

    /**
     * 路径
     */
    @ApiModelProperty(value="路径")
    private String path;

    /**
     * 网络路径
     */
    @ApiModelProperty(value="网络路径")
    private String imgUrl;

    /**
     * 类型
     */
    @ApiModelProperty(value="类型")
    private String type;

    /**
     * 大小
     */
    @ApiModelProperty(value="大小")
    private String size;

    /**
     * 创建者
     */
    @ApiModelProperty(value="创建者")
    private String createBy;

    /**
     * 更新者
     */
    @ApiModelProperty(value="更新者")
    private String updateBy;

    /**
     * 创建日期
     */
    @ApiModelProperty(value="创建日期")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}
