package com.quyang.voice.token.mobile;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 验证码登录 验证逻辑类
 *
 * @author wlx
 * @since 1.0
 */
@Data
public class SmsCodeAuthenticationProvider implements AuthenticationProvider {

    Logger logger = LoggerFactory.getLogger(getClass());

    UserDetailsService userDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        SmsCodeAuthenticationToken authenticatioanToken = (SmsCodeAuthenticationToken) authentication;
        UserDetails userDetails = userDetailsService.loadUserByUsername((String) authentication.getPrincipal());
        SmsCodeAuthenticationToken authenticatioanRusult = new SmsCodeAuthenticationToken(userDetails,
                userDetails.getAuthorities());
        authenticatioanRusult.setDetails(authenticatioanToken.getDetails());
        return authenticatioanRusult;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return SmsCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
