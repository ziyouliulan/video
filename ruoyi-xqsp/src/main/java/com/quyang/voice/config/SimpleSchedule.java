package com.quyang.voice.config;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.quyang.voice.service.ClientConfigService;
import com.ruoyi.xqsp.domain.ScrollNotifications;
import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.service.ISpUsersService;
import com.ruoyi.xqsp.service.ScrollNotificationsService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@Component
public class SimpleSchedule {


    @Autowired
    ScrollNotificationsService scrollNotificationsService;

    @Autowired
    private ISpUsersService spUsersService;

    @Autowired
    ClientConfigService configService;

    //0 0 10,14,16 * * ? 每天上午10点，下午2点，4点
    //0 0/30 9-17 * * ? 朝九晚五工作时间内每半小时
    //0 0 12 ? * WED 表示每个星期三中午12点
    //0 0 12 * * ? 每天中午12点触发
    //0 15 10 ? * * 每天上午10:15触发
    //0 15 10 * * ? 每天上午10:15触发
    //0 15 10 * * ? * 每天上午10:15触发
    //0 15 10 * * ? 2005 2005年的每天上午10:15触发
    //0 * 14 * * ? 在每天下午2点到下午2:59期间的每1分钟触发
    //0 0/5 14 * * ? 在每天下午2点到下午2:55期间的每5分钟触发
    //0 0/5 14,18 * * ? 在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发
    //0 0-5 14 * * ? 在每天下午2点到下午2:05期间的每1分钟触发
    //0 10,44 14 ? 3 WED 每年三月的星期三的下午2:10和2:44触发
    //0 15 10 ? * MON-FRI 周一至周五的上午10:15触发
    //0 15 10 15 * ? 每月15日上午10:15触发
    //0 15 10 L * ? 每月最后一日的上午10:15触发
    //0 15 10 ? * 6L 每月的最后一个星期五上午10:15触发
    //0 15 10 ? * 6L 2002-2005 2002年至2005年的每月的最后一个星期五上午10:15触发
    //0 15 10 ? * 6#3 每月的第三个星期五上午10:15触发
//    private Integer time = 0;
//    @Scheduled(cron = "*/6 * * * * ?")   //定时器定义，设置执行时间
//    private void process() {
//        System.out.println("定时器1执行"+time++);
//    }

//  	@Scheduled(fixedDelay = 2 * 60 * 60 *1000)   //定时器定义，设置执行时间 1s


//  	@Scheduled(fixedDelay =  20 * 60 *1000)   //定时器定义，设置执行时间 1s
    @Scheduled(cron = " 0 30 * * * ?  ")
    private void process1() {
        ScrollNotifications scrollNotifications = new ScrollNotifications();
        String s = RandomStringUtils.randomAlphabetic(8);
        Random random = new Random();
        int i = random.nextInt(4);
        switch (i){

            case 1:
                scrollNotifications.setNoticeContent("恭喜"+s.toUpperCase()+"开通月卡会员");
                break;
            case 2:
                scrollNotifications.setNoticeContent("恭喜"+s.toUpperCase()+"开通季卡会员");
                break;
            case 3:
                scrollNotifications.setNoticeContent("恭喜"+s.toUpperCase()+"开通年卡会员");
                break;
        }


        scrollNotificationsService.save(scrollNotifications);
//        System.out.println("定时器2执行"+time++);
    }


    @Scheduled(cron = " 0 0 0 * * ?  ")
//    @Scheduled(cron = "*/6 * * * * ?")
    public void deleteDayAll() {

        HashMap map = configService.configToMap();
        UpdateWrapper<SpUsers> usersUpdateWrapper = new UpdateWrapper<>();
        usersUpdateWrapper.eq("user_state","0");
        usersUpdateWrapper.and(e -> e.ne("user_today_short",Long.valueOf(map.get("userWelfareShort").toString()))
                .or().ne("user_today_long",Long.valueOf(map.get("userWelfareLong").toString()))
        );
        usersUpdateWrapper.set("user_welfare_long", Long.valueOf(map.get("userWelfareLong").toString()));
        usersUpdateWrapper.set("user_welfare_short", Long.valueOf(map.get("userWelfareShort").toString()));
        usersUpdateWrapper.set("user_today_short", Long.valueOf(map.get("userWelfareShort").toString()));
        usersUpdateWrapper.set("user_today_long", Long.valueOf(map.get("userWelfareLong").toString()));

        spUsersService.update(usersUpdateWrapper);
    }

    public static void main(String[] args) {


    }

}

