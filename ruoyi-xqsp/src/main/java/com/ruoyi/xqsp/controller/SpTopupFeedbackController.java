package com.ruoyi.xqsp.controller;

import java.util.Date;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpTopupFeedback;
import com.ruoyi.xqsp.service.ISpTopupFeedbackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 充值反馈Controller
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
@RestController
@RequestMapping("/xqsp/topupfeedback")
public class SpTopupFeedbackController extends BaseController
{
    @Autowired
    private ISpTopupFeedbackService spTopupFeedbackService;

    /**
     * 查询充值反馈列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:topupfeedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpTopupFeedback spTopupFeedback)
    {
        startPage();
        List<SpTopupFeedback> list = spTopupFeedbackService.selectSpTopupFeedbackList(spTopupFeedback);
        return getDataTable(list);
    }

    /**
     * 导出充值反馈列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:topupfeedback:export')")
    @Log(title = "充值反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpTopupFeedback spTopupFeedback)
    {
        List<SpTopupFeedback> list = spTopupFeedbackService.selectSpTopupFeedbackList(spTopupFeedback);
        ExcelUtil<SpTopupFeedback> util = new ExcelUtil<SpTopupFeedback>(SpTopupFeedback.class);
        return util.exportExcel(list, "topupfeedback");
    }

    /**
     * 获取充值反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:topupfeedback:query')")
    @GetMapping(value = "/{topupFeedbackId}")
    public AjaxResult getInfo(@PathVariable("topupFeedbackId") Long topupFeedbackId)
    {
        return AjaxResult.success(spTopupFeedbackService.selectSpTopupFeedbackById(topupFeedbackId));
    }

    /**
     * 新增充值反馈
     */

    @Log(title = "充值反馈", businessType = BusinessType.INSERT)
    @PostMapping("/addTopupFeedback")
    public AjaxResult addTopupFeedback(@RequestBody SpTopupFeedback spTopupFeedback)
    {
        spTopupFeedback.setTopupFeedbackTime(new Date());
        return toAjax(spTopupFeedbackService.insertSpTopupFeedback(spTopupFeedback));
    }

    /**
     * 修改充值反馈
     */
    @PreAuthorize("@ss.hasPermi('xqsp:topupfeedback:edit')")
    @Log(title = "充值反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpTopupFeedback spTopupFeedback)
    {
        return toAjax(spTopupFeedbackService.updateSpTopupFeedback(spTopupFeedback));
    }

    /**
     * 删除充值反馈
     */
    @PreAuthorize("@ss.hasPermi('xqsp:topupfeedback:remove')")
    @Log(title = "充值反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{topupFeedbackIds}")
    public AjaxResult remove(@PathVariable Long[] topupFeedbackIds)
    {
        return toAjax(spTopupFeedbackService.deleteSpTopupFeedbackByIds(topupFeedbackIds));
    }

    /**
     * 根据用户ID查询
     * @return
     */

    @GetMapping("/getAllTopup/{userId}")
    public TableDataInfo getAllTopup(@PathVariable("userId") Long userId){
        startPage();
        List<SpTopupFeedback>list=spTopupFeedbackService.selectSpTopupFeedbackByUserId(userId);
        return getDataTable(list);
    }
}
