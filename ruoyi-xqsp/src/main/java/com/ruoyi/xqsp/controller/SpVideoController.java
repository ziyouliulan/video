package com.ruoyi.xqsp.controller;

import java.math.BigDecimal;
import java.util.*;

import com.alipay.api.domain.Video;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quyang.voice.dao.DictDataDao;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.LpStringUtil;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.*;
import com.ruoyi.xqsp.domain.entity.*;
import com.ruoyi.xqsp.service.*;
import com.ruoyi.xqsp.utils.TokenServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.list.TreeList;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频管理Controller
 *
 * @author ruoyi
 * @date 2021-04-23
 */
@Api(value = "视频管理", tags = "视频管理")
@RestController
@RequestMapping("/xqsp/video")
public class SpVideoController extends BaseController {
    @Autowired
    private ISpVideoService spVideoService;

    @Autowired
    private ISpActorService spActorService;
    @Autowired
    private ISpVideoCategoryService spVideoCategoryService;
    @Autowired
    private ISpLikeService spLikeService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpUnlikeService spUnlikeService;
    @Autowired
    private ISpCollectService spCollectService;
    @Autowired
    private ISpActorMessageService spActorMessageService;
    @Autowired
    private ISpVarietyService spVarietyService;
    @Autowired
    private ISpVideoEvaluationService spVideoEvaluationService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpExpenseCalendarService spExpenseCalendarService;
    @Autowired
    private ISpVideoWatchHistoryService spVideoWatchHistoryService;
    @Autowired
    private ISpVideoCacheService spVideoCacheService;
    @Autowired
    private ISpAdvertisingService spAdvertisingService;

    @Autowired
    private SpCategoryTagService categoryTagService;


    @Autowired
    SpConsumeRecordService consumeRecordService;



    @Autowired
    VideoDiscountService videoDiscountService;


    @Autowired
    ISpComicService comicService;

    @Autowired
    SpVideoHeadlinesService videoHeadlinesService;

    @Autowired
    SpHotSearchService hotSearchService;

    @Autowired
    TemplateConfigsService templateConfigsService;

    @Autowired
    VideoTagConfigsService videoTagConfigsService;

    @Autowired
    DictDataDao dictDataDao;


    @Autowired
    TokenServices tokenServices;

    /**
     * 查询视频管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVideo spVideo) {
        startPage();
        spVideo.setVideoType(1);
        List<SpVideo> list = spVideoService.selectSpVideoList(spVideo);


        List<ActorVideo> actorVideoList = new ArrayList<>();
        TableDataInfo dataTable = getDataTable(list);
        ActorVideo actorVideo;
        for (SpVideo video : list) {
            actorVideo = new ActorVideo();
            actorVideo.setSpVideo(video);
            if (video.getActorId() == null) {
                actorVideo.setActorName("该视频无演员");
            } else {
                if (spActorService.selectSpActorById(video.getActorId())!=null){
                    actorVideo.setActorName(spActorService.selectSpActorById(video.getActorId()).getActorName());
                }

            }
            if (video.getVideoCategoryId() == null) {
                actorVideo.setVideoCategoryName("该视频暂未分类");
            } else {
                if (spVideoCategoryService.selectSpVideoCategoryById(video.getVideoCategoryId())!=null){
                    actorVideo.setVideoCategoryName(spVideoCategoryService.selectSpVideoCategoryById(video.getVideoCategoryId()).getVideoCategoryName());
                }

            }

            actorVideoList.add(actorVideo);
        }
        dataTable.setRows(actorVideoList);
        return dataTable;
    }


    /**
     * 根据分类id查询该分类下的视频
     */
    @ApiOperation("根据分类id查询该分类下的视频")
    @GetMapping("/getVideoByCategoryId/{videoCategoryId}")
    public ResponseUtil getVideoByCategoryId(@PathVariable("videoCategoryId") Long videoCategoryId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<SpVideo> list = spVideoService.selectSpVideoByCategoryId(videoCategoryId);
        PageInfo<SpVideo> pageInfo = new PageInfo<>(list);
        return ResponseUtil.success(pageInfo);
    }


    /**
     * 导出视频管理列表  测试
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:export')")
    @Log(title = "视频管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVideo spVideo) {
        List<SpVideo> list = spVideoService.selectSpVideoList(spVideo);
        ExcelUtil<SpVideo> util = new ExcelUtil<SpVideo>(SpVideo.class);
        return util.exportExcel(list, "video");
    }

    /**
     * 获取视频管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:query')")
    @GetMapping(value = "/{videoId}")
    public AjaxResult getInfo(@PathVariable("videoId") Long videoId) {
        return AjaxResult.success(spVideoService.selectSpVideoById(videoId));
    }


    /**
     * 根据演员id获取视频管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:query')")
    @GetMapping(value = "/actorId/{actorId}")
    public TableDataInfo getVideoByActorId(@PathVariable("actorId") Long actorId) {
        startPage();
        List<SpVideo> list = spVideoService.selectSpVideoByActorId(actorId);
        return getDataTable(list);
    }

//    /**
//     * 根据演员id获取视频管理详细信息
//     */
//
//    @GetMapping(value = "/videoList/{actorId}")
//    public TableDataInfo getVideoListByActorId(@PathVariable("actorId") Long actorId)
//    {
//        startPage();
//        List<SpVideo>list=spVideoService.selectSpVideoByActorId(actorId);
//        return getDataTable(list);
//    }

    /**
     * 新增视频管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:add')")
    @Log(title = "视频管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVideo spVideo) {

        spVideo.setVideoTime(new Date());
        spVideo.setVideoType(1);
        return toAjax(spVideoService.insertSpVideo(spVideo));
    }

    /**
     * 修改视频管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:edit')")
    @Log(title = "视频管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVideo spVideo) {
        return toAjax(spVideoService.updateSpVideo(spVideo));
    }

    /**
     * 删除视频管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:remove')")
    @Log(title = "视频管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{videoIds}")
    public AjaxResult remove(@PathVariable Long[] videoIds) {
        Long type=0l;
        Long type1=1l;
        for (Long videoId : videoIds){
            spCollectService.deleteSpCollect1(videoId,type);
            spVideoWatchHistoryService.deleteSpVideoWatchHistory(videoId,type1);
            spVideoCacheService.deleteSpVideoCacheBy(videoId,type1);
            spVideoEvaluationService.deleteSpVideoEvaluationByVideoId(videoId);
        }
        return toAjax(spVideoService.deleteSpVideoByIds(videoIds));
    }

    /**
     * 点赞
     */
    @Log(title = "视频点赞", businessType = BusinessType.OTHER)
    @ApiOperation("视频点赞")
    @PostMapping("/videoLike")
    public ResponseUtil videoLike( UserLike userLike) {
        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVideoId());
        spLike.setLikeType(0L);
        SpUnlike spUnlike = new SpUnlike();
        spUnlike.setUserId(userLike.getUserId());
        spUnlike.setUnlikeTypeId(userLike.getVideoId());
        spUnlike.setUnlikeType(0L);

        if (spLikeService.selectSpLike(spLike) == null) {
            if (spUnlikeService.selectSpUnlikeList(spUnlike).size() == 0) {
                spVideoService.updateSpVideoLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spLikeService.insertSpLike(spLike));
            } else {
                if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoUnlikeNumber() > 0) {
                    spVideoService.updateSpVideoUnLikeNumber1(userLike.getVideoId());
                }
                spUnlikeService.deleteSpUnlike(spUnlike);
                spVideoService.updateSpVideoLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spLikeService.insertSpLike(spLike));
            }

        } else {
            if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoLikeNumber() > 0) {
                spVideoService.updateSpVideoLikeNumber1(userLike.getVideoId());
            }

            return ResponseUtil.update(spLikeService.deleteSpLike(spLike));
        }


    }

    /**
     * 差评
     */
    @ApiOperation("视频差评")
    @PostMapping("/videoUnLike")
    public ResponseUtil videoUnLike( UserLike userLike) {
        SpUnlike spUnlike = new SpUnlike();
        spUnlike.setUserId(userLike.getUserId());
        spUnlike.setUnlikeTypeId(userLike.getVideoId());
        spUnlike.setUnlikeType(0L);

        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVideoId());
        spLike.setLikeType(0L);

        if (spUnlikeService.selectSpUnlikeList(spUnlike).size() == 0) {
            if (spLikeService.selectSpLike(spLike) == null) {
                spVideoService.updateSpVideoUnLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spUnlikeService.insertSpUnlike(spUnlike));
            } else {
                if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoLikeNumber() > 0) {
                    spVideoService.updateSpVideoLikeNumber1(userLike.getVideoId());
                }
                spLikeService.deleteSpLike(spLike);
                spVideoService.updateSpVideoUnLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spUnlikeService.insertSpUnlike(spUnlike));
            }

        } else {
            if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoUnlikeNumber() > 0) {
                spVideoService.updateSpVideoUnLikeNumber1(userLike.getVideoId());
            }

            return ResponseUtil.update(spUnlikeService.deleteSpUnlike(spUnlike));
        }


    }

    /**
     * 视频收藏
     *
     * @return
     */
    @Log(title = "视频收藏", businessType = BusinessType.OTHER)
    @ApiOperation("视频收藏")
    @PostMapping("/videoCollect")
    public ResponseUtil videoCollect( UserLike userLike) {
        SpCollect spCollect = new SpCollect();
        spCollect.setUserId(userLike.getUserId());
        spCollect.setCollectTypeId(userLike.getVideoId());
        spCollect.setCollectType(1l);
        spCollect.setCollectTime(new Date());
        if (spCollectService.selectSpCollectList(spCollect).size() == 0) {
            spVideoService.updateSpVideoCollectNumber(userLike.getVideoId());
            return ResponseUtil.update(spCollectService.insertSpCollect(spCollect));
        } else {
            spVideoService.updateSpVideoCollectNumber1(userLike.getVideoId());
            return ResponseUtil.update(spCollectService.deleteSpCollect(spCollect));
        }
    }

    /**
     * 视频收藏
     *
     * @return
     */

    @Log(title = "视频购买", businessType = BusinessType.OTHER)
    @ApiOperation("视频购买")
    @PostMapping("/videoPurchase")
    public ResponseUtil videoPurchase( SpConsumeRecord spConsumeRecord) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_id",spConsumeRecord.getUserId());
        wrapper.eq("type",10);
        wrapper.eq("to_user_id",spConsumeRecord.getToUserId());
        SpConsumeRecord consumeRecord = consumeRecordService.getOne(wrapper);

        SpVideo spVideo = spVideoService.getById(spConsumeRecord.getToUserId());

        SpUsers spUsers = spUsersService.getOne(new QueryWrapper<SpUsers>().eq("user_id", spConsumeRecord.getUserId()));
        BigDecimal videoUnlockGlod = spVideo.getVideoUnlockGlod();
        Boolean isVip = false;
        if (spUsers.getUserMembersDay() != null &&  spUsers.getUserMembersDay().compareTo(new Date())>0){
            VideoDiscount videoDiscount = videoDiscountService.getById(2);
            isVip = true;
            videoUnlockGlod  = videoUnlockGlod.multiply(BigDecimal.valueOf(videoDiscount.getDiscount()));
        }else {
            VideoDiscount videoDiscount = videoDiscountService.getById(1);
            videoUnlockGlod  = videoUnlockGlod.multiply(BigDecimal.valueOf(videoDiscount.getDiscount()));
        }

        switch (spVideo.getVideoUnlock().intValue()){
            case 1:
                return ResponseUtil.fail("无需购买");
            case 2:
//                if (ObjectUtils.isNotEmpty(consumeRecord)){
//                    return ResponseUtil.fail("重复购买");
//                }else {
//
//                    if (videoUnlockGlod.compareTo(spUsers.getUserGlod())>-1){
//                        return ResponseUtil.fail("余额不足");
//                    }
//
//                    spUsers.setUserGlod(spUsers.getUserGlod().subtract(spVideo.getVideoUnlockGlod()) );
//                    spUsersService.updateSpUsers(spUsers);
//
//                    spConsumeRecord.setTradeNo(LpStringUtil.generateOrderNumber());
//                    spConsumeRecord.setMoney(spVideo.getVideoUnlockGlod());
//                    spConsumeRecord.setType(10);
//                    spConsumeRecord.setDescs("视频购买");
//                    spConsumeRecord.setPayType(2);
//                    spConsumeRecord.setChangeType(2);
//                    spConsumeRecord.setMoney(videoUnlockGlod);
//                    spConsumeRecord.setCurrentBalance(spUsers.getUserGlod());
//                    spConsumeRecord.setOperationAmount(spVideo.getVideoUnlockGlod());
//                    consumeRecordService.save(spConsumeRecord);
//                }
                return ResponseUtil.fail("请开通会员");
            case 3:
                if (ObjectUtils.isNotEmpty(consumeRecord)){
                    return ResponseUtil.fail("重复购买");
                }else {

                    if (videoUnlockGlod .compareTo( spUsers.getUserGlod())> -1){
                        return ResponseUtil.fail("余额不足");
                    }

                    spUsers.setUserGlod(spUsers.getUserGlod() .subtract(videoUnlockGlod));
                    spUsersService.updateSpUsers(spUsers);
                    spVideo.setUnlockCount(spVideo.getUnlockCount()+1);
                    spVideoService.updateById(spVideo);
                    spConsumeRecord.setTradeNo(LpStringUtil.generateOrderNumber());
                    spConsumeRecord.setType(10);
                    spConsumeRecord.setDescs("视频购买");
                    spConsumeRecord.setPayType(2);
                    spConsumeRecord.setChangeType(2);
                    spConsumeRecord.setMoney(videoUnlockGlod);
                    spConsumeRecord.setCurrentBalance(spUsers.getUserGlod());
                    spConsumeRecord.setOperationAmount(spVideo.getVideoUnlockGlod());
                    consumeRecordService.save(spConsumeRecord);
                }

        }

        return ResponseUtil.success();

    }

    /**
     * 视频详情
     * @Author lipeng
     * @Date 2022/2/17 14:01
     * @param videoId 视频id
     * @param userId 用户id
     * @return com.ruoyi.common.utils.status.ResponseBody
     */
    @Log(title = "视频详情", businessType = BusinessType.OTHER)
    @ApiOperation("视频详情")
    @PostMapping("/videoGetAll")
    public ResponseUtil getAll(Long videoId , Long userId) {

        HomeVideo homeVideo1 = new HomeVideo();

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("video_id",videoId);
        wrapper.select("\tvideo_id,\n" +
                "\tvideo_name,\n" +
                "\tvideo_cover_img,\n" +
                "\tactor_id,\n" +
                "\tvideo_introduce,\n" +
                "\tvideo_watch_number,\n" +
                "\tvideo_like_number,\n" +
                "\tvideo_unlock,\n" +
                "\tvideo_category_id,\n" +
                "\tvideo_putaway,\n" +
                "\tvideo_definition,\n" +
                "\tvideo_unlock_glod,\n" +
                "\tvideo_time,\n" +
                "\tvideo_url,\n" +
                "\tvideo_unlike_number,\n" +
                "\tvideo_collect_number,\n" +
                "\tvideo_author,\n" +
                "\tvideo_type,\n" +
                "\tvideo_url_preview,\n" +
                "\tvideo_duration,\n" +
                "\tis_top,\n" +
                "\tis_top_date,\n" +
                "\tunlock_count ,\n" +
                "\tCASE WHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 1\n" +
                "\tTHEN IF(video_unlike_number >0,99.99,100.00)\n" +
                "\tWHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 0 \n" +
                "\tTHEN  IF(video_like_number >0,0.01,0.00)\n" +
                "\tWHEN video_unlike_number = 0 and video_like_number = 0\n" +
                "\tTHEN 100.00\n" +
                "\tELSE\n" +
                "\t\t\tROUND(video_like_number /(video_like_number + video_unlike_number),4) * 100\n" +
                "\tEND  as video_score");
        SpVideo spVideo = spVideoService.getOne(wrapper);
        homeVideo1.setSpVideo(spVideo);
        homeVideo1.setSpUsers(spUsersService.selectSpUsersById(userId));
        List<SpVideo> spVideoList = spVideoService.selectSpVideoByCategoryIds(spVideoService.selectSpVideoById(videoId).getVideoCategoryId());


        homeVideo1.setSpVideoList(spVideoList);
        homeVideo1.setVideoEvaluationCounts(spVideoEvaluationService.selectCountById(videoId));

        SpUsers spUsers = spUsersService.getOne(new QueryWrapper<SpUsers>().eq("user_id", userId));
        if (spLikeService.selectSpLikeByIds(userId, videoId) != null) {
            homeVideo1.setSpLike(1L);//已点赞
        } else {
            homeVideo1.setSpLike(0l);//位点赞
        }
        if (spUnlikeService.selectSpUnlikeByIds(userId, videoId) != null) {
            homeVideo1.setSpUnlike(1l);//已差评
        } else {
            homeVideo1.setSpUnlike(0l);//为差评
        }
        if (spCollectService.selectSpCollectByIds(userId, videoId) != null) {
            homeVideo1.setSpCollect(1l);//已收藏
        } else {
            homeVideo1.setSpCollect(0l);//未收藏
        }
        SpExpenseCalendar spExpenseCalendar = new SpExpenseCalendar();
        spExpenseCalendar.setExpenseCalendarState(videoId);
        spExpenseCalendar.setExpenseCalendarWay(1);
        spExpenseCalendar.setUserId(userId);
        SpConsumeRecord consumeRecordServiceOne = consumeRecordService.getOne(new QueryWrapper<SpConsumeRecord>().eq("user_id", userId).eq("to_user_id", videoId));
        switch (spVideo.getVideoUnlock().intValue()){

            case 1:
                homeVideo1.setSpUnlock(1l);//已解锁
                break;
            case 2:
                if (spUsers != null && spUsers.getUserMembersDay().compareTo(new Date()) > 0){
                    homeVideo1.setSpUnlock(1l);//已解锁
                }else {
                    if (consumeRecordServiceOne !=null) {
                        homeVideo1.setSpUnlock(1l);//已解锁
                    } else {
                        if (spUsers.getUserTodayLong() >0){
                            homeVideo1.setSpUnlock(1l);//未解锁
                        }else {
                            homeVideo1.setSpUnlock(0l);//未解锁
                        }


                    }
                }
                break;
            case 3:
                if (consumeRecordServiceOne !=null) {
                    homeVideo1.setSpUnlock(1l);//已解锁
                } else {
                    homeVideo1.setSpUnlock(0l);//未解锁
                }

                break;
        }

        if (spVideoService.selectSpVideoById(videoId).getVideoUnlock()==1){
            Integer count=spExpenseCalendarService.selectSpExpenseCalendarByCount(videoId,1);//解锁人数
            homeVideo1.setUnlockCount(count);
        }

//        Long integer1= spVideo.getVideoUnlikeNumber();
//        Long integer2= spVideo.getVideoLikeNumber();
//        BigDecimal bigDecimal1 = BigDecimal.valueOf(integer1);
//        BigDecimal bigDecimal2 = BigDecimal.valueOf(integer2);
//        BigDecimal divide;
//        if (integer1.compareTo(integer2) > -1){
//            divide  = bigDecimal2.divide(bigDecimal1.add(bigDecimal2),4,BigDecimal.ROUND_UP);
//        }else {
//            divide  = bigDecimal2.divide(bigDecimal1.add(bigDecimal2),4,BigDecimal.ROUND_DOWN);
//        }
//        BigDecimal bigDecimal = divide.multiply(new BigDecimal(100)).setScale(2);
//        System.out.println("divide = " + bigDecimal);
//        homeVideo1.setFavorableRate(bigDecimal);


        return ResponseUtil.success(homeVideo1);
    }

    /**
     * 排序
     *
     * @param videoSort
     * @return
     */
    @Log(title = "视频排序", businessType = BusinessType.OTHER)
    @ApiOperation("视频排序  sort： 1：查询所有  2：根据时间排序 3：根据点赞人数排序 4：根据观看人数排序 5：根据收藏排序")
    @PostMapping("/getSort")
    public ResponseUtil getSort( VideoSort videoSort) {

//        startPages(videoSort.getPageNums(), videoSort.getPageSize());
        PageHelper.startPage(videoSort.getPageNums(),videoSort.getPageSize());
        if (videoSort.getVideoCategoryId() == 5) {
            List<SpVariety> spVarietyList = new ArrayList<>();
            SpVariety spVariety = new SpVariety();

            if (videoSort.getSort() == 1) {
                spVarietyList = spVarietyService.selectSpVarietyList(spVariety);
            }
            if (videoSort.getSort() == 2) {
                spVarietyList = spVarietyService.selectSpVarietySortLike();
            }
            if (videoSort.getSort() == 3) {
                spVarietyList = spVarietyService.selectSpVarietySortWatch();
            }

            return ResponseUtil.success(spVarietyList);

        } else {
            List<SpVideo> spVideoList = new ArrayList<>();
            SpVideo spVideo = new SpVideo();
            spVideo.setVideoName(videoSort.getVideoName());
            spVideo.setVideoCategoryId(videoSort.getVideoCategoryId());
            if (videoSort.getSort() == 1) {
                spVideoList = spVideoService.selectSpVideoLists(spVideo);
            }
            if (videoSort.getSort() == 2) {
                spVideoList = spVideoService.selectSpVideoListOrderByDate(spVideo);
            }
            if (videoSort.getSort() == 3) {
                spVideoList = spVideoService.selectSpVideoListOrderByLike(spVideo);
            }
            if (videoSort.getSort() == 4) {
                spVideoList = spVideoService.selectSpVideoListOrderByWatch(spVideo);
            }
            if (videoSort.getSort() == 5) {
                spVideoList = spVideoService.selectSpVideoListOrderByCollect(spVideo);
            }

            for (SpVideo video : spVideoList) {
                if (video.getVideoUnlock() != null && video.getVideoUnlock() == 1) {
                    Integer count = spExpenseCalendarService.selectSpExpenseCalendarByCount(video.getVideoId(), 1);//解锁人数

                    video.setUnlockCount(count);
                }
            }
            PageInfo<SpVideo> pageInfo = new PageInfo<>(spVideoList);
            return ResponseUtil.success(pageInfo);
        }

    }
    /**
     *
     *
     * @return
     */
    @ApiOperation("首页")
    @PostMapping("/getAllHome")
    public ResponseUtil getAllHome() {


        QueryWrapper<TemplateConfigs> wrapper = new QueryWrapper();
        wrapper.eq("parent_id",0);
        wrapper.eq("status",0);
        List<TemplateConfigs> templateConfigs = templateConfigsService.list(wrapper);
        TreeList list = new TreeList();
        if (templateConfigs.size() >0){
            TemplateConfigs templateConfigs1 = templateConfigs.get(0);
            List<TemplateConfigs> parent_id = templateConfigsService.list(new QueryWrapper<TemplateConfigs>().eq("parent_id", templateConfigs1.getDeptId()).orderByAsc("order_num"));
            //遍历所有模板
            for (TemplateConfigs templateConfigs2:parent_id
                 ) {
                Map map = new HashMap();
                map.put("templateName",templateConfigs2.getDeptName());
                map.put("templatetype",templateConfigs2.getType());
                map.put("templateId",templateConfigs2.getDeptId());
//                查出模板下所有视频分类
                List<TemplateConfigs> parent_id1 = templateConfigsService.list(new QueryWrapper<TemplateConfigs>().eq("parent_id", templateConfigs2.getDeptId()).isNotNull("category_tag_value"));
                ArrayList<String> list1 = new ArrayList();
                for (TemplateConfigs templateConfigs3:parent_id1
                     ) {
                    list1.add(templateConfigs3.getCategoryTagValue());
                }
                String join = String.join(",", list1);
                PageHelper.startPage(1,10);
                if(list1.size()>0){
                    QueryWrapper queryWrapper =  new QueryWrapper();
                    queryWrapper.orderByDesc("is_top_date","video_time");
                    queryWrapper.in("video_category_id",list1);
                    queryWrapper.select("\tvideo_id,\n" +
                            "\tvideo_name,\n" +
                            "\tvideo_cover_img,\n" +
                            "\tactor_id,\n" +
                            "\tvideo_introduce,\n" +
                            "\tvideo_watch_number,\n" +
                            "\tvideo_like_number,\n" +
                            "\tvideo_unlock,\n" +
                            "\tvideo_category_id,\n" +
                            "\tvideo_putaway,\n" +
                            "\tvideo_definition,\n" +
                            "\tvideo_unlock_glod,\n" +
                            "\tvideo_time,\n" +
                            "\tvideo_url,\n" +
                            "\tvideo_unlike_number,\n" +
                            "\tvideo_collect_number,\n" +
                            "\tvideo_author,\n" +
                            "\tvideo_type,\n" +
                            "\tvideo_url_preview,\n" +
                            "\tvideo_duration,\n" +
                            "\tis_top,\n" +
                            "\tis_top_date,\n" +
                            "\tunlock_count ,\n" +
                            "\tCASE WHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 1\n" +
                            "\tTHEN IF(video_unlike_number >0,99.99,100.00)\n" +
                            "\tWHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 0 \n" +
                            "\tTHEN  IF(video_like_number >0,0.01,0.00)\n" +
                            "\tWHEN video_unlike_number = 0 and video_like_number = 0\n" +
                            "\tTHEN 100.00\n" +
                            "\tELSE\n" +
                            "\t\t\tROUND(video_like_number /(video_like_number + video_unlike_number),4) * 100\n" +
                            "\tEND  as video_score");
                    map.put("videoList",spVideoService.list(queryWrapper));
                }else {
                    map.put("videoList",null);
                }
                list.add(map);
            }

        }else {

        }
//        templateConfigs.get(0);


//        PageHelper.startPage(pageNum, pageSize);

//        List<SpVideo> spVideos = spVideoService.selectSpVideoListByName(name);
//        QueryWrapper wrapper = new QueryWrapper();

//        List<SpVideo> list = spVideoService.list(wrapper);

//        PageInfo<SpVideo> pageInfo = new PageInfo<>(list);
        return ResponseUtil.success(list);
    }
    /**
     *
     *
     * @param name
     * @return
     */
    @ApiOperation("视频模糊查询")
    @PostMapping("/videoSearch")
    public ResponseUtil videoSearch(String name, Integer pageNum, Integer pageSize) {

        PageHelper.startPage(pageNum, pageSize);
        QueryWrapper wrapper = new QueryWrapper();

        if (name != null){
            wrapper.like("video_name",name);
        }
        wrapper.select("\tvideo_id,\n" +
                "\tvideo_name,\n" +
                "\tvideo_cover_img,\n" +
                "\tactor_id,\n" +
                "\tvideo_introduce,\n" +
                "\tvideo_watch_number,\n" +
                "\tvideo_like_number,\n" +
                "\tvideo_unlock,\n" +
                "\tvideo_category_id,\n" +
                "\tvideo_putaway,\n" +
                "\tvideo_definition,\n" +
                "\tvideo_unlock_glod,\n" +
                "\tvideo_time,\n" +
                "\tvideo_url,\n" +
                "\tvideo_unlike_number,\n" +
                "\tvideo_collect_number,\n" +
                "\tvideo_author,\n" +
                "\tvideo_type,\n" +
                "\tvideo_url_preview,\n" +
                "\tvideo_duration,\n" +
                "\tis_top,\n" +
                "\tis_top_date,\n" +
                "\tunlock_count ,\n" +
                "\tCASE WHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 1\n" +
                "\tTHEN IF(video_unlike_number >0,99.99,100.00)\n" +
                "\tWHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 0 \n" +
                "\tTHEN  IF(video_like_number >0,0.01,0.00)\n" +
                "\tWHEN video_unlike_number = 0 and video_like_number = 0\n" +
                "\tTHEN 100.00\n" +
                "\tELSE\n" +
                "\t\t\tROUND(video_like_number /(video_like_number + video_unlike_number),4) * 100\n" +
                "\tEND  as video_score");
        wrapper.orderByDesc("video_time");
        List<SpVideo> list = spVideoService.list(wrapper);
        PageInfo<SpVideo> pageInfo = new PageInfo<>(list);
        return ResponseUtil.success(pageInfo);
    }
    /**
     * 更多视频
     * @Author lipeng
     * @Date 2022/7/9 22:07
     * @param id
     * @param type 1标签跳转  2模板跳转
     * @return com.ruoyi.common.utils.status.ResponseUtil
     */
    @ApiOperation("更多视频")
    @PostMapping("/moreVideos")
    public ResponseUtil moreVideos(Integer id,Integer type,Integer categoryId ,Integer sort,Integer duration,Integer unlock,Integer pageNum,Integer pageSize) {
        List<String> strList = new ArrayList();
        HashMap maps = new HashMap();
        List<Map> category = new ArrayList<>();
        QueryWrapper<SpVideo> wrapper = new QueryWrapper();
        switch (type){
            case 1:
                category = videoTagConfigsService.category(id);
                for (Map<String,String> map:category
                     ) {
                    strList.add(map.get("id"));
                }

                if (categoryId != null && categoryId != 0){
                    wrapper.eq("video_category_id",categoryId);
                }else {
                    if (strList.size() >0){
                        wrapper.in("video_category_id",strList);
                    }else {
                        wrapper.in("video_category_id",0);
                    }

                }
//排序 1最新排序  2最多播放 3最多收藏
                if (sort != null ){
                    switch (sort){
                        case 1:
                            wrapper.orderByDesc("video_time");
                            break;
                        case 2:
                            wrapper.orderByDesc("video_watch_number");
                            break;
                        case 3:
                            wrapper.orderByDesc("video_collect_number");
                            break;
                    }

                }
//片长 0 所有片长   1：0-30   2：30-120  3：120以上
                if (duration != null && duration != 0){
                    switch (duration){
                        case 1:
                            wrapper.le("video_duration",30);
                            break;
                        case 2:
                            wrapper.ge("video_duration",30);
                            wrapper.le("video_duration",120);
                            break;
                        case 3:
                            wrapper.ge("video_duration",120);
                            break;
                    }
                }
//解锁状态 0 所有  1免费  2会员免费 3收费
                if (unlock != null && unlock != 0){
                    wrapper.eq("video_unlock",unlock);
                }
                PageHelper.startPage(pageNum, pageSize);


                maps.put("tag",category);
                wrapper.select("\tvideo_id,\n" +
                        "\tvideo_name,\n" +
                        "\tvideo_cover_img,\n" +
                        "\tactor_id,\n" +
                        "\tvideo_introduce,\n" +
                        "\tvideo_watch_number,\n" +
                        "\tvideo_like_number,\n" +
                        "\tvideo_unlock,\n" +
                        "\tvideo_category_id,\n" +
                        "\tvideo_putaway,\n" +
                        "\tvideo_definition,\n" +
                        "\tvideo_unlock_glod,\n" +
                        "\tvideo_time,\n" +
                        "\tvideo_url,\n" +
                        "\tvideo_unlike_number,\n" +
                        "\tvideo_collect_number,\n" +
                        "\tvideo_author,\n" +
                        "\tvideo_type,\n" +
                        "\tvideo_url_preview,\n" +
                        "\tvideo_duration,\n" +
                        "\tis_top,\n" +
                        "\tis_top_date,\n" +
                        "\tunlock_count ,\n" +
                        "\tCASE WHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 1\n" +
                        "\tTHEN IF(video_unlike_number >0,99.99,100.00)\n" +
                        "\tWHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 0 \n" +
                        "\tTHEN  IF(video_like_number >0,0.01,0.00)\n" +
                        "\tWHEN video_unlike_number = 0 and video_like_number = 0\n" +
                        "\tTHEN 100.00\n" +
                        "\tELSE\n" +
                        "\t\t\tROUND(video_like_number /(video_like_number + video_unlike_number),4) * 100\n" +
                        "\tEND  as video_score");
                maps.put("list",PageInfo.of(spVideoService.list(wrapper)));
                return ResponseUtil.success(maps);
//                break;
            case 2:
                category = templateConfigsService.category(id);

                for (Map<String,String> map:category
                ) {
                    strList.add(map.get("id"));
                }

                if (categoryId != null && categoryId != 0){
                    wrapper.eq("video_category_id",categoryId);
                }else {
                    wrapper.in("video_category_id",strList);
                }
//排序 1最新排序  2最多播放 3最多收藏
                if (sort != null ){
                    switch (sort){
                        case 1:
                            wrapper.orderByDesc("video_time");
                            break;
                        case 2:
                            wrapper.orderByDesc("video_watch_number");
                            break;
                        case 3:
                            wrapper.orderByDesc("video_collect_number");
                            break;
                    }

                }
//片长 0 所有片长   1：0-30   2：30-120  3：120以上
                if (duration != null && duration != 0){
                    switch (duration){
                        case 1:
                            wrapper.le("video_duration",30);
                            break;
                        case 2:
                            wrapper.ge("video_duration",30);
                            wrapper.le("video_duration",120);
                            break;
                        case 3:
                            wrapper.ge("video_duration",120);
                            break;
                    }
                }
//解锁状态 0 所有  1免费  2会员免费 3收费
                if (unlock != null && unlock != 0){
                    wrapper.eq("video_unlock",unlock);
                }
                PageHelper.startPage(pageNum, pageSize);
//                return ResponseUtil.success(PageInfo.of(spVideoService.list(wrapper)));
                maps.put("tag",category);
                wrapper.select("\tvideo_id,\n" +
                        "\tvideo_name,\n" +
                        "\tvideo_cover_img,\n" +
                        "\tactor_id,\n" +
                        "\tvideo_introduce,\n" +
                        "\tvideo_watch_number,\n" +
                        "\tvideo_like_number,\n" +
                        "\tvideo_unlock,\n" +
                        "\tvideo_category_id,\n" +
                        "\tvideo_putaway,\n" +
                        "\tvideo_definition,\n" +
                        "\tvideo_unlock_glod,\n" +
                        "\tvideo_time,\n" +
                        "\tvideo_url,\n" +
                        "\tvideo_unlike_number,\n" +
                        "\tvideo_collect_number,\n" +
                        "\tvideo_author,\n" +
                        "\tvideo_type,\n" +
                        "\tvideo_url_preview,\n" +
                        "\tvideo_duration,\n" +
                        "\tis_top,\n" +
                        "\tis_top_date,\n" +
                        "\tunlock_count ,\n" +
                        "\tCASE WHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 1\n" +
                        "\tTHEN IF(video_unlike_number >0,99.99,100.00)\n" +
                        "\tWHEN ROUND(video_like_number /(video_like_number + video_unlike_number),4) = 0 \n" +
                        "\tTHEN  IF(video_like_number >0,0.01,0.00)\n" +
                        "\tWHEN video_unlike_number = 0 and video_like_number = 0\n" +
                        "\tTHEN 100.00\n" +
                        "\tELSE\n" +
                        "\t\t\tROUND(video_like_number /(video_like_number + video_unlike_number),4) * 100\n" +
                        "\tEND  as video_score");
                maps.put("list",PageInfo.of(spVideoService.list(wrapper)));
                return ResponseUtil.success(maps);



//                break;
        }
        return ResponseUtil.success();

    }


    @ApiOperation("标签分类")
    @PostMapping("/videoTag")
    public ResponseUtil getAllLikeName() {
        QueryWrapper<VideoTagConfigs> wrapper = new QueryWrapper();
        wrapper.eq("parent_id",0);
        wrapper.eq("status",0);
        wrapper.orderByAsc("order_num");
//        wrapper.isNotNull("category_tag_value");
        List<VideoTagConfigs> videoTagConfigs = videoTagConfigsService.list(wrapper);
        TreeList list = new TreeList();
        for (VideoTagConfigs videoTagConfigs1 :videoTagConfigs
        ) {
            HashMap map = new HashMap();
            map.put("templateName",videoTagConfigs1.getDeptName());
            map.put("templatetype",videoTagConfigs1.getType());
            map.put("templateId",videoTagConfigs1.getDeptId());
            list.add(map);
        }
        return ResponseUtil.success(list);
    }

    /**
     *
     *
     * @param name
     * @return
     */
    @ApiOperation("首页的模糊查询  pageNum:1 pageSize:10 orderByColumn: video_time 时间 ，" +
            "video_like_number  点赞人数 " +
            "video_collect_number 收藏人数"  +
            ",isAsc:倒序 desc 顺序 asc")
    @PostMapping("/getAllLikeNameV2")
    public ResponseUtil getAllLikeNameV2(String name, @Param("videoCategoryId") Long videoCategoryId, Integer pageNum, Integer pageSize) {

        PageHelper.startPage(pageNum, pageSize);
        QueryWrapper wrapper = new QueryWrapper();

        wrapper.like("video_name",name);
        wrapper.eq("video_category_id",videoCategoryId);

        List<SpVideo> list = spVideoService.list(wrapper);

        PageInfo<SpVideo> pageInfo = new PageInfo<>(list);
        return ResponseUtil.success();
    }


    /**
     *
     *
     * @param name
     * @return
     */
    @ApiOperation("首页的模糊查询  pageNum:1 pageSize:10 orderByColumn: video_time 时间 ，" +
            "video_like_number  点赞人数 " +
            "video_collect_number 收藏人数"  +
            ",isAsc:倒序 desc 顺序 asc")
    @PostMapping("/like")
    @Log(title = "首页的模糊查询", businessType = BusinessType.EXPORT)
    public ResponseUtil getAllLikeNameV2(String name,  Integer pageNum, Integer pageSize) {
        QueryWrapper<SpHotSearch> wrapper = new QueryWrapper();
        wrapper.eq("name",name);
        SpHotSearch spHotSearch = hotSearchService.getOne(wrapper);

        if (ObjectUtils.isEmpty(spHotSearch)){
            spHotSearch = new SpHotSearch();
            spHotSearch.setName(name);
            spHotSearch.setNum(1);
            spHotSearch.setDeleted(0);
            hotSearchService.save(spHotSearch);
        }else {
            spHotSearch.setNum(spHotSearch.getNum()+1);
            hotSearchService
                    .updateById(spHotSearch);
        }

        return spVideoService.likeAllName(name,pageNum,pageSize);
    }


    /**
     *
     *
     * @param name
     * @return
     */
    @ApiOperation("首页的模糊查询  pageNum:1 pageSize:10 orderByColumn: video_time 时间 ，" +
            "video_like_number  点赞人数 " +
            "video_collect_number 收藏人数"  +
            ",isAsc:倒序 desc 顺序 asc")
    @PostMapping("/getAllLikeName")
    @Log(title = "首页的模糊查询", businessType = BusinessType.EXPORT)
    public ResponseUtil getAllLikeNameV3(String name,  Integer pageNum, Integer pageSize) {
        QueryWrapper<SpHotSearch> wrapper = new QueryWrapper();
        wrapper.eq("name",name);
        SpHotSearch spHotSearch = hotSearchService.getOne(wrapper);

        if (ObjectUtils.isEmpty(spHotSearch)){
            spHotSearch = new SpHotSearch();
            spHotSearch.setName(name);
            spHotSearch.setNum(1);
            spHotSearch.setDeleted(0);
            hotSearchService.save(spHotSearch);
        }else {
            spHotSearch.setNum(spHotSearch.getNum()+1);
            hotSearchService
                    .updateById(spHotSearch);
        }

        return spVideoService.likeAllName(name,pageNum,pageSize);
    }


    /**
     *
     *
     * @param pageSize
     * @return
     */
    @PostMapping("/hotSearch")
    public ResponseUtil hotSearch(  Integer pageNum, Integer pageSize) {


        if (pageNum !=null  && pageSize != null){
            PageHelper.startPage(pageNum,pageSize);
        }else {
            PageHelper.startPage(1,10);
        }
        QueryWrapper wrapper =  new QueryWrapper();
        wrapper.orderByDesc("num");
        List<SpHotSearch> list = hotSearchService.list(wrapper);

        return ResponseUtil.success(PageInfo.of(list));
    }



    /**
     *
     *
     * @param name
     * @return
     */
    @PostMapping("/headlines")
    public ResponseUtil headlines(String name,  Integer pageNum, Integer pageSize) {

        if (pageNum !=null  && pageSize != null){
            PageHelper.startPage(pageNum,pageSize);
        }else {
            PageHelper.startPage(1,10);
        }
//
//        QueryWrapper wrapper = new QueryWrapper();
//        wrapper.orderByDesc("video_time");
//        List list = videoHeadlinesService.list(wrapper);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("is_headlines",true);
        wrapper.orderByDesc("is_headlines_time");
        List list = spVideoService.list(wrapper);
        return ResponseUtil.success(PageInfo.of(list));
    }






    //动漫操作


    /**
     * 查询动漫视频管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:list')")
    @GetMapping("/list1")
    public TableDataInfo list1(SpVideo spVideo) {
        startPage();
        spVideo.setVideoType(2);
        List<SpVideo> list = spVideoService.selectSpVideoList(spVideo);


        List<ActorVideo> actorVideoList = new ArrayList<>();
        TableDataInfo dataTable = getDataTable(list);
        ActorVideo actorVideo;
        for (SpVideo video : list) {
            actorVideo = new ActorVideo();
            actorVideo.setSpVideo(video);
            if (video.getVideoCategoryId() == null) {
                actorVideo.setVideoCategoryName("该视频暂未分类");
            } else {
                actorVideo.setVideoCategoryName(spVideoCategoryService.selectSpVideoCategoryById(video.getVideoCategoryId()).getVideoCategoryName());
            }

            actorVideoList.add(actorVideo);
        }
        dataTable.setRows(actorVideoList);
        return dataTable;
    }

    /**
     * 新增动漫视频管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video:add')")
    @Log(title = "视频管理", businessType = BusinessType.INSERT)
    @PostMapping("/add1")
    public AjaxResult add1(@RequestBody SpVideo spVideo) {

        spVideo.setVideoTime(new Date());
        spVideo.setVideoType(2);
        return toAjax(spVideoService.insertSpVideo(spVideo));
    }



    /**
     * 动漫排序
     *
     * @param animeVideoSort
     * @return
     */
    @ApiOperation("动漫排序")
    @PostMapping("/getSortAnime")
    public TableDataInfo getSortAnime(@RequestBody AnimeVideoSort animeVideoSort) {
/**
 * 1，全部 2.3D 3.无码 4.有码 5.最近更新 6.人气榜 7.播放次数
 */
        startPages(animeVideoSort.getPageNums(), animeVideoSort.getPageSize());
        List<SpVideo>spVideoList=new ArrayList<>();
        Long videoCategoryId=null;
        if (animeVideoSort.getSort()==1){
           spVideoList= spVideoService.selectSpAnimeVideoList(videoCategoryId);
        }
        if (animeVideoSort.getSort()==2){
            videoCategoryId=9l;
            spVideoList= spVideoService.selectSpAnimeVideoList(videoCategoryId);
        }
        if (animeVideoSort.getSort()==3){
            videoCategoryId=10l;
            spVideoList= spVideoService.selectSpAnimeVideoList(videoCategoryId);
        }
        if (animeVideoSort.getSort()==4){
            videoCategoryId=11l;
            spVideoList= spVideoService.selectSpAnimeVideoList(videoCategoryId);
        }
        if (animeVideoSort.getSort()==5){
            spVideoList=spVideoService.selectSpAnimeVideoListOrderTime(videoCategoryId);
        }
        if (animeVideoSort.getSort()==6){
            spVideoList=spVideoService.selectSpAnimeVideoListOrderLike(videoCategoryId);
        }
        if (animeVideoSort.getSort()==7){
            spVideoList=spVideoService.selectSpAnimeVideoListOrderWatch(videoCategoryId);
        }

        return getDataTable(spVideoList);
    }
//
//    /**
//     * 动漫的模糊查询
//     * @param videoName
//     * @return
//     */
//    @ApiOperation("动漫的模糊查询")
//  @GetMapping("/getAnimeByName")
//    public ResponseBody getAnimeByName(@RequestParam(value = "videoName",required = false) String videoName){
//         startPage();
//        List<SpVideo> spVideoList= spVideoService.selectSpAnimeVideoListByName(videoName);
//          return ResponseBody.success(spVideoList);
//    }

    /**
     * 动漫视频详情
     *
     * @param homeVideo
     * @return
     */
    @ApiOperation("动漫视频详情")
    @PostMapping("/getAnime")
    public ResponseUtil getAnime(@RequestBody HomeVideo homeVideo) {
        HomeVideo homeVideo1 = new HomeVideo();
        homeVideo1.setSpVideo(spVideoService.selectSpVideoById(homeVideo.getVideoId()));
        homeVideo1.setSpUsers(spUsersService.selectSpUsersById(homeVideo.getUserId()));
        List<SpVideo> spVideoList = spVideoService.selectSpVideoByCategoryIds(spVideoService.selectSpVideoById(homeVideo.getVideoId()).getVideoCategoryId());
        homeVideo1.setSpVideoList(spVideoList);
        homeVideo1.setVideoEvaluationCounts(spVideoEvaluationService.selectCountById(homeVideo.getVideoId()));
        if (spLikeService.selectSpLikeByIds(homeVideo.getUserId(), homeVideo.getVideoId()) != null) {
            homeVideo1.setSpLike(1L);//已点赞
        } else {
            homeVideo1.setSpLike(0l);//位点赞
        }
        if (spUnlikeService.selectSpUnlikeByIds(homeVideo.getUserId(), homeVideo.getVideoId()) != null) {
            homeVideo1.setSpUnlike(1l);//已差评
        } else {
            homeVideo1.setSpUnlike(0l);//为差评
        }
        if (spCollectService.selectSpCollectByIds(homeVideo.getUserId(), homeVideo.getVideoId()) != null) {
            homeVideo1.setSpCollect(1l);//已收藏
        } else {
            homeVideo1.setSpCollect(0l);//未收藏
        }
        SpExpenseCalendar spExpenseCalendar = new SpExpenseCalendar();
        spExpenseCalendar.setExpenseCalendarState(homeVideo.getVideoId());
        spExpenseCalendar.setExpenseCalendarWay(1);
        spExpenseCalendar.setUserId(homeVideo.getUserId());
        if (spExpenseCalendarService.selectSpExpenseCalendarList(spExpenseCalendar).size() > 0) {
            homeVideo1.setSpUnlock(1l);//已解锁
        } else {
            homeVideo1.setSpUnlock(0l);//未解锁
        }
        return ResponseUtil.success(homeVideo1);
    }



    /**
     * 动漫视频点赞
     */
    @ApiOperation("动漫视频点赞")
    @PostMapping("/animeVideoLike")
    public ResponseUtil animeVideoLike(@RequestBody UserLike userLike) {
        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVideoId());
        spLike.setLikeType(0L);
        SpUnlike spUnlike = new SpUnlike();
        spUnlike.setUserId(userLike.getUserId());
        spUnlike.setUnlikeTypeId(userLike.getVideoId());
        spUnlike.setUnlikeType(0L);

        if (spLikeService.selectSpLike(spLike) == null) {
            if (spUnlikeService.selectSpUnlikeList(spUnlike).size() == 0) {
                spVideoService.updateSpVideoLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spLikeService.insertSpLike(spLike));
            } else {
                if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoUnlikeNumber() > 0) {
                    spVideoService.updateSpVideoUnLikeNumber1(userLike.getVideoId());
                }
                spUnlikeService.deleteSpUnlike(spUnlike);
                spVideoService.updateSpVideoLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spLikeService.insertSpLike(spLike));
            }

        } else {
            if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoLikeNumber() > 0) {
                spVideoService.updateSpVideoLikeNumber1(userLike.getVideoId());
            }

            return ResponseUtil.update(spLikeService.deleteSpLike(spLike));
        }


    }

    /**
     * 动漫视频差评
     */
    @ApiOperation("动漫视频差评")
    @PostMapping("/animeVideoUnLike")
    public ResponseUtil animeVideoUnLike(@RequestBody UserLike userLike) {
        SpUnlike spUnlike = new SpUnlike();
        spUnlike.setUserId(userLike.getUserId());
        spUnlike.setUnlikeTypeId(userLike.getVideoId());
        spUnlike.setUnlikeType(0L);

        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVideoId());
        spLike.setLikeType(0L);

        if (spUnlikeService.selectSpUnlikeList(spUnlike).size() == 0) {
            if (spLikeService.selectSpLike(spLike) == null) {
                spVideoService.updateSpVideoUnLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spUnlikeService.insertSpUnlike(spUnlike));
            } else {
                if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoLikeNumber() > 0) {
                    spVideoService.updateSpVideoLikeNumber1(userLike.getVideoId());
                }
                spLikeService.deleteSpLike(spLike);
                spVideoService.updateSpVideoUnLikeNumber(userLike.getVideoId());
                return ResponseUtil.update(spUnlikeService.insertSpUnlike(spUnlike));
            }

        } else {
            if (spVideoService.selectSpVideoById(userLike.getVideoId()).getVideoUnlikeNumber() > 0) {
                spVideoService.updateSpVideoUnLikeNumber1(userLike.getVideoId());
            }

            return ResponseUtil.update(spUnlikeService.deleteSpUnlike(spUnlike));
        }


    }


    /**
     * 动漫视频收藏
     *
     * @param userLike
     * @return
     */
    @ApiOperation("动漫视频收藏")
    @PostMapping("/animeVideoCollect")
    public ResponseUtil animeVideoCollect(@RequestBody UserLike userLike) {
        SpCollect spCollect = new SpCollect();
        spCollect.setUserId(userLike.getUserId());
        spCollect.setCollectTypeId(userLike.getVideoId());
        spCollect.setCollectType(0l);
        spCollect.setCollectTime(new Date());
        if (spCollectService.selectSpCollectList(spCollect).size() == 0) {
            spVideoService.updateSpVideoCollectNumber(userLike.getVideoId());
            return ResponseUtil.success(spCollectService.insertSpCollect(spCollect));
        } else {
            spVideoService.updateSpVideoCollectNumber1(userLike.getVideoId());
            return ResponseUtil.success(spCollectService.deleteSpCollect(spCollect));
        }
    }

    /**
     * 楼凤页banner图
     * @return
     */
    @ApiOperation("banner图")
    @GetMapping("/animeBanner")
    public ResponseUtil animeBanner(String bannerType, String advertisingHome){
        QueryWrapper<SpAdvertising> wrapper = new QueryWrapper();
        wrapper.eq("advertising_home",advertisingHome);
        wrapper.eq("advertising_state",0);
        wrapper.eq("banner_type",bannerType);

        return ResponseUtil.success(spAdvertisingService.list(wrapper));
//        return AjaxResult.success(spAdvertisingService.selectSpAdvertisingAnime());
    }

    /**
     * 楼凤页banner图
     * @return
     */
    @ApiOperation("分类标签")
    @GetMapping("/categoryTag")
    public ResponseUtil categoryTag(String bannerType, String advertisingHome){

        QueryWrapper<SpCategoryTag> wrapper = new QueryWrapper();
        wrapper.eq("is_show",true);
        wrapper.groupBy("status");
        wrapper.orderByAsc("status");
        wrapper.select("status");
        List<SpCategoryTag> list = categoryTagService.list(wrapper);
        ArrayList lists = new ArrayList();
        for (SpCategoryTag categoryTag : list) {
            wrapper = new QueryWrapper<>();
            wrapper.eq("is_show",true);
            wrapper.eq("status",categoryTag.getStatus());
            wrapper.orderByAsc("order_num");
            List<SpCategoryTag> list1 = categoryTagService.list(wrapper);
            lists.add(list1);;
        }
        return ResponseUtil.success(lists);
    }


}
