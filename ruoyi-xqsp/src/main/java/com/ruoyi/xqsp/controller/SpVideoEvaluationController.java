package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.xqsp.domain.SpLike;
import com.ruoyi.xqsp.domain.VideoEvaluation;
import com.ruoyi.xqsp.domain.entity.HomeVideo;
import com.ruoyi.xqsp.domain.entity.UserLike;
import com.ruoyi.xqsp.domain.entity.VideoEvaluationLike;
import com.ruoyi.xqsp.service.ISpLikeService;
import com.ruoyi.xqsp.service.ISpUsersService;
import com.ruoyi.xqsp.service.ISpVideoService;
import org.apache.poi.ss.formula.functions.Now;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;
import com.ruoyi.xqsp.service.ISpVideoEvaluationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频评价Controller
 *
 * @author ruoyi
 * @date 2021-04-24
 */
@RestController
@RequestMapping("/xqsp/evaluation")
public class SpVideoEvaluationController extends BaseController {
    @Autowired
    private ISpVideoEvaluationService spVideoEvaluationService;

    @Autowired
    private ISpVideoService iSpVideoService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpLikeService spLikeService;

    /**
     * 查询视频评价列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:evaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVideoEvaluation spVideoEvaluation) {
        startPage();
        List<SpVideoEvaluation> list = spVideoEvaluationService.selectSpVideoEvaluationList(spVideoEvaluation);
        TableDataInfo dataTable = getDataTable(list);
        List<VideoEvaluation> videoEvaluationList = new ArrayList<>();
        VideoEvaluation videoEvaluation;
        for (SpVideoEvaluation evaluation : list) {
            videoEvaluation = new VideoEvaluation();
            videoEvaluation.setSpVideoEvaluation(evaluation);
            if (evaluation.getUserId() != null) {
                if (spUsersService.selectSpUsersById(evaluation.getUserId()) != null) {
                    videoEvaluation.setUserPhone(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserPhone());
                }

            } else {
                videoEvaluation.setUserPhone("无");
            }
            if (evaluation.getVideoId() != null) {
                if (iSpVideoService.selectSpVideoById(evaluation.getVideoId()) != null) {
                    videoEvaluation.setVideoName(iSpVideoService.selectSpVideoById(evaluation.getVideoId()).getVideoName());
                }

            } else {
                videoEvaluation.setVideoName("无");
            }


            videoEvaluationList.add(videoEvaluation);
        }
        dataTable.setRows(videoEvaluationList);
        return dataTable;
    }

    /**
     * 查询视频评价列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:evaluation:list')")
    @GetMapping("/videoId/{videoId}")
    public TableDataInfo getAllByVideoId(@PathVariable("videoId") Long videoId) {
        startPage();
        List<SpVideoEvaluation> list = spVideoEvaluationService.selectSpVideoEvaluationListByVideoId(videoId);
        TableDataInfo dataTable = getDataTable(list);
        List<VideoEvaluation> videoEvaluationList = new ArrayList<>();
        VideoEvaluation videoEvaluation;
        for (SpVideoEvaluation evaluation : list) {
            videoEvaluation = new VideoEvaluation();
            videoEvaluation.setSpVideoEvaluation(evaluation);
            if (evaluation.getUserId() != null) {
                videoEvaluation.setUserPhone(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserPhone());
            } else {
                videoEvaluation.setUserPhone("无");
            }
            videoEvaluation.setVideoName(iSpVideoService.selectSpVideoById(evaluation.getVideoId()).getVideoName());
            videoEvaluationList.add(videoEvaluation);
        }
        dataTable.setRows(videoEvaluationList);
        return dataTable;
    }

    /**
     * 导出视频评价列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:evaluation:export')")
    @Log(title = "视频评价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVideoEvaluation spVideoEvaluation) {
        List<SpVideoEvaluation> list = spVideoEvaluationService.selectSpVideoEvaluationList(spVideoEvaluation);
        ExcelUtil<SpVideoEvaluation> util = new ExcelUtil<SpVideoEvaluation>(SpVideoEvaluation.class);
        return util.exportExcel(list, "evaluation");
    }

    /**
     * 获取视频评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:evaluation:query')")
    @GetMapping(value = "/{videoEvaluationId}")
    public AjaxResult getInfo(@PathVariable("videoEvaluationId") Long videoEvaluationId) {
        return AjaxResult.success(spVideoEvaluationService.selectSpVideoEvaluationById(videoEvaluationId));
    }

    /**
     * 新增视频评价
     */

    @Log(title = "视频评价", businessType = BusinessType.INSERT)
    @PostMapping("/addVideoEvaluation")
    public AjaxResult add(@RequestBody SpVideoEvaluation spVideoEvaluation) {
        spVideoEvaluation.setVideoEvaluationTime(new Date());
        spVideoEvaluation.setVideoEvaluationLike(0l);
        return toAjax(spVideoEvaluationService.insertSpVideoEvaluation(spVideoEvaluation));
    }

    /**
     * 修改视频评价
     */
    @PreAuthorize("@ss.hasPermi('xqsp:evaluation:edit')")
    @Log(title = "视频评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVideoEvaluation spVideoEvaluation) {
        return toAjax(spVideoEvaluationService.updateSpVideoEvaluation(spVideoEvaluation));
    }

    /**
     * 删除视频评价
     */
    @PreAuthorize("@ss.hasPermi('xqsp:evaluation:remove')")
    @Log(title = "视频评价", businessType = BusinessType.DELETE)
    @DeleteMapping("/{videoEvaluationIds}")
    public AjaxResult remove(@PathVariable Long[] videoEvaluationIds) {
        return toAjax(spVideoEvaluationService.deleteSpVideoEvaluationByIds(videoEvaluationIds));
    }


    /**
     * 点赞
     */
    @PostMapping("/videoEvaluationLike")
    public AjaxResult videoEvaluationLike(@RequestBody UserLike userLike) {
        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVideoEvaluationId());
        spLike.setLikeType(1L);
        if (spLikeService.selectSpLike(spLike) == null) {

            spVideoEvaluationService.updateSpVideoEvaluationLike(userLike.getVideoEvaluationId());
            return toAjax(spLikeService.insertSpLike(spLike));


        } else {
            if (spVideoEvaluationService.selectSpVideoEvaluationById(userLike.getVideoEvaluationId()).getVideoEvaluationLike() > 0) {
                spVideoEvaluationService.updateSpVideoEvaluationLike1(userLike.getVideoEvaluationId());
            }

            return toAjax(spLikeService.deleteSpLike(spLike));
        }


    }

    /**
     * 视频详情页的视频评论
     *
     * @param homeVideo
     * @return
     */
    @PostMapping("/getEvaluationByVideoId")
    public TableDataInfo getEvaluationByVideoId(@RequestBody VideoEvaluationLike homeVideo) {
        startPages(homeVideo.getPageNums(), homeVideo.getPageSize());
        List<VideoEvaluationLike> homeVideoList = new ArrayList<>();
        VideoEvaluationLike evaluationLike;
        SpLike spLike;
        List<SpVideoEvaluation> spVideoEvaluationList = spVideoEvaluationService.selectSpVideoEvaluationListByVideoId(homeVideo.getVideoId());
        TableDataInfo dataInfo = getDataTable(spVideoEvaluationList);
        for (SpVideoEvaluation evaluation : spVideoEvaluationList) {
            evaluationLike = new VideoEvaluationLike();
            evaluationLike.setSpUsers(spUsersService.selectSpUserById(evaluation.getUserId()).getUserName());
//            evaluationLike.setUserHead(spUsersService.selectSpUserById(evaluation.getUserId()).getUserImg());
            spLike = new SpLike();
            spLike.setUserId(homeVideo.getUserId());
            spLike.setLikeTypeId(evaluation.getVideoEvaluationId());
            spLike.setLikeType(1L);
            evaluationLike.setSpVideoEvaluation(evaluation);
            if (spLikeService.selectSpLike(spLike) != null) {
                evaluationLike.setLike(1);
            } else {
                evaluationLike.setLike(0);
            }
            homeVideoList.add(evaluationLike);
        }
        dataInfo.setRows(homeVideoList);

        return dataInfo;
    }


    /**
     * 动漫视频详情页的视频评论
     *
     * @param homeVideo
     * @return
     */
    @PostMapping("/getAnimeEvaluationByVideoId")
    public TableDataInfo getAnimeEvaluationByVideoId(@RequestBody VideoEvaluationLike homeVideo) {
        startPages(homeVideo.getPageNums(), homeVideo.getPageSize());
        List<VideoEvaluationLike> homeVideoList = new ArrayList<>();
        VideoEvaluationLike evaluationLike;
        SpLike spLike;
        List<SpVideoEvaluation> spVideoEvaluationList = spVideoEvaluationService.selectSpVideoEvaluationListByVideoId(homeVideo.getVideoId());
        TableDataInfo dataInfo = getDataTable(spVideoEvaluationList);
        for (SpVideoEvaluation evaluation : spVideoEvaluationList) {
            evaluationLike = new VideoEvaluationLike();
            evaluationLike.setSpUsers(spUsersService.selectSpUserById(evaluation.getUserId()).getUserName());
//            evaluationLike.setUserHead(spUsersService.selectSpUserById(evaluation.getUserId()).getUserImg());
            spLike = new SpLike();
            spLike.setUserId(homeVideo.getUserId());
            spLike.setLikeTypeId(evaluation.getVideoEvaluationId());
            spLike.setLikeType(1L);
            evaluationLike.setSpVideoEvaluation(evaluation);
            if (spLikeService.selectSpLike(spLike) != null) {
                evaluationLike.setLike(1);
            } else {
                evaluationLike.setLike(0);
            }
            homeVideoList.add(evaluationLike);
        }
        dataInfo.setRows(homeVideoList);

        return dataInfo;
    }

    /**
     * 新增动漫视频评价
     */

    @Log(title = "视频评价", businessType = BusinessType.INSERT)
    @PostMapping("/addAnimeVideoEvaluation")
    public AjaxResult addAnimeVideoEvaluation(@RequestBody SpVideoEvaluation spVideoEvaluation) {
        spVideoEvaluation.setVideoEvaluationTime(new Date());
        spVideoEvaluation.setVideoEvaluationLike(0l);
        return toAjax(spVideoEvaluationService.insertSpVideoEvaluation(spVideoEvaluation));
    }


}
