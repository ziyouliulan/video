package com.ruoyi.xqsp.controller;

import java.util.List;

import com.ruoyi.xqsp.utils.XqidUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpKey;
import com.ruoyi.xqsp.service.ISpKeyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 激活码表Controller
 * 
 * @author ruoyi
 * @date 2021-05-14
 */
@RestController
@RequestMapping("/xqsp/key")
public class SpKeyController extends BaseController
{
    @Autowired
    private ISpKeyService spKeyService;

    /**
     * 查询激活码表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:key:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpKey spKey)
    {
        startPage();
        List<SpKey> list = spKeyService.selectSpKeyList(spKey);
        return getDataTable(list);
    }

    /**
     * 导出激活码表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:key:export')")
    @Log(title = "激活码表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpKey spKey)
    {
        List<SpKey> list = spKeyService.selectSpKeyList(spKey);
        ExcelUtil<SpKey> util = new ExcelUtil<SpKey>(SpKey.class);
        return util.exportExcel(list, "key");
    }

    /**
     * 获取激活码表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:key:query')")
    @GetMapping(value = "/{keyId}")
    public AjaxResult getInfo(@PathVariable("keyId") Long keyId)
    {
        return AjaxResult.success(spKeyService.selectSpKeyById(keyId));
    }

    /**
     * 新增激活码表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:key:add')")
    @Log(title = "激活码表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpKey spKey)
    {

          String  keyActivationCode= XqidUtils.UserKey();

          spKey.setKeyActivationCode(keyActivationCode);

        return toAjax(spKeyService.insertSpKey(spKey));
    }

    /**
     * 修改激活码表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:key:edit')")
    @Log(title = "激活码表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpKey spKey)
    {
        return toAjax(spKeyService.updateSpKey(spKey));
    }

    /**
     * 删除激活码表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:key:remove')")
    @Log(title = "激活码表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{keyIds}")
    public AjaxResult remove(@PathVariable Long[] keyIds)
    {
        return toAjax(spKeyService.deleteSpKeyByIds(keyIds));
    }
}
