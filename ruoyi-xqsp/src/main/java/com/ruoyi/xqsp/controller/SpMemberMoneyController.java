package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpMemberMoney;
import com.ruoyi.xqsp.service.ISpMemberMoneyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员设置Controller
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
@RestController
@RequestMapping("/xqsp/membermoney")
public class SpMemberMoneyController extends BaseController
{
    @Autowired
    private ISpMemberMoneyService spMemberMoneyService;

    /**
     * 查询会员设置列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:membermoney:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpMemberMoney spMemberMoney)
    {
        startPage();
        List<SpMemberMoney> list = spMemberMoneyService.selectSpMemberMoneyList(spMemberMoney);
        return getDataTable(list);
    }

    /**
     * 导出会员设置列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:membermoney:export')")
    @Log(title = "会员设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpMemberMoney spMemberMoney)
    {
        List<SpMemberMoney> list = spMemberMoneyService.selectSpMemberMoneyList(spMemberMoney);
        ExcelUtil<SpMemberMoney> util = new ExcelUtil<SpMemberMoney>(SpMemberMoney.class);
        return util.exportExcel(list, "membermoney");
    }

    /**
     * 获取会员设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:membermoney:query')")
    @GetMapping(value = "/{memberMoneyId}")
    public AjaxResult getInfo(@PathVariable("memberMoneyId") Long memberMoneyId)
    {
        return AjaxResult.success(spMemberMoneyService.selectSpMemberMoneyById(memberMoneyId));
    }

    /**
     * 新增会员设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:membermoney:add')")
    @Log(title = "会员设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpMemberMoney spMemberMoney)
    {
        return toAjax(spMemberMoneyService.insertSpMemberMoney(spMemberMoney));
    }

    /**
     * 修改会员设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:membermoney:edit')")
    @Log(title = "会员设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpMemberMoney spMemberMoney)
    {
        return toAjax(spMemberMoneyService.updateSpMemberMoney(spMemberMoney));
    }

    /**
     * 删除会员设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:membermoney:remove')")
    @Log(title = "会员设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{memberMoneyIds}")
    public AjaxResult remove(@PathVariable Long[] memberMoneyIds)
    {
        return toAjax(spMemberMoneyService.deleteSpMemberMoneyByIds(memberMoneyIds));
    }
}
