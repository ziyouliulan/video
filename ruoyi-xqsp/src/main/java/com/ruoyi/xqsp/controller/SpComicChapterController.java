package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicChapter;
import com.ruoyi.xqsp.service.ISpComicChapterService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 漫画章节Controller
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
@RestController
@RequestMapping("/xqsp/comicChapter")
public class SpComicChapterController extends BaseController
{
    @Autowired
    private ISpComicChapterService spComicChapterService;

    /**
     * 查询漫画章节列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicChapter:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicChapter spComicChapter)
    {
        startPage();
        List<SpComicChapter> list = spComicChapterService.selectSpComicChapterList(spComicChapter);
        return getDataTable(list);
    }

/**
 * 查询根据漫画id漫画章节
 */
@PreAuthorize("@ss.hasPermi('xqsp:comicChapter:query')")
@GetMapping(value = "/comicId")
    public TableDataInfo listByComicId(SpComicChapter spComicChapter){
        startPage();
        List<SpComicChapter>list=spComicChapterService.selectSpComicChapterByComicId1(spComicChapter);
        return getDataTable(list);
    }

    /**
     * 导出漫画章节列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicChapter:export')")
    @Log(title = "漫画章节", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicChapter spComicChapter)
    {
        List<SpComicChapter> list = spComicChapterService.selectSpComicChapterList(spComicChapter);
        ExcelUtil<SpComicChapter> util = new ExcelUtil<SpComicChapter>(SpComicChapter.class);
        return util.exportExcel(list, "comicChapter");
    }

    /**
     * 获取漫画章节详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicChapter:query')")
    @GetMapping(value = "/{comicChapterId}")
    public AjaxResult getInfo(@PathVariable("comicChapterId") Long comicChapterId)
    {
        return AjaxResult.success(spComicChapterService.selectSpComicChapterById(comicChapterId));
    }

    /**
     * 新增漫画章节
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicChapter:add')")
    @Log(title = "漫画章节", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicChapter spComicChapter)
    {
        if (spComicChapter.getComicChapterImg().lastIndexOf(",") == spComicChapter.getComicChapterImg().length()-1 || spComicChapter.getComicChapterImg().lastIndexOf("，") == spComicChapter.getComicChapterImg().length()-1){
            spComicChapter.setComicChapterImg( spComicChapter.getComicChapterImg().substring(0,spComicChapter.getComicChapterImg().length()-1));
        }
        return toAjax(spComicChapterService.insertSpComicChapter(spComicChapter));
    }

    /**
     * 修改漫画章节
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicChapter:edit')")
    @Log(title = "漫画章节", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicChapter spComicChapter)
    {

        if (spComicChapter.getComicChapterImg().lastIndexOf(",") == spComicChapter.getComicChapterImg().length()-1 ||spComicChapter.getComicChapterImg().lastIndexOf("，") == spComicChapter.getComicChapterImg().length()-1){
           spComicChapter.setComicChapterImg( spComicChapter.getComicChapterImg().substring(0,spComicChapter.getComicChapterImg().length()-1));
        }
        return toAjax(spComicChapterService.updateSpComicChapter(spComicChapter));
    }

    /**
     * 删除漫画章节
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicChapter:remove')")
    @Log(title = "漫画章节", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicChapterIds}")
    public AjaxResult remove(@PathVariable Long[] comicChapterIds)
    {
        return toAjax(spComicChapterService.deleteSpComicChapterByIds(comicChapterIds));
    }
}
