package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpChapterBuy;
import com.ruoyi.xqsp.service.ISpChapterBuyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 章节购买Controller
 * 
 * @author ruoyi
 * @date 2021-05-22
 */
@RestController
@RequestMapping("/xqsp/buy")
public class SpChapterBuyController extends BaseController
{
    @Autowired
    private ISpChapterBuyService spChapterBuyService;

    /**
     * 查询章节购买列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:buy:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpChapterBuy spChapterBuy)
    {
        startPage();
        List<SpChapterBuy> list = spChapterBuyService.selectSpChapterBuyList(spChapterBuy);
        return getDataTable(list);
    }

    /**
     * 导出章节购买列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:buy:export')")
    @Log(title = "章节购买", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpChapterBuy spChapterBuy)
    {
        List<SpChapterBuy> list = spChapterBuyService.selectSpChapterBuyList(spChapterBuy);
        ExcelUtil<SpChapterBuy> util = new ExcelUtil<SpChapterBuy>(SpChapterBuy.class);
        return util.exportExcel(list, "buy");
    }

    /**
     * 获取章节购买详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:buy:query')")
    @GetMapping(value = "/{chapterBuyId}")
    public AjaxResult getInfo(@PathVariable("chapterBuyId") Long chapterBuyId)
    {
        return AjaxResult.success(spChapterBuyService.selectSpChapterBuyById(chapterBuyId));
    }

    /**
     * 新增章节购买
     */
    @PreAuthorize("@ss.hasPermi('xqsp:buy:add')")
    @Log(title = "章节购买", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpChapterBuy spChapterBuy)
    {
        return toAjax(spChapterBuyService.insertSpChapterBuy(spChapterBuy));
    }

    /**
     * 修改章节购买
     */
    @PreAuthorize("@ss.hasPermi('xqsp:buy:edit')")
    @Log(title = "章节购买", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpChapterBuy spChapterBuy)
    {
        return toAjax(spChapterBuyService.updateSpChapterBuy(spChapterBuy));
    }

    /**
     * 删除章节购买
     */
    @PreAuthorize("@ss.hasPermi('xqsp:buy:remove')")
    @Log(title = "章节购买", businessType = BusinessType.DELETE)
	@DeleteMapping("/{chapterBuyIds}")
    public AjaxResult remove(@PathVariable Long[] chapterBuyIds)
    {
        return toAjax(spChapterBuyService.deleteSpChapterBuyByIds(chapterBuyIds));
    }
}
