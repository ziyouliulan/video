package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.ActorMessage;
import com.ruoyi.xqsp.domain.SpCollect;
import com.ruoyi.xqsp.domain.entity.ActorAndMessage;
import com.ruoyi.xqsp.domain.entity.ActorMessageCollect;
import com.ruoyi.xqsp.service.ISpActorService;
import com.ruoyi.xqsp.service.ISpCollectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpActorMessage;
import com.ruoyi.xqsp.service.ISpActorMessageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 演员信息Controller
 *
 * @author ruoyi
 * @date 2021-04-22
 */
@Api(value = "演员信息", tags = "演员信息")
@RestController
@RequestMapping("/xqsp/message")
public class SpActorMessageController extends BaseController {
    @Autowired
    private ISpActorMessageService spActorMessageService;
    @Autowired
    private ISpActorService iSpActorService;
@Autowired
private ISpCollectService spCollectService;

    /**
     * 查询演员信息列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpActorMessage spActorMessage) {
        startPage();
        List<SpActorMessage> list = spActorMessageService.selectSpActorMessageList(spActorMessage);
        TableDataInfo dataTable = getDataTable(list);
        List<ActorMessage> returnLst = new ArrayList<>();
        ActorMessage message;
        for (SpActorMessage actorMessage : list) {
            message = new ActorMessage();
            message.setSpActorMessage(actorMessage);
           if (actorMessage.getActorId()==null){
               message.setActorName("详情无演员");
           }else {
               if (iSpActorService.selectSpActorById(actorMessage.getActorId())!=null){
                   if (iSpActorService.selectSpActorById(actorMessage.getActorId()).getActorName()!=null){
                       message.setActorName(iSpActorService.selectSpActorById(actorMessage.getActorId()).getActorName());
                   }

               }

           }

            returnLst.add(message);
        }
        dataTable.setRows(returnLst);
        return dataTable;
    }

    /**
     * 导出演员信息列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:export')")
    @Log(title = "演员信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpActorMessage spActorMessage) {
        List<SpActorMessage> list = spActorMessageService.selectSpActorMessageList(spActorMessage);
        ExcelUtil<SpActorMessage> util = new ExcelUtil<SpActorMessage>(SpActorMessage.class);
        return util.exportExcel(list, "message");
    }

    /**
     * 获取演员信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:query')")
    @GetMapping(value = "/{actorMessageId}")
    public AjaxResult getInfo(@PathVariable("actorMessageId") Long actorMessageId) {
        return AjaxResult.success(spActorMessageService.selectSpActorMessageById(actorMessageId));
    }

    /**
     * 获取演员信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:query')")
    @GetMapping("/actorId/{actorId}")
    public AjaxResult getInfos(@PathVariable("actorId") Long actorId) {
        return AjaxResult.success(spActorMessageService.selectSpActorMessageByActorId(actorId));
    }

    /**
     * 获取演员信息详细信息
     */

    @ApiOperation("获取演员信息详细信息")
    @PostMapping("/actorMessage")
    public ResponseUtil getAllMessage(@RequestBody ActorMessageCollect actorMessageCollect) {
        ActorAndMessage actorAndMessage=new ActorAndMessage();
        actorAndMessage.setSpActorMessage(spActorMessageService.selectSpActorMessageByActorId(actorMessageCollect.getActorId()));
        actorAndMessage.setSpActor(iSpActorService.selectSpActorById(actorMessageCollect.getActorId()));
        SpCollect spCollect=new SpCollect();
        spCollect.setUserId(actorMessageCollect.getUserId());
        spCollect.setCollectTypeId(actorMessageCollect.getActorId());
        spCollect.setCollectType(4l);

        if (spCollectService.selectSpCollectList(spCollect).size()==0){
            actorAndMessage.setActorCollect(0l);//未收藏
        }
        if (spCollectService.selectSpCollectList(spCollect).size()>0){
            actorAndMessage.setActorCollect(1l);//已收藏
        }
        return ResponseUtil.success(actorAndMessage);
    }

    /**
     * 新增演员信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:add')")
    @Log(title = "演员信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpActorMessage spActorMessage) {
        return toAjax(spActorMessageService.insertSpActorMessage(spActorMessage));
    }

    /**
     * 修改演员信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:edit')")
    @Log(title = "演员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpActorMessage spActorMessage) {
        return toAjax(spActorMessageService.updateSpActorMessage(spActorMessage));
    }

    /**
     * 删除演员信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:message:remove')")
    @Log(title = "演员信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{actorMessageIds}")
    public AjaxResult remove(@PathVariable Long[] actorMessageIds) {
        return toAjax(spActorMessageService.deleteSpActorMessageByIds(actorMessageIds));
    }
}
