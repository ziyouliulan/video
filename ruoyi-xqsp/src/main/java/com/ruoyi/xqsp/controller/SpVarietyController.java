package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.entity.VarietyVideo;
import com.ruoyi.xqsp.service.ISpVarietyVideoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.service.ISpVarietyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 综艺管理Controller
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
@RestController
@RequestMapping("/xqsp/variety")
public class SpVarietyController extends BaseController
{
    @Autowired
    private ISpVarietyService spVarietyService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;

    /**
     * 查询综艺管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:variety:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVariety spVariety)
    {
        startPage();
        List<SpVariety> list = spVarietyService.selectSpVarietyList(spVariety);
        return getDataTable(list);
    }
    /**
     * 查询综艺管理列表
     */

    @GetMapping("/getVarietylist")
    public TableDataInfo getVarietylist()
    {
        startPage();
        SpVariety spVariety=new SpVariety();
        List<SpVariety> list = spVarietyService.selectSpVarietyList(spVariety);
        return getDataTable(list);
    }

    /**
     * 查询综艺列表
     */

    @GetMapping("/getVarietylist/{varietyId}")
    public AjaxResult getVarietylist(@PathVariable("varietyId") Long varietyId)
    {
        return AjaxResult.success(spVarietyService.selectSpVarietyById(varietyId));

    }


    /**
     * 导出综艺管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:variety:export')")
    @Log(title = "综艺管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVariety spVariety)
    {
        List<SpVariety> list = spVarietyService.selectSpVarietyList(spVariety);
        ExcelUtil<SpVariety> util = new ExcelUtil<SpVariety>(SpVariety.class);
        return util.exportExcel(list, "variety");
    }

    /**
     * 获取综艺管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:variety:query')")
    @GetMapping(value = "/{varietyId}")
    public AjaxResult getInfo(@PathVariable("varietyId") Long varietyId)
    {
        return AjaxResult.success(spVarietyService.selectSpVarietyById(varietyId));
    }

    /**
     * 新增综艺管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:variety:add')")
    @Log(title = "综艺管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVariety spVariety)
    {
        return toAjax(spVarietyService.insertSpVariety(spVariety));
    }

    /**
     * 修改综艺管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:variety:edit')")
    @Log(title = "综艺管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVariety spVariety)
    {
        return toAjax(spVarietyService.updateSpVariety(spVariety));
    }

    /**
     * 删除综艺管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:variety:remove')")
    @Log(title = "综艺管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{varietyIds}")
    public AjaxResult remove(@PathVariable Long[] varietyIds)
    {
        List<SpVarietyVideo>spVarietyVideoList=new ArrayList<>();
        SpVarietyVideo spVarietyVideo;
        for (Long id :varietyIds){
            if (spVarietyVideoService.selectSpVarietyVideoByVarietyId(id)!=null){
                spVarietyVideoList=spVarietyVideoService.selectSpVarietyVideoByVarietyId(id);
            }

        }
        if (spVarietyVideoList.size()>0){
            return AjaxResult.error("该综艺下存在视频无法删除");
        }else {
            return toAjax(spVarietyService.deleteSpVarietyByIds(varietyIds));
        }

    }



}
