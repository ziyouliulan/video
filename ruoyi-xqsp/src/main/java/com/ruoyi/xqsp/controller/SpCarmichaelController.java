package com.ruoyi.xqsp.controller;

import java.util.List;

import com.ruoyi.xqsp.utils.XqidUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpCarmichael;
import com.ruoyi.xqsp.service.ISpCarmichaelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 卡密操作Controller
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@RestController
@RequestMapping("/xqsp/carmichael")
public class SpCarmichaelController extends BaseController
{
    @Autowired
    private ISpCarmichaelService spCarmichaelService;

    /**
     * 查询卡密操作列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:carmichael:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpCarmichael spCarmichael)
    {
        startPage();
        List<SpCarmichael> list = spCarmichaelService.selectSpCarmichaelList(spCarmichael);
        return getDataTable(list);
    }

    /**
     * 导出卡密操作列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:carmichael:export')")
    @Log(title = "卡密操作", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpCarmichael spCarmichael)
    {
        List<SpCarmichael> list = spCarmichaelService.selectSpCarmichaelList(spCarmichael);
        ExcelUtil<SpCarmichael> util = new ExcelUtil<SpCarmichael>(SpCarmichael.class);
        return util.exportExcel(list, "carmichael");
    }

    /**
     * 获取卡密操作详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:carmichael:query')")
    @GetMapping(value = "/{carmichaelId}")
    public AjaxResult getInfo(@PathVariable("carmichaelId") Long carmichaelId)
    {
        return AjaxResult.success(spCarmichaelService.selectSpCarmichaelById(carmichaelId));
    }

    /**
     * 新增卡密操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:carmichael:add')")
    @Log(title = "卡密操作", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpCarmichael spCarmichael)
    {
        String carmichaelName= XqidUtils.UserKey();
        spCarmichael.setCarmichaelName(carmichaelName);
        return toAjax(spCarmichaelService.insertSpCarmichael(spCarmichael));
    }

    /**
     * 修改卡密操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:carmichael:edit')")
    @Log(title = "卡密操作", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpCarmichael spCarmichael)
    {
        return toAjax(spCarmichaelService.updateSpCarmichael(spCarmichael));
    }

    /**
     * 删除卡密操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:carmichael:remove')")
    @Log(title = "卡密操作", businessType = BusinessType.DELETE)
	@DeleteMapping("/{carmichaelIds}")
    public AjaxResult remove(@PathVariable Long[] carmichaelIds)
    {
        return toAjax(spCarmichaelService.deleteSpCarmichaelByIds(carmichaelIds));
    }
}
