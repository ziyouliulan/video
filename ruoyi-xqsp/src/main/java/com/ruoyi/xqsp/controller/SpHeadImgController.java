package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpHeadImg;
import com.ruoyi.xqsp.service.ISpHeadImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户头像Controller
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@RestController
@RequestMapping("/xqsp/head_img")
public class SpHeadImgController extends BaseController
{
    @Autowired
    private ISpHeadImgService spHeadImgService;

    /**
     * 查询用户头像列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:head_img:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpHeadImg spHeadImg)
    {
        startPage();
        List<SpHeadImg> list = spHeadImgService.selectSpHeadImgList(spHeadImg);
        return getDataTable(list);
    }

    /**
     * 导出用户头像列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:head_img:export')")
    @Log(title = "用户头像", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpHeadImg spHeadImg)
    {
        List<SpHeadImg> list = spHeadImgService.selectSpHeadImgList(spHeadImg);
        ExcelUtil<SpHeadImg> util = new ExcelUtil<SpHeadImg>(SpHeadImg.class);
        return util.exportExcel(list, "head_img");
    }

    /**
     * 获取用户头像详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:head_img:query')")
    @GetMapping(value = "/{headImgId}")
    public AjaxResult getInfo(@PathVariable("headImgId") Long headImgId)
    {
        return AjaxResult.success(spHeadImgService.selectSpHeadImgById(headImgId));
    }

    /**
     * 新增用户头像
     */
    @PreAuthorize("@ss.hasPermi('xqsp:head_img:add')")
    @Log(title = "用户头像", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpHeadImg spHeadImg)
    {
        return toAjax(spHeadImgService.insertSpHeadImg(spHeadImg));
    }

    /**
     * 修改用户头像
     */
    @PreAuthorize("@ss.hasPermi('xqsp:head_img:edit')")
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpHeadImg spHeadImg)
    {
        return toAjax(spHeadImgService.updateSpHeadImg(spHeadImg));
    }

    /**
     * 删除用户头像
     */
    @PreAuthorize("@ss.hasPermi('xqsp:head_img:remove')")
    @Log(title = "用户头像", businessType = BusinessType.DELETE)
	@DeleteMapping("/{headImgIds}")
    public AjaxResult remove(@PathVariable Long[] headImgIds)
    {
        return toAjax(spHeadImgService.deleteSpHeadImgByIds(headImgIds));
    }

    /**
     * 用户头像
     * @param headImgId
     * @return
     */
    @GetMapping("/getHeadImg/{headImgId}")
    public AjaxResult getHeadImg(@PathVariable("headImgId")Long headImgId){
        return AjaxResult.success(spHeadImgService.selectSpHeadImgById(headImgId));
    }
//    @GetMapping("/getAll")
//    public AjaxResult getAll(){
//        return AjaxResult.success(spHeadImgService.selectSpHeadImg());
//    }

}
