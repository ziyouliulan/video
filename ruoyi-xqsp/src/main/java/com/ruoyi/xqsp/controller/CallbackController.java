package com.ruoyi.xqsp.controller;


import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.xqsp.domain.SpConsumeRecord;
import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.service.ISpUsersService;
import com.ruoyi.xqsp.service.SpConsumeRecordService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

@Api(value = "支付回调", tags = "支付回调")
@RestController
@RequestMapping("/xqsp/callback")
@Slf4j
public class CallbackController {


    @Autowired
    RedisCache redisCache;


    @Autowired
    SpConsumeRecordService spConsumeRecordService;



    @Autowired
    ISpUsersService spUsersService;



    @Log(title = "支付FM")
    @RequestMapping("/zhifuFM/{order}")
    public ResponseEntity zhifuFM(@PathVariable("order") String order, HttpServletResponse httpServletResponse , HttpServletRequest request){

//        String order = request.getParameter("order");
        log.info("order:::::{}",order);

        SpConsumeRecord spConsumeRecord = redisCache.getCacheObject("order:" + order);

//
//        map.put("userId",userId);
//        map.put("type",type);
//        map.put("id",id);
//        map.put("amount",amount);

        Map<String, Object> cacheMap = redisCache.getCacheMap(order);
        long userId = (long) cacheMap.get("userId");
        int type = (int) cacheMap.get("type");


        SpUsers spUsers = spUsersService.getById(userId);
        switch (type){
            case 1:
                int userVipType = (int) cacheMap.get("userVipType");

                switch (userVipType){
                    //月卡
                    case 1:
                        spConsumeRecord.setType(1);
                        spConsumeRecord.setDescs("月卡");
//                        spConsumeRecord.setMoney(spVipCategories.get(0).getFavorable());
                        if (spUsers.getUserMembersDay() !=null && spUsers.getUserMembersDay().compareTo(new Date()) > - 1){
                            spUsers.setUserMembersDay(DateUtil.offsetMonth(spUsers.getUserMembersDay(),1));
                            if (spUsers.getUserVipType() <  userVipType){
                                spUsers.setUserVipType(userVipType);
                            }

                        }else {
                            spUsers.setUserMembersDay(DateUtil.offsetMonth(new Date(),1));
                        }


                        break;
//                    季卡
                    case 2:
                        spConsumeRecord.setType(2);
                        spConsumeRecord.setDescs("季卡");
                        if (spUsers.getUserMembersDay() !=null && spUsers.getUserMembersDay().compareTo(new Date()) > - 1){
                            spUsers.setUserMembersDay(DateUtil.offsetMonth(spUsers.getUserMembersDay(),3));
                            if (spUsers.getUserVipType() <  userVipType){
                                spUsers.setUserVipType(userVipType);
                            }

                        }else {
                            spUsers.setUserMembersDay(DateUtil.offsetMonth(new Date(),3));
                        }

                        break;
//                    年卡
                    case 3:
                        spConsumeRecord.setDescs("年卡");
                        spConsumeRecord.setType(3);

                        if (spUsers.getUserMembersDay() !=null && spUsers.getUserMembersDay().compareTo(new Date()) > - 1){
                            spUsers.setUserMembersDay(DateUtil.offsetMonth(spUsers.getUserMembersDay(),12));
                            if (spUsers.getUserVipType() <  userVipType){
                                spUsers.setUserVipType(userVipType);
                            }
                        }else {
                            spUsers.setUserMembersDay(DateUtil.offsetMonth(new Date(),12));
                        }

                        break;
                    case 0:
                        spConsumeRecord.setDescs("日卡");
                        spConsumeRecord.setType(0);
                        if (spUsers.getUserMembersDay() !=null && spUsers.getUserMembersDay().compareTo(new Date()) > - 1){
                            spUsers.setUserMembersDay(DateUtil.offsetDay(spUsers.getUserMembersDay(),1));
                            if (spUsers.getUserVipType() <  userVipType){
                                spUsers.setUserVipType(userVipType);
                            }
                        }else {
                            spUsers.setUserMembersDay(DateUtil.offsetDay(new Date(),1));
                        }

                        break;
                }


                break;
            case 2:
                long gold = (long) cacheMap.get("gold");
                spUsers.setUserGlod(spUsers.getUserGlod().add(BigDecimal.valueOf(gold)));
                break;
        }
        spUsersService.updateSpUsers(spUsers);

        spConsumeRecordService.save(spConsumeRecord);
        Enumeration<String> parameterNames = request.getParameterNames();
        System.out.println("parameterNames = " + JSONObject.toJSONString(parameterNames));


        return ResponseEntity.ok("success");
    }

}
