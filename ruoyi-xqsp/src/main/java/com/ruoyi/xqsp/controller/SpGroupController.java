package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpGroup;
import com.ruoyi.xqsp.service.ISpGroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车友群Controller
 * 
 * @author ruoyi
 * @date 2021-05-28
 */
@RestController
@RequestMapping("/xqsp/group")
public class SpGroupController extends BaseController
{
    @Autowired
    private ISpGroupService spGroupService;

    /**
     * 查询车友群列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:group:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpGroup spGroup)
    {
        startPage();
        List<SpGroup> list = spGroupService.selectSpGroupList(spGroup);
        return getDataTable(list);
    }

    /**
     * 导出车友群列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:group:export')")
    @Log(title = "车友群", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpGroup spGroup)
    {
        List<SpGroup> list = spGroupService.selectSpGroupList(spGroup);
        ExcelUtil<SpGroup> util = new ExcelUtil<SpGroup>(SpGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 获取车友群详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:group:query')")
    @GetMapping(value = "/{groupId}")
    public AjaxResult getInfo(@PathVariable("groupId") Long groupId)
    {
        return AjaxResult.success(spGroupService.selectSpGroupById(groupId));
    }

    /**
     * 新增车友群
     */
    @PreAuthorize("@ss.hasPermi('xqsp:group:add')")
    @Log(title = "车友群", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpGroup spGroup)
    {
        return toAjax(spGroupService.insertSpGroup(spGroup));
    }

    /**
     * 修改车友群
     */
    @PreAuthorize("@ss.hasPermi('xqsp:group:edit')")
    @Log(title = "车友群", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpGroup spGroup)
    {
        return toAjax(spGroupService.updateSpGroup(spGroup));
    }

    /**
     * 删除车友群
     */
    @PreAuthorize("@ss.hasPermi('xqsp:group:remove')")
    @Log(title = "车友群", businessType = BusinessType.DELETE)
	@DeleteMapping("/{groupIds}")
    public AjaxResult remove(@PathVariable Long[] groupIds)
    {
        return toAjax(spGroupService.deleteSpGroupByIds(groupIds));
    }

    /**
     * 查询车友群信息
     * @return
     */
    @GetMapping("/getAll")
    public AjaxResult getAll(){
        SpGroup spGroup=new SpGroup();
        return AjaxResult.success(spGroupService.selectSpGroupList(spGroup));
    }
}
