package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.*;
import com.ruoyi.xqsp.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 分类子表Controller
 *
 * @author ruoyi
 * @date 2021-04-25
 */
@Api(value = "分类", tags = "视频分类")
@RestController
@RequestMapping("/xqsp/category")
public class SpVideoCategoryController extends BaseController {
    @Autowired
    private ISpVideoCategoryService spVideoCategoryService;

    @Autowired
    private SpCategoryService categoryService;

    @Autowired
    private ISpVideoService spVideoService;
    @Autowired
    private ISpVarietyService spVarietyService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpActorService spActorService;
    @Autowired
    private ISpExpenseCalendarService spExpenseCalendarService;
    @Autowired
    private ISpAdvertisingService spAdvertisingService;
    /**
     * 查询分类子表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVideoCategory spVideoCategory) {
        startPage();
        List<SpVideoCategory> list = spVideoCategoryService.selectSpVideoCategoryList(spVideoCategory);
        TableDataInfo dataTable = getDataTable(list);
        List<CategoryParent> categoryParentList = new ArrayList<>();
        CategoryParent categoryParent;
        for (SpVideoCategory category : list) {
            categoryParent = new CategoryParent();
            categoryParent.setSpVideoCategory(category);
            categoryParentList.add(categoryParent);
        }
        dataTable.setRows(categoryParentList);
        return dataTable;
    }

    @ApiOperation("视频分类查询")
    @GetMapping("/videoCategory")
    public ResponseUtil videoCategory() {
        QueryWrapper<SpVideoCategory> queryWrapper = new QueryWrapper();
        queryWrapper.eq("video_category_state",0);
        queryWrapper.orderByDesc("video_category_weight");
        return ResponseUtil.success(spVideoCategoryService.list(queryWrapper));
    }

    @ApiOperation("视频主页查询")
    @GetMapping("/videoHome")
    public ResponseUtil videoHome() {
        QueryWrapper<SpVideoCategory> queryWrapper = new QueryWrapper();
        queryWrapper.eq("video_category_state",0);
        queryWrapper.orderByDesc("video_category_weight");
        List<SpVideoCategory> list = spVideoCategoryService.list(queryWrapper);
        SpCategory spCategory = categoryService.getById(1);

        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("categorys",list);
        ArrayList<Object> list1 = Lists.newArrayList();
        QueryWrapper<SpVideo> wrapper = new QueryWrapper<>();
        int i= 1;
//        List<SpAdvertising> spAdvertisings = spAdvertisingService.list();
//        HashMap<Object, Object> map1 = Maps.newHashMap();
//        map1.put("category", spAdvertisings.get(0));
//        list1.add(map1);
        for (SpVideoCategory spVideoCategory: list) {

            if (spCategory.getVideoCategoryWeight() == i){
                HashMap<Object, Object> map2 = Maps.newHashMap();
                Page<SpActor> page = new Page<SpActor>(1,10);
                List<SpActor> records = spActorService.page(page).getRecords();
                map2.put("actor",records);
                map2.put("category",spCategory);
                if (records.size()>0){


                    Page<SpVideo> page2 = new Page<SpVideo>(1,10);
                    wrapper.eq("actor_id",records.get(0).getActorId());
                    wrapper.eq("is_top",true);
                    wrapper.orderByDesc("is_top_date");
                    Page<SpVideo> page1 = spVideoService.page(page2, wrapper);
                    List<SpVideo> records2 = page1.getRecords();
                    map2.put("video",records2);
                }

                list1.add(map2);
                i++;
            }
            if (spVideoCategory.getSelected()){
                HashMap<Object, Object> map2 = Maps.newHashMap();
                Page<SpVideo> page = new Page<SpVideo>(1,10);
                wrapper.eq("is_top",true);
                wrapper.orderByDesc("is_top_date");
                Page<SpVideo> page1 = spVideoService.page(page, wrapper);
                List<SpVideo> records = page1.getRecords();
                map2.put("video",records);
                map2.put("category",spVideoCategory);
                list1.add(map2);
                i++;
            }
        }


        map.put("videos",list1);

//        dataInfo.setRows(homeCategoryList);
        return ResponseUtil.success(map);
    }


    /**
     * 导出分类子表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:category:export')")
    @Log(title = "分类子表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVideoCategory spVideoCategory) {
        List<SpVideoCategory> list = spVideoCategoryService.selectSpVideoCategoryList(spVideoCategory);
        ExcelUtil<SpVideoCategory> util = new ExcelUtil<SpVideoCategory>(SpVideoCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取分类子表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:category:query')")
    @GetMapping(value = "/{videoCategoryId}")
    public AjaxResult getInfo(@PathVariable("videoCategoryId") Long videoCategoryId) {
        return AjaxResult.success(spVideoCategoryService.selectSpVideoCategoryById(videoCategoryId));
    }

    /**
     * 新增分类子表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:category:add')")
    @Log(title = "分类子表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVideoCategory spVideoCategory) {
        return toAjax(spVideoCategoryService.insertSpVideoCategory(spVideoCategory));
    }

    /**
     * 修改分类子表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:category:edit')")
    @Log(title = "分类子表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVideoCategory spVideoCategory) {
        return toAjax(spVideoCategoryService.updateSpVideoCategory(spVideoCategory));
    }

    /**
     * 删除分类子表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:category:remove')")
    @Log(title = "分类子表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{videoCategoryIds}")
    public AjaxResult remove(@PathVariable Long[] videoCategoryIds) {
        List<SpVideo> spVideoList = new ArrayList<>();
        for (Long ids : videoCategoryIds) {
            if (spVideoService.selectSpVideoByCategoryId(ids) != null) {
                spVideoList = spVideoService.selectSpVideoByCategoryId(ids);
            }

        }
        if (spVideoList.size() > 0) {
            return AjaxResult.error("该分类下存在视频无法删除");
        } else {
            return toAjax(spVideoCategoryService.deleteSpVideoCategoryByIds(videoCategoryIds));
        }
    }
}
