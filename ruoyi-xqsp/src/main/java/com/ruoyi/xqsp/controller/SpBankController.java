package com.ruoyi.xqsp.controller;

import java.util.List;

import com.ruoyi.xqsp.domain.SpBanner;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpBank;
import com.ruoyi.xqsp.service.ISpBankService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 银行卡操作Controller
 * 
 * @author ruoyi
 * @date 2021-06-07
 */
@RestController
@RequestMapping("/xqsp/bank")
public class SpBankController extends BaseController
{
    @Autowired
    private ISpBankService spBankService;

    /**
     * 查询银行卡操作列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:bank:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpBank spBank)
    {
        startPage();
        List<SpBank> list = spBankService.selectSpBankList(spBank);
        return getDataTable(list);
    }

    /**
     * 导出银行卡操作列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:bank:export')")
    @Log(title = "银行卡操作", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpBank spBank)
    {
        List<SpBank> list = spBankService.selectSpBankList(spBank);
        ExcelUtil<SpBank> util = new ExcelUtil<SpBank>(SpBank.class);
        return util.exportExcel(list, "bank");
    }

    /**
     * 获取银行卡操作详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:bank:query')")
    @GetMapping(value = "/{spBankId}")
    public AjaxResult getInfo(@PathVariable("spBankId") Long spBankId)
    {
        return AjaxResult.success(spBankService.selectSpBankById(spBankId));
    }

    /**
     * 新增银行卡操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:bank:add')")
    @Log(title = "银行卡操作", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpBank spBank)
    {
        return toAjax(spBankService.insertSpBank(spBank));
    }

    /**
     * 修改银行卡操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:bank:edit')")
    @Log(title = "银行卡操作", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpBank spBank)
    {
        return toAjax(spBankService.updateSpBank(spBank));
    }

    /**
     * 删除银行卡操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:bank:remove')")
    @Log(title = "银行卡操作", businessType = BusinessType.DELETE)
	@DeleteMapping("/{spBankIds}")
    public AjaxResult remove(@PathVariable Long[] spBankIds)
    {
        return toAjax(spBankService.deleteSpBankByIds(spBankIds));
    }

    /**
     * 查询
     * @return
     */
    @GetMapping("/getBank")
    public AjaxResult getBank(){
        SpBank spBank=spBankService.selectSpBankById(1l);
        return AjaxResult.success(spBank);
    }
}
