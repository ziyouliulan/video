package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpMineImg;
import com.ruoyi.xqsp.service.ISpMineImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 我的广告Controller
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@RestController
@RequestMapping("/xqsp/mine_img")
public class SpMineImgController extends BaseController
{
    @Autowired
    private ISpMineImgService spMineImgService;

    /**
     * 查询我的广告列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:mine_img:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpMineImg spMineImg)
    {
        startPage();
        List<SpMineImg> list = spMineImgService.selectSpMineImgList(spMineImg);
        return getDataTable(list);
    }

    /**
     * 导出我的广告列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:mine_img:export')")
    @Log(title = "我的广告", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpMineImg spMineImg)
    {
        List<SpMineImg> list = spMineImgService.selectSpMineImgList(spMineImg);
        ExcelUtil<SpMineImg> util = new ExcelUtil<SpMineImg>(SpMineImg.class);
        return util.exportExcel(list, "mine_img");
    }

    /**
     * 获取我的广告详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:mine_img:query')")
    @GetMapping(value = "/{mineImgId}")
    public AjaxResult getInfo(@PathVariable("mineImgId") Long mineImgId)
    {
        return AjaxResult.success(spMineImgService.selectSpMineImgById(mineImgId));
    }

    /**
     * 新增我的广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:mine_img:add')")
    @Log(title = "我的广告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpMineImg spMineImg)
    {
        return toAjax(spMineImgService.insertSpMineImg(spMineImg));
    }

    /**
     * 修改我的广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:mine_img:edit')")
    @Log(title = "我的广告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpMineImg spMineImg)
    {
        return toAjax(spMineImgService.updateSpMineImg(spMineImg));
    }

    /**
     * 删除我的广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:mine_img:remove')")
    @Log(title = "我的广告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{mineImgIds}")
    public AjaxResult remove(@PathVariable Long[] mineImgIds)
    {
        return toAjax(spMineImgService.deleteSpMineImgByIds(mineImgIds));
    }

    /**
     * 我的页面广告位
     * @return
     */
    @GetMapping("/getMineImg")
    public AjaxResult getMineImg(){
        List<SpMineImg>list=spMineImgService.selectSpMineImgList(new SpMineImg());
        return AjaxResult.success(list);
    }
}
