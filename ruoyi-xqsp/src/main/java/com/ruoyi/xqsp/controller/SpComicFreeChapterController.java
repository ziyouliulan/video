package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicFreeChapter;
import com.ruoyi.xqsp.service.ISpComicFreeChapterService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 免费章节数Controller
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@RestController
@RequestMapping("/xqsp/comic_free_chapter")
public class SpComicFreeChapterController extends BaseController
{
    @Autowired
    private ISpComicFreeChapterService spComicFreeChapterService;

    /**
     * 查询免费章节数列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comic_free_chapter:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicFreeChapter spComicFreeChapter)
    {
        startPage();
        List<SpComicFreeChapter> list = spComicFreeChapterService.selectSpComicFreeChapterList(spComicFreeChapter);
        return getDataTable(list);
    }

    /**
     * 导出免费章节数列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comic_free_chapter:export')")
    @Log(title = "免费章节数", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicFreeChapter spComicFreeChapter)
    {
        List<SpComicFreeChapter> list = spComicFreeChapterService.selectSpComicFreeChapterList(spComicFreeChapter);
        ExcelUtil<SpComicFreeChapter> util = new ExcelUtil<SpComicFreeChapter>(SpComicFreeChapter.class);
        return util.exportExcel(list, "comic_free_chapter");
    }

    /**
     * 获取免费章节数详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comic_free_chapter:query')")
    @GetMapping(value = "/{comicFreeChapter}")
    public AjaxResult getInfo(@PathVariable("comicFreeChapter") Long comicFreeChapter)
    {
        return AjaxResult.success(spComicFreeChapterService.selectSpComicFreeChapterById(comicFreeChapter));
    }

    /**
     * 新增免费章节数
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comic_free_chapter:add')")
    @Log(title = "免费章节数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicFreeChapter spComicFreeChapter)
    {
        return toAjax(spComicFreeChapterService.insertSpComicFreeChapter(spComicFreeChapter));
    }

    /**
     * 修改免费章节数
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comic_free_chapter:edit')")
    @Log(title = "免费章节数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicFreeChapter spComicFreeChapter)
    {
        return toAjax(spComicFreeChapterService.updateSpComicFreeChapter(spComicFreeChapter));
    }

    /**
     * 删除免费章节数
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comic_free_chapter:remove')")
    @Log(title = "免费章节数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicFreeChapters}")
    public AjaxResult remove(@PathVariable Long[] comicFreeChapters)
    {
        return toAjax(spComicFreeChapterService.deleteSpComicFreeChapterByIds(comicFreeChapters));
    }

    @GetMapping("/getComicFreeChapter")
    public AjaxResult getComicFreeChapter(){
        return AjaxResult.success(spComicFreeChapterService.selectSpComicFreeChapterById(1l));
    }
}
