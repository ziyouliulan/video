package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.SpLike;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;
import com.ruoyi.xqsp.domain.entity.*;
import com.ruoyi.xqsp.service.ISpLikeService;
import com.ruoyi.xqsp.service.ISpUsersService;
import com.ruoyi.xqsp.service.ISpVarietyVideoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpVarietyVideoEvaluation;
import com.ruoyi.xqsp.service.ISpVarietyVideoEvaluationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 综艺视频评论Controller
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@RestController
@RequestMapping("/xqsp/varietyVideoEvaluation")
public class SpVarietyVideoEvaluationController extends BaseController
{
    @Autowired
    private ISpVarietyVideoEvaluationService spVarietyVideoEvaluationService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpLikeService spLikeService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;

    /**
     * 查询综艺视频评论列表
     */
    @GetMapping("/list")
    public ResponseUtil list(Long videoId,Integer userId,Integer pageNum ,Integer pageSize)
    {
        PageHelper.startPage(pageNum,pageSize);
        List<SpVarietyVideoEvaluation> spVarietyVideoEvaluations = spVarietyVideoEvaluationService.selectSpVarietyVideoEvaluationListV2(videoId.intValue());

        return ResponseUtil.success(PageInfo.of(spVarietyVideoEvaluations)) ;
    }

    /**
     * 获取综艺视频评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideoEvaluation:query')")
    @GetMapping(value = "/{varietyVideoEvaluationId}")
    public AjaxResult getInfo(@PathVariable("varietyVideoEvaluationId") Long varietyVideoEvaluationId)
    {
        return AjaxResult.success(spVarietyVideoEvaluationService.selectSpVarietyVideoEvaluationById(varietyVideoEvaluationId));
    }

    /**
     * 新增综艺视频评论
     */
    @PostMapping("add")
    public ResponseUtil add(SpVarietyVideoEvaluation spVarietyVideoEvaluation)
    {
        return ResponseUtil.success(spVarietyVideoEvaluationService.insertSpVarietyVideoEvaluation(spVarietyVideoEvaluation));
    }

    /**
     * 修改综艺视频评论
     */


    @PostMapping("edit")
    public ResponseUtil edit(SpVarietyVideoEvaluation spVarietyVideoEvaluation)
    {
        return ResponseUtil.success(spVarietyVideoEvaluationService.updateSpVarietyVideoEvaluation(spVarietyVideoEvaluation));
    }

    /**
     * 删除综艺视频评论
     */
	@PostMapping("/del")
    public ResponseUtil remove( Long varietyVideoEvaluationId)
    {
        return ResponseUtil.success(spVarietyVideoEvaluationService.deleteSpVarietyVideoEvaluationById(varietyVideoEvaluationId));
    }


    /**
     * 综艺视频详情页的综艺视频评论
     * @param homeVideo
     * @return
     */
    @PostMapping("/getVarietyEvaluationById")
    public TableDataInfo getVarietyEvaluationById(@RequestBody VarietyVideoEvaluationLike homeVideo){
        startPages(homeVideo.getPageNums(),homeVideo.getPageSize());
        List<VarietyVideoEvaluationLike>homeVideoList=new ArrayList<>();

        VarietyVideoEvaluationLike videoEvaluationLike;
        SpLike spLike;
        List<SpVarietyVideoEvaluation>spVideoEvaluationList=spVarietyVideoEvaluationService.selectSpVarietyVideoEvaluationByIds(homeVideo.getVarietyVideoId());
        TableDataInfo dataInfo=getDataTable(spVideoEvaluationList);
        for (SpVarietyVideoEvaluation evaluation:spVideoEvaluationList){
            videoEvaluationLike=new VarietyVideoEvaluationLike();
            videoEvaluationLike.setSpUsers(spUsersService.selectSpUserById(evaluation.getUserId()).getUserName());
            spLike=new SpLike();
            spLike.setUserId(homeVideo.getUserId());
            spLike.setLikeTypeId(evaluation.getVarietyVideoEvaluationId());
            spLike.setLikeType(7L);
            videoEvaluationLike.setSpVarietyVideoEvaluation(evaluation);
            if (spLikeService.selectSpLike(spLike)!=null){
                videoEvaluationLike.setLike(1);
            }else {
                videoEvaluationLike.setLike(0);
            }
            homeVideoList.add(videoEvaluationLike);
        }
        dataInfo.setRows(homeVideoList);

        return dataInfo;
    }


    /**
     * 点赞
     *
     */
    @PostMapping("/VarietyVideoEvaluationLike")
    public AjaxResult VarietyVideoEvaluationLike(@RequestBody UserLike userLike){
        SpLike spLike=new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVarietyVideoEvaluationId());
        spLike.setLikeType(7L);
        if (spLikeService.selectSpLike(spLike)==null){

            spVarietyVideoEvaluationService.updateSpVarietyVideoEvaluationLike(userLike.getVarietyVideoEvaluationId());
            return toAjax(spLikeService.insertSpLike(spLike));


        }else {

            spVarietyVideoEvaluationService.updateSpVarietyVideoEvaluationLike1(userLike.getVarietyVideoEvaluationId());


            return toAjax(spLikeService.deleteSpLike(spLike));
        }


    }

    /**
     * 新增视频评价
     */

    @Log(title = "综艺视频评价", businessType = BusinessType.INSERT)
    @PostMapping("/addVarietyVideoEvaluation")
    public AjaxResult addVarietyVideoEvaluation(@RequestBody SpVarietyVideoEvaluation spVarietyVideoEvaluation)
    {
        spVarietyVideoEvaluation.setVarietyVideoEvaluationTime(new Date());
        spVarietyVideoEvaluation.setVarietyVideoEvaluationLike(0l);
        return toAjax(spVarietyVideoEvaluationService.insertSpVarietyVideoEvaluation(spVarietyVideoEvaluation));
    }


}
