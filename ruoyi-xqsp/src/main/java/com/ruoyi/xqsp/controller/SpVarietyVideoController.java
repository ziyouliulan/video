package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.xqsp.domain.*;
import com.ruoyi.xqsp.domain.entity.*;
import com.ruoyi.xqsp.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 综艺视频Controller
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@RestController
@RequestMapping("/xqsp/varietyVideo")
public class SpVarietyVideoController extends BaseController {
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpVarietyService spVarietyService;
    @Autowired
    private ISpLikeService spLikeService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpUnlikeService spUnlikeService;
    @Autowired
    private ISpCollectService spCollectService;
    @Autowired
    private ISpVarietyVideoEvaluationService spVarietyVideoEvaluationService;
    @Autowired
    private ISpExpenseCalendarService spExpenseCalendarService;
    @Autowired
    private ISpVideoWatchHistoryService spVideoWatchHistoryService;
    @Autowired
    private ISpVideoCacheService spVideoCacheService;

    /**
     * 查询综艺视频列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVarietyVideo spVarietyVideo) {
        startPage();
        List<SpVarietyVideo> list = spVarietyVideoService.selectSpVarietyVideoList(spVarietyVideo);
        TableDataInfo dataTable = getDataTable(list);
        List<VarietyVideo> varietyVideoList = new ArrayList<>();
        VarietyVideo varietyVideo;
        for (SpVarietyVideo variety : list) {
            varietyVideo = new VarietyVideo();
            varietyVideo.setSpVarietyVideo(variety);
            if (variety.getVarietyId() != null) {
                if (spVarietyService.selectSpVarietyById(variety.getVarietyId()) != null) {
                    varietyVideo.setVarietyName(spVarietyService.selectSpVarietyById(variety.getVarietyId()).getVarietyName());
                }
            } else {
                varietyVideo.setVarietyName("该视频未归属综艺");
            }
            varietyVideoList.add(varietyVideo);
        }
        dataTable.setRows(varietyVideoList);
        return dataTable;
    }


    /**
     * 导出综艺视频列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:export')")
    @Log(title = "综艺视频", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVarietyVideo spVarietyVideo) {
        List<SpVarietyVideo> list = spVarietyVideoService.selectSpVarietyVideoList(spVarietyVideo);
        ExcelUtil<SpVarietyVideo> util = new ExcelUtil<SpVarietyVideo>(SpVarietyVideo.class);
        return util.exportExcel(list, "varietyVideo");
    }

    /**
     * 获取综艺视频详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:query')")
    @GetMapping(value = "/varietyId/{varietyId}")
    public TableDataInfo getInfoByVarietyId(@PathVariable("varietyId") Long varietyId) {
        startPage();
        List<SpVarietyVideo> list = spVarietyVideoService.selectSpVarietyVideoByVarietyIds(varietyId);
        return getDataTable(list);
    }

    /**
     * 获取综艺视频根据综艺id
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:query')")
    @GetMapping(value = "/{varietyVideoId}")
    public AjaxResult getInfo(@PathVariable("varietyVideoId") Long varietyVideoId) {
        return AjaxResult.success(spVarietyVideoService.selectSpVarietyVideoById(varietyVideoId));
    }

    /**
     * 新增综艺视频
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:add')")
    @Log(title = "综艺视频", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVarietyVideo spVarietyVideo) {
        spVarietyVideo.setVarietyVideoTime(new Date());
        return toAjax(spVarietyVideoService.insertSpVarietyVideo(spVarietyVideo));
    }

    /**
     * 修改综艺视频
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:edit')")
    @Log(title = "综艺视频", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVarietyVideo spVarietyVideo) {
        return toAjax(spVarietyVideoService.updateSpVarietyVideo(spVarietyVideo));
    }

    /**
     * 删除综艺视频
     */
    @PreAuthorize("@ss.hasPermi('xqsp:varietyVideo:remove')")
    @Log(title = "综艺视频", businessType = BusinessType.DELETE)
    @DeleteMapping("/{varietyVideoIds}")
    public AjaxResult remove(@PathVariable Long[] varietyVideoIds) {
        Long type = 3l;
        Long type1 = 2l;
        for (Long videoId : varietyVideoIds) {
            spCollectService.deleteSpCollect1(videoId, type);
            spVideoWatchHistoryService.deleteSpVideoWatchHistory(videoId, type1);
            spVideoCacheService.deleteSpVideoCacheBy(videoId, type1);
            spVarietyVideoEvaluationService.deleteSpVarietyVideoEvaluationByVvId(videoId);
        }
        return toAjax(spVarietyVideoService.deleteSpVarietyVideoByIds(varietyVideoIds));
    }


    /**
     * 点赞
     */
    @PostMapping("/varietyVideoLike")
    public AjaxResult videoLike(@RequestBody UserLike userLike) {
        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVarietyVideoId());
        spLike.setLikeType(6L);
        SpUnlike spUnlike = new SpUnlike();
        spUnlike.setUserId(userLike.getUserId());
        spUnlike.setUnlikeTypeId(userLike.getVarietyVideoId());
        spUnlike.setUnlikeType(6L);

        if (spLikeService.selectSpLike(spLike) == null) {
            if (spUnlikeService.selectSpUnlikeList(spUnlike).size() == 0) {
                spVarietyVideoService.updateVideoLikeNumber(userLike.getVarietyVideoId());
                return toAjax(spLikeService.insertSpLike(spLike));
            } else {
                if (spVarietyVideoService.selectSpVarietyVideoById(userLike.getVarietyVideoId()).getVarietyVideoUnlikeNumber() > 0) {
                    spVarietyVideoService.updateVideoUnLikeNumber1(userLike.getVarietyVideoId());
                }
                spUnlikeService.deleteVarietyVideoUnlike(spUnlike);
                spVarietyVideoService.updateVideoLikeNumber(userLike.getVarietyVideoId());
                return toAjax(spLikeService.insertSpLike(spLike));
            }

        } else {
            if (spVarietyVideoService.selectSpVarietyVideoById(userLike.getVarietyVideoId()).getVarietyVideoLikeNumber() > 0) {
                spVarietyVideoService.updateVideoLikeNumber1(userLike.getVarietyVideoId());
            }

            return toAjax(spLikeService.deleteSpLike(spLike));
        }


    }

    /**
     * 差评
     */
    @PostMapping("/varietyVideoUnLike")
    public AjaxResult videoUnLike(@RequestBody UserLike userLike) {
        SpUnlike spUnlike = new SpUnlike();
        spUnlike.setUserId(userLike.getUserId());
        spUnlike.setUnlikeTypeId(userLike.getVarietyVideoId());
        spUnlike.setUnlikeType(1L);

        SpLike spLike = new SpLike();
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeTypeId(userLike.getVarietyVideoId());
        spLike.setLikeType(6L);

        if (spUnlikeService.selectSpUnlikeList(spUnlike).size() == 0) {
            if (spLikeService.selectSpLike(spLike) == null) {
                spVarietyVideoService.updateVideoUnLikeNumber(userLike.getVarietyVideoId());
                return toAjax(spUnlikeService.insertSpUnlike(spUnlike));
            } else {
                if (spVarietyVideoService.selectSpVarietyVideoById(userLike.getVarietyVideoId()).getVarietyVideoLikeNumber() > 0) {
                    spVarietyVideoService.updateVideoLikeNumber1(userLike.getVarietyVideoId());
                }
                spLikeService.deleteSpLike(spLike);
                spVarietyVideoService.updateVideoUnLikeNumber(userLike.getVarietyVideoId());
                return toAjax(spUnlikeService.insertSpUnlike(spUnlike));
            }

        } else {
            if (spVarietyVideoService.selectSpVarietyVideoById(userLike.getVarietyVideoId()).getVarietyVideoUnlikeNumber() > 0) {
                spVarietyVideoService.updateVideoUnLikeNumber1(userLike.getVarietyVideoId());
            }
            return toAjax(spUnlikeService.deleteVarietyVideoUnlike(spUnlike));


        }


    }

    /**
     * 视频收藏
     *
     * @param userLike
     * @return
     */
    @PostMapping("/varietyVideoCollect")
    public AjaxResult videoCollect(@RequestBody UserLike userLike) {
        SpCollect spCollect = new SpCollect();
        spCollect.setUserId(userLike.getUserId());
        spCollect.setCollectTypeId(userLike.getVarietyVideoId());
        spCollect.setCollectType(3l);
        spCollect.setCollectTime(new Date());
        if (spCollectService.selectSpCollectList(spCollect).size() == 0) {
            spVarietyVideoService.updateVideoCollectNumber(userLike.getVarietyVideoId());
            return toAjax(spCollectService.insertSpCollect(spCollect));
        } else {
            spVarietyVideoService.updateVideoCollectNumber1(userLike.getVarietyVideoId());
            return toAjax(spCollectService.deleteSpCollect(spCollect));
        }
    }


    /**
     * 综艺视频详情
     *
     * @param homeVideo
     * @return
     */
    @PostMapping("/varietyVideoGetAll")
    public AjaxResult getAll(@RequestBody HomeVarietyVideoAll homeVideo) {
        HomeVarietyVideoAll homeVideo1 = new HomeVarietyVideoAll();
        homeVideo1.setSpVarietyVideo(spVarietyVideoService.selectSpVarietyVideoById(homeVideo.getVarietyVideoId()));
        homeVideo1.setSpUsers(spUsersService.selectSpUsersById(homeVideo.getUserId()));
        List<SpVarietyVideo> spVarietyVideoList = spVarietyVideoService.selectSpVarietyVideo();
        homeVideo1.setSpVarietyVideoList(spVarietyVideoList);
        homeVideo1.setVideoEvaluationCounts(spVarietyVideoEvaluationService.selectCountById(homeVideo.getVarietyVideoId()));
        if (spLikeService.selectSpLikeByVarietyVideoId(homeVideo.getUserId(), homeVideo.getVarietyVideoId()) != null) {
            homeVideo1.setSpLike(1L);//已点赞
        } else {
            homeVideo1.setSpLike(0l);//位点赞
        }
        if (spUnlikeService.selectSpUnlikeByVarietyVideoId(homeVideo.getUserId(), homeVideo.getVarietyVideoId()) != null) {
            homeVideo1.setSpUnlike(1l);//已差评
        } else {
            homeVideo1.setSpUnlike(0l);//为差评
        }
        if (spCollectService.selectSpCollectByVarietyVideoId(homeVideo.getUserId(), homeVideo.getVarietyVideoId()) != null) {
            homeVideo1.setSpCollect(1l);//已收藏
        } else {
            homeVideo1.setSpCollect(0l);//未收藏
        }
        SpExpenseCalendar spExpenseCalendar = new SpExpenseCalendar();
        spExpenseCalendar.setExpenseCalendarState(homeVideo.getVarietyVideoId());
        spExpenseCalendar.setExpenseCalendarWay(4);
        spExpenseCalendar.setUserId(homeVideo.getUserId());
        if (spExpenseCalendarService.selectSpExpenseCalendarList(spExpenseCalendar).size() > 0) {
            homeVideo1.setSpUnlock(1l);//已解锁
        } else {
            homeVideo1.setSpUnlock(0l);//未解锁
        }
        return AjaxResult.success(homeVideo1);
    }


    /**
     * 排序
     *
     * @param videoSort
     * @return
     */
    @PostMapping("/getSort")
    public TableDataInfo getSort(@RequestBody VideoSort videoSort) {

        startPages(videoSort.getPageNums(), videoSort.getPageSize());

        List<SpVarietyVideo> spVarietyVideoList = new ArrayList<>();

        SpVarietyVideo spVarietyVideo = new SpVarietyVideo();
        spVarietyVideo.setVarietyId(videoSort.getVarietyId());
        if (videoSort.getSort() == 1) {
            spVarietyVideoList = spVarietyVideoService.selectSpVarietyVideoLists(spVarietyVideo);
        }
        if (videoSort.getSort() == 2) {
            spVarietyVideoList = spVarietyVideoService.selectSpVarietyVideoListByDate(spVarietyVideo);
        }
        if (videoSort.getSort() == 3) {
            spVarietyVideoList = spVarietyVideoService.selectSpVarietyVideoListByWatch(spVarietyVideo);
        }

        for (SpVarietyVideo varietyVideo : spVarietyVideoList) {
            if (varietyVideo.getVarietyVideoUnlock() != null && varietyVideo.getVarietyVideoUnlock() == 1) {
                Integer count = spExpenseCalendarService.selectSpExpenseCalendarByCount(varietyVideo.getVarietyId(), 4);//解锁人数
                varietyVideo.setUnlockCount(count);
            }
        }

        return getDataTable(spVarietyVideoList);

    }


}
