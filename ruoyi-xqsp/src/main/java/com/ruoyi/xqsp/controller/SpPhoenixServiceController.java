package com.ruoyi.xqsp.controller;

import java.util.*;

import com.ruoyi.xqsp.domain.*;
import com.ruoyi.xqsp.domain.entity.*;
import com.ruoyi.xqsp.service.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 楼凤操作Controller
 *
 * @author ruoyi
 * @date 2021-04-26
 */
@RestController
@RequestMapping("/xqsp/service")
public class SpPhoenixServiceController extends BaseController {
    @Autowired
    private ISpPhoenixServiceService spPhoenixServiceService;
    @Autowired
    private ISpCollectService spCollectService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpAdvertisingService spAdvertisingService;
    @Autowired
    private ISpExpenseCalendarService spExpenseCalendarService;
    @Autowired
    private ISpPhoenixEvaluationService spPhoenixEvaluationService;
    @Autowired
    private IPhoenixReportService phoenixReportService;

    /**
     * 查询楼凤操作列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpPhoenixService spPhoenixService) {
        startPage();
        List<SpPhoenixService> list = spPhoenixServiceService.selectSpPhoenixServiceList(spPhoenixService);
        return getDataTable(list);
    }

    /**
     * 查询楼凤审核列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:list')")
    @GetMapping("/list/audit")
    public TableDataInfo listAudit(SpPhoenixService spPhoenixService) {
        startPage();
        List<SpPhoenixService> list = spPhoenixServiceService.selectSpPhoenixServiceListAudit(spPhoenixService);
        return getDataTable(list);
    }

    /**
     * 导出楼凤操作列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:export')")
    @Log(title = "楼凤操作", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpPhoenixService spPhoenixService) {
        List<SpPhoenixService> list = spPhoenixServiceService.selectSpPhoenixServiceList(spPhoenixService);
        ExcelUtil<SpPhoenixService> util = new ExcelUtil<SpPhoenixService>(SpPhoenixService.class);
        return util.exportExcel(list, "service");
    }

    /**
     * 获取楼凤操作详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:query')")
    @GetMapping(value = "/{phoenixId}")
    public AjaxResult getInfo(@PathVariable("phoenixId") Long phoenixId) {
        return AjaxResult.success(spPhoenixServiceService.selectSpPhoenixServiceById(phoenixId));
    }

    /**
     * 新增楼凤操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:add')")
    @Log(title = "楼凤操作", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpPhoenixService spPhoenixService) {
        return toAjax(spPhoenixServiceService.insertSpPhoenixService(spPhoenixService));
    }

    /**
     * 修改楼凤操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:edit')")
    @Log(title = "楼凤操作", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpPhoenixService spPhoenixService) {
        phoenixReportService.updatePhoenixReportByPhoenixId(spPhoenixService.getPhoenixId(), spPhoenixService.getPhoenixAudit());
        return toAjax(spPhoenixServiceService.updateSpPhoenixService(spPhoenixService));
    }

    /**
     * 删除楼凤操作
     */
    @PreAuthorize("@ss.hasPermi('xqsp:service:remove')")
    @Log(title = "楼凤操作", businessType = BusinessType.DELETE)
    @DeleteMapping("/{phoenixIds}")
    public AjaxResult remove(@PathVariable Long[] phoenixIds) {
        Long type = 2l;
        for (Long phoenixId : phoenixIds) {
            spCollectService.deleteSpCollect1(phoenixId, type);
            spPhoenixEvaluationService.deleteSpPhoenixEvaluationByPhoenixId(phoenixId);
        }
        return toAjax(spPhoenixServiceService.deleteSpPhoenixServiceByIds(phoenixIds));
    }


    /**
     * 漏风首页
     *
     * @return
     */
    @PostMapping("/getAllPhoenix")
    public TableDataInfo getAllPhoenix(@RequestBody PhoenixSort phoenixSort) {
        startPages(phoenixSort.getPageNums(), phoenixSort.getPageSize());
        if (phoenixSort.getType() == 1) {// 精选
            if (phoenixSort.getArea().equals("全国")) {
                List<SpPhoenixService> spPhoenixServiceList = spPhoenixServiceService.selectSpPhoenixService();
                return getDataTable(spPhoenixServiceList);
            } else {
                List<SpPhoenixService> spPhoenixServiceList = spPhoenixServiceService.selectSpPhoenixServiceByAddress(phoenixSort.getArea());
                return getDataTable(spPhoenixServiceList);
            }
        }
        if (phoenixSort.getType() == 2) {//最新
            if (phoenixSort.getArea().equals("全国")) {
                List<SpPhoenixService> spPhoenixEvaluationList = spPhoenixServiceService.selectSpPhoenixServiceOrderTime();
                return getDataTable(spPhoenixEvaluationList);
            } else {
                List<SpPhoenixService> spPhoenixServiceList = spPhoenixServiceService.selectSpPhoenixServiceByAddressOrderTime(phoenixSort.getArea());
                return getDataTable(spPhoenixServiceList);
            }

        }
        List<SpPhoenixService> spPhoenixServiceList = spPhoenixServiceService.selectSpPhoenixService();
        return getDataTable(spPhoenixServiceList);
    }

    /**
     * 根据id查询楼凤
     *
     * @param phoenixCalendar
     * @return
     */
    @PostMapping("/home/phoenixId")
    public AjaxResult getPhoenixById(@RequestBody PhoenixCalendar phoenixCalendar) {

        PhoenixCalendar phoenixCalendar1 = new PhoenixCalendar();
        phoenixCalendar1.setSpPhoenixService(spPhoenixServiceService.selectSpPhoenixServiceById(phoenixCalendar.getPhoenixId()));

        SpExpenseCalendar spExpenseCalendar = new SpExpenseCalendar();
        spExpenseCalendar.setUserId(phoenixCalendar.getUserId());
        spExpenseCalendar.setExpenseCalendarState(phoenixCalendar.getPhoenixId());
        spExpenseCalendar.setExpenseCalendarWay(3);
        if (spExpenseCalendarService.selectSpExpenseCalendarList(spExpenseCalendar).size() == 0) {
            phoenixCalendar1.setUnlock(0l);//未解锁
        }
        if (spExpenseCalendarService.selectSpExpenseCalendarList(spExpenseCalendar).size() > 0) {
            phoenixCalendar1.setUnlock(1l);//已解锁
        }
        SpCollect spCollect = new SpCollect();
        spCollect.setUserId(phoenixCalendar.getUserId());
        spCollect.setCollectTypeId(phoenixCalendar.getPhoenixId());
        spCollect.setCollectType(2l);
        if (spCollectService.selectSpCollectList(spCollect).size() == 0) {
            phoenixCalendar1.setCollect(0l);//未收藏
        }
        if (spCollectService.selectSpCollectList(spCollect).size() > 0) {
            phoenixCalendar1.setCollect(1l);//已收藏
        }

        SpUsers users = spUsersService.selectSpUsersById(phoenixCalendar.getUserId());
        if (users != null) {
            if (users.getUserVipType() == 1 ||users.getUserVipType() == 2)
                phoenixCalendar1.setUnlock(1l);//已解锁
        }

        return AjaxResult.success(phoenixCalendar1);
    }

    /**
     * 楼凤收藏
     *
     * @param userLike
     * @return
     */
    @PostMapping("/phoenixCollect")
    public AjaxResult phoenixCollect(@RequestBody UserLike userLike) {
        SpCollect spCollect = new SpCollect();
        spCollect.setUserId(userLike.getUserId());
        spCollect.setCollectTypeId(userLike.getPhoenixId());
        spCollect.setCollectType(2l);
        spCollect.setCollectTime(new Date());
        if (spCollectService.selectSpCollectList(spCollect).size() == 0) {
            spPhoenixServiceService.updateSpPhoenixServiceLike(userLike.getPhoenixId());
            return toAjax(spCollectService.insertSpCollect(spCollect));
        } else {
            spPhoenixServiceService.updateSpPhoenixServiceLike1(userLike.getPhoenixId());
            return toAjax(spCollectService.deleteSpCollect(spCollect));
        }
    }

    /**
     * 楼凤页banner图
     *
     * @return
     */
    @GetMapping("/phoenixBanner")
    public AjaxResult phoenixBanner() {
        return AjaxResult.success(spAdvertisingService.selectSpAdvertisingPhoenix());
    }


    /**
     * 新增楼凤操作
     */

    @Log(title = "楼凤操作", businessType = BusinessType.INSERT)
    @PostMapping("/addPhoenix")
    public AjaxResult addPhoenix(@RequestBody SpPhoenixService spPhoenixService) {
        spPhoenixService.setPhoenixTime(new Date());
        return toAjax(spPhoenixServiceService.insertSpPhoenixService(spPhoenixService));
    }

    /**
     * 根据名字模糊查询
     *
     * @param phoenixName
     * @return
     */
    @GetMapping("/getPhoenixByName")
    public TableDataInfo getPhoenixByName(@RequestParam(value = "phoenixName", required = false) String phoenixName) {
        startPage();
        List<SpPhoenixService> spPhoenixServiceList = spPhoenixServiceService.selectSpPhoenixServiceByName(phoenixName);
        return getDataTable(spPhoenixServiceList);
    }

    /**
     * 查看用户的楼凤收藏和已解锁
     *
     * @return
     */
    @PostMapping("/getPhoenixCollect")
    public TableDataInfo getPhoenixCollect(@RequestBody PhoenixCollect phoenixCollect) {

        // List<PhoenixCollect>phoenixCollectList=new ArrayList<>();
        Set<Long> ids1 = new HashSet<>();

        SpCollect spCollect = new SpCollect();
        spCollect.setUserId(phoenixCollect.getUserId());
        spCollect.setCollectType(2l);
        List<SpCollect> spCollectList = spCollectService.selectSpCollectList(spCollect);

        for (SpCollect collect : spCollectList) {
            Long ids = collect.getCollectTypeId();
            ids1.add(ids);

        }
        // List<PhoenixCollect>phoenixCollectList2=new ArrayList<>();

        SpExpenseCalendar spExpenseCalendar = new SpExpenseCalendar();
        spExpenseCalendar.setUserId(phoenixCollect.getUserId());
        spExpenseCalendar.setExpenseCalendarWay(3);
        List<SpExpenseCalendar> spExpenseCalendarList = spExpenseCalendarService.selectSpExpenseCalendarList(spExpenseCalendar);
        for (SpExpenseCalendar calendar : spExpenseCalendarList) {
            Long ids = calendar.getExpenseCalendarState();
            ids1.add(ids);
        }
        System.out.println(ids1);
        startPages(phoenixCollect.getPageNums(), phoenixCollect.getPageSize());
        List<SpPhoenixService> spPhoenixServiceList = spPhoenixServiceService.selectSpPhoenixServiceListByIds(ids1);
        return getDataTable(spPhoenixServiceList);
    }

}
