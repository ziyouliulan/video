package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpWxpay;
import com.ruoyi.xqsp.service.ISpWxpayService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 微信支付Controller
 * 
 * @author ruoyi
 * @date 2021-06-01
 */
@RestController
@RequestMapping("/xqsp/wxpay")
public class SpWxpayController extends BaseController
{
    @Autowired
    private ISpWxpayService spWxpayService;

    /**
     * 查询微信支付列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:wxpay:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpWxpay spWxpay)
    {
        startPage();
        List<SpWxpay> list = spWxpayService.selectSpWxpayList(spWxpay);
        return getDataTable(list);
    }

    /**
     * 导出微信支付列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:wxpay:export')")
    @Log(title = "微信支付", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpWxpay spWxpay)
    {
        List<SpWxpay> list = spWxpayService.selectSpWxpayList(spWxpay);
        ExcelUtil<SpWxpay> util = new ExcelUtil<SpWxpay>(SpWxpay.class);
        return util.exportExcel(list, "wxpay");
    }

    /**
     * 获取微信支付详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:wxpay:query')")
    @GetMapping(value = "/{wxpayId}")
    public AjaxResult getInfo(@PathVariable("wxpayId") Long wxpayId)
    {
        return AjaxResult.success(spWxpayService.selectSpWxpayById(wxpayId));
    }

    /**
     * 新增微信支付
     */
    @PreAuthorize("@ss.hasPermi('xqsp:wxpay:add')")
    @Log(title = "微信支付", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpWxpay spWxpay)
    {
        return toAjax(spWxpayService.insertSpWxpay(spWxpay));
    }

    /**
     * 修改微信支付
     */
    @PreAuthorize("@ss.hasPermi('xqsp:wxpay:edit')")
    @Log(title = "微信支付", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpWxpay spWxpay)
    {
        return toAjax(spWxpayService.updateSpWxpay(spWxpay));
    }

    /**
     * 删除微信支付
     */
    @PreAuthorize("@ss.hasPermi('xqsp:wxpay:remove')")
    @Log(title = "微信支付", businessType = BusinessType.DELETE)
	@DeleteMapping("/{wxpayIds}")
    public AjaxResult remove(@PathVariable Long[] wxpayIds)
    {
        return toAjax(spWxpayService.deleteSpWxpayByIds(wxpayIds));
    }
}
