package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpOpenImg;
import com.ruoyi.xqsp.service.ISpOpenImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 开屏广告Controller
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@RestController
@RequestMapping("/xqsp/open_img")
public class SpOpenImgController extends BaseController
{
    @Autowired
    private ISpOpenImgService spOpenImgService;

    /**
     * 查询开屏广告列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:open_img:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpOpenImg spOpenImg)
    {
        startPage();
        List<SpOpenImg> list = spOpenImgService.selectSpOpenImgList(spOpenImg);
        return getDataTable(list);
    }

    /**
     * 导出开屏广告列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:open_img:export')")
    @Log(title = "开屏广告", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpOpenImg spOpenImg)
    {
        List<SpOpenImg> list = spOpenImgService.selectSpOpenImgList(spOpenImg);
        ExcelUtil<SpOpenImg> util = new ExcelUtil<SpOpenImg>(SpOpenImg.class);
        return util.exportExcel(list, "open_img");
    }

    /**
     * 获取开屏广告详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:open_img:query')")
    @GetMapping(value = "/{openImgId}")
    public AjaxResult getInfo(@PathVariable("openImgId") Long openImgId)
    {
        return AjaxResult.success(spOpenImgService.selectSpOpenImgById(openImgId));
    }

    /**
     * 新增开屏广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:open_img:add')")
    @Log(title = "开屏广告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpOpenImg spOpenImg)
    {
        return toAjax(spOpenImgService.insertSpOpenImg(spOpenImg));
    }

    /**
     * 修改开屏广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:open_img:edit')")
    @Log(title = "开屏广告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpOpenImg spOpenImg)
    {
        return toAjax(spOpenImgService.updateSpOpenImg(spOpenImg));
    }

    /**
     * 删除开屏广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:open_img:remove')")
    @Log(title = "开屏广告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{openImgIds}")
    public AjaxResult remove(@PathVariable Long[] openImgIds)
    {
        return toAjax(spOpenImgService.deleteSpOpenImgByIds(openImgIds));
    }
    @GetMapping("/getOpenImg")
    public AjaxResult getOpenImg(){
        return AjaxResult.success(spOpenImgService.selectSpOpenImgById(1l));
    }
}
