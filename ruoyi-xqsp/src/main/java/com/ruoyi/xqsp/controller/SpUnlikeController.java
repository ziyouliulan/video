package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpUnlike;
import com.ruoyi.xqsp.service.ISpUnlikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 差评Controller
 * 
 * @author ruoyi
 * @date 2021-05-08
 */
@RestController
@RequestMapping("/xqsp/unlike")
public class SpUnlikeController extends BaseController
{
    @Autowired
    private ISpUnlikeService spUnlikeService;

    /**
     * 查询差评列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:unlike:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpUnlike spUnlike)
    {
        startPage();
        List<SpUnlike> list = spUnlikeService.selectSpUnlikeList(spUnlike);
        return getDataTable(list);
    }

    /**
     * 导出差评列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:unlike:export')")
    @Log(title = "差评", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpUnlike spUnlike)
    {
        List<SpUnlike> list = spUnlikeService.selectSpUnlikeList(spUnlike);
        ExcelUtil<SpUnlike> util = new ExcelUtil<SpUnlike>(SpUnlike.class);
        return util.exportExcel(list, "unlike");
    }

    /**
     * 获取差评详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:unlike:query')")
    @GetMapping(value = "/{unlikeId}")
    public AjaxResult getInfo(@PathVariable("unlikeId") Long unlikeId)
    {
        return AjaxResult.success(spUnlikeService.selectSpUnlikeById(unlikeId));
    }

    /**
     * 新增差评
     */
    @PreAuthorize("@ss.hasPermi('xqsp:unlike:add')")
    @Log(title = "差评", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpUnlike spUnlike)
    {
        return toAjax(spUnlikeService.insertSpUnlike(spUnlike));
    }

    /**
     * 修改差评
     */
    @PreAuthorize("@ss.hasPermi('xqsp:unlike:edit')")
    @Log(title = "差评", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpUnlike spUnlike)
    {
        return toAjax(spUnlikeService.updateSpUnlike(spUnlike));
    }

    /**
     * 删除差评
     */
    @PreAuthorize("@ss.hasPermi('xqsp:unlike:remove')")
    @Log(title = "差评", businessType = BusinessType.DELETE)
	@DeleteMapping("/{unlikeIds}")
    public AjaxResult remove(@PathVariable Long[] unlikeIds)
    {
        return toAjax(spUnlikeService.deleteSpUnlikeByIds(unlikeIds));
    }
}
