package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.xqsp.domain.entity.UserPhoenixReport;
import com.ruoyi.xqsp.service.ISpPhoenixServiceService;
import com.ruoyi.xqsp.service.ISpUsersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.PhoenixReport;
import com.ruoyi.xqsp.service.IPhoenixReportService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 楼凤举报Controller
 * 
 * @author ruoyi
 * @date 2021-05-21
 */
@RestController
@RequestMapping("/xqsp/phoenixreport")
public class PhoenixReportController extends BaseController
{
    @Autowired
    private IPhoenixReportService phoenixReportService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpPhoenixServiceService spPhoenixServiceService;

    /**
     * 查询楼凤举报列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixreport:list')")
    @GetMapping("/list")
    public TableDataInfo list(PhoenixReport phoenixReport)
    {
        startPage();
        List<PhoenixReport> list = phoenixReportService.selectPhoenixReportList(phoenixReport);
        TableDataInfo dataInfo=getDataTable(list);
        List<UserPhoenixReport>userPhoenixReportList=new ArrayList<>();
        UserPhoenixReport userPhoenixReport;
        for (PhoenixReport report:list){
            userPhoenixReport=new UserPhoenixReport();
            userPhoenixReport.setPhoenixReport(report);
            if (report.getUserId()!=null){
                if (spUsersService.selectSpUsersById(report.getUserId())!=null){
                    userPhoenixReport.setUserName( spUsersService.selectSpUsersById(report.getUserId()).getUserName());
                }

            }
            if (report.getPhoenixId()!=null){
                if (spPhoenixServiceService.selectSpPhoenixServiceById(report.getPhoenixId())!=null){
                    userPhoenixReport.setPhoenixName(spPhoenixServiceService.selectSpPhoenixServiceById(report.getPhoenixId()).getPhoenixTitle());
                }

            }
            userPhoenixReportList.add(userPhoenixReport);
        }
        dataInfo.setRows(userPhoenixReportList);
        return dataInfo;
    }

    /**
     * 导出楼凤举报列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixreport:export')")
    @Log(title = "楼凤举报", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PhoenixReport phoenixReport)
    {
        List<PhoenixReport> list = phoenixReportService.selectPhoenixReportList(phoenixReport);
        ExcelUtil<PhoenixReport> util = new ExcelUtil<PhoenixReport>(PhoenixReport.class);
        return util.exportExcel(list, "phoenixreport");
    }

    /**
     * 获取楼凤举报详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixreport:query')")
    @GetMapping(value = "/{reportId}")
    public AjaxResult getInfo(@PathVariable("reportId") Long reportId)
    {
        return AjaxResult.success(phoenixReportService.selectPhoenixReportById(reportId));
    }

    /**
     * 新增楼凤举报
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixreport:add')")
    @Log(title = "楼凤举报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PhoenixReport phoenixReport)
    {
        return toAjax(phoenixReportService.insertPhoenixReport(phoenixReport));
    }

    /**
     * 修改楼凤举报
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixreport:edit')")
    @Log(title = "楼凤举报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PhoenixReport phoenixReport)
    {
        return toAjax(phoenixReportService.updatePhoenixReport(phoenixReport));
    }

    /**
     * 删除楼凤举报
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixreport:remove')")
    @Log(title = "楼凤举报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{reportIds}")
    public AjaxResult remove(@PathVariable Long[] reportIds)
    {
        return toAjax(phoenixReportService.deletePhoenixReportByIds(reportIds));
    }


    /**
     * 新增楼凤举报
     */

    @Log(title = "楼凤举报", businessType = BusinessType.INSERT)
    @PostMapping("/addReport")
    public AjaxResult addReport(@RequestBody PhoenixReport phoenixReport)
    {

       Long state=Long.valueOf(spPhoenixServiceService.selectSpPhoenixServiceById(phoenixReport.getPhoenixId()).getPhoenixState());
        phoenixReport.setPhoenixState(state);
        return toAjax(phoenixReportService.insertPhoenixReport(phoenixReport));
    }

}
