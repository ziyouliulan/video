package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicBookmark;
import com.ruoyi.xqsp.service.ISpComicBookmarkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 漫画书签Controller
 * 
 * @author ruoyi
 * @date 2021-05-22
 */
@RestController
@RequestMapping("/xqsp/comicbookmark")
public class SpComicBookmarkController extends BaseController
{
    @Autowired
    private ISpComicBookmarkService spComicBookmarkService;

    /**
     * 查询漫画书签列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicbookmark:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicBookmark spComicBookmark)
    {
        startPage();
        List<SpComicBookmark> list = spComicBookmarkService.selectSpComicBookmarkList(spComicBookmark);
        return getDataTable(list);
    }

    /**
     * 导出漫画书签列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicbookmark:export')")
    @Log(title = "漫画书签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicBookmark spComicBookmark)
    {
        List<SpComicBookmark> list = spComicBookmarkService.selectSpComicBookmarkList(spComicBookmark);
        ExcelUtil<SpComicBookmark> util = new ExcelUtil<SpComicBookmark>(SpComicBookmark.class);
        return util.exportExcel(list, "comicbookmark");
    }

    /**
     * 获取漫画书签详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicbookmark:query')")
    @GetMapping(value = "/{comicBookmarkId}")
    public AjaxResult getInfo(@PathVariable("comicBookmarkId") Long comicBookmarkId)
    {
        return AjaxResult.success(spComicBookmarkService.selectSpComicBookmarkById(comicBookmarkId));
    }

    /**
     * 新增漫画书签
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicbookmark:add')")
    @Log(title = "漫画书签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicBookmark spComicBookmark)
    {
        return toAjax(spComicBookmarkService.insertSpComicBookmark(spComicBookmark));
    }

    /**
     * 修改漫画书签
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicbookmark:edit')")
    @Log(title = "漫画书签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicBookmark spComicBookmark)
    {
        return toAjax(spComicBookmarkService.updateSpComicBookmark(spComicBookmark));
    }

    /**
     * 删除漫画书签
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicbookmark:remove')")
    @Log(title = "漫画书签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicBookmarkIds}")
    public AjaxResult remove(@PathVariable Long[] comicBookmarkIds)
    {
        return toAjax(spComicBookmarkService.deleteSpComicBookmarkByIds(comicBookmarkIds));
    }
}
