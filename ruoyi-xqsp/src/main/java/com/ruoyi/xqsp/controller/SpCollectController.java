package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.entity.DeleteCollects;
import com.ruoyi.xqsp.domain.entity.VarietyVideoAndVideo;
import com.ruoyi.xqsp.service.*;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpCollect;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户收藏Controller
 *
 * @author ruoyi
 * @date 2021-05-08
 */
@RestController
@RequestMapping("/xqsp/collect")
public class SpCollectController extends BaseController
{
    @Autowired
    private ISpCollectService spCollectService;
    @Autowired
    private ISpVideoService spVideoService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpActorService spActorService;
    @Autowired
    private ISpVarietyService spVarietyService;

    @Autowired
    ISpComicService spComicService;

    /**
     * 查询用户收藏列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:collect:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpCollect spCollect)
    {
        startPage();
        List<SpCollect> list = spCollectService.selectSpCollectList(spCollect);
        return getDataTable(list);
    }

    /**
     * 导出用户收藏列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:collect:export')")
    @Log(title = "用户收藏", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpCollect spCollect)
    {
        List<SpCollect> list = spCollectService.selectSpCollectList(spCollect);
        ExcelUtil<SpCollect> util = new ExcelUtil<SpCollect>(SpCollect.class);
        return util.exportExcel(list, "collect");
    }

    /**
     * 获取用户收藏详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:collect:query')")
    @GetMapping(value = "/{collectId}")
    public AjaxResult getInfo(@PathVariable("collectId") Long collectId)
    {
        return AjaxResult.success(spCollectService.selectSpCollectById(collectId));
    }

    /**
     * 根据用户id查询该用户收藏的视频和综艺视频
     * @param userId
     * @return
     */
    @GetMapping("/getAllByUserId")
    public ResponseUtil getAllByUserId(Long userId, Integer pageNum, Integer pageSize,Long type)
    {

        PageHelper.startPage(pageNum,pageSize);

        SpCollect spCollect = new SpCollect();
        spCollect.setCollectType(type);
        spCollect.setUserId(userId);
        List<SpCollect> list = spCollectService.selectSpCollectListV2(spCollect);


        for (SpCollect collect:list){
            switch (collect.getCollectType().intValue()){
                case 1:
                    collect.setSpVideo(spVideoService.getById(collect.getCollectTypeId()));
                    break;
                case 2:
                    collect.setSpComic(spComicService.getById(collect.getCollectTypeId()));
                    break;
                case 3:
                    break;
            }

        }

//        dataInfo.setRows(varietyVideoAndVideoList);
        return ResponseUtil.success(PageInfo.of(list));
    }


    /**
     * 新增用户收藏
     */
    @Log(title = "用户收藏", businessType = BusinessType.INSERT)
    @PostMapping("addCollect")
    public ResponseUtil add(SpCollect spCollect)
    {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_id",spCollect.getUserId());
        wrapper.eq("collect_type_id",spCollect.getCollectTypeId());
        wrapper.eq("collect_type",spCollect.getCollectType());
        SpCollect serviceOne = spCollectService.getOne(wrapper);
        if (ObjectUtils.isEmpty(serviceOne)){
            spCollectService.save(spCollect);
        }
        return ResponseUtil.success();
    }




    /**
     * 修改用户收藏
     */
    @PreAuthorize("@ss.hasPermi('xqsp:collect:edit')")
    @Log(title = "用户收藏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpCollect spCollect)
    {

        return toAjax(spCollectService.updateSpCollect(spCollect));
    }

    /**
     * 删除用户收藏
     */

	@PostMapping("/delById")
    public ResponseUtil delById( DeleteCollects deleteCollects)
    {
        return ResponseUtil.update(spCollectService.deleteSpCollectByIds(deleteCollects.getCollectIds()));
    }


    /**
     * 根据用户id查询该用户收藏的演员
     * @param userId
     * @return
     */
    @GetMapping("/getByUserId")
    public TableDataInfo getActorByUserId(Long userId)
    {
        startPage();
        List<SpCollect> list = spCollectService.selectSpCollectActorByUserId(userId);
        TableDataInfo dataInfo=getDataTable(list);
        List<VarietyVideoAndVideo>varietyVideoAndVideoList=new ArrayList<>();
        VarietyVideoAndVideo varietyVideoAndVideo;
        for (SpCollect collect:list){
            varietyVideoAndVideo=new VarietyVideoAndVideo();
            if (collect.getCollectType()==4){
                varietyVideoAndVideo.setSpActor(spActorService.selectSpActorById(collect.getCollectTypeId()));
                varietyVideoAndVideo.setID(spCollectService.selectSpCollectByActorId(userId,spActorService.selectSpActorById(collect.getCollectTypeId()).getActorId()).getCollectId());
                varietyVideoAndVideoList.add(varietyVideoAndVideo);
            }

        }
        dataInfo.setRows(varietyVideoAndVideoList);
        return dataInfo;
    }


}
