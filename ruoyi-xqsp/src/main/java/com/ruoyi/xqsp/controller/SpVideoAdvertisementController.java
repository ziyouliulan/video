package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpVideoAdvertisement;
import com.ruoyi.xqsp.service.ISpVideoAdvertisementService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频广告Controller
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@RestController
@RequestMapping("/xqsp/video_advertisement")
public class SpVideoAdvertisementController extends BaseController
{
    @Autowired
    private ISpVideoAdvertisementService spVideoAdvertisementService;

    /**
     * 查询视频广告列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video_advertisement:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVideoAdvertisement spVideoAdvertisement)
    {
        startPage();
        List<SpVideoAdvertisement> list = spVideoAdvertisementService.selectSpVideoAdvertisementList(spVideoAdvertisement);
        return getDataTable(list);
    }

    /**
     * 导出视频广告列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video_advertisement:export')")
    @Log(title = "视频广告", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVideoAdvertisement spVideoAdvertisement)
    {
        List<SpVideoAdvertisement> list = spVideoAdvertisementService.selectSpVideoAdvertisementList(spVideoAdvertisement);
        ExcelUtil<SpVideoAdvertisement> util = new ExcelUtil<SpVideoAdvertisement>(SpVideoAdvertisement.class);
        return util.exportExcel(list, "video_advertisement");
    }

    /**
     * 获取视频广告详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video_advertisement:query')")
    @GetMapping(value = "/{videoAdvertisementId}")
    public AjaxResult getInfo(@PathVariable("videoAdvertisementId") Long videoAdvertisementId)
    {
        return AjaxResult.success(spVideoAdvertisementService.selectSpVideoAdvertisementById(videoAdvertisementId));
    }

    /**
     * 新增视频广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video_advertisement:add')")
    @Log(title = "视频广告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVideoAdvertisement spVideoAdvertisement)
    {
        return toAjax(spVideoAdvertisementService.insertSpVideoAdvertisement(spVideoAdvertisement));
    }

    /**
     * 修改视频广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video_advertisement:edit')")
    @Log(title = "视频广告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVideoAdvertisement spVideoAdvertisement)
    {
        return toAjax(spVideoAdvertisementService.updateSpVideoAdvertisement(spVideoAdvertisement));
    }

    /**
     * 删除视频广告
     */
    @PreAuthorize("@ss.hasPermi('xqsp:video_advertisement:remove')")
    @Log(title = "视频广告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{videoAdvertisementIds}")
    public AjaxResult remove(@PathVariable Long[] videoAdvertisementIds)
    {
        return toAjax(spVideoAdvertisementService.deleteSpVideoAdvertisementByIds(videoAdvertisementIds));
    }

    @GetMapping("/getVideoAdvertisement")
    public AjaxResult getVideoAdvertisement(){
        return AjaxResult.success(spVideoAdvertisementService.selectSpVideoAdvertisementById(1l));
    }
}
