package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.xqsp.domain.SpComicEvaluation;
import com.ruoyi.xqsp.domain.entity.ComicUserEvaluation;
import com.ruoyi.xqsp.domain.entity.UserComic;
import com.ruoyi.xqsp.service.ISpComicService;
import com.ruoyi.xqsp.service.ISpUsersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicUser;
import com.ruoyi.xqsp.service.ISpComicUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 书架管理Controller
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
@RestController
@RequestMapping("/xqsp/user")
public class SpComicUserController extends BaseController
{
    @Autowired
    private ISpComicUserService spComicUserService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpComicService spComicService;
    /**
     * 查询书架管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicUser spComicUser)
    {
        startPage();
        List<SpComicUser> list = spComicUserService.selectSpComicUserList(spComicUser);
        TableDataInfo dataTable = getDataTable(list);
        List<UserComic>userComicList=new ArrayList<>();
        UserComic userComic;
        for (SpComicUser comicUser : list){
            userComic=new UserComic();
            userComic.setSpComicUser(comicUser);
            if (comicUser.getUserId()!=null){
                if (spUsersService.selectSpUsersById(comicUser.getUserId())!=null){
                    userComic.setUserPhone(spUsersService.selectSpUsersById(comicUser.getUserId()).getUserPhone());
                }

            }else {
                userComic.setUserPhone("无");            }
            if (comicUser.getComicId()!=null){
                if (spComicService.selectSpComicById(comicUser.getComicId())!=null){
                    userComic.setComicName(spComicService.selectSpComicById(comicUser.getComicId()).getComicName());
                }

            }else {
                userComic.setComicName("无");
            }
            userComicList.add(userComic);

        }
        dataTable.setRows(userComicList);

        return dataTable;
    }

    /**
     * 导出书架管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:user:export')")
    @Log(title = "书架管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicUser spComicUser)
    {
        List<SpComicUser> list = spComicUserService.selectSpComicUserList(spComicUser);
        ExcelUtil<SpComicUser> util = new ExcelUtil<SpComicUser>(SpComicUser.class);
        return util.exportExcel(list, "user");
    }

    /**
     * 获取书架管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:user:query')")
    @GetMapping(value = "/{comicUserId}")
    public AjaxResult getInfo(@PathVariable("comicUserId") Long comicUserId)
    {
        return AjaxResult.success(spComicUserService.selectSpComicUserById(comicUserId));
    }

    /**
     * 新增书架管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:user:add')")
    @Log(title = "书架管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicUser spComicUser)
    {
        return toAjax(spComicUserService.insertSpComicUser(spComicUser));
    }

    /**
     * 修改书架管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:user:edit')")
    @Log(title = "书架管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicUser spComicUser)
    {
        return toAjax(spComicUserService.updateSpComicUser(spComicUser));
    }

    /**
     * 删除书架管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:user:remove')")
    @Log(title = "书架管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicUserIds}")
    public AjaxResult remove(@PathVariable Long[] comicUserIds)
    {
        return toAjax(spComicUserService.deleteSpComicUserByIds(comicUserIds));
    }
}
