package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicTag;
import com.ruoyi.xqsp.service.ISpComicTagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 漫画标签Controller
 * 
 * @author ruoyi
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/xqsp/comictag")
public class SpComicTagController extends BaseController
{
    @Autowired
    private ISpComicTagService spComicTagService;

    /**
     * 查询漫画标签列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comictag:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicTag spComicTag)
    {
        startPage();
        List<SpComicTag> list = spComicTagService.selectSpComicTagList(spComicTag);
        return getDataTable(list);
    }

    /**
     * 导出漫画标签列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comictag:export')")
    @Log(title = "漫画标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicTag spComicTag)
    {
        List<SpComicTag> list = spComicTagService.selectSpComicTagList(spComicTag);
        ExcelUtil<SpComicTag> util = new ExcelUtil<SpComicTag>(SpComicTag.class);
        return util.exportExcel(list, "comictag");
    }

    /**
     * 获取漫画标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comictag:query')")
    @GetMapping(value = "/{comicTagId}")
    public AjaxResult getInfo(@PathVariable("comicTagId") Long comicTagId)
    {
        return AjaxResult.success(spComicTagService.selectSpComicTagById(comicTagId));
    }

    /**
     * 新增漫画标签
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comictag:add')")
    @Log(title = "漫画标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicTag spComicTag)
    {
        return toAjax(spComicTagService.insertSpComicTag(spComicTag));
    }

    /**
     * 修改漫画标签
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comictag:edit')")
    @Log(title = "漫画标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicTag spComicTag)
    {
        return toAjax(spComicTagService.updateSpComicTag(spComicTag));
    }

    /**
     * 删除漫画标签
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comictag:remove')")
    @Log(title = "漫画标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicTagIds}")
    public AjaxResult remove(@PathVariable Long[] comicTagIds)
    {
        return toAjax(spComicTagService.deleteSpComicTagByIds(comicTagIds));
    }
}
