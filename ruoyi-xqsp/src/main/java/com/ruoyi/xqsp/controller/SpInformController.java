package com.ruoyi.xqsp.controller;

import java.util.Date;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpInform;
import com.ruoyi.xqsp.service.ISpInformService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 系统通知表Controller
 * 
 * @author ruoyi
 * @date 2021-04-21
 */
@RestController
@RequestMapping("/xqsp/inform")
public class SpInformController extends BaseController
{
    @Autowired
    private ISpInformService spInformService;

    /**
     * 查询系统通知表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:inform:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpInform spInform)
    {
        startPage();
        List<SpInform> list = spInformService.selectSpInformList(spInform);
        return getDataTable(list);
    }

    /**
     * 导出系统通知表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:inform:export')")
    @Log(title = "系统通知表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpInform spInform)
    {
        List<SpInform> list = spInformService.selectSpInformList(spInform);
        ExcelUtil<SpInform> util = new ExcelUtil<SpInform>(SpInform.class);
        return util.exportExcel(list, "inform");
    }

    /**
     * 获取系统通知表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:inform:query')")
    @GetMapping(value = "/{informId}")
    public AjaxResult getInfo(@PathVariable("informId") Long informId)
    {
        return AjaxResult.success(spInformService.selectSpInformById(informId));
    }

    /**
     * 新增系统通知表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:inform:add')")
    @Log(title = "系统通知表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpInform spInform)
    {
        spInform.setInformTime(new Date());
        return toAjax(spInformService.insertSpInform(spInform));
    }

    /**
     * 修改系统通知表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:inform:edit')")
    @Log(title = "系统通知表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpInform spInform)
    {
        return toAjax(spInformService.updateSpInform(spInform));
    }

    /**
     * 删除系统通知表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:inform:remove')")
    @Log(title = "系统通知表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{informIds}")
    public AjaxResult remove(@PathVariable Long[] informIds)
    {
        return toAjax(spInformService.deleteSpInformByIds(informIds));
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping("/getAll")
    public TableDataInfo getAll(){
        startPage();
        SpInform spInform=new SpInform();
        List<SpInform>list=spInformService.selectSpInformList(spInform);
        return getDataTable(list);
    }
}
