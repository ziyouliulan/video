package com.ruoyi.xqsp.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.quyang.voice.service.ClientConfigService;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.ComicDiscount;
import com.ruoyi.xqsp.domain.Notice;
import com.ruoyi.xqsp.domain.VideoDiscount;
import com.ruoyi.xqsp.service.ComicDiscountService;
import com.ruoyi.xqsp.service.NoticeService;
import com.ruoyi.xqsp.service.ScrollNotificationsService;
import com.ruoyi.xqsp.service.VideoDiscountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@Api(value = "配置", tags = "配置")
@RestController
@RequestMapping("/xqsp/config")
public class ConfigController {



    @Autowired
    ComicDiscountService comicDiscountService;

    @Autowired
    VideoDiscountService videoDiscountService;

    @Autowired
    ClientConfigService clientConfigService;


    @Autowired
    NoticeService noticeService;


    @Autowired
    ScrollNotificationsService scrollNotificationsService;
     @GetMapping
    public ResponseUtil getConfig(){

        return ResponseUtil.success();
    }


     @GetMapping("comicDiscount")
    public ResponseUtil comicDiscount(){

         List<ComicDiscount> list = comicDiscountService.list();

         return ResponseUtil.success(list);
    }


     @GetMapping("videoDiscount")
    public ResponseUtil videoDiscount(){

         List<VideoDiscount> list = videoDiscountService.list();

         return ResponseUtil.success(list);
    }




     @GetMapping("/clientConfig")
    public ResponseUtil configService(){

         HashMap map = clientConfigService.configToMap();

         return ResponseUtil.success(map);
    }



     @GetMapping("notice")
    public ResponseUtil notice(){

         return ResponseUtil.success(noticeService.getById(1));
    }


    @ApiOperation("滚动通知")
    @GetMapping("scrollNotifications")
    public ResponseUtil scrollNotifications(){

        QueryWrapper wrapper = new QueryWrapper();
        PageHelper.startPage(1,10);
        wrapper.orderByDesc("create_time");
        List list = scrollNotificationsService.list(wrapper);

        return ResponseUtil.success(list);
    }





}
