package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpVersion;
import com.ruoyi.xqsp.service.ISpVersionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 版本设置Controller
 * 
 * @author ruoyi
 * @date 2021-06-02
 */
@RestController
@RequestMapping("/xqsp/version")
public class SpVersionController extends BaseController
{
    @Autowired
    private ISpVersionService spVersionService;

    /**
     * 查询版本设置列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:version:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVersion spVersion)
    {
        startPage();
        List<SpVersion> list = spVersionService.selectSpVersionList(spVersion);
        return getDataTable(list);
    }

    /**
     * 导出版本设置列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:version:export')")
    @Log(title = "版本设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVersion spVersion)
    {
        List<SpVersion> list = spVersionService.selectSpVersionList(spVersion);
        ExcelUtil<SpVersion> util = new ExcelUtil<SpVersion>(SpVersion.class);
        return util.exportExcel(list, "version");
    }

    /**
     * 获取版本设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:version:query')")
    @GetMapping(value = "/{versionId}")
    public AjaxResult getInfo(@PathVariable("versionId") Long versionId)
    {
        return AjaxResult.success(spVersionService.selectSpVersionById(versionId));
    }

    /**
     * 新增版本设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:version:add')")
    @Log(title = "版本设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVersion spVersion)
    {
        return toAjax(spVersionService.insertSpVersion(spVersion));
    }

    /**
     * 修改版本设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:version:edit')")
    @Log(title = "版本设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVersion spVersion)
    {
        return toAjax(spVersionService.updateSpVersion(spVersion));
    }

    /**
     * 删除版本设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:version:remove')")
    @Log(title = "版本设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{versionIds}")
    public AjaxResult remove(@PathVariable Long[] versionIds)
    {
        return toAjax(spVersionService.deleteSpVersionByIds(versionIds));
    }

    /**
     * 查询版本号
     * @return
     */
    @GetMapping("/getAll")
    public AjaxResult getAll(){
        return AjaxResult.success(spVersionService.selectSpVersionById(1l));
    }
}
