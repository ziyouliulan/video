package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.xqsp.domain.PhoenixEvaluation;
import com.ruoyi.xqsp.domain.entity.UserPhoenixEvaluation;
import com.ruoyi.xqsp.domain.entity.UserVarietyVideoEvaluation;
import com.ruoyi.xqsp.service.ISpPhoenixServiceService;
import com.ruoyi.xqsp.service.ISpUsersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import com.ruoyi.xqsp.service.ISpPhoenixEvaluationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 楼凤评价表Controller
 *
 * @author ruoyi
 * @date 2021-04-25
 */
@RestController
@RequestMapping("/xqsp/phoenixevaluation")
public class SpPhoenixEvaluationController extends BaseController
{
    @Autowired
    private ISpPhoenixEvaluationService spPhoenixEvaluationService;

    @Autowired
    private ISpPhoenixServiceService spPhoenixServiceService;
    @Autowired
    private ISpUsersService spUsersService;

    /**
     * 查询楼凤评价表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpPhoenixEvaluation spPhoenixEvaluation)
    {
        startPage();
        List<SpPhoenixEvaluation> list = spPhoenixEvaluationService.selectSpPhoenixEvaluationList(spPhoenixEvaluation);
        TableDataInfo dataTable = getDataTable(list);
      List<PhoenixEvaluation>phoenixEvaluationList=new ArrayList<>();
      PhoenixEvaluation phoenixEvaluation;
      for (SpPhoenixEvaluation evaluation:list){
          phoenixEvaluation=new PhoenixEvaluation();
          phoenixEvaluation.setSpPhoenixEvaluation(evaluation);
          if (evaluation.getUserId()!=null){
              if (spUsersService.selectSpUsersById(evaluation.getUserId())!=null){
                  phoenixEvaluation.setUserName(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserName());
              }

          }else {
              phoenixEvaluation.setUserName("无");
          }
          if(evaluation.getPhoenixId()!=null){
              if (spPhoenixServiceService.selectSpPhoenixServiceById(evaluation.getPhoenixId())!=null){
                  phoenixEvaluation.setPhoenixName(spPhoenixServiceService.selectSpPhoenixServiceById(evaluation.getPhoenixId()).getPhoenixTitle());
              }

          }else {
              phoenixEvaluation.setPhoenixName("无");
          }

          phoenixEvaluationList.add(phoenixEvaluation);
      }
      dataTable.setRows(phoenixEvaluationList);
        return dataTable;
    }

    /**
     * 导出楼凤评价表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:export')")
    @Log(title = "楼凤评价表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpPhoenixEvaluation spPhoenixEvaluation)
    {
        List<SpPhoenixEvaluation> list = spPhoenixEvaluationService.selectSpPhoenixEvaluationList(spPhoenixEvaluation);
        ExcelUtil<SpPhoenixEvaluation> util = new ExcelUtil<SpPhoenixEvaluation>(SpPhoenixEvaluation.class);
        return util.exportExcel(list, "phoenixevaluation");
    }

    /**
     * 获取楼凤评价表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:query')")
    @GetMapping(value = "/{phoenixEvaluationId}")
    public AjaxResult getInfo(@PathVariable("phoenixEvaluationId") Long phoenixEvaluationId)
    {
        return AjaxResult.success(spPhoenixEvaluationService.selectSpPhoenixEvaluationById(phoenixEvaluationId));
    }

    /**
     * 获取楼凤评价表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:query')")
    @GetMapping(value = "/phoenixId/{phoenixId}")
    public TableDataInfo getAll(@PathVariable("phoenixId") Long phoenixId)
    {
        startPage();
        List<SpPhoenixEvaluation> list = spPhoenixEvaluationService.selectSpPhoenixEvaluationByPhoenixId(phoenixId);
        TableDataInfo dataTable = getDataTable(list);
        List<PhoenixEvaluation>phoenixEvaluationList=new ArrayList<>();
        PhoenixEvaluation phoenixEvaluation;
        for (SpPhoenixEvaluation evaluation:list){
            phoenixEvaluation=new PhoenixEvaluation();
            phoenixEvaluation.setSpPhoenixEvaluation(evaluation);

            if (evaluation.getUserId()!=null){
                phoenixEvaluation.setUserName(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserName());
            }else {
                phoenixEvaluation.setUserName("无");
            }
            if(evaluation.getPhoenixId()!=null){
                phoenixEvaluation.setPhoenixName(spPhoenixServiceService.selectSpPhoenixServiceById(evaluation.getPhoenixId()).getPhoenixName());
            }else {
                phoenixEvaluation.setPhoenixName("无");
            }

            phoenixEvaluationList.add(phoenixEvaluation);
        }
        dataTable.setRows(phoenixEvaluationList);
        return dataTable;
    }

    /**
     * 新增楼凤评价表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:add')")
    @Log(title = "楼凤评价表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpPhoenixEvaluation spPhoenixEvaluation)
    {
        return toAjax(spPhoenixEvaluationService.insertSpPhoenixEvaluation(spPhoenixEvaluation));
    }

    /**
     * 修改楼凤评价表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:edit')")
    @Log(title = "楼凤评价表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpPhoenixEvaluation spPhoenixEvaluation)
    {
        return toAjax(spPhoenixEvaluationService.updateSpPhoenixEvaluation(spPhoenixEvaluation));
    }

    /**
     * 删除楼凤评价表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:phoenixevaluation:remove')")
    @Log(title = "楼凤评价表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{phoenixEvaluationIds}")
    public AjaxResult remove(@PathVariable Long[] phoenixEvaluationIds)
    {
        return toAjax(spPhoenixEvaluationService.deleteSpPhoenixEvaluationByIds(phoenixEvaluationIds));
    }

    /**
     * 漏风体验报告首页
     * @return
     */
    @GetMapping("/getAllPhoenixEvaluation")
    public TableDataInfo getAllPhoenixEvaluation(){
        SpPhoenixEvaluation spPhoenixEvaluation=new SpPhoenixEvaluation();
        startPage();
        List<UserPhoenixEvaluation>userPhoenixEvaluationList=new ArrayList<>();
        UserPhoenixEvaluation userPhoenixEvaluation;
        List<SpPhoenixEvaluation>spPhoenixEvaluationList=spPhoenixEvaluationService.selectSpPhoenixEvaluationList(spPhoenixEvaluation);
               TableDataInfo dataInfo=getDataTable(spPhoenixEvaluationList);
        for (SpPhoenixEvaluation evaluation:spPhoenixEvaluationList){
            userPhoenixEvaluation=new UserPhoenixEvaluation();
            userPhoenixEvaluation.setSpPhoenixEvaluation(evaluation);
            userPhoenixEvaluation.setUserPhone(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserPhone());
//            userPhoenixEvaluation.setUserHead(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserImg());
            userPhoenixEvaluationList.add(userPhoenixEvaluation);
        }
        dataInfo.setRows(userPhoenixEvaluationList);
        return dataInfo;
    }

    /**
     * 根据id查询楼凤评论
     * @param phoenixId
     * @return
     */
    @GetMapping("/home/phoenixIdEvaluation/{phoenixId}")
    public TableDataInfo getPhoenixById(@PathVariable("phoenixId") Long phoenixId){

       startPage();
       List<UserPhoenixEvaluation>userPhoenixEvaluationList=new ArrayList<>();
       UserPhoenixEvaluation userPhoenixEvaluation;
       List<SpPhoenixEvaluation>list=spPhoenixEvaluationService.selectSpPhoenixEvaluationByPhoenixId(phoenixId);
       TableDataInfo dataInfo=getDataTable(list);
       for (SpPhoenixEvaluation evaluation:list){
           userPhoenixEvaluation=new UserPhoenixEvaluation();
           userPhoenixEvaluation.setSpPhoenixEvaluation(evaluation);
           userPhoenixEvaluation.setUserPhone(spUsersService.selectSpUsersById(evaluation.getUserId()).getUserPhone());
           userPhoenixEvaluationList.add(userPhoenixEvaluation);
       }
       dataInfo.setRows(userPhoenixEvaluationList);
       return dataInfo;
    }

    /**
     * 用户评论
     * @return
     */
    @PostMapping("/addPhoenixEvaluation")
    public AjaxResult addPhoenixEvaluation(@RequestBody SpPhoenixEvaluation spPhoenixEvaluation){

        spPhoenixEvaluation.setPhoenixEvaluationTime(new Date());
        return toAjax(spPhoenixEvaluationService.insertSpPhoenixEvaluation(spPhoenixEvaluation));
    }
}
