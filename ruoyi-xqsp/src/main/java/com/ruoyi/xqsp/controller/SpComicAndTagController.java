package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.xqsp.domain.SpComicTag;
import com.ruoyi.xqsp.service.ISpComicTagService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicAndTag;
import com.ruoyi.xqsp.service.ISpComicAndTagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 漫画标签中间表Controller
 * 
 * @author ruoyi
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/xqsp/comicandtag")
public class SpComicAndTagController extends BaseController
{
    @Autowired
    private ISpComicAndTagService spComicAndTagService;
    @Autowired
    private ISpComicTagService spComicTagService;

    /**
     * 查询漫画标签中间表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicAndTag spComicAndTag)
    {
        startPage();
        List<SpComicAndTag> list = spComicAndTagService.selectSpComicAndTagList(spComicAndTag);
        return getDataTable(list);
    }

    /**
     * 导出漫画标签中间表列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:export')")
    @Log(title = "漫画标签中间表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicAndTag spComicAndTag)
    {
        List<SpComicAndTag> list = spComicAndTagService.selectSpComicAndTagList(spComicAndTag);
        ExcelUtil<SpComicAndTag> util = new ExcelUtil<SpComicAndTag>(SpComicAndTag.class);
        return util.exportExcel(list, "comicandtag");
    }

    /**
     * 获取漫画标签中间表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:query')")
    @GetMapping(value = "/{comicAndTagId}")
    public AjaxResult getInfo(@PathVariable("comicAndTagId") Long comicAndTagId)
    {
        return AjaxResult.success(spComicAndTagService.selectSpComicAndTagById(comicAndTagId));
    }

    /**
     * 获取漫画标签中间表详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:query')")
    @GetMapping(value = "/comicId/{comicId}")
    public AjaxResult getComicTag(@PathVariable("comicId") Long comicId)
    {

      List<SpComicAndTag>list=  spComicAndTagService.selectSpComicAndTagByComicId(comicId);
      SpComicTag spComicTag;
      List<SpComicTag>spComicTagList=new ArrayList<>();
      for (SpComicAndTag tag:list){
          spComicTag=new SpComicTag();
          spComicTag.setComicTagName(spComicTagService.selectSpComicTagById(tag.getComicTagId()).getComicTagName());
          spComicTagList.add(spComicTag);
      }
        return AjaxResult.success(spComicTagList);
    }

    /**
     * 新增漫画标签中间表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:add')")
    @Log(title = "漫画标签中间表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicAndTag spComicAndTag)
    {
                SpComicAndTag spComicAndTag1;
                for (Long ids:spComicAndTag.getValue1()){
                    spComicAndTag1=new SpComicAndTag();
                    spComicAndTag1.setComicTagId(ids);
                    spComicAndTag1.setComicId(spComicAndTag.getComicId());
                    spComicAndTagService.insertSpComicAndTag(spComicAndTag1);
                }
        return AjaxResult.success();
    }

    /**
     * 修改漫画标签中间表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:edit')")
    @Log(title = "漫画标签中间表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicAndTag spComicAndTag)
    {
        SpComicAndTag spComicAndTag1;
        spComicAndTagService.deleteSpComicAndTagByComicId(spComicAndTag.getComicId());
        for (Long ids:spComicAndTag.getValue1()){
            spComicAndTag1=new SpComicAndTag();
            spComicAndTag1.setComicTagId(ids);
            spComicAndTag1.setComicId(spComicAndTag.getComicId());
            spComicAndTagService.insertSpComicAndTag(spComicAndTag1);
        }
        return AjaxResult.success();
    }

    /**
     * 删除漫画标签中间表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicandtag:remove')")
    @Log(title = "漫画标签中间表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicAndTagIds}")
    public AjaxResult remove(@PathVariable Long[] comicAndTagIds)
    {
        return toAjax(spComicAndTagService.deleteSpComicAndTagByIds(comicAndTagIds));
    }
}
