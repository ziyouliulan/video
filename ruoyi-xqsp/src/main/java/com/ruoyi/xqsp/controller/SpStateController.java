package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpState;
import com.ruoyi.xqsp.service.ISpStateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * APP状态Controller
 * 
 * @author ruoyi
 * @date 2021-06-10
 */
@RestController
@RequestMapping("/xqsp/state")
public class SpStateController extends BaseController
{
    @Autowired
    private ISpStateService spStateService;

    /**
     * 查询APP状态列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:state:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpState spState)
    {
        startPage();
        List<SpState> list = spStateService.selectSpStateList(spState);
        return getDataTable(list);
    }

    /**
     * 导出APP状态列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:state:export')")
    @Log(title = "APP状态", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpState spState)
    {
        List<SpState> list = spStateService.selectSpStateList(spState);
        ExcelUtil<SpState> util = new ExcelUtil<SpState>(SpState.class);
        return util.exportExcel(list, "state");
    }

    /**
     * 获取APP状态详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:state:query')")
    @GetMapping(value = "/{stateId}")
    public AjaxResult getInfo(@PathVariable("stateId") Long stateId)
    {
        return AjaxResult.success(spStateService.selectSpStateById(stateId));
    }

    /**
     * 新增APP状态
     */
    @PreAuthorize("@ss.hasPermi('xqsp:state:add')")
    @Log(title = "APP状态", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpState spState)
    {
        return toAjax(spStateService.insertSpState(spState));
    }

    /**
     * 修改APP状态
     */
    @PreAuthorize("@ss.hasPermi('xqsp:state:edit')")
    @Log(title = "APP状态", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpState spState)
    {
        return toAjax(spStateService.updateSpState(spState));
    }

    /**
     * 删除APP状态
     */
    @PreAuthorize("@ss.hasPermi('xqsp:state:remove')")
    @Log(title = "APP状态", businessType = BusinessType.DELETE)
	@DeleteMapping("/{stateIds}")
    public AjaxResult remove(@PathVariable Long[] stateIds)
    {
        return toAjax(spStateService.deleteSpStateByIds(stateIds));
    }

    /**
     * 查询
     * @return
     */
    @GetMapping("/getAll")
    public AjaxResult getAll(){
        SpState spState=spStateService.selectSpStateById(1l);
        return AjaxResult.success(spState.getStateValue());

    }
}
