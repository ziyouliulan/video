package com.ruoyi.xqsp.controller;


import com.ruoyi.xqsp.domain.SpRechargeRecord;
import com.ruoyi.xqsp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayAppController {
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpRechargeRecordService spRechargeRecordService;

    private static Logger logger = LoggerFactory.getLogger(PayAppController.class);

    @Autowired
    private WXAppPayService wxAppPayService;
    @Autowired
    private ISpGlodMoneyService spGlodMoneyService;
    @Autowired
    private ISpMemberMoneyService spMemberMoneyService;



    /**
     *
     * App支付统一下单
     *  out_trade_no  订单号
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/order")
    public Map<String, String> order(@RequestBody SpRechargeRecord spRechargeRecord)throws Exception{
        String out_trade_no = new Date().getTime()+"123";

        spRechargeRecord.setRechargeRecordTime(new Date());
        spRechargeRecord.setRechargeRecordState(1);
//        spRechargeRecord.setRechargeRecordWay(0l);
        spRechargeRecord.setRechargeRecordOrder(out_trade_no);
        if (spRechargeRecord.getType() == 1) {
            spRechargeRecord.setRechargeRecordMoney(spMemberMoneyService.selectSpMemberMoneyById(spRechargeRecord.getMoneyType()).getMemberMoneyNumber());
        }
        if (spRechargeRecord.getType() == 2){
            spRechargeRecord.setRechargeRecordMoney(spGlodMoneyService.selectSpGlodMoneyById(spRechargeRecord.getMoneyType()).getGlodMoneyNumber());
        }
        spRechargeRecordService.insertSpRechargeRecord(spRechargeRecord);//向充值记录表中添加数据
        String type="APP";
        return wxAppPayService.dounifiedOrder(type,out_trade_no,String.valueOf(spRechargeRecord.getRechargeRecordMoney()));
    }


    /**
     *   微信支付异步结果通知
     */
    @RequestMapping(value = "wxPayNotify", method = {RequestMethod.GET, RequestMethod.POST})
    public String wxPayNotify(HttpServletRequest request, HttpServletResponse response) {
        System.out.print("微信回调开始"+"\n");
        String resXml = "";
        try {
            InputStream inputStream = request.getInputStream();
            //将InputStream转换成xmlString
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            resXml = sb.toString();
            String result = wxAppPayService.payBack(resXml);
            System.out.print("微信回调结束"+"\n");
            return result;
        } catch (Exception e) {
            System.out.println("微信手机支付失败:" + e.getMessage());
            String result = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            return result;
        }
    }



}
