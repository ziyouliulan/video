package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.xqsp.domain.SpLike;
import com.ruoyi.xqsp.domain.entity.ComicUserEvaluation;
import com.ruoyi.xqsp.domain.entity.UserLike;
import com.ruoyi.xqsp.service.ISpComicService;
import com.ruoyi.xqsp.service.ISpLikeService;
import com.ruoyi.xqsp.service.ISpUsersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpComicEvaluation;
import com.ruoyi.xqsp.service.ISpComicEvaluationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 漫画评价Controller
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
@RestController
@RequestMapping("/xqsp/comicEvaluation")
public class SpComicEvaluationController extends BaseController
{
    @Autowired
    private ISpComicEvaluationService spComicEvaluationService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpComicService spComicService;
    @Autowired
    private ISpLikeService spLikeService;

    /**
     * 查询漫画评价列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicEvaluation:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpComicEvaluation spComicEvaluation)
    {
        startPage();
        List<SpComicEvaluation> list = spComicEvaluationService.selectSpComicEvaluationList(spComicEvaluation);
        TableDataInfo dataTable = getDataTable(list);
        List<ComicUserEvaluation>comicUserEvaluationList=new ArrayList<>();
        ComicUserEvaluation comicUserEvaluation;
        for (SpComicEvaluation comicEvaluation : list){
            comicUserEvaluation=new ComicUserEvaluation();
            comicUserEvaluation.setSpComicEvaluation(comicEvaluation);
            if (comicEvaluation.getUserId()!=null){
                if (spUsersService.selectSpUsersById(comicEvaluation.getUserId())!=null){
                    comicUserEvaluation.setUserPhone(spUsersService.selectSpUsersById(comicEvaluation.getUserId()).getUserPhone());
                }

            }else {
                comicUserEvaluation.setUserPhone("无");            }
            if (comicEvaluation.getComicId()!=null){
                if (spComicService.selectSpComicById(comicEvaluation.getComicId())!=null){
                    comicUserEvaluation.setComicName(spComicService.selectSpComicById(comicEvaluation.getComicId()).getComicName());
                }

            }else {
                comicUserEvaluation.setComicName("无");
            }
            comicUserEvaluationList.add(comicUserEvaluation);

        }
dataTable.setRows(comicUserEvaluationList);
        return dataTable;
    }

    /**
     * 导出漫画评价列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicEvaluation:export')")
    @Log(title = "漫画评价", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpComicEvaluation spComicEvaluation)
    {
        List<SpComicEvaluation> list = spComicEvaluationService.selectSpComicEvaluationList(spComicEvaluation);
        ExcelUtil<SpComicEvaluation> util = new ExcelUtil<SpComicEvaluation>(SpComicEvaluation.class);
        return util.exportExcel(list, "comicEvaluation");
    }

    /**
     * 获取漫画评价详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicEvaluation:query')")
    @GetMapping(value = "/{comicEvaluationId}")
    public AjaxResult getInfo(@PathVariable("comicEvaluationId") Long comicEvaluationId)
    {
        return AjaxResult.success(spComicEvaluationService.selectSpComicEvaluationById(comicEvaluationId));
    }

    /**
     * 新增漫画评价
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicEvaluation:add')")
    @Log(title = "漫画评价", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpComicEvaluation spComicEvaluation)
    {
        return toAjax(spComicEvaluationService.insertSpComicEvaluation(spComicEvaluation));
    }

    /**
     * 修改漫画评价
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicEvaluation:edit')")
    @Log(title = "漫画评价", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpComicEvaluation spComicEvaluation)
    {
        return toAjax(spComicEvaluationService.updateSpComicEvaluation(spComicEvaluation));
    }

    /**
     * 删除漫画评价
     */
    @PreAuthorize("@ss.hasPermi('xqsp:comicEvaluation:remove')")
    @Log(title = "漫画评价", businessType = BusinessType.DELETE)
	@DeleteMapping("/{comicEvaluationIds}")
    public AjaxResult remove(@PathVariable Long[] comicEvaluationIds)
    {
        return toAjax(spComicEvaluationService.deleteSpComicEvaluationByIds(comicEvaluationIds));
    }

    /**
     * 漫画评论点赞
     * @param userLike
     * @return
     */
    @PostMapping("/evaluationLike")
    public AjaxResult evaluationLike(@RequestBody UserLike userLike){
        SpLike spLike=new SpLike();
        spLike.setLikeTypeId(userLike.getComicEvaluationId());
        spLike.setUserId(userLike.getUserId());
        spLike.setLikeType(3l);
        if (spLikeService.selectSpLike(spLike)==null){
            spComicEvaluationService.updateSpComicEvaluationLike(userLike.getComicEvaluationId());
            spLikeService.insertSpLike(spLike);
            return    AjaxResult.success();
        }
        if (spLikeService.selectSpLike(spLike)!=null){
            spComicEvaluationService.updateSpComicEvaluationLike1(userLike.getComicEvaluationId());
            spLikeService.deleteSpLike(spLike);
            return    AjaxResult.success();
        }
        return AjaxResult.error("未知错误");
    }

}
