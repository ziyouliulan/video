package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpAlipay;
import com.ruoyi.xqsp.service.ISpAlipayService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 支付宝支付Controller
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@RestController
@RequestMapping("/xqsp/alipay")
public class SpAlipayController extends BaseController
{
    @Autowired
    private ISpAlipayService spAlipayService;

    /**
     * 查询支付宝支付列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:alipay:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpAlipay spAlipay)
    {
        startPage();
        List<SpAlipay> list = spAlipayService.selectSpAlipayList(spAlipay);
        return getDataTable(list);
    }

    /**
     * 导出支付宝支付列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:alipay:export')")
    @Log(title = "支付宝支付", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpAlipay spAlipay)
    {
        List<SpAlipay> list = spAlipayService.selectSpAlipayList(spAlipay);
        ExcelUtil<SpAlipay> util = new ExcelUtil<SpAlipay>(SpAlipay.class);
        return util.exportExcel(list, "alipay");
    }

    /**
     * 获取支付宝支付详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:alipay:query')")
    @GetMapping(value = "/{alipayId}")
    public AjaxResult getInfo(@PathVariable("alipayId") Long alipayId)
    {
        return AjaxResult.success(spAlipayService.selectSpAlipayById(alipayId));
    }

    /**
     * 新增支付宝支付
     */
    @PreAuthorize("@ss.hasPermi('xqsp:alipay:add')")
    @Log(title = "支付宝支付", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpAlipay spAlipay)
    {
        return toAjax(spAlipayService.insertSpAlipay(spAlipay));
    }

    /**
     * 修改支付宝支付
     */
    @PreAuthorize("@ss.hasPermi('xqsp:alipay:edit')")
    @Log(title = "支付宝支付", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpAlipay spAlipay)
    {
        return toAjax(spAlipayService.updateSpAlipay(spAlipay));
    }

    /**
     * 删除支付宝支付
     */
    @PreAuthorize("@ss.hasPermi('xqsp:alipay:remove')")
    @Log(title = "支付宝支付", businessType = BusinessType.DELETE)
	@DeleteMapping("/{alipayIds}")
    public AjaxResult remove(@PathVariable Long[] alipayIds)
    {
        return toAjax(spAlipayService.deleteSpAlipayByIds(alipayIds));
    }
}
