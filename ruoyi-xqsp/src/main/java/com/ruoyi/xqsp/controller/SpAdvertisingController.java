package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.entity.BannerVideo;
import com.ruoyi.xqsp.service.ISpVarietyVideoService;
import com.ruoyi.xqsp.service.ISpVideoCategoryService;
import com.ruoyi.xqsp.service.ISpVideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpAdvertising;
import com.ruoyi.xqsp.service.ISpAdvertisingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 广告位管理Controller
 *
 * @author ruoyi
 * @date 2021-04-17
 */
@Api(value = "广告", tags = "轮播广告")
@RestController
@RequestMapping("/xqsp/advertising")
public class SpAdvertisingController extends BaseController
{
    @Autowired
    private ISpAdvertisingService spAdvertisingService;
    @Autowired
    private ISpVideoService spVideoService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpVideoCategoryService spVideoCategoryService;

    /**
     * 查询广告位管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:advertising:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpAdvertising spAdvertising)
    {
        startPage();
        List<SpAdvertising> list = spAdvertisingService.selectSpAdvertisingList(spAdvertising);
     TableDataInfo dataInfo=  getDataTable(list);
        List<BannerVideo>bannerVideoList=new ArrayList<>();
        BannerVideo bannerVideo;
        for (SpAdvertising advertising:list){
            bannerVideo=new BannerVideo();
            bannerVideo.setSpAdvertising(advertising);
            if (advertising.getAdvertisingType()!=null){
                if (advertising.getAdvertisingType()==3){
                    if (spVideoService.selectSpVideoById(advertising.getAdvertisingTypeId())!=null){
                        bannerVideo.setVideoName(spVideoService.selectSpVideoById(advertising.getAdvertisingTypeId()).getVideoName());
                    }

                }
                else if (advertising.getAdvertisingType()==4){
                    if (spVarietyVideoService.selectSpVarietyVideoById(advertising.getAdvertisingTypeId())!=null){
                        bannerVideo.setVideoName(spVarietyVideoService.selectSpVarietyVideoById(advertising.getAdvertisingTypeId()).getVarietyVideoName());
                    }

                }
                else {
                    bannerVideo.setVideoName("无");
                }
            }
            if (advertising.getVideoCategoryId()!=null){
                bannerVideo.setCategoryName(spVideoCategoryService.selectSpVideoCategoryById(advertising.getVideoCategoryId()).getVideoCategoryName());
            }

            bannerVideoList.add(bannerVideo);
        }
        dataInfo.setRows(bannerVideoList);
        return dataInfo ;
    }



    /**
     * 导出广告位管理列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:advertising:export')")
    @Log(title = "广告位管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpAdvertising spAdvertising)
    {
        List<SpAdvertising> list = spAdvertisingService.selectSpAdvertisingList(spAdvertising);
        ExcelUtil<SpAdvertising> util = new ExcelUtil<SpAdvertising>(SpAdvertising.class);
        return util.exportExcel(list, "advertising");
    }

    /**
     * 获取广告位管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:advertising:query')")
    @GetMapping(value = "/{advertisingId}")
    public AjaxResult getInfo(@PathVariable("advertisingId") Long advertisingId)
    {
        return AjaxResult.success(spAdvertisingService.selectSpAdvertisingById(advertisingId));
    }

    /**
     * 新增广告位管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:advertising:add')")
    @Log(title = "广告位管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpAdvertising spAdvertising)
    {
        return toAjax(spAdvertisingService.insertSpAdvertising(spAdvertising));
    }

    /**
     * 修改广告位管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:advertising:edit')")
    @Log(title = "广告位管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpAdvertising spAdvertising)
    {
        return toAjax(spAdvertisingService.updateSpAdvertising(spAdvertising));
    }

    /**
     * 删除广告位管理
     */
    @PreAuthorize("@ss.hasPermi('xqsp:advertising:remove')")
    @Log(title = "广告位管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{advertisingIds}")
    public AjaxResult remove(@PathVariable Long[] advertisingIds)
    {
        return toAjax(spAdvertisingService.deleteSpAdvertisingByIds(advertisingIds));
    }

    //前台分类查询
    @ApiOperation("前台分类查询")
    @GetMapping("/getAllByCategoryId/{categoryId}")
    public ResponseUtil getAllByCategoryId(@PathVariable("categoryId") Long categoryId){

        return ResponseUtil.success(spAdvertisingService.selectSpAdvertisingByCategoryId(categoryId));
    }
    //前台首页精选查询
    @ApiOperation("前台首页精选查询")
    @GetMapping("/getAll")
    public ResponseUtil getAll(String bannerType){
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("banner_type",bannerType);
        wrapper.eq("advertising_state",0);

        return ResponseUtil.success( spAdvertisingService.list(wrapper));
    }
    //banner图点击次数
    @ApiOperation("banner图点击次数")
    @GetMapping("/updateCount/{advertisingId}")
    public ResponseUtil updateCount(@PathVariable("advertisingId") Long advertisingId){

        return ResponseUtil.update(spAdvertisingService.updateSpAdvertisingCount(advertisingId));
    }
}
