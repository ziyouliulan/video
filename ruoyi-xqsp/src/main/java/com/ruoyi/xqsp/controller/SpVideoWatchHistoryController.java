package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Interner;
import com.google.common.collect.Interners;
import com.quyang.voice.model.ConsumeRecord;
import com.quyang.voice.model.Publishs;
import com.quyang.voice.service.ShortVideoService;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.*;
import com.ruoyi.xqsp.domain.entity.DeleteCollects;
import com.ruoyi.xqsp.domain.entity.VarietyVideoAndVideo;
import com.ruoyi.xqsp.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频观看历史Controller
 *
 * @author ruoyi
 * @date 2021-05-11
 */
@Api(value = "视频观看历史",tags = "视频观看历史")
@RestController
@RequestMapping("/xqsp/history")
public class SpVideoWatchHistoryController extends BaseController
{
    @Autowired
    private ISpVideoWatchHistoryService spVideoWatchHistoryService;
    @Autowired
    private ISpVideoService spVideoService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpVarietyService spVarietyService;
    @Autowired
    private ISpUsersService spUsersService;
    @Autowired
    private ISpExpenseCalendarService spExpenseCalendarService;


    @Autowired
    ShortVideoService shortVideoService;

    @Autowired
    SpConsumeRecordService consumeRecordService;

    /**
     * 查询视频观看历史列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:history:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVideoWatchHistory spVideoWatchHistory)
    {
        startPage();
        List<SpVideoWatchHistory> list = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory);
        return getDataTable(list);
    }

    /**
     * 导出视频观看历史列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:history:export')")
    @Log(title = "视频观看历史", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVideoWatchHistory spVideoWatchHistory)
    {
        List<SpVideoWatchHistory> list = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory);
        ExcelUtil<SpVideoWatchHistory> util = new ExcelUtil<SpVideoWatchHistory>(SpVideoWatchHistory.class);
        return util.exportExcel(list, "history");
    }

    /**
     * 获取视频观看历史详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:history:query')")
    @GetMapping(value = "/{videoWatchHistoryId}")
    public AjaxResult getInfo(@PathVariable("videoWatchHistoryId") Long videoWatchHistoryId)
    {
        return AjaxResult.success(spVideoWatchHistoryService.selectSpVideoWatchHistoryById(videoWatchHistoryId));
    }

    /**
     * 新增视频观看历史
     */
    @PreAuthorize("@ss.hasPermi('xqsp:history:add')")
    @Log(title = "视频观看历史", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVideoWatchHistory spVideoWatchHistory)
    {
        return toAjax(spVideoWatchHistoryService.insertSpVideoWatchHistory(spVideoWatchHistory));
    }

    /**
     * 修改视频观看历史
     */
    @PreAuthorize("@ss.hasPermi('xqsp:history:edit')")
    @Log(title = "视频观看历史", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVideoWatchHistory spVideoWatchHistory)
    {
        return toAjax(spVideoWatchHistoryService.updateSpVideoWatchHistory(spVideoWatchHistory));
    }

    /**
     * 删除视频观看历史
     */
    @Log(title = "删除视频观看历史", businessType = BusinessType.OTHER)
    @ApiOperation(" 删除视频观看历史")
	@PostMapping("/delHistory")
    public ResponseUtil delHistory( DeleteCollects deleteCollects)
    {
        if (deleteCollects.getVideoWatchHistoryIds().length >0 ){
            spVideoWatchHistoryService.deleteSpVideoWatchHistoryByIds(deleteCollects.getVideoWatchHistoryIds());
        }else {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("user_id",deleteCollects.getUserId());
            spVideoWatchHistoryService.remove(wrapper);
        }

        return ResponseUtil.success();
    }

    /**
     * 根据用户id查询该用户的视频观看历史
     * @param userId
     * @return
     */
    @Log(title = "根据用户id查询该用户的视频观看历史", businessType = BusinessType.OTHER)
    @ApiOperation(" 根据用户id查询该用户的视频观看历史")
    @GetMapping("/getAllHistoryByUserId")
    public ResponseUtil getAllHistoryByUserId( Long userId,Integer pageNum,Integer pageSize,Integer type)
    {
        PageHelper.startPage(pageNum,pageSize);
        List<SpVideoWatchHistory> list = null;
        List<VarietyVideoAndVideo>varietyVideoAndVideoList=new ArrayList<>();
        VarietyVideoAndVideo varietyVideoAndVideo;

        switch (type){
            case 1:
                list = spVideoWatchHistoryService.selectSpVideoWatchHistoryByUserId(userId);

                for (SpVideoWatchHistory history:list){
                    varietyVideoAndVideo=new VarietyVideoAndVideo();

                    if (history.getVideoWatchHistoryType()==1){

                        varietyVideoAndVideo.setSpVideo(spVideoService.selectSpVideoById(history.getVideoWatchHistoryTypeId()));
                        varietyVideoAndVideo.setType(1l);
//                        varietyVideoAndVideo.setID(history.getVideoWatchHistoryId());
                    }
                    if(history.getVideoWatchHistoryType()==2){
                        varietyVideoAndVideo.setSpVarietyVideo(spVarietyVideoService.selectSpVarietyVideoById(history.getVideoWatchHistoryTypeId()));
                        varietyVideoAndVideo.setSpVariety(spVarietyService.selectSpVarietyById(history.getVideoWatchHistoryTypeId()));
//                        varietyVideoAndVideo.setID(history.getVideoWatchHistoryId());
                        varietyVideoAndVideo.setType(2l);
                    }
                    history.setVarietyVideoAndVideo(varietyVideoAndVideo);

//            varietyVideoAndVideoList.add(varietyVideoAndVideo);
                }
                return ResponseUtil.success(PageInfo.of(list));
//                break;
            case 2:
                QueryWrapper<SpConsumeRecord> wrapper = new QueryWrapper();
                wrapper.eq("type",10);
                wrapper.eq("user_id",userId);
                List<SpConsumeRecord> consumeRecords = consumeRecordService.list(wrapper);
                for (SpConsumeRecord consumeRecord :
                        consumeRecords) {

                    consumeRecord.setSpVideo(spVideoService.getById(consumeRecord.getToUserId()));

                }
                return ResponseUtil.success(PageInfo.of(consumeRecords));
//                list = spVideoWatchHistoryService.selectSpVideoWatchHistoryByUserIdV2(userId);

        }
        return ResponseUtil.success();
//        TableDataInfo dataInfo=getDataTable(list);

//        dataInfo.setRows(varietyVideoAndVideoList);

    }

    /**
     * 用户观看历史添加
     * @param spVideoWatchHistory
     * @return
     */
    @Log(title = "用户观看历史添加", businessType = BusinessType.OTHER)
    @ApiOperation(" 用户观看历史添加")
    @PostMapping("/addHistory")
    public ResponseUtil addHistory(SpVideoWatchHistory spVideoWatchHistory)
    {
        System.out.println("spVideoWatchHistory = " + spVideoWatchHistory);
        String s = spVideoWatchHistory.getUserId() + "" + spVideoWatchHistory.getVideoWatchHistoryTypeId();
        Interner<String> lock = Interners.newWeakInterner();
        synchronized (lock.intern(s)) {
            spVideoWatchHistory.setVideoWatchHistoryTime(new Date());

            SpExpenseCalendar spExpenseCalendar=new SpExpenseCalendar();
            spExpenseCalendar.setUserId(spVideoWatchHistory.getUserId());
            spExpenseCalendar.setExpenseCalendarState(spVideoWatchHistory.getVideoWatchHistoryTypeId());

            SpConsumeRecord consumeRecordServiceOne = consumeRecordService.getOne(new QueryWrapper<SpConsumeRecord>()
                    .eq("user_id", spVideoWatchHistory.getUserId())
                    .eq("to_user_id", spVideoWatchHistory.getVideoWatchHistoryTypeId())
                    .eq("type", 10));
//            SpUsers spUsers = spUsersService.getOne(new QueryWrapper<SpUsers>().eq("user_id", spVideoWatchHistory.getUserId()));
            SpUsers spUsers = spUsersService.getById(spVideoWatchHistory.getUserId());
            if (spUsers.getUserMembersDay().compareTo(new Date())> -1){
                spUsers.setIsVip(true);
            }

            String today = DateUtil.today();
            DateTime parse = DateUtil.parse(today, "yyyy-MM-dd");
            switch (spVideoWatchHistory.getVideoWatchHistoryType()){
                //长视频
                case 1:
                    //查看是否付费视频
                    SpVideo spVideo = spVideoService.getById(spVideoWatchHistory.getVideoWatchHistoryTypeId());


                    switch (spVideo.getVideoUnlock().intValue()){
//                    免费视频
                        case 1:
                            spUsers.setIsUnlock(true);
                            return ResponseUtil.success(spUsers);
                        //会员免费
                        case 2:

                            if (spUsers.getIsVip() ) {
                                List<SpVideoWatchHistory> spVideoWatchHistories = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory);
                                //有记录更新
                                if (spVideoWatchHistories.size() > 0){
                                    SpVideoWatchHistory spVideoWatchHistory1 = spVideoWatchHistories.get(0);
                                    spVideoWatchHistory1.setVideoWatchHistoryTime(new Date());
                                    QueryWrapper wrapper = new QueryWrapper();
                                    wrapper.eq("user_id",spVideoWatchHistory.getUserId());
                                    wrapper.eq("video_watch_history_type",spVideoWatchHistory.getVideoWatchHistoryType());
                                    wrapper.eq("video_watch_history_type_id",spVideoWatchHistory.getVideoWatchHistoryTypeId());
                                    spVideoWatchHistoryService.update(spVideoWatchHistory1,wrapper);
//                                    spVideoWatchHistoryService.updateById(spVideoWatchHistory1);
                                }else {
                                    //没有记录新增
                                    spVideoWatchHistoryService.save(spVideoWatchHistory);
                                }
                                spVideoService.updateSpVideoWatchNumber(spVideoWatchHistory.getVideoWatchHistoryTypeId());
                                spUsers.setIsUnlock(true);
                                return ResponseUtil.success(spUsers);
                            } else {
                                //非会员

                                List<SpVideoWatchHistory> spVideoWatchHistories = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory);
                                //说明有记录 查看是否是今天的
                                if (spVideoWatchHistories.size()> 0){
                                    SpVideoWatchHistory spVideoWatchHistory1 = spVideoWatchHistories.get(0);
                                    //说明是今天观看的 再次观看不减少历史记录
                                    if (spVideoWatchHistory1.getVideoWatchHistoryTime().compareTo(parse) > -1) {
                                        spVideoService.updateSpVideoWatchNumber(spVideoWatchHistory.getVideoWatchHistoryTypeId());
                                        spUsers.setIsUnlock(true);
                                        return ResponseUtil.success(spUsers);
                                    }else {
                                        //不是今天观看的  减少福利次数
//                                判断福利次数
                                        if (spUsers.getUserTodayLong() > 0) {
//                                        spUsersService.updateSpUsersTodayLongById(spVideoWatchHistory.getUserId());
                                            spUsers.setUserTodayLong(spUsers.getUserTodayLong() - 1);
                                            spUsersService.updateById(spUsers);
//                                更新观看记录的时间
                                            spVideoWatchHistory1.setVideoWatchHistoryTime(new Date());
                                            spVideoWatchHistoryService.updateById(spVideoWatchHistory1);
                                            spVideoService.updateSpVideoWatchNumber(spVideoWatchHistory.getVideoWatchHistoryTypeId());
                                            spUsers.setIsUnlock(true);
                                            return ResponseUtil.success(spUsers);
                                        }else {
                                            return ResponseUtil.success(spUsers);
                                        }
                                    }
                                }else {
                                    //没有记录
                                    //                                判断福利次数
                                    if (spUsers.getUserTodayLong() > 0) {

//                                        spUsers.setUserTodayLong(spUsers.getUserTodayLong() - 1);
                                        spUsersService.updateSpUsersTodayLongById(spUsers.getUserId());
//                                新增观看记录
                                        spVideoWatchHistoryService.save(spVideoWatchHistory);
                                        spVideoService.updateSpVideoWatchNumber(spVideoWatchHistory.getVideoWatchHistoryTypeId());
                                        spUsers.setIsUnlock(true);
                                        return ResponseUtil.success(spUsers);
                                    }else {

                                        return ResponseUtil.success(spUsers);
                                    }

                                }

//                            return ResponseUtil.fail("非会员无法观看");
                            }

//                付费
                        case 3:

                            //查询消费记录
                            if (consumeRecordServiceOne != null) {
                                List<SpVideoWatchHistory> spVideoWatchHistories = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory);
                                //有记录更新
                                if (spVideoWatchHistories.size() > 0){
                                    SpVideoWatchHistory spVideoWatchHistory1 = spVideoWatchHistories.get(0);
                                    spVideoWatchHistory1.setVideoWatchHistoryTime(new Date());
                                    spVideoWatchHistoryService.updateById(spVideoWatchHistory1);
                                }else {
                                    //没有记录新增
                                    spVideoWatchHistoryService.save(spVideoWatchHistory);
                                }
                                spVideoService.updateSpVideoWatchNumber(spVideoWatchHistory.getVideoWatchHistoryTypeId());
                                return ResponseUtil.success(spUsers);
                            } else {
                                return ResponseUtil.success(spUsers);
                            }

                    }
                    break;
//                ----------------------------------------------------------------------------------------------------------------
                //短视频
//                  type:     0正常  1：请开通会员
                case 2:
                    HashMap map = new HashMap();
                    //查询当前视频今天是否观看过了
//                List<SpVideoWatchHistory> spVideoWatchHistories = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory);
                    QueryWrapper<SpVideoWatchHistory> wrapper = new QueryWrapper();
                    wrapper.eq("user_id",spVideoWatchHistory.getUserId());
                    wrapper.eq("video_watch_history_type",spVideoWatchHistory.getVideoWatchHistoryType());
                    wrapper.eq("video_watch_history_type_id",spVideoWatchHistory.getVideoWatchHistoryTypeId());
                    wrapper.orderByDesc("video_watch_history_time");
//                wrapper.ge("video_watch_history_time",DateUtil.today());


//                //查询今天观看的次数
                    SpVideoWatchHistory spVideoWatchHistory1 = new SpVideoWatchHistory();
                    spVideoWatchHistory1 = spVideoWatchHistory;
                    Long videoWatchHistoryTypeId = spVideoWatchHistory.getVideoWatchHistoryTypeId();
//                spVideoWatchHistory1.setVideoWatchHistoryTypeId(null);
//                List<SpVideoWatchHistory> spVideoWatchHistories1 = spVideoWatchHistoryService.selectSpVideoWatchHistoryList(spVideoWatchHistory1);
//                spUsersService.updateSpUsersWelfareById(spVideoWatchHistory.getUserId());

                    QueryWrapper<SpVideoWatchHistory> wrapper1 = new QueryWrapper();
                    wrapper1.eq("user_id",spVideoWatchHistory.getUserId());
                    wrapper1.eq("video_watch_history_type",spVideoWatchHistory.getVideoWatchHistoryType());
                    wrapper1.ge("video_watch_history_time",DateUtil.today());

                    List<SpVideoWatchHistory> spVideoWatchHistoryList = spVideoWatchHistoryService.list(wrapper);

//                SpUsers spUsers = spUsersService.selectSpUsersById(spVideoWatchHistory.getUserId());

                    //查询是否是会员  是会员
                    if (DateUtil.compare(spUsers.getUserMembersDay(),new Date()) > 0) {
//                        List<SpVideoWatchHistory> list = spVideoWatchHistoryService.list(wrapper1);
//                        List<SpVideoWatchHistory> spVideoWatchHistoryList = spVideoWatchHistoryService.list(wrapper);
                        spVideoWatchHistoryService.save(spVideoWatchHistory);
                        Publishs publishs  = shortVideoService.getById(spVideoWatchHistory.getVideoWatchHistoryTypeId());
                        map.put("user_today_short",spUsers.getUserTodayShort()  );
                        map.put("user_welfare_short",spUsers.getUserWelfareShort());
                        map.put("user_today_long",spUsers.getUserTodayLong());
                        map.put("user_welfare_long",spUsers.getUserWelfareLong());
                        map.put("isVip",spUsers.getIsVip());
                        map.put("videoUrl",publishs.getVideoUrl());
                        map.put("type","0");
                        map.put("publishs",publishs);
                        if (spVideoWatchHistoryList.size() == 0){
                            spVideoWatchHistoryService.save(spVideoWatchHistory);
                        }else {
                            spVideoWatchHistoryList.get(0).setVideoWatchHistoryTime(new Date());
                            spVideoWatchHistoryService.update(spVideoWatchHistoryList.get(0),wrapper);
                        }
                        return ResponseUtil.success(map);
                        //不是会员
                    }else {
//                        wrapper.eq("is_vip",false);

//                        List<SpVideoWatchHistory> list = spVideoWatchHistoryService.list(wrapper1);
//                    判断次数是否充足
                        if (  spUsers.getUserTodayShort() >0 ){
                            Publishs publishs  = shortVideoService.getById(videoWatchHistoryTypeId);
//                            spUsersService.updateSpUsersTodayShortById(spVideoWatchHistory.getUserId());

                            if (spVideoWatchHistoryList.size() == 0){
                                spVideoWatchHistory.setIsVip(false);
                                spVideoWatchHistoryService.save(spVideoWatchHistory);
                                spUsers.setUserTodayShort(spUsers.getUserTodayShort()  -1 );
                                spUsersService.updateSpUsersTodayShortById(spUsers.getUserId());
                            }else {
                                SpVideoWatchHistory videoWatchHistory = spVideoWatchHistoryList.get(0);

                                if (videoWatchHistory.getIsVip()){
                                    spUsers.setUserTodayShort(spUsers.getUserTodayShort()  -1 );
                                    spUsersService.updateById(spUsers);
                                }

                                videoWatchHistory.setVideoWatchHistoryTime(new Date());
                                videoWatchHistory.setIsVip(false);
                                spVideoWatchHistoryService.update(spVideoWatchHistoryList.get(0),wrapper);
                            }
                            map.put("user_today_short",spUsers.getUserTodayShort());
                            map.put("user_welfare_short",spUsers.getUserWelfareShort());
                            map.put("user_today_long",spUsers.getUserTodayLong());
                            map.put("user_welfare_long",spUsers.getUserWelfareLong());
                            map.put("videoUrl",publishs.getVideoUrl());
                            map.put("publishs",publishs);
                            map.put("isVip",spUsers.getIsVip());
                            map.put("type","0");

                            return ResponseUtil.success(map);
                        }else {
                            if (spVideoWatchHistoryList.size()>0){
                                //次数不足查看今天是否观看过
                                SpVideoWatchHistory videoWatchHistory = spVideoWatchHistoryList.get(0);
                                if (videoWatchHistory.getVideoWatchHistoryTime().compareTo(DateUtil.parse(today,"yyyy-MM-dd"))>0){
                                    if (! videoWatchHistory.getIsVip()){
                                        Publishs publishs  = shortVideoService.getById(videoWatchHistoryTypeId);
                                        spVideoWatchHistoryList.get(0).setVideoWatchHistoryTime(new Date());
                                        spVideoWatchHistoryService.update(spVideoWatchHistoryList.get(0),wrapper);
                                        map.put("user_today_short",spUsers.getUserTodayShort() );
                                        map.put("user_welfare_short",spUsers.getUserWelfareShort());
                                        map.put("user_today_long",spUsers.getUserTodayLong());
                                        map.put("user_welfare_long",spUsers.getUserWelfareLong());
                                        map.put("videoUrl",publishs.getVideoUrl());
                                        map.put("publishs",publishs);
                                        map.put("type","0");
                                        map.put("isVip",spUsers.getIsVip());
                                        return ResponseUtil.success(map);

                                    }

                                }

                            }

                        }
                        map.put("user_today_short",spUsers.getUserTodayShort() );
                        map.put("user_welfare_short",spUsers.getUserWelfareShort());
                        map.put("user_today_long",spUsers.getUserTodayLong());
                        map.put("user_welfare_long",spUsers.getUserWelfareLong());
//                        map.put("videoUrl",publishs.getVideoUrl());
                        map.put("type","1");
                        map.put("isVip",spUsers.getIsVip());
                        return ResponseUtil.success(map);

//                        return ResponseUtil.fail("请开通会员");
    }

//
//
//                    if (videoWatchHistory == null){
//                        if (spUsers.getUserWelfareShort() > list.size() ){
//
//                        }else {
//                            map.put("user_today_short",spUsers.getUserTodayShort() );
//                            map.put("user_welfare_short",spUsers.getUserWelfareShort());
//                            map.put("user_today_long",spUsers.getUserTodayLong());
//                            map.put("user_welfare_long",spUsers.getUserWelfareLong());
//                            map.put("videoUrl",null);
//                            map.put("publishs",null);
//                            map.put("isVip",0);
//                            return ResponseUtil.success(map);
//                        }
//
//
//                    }else {
//                        if (videoWatchHistory.getVideoWatchHistoryTime().compareTo(DateUtil.parse(today,"yyyy-MM-dd"))>0){
//                            Publishs publishs  = shortVideoService.getById(videoWatchHistoryTypeId);
//                            videoWatchHistory.setVideoWatchHistoryTime(new Date());
//
//                            spVideoWatchHistoryService.updateById(videoWatchHistory);
//                            map.put("user_today_short",spUsers.getUserTodayShort() -1 );
//                            map.put("user_welfare_short",spUsers.getUserWelfareShort());
//                            map.put("user_today_long",spUsers.getUserTodayLong());
//                            map.put("user_welfare_long",spUsers.getUserWelfareLong());
//                            map.put("videoUrl",publishs.getVideoUrl());
//                            map.put("publishs",publishs);
//                            map.put("isVip",0);
//                            return ResponseUtil.success(map);
//
//                        }else {
//                            Publishs publishs  = shortVideoService.getById(videoWatchHistoryTypeId);
//                            videoWatchHistory.setVideoWatchHistoryTime(new Date());
//                            spUsersService.updateSpUsersTodayShortById(spVideoWatchHistory.getUserId());
//                            spVideoWatchHistoryService.updateById(videoWatchHistory);
//                            map.put("user_today_short",spUsers.getUserTodayShort() -1 );
//                            map.put("user_welfare_short",spUsers.getUserWelfareShort());
//                            map.put("user_today_long",spUsers.getUserTodayLong());
//                            map.put("user_welfare_long",spUsers.getUserWelfareLong());
//                            map.put("videoUrl",publishs.getVideoUrl());
//                            map.put("publishs",publishs);
//                            map.put("isVip",0);
//                            return ResponseUtil.success(map);
//                        }
//
//
//
//                    }

                }





//                break;

        }

        return ResponseUtil.fail("参数错误");

    }


    public static void main(String[] args) {
        String today = DateUtil.today();
        System.out.println("today = " + today);
        System.out.println("today = " + DateUtil.parse(today,"yyyy-MM-dd"));

    }
}
