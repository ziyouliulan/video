package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpGlodMoney;
import com.ruoyi.xqsp.service.ISpGlodMoneyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金币设置Controller
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
@RestController
@RequestMapping("/xqsp/glodmoney")
public class SpGlodMoneyController extends BaseController
{
    @Autowired
    private ISpGlodMoneyService spGlodMoneyService;

    /**
     * 查询金币设置列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:glodmoney:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpGlodMoney spGlodMoney)
    {
        startPage();
        List<SpGlodMoney> list = spGlodMoneyService.selectSpGlodMoneyList(spGlodMoney);
        return getDataTable(list);
    }

    /**
     * 导出金币设置列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:glodmoney:export')")
    @Log(title = "金币设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpGlodMoney spGlodMoney)
    {
        List<SpGlodMoney> list = spGlodMoneyService.selectSpGlodMoneyList(spGlodMoney);
        ExcelUtil<SpGlodMoney> util = new ExcelUtil<SpGlodMoney>(SpGlodMoney.class);
        return util.exportExcel(list, "glodmoney");
    }

    /**
     * 获取金币设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:glodmoney:query')")
    @GetMapping(value = "/{glodMoneyId}")
    public AjaxResult getInfo(@PathVariable("glodMoneyId") Long glodMoneyId)
    {
        return AjaxResult.success(spGlodMoneyService.selectSpGlodMoneyById(glodMoneyId));
    }

    /**
     * 新增金币设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:glodmoney:add')")
    @Log(title = "金币设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpGlodMoney spGlodMoney)
    {
        return toAjax(spGlodMoneyService.insertSpGlodMoney(spGlodMoney));
    }

    /**
     * 修改金币设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:glodmoney:edit')")
    @Log(title = "金币设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpGlodMoney spGlodMoney)
    {
        return toAjax(spGlodMoneyService.updateSpGlodMoney(spGlodMoney));
    }

    /**
     * 删除金币设置
     */
    @PreAuthorize("@ss.hasPermi('xqsp:glodmoney:remove')")
    @Log(title = "金币设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{glodMoneyIds}")
    public AjaxResult remove(@PathVariable Long[] glodMoneyIds)
    {
        return toAjax(spGlodMoneyService.deleteSpGlodMoneyByIds(glodMoneyIds));
    }
}
