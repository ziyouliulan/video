package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.xqsp.domain.SpCollect;
import com.ruoyi.xqsp.domain.entity.DeleteCollects;
import com.ruoyi.xqsp.domain.entity.VarietyVideoAndVideo;
import com.ruoyi.xqsp.service.ISpVarietyService;
import com.ruoyi.xqsp.service.ISpVarietyVideoService;
import com.ruoyi.xqsp.service.ISpVideoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpVideoCache;
import com.ruoyi.xqsp.service.ISpVideoCacheService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频缓存Controller
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
@RestController
@RequestMapping("/xqsp/cache")
public class SpVideoCacheController extends BaseController
{
    @Autowired
    private ISpVideoCacheService spVideoCacheService;
    @Autowired
    private ISpVideoService spVideoService;
    @Autowired
    private ISpVarietyVideoService spVarietyVideoService;
    @Autowired
    private ISpVarietyService spVarietyService;

    /**
     * 查询视频缓存列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:cache:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpVideoCache spVideoCache)
    {
        startPage();
        List<SpVideoCache> list = spVideoCacheService.selectSpVideoCacheList(spVideoCache);
        return getDataTable(list);
    }

    /**
     * 导出视频缓存列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:cache:export')")
    @Log(title = "视频缓存", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpVideoCache spVideoCache)
    {
        List<SpVideoCache> list = spVideoCacheService.selectSpVideoCacheList(spVideoCache);
        ExcelUtil<SpVideoCache> util = new ExcelUtil<SpVideoCache>(SpVideoCache.class);
        return util.exportExcel(list, "cache");
    }

    /**
     * 获取视频缓存详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:cache:query')")
    @GetMapping(value = "/{videoCacheId}")
    public AjaxResult getInfo(@PathVariable("videoCacheId") Long videoCacheId)
    {
        return AjaxResult.success(spVideoCacheService.selectSpVideoCacheById(videoCacheId));
    }


    /**
     * 新增视频缓存
     */
    @PreAuthorize("@ss.hasPermi('xqsp:cache:add')")
    @Log(title = "视频缓存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpVideoCache spVideoCache)
    {
        return toAjax(spVideoCacheService.insertSpVideoCache(spVideoCache));
    }

    /**
     * 修改视频缓存
     */
    @PreAuthorize("@ss.hasPermi('xqsp:cache:edit')")
    @Log(title = "视频缓存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpVideoCache spVideoCache)
    {
        return toAjax(spVideoCacheService.updateSpVideoCache(spVideoCache));
    }

    /**
     * 删除视频缓存
     */

    @Log(title = "视频缓存", businessType = BusinessType.DELETE)
	@PostMapping("/delCache")
    public AjaxResult delCache(@RequestBody DeleteCollects deleteCollects)
    {
        return toAjax(spVideoCacheService.deleteSpVideoCacheByIds(deleteCollects.getVideoCacheIds()));
    }



    /**
     * 根据用户id查询该用户缓存的视频和综艺视频
     * @param userId
     * @return
     */
    @GetMapping("/getAllCacheByUserId/{userId}")
    public TableDataInfo getAllByUserId(@PathVariable("userId") Long userId)
    {
        startPage();
        List<SpVideoCache> list = spVideoCacheService.selectSpVideoCacheByUserId(userId);
        TableDataInfo dataInfo=getDataTable(list);
        List<VarietyVideoAndVideo>varietyVideoAndVideoList=new ArrayList<>();
        VarietyVideoAndVideo varietyVideoAndVideo;
        for (SpVideoCache cache:list){
            varietyVideoAndVideo=new VarietyVideoAndVideo();
            varietyVideoAndVideo.setCacheUrl(cache.getVideoCacheUrl());
            if (cache.getVideoCacheType()==1){
                varietyVideoAndVideo.setSpVideo(spVideoService.selectSpVideoById(cache.getVideoCacheTypeId()));
                varietyVideoAndVideo.setType(1l);
                varietyVideoAndVideo.setID(cache.getVideoCacheId());
            }
            if(cache.getVideoCacheType()==2){
                varietyVideoAndVideo.setSpVarietyVideo(spVarietyVideoService.selectSpVarietyVideoById(cache.getVideoCacheTypeId()));
                varietyVideoAndVideo.setSpVariety(spVarietyService.selectSpVarietyById(spVarietyVideoService.selectSpVarietyVideoById(cache.getVideoCacheTypeId()).getVarietyId()));
                varietyVideoAndVideo.setType(2l);
                varietyVideoAndVideo.setID(cache.getVideoCacheId());
            }
            varietyVideoAndVideoList.add(varietyVideoAndVideo);
        }
        dataInfo.setRows(varietyVideoAndVideoList);
        return dataInfo;
    }

    /**
     * 用户缓存视频
     * @param spVideoCache
     * @return
     */
    @PostMapping("/addCache")
    public AjaxResult addCache(@RequestBody SpVideoCache spVideoCache)
    {
        spVideoCache.setVideoCacheTime(new Date());
        if(spVideoCacheService.selectSpVideoCacheList(spVideoCache).size()==0){
            return toAjax(spVideoCacheService.insertSpVideoCache(spVideoCache));
        }else {
            return AjaxResult.success();
        }

    }

    /**
     * 用户视频缓存的地址
     * @param spVideoCache
     * @return
     */
    @PostMapping("/addCacheUrl")
    public AjaxResult addCacheUrl(@RequestBody SpVideoCache spVideoCache){
        if (spVideoCacheService.selectSpVideoCacheById(spVideoCache.getVideoCacheId())!=null){
            spVideoCacheService.updateSpVideoCacheUrl(spVideoCache.getVideoCacheId(),spVideoCache.getVideoCacheUrl());
            return AjaxResult.success();
        }else {
            return AjaxResult.error();
        }


    }

}
