package com.ruoyi.xqsp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.entity.UserFeedback;
import com.ruoyi.xqsp.service.ISpUsersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpAdviceFeedback;
import com.ruoyi.xqsp.service.ISpAdviceFeedbackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 意见反馈Controller
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/xqsp/feedback")
public class SpAdviceFeedbackController extends BaseController
{
    @Autowired
    private ISpAdviceFeedbackService spAdviceFeedbackService;
    @Autowired
    private ISpUsersService spUsersService;

    /**
     * 查询意见反馈列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:feedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpAdviceFeedback spAdviceFeedback)
    {
        startPage();
        List<SpAdviceFeedback> list = spAdviceFeedbackService.selectSpAdviceFeedbackList(spAdviceFeedback);
        TableDataInfo dataTable = getDataTable(list);
        List<UserFeedback>userFeedbackList=new ArrayList<>();
        UserFeedback userFeedback;
        for (SpAdviceFeedback feedback:list){
            userFeedback=new UserFeedback();
            userFeedback.setSpAdviceFeedback(feedback);
            if (feedback.getUserId()!=null){
                if (spUsersService.selectSpUsersById(feedback.getUserId())!=null){
                    userFeedback.setUserName(spUsersService.selectSpUsersById(feedback.getUserId()).getUserName());
                }
            }


            userFeedbackList.add(userFeedback);
        }
        dataTable.setRows(userFeedbackList);
        return dataTable;
    }

    /**
     * 导出意见反馈列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:feedback:export')")
    @Log(title = "意见反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpAdviceFeedback spAdviceFeedback)
    {
        List<SpAdviceFeedback> list = spAdviceFeedbackService.selectSpAdviceFeedbackList(spAdviceFeedback);
        ExcelUtil<SpAdviceFeedback> util = new ExcelUtil<SpAdviceFeedback>(SpAdviceFeedback.class);
        return util.exportExcel(list, "feedback");
    }

    /**
     * 获取意见反馈详细信息
     */
    @GetMapping(value = "/detail")
    public ResponseUtil getInfo( Long adviceFeedbackId)
    {
        return ResponseUtil.success(spAdviceFeedbackService.selectSpAdviceFeedbackById(adviceFeedbackId));
    }



    /**
     * 新增意见反馈
     */
    @PostMapping("/addFeedback")
    public ResponseUtil addFeedback(SpAdviceFeedback spAdviceFeedback)
    {
        spAdviceFeedback.setAdviceFeedbackTime(new Date());
        return ResponseUtil.update(spAdviceFeedbackService.insertSpAdviceFeedback(spAdviceFeedback));
    }




    /**
     * 修改意见反馈
     */

    @Log(title = "意见反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpAdviceFeedback spAdviceFeedback)
    {
        return toAjax(spAdviceFeedbackService.updateSpAdviceFeedback(spAdviceFeedback));
    }

    /**
     * 删除意见反馈
     */
    @PreAuthorize("@ss.hasPermi('xqsp:feedback:remove')")
    @Log(title = "意见反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{adviceFeedbackIds}")
    public AjaxResult remove(@PathVariable Long[] adviceFeedbackIds)
    {
        return toAjax(spAdviceFeedbackService.deleteSpAdviceFeedbackByIds(adviceFeedbackIds));
    }

    /**
     * 根据用户id查询
     * @return
     */
@GetMapping("/getAllAdvice")
    public ResponseUtil getAllAdvice( Long userId, Integer pageNum, Integer pageSize){
    PageHelper.startPage(pageNum,pageSize);
//    List<SpAdviceFeedback>list=spAdviceFeedbackService.selectSpAdviceFeedbackByUserId(userId);
    QueryWrapper wrapper = new QueryWrapper();
    wrapper.eq("user_id",userId);
    List list = spAdviceFeedbackService.list(wrapper);
    return ResponseUtil.success(PageInfo.of(list));

    }


}
