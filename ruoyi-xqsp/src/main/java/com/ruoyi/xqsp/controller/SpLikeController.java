package com.ruoyi.xqsp.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.xqsp.domain.SpLike;
import com.ruoyi.xqsp.service.ISpLikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户点赞Controller
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
@RestController
@RequestMapping("/xqsp/like")
public class SpLikeController extends BaseController
{
    @Autowired
    private ISpLikeService spLikeService;

    /**
     * 查询用户点赞列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:like:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpLike spLike)
    {
        startPage();
        List<SpLike> list = spLikeService.selectSpLikeList(spLike);
        return getDataTable(list);
    }

    /**
     * 导出用户点赞列表
     */
    @PreAuthorize("@ss.hasPermi('xqsp:like:export')")
    @Log(title = "用户点赞", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SpLike spLike)
    {
        List<SpLike> list = spLikeService.selectSpLikeList(spLike);
        ExcelUtil<SpLike> util = new ExcelUtil<SpLike>(SpLike.class);
        return util.exportExcel(list, "like");
    }

    /**
     * 获取用户点赞详细信息
     */
    @PreAuthorize("@ss.hasPermi('xqsp:like:query')")
    @GetMapping(value = "/{likeId}")
    public AjaxResult getInfo(@PathVariable("likeId") Long likeId)
    {
        return AjaxResult.success(spLikeService.selectSpLikeById(likeId));
    }

    /**
     * 新增用户点赞
     */
    @PreAuthorize("@ss.hasPermi('xqsp:like:add')")
    @Log(title = "用户点赞", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpLike spLike)
    {
        return toAjax(spLikeService.insertSpLike(spLike));
    }

    /**
     * 修改用户点赞
     */
    @PreAuthorize("@ss.hasPermi('xqsp:like:edit')")
    @Log(title = "用户点赞", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpLike spLike)
    {
        return toAjax(spLikeService.updateSpLike(spLike));
    }

    /**
     * 删除用户点赞
     */
    @PreAuthorize("@ss.hasPermi('xqsp:like:remove')")
    @Log(title = "用户点赞", businessType = BusinessType.DELETE)
	@DeleteMapping("/{likeIds}")
    public AjaxResult remove(@PathVariable Long[] likeIds)
    {
        return toAjax(spLikeService.deleteSpLikeByIds(likeIds));
    }
}
