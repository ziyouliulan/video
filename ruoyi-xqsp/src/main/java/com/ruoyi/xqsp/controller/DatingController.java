package com.ruoyi.xqsp.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.*;
import com.ruoyi.xqsp.service.*;
import com.ruoyi.xqsp.utils.TokenServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import net.bytebuddy.implementation.bind.annotation.Default;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Api(value = "广场", tags = "广场管理")
@RestController
@RequestMapping("/xqsp/square")
public class DatingController {

    @Autowired
    DatingUserService datingUserService;

    @Autowired
    DatingUserCommentService datingUserCommentService;

    @Autowired
    UserVipService userVipService;

    @Autowired
    DatingUserCollectionService datingUserCollectionService;

    @Autowired
    CityService cityService;

    @Autowired
    ISpUsersService usersService;

    @Autowired
    TokenServices tokenServices;

    @ApiOperation("用户列表 type:1精选 2最新 ")
    @GetMapping("userList")
    public ResponseUtil list(Integer pageNum, Integer pageSize, Integer type, Integer cityId ){

        PageHelper.startPage(pageNum,pageSize);
        QueryWrapper<DatingUser> wrapper = new QueryWrapper<>();


        if (type == 1){
            wrapper.orderByDesc("is_top_date","create_time");
            wrapper.eq("recommend",true);
        }else {
            wrapper.orderByDesc("create_time");
            wrapper.eq("recommend",false);
        }

        if (cityId != null && cityId != 0){
            wrapper.eq("city_id",cityId);
        }

        List<DatingUser> list = datingUserService.list(wrapper);
        PageInfo<DatingUser> of = PageInfo.of(list);
        return ResponseUtil.success(of);

    }


    @ApiOperation("用户收藏")
    @GetMapping("collection")
    public ResponseUtil collection(DatingUserCollection datingUserCollection){

        if (datingUserCollection.getIsCollection()){
            return ResponseUtil.success(datingUserCollectionService.save(datingUserCollection));
        }else {
            QueryWrapper wrapper = new QueryWrapper();
            wrapper.eq("user_id",datingUserCollection.getUserId());
            wrapper.eq("dating_use_id",datingUserCollection.getDatingUseId());

            return ResponseUtil.success(datingUserCollectionService.remove(wrapper));
        }

    }


    @ApiOperation("我的收藏")
    @GetMapping("collectionLists")
    public ResponseUtil collection(Integer userId){
//        QueryWrapper<DatingUserCollection> wrapper = new QueryWrapper();
//        wrapper.eq("user_id",userId);
//        List<DatingUserCollection> list = datingUserCollectionService.list(wrapper);
//        List<DatingUser> datingUsers = new ArrayList<>();
//        for (DatingUserCollection  datingUserCollection:list) {
//            DatingUser byId = datingUserService.getById(datingUserCollection.getDatingUseId());
//            datingUsers.add(byId);
//        }

         datingUserService.favoritesList(userId);

        return ResponseUtil.success(datingUserService.favoritesList(userId));
    }


    @ApiOperation("用户评论")
    @GetMapping("comment")

    public ResponseUtil comment(DatingUserComment datingUserComment){


        datingUserCommentService.save(datingUserComment);


        return ResponseUtil.success();

    }

    @ApiOperation("用户评论列表")
    @GetMapping("commentList")
    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "Long" ,name = "userId",value = "用户id",required = true),
            @ApiImplicitParam(dataType = "Long" ,name = "id",value = "广场用户id",required = true),
    })
    public ResponseUtil commentList( Long id,Integer pageNum, Integer pageSize){
        PageHelper.startPage(pageNum,pageSize);
        return ResponseUtil.success(PageInfo.of(datingUserCommentService.byIdList(id)));

    }

    @ApiOperation("用户信息 返回字段isCollection true 为已收藏 false为未收藏； 手机号没有为未解锁")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Long" ,name = "userId",value = "用户id",required = true),
            @ApiImplicitParam(dataType = "Long" ,name = "id",value = "广场用户id",required = true),
    })
    @GetMapping("get")
    public ResponseUtil get(Long userId, Long id, HttpServletRequest request){

        DatingUser datingUser = datingUserService.getById(id);

//        QueryWrapper wrapper = new QueryWrapper();
//        wrapper.eq("user_id",userId);

        SpUsers user = tokenServices.getUser(request);
        if (user != null ){
            SpUsers byId = usersService.getById(user.getUserId());

            if (byId.getUserMembersDay().compareTo(new Date()) < 0){
                datingUser.setPhonenumber(null);
                datingUser.setQq(null);
                datingUser.setWechat(null);
            }

        }else {
            datingUser.setPhonenumber(null);
            datingUser.setQq(null);
            datingUser.setWechat(null);
        }

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id",userId);
        queryWrapper.eq("dating_use_id",id);
        DatingUserCollection one = datingUserCollectionService.getOne(queryWrapper);
        datingUser.setIsCollection(! ObjectUtils.isEmpty(one));


        return ResponseUtil.success(datingUser);

    }




    @ApiOperation("城市列表")
    @GetMapping("cityList")
    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "Long" ,name = "userId",value = "用户id",required = true),
            @ApiImplicitParam(dataType = "Long" ,name = "id",value = "广场用户id",required = true),
    })
    public ResponseUtil cityService( ){

        List<City> list = cityService.list();

        return ResponseUtil.success(list);

    }


//    @ApiOperation("用户解锁")
//    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "Long", name = "userId",value = "用户id",required = true),
//            @ApiImplicitParam(dataType = "Long", name = "datingUseId",value = "解锁的用户id",required = true),
//    })
//    @GetMapping("userUnlock")
//    public ResponseUtil userUnlock(DatingUserUnlock datingUserUnlock){
//        boolean save = datingUserUnlockService.save(datingUserUnlock);
//        return ResponseUtil.success(save);
//
//    }




}
