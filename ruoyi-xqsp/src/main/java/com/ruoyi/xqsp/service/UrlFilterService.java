package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.UrlFilter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_url_filter】的数据库操作Service
* @createDate 2022-08-17 03:29:14
*/
public interface UrlFilterService extends IService<UrlFilter> {

}
