package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpHotSearch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_hot_search】的数据库操作Service
* @createDate 2022-06-18 16:21:38
*/
public interface SpHotSearchService extends IService<SpHotSearch> {

}
