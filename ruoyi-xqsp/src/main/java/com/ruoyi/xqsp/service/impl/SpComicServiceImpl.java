package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.mapper.SpActorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicMapper;
import com.ruoyi.xqsp.domain.SpComic;
import com.ruoyi.xqsp.service.ISpComicService;

/**
 * 漫画操作Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-28
 */
@Service
public class SpComicServiceImpl extends ServiceImpl<SpComicMapper, SpComic> implements ISpComicService
{
    @Autowired
    private SpComicMapper spComicMapper;

    /**
     * 查询漫画操作
     *
     * @param comicId 漫画操作ID
     * @return 漫画操作
     */
    @Override
    public SpComic selectSpComicById(Long comicId)
    {
        return spComicMapper.selectSpComicById(comicId);
    }

    /**
     * 查询漫画操作列表
     *
     * @param spComic 漫画操作
     * @return 漫画操作
     */
    @Override
    public List<SpComic> selectSpComicList(SpComic spComic)
    {
        return spComicMapper.selectSpComicList(spComic);
    }

    /**
     * 新增漫画操作
     *
     * @param spComic 漫画操作
     * @return 结果
     */
    @Override
    public int insertSpComic(SpComic spComic)
    {
        return spComicMapper.insertSpComic(spComic);
    }

    /**
     * 修改漫画操作
     *
     * @param spComic 漫画操作
     * @return 结果
     */
    @Override
    public int updateSpComic(SpComic spComic)
    {
        return spComicMapper.updateSpComic(spComic);
    }

    /**
     * 批量删除漫画操作
     *
     * @param comicIds 需要删除的漫画操作ID
     * @return 结果
     */
    @Override
    public int deleteSpComicByIds(Long[] comicIds)
    {
        return spComicMapper.deleteSpComicByIds(comicIds);
    }

    /**
     * 删除漫画操作信息
     *
     * @param comicId 漫画操作ID
     * @return 结果
     */
    @Override
    public int deleteSpComicById(Long comicId)
    {
        return spComicMapper.deleteSpComicById(comicId);
    }

    @Override
    public SpComic selectSpComicById1(Long comicId) {
        return spComicMapper.selectSpComicById1(comicId);
    }

    @Override
    public List<SpComic> selectSpComicByCategory(String comicCategory) {
        return spComicMapper.selectSpComicByCategory(comicCategory);
    }

    @Override
    public List<SpComic> selectSpComicListOrderTime(SpComic spComic) {
        return spComicMapper.selectSpComicListOrderTime(spComic);
    }

    @Override
    public List<SpComic> selectSpComicListOrderWatch(SpComic spComic) {
        return spComicMapper.selectSpComicListOrderWatch(spComic);
    }

    @Override
    public int updateSpComicByComicId(Long comicId) {
        return spComicMapper.updateSpComicByComicId(comicId);
    }

    @Override
    public int updateSpComicByComicId1(Long comicId) {
        return spComicMapper.updateSpComicByComicId1(comicId);
    }

    @Override
    public int updateWatchByComicId(Long comicId) {
        return spComicMapper.updateWatchByComicId(comicId);
    }
}
