package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpBannerMapper;
import com.ruoyi.xqsp.domain.SpBanner;
import com.ruoyi.xqsp.service.ISpBannerService;

/**
 * banner图操作Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
@Service
public class SpBannerServiceImpl implements ISpBannerService 
{
    @Autowired
    private SpBannerMapper spBannerMapper;

    /**
     * 查询banner图操作
     * 
     * @param bannerId banner图操作ID
     * @return banner图操作
     */
    @Override
    public SpBanner selectSpBannerById(Long bannerId)
    {
        return spBannerMapper.selectSpBannerById(bannerId);
    }

    /**
     * 查询banner图操作列表
     * 
     * @param spBanner banner图操作
     * @return banner图操作
     */
    @Override
    public List<SpBanner> selectSpBannerList(SpBanner spBanner)
    {
        return spBannerMapper.selectSpBannerList(spBanner);
    }

    /**
     * 新增banner图操作
     * 
     * @param spBanner banner图操作
     * @return 结果
     */
    @Override
    public int insertSpBanner(SpBanner spBanner)
    {
        return spBannerMapper.insertSpBanner(spBanner);
    }

    /**
     * 修改banner图操作
     * 
     * @param spBanner banner图操作
     * @return 结果
     */
    @Override
    public int updateSpBanner(SpBanner spBanner)
    {
        return spBannerMapper.updateSpBanner(spBanner);
    }

    /**
     * 批量删除banner图操作
     * 
     * @param bannerIds 需要删除的banner图操作ID
     * @return 结果
     */
    @Override
    public int deleteSpBannerByIds(Long[] bannerIds)
    {
        return spBannerMapper.deleteSpBannerByIds(bannerIds);
    }

    /**
     * 删除banner图操作信息
     * 
     * @param bannerId banner图操作ID
     * @return 结果
     */
    @Override
    public int deleteSpBannerById(Long bannerId)
    {
        return spBannerMapper.deleteSpBannerById(bannerId);
    }
}
