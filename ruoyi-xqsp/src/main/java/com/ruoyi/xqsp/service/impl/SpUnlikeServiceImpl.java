package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpUnlikeMapper;
import com.ruoyi.xqsp.domain.SpUnlike;
import com.ruoyi.xqsp.service.ISpUnlikeService;

/**
 * 差评Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-08
 */
@Service
public class SpUnlikeServiceImpl implements ISpUnlikeService 
{
    @Autowired
    private SpUnlikeMapper spUnlikeMapper;

    /**
     * 查询差评
     * 
     * @param unlikeId 差评ID
     * @return 差评
     */
    @Override
    public SpUnlike selectSpUnlikeById(Long unlikeId)
    {
        return spUnlikeMapper.selectSpUnlikeById(unlikeId);
    }

    /**
     * 查询差评列表
     * 
     * @param spUnlike 差评
     * @return 差评
     */
    @Override
    public List<SpUnlike> selectSpUnlikeList(SpUnlike spUnlike)
    {
        return spUnlikeMapper.selectSpUnlikeList(spUnlike);
    }

    /**
     * 新增差评
     * 
     * @param spUnlike 差评
     * @return 结果
     */
    @Override
    public int insertSpUnlike(SpUnlike spUnlike)
    {
        return spUnlikeMapper.insertSpUnlike(spUnlike);
    }

    /**
     * 修改差评
     * 
     * @param spUnlike 差评
     * @return 结果
     */
    @Override
    public int updateSpUnlike(SpUnlike spUnlike)
    {
        return spUnlikeMapper.updateSpUnlike(spUnlike);
    }

    /**
     * 批量删除差评
     * 
     * @param unlikeIds 需要删除的差评ID
     * @return 结果
     */
    @Override
    public int deleteSpUnlikeByIds(Long[] unlikeIds)
    {
        return spUnlikeMapper.deleteSpUnlikeByIds(unlikeIds);
    }

    /**
     * 删除差评信息
     * 
     * @param unlikeId 差评ID
     * @return 结果
     */
    @Override
    public int deleteSpUnlikeById(Long unlikeId)
    {
        return spUnlikeMapper.deleteSpUnlikeById(unlikeId);
    }

    @Override
    public int deleteSpUnlike(SpUnlike spUnlike) {
        return spUnlikeMapper.deleteSpUnlike(spUnlike);
    }

    @Override
    public SpUnlike selectSpUnlikeByIds(Long userId, Long videoId) {
        return spUnlikeMapper.selectSpUnlikeByIds(userId,videoId);
    }

    @Override
    public SpUnlike selectSpUnlikeByVarietyVideoId(Long userId, Long varietyVideoId) {
        return spUnlikeMapper.selectSpUnlikeByVarietyVideoId(userId,varietyVideoId);
    }

    @Override
    public int deleteVarietyVideoUnlike(SpUnlike spUnlike) {
        return spUnlikeMapper.deleteVarietyVideoUnlike(spUnlike);
    }
}
