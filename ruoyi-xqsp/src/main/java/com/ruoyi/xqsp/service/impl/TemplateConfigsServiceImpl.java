package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.TemplateConfigs;
import com.ruoyi.xqsp.service.TemplateConfigsService;
import com.ruoyi.xqsp.mapper.TemplateConfigsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @author lpden
* @description 针对表【sys_template_configs(视频分类)】的数据库操作Service实现
* @createDate 2022-07-09 20:27:16
*/
@Service
public class TemplateConfigsServiceImpl extends ServiceImpl<TemplateConfigsMapper, TemplateConfigs>
    implements TemplateConfigsService{

    @Autowired
    TemplateConfigsMapper templateConfigsMapper;

    @Override
    public List<Map> category(Integer id) {
        return templateConfigsMapper.category(id);
    }


}




