package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicFreeChapterMapper;
import com.ruoyi.xqsp.domain.SpComicFreeChapter;
import com.ruoyi.xqsp.service.ISpComicFreeChapterService;

/**
 * 免费章节数Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@Service
public class SpComicFreeChapterServiceImpl implements ISpComicFreeChapterService 
{
    @Autowired
    private SpComicFreeChapterMapper spComicFreeChapterMapper;

    /**
     * 查询免费章节数
     * 
     * @param comicFreeChapter 免费章节数ID
     * @return 免费章节数
     */
    @Override
    public SpComicFreeChapter selectSpComicFreeChapterById(Long comicFreeChapter)
    {
        return spComicFreeChapterMapper.selectSpComicFreeChapterById(comicFreeChapter);
    }

    /**
     * 查询免费章节数列表
     * 
     * @param spComicFreeChapter 免费章节数
     * @return 免费章节数
     */
    @Override
    public List<SpComicFreeChapter> selectSpComicFreeChapterList(SpComicFreeChapter spComicFreeChapter)
    {
        return spComicFreeChapterMapper.selectSpComicFreeChapterList(spComicFreeChapter);
    }

    /**
     * 新增免费章节数
     * 
     * @param spComicFreeChapter 免费章节数
     * @return 结果
     */
    @Override
    public int insertSpComicFreeChapter(SpComicFreeChapter spComicFreeChapter)
    {
        return spComicFreeChapterMapper.insertSpComicFreeChapter(spComicFreeChapter);
    }

    /**
     * 修改免费章节数
     * 
     * @param spComicFreeChapter 免费章节数
     * @return 结果
     */
    @Override
    public int updateSpComicFreeChapter(SpComicFreeChapter spComicFreeChapter)
    {
        return spComicFreeChapterMapper.updateSpComicFreeChapter(spComicFreeChapter);
    }

    /**
     * 批量删除免费章节数
     * 
     * @param comicFreeChapters 需要删除的免费章节数ID
     * @return 结果
     */
    @Override
    public int deleteSpComicFreeChapterByIds(Long[] comicFreeChapters)
    {
        return spComicFreeChapterMapper.deleteSpComicFreeChapterByIds(comicFreeChapters);
    }

    /**
     * 删除免费章节数信息
     * 
     * @param comicFreeChapter 免费章节数ID
     * @return 结果
     */
    @Override
    public int deleteSpComicFreeChapterById(Long comicFreeChapter)
    {
        return spComicFreeChapterMapper.deleteSpComicFreeChapterById(comicFreeChapter);
    }
}
