package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVideo;

/**
 * 综艺视频Service接口
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
public interface ISpVarietyVideoService 
{
    /**
     * 查询综艺视频
     * 
     * @param varietyVideoId 综艺视频ID
     * @return 综艺视频
     */
    public SpVarietyVideo selectSpVarietyVideoById(Long varietyVideoId);

    /**
     * 查询综艺视频列表
     * 
     * @param spVarietyVideo 综艺视频
     * @return 综艺视频集合
     */
    public List<SpVarietyVideo> selectSpVarietyVideoList(SpVarietyVideo spVarietyVideo);

    /**
     * 新增综艺视频
     * 
     * @param spVarietyVideo 综艺视频
     * @return 结果
     */
    public int insertSpVarietyVideo(SpVarietyVideo spVarietyVideo);

    /**
     * 修改综艺视频
     * 
     * @param spVarietyVideo 综艺视频
     * @return 结果
     */
    public int updateSpVarietyVideo(SpVarietyVideo spVarietyVideo);

    /**
     * 批量删除综艺视频
     * 
     * @param varietyVideoIds 需要删除的综艺视频ID
     * @return 结果
     */
    public int deleteSpVarietyVideoByIds(Long[] varietyVideoIds);

    /**
     * 删除综艺视频信息
     * 
     * @param varietyVideoId 综艺视频ID
     * @return 结果
     */
    public int deleteSpVarietyVideoById(Long varietyVideoId);

    /**
     * 根据综艺id查询综艺视频
     * @param varietyId
     * @return
     */
    List<SpVarietyVideo> selectSpVarietyVideoByVarietyId(Long varietyId);

    /**
     * 根据综艺id查询综艺下的视频数量
     * @param varietyId
     * @return
     */
    Integer selectCountByVarietyId(Long varietyId);

    List<SpVarietyVideo> selectSpVarietyVideoByVarietyIds(Long varietyId);

    /**
     * 点赞
     * @param varietyVideoId
     * @return
     */
    int updateVideoLikeNumber(Long varietyVideoId);

    /**
     * 取消点赞
     * @param varietyVideoId
     * @return
     */
    int updateVideoLikeNumber1(Long varietyVideoId);

    /**
     * 差评减一
     * @param varietyVideoId
     * @return
     */
    int updateVideoUnLikeNumber1(Long varietyVideoId);

    /**
     * 查评加一
     * @param varietyVideoId
     * @return
     */
    int updateVideoUnLikeNumber(Long varietyVideoId);

    List<SpVarietyVideo> selectSpVarietyVideo();

    /**
     * 收藏加一
     * @param varietyVideoId
     * @return
     */
    int updateVideoCollectNumber(Long varietyVideoId);

    /**
     * 收藏减一
     * @param varietyVideoId
     * @return
     */
    int updateVideoCollectNumber1(Long varietyVideoId);

    /**
     * 观看人数加一
     * @param videoWatchHistoryTypeId
     * @return
     */
    int updateVideoWatchNumber(Long videoWatchHistoryTypeId);

    /**
     * 查询所有已上架的综艺视频
     * @param spVarietyVideo
     * @return
     */
    List<SpVarietyVideo> selectSpVarietyVideoLists(SpVarietyVideo spVarietyVideo);

    /**
     * 根据时间排序
     * @param spVarietyVideo
     * @return
     */
    List<SpVarietyVideo> selectSpVarietyVideoListByDate(SpVarietyVideo spVarietyVideo);

    /**
     * 根据播放次数排序
     * @param spVarietyVideo
     * @return
     */
    List<SpVarietyVideo> selectSpVarietyVideoListByWatch(SpVarietyVideo spVarietyVideo);

    /**
     * 根据名字模糊查询
     * @param name
     * @return
     */
    List<SpVarietyVideo> selectSpVarietyVideoListByName(String name);
}
