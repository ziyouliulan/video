package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.City;
import com.ruoyi.xqsp.service.CityService;
import com.ruoyi.xqsp.mapper.CityMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_city】的数据库操作Service实现
* @createDate 2022-07-05 21:19:26
*/
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City>
    implements CityService{

}




