package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.ComicDiscount;
import com.ruoyi.xqsp.service.ComicDiscountService;
import com.ruoyi.xqsp.mapper.ComicDiscountMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【comic_discount】的数据库操作Service实现
* @createDate 2022-05-08 00:04:31
*/
@Service
public class ComicDiscountServiceImpl extends ServiceImpl<ComicDiscountMapper, ComicDiscount>
    implements ComicDiscountService{

}




