package com.ruoyi.xqsp.service;

import java.util.List;

import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVarietyVideoEvaluation;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;

/**
 * 综艺视频评论Service接口
 *
 * @author ruoyi
 * @date 2021-05-07
 */
public interface ISpVarietyVideoEvaluationService
{
    /**
     * 查询综艺视频评论
     *
     * @param varietyVideoEvaluationId 综艺视频评论ID
     * @return 综艺视频评论
     */
    public SpVarietyVideoEvaluation selectSpVarietyVideoEvaluationById(Long varietyVideoEvaluationId);

    /**
     * 查询综艺视频评论列表
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 综艺视频评论集合
     */
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationList(SpVarietyVideoEvaluation spVarietyVideoEvaluation);

    /**
     * 查询综艺视频评论列表
     * 
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 综艺视频评论集合
     */
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationListV2(Integer id);

    /**
     * 新增综艺视频评论
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 结果
     */
    public int insertSpVarietyVideoEvaluation(SpVarietyVideoEvaluation spVarietyVideoEvaluation);

    /**
     * 修改综艺视频评论
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 结果
     */
    public int updateSpVarietyVideoEvaluation(SpVarietyVideoEvaluation spVarietyVideoEvaluation);

    /**
     * 批量删除综艺视频评论
     *
     * @param varietyVideoEvaluationIds 需要删除的综艺视频评论ID
     * @return 结果
     */
    public int deleteSpVarietyVideoEvaluationByIds(Long[] varietyVideoEvaluationIds);

    /**
     * 删除综艺视频评论信息
     *
     * @param varietyVideoEvaluationId 综艺视频评论ID
     * @return 结果
     */
    public int deleteSpVarietyVideoEvaluationById(Long varietyVideoEvaluationId);

    /**
     * 评论数量
     * @param spVarietyVideo
     * @return
     */
    Long selectCountById(Long varietyVideoId);

    /**
     * 评论查询
     * @param varietyVideoId
     * @return
     */
    List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationByIds(Long varietyVideoId);

    /**
     * 点赞人数加一
     * @param varietyVideoEvaluationId
     * @return
     */
    int updateSpVarietyVideoEvaluationLike(Long varietyVideoEvaluationId);

    /**
     *
     * @param varietyVideoEvaluationId
     * @return
     */
    int updateSpVarietyVideoEvaluationLike1(Long varietyVideoEvaluationId);

    /**
     * 根据综艺视频id删除综艺视频评论
     * @param videoId
     * @return
     */
    int deleteSpVarietyVideoEvaluationByVvId(Long varietyVideoId);
}
