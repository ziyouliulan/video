package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.ruoyi.xqsp.domain.ActorMessage;
import com.ruoyi.xqsp.mapper.SpActorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpActorMessageMapper;
import com.ruoyi.xqsp.domain.SpActorMessage;
import com.ruoyi.xqsp.service.ISpActorMessageService;

/**
 * 演员信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-22
 */
@Service
public class SpActorMessageServiceImpl implements ISpActorMessageService 
{
    @Autowired
    private SpActorMessageMapper spActorMessageMapper;
    @Autowired
    private SpActorMapper spActorMapper;

    /**
     * 查询演员信息
     * 
     * @param actorMessageId 演员信息ID
     * @return 演员信息
     */
    @Override
    public SpActorMessage selectSpActorMessageById(Long actorMessageId)
    {
        return spActorMessageMapper.selectSpActorMessageById(actorMessageId);
    }

    @Override
    public SpActorMessage selectSpActorMessageByActorId(Long actorId) {
        return spActorMessageMapper.selectSpActorMessageByActorId(actorId);
    }

    /**
     * 查询演员信息列表
     * 
     * @param spActorMessage 演员信息
     * @return 演员信息
     */
    @Override
    public List<SpActorMessage> selectSpActorMessageList(SpActorMessage spActorMessage)
    {
        return spActorMessageMapper.selectSpActorMessageList(spActorMessage);
    }

    @Override
    public List<ActorMessage> selectActorMessageList(SpActorMessage actorMessage) {

        return spActorMessageMapper.selectActorMessageList(actorMessage);
    }

    /**
     * 新增演员信息
     * 
     * @param spActorMessage 演员信息
     * @return 结果
     */
    @Override
    public int insertSpActorMessage(SpActorMessage spActorMessage)
    {
        return spActorMessageMapper.insertSpActorMessage(spActorMessage);
    }

    /**
     * 修改演员信息
     * 
     * @param spActorMessage 演员信息
     * @return 结果
     */
    @Override
    public int updateSpActorMessage(SpActorMessage spActorMessage)
    {
        return spActorMessageMapper.updateSpActorMessage(spActorMessage);
    }

    /**
     * 批量删除演员信息
     * 
     * @param actorMessageIds 需要删除的演员信息ID
     * @return 结果
     */
    @Override
    public int deleteSpActorMessageByIds(Long[] actorMessageIds)
    {
        return spActorMessageMapper.deleteSpActorMessageByIds(actorMessageIds);
    }

    /**
     * 删除演员信息信息
     * 
     * @param actorMessageId 演员信息ID
     * @return 结果
     */
    @Override
    public int deleteSpActorMessageById(Long actorMessageId)
    {
        return spActorMessageMapper.deleteSpActorMessageById(actorMessageId);
    }
}
