package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.ComicDiscount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【comic_discount】的数据库操作Service
* @createDate 2022-05-08 00:04:31
*/
public interface ComicDiscountService extends IService<ComicDiscount> {

}
