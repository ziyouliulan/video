package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.ScrollNotifications;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_scroll_notifications(通知公告表)】的数据库操作Service
* @createDate 2022-07-25 21:06:45
*/
public interface ScrollNotificationsService extends IService<ScrollNotifications> {

}
