package com.ruoyi.xqsp.service;

import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.UserVip;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_user_vip】的数据库操作Service
* @createDate 2022-02-26 09:10:19
*/
public interface UserVipService extends IService<UserVip> {

    ResponseUtil openVIp(Integer userId, Integer vId);

    ResponseUtil membershipRecords(Integer userId,Integer type,Integer pageNum, Integer pageSize);

}
