package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.ToolLocalStorage;
import com.ruoyi.xqsp.service.ToolLocalStorageService;
import com.ruoyi.xqsp.mapper.ToolLocalStorageMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【tool_local_storage(本地存储)】的数据库操作Service实现
* @createDate 2022-08-09 23:30:50
*/
@Service
public class ToolLocalStorageServiceImpl extends ServiceImpl<ToolLocalStorageMapper, ToolLocalStorage>
    implements ToolLocalStorageService{

}




