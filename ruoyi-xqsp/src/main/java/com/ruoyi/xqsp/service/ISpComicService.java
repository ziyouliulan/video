package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.DatingUser;
import com.ruoyi.xqsp.domain.SpComic;

/**
 * 漫画操作Service接口
 *
 * @author ruoyi
 * @date 2021-04-28
 */
public interface ISpComicService extends IService<SpComic>
{
    /**
     * 查询漫画操作
     *
     * @param comicId 漫画操作ID
     * @return 漫画操作
     */
    public SpComic selectSpComicById(Long comicId);

    /**
     * 查询漫画操作列表
     *
     * @param spComic 漫画操作
     * @return 漫画操作集合
     */
    public List<SpComic> selectSpComicList(SpComic spComic);

    /**
     * 新增漫画操作
     *
     * @param spComic 漫画操作
     * @return 结果
     */
    public int insertSpComic(SpComic spComic);

    /**
     * 修改漫画操作
     *
     * @param spComic 漫画操作
     * @return 结果
     */
    public int updateSpComic(SpComic spComic);

    /**
     * 批量删除漫画操作
     *
     * @param comicIds 需要删除的漫画操作ID
     * @return 结果
     */
    public int deleteSpComicByIds(Long[] comicIds);

    /**
     * 删除漫画操作信息
     *
     * @param comicId 漫画操作ID
     * @return 结果
     */
    public int deleteSpComicById(Long comicId);

    /**
     * 根据id查询
     * @param comicId
     * @return
     */
    SpComic selectSpComicById1(Long comicId);

    /**
     * 根据分类查询
     * @param comicCategory
     * @return
     */
    List<SpComic> selectSpComicByCategory(String comicCategory);

    /**
     * 时间排序
     * @param spComic
     * @return
     */
    List<SpComic> selectSpComicListOrderTime(SpComic spComic);

    /**
     * 人气，根据观看人数排序
     * @param spComic
     * @return
     */
    List<SpComic> selectSpComicListOrderWatch(SpComic spComic);

    /**
     * 喜欢人数加一
     * @param comicId
     * @return
     */
    int updateSpComicByComicId(Long comicId);

    /**
     * 喜欢人数减一
     * @param comicId
     * @return
     */
    int updateSpComicByComicId1(Long comicId);

    /**
     * 观看人数
     * @param comicId
     * @return
     */
    int updateWatchByComicId(Long comicId);
}
