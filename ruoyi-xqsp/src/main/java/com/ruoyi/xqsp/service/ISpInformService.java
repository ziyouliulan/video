package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpInform;

/**
 * 系统通知表Service接口
 * 
 * @author ruoyi
 * @date 2021-04-21
 */
public interface ISpInformService 
{
    /**
     * 查询系统通知表
     * 
     * @param informId 系统通知表ID
     * @return 系统通知表
     */
    public SpInform selectSpInformById(Long informId);

    /**
     * 查询系统通知表列表
     * 
     * @param spInform 系统通知表
     * @return 系统通知表集合
     */
    public List<SpInform> selectSpInformList(SpInform spInform);

    /**
     * 新增系统通知表
     * 
     * @param spInform 系统通知表
     * @return 结果
     */
    public int insertSpInform(SpInform spInform);

    /**
     * 修改系统通知表
     * 
     * @param spInform 系统通知表
     * @return 结果
     */
    public int updateSpInform(SpInform spInform);

    /**
     * 批量删除系统通知表
     * 
     * @param informIds 需要删除的系统通知表ID
     * @return 结果
     */
    public int deleteSpInformByIds(Long[] informIds);

    /**
     * 删除系统通知表信息
     * 
     * @param informId 系统通知表ID
     * @return 结果
     */
    public int deleteSpInformById(Long informId);
}
