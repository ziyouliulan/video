package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpComicEvaluation;

/**
 * 漫画评价Service接口
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
public interface ISpComicEvaluationService 
{
    /**
     * 查询漫画评价
     * 
     * @param comicEvaluationId 漫画评价ID
     * @return 漫画评价
     */
    public SpComicEvaluation selectSpComicEvaluationById(Long comicEvaluationId);

    /**
     * 查询漫画评价列表
     * 
     * @param spComicEvaluation 漫画评价
     * @return 漫画评价集合
     */
    public List<SpComicEvaluation> selectSpComicEvaluationList(SpComicEvaluation spComicEvaluation);

    /**
     * 新增漫画评价
     * 
     * @param spComicEvaluation 漫画评价
     * @return 结果
     */
    public int insertSpComicEvaluation(SpComicEvaluation spComicEvaluation);

    /**
     * 修改漫画评价
     * 
     * @param spComicEvaluation 漫画评价
     * @return 结果
     */
    public int updateSpComicEvaluation(SpComicEvaluation spComicEvaluation);

    /**
     * 批量删除漫画评价
     * 
     * @param comicEvaluationIds 需要删除的漫画评价ID
     * @return 结果
     */
    public int deleteSpComicEvaluationByIds(Long[] comicEvaluationIds);

    /**
     * 删除漫画评价信息
     * 
     * @param comicEvaluationId 漫画评价ID
     * @return 结果
     */
    public int deleteSpComicEvaluationById(Long comicEvaluationId);

    /**
     * 查询漫画评论数量
     * @param comicId
     * @return
     */
    Integer selectSpComicEvaluationCountById(Long comicId);

    /**
     * 根据漫画id查询该漫画的评论
     * @param comicId
     * @return
     */
    List<SpComicEvaluation> selectSpComicEvaluationByComicId(Long comicId);

    /**
     * 漫画评论点赞人数加一
     * @param comicEvaluationId
     * @return
     */
    int updateSpComicEvaluationLike(Long comicEvaluationId);

    /**
     * 人数减一
     * @param comicEvaluationId
     * @return
     */
    int updateSpComicEvaluationLike1(Long comicEvaluationId);
}
