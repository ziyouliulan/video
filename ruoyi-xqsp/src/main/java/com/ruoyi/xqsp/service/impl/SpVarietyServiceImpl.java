package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVarietyMapper;
import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.service.ISpVarietyService;

/**
 * 综艺管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
@Service
public class SpVarietyServiceImpl implements ISpVarietyService 
{
    @Autowired
    private SpVarietyMapper spVarietyMapper;

    /**
     * 查询综艺管理
     * 
     * @param varietyId 综艺管理ID
     * @return 综艺管理
     */
    @Override
    public SpVariety selectSpVarietyById(Long varietyId)
    {
        return spVarietyMapper.selectSpVarietyById(varietyId);
    }

    /**
     * 查询综艺管理列表
     * 
     * @param spVariety 综艺管理
     * @return 综艺管理
     */
    @Override
    public List<SpVariety> selectSpVarietyList(SpVariety spVariety)
    {
        return spVarietyMapper.selectSpVarietyList(spVariety);
    }

    /**
     * 新增综艺管理
     * 
     * @param spVariety 综艺管理
     * @return 结果
     */
    @Override
    public int insertSpVariety(SpVariety spVariety)
    {
        return spVarietyMapper.insertSpVariety(spVariety);
    }

    /**
     * 修改综艺管理
     * 
     * @param spVariety 综艺管理
     * @return 结果
     */
    @Override
    public int updateSpVariety(SpVariety spVariety)
    {
        return spVarietyMapper.updateSpVariety(spVariety);
    }

    /**
     * 批量删除综艺管理
     * 
     * @param varietyIds 需要删除的综艺管理ID
     * @return 结果
     */
    @Override
    public int deleteSpVarietyByIds(Long[] varietyIds)
    {
        return spVarietyMapper.deleteSpVarietyByIds(varietyIds);
    }

    /**
     * 删除综艺管理信息
     * 
     * @param varietyId 综艺管理ID
     * @return 结果
     */
    @Override
    public int deleteSpVarietyById(Long varietyId)
    {
        return spVarietyMapper.deleteSpVarietyById(varietyId);
    }

    @Override
    public List<SpVariety> selectSpVarietySortWatch() {
        return spVarietyMapper.selectSpVarietySortWatch();
    }

    @Override
    public List<SpVariety> selectSpVarietySortLike() {
        return spVarietyMapper.selectSpVarietySortLike();
    }

    @Override
    public List<SpVariety> selectSpVarietyLists(SpVariety spVariety) {
        return spVarietyMapper.selectSpVarietyLists(spVariety);
    }
}
