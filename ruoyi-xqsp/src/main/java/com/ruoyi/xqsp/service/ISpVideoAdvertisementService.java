package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVideoAdvertisement;

/**
 * 视频广告Service接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface ISpVideoAdvertisementService 
{
    /**
     * 查询视频广告
     * 
     * @param videoAdvertisementId 视频广告ID
     * @return 视频广告
     */
    public SpVideoAdvertisement selectSpVideoAdvertisementById(Long videoAdvertisementId);

    /**
     * 查询视频广告列表
     * 
     * @param spVideoAdvertisement 视频广告
     * @return 视频广告集合
     */
    public List<SpVideoAdvertisement> selectSpVideoAdvertisementList(SpVideoAdvertisement spVideoAdvertisement);

    /**
     * 新增视频广告
     * 
     * @param spVideoAdvertisement 视频广告
     * @return 结果
     */
    public int insertSpVideoAdvertisement(SpVideoAdvertisement spVideoAdvertisement);

    /**
     * 修改视频广告
     * 
     * @param spVideoAdvertisement 视频广告
     * @return 结果
     */
    public int updateSpVideoAdvertisement(SpVideoAdvertisement spVideoAdvertisement);

    /**
     * 批量删除视频广告
     * 
     * @param videoAdvertisementIds 需要删除的视频广告ID
     * @return 结果
     */
    public int deleteSpVideoAdvertisementByIds(Long[] videoAdvertisementIds);

    /**
     * 删除视频广告信息
     * 
     * @param videoAdvertisementId 视频广告ID
     * @return 结果
     */
    public int deleteSpVideoAdvertisementById(Long videoAdvertisementId);
}
