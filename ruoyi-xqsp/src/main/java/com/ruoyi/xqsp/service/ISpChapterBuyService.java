package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpChapterBuy;

/**
 * 章节购买Service接口
 *
 * @author ruoyi
 * @date 2021-05-22
 */
public interface ISpChapterBuyService  extends IService<SpChapterBuy>
{
    /**
     * 查询章节购买
     *
     * @param chapterBuyId 章节购买ID
     * @return 章节购买
     */
    public SpChapterBuy selectSpChapterBuyById(Long chapterBuyId);

    /**
     * 查询章节购买列表
     *
     * @param spChapterBuy 章节购买
     * @return 章节购买集合
     */
    public List<SpChapterBuy> selectSpChapterBuyList(SpChapterBuy spChapterBuy);

    /**
     * 新增章节购买
     *
     * @param spChapterBuy 章节购买
     * @return 结果
     */
    public int insertSpChapterBuy(SpChapterBuy spChapterBuy);

    /**
     * 修改章节购买
     *
     * @param spChapterBuy 章节购买
     * @return 结果
     */
    public int updateSpChapterBuy(SpChapterBuy spChapterBuy);

    /**
     * 批量删除章节购买
     *
     * @param chapterBuyIds 需要删除的章节购买ID
     * @return 结果
     */
    public int deleteSpChapterBuyByIds(Long[] chapterBuyIds);

    /**
     * 删除章节购买信息
     *
     * @param chapterBuyId 章节购买ID
     * @return 结果
     */
    public int deleteSpChapterBuyById(Long chapterBuyId);

    /**
     * 查询已购买章节数量
     * @param spChapterBuy
     * @return
     */
    Long selectSpChapterBuyCount(SpChapterBuy spChapterBuy);
}
