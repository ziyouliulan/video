package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpConsumeRecord;
import com.ruoyi.xqsp.service.SpConsumeRecordService;
import com.ruoyi.xqsp.mapper.SpConsumeRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_consume_record(消费记录)】的数据库操作Service实现
* @createDate 2022-05-23 21:51:34
*/
@Service
public class SpConsumeRecordServiceImpl extends ServiceImpl<SpConsumeRecordMapper, SpConsumeRecord>
    implements SpConsumeRecordService{

}




