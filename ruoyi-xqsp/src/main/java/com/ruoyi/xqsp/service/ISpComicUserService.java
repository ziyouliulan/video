package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpComicUser;

/**
 * 书架管理Service接口
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
public interface ISpComicUserService 
{
    /**
     * 查询书架管理
     * 
     * @param comicUserId 书架管理ID
     * @return 书架管理
     */
    public SpComicUser selectSpComicUserById(Long comicUserId);

    /**
     * 查询书架管理列表
     * 
     * @param spComicUser 书架管理
     * @return 书架管理集合
     */
    public List<SpComicUser> selectSpComicUserList(SpComicUser spComicUser);

    /**
     * 新增书架管理
     * 
     * @param spComicUser 书架管理
     * @return 结果
     */
    public int insertSpComicUser(SpComicUser spComicUser);

    /**
     * 修改书架管理
     * 
     * @param spComicUser 书架管理
     * @return 结果
     */
    public int updateSpComicUser(SpComicUser spComicUser);

    /**
     * 批量删除书架管理
     * 
     * @param comicUserIds 需要删除的书架管理ID
     * @return 结果
     */
    public int deleteSpComicUserByIds(Long[] comicUserIds);

    /**
     * 删除书架管理信息
     * 
     * @param comicUserId 书架管理ID
     * @return 结果
     */
    public int deleteSpComicUserById(Long comicUserId);

    /**
     * 书架漫画删除
     * @param spComicUser
     * @return
     */
    int deleteSpComicUser(SpComicUser spComicUser);
}
