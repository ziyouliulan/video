package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.UserPhone;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【user_phone】的数据库操作Service
* @createDate 2022-07-26 20:34:55
*/
public interface UserPhoneService extends IService<UserPhone> {

}
