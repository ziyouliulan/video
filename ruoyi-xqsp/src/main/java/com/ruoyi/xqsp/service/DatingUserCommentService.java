package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.DatingUserComment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_dating_user_comment】的数据库操作Service
* @createDate 2022-02-23 17:25:50
*/
public interface DatingUserCommentService extends IService<DatingUserComment> {

    public List<DatingUserComment> byIdList(Long id);

}
