package com.ruoyi.xqsp.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpUsers;

/**
 * 用户表Service接口
 *
 * @author ruoyi
 * @date 2021-04-26
 */
public interface ISpUsersService extends IService<SpUsers>
{
    /**
     * 查询用户表
     *
     * @param userId 用户表ID
     * @return 用户表
     */
    public SpUsers selectSpUsersById(Long userId);

    /**
     * 查询用户表列表
     *
     * @param spUsers 用户表
     * @return 用户表集合
     */
    public List<SpUsers> selectSpUsersList(SpUsers spUsers);

    /**
     * 新增用户表
     *
     * @param spUsers 用户表
     * @return 结果
     */
    public int insertSpUsers(SpUsers spUsers);

    /**
     * 修改用户表
     *
     * @param spUsers 用户表
     * @return 结果
     */
    public int updateSpUsers(SpUsers spUsers);

    /**
     * 批量删除用户表
     *
     * @param userIds 需要删除的用户表ID
     * @return 结果
     */
    public int deleteSpUsersByIds(Long[] userIds);

    /**
     * 删除用户表信息
     *
     * @param userId 用户表ID
     * @return 结果
     */
    public int deleteSpUsersById(Long userId);

    /**
     * 获取月新增
     * @param spUsers
     * @return
     */
    List<SpUsers> selectSpUsersListMonth(SpUsers spUsers);

    /**
     * 判断xqid是否存在
     * @param xqid
     * @return
     */
    public SpUsers selectSpUsersByXqId(String xqid);

    /**
     * 通过注册用户的邀请码查询邀请该注册用户的用户
     * @param userPromoteCode
     * @return
     */
    int selectSpUsersCountByXqId(String userPromoteCode);

    /**
     * 给推广这增加会员天数
     * @param userPromoteCode
     * @param days
     * @return
     */
    public int updateSpUsersByxqid(String userPromoteCode ,int days);

    List<SpUsers> selectSpUsersListByXqid(String userXqid);

    /**
     * 根据用户id查询用户名字
     * @param userId
     * @return
     */
    SpUsers selectSpUserById(Long userId);

    /**
     * 观看次数减一
     * @param userId
     * @return
     */
    int updateSpUsersById(Long userId);

    /**
     * 用户金币数减少
     * @param userId
     * @param expenseCalendarGlod
     * @return
     */
    int updateUserGlod(Long userId, BigDecimal expenseCalendarGlod);

    /**
     * 会员天数每天减一
     * @return
     */
    int updatEmembersDay();

    /**
     * 刷新用户vip类型
     * */
    int refreshUserVipType();

    /**
     * 福利观看次数减一
     * @param userId
     * @return
     */
    int updateSpUsersWelfareById(Long userId);

    /**
     * 长视频福利观看次数减一
     * @param userId
     * @return
     */
    int updateSpUsersTodayLongById(Long userId);

    /**
     * 短视频福利观看次数减一
     * @param userId
     * @return
     */
    int updateSpUsersTodayShortById(Long userId);

    /**
     * 用户开启漫画自动购买
     * @param userId
     * @return
     */
    int updateSpUsersBuyTypeById(Long userId);

    /**
     * 用户关闭漫画自动购买
     * @param userId
     * @return
     */
    int updateSpUsersBuyTypeById1(Long userId);

    /**
     * 给用户增加会员天数
     * @param userId
     * @param days
     * @return
     */
    int updatMembersDayByUserId(Long userId, Long days);

    /**
     * 金币充值
     * @param userId
     * @param glod
     * @return
     */
    int updatGlodByUserId(Long userId, Long glod);

    /**
     * 给用户增加福利观看次数
     * @param userId
     * @param carmichaelDays
     * @return
     */
    int updateSpUsersWelfare(Long userId, Long carmichaelDays);

    /**
     * 给用户增加福利观看次数
     * @param userId
     * @param carmichaelDays
     * @return
     */
    int updateSpUsersWelfareLong(Long userId, Long carmichaelDays);

    int updateSpUsersMg(SpUsers spUsers);

    /**
     * 修改登录时间
     * @param userId
     * @param date
     * @return
     */
    int updateUserLoginState(Long userId, Date date);

    List<SpUsers> selectSpUsersList1();

    int updateUserLoginState1(Long userId);

    int updateUserLoginState12(Long userId);

    int addTimesWatched(Long userId,Long type);

}
