package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVideoWatchHistoryMapper;
import com.ruoyi.xqsp.domain.SpVideoWatchHistory;
import com.ruoyi.xqsp.service.ISpVideoWatchHistoryService;

/**
 * 视频观看历史Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-11
 */
@Service
public class SpVideoWatchHistoryServiceImpl  extends ServiceImpl<SpVideoWatchHistoryMapper,SpVideoWatchHistory> implements ISpVideoWatchHistoryService
{
    @Autowired
    private SpVideoWatchHistoryMapper spVideoWatchHistoryMapper;

    /**
     * 查询视频观看历史
     *
     * @param videoWatchHistoryId 视频观看历史ID
     * @return 视频观看历史
     */
    @Override
    public SpVideoWatchHistory selectSpVideoWatchHistoryById(Long videoWatchHistoryId)
    {
        return spVideoWatchHistoryMapper.selectSpVideoWatchHistoryById(videoWatchHistoryId);
    }

    /**
     * 查询视频观看历史列表
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 视频观看历史
     */
    @Override
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryList(SpVideoWatchHistory spVideoWatchHistory)
    {
        return spVideoWatchHistoryMapper.selectSpVideoWatchHistoryList(spVideoWatchHistory);
    }

    /**
     * 查询视频观看历史列表
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 视频观看历史
     */
    @Override
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryList2(SpVideoWatchHistory spVideoWatchHistory)
    {
        return spVideoWatchHistoryMapper.selectSpVideoWatchHistoryList2(spVideoWatchHistory);
    }

    /**
     * 新增视频观看历史
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 结果
     */
    @Override
    public int insertSpVideoWatchHistory(SpVideoWatchHistory spVideoWatchHistory)
    {
        try {
            return spVideoWatchHistoryMapper.insertSpVideoWatchHistory(spVideoWatchHistory);
        } catch (Exception e) {
            return -1;
//            throw new RuntimeException(e);
        }
    }

    /**
     * 修改视频观看历史
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 结果
     */
    @Override
    public int updateSpVideoWatchHistory(SpVideoWatchHistory spVideoWatchHistory)
    {
        return spVideoWatchHistoryMapper.updateSpVideoWatchHistory(spVideoWatchHistory);
    }

    /**
     * 批量删除视频观看历史
     *
     * @param videoWatchHistoryIds 需要删除的视频观看历史ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoWatchHistoryByIds(Long[] videoWatchHistoryIds)
    {
        return spVideoWatchHistoryMapper.deleteSpVideoWatchHistoryByIds(videoWatchHistoryIds);
    }

    /**
     * 删除视频观看历史信息
     *
     * @param videoWatchHistoryId 视频观看历史ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoWatchHistoryById(Long videoWatchHistoryId)
    {
        return spVideoWatchHistoryMapper.deleteSpVideoWatchHistoryById(videoWatchHistoryId);
    }

    @Override
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryByUserId(Long userId) {
        return spVideoWatchHistoryMapper.selectSpVideoWatchHistoryByUserId(userId);
    }

    @Override
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryByUserIdV2(Long userId) {
        return spVideoWatchHistoryMapper.selectSpVideoWatchHistoryByUserIdV2(userId);
    }

    @Override
    public int deleteSpVideoWatchHistory(Long videoId, Long type1) {
        return spVideoWatchHistoryMapper.deleteSpVideoWatchHistory(videoId,type1);
    }
}
