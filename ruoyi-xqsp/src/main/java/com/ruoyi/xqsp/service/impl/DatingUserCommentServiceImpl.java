package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.DatingUserComment;
import com.ruoyi.xqsp.service.DatingUserCommentService;
import com.ruoyi.xqsp.mapper.DatingUserCommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_dating_user_comment】的数据库操作Service实现
* @createDate 2022-02-23 17:25:50
*/
@Service
public class DatingUserCommentServiceImpl extends ServiceImpl<DatingUserCommentMapper, DatingUserComment>
    implements DatingUserCommentService{


    @Autowired
    DatingUserCommentMapper datingUserCommentMapper;

    @Override
    public List<DatingUserComment> byIdList(Long id) {

        return datingUserCommentMapper.byIdList(id);
    }
}




