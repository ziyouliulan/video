package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpCategoryTag;
import com.ruoyi.xqsp.service.SpCategoryTagService;
import com.ruoyi.xqsp.mapper.SpCategoryTagMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_category_tag】的数据库操作Service实现
* @createDate 2022-02-17 16:19:07
*/
@Service
public class SpCategoryTagServiceImpl extends ServiceImpl<SpCategoryTagMapper, SpCategoryTag>
    implements SpCategoryTagService{

}




