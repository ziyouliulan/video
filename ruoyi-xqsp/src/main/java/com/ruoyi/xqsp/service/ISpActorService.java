package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.domain.SpAdvertising;

/**
 * 演员管理Service接口
 *
 * @author ruoyi
 * @date 2021-04-17
 */
public interface ISpActorService  extends IService<SpActor>
{
    /**
     * 查询演员管理
     *
     * @param actorId 演员管理ID
     * @return 演员管理
     */
    public SpActor selectSpActorById(Long actorId);

    /**
     * 查询演员管理列表
     *
     * @param spActor 演员管理
     * @return 演员管理集合
     */
    public List<SpActor> selectSpActorList(SpActor spActor);

    /**
     * 新增演员管理
     *
     * @param spActor 演员管理
     * @return 结果
     */
    public int insertSpActor(SpActor spActor);

    /**
     * 修改演员管理
     *
     * @param spActor 演员管理
     * @return 结果
     */
    public int updateSpActor(SpActor spActor);

    /**
     * 批量删除演员管理
     *
     * @param actorIds 需要删除的演员管理ID
     * @return 结果
     */
    public int deleteSpActorByIds(Long[] actorIds);

    /**
     * 删除演员管理信息
     *
     * @param actorId 演员管理ID
     * @return 结果
     */
    public int deleteSpActorById(Long actorId);

    /**
     * 首页
     * @param spActor
     * @return
     */
    List<SpActor> selectSpActorLists(SpActor spActor);
}
