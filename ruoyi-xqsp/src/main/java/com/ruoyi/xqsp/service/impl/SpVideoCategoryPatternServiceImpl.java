package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;
import com.ruoyi.xqsp.service.SpVideoCategoryPatternService;
import com.ruoyi.xqsp.mapper.SpVideoCategoryPatternMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_video_category_pattern】的数据库操作Service实现
* @createDate 2022-02-15 16:20:21
*/
@Service
public class SpVideoCategoryPatternServiceImpl extends ServiceImpl<SpVideoCategoryPatternMapper, SpVideoCategoryPattern>
    implements SpVideoCategoryPatternService{

}




