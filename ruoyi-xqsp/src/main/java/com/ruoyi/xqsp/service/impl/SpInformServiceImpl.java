package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpInformMapper;
import com.ruoyi.xqsp.domain.SpInform;
import com.ruoyi.xqsp.service.ISpInformService;

/**
 * 系统通知表Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-21
 */
@Service
public class SpInformServiceImpl implements ISpInformService 
{
    @Autowired
    private SpInformMapper spInformMapper;

    /**
     * 查询系统通知表
     * 
     * @param informId 系统通知表ID
     * @return 系统通知表
     */
    @Override
    public SpInform selectSpInformById(Long informId)
    {
        return spInformMapper.selectSpInformById(informId);
    }

    /**
     * 查询系统通知表列表
     * 
     * @param spInform 系统通知表
     * @return 系统通知表
     */
    @Override
    public List<SpInform> selectSpInformList(SpInform spInform)
    {
        return spInformMapper.selectSpInformList(spInform);
    }

    /**
     * 新增系统通知表
     * 
     * @param spInform 系统通知表
     * @return 结果
     */
    @Override
    public int insertSpInform(SpInform spInform)
    {
        return spInformMapper.insertSpInform(spInform);
    }

    /**
     * 修改系统通知表
     * 
     * @param spInform 系统通知表
     * @return 结果
     */
    @Override
    public int updateSpInform(SpInform spInform)
    {
        return spInformMapper.updateSpInform(spInform);
    }

    /**
     * 批量删除系统通知表
     * 
     * @param informIds 需要删除的系统通知表ID
     * @return 结果
     */
    @Override
    public int deleteSpInformByIds(Long[] informIds)
    {
        return spInformMapper.deleteSpInformByIds(informIds);
    }

    /**
     * 删除系统通知表信息
     * 
     * @param informId 系统通知表ID
     * @return 结果
     */
    @Override
    public int deleteSpInformById(Long informId)
    {
        return spInformMapper.deleteSpInformById(informId);
    }
}
