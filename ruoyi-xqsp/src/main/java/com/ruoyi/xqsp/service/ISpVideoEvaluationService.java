package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;

/**
 * 视频评价Service接口
 * 
 * @author ruoyi
 * @date 2021-04-24
 */
public interface ISpVideoEvaluationService 
{
    /**
     * 查询视频评价
     * 
     * @param videoEvaluationId 视频评价ID
     * @return 视频评价
     */
    public SpVideoEvaluation selectSpVideoEvaluationById(Long videoEvaluationId);

    /**
     * 查询视频评价列表
     * 
     * @param spVideoEvaluation 视频评价
     * @return 视频评价集合
     */
    public List<SpVideoEvaluation> selectSpVideoEvaluationList(SpVideoEvaluation spVideoEvaluation);

    /**
     * 新增视频评价
     * 
     * @param spVideoEvaluation 视频评价
     * @return 结果
     */
    public int insertSpVideoEvaluation(SpVideoEvaluation spVideoEvaluation);

    /**
     * 修改视频评价
     * 
     * @param spVideoEvaluation 视频评价
     * @return 结果
     */
    public int updateSpVideoEvaluation(SpVideoEvaluation spVideoEvaluation);

    /**
     * 批量删除视频评价
     * 
     * @param videoEvaluationIds 需要删除的视频评价ID
     * @return 结果
     */
    public int deleteSpVideoEvaluationByIds(Long[] videoEvaluationIds);

    /**
     * 删除视频评价信息
     * 
     * @param videoEvaluationId 视频评价ID
     * @return 结果
     */
    public int deleteSpVideoEvaluationById(Long videoEvaluationId);

    /**
     * 根据视频id查询评价
     * @param videoId
     * @return
     */
    List<SpVideoEvaluation> selectSpVideoEvaluationListByVideoId(Long videoId);

    /**
     * 点赞人数加一
     * @param videoEvaluationId
     * @return
     */
    int updateSpVideoEvaluationLike(Long videoEvaluationId);

    /**
     * 取消点赞点赞人数减一
     * @param videoEvaluationId
     * @return
     */
    int updateSpVideoEvaluationLike1(Long videoEvaluationId);

    /**
     * 评论数量
     * @param videoId
     * @return
     */
    Long selectCountById(Long videoId);

    /**
     * 根据视频ID删除视频评论
     * @param videoId
     * @return
     */
    int deleteSpVideoEvaluationByVideoId(Long videoId);
}
