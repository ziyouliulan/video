package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpCollect;

/**
 * 用户收藏Service接口
 *
 * @author ruoyi
 * @date 2021-05-08
 */
public interface ISpCollectService extends IService<SpCollect>
{
    /**
     * 查询用户收藏
     *
     * @param collectId 用户收藏ID
     * @return 用户收藏
     */
    public SpCollect selectSpCollectById(Long collectId);

    /**
     * 查询用户收藏列表
     *
     * @param spCollect 用户收藏
     * @return 用户收藏集合
     */
    public List<SpCollect> selectSpCollectList(SpCollect spCollect);
    /**
     * 查询用户收藏列表
     *
     * @param spCollect 用户收藏
     * @return 用户收藏集合
     */
    public List<SpCollect> selectSpCollectListV2(SpCollect spCollect);

    /**
     * 新增用户收藏
     *
     * @param spCollect 用户收藏
     * @return 结果
     */
    public int insertSpCollect(SpCollect spCollect);

    /**
     * 修改用户收藏
     *
     * @param spCollect 用户收藏
     * @return 结果
     */
    public int updateSpCollect(SpCollect spCollect);

    /**
     * 批量删除用户收藏
     *
     * @param collectIds 需要删除的用户收藏ID
     * @return 结果
     */
    public int deleteSpCollectByIds(Long[] collectIds);

    /**
     * 删除用户收藏信息
     *
     * @param collectId 用户收藏ID
     * @return 结果
     */
    public int deleteSpCollectById(Long collectId);

    int deleteSpCollect(SpCollect spCollect);

    /**
     * 查看该用户是否收藏视频
     * @param userId
     * @param videoId
     * @return
     */
    SpCollect selectSpCollectByIds(Long userId, Long videoId);

    /**
     * 查询该用户是否收藏该综艺视频
     * @param userId
     * @param varietyVideoId
     * @return
     */
    SpCollect selectSpCollectByVarietyVideoId(Long userId, Long varietyVideoId);

    List<SpCollect> selectSpCollectByUserId(Long userId);

    SpCollect selectSpCollectByActorId(Long userId, Long actorId);

    List<SpCollect> selectSpCollectActorByUserId(Long userId);

    int deleteSpCollect1(Long actorId, long type);
}
