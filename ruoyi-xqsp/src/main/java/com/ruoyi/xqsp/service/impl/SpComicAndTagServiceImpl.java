package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicAndTagMapper;
import com.ruoyi.xqsp.domain.SpComicAndTag;
import com.ruoyi.xqsp.service.ISpComicAndTagService;

/**
 * 漫画标签中间表Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-24
 */
@Service
public class SpComicAndTagServiceImpl implements ISpComicAndTagService 
{
    @Autowired
    private SpComicAndTagMapper spComicAndTagMapper;

    /**
     * 查询漫画标签中间表
     * 
     * @param comicAndTagId 漫画标签中间表ID
     * @return 漫画标签中间表
     */
    @Override
    public SpComicAndTag selectSpComicAndTagById(Long comicAndTagId)
    {
        return spComicAndTagMapper.selectSpComicAndTagById(comicAndTagId);
    }

    /**
     * 查询漫画标签中间表列表
     * 
     * @param spComicAndTag 漫画标签中间表
     * @return 漫画标签中间表
     */
    @Override
    public List<SpComicAndTag> selectSpComicAndTagList(SpComicAndTag spComicAndTag)
    {
        return spComicAndTagMapper.selectSpComicAndTagList(spComicAndTag);
    }

    /**
     * 新增漫画标签中间表
     * 
     * @param spComicAndTag 漫画标签中间表
     * @return 结果
     */
    @Override
    public int insertSpComicAndTag(SpComicAndTag spComicAndTag)
    {
        return spComicAndTagMapper.insertSpComicAndTag(spComicAndTag);
    }

    /**
     * 修改漫画标签中间表
     * 
     * @param spComicAndTag 漫画标签中间表
     * @return 结果
     */
    @Override
    public int updateSpComicAndTag(SpComicAndTag spComicAndTag)
    {
        return spComicAndTagMapper.updateSpComicAndTag(spComicAndTag);
    }

    /**
     * 批量删除漫画标签中间表
     * 
     * @param comicAndTagIds 需要删除的漫画标签中间表ID
     * @return 结果
     */
    @Override
    public int deleteSpComicAndTagByIds(Long[] comicAndTagIds)
    {
        return spComicAndTagMapper.deleteSpComicAndTagByIds(comicAndTagIds);
    }

    /**
     * 删除漫画标签中间表信息
     * 
     * @param comicAndTagId 漫画标签中间表ID
     * @return 结果
     */
    @Override
    public int deleteSpComicAndTagById(Long comicAndTagId)
    {
        return spComicAndTagMapper.deleteSpComicAndTagById(comicAndTagId);
    }

    @Override
    public int deleteSpComicAndTagByComicId(Long comicId) {
        return spComicAndTagMapper.deleteSpComicAndTagByComicId(comicId);
    }

    @Override
    public List<SpComicAndTag> selectSpComicAndTagByComicId(Long comicId) {
        return spComicAndTagMapper.selectSpComicAndTagByComicId(comicId);
    }
}
