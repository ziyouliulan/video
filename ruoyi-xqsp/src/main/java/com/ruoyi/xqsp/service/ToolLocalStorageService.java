package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.ToolLocalStorage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【tool_local_storage(本地存储)】的数据库操作Service
* @createDate 2022-08-09 23:30:50
*/
public interface ToolLocalStorageService extends IService<ToolLocalStorage> {

}
