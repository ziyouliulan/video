package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpOpenImgMapper;
import com.ruoyi.xqsp.domain.SpOpenImg;
import com.ruoyi.xqsp.service.ISpOpenImgService;

/**
 * 开屏广告Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@Service
public class SpOpenImgServiceImpl implements ISpOpenImgService 
{
    @Autowired
    private SpOpenImgMapper spOpenImgMapper;

    /**
     * 查询开屏广告
     * 
     * @param openImgId 开屏广告ID
     * @return 开屏广告
     */
    @Override
    public SpOpenImg selectSpOpenImgById(Long openImgId)
    {
        return spOpenImgMapper.selectSpOpenImgById(openImgId);
    }

    /**
     * 查询开屏广告列表
     * 
     * @param spOpenImg 开屏广告
     * @return 开屏广告
     */
    @Override
    public List<SpOpenImg> selectSpOpenImgList(SpOpenImg spOpenImg)
    {
        return spOpenImgMapper.selectSpOpenImgList(spOpenImg);
    }

    /**
     * 新增开屏广告
     * 
     * @param spOpenImg 开屏广告
     * @return 结果
     */
    @Override
    public int insertSpOpenImg(SpOpenImg spOpenImg)
    {
        return spOpenImgMapper.insertSpOpenImg(spOpenImg);
    }

    /**
     * 修改开屏广告
     * 
     * @param spOpenImg 开屏广告
     * @return 结果
     */
    @Override
    public int updateSpOpenImg(SpOpenImg spOpenImg)
    {
        return spOpenImgMapper.updateSpOpenImg(spOpenImg);
    }

    /**
     * 批量删除开屏广告
     * 
     * @param openImgIds 需要删除的开屏广告ID
     * @return 结果
     */
    @Override
    public int deleteSpOpenImgByIds(Long[] openImgIds)
    {
        return spOpenImgMapper.deleteSpOpenImgByIds(openImgIds);
    }

    /**
     * 删除开屏广告信息
     * 
     * @param openImgId 开屏广告ID
     * @return 结果
     */
    @Override
    public int deleteSpOpenImgById(Long openImgId)
    {
        return spOpenImgMapper.deleteSpOpenImgById(openImgId);
    }
}
