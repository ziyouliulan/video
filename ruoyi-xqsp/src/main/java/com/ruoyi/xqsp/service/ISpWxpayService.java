package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpWxpay;

/**
 * 微信支付Service接口
 * 
 * @author ruoyi
 * @date 2021-06-01
 */
public interface ISpWxpayService 
{
    /**
     * 查询微信支付
     * 
     * @param wxpayId 微信支付ID
     * @return 微信支付
     */
    public SpWxpay selectSpWxpayById(Long wxpayId);

    /**
     * 查询微信支付列表
     * 
     * @param spWxpay 微信支付
     * @return 微信支付集合
     */
    public List<SpWxpay> selectSpWxpayList(SpWxpay spWxpay);

    /**
     * 新增微信支付
     * 
     * @param spWxpay 微信支付
     * @return 结果
     */
    public int insertSpWxpay(SpWxpay spWxpay);

    /**
     * 修改微信支付
     * 
     * @param spWxpay 微信支付
     * @return 结果
     */
    public int updateSpWxpay(SpWxpay spWxpay);

    /**
     * 批量删除微信支付
     * 
     * @param wxpayIds 需要删除的微信支付ID
     * @return 结果
     */
    public int deleteSpWxpayByIds(Long[] wxpayIds);

    /**
     * 删除微信支付信息
     * 
     * @param wxpayId 微信支付ID
     * @return 结果
     */
    public int deleteSpWxpayById(Long wxpayId);
}
