package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpUnlike;

/**
 * 差评Service接口
 * 
 * @author ruoyi
 * @date 2021-05-08
 */
public interface ISpUnlikeService 
{
    /**
     * 查询差评
     * 
     * @param unlikeId 差评ID
     * @return 差评
     */
    public SpUnlike selectSpUnlikeById(Long unlikeId);

    /**
     * 查询差评列表
     * 
     * @param spUnlike 差评
     * @return 差评集合
     */
    public List<SpUnlike> selectSpUnlikeList(SpUnlike spUnlike);

    /**
     * 新增差评
     * 
     * @param spUnlike 差评
     * @return 结果
     */
    public int insertSpUnlike(SpUnlike spUnlike);

    /**
     * 修改差评
     * 
     * @param spUnlike 差评
     * @return 结果
     */
    public int updateSpUnlike(SpUnlike spUnlike);

    /**
     * 批量删除差评
     * 
     * @param unlikeIds 需要删除的差评ID
     * @return 结果
     */
    public int deleteSpUnlikeByIds(Long[] unlikeIds);

    /**
     * 删除差评信息
     * 
     * @param unlikeId 差评ID
     * @return 结果
     */
    public int deleteSpUnlikeById(Long unlikeId);

    /**
     * 删除差评根据用户id和videoid
     * @param spUnlike
     * @return
     */
    int deleteSpUnlike(SpUnlike spUnlike);

    /**
     * 查询该用户是否对视频差评
     * @param userId
     * @param videoId
     * @return
     */
    SpUnlike selectSpUnlikeByIds(Long userId, Long videoId);

    /**
     * 查询该用户是否对该综艺视频差评
     * @param userId
     * @param varietyVideoId
     * @return
     */
    SpUnlike selectSpUnlikeByVarietyVideoId(Long userId, Long varietyVideoId);

    /**
     * 综艺视频茶品删除
     * @param spUnlike
     * @return
     */
    int deleteVarietyVideoUnlike(SpUnlike spUnlike);
}
