package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.UserPhone;
import com.ruoyi.xqsp.service.UserPhoneService;
import com.ruoyi.xqsp.mapper.UserPhoneMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【user_phone】的数据库操作Service实现
* @createDate 2022-07-26 20:34:55
*/
@Service
public class UserPhoneServiceImpl extends ServiceImpl<UserPhoneMapper, UserPhone>
    implements UserPhoneService{

}




