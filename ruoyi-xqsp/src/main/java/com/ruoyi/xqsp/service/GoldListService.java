package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.GoldList;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_gold_list】的数据库操作Service
* @createDate 2022-08-05 19:00:16
*/
public interface GoldListService extends IService<GoldList> {

}
