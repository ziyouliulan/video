package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpVideo;
import com.ruoyi.xqsp.mapper.SpVideoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicChapterMapper;
import com.ruoyi.xqsp.domain.SpComicChapter;
import com.ruoyi.xqsp.service.ISpComicChapterService;

/**
 * 漫画章节Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
@Service
public class SpComicChapterServiceImpl extends ServiceImpl<SpComicChapterMapper, SpComicChapter> implements ISpComicChapterService
{
    @Autowired
    private SpComicChapterMapper spComicChapterMapper;

    /**
     * 查询漫画章节
     * 
     * @param comicChapterId 漫画章节ID
     * @return 漫画章节
     */
    @Override
    public SpComicChapter selectSpComicChapterById(Long comicChapterId)
    {
        return spComicChapterMapper.selectSpComicChapterById(comicChapterId);
    }

    /**
     * 查询漫画章节列表
     * 
     * @param spComicChapter 漫画章节
     * @return 漫画章节
     */
    @Override
    public List<SpComicChapter> selectSpComicChapterList(SpComicChapter spComicChapter)
    {
        return spComicChapterMapper.selectSpComicChapterList(spComicChapter);
    }

    /**
     * 新增漫画章节
     * 
     * @param spComicChapter 漫画章节
     * @return 结果
     */
    @Override
    public int insertSpComicChapter(SpComicChapter spComicChapter)
    {
        return spComicChapterMapper.insertSpComicChapter(spComicChapter);
    }

    /**
     * 修改漫画章节
     * 
     * @param spComicChapter 漫画章节
     * @return 结果
     */
    @Override
    public int updateSpComicChapter(SpComicChapter spComicChapter)
    {
        return spComicChapterMapper.updateSpComicChapter(spComicChapter);
    }

    /**
     * 批量删除漫画章节
     * 
     * @param comicChapterIds 需要删除的漫画章节ID
     * @return 结果
     */
    @Override
    public int deleteSpComicChapterByIds(Long[] comicChapterIds)
    {
        return spComicChapterMapper.deleteSpComicChapterByIds(comicChapterIds);
    }

    /**
     * 删除漫画章节信息
     * 
     * @param comicChapterId 漫画章节ID
     * @return 结果
     */
    @Override
    public int deleteSpComicChapterById(Long comicChapterId)
    {
        return spComicChapterMapper.deleteSpComicChapterById(comicChapterId);
    }

    /**
     * 根据漫画id查询章节
     * @param comicId
     * @return
     */
    @Override
    public List<SpComicChapter> selectSpComicChapterByComicId(Long comicId) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("comic_id",comicId);
        queryWrapper.orderByAsc("serial_number");
        return spComicChapterMapper.selectList(queryWrapper);
//        return spComicChapterMapper.selectSpComicChapterByComicId(comicId);
    }

    @Override
    public Long selectSpComicChapterCountByComicId(Long comicId) {
        return spComicChapterMapper.selectSpComicChapterCountByComicId(comicId);
    }

    @Override
    public SpComicChapter selectSpComicChapter(SpComicChapter spComicChapter) {
        return spComicChapterMapper.selectSpComicChapter(spComicChapter);
    }

    @Override
    public List<SpComicChapter> selectSpComicChapterByComicId1(SpComicChapter spComicChapter) {
        return spComicChapterMapper.selectSpComicChapterByComicId1(spComicChapter);
    }

    @Override
    public List<SpComicChapter> selectSerialNumber(Long comicId ,Integer SerialNumber, Integer number) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.ge("comic_chapter_id",SerialNumber);
        wrapper.eq("comic_id",comicId);
        if (number != -1){
            Page<SpComicChapter> page= new Page<>(0,number);
            return spComicChapterMapper.selectPage(page,wrapper).getRecords();
        }else {
            return spComicChapterMapper.selectList(wrapper);
        }
    }
}
