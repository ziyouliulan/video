package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.DatingUser;
import com.ruoyi.xqsp.service.DatingUserService;
import com.ruoyi.xqsp.mapper.DatingUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_dating_user(用户信息表)】的数据库操作Service实现
* @createDate 2022-07-05 21:23:08
*/
@Service
public class DatingUserServiceImpl extends ServiceImpl<DatingUserMapper, DatingUser>
    implements DatingUserService{

    @Autowired
    DatingUserMapper datingUserMapper;
    @Override
    public List<DatingUser> favoritesList(Integer userId) {
        return datingUserMapper.favoritesList(userId);
    }
}




