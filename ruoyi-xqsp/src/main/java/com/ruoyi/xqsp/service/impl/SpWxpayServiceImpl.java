package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpWxpayMapper;
import com.ruoyi.xqsp.domain.SpWxpay;
import com.ruoyi.xqsp.service.ISpWxpayService;

/**
 * 微信支付Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-01
 */
@Service
public class SpWxpayServiceImpl implements ISpWxpayService 
{
    @Autowired
    private SpWxpayMapper spWxpayMapper;

    /**
     * 查询微信支付
     * 
     * @param wxpayId 微信支付ID
     * @return 微信支付
     */
    @Override
    public SpWxpay selectSpWxpayById(Long wxpayId)
    {
        return spWxpayMapper.selectSpWxpayById(wxpayId);
    }

    /**
     * 查询微信支付列表
     * 
     * @param spWxpay 微信支付
     * @return 微信支付
     */
    @Override
    public List<SpWxpay> selectSpWxpayList(SpWxpay spWxpay)
    {
        return spWxpayMapper.selectSpWxpayList(spWxpay);
    }

    /**
     * 新增微信支付
     * 
     * @param spWxpay 微信支付
     * @return 结果
     */
    @Override
    public int insertSpWxpay(SpWxpay spWxpay)
    {
        return spWxpayMapper.insertSpWxpay(spWxpay);
    }

    /**
     * 修改微信支付
     * 
     * @param spWxpay 微信支付
     * @return 结果
     */
    @Override
    public int updateSpWxpay(SpWxpay spWxpay)
    {
        return spWxpayMapper.updateSpWxpay(spWxpay);
    }

    /**
     * 批量删除微信支付
     * 
     * @param wxpayIds 需要删除的微信支付ID
     * @return 结果
     */
    @Override
    public int deleteSpWxpayByIds(Long[] wxpayIds)
    {
        return spWxpayMapper.deleteSpWxpayByIds(wxpayIds);
    }

    /**
     * 删除微信支付信息
     * 
     * @param wxpayId 微信支付ID
     * @return 结果
     */
    @Override
    public int deleteSpWxpayById(Long wxpayId)
    {
        return spWxpayMapper.deleteSpWxpayById(wxpayId);
    }
}
