package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.City;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_city】的数据库操作Service
* @createDate 2022-07-05 21:19:26
*/
public interface CityService extends IService<City> {

}
