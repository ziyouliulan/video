package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpPhoenixEvaluationMapper;
import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import com.ruoyi.xqsp.service.ISpPhoenixEvaluationService;

/**
 * 楼凤评价表Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-25
 */
@Service
public class SpPhoenixEvaluationServiceImpl implements ISpPhoenixEvaluationService 
{
    @Autowired
    private SpPhoenixEvaluationMapper spPhoenixEvaluationMapper;

    /**
     * 查询楼凤评价表
     * 
     * @param phoenixEvaluationId 楼凤评价表ID
     * @return 楼凤评价表
     */
    @Override
    public SpPhoenixEvaluation selectSpPhoenixEvaluationById(Long phoenixEvaluationId)
    {
        return spPhoenixEvaluationMapper.selectSpPhoenixEvaluationById(phoenixEvaluationId);
    }

    /**
     * 查询楼凤评价表列表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 楼凤评价表
     */
    @Override
    public List<SpPhoenixEvaluation> selectSpPhoenixEvaluationList(SpPhoenixEvaluation spPhoenixEvaluation)
    {
        return spPhoenixEvaluationMapper.selectSpPhoenixEvaluationList(spPhoenixEvaluation);
    }

    /**
     * 新增楼凤评价表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 结果
     */
    @Override
    public int insertSpPhoenixEvaluation(SpPhoenixEvaluation spPhoenixEvaluation)
    {
        return spPhoenixEvaluationMapper.insertSpPhoenixEvaluation(spPhoenixEvaluation);
    }

    /**
     * 修改楼凤评价表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 结果
     */
    @Override
    public int updateSpPhoenixEvaluation(SpPhoenixEvaluation spPhoenixEvaluation)
    {
        return spPhoenixEvaluationMapper.updateSpPhoenixEvaluation(spPhoenixEvaluation);
    }

    /**
     * 批量删除楼凤评价表
     * 
     * @param phoenixEvaluationIds 需要删除的楼凤评价表ID
     * @return 结果
     */
    @Override
    public int deleteSpPhoenixEvaluationByIds(Long[] phoenixEvaluationIds)
    {
        return spPhoenixEvaluationMapper.deleteSpPhoenixEvaluationByIds(phoenixEvaluationIds);
    }

    /**
     * 删除楼凤评价表信息
     * 
     * @param phoenixEvaluationId 楼凤评价表ID
     * @return 结果
     */
    @Override
    public int deleteSpPhoenixEvaluationById(Long phoenixEvaluationId)
    {
        return spPhoenixEvaluationMapper.deleteSpPhoenixEvaluationById(phoenixEvaluationId);
    }

    @Override
    public List<SpPhoenixEvaluation> selectSpPhoenixEvaluationByPhoenixId(Long phoenixId) {
        return spPhoenixEvaluationMapper.selectSpPhoenixEvaluationByPhoneixId(phoenixId);
    }

    @Override
    public int deleteSpPhoenixEvaluationByPhoenixId(Long phoenixId) {
        return spPhoenixEvaluationMapper.deleteSpPhoenixEvaluationByPhoenixId(phoenixId);
    }

}
