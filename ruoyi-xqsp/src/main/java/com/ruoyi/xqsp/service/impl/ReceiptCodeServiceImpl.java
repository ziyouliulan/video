package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.ReceiptCode;
import com.ruoyi.xqsp.service.ReceiptCodeService;
import com.ruoyi.xqsp.mapper.ReceiptCodeMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_receipt_code】的数据库操作Service实现
* @createDate 2022-08-05 19:55:54
*/
@Service
public class ReceiptCodeServiceImpl extends ServiceImpl<ReceiptCodeMapper, ReceiptCode>
    implements ReceiptCodeService{

}




