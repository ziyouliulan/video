package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_category】的数据库操作Service
* @createDate 2022-02-16 15:29:48
*/
public interface SpCategoryService extends IService<SpCategory> {

}
