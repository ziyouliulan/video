package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpComicImurl;
import com.ruoyi.xqsp.mapper.SpComicImurlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpChapterBuyMapper;
import com.ruoyi.xqsp.domain.SpChapterBuy;
import com.ruoyi.xqsp.service.ISpChapterBuyService;

/**
 * 章节购买Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-22
 */
@Service
public class SpChapterBuyServiceImpl extends ServiceImpl<SpChapterBuyMapper, SpChapterBuy> implements ISpChapterBuyService
{
    @Autowired
    private SpChapterBuyMapper spChapterBuyMapper;

    /**
     * 查询章节购买
     *
     * @param chapterBuyId 章节购买ID
     * @return 章节购买
     */
    @Override
    public SpChapterBuy selectSpChapterBuyById(Long chapterBuyId)
    {
        return spChapterBuyMapper.selectSpChapterBuyById(chapterBuyId);
    }

    /**
     * 查询章节购买列表
     *
     * @param spChapterBuy 章节购买
     * @return 章节购买
     */
    @Override
    public List<SpChapterBuy> selectSpChapterBuyList(SpChapterBuy spChapterBuy)
    {
        return spChapterBuyMapper.selectSpChapterBuyList(spChapterBuy);
    }

    /**
     * 新增章节购买
     *
     * @param spChapterBuy 章节购买
     * @return 结果
     */
    @Override
    public int insertSpChapterBuy(SpChapterBuy spChapterBuy)
    {
        return spChapterBuyMapper.insertSpChapterBuy(spChapterBuy);
    }

    /**
     * 修改章节购买
     *
     * @param spChapterBuy 章节购买
     * @return 结果
     */
    @Override
    public int updateSpChapterBuy(SpChapterBuy spChapterBuy)
    {
        return spChapterBuyMapper.updateSpChapterBuy(spChapterBuy);
    }

    /**
     * 批量删除章节购买
     *
     * @param chapterBuyIds 需要删除的章节购买ID
     * @return 结果
     */
    @Override
    public int deleteSpChapterBuyByIds(Long[] chapterBuyIds)
    {
        return spChapterBuyMapper.deleteSpChapterBuyByIds(chapterBuyIds);
    }

    /**
     * 删除章节购买信息
     *
     * @param chapterBuyId 章节购买ID
     * @return 结果
     */
    @Override
    public int deleteSpChapterBuyById(Long chapterBuyId)
    {
        return spChapterBuyMapper.deleteSpChapterBuyById(chapterBuyId);
    }

    @Override
    public Long selectSpChapterBuyCount(SpChapterBuy spChapterBuy) {
        return spChapterBuyMapper.selectSpChapterBuyCount(spChapterBuy);
    }
}
