package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpExpenseCalendarMapper;
import com.ruoyi.xqsp.domain.SpExpenseCalendar;
import com.ruoyi.xqsp.service.ISpExpenseCalendarService;

/**
 * 消费记录Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-08
 */
@Service
public class SpExpenseCalendarServiceImpl extends ServiceImpl<SpExpenseCalendarMapper,SpExpenseCalendar> implements ISpExpenseCalendarService
{
    @Autowired
    private SpExpenseCalendarMapper spExpenseCalendarMapper;

    /**
     * 查询消费记录
     *
     * @param expenseCalendarId 消费记录ID
     * @return 消费记录
     */
    @Override
    public SpExpenseCalendar selectSpExpenseCalendarById(Long expenseCalendarId)
    {
        return spExpenseCalendarMapper.selectSpExpenseCalendarById(expenseCalendarId);
    }

    /**
     * 查询消费记录列表
     *
     * @param spExpenseCalendar 消费记录
     * @return 消费记录
     */
    @Override
    public List<SpExpenseCalendar> selectSpExpenseCalendarList(SpExpenseCalendar spExpenseCalendar)
    {
        return spExpenseCalendarMapper.selectSpExpenseCalendarList(spExpenseCalendar);
    }

    /**
     * 新增消费记录
     *
     * @param spExpenseCalendar 消费记录
     * @return 结果
     */
    @Override
    public int insertSpExpenseCalendar(SpExpenseCalendar spExpenseCalendar)
    {
        return spExpenseCalendarMapper.insertSpExpenseCalendar(spExpenseCalendar);
    }

    /**
     * 修改消费记录
     *
     * @param spExpenseCalendar 消费记录
     * @return 结果
     */
    @Override
    public int updateSpExpenseCalendar(SpExpenseCalendar spExpenseCalendar)
    {
        return spExpenseCalendarMapper.updateSpExpenseCalendar(spExpenseCalendar);
    }

    /**
     * 批量删除消费记录
     *
     * @param expenseCalendarIds 需要删除的消费记录ID
     * @return 结果
     */
    @Override
    public int deleteSpExpenseCalendarByIds(Long[] expenseCalendarIds)
    {
        return spExpenseCalendarMapper.deleteSpExpenseCalendarByIds(expenseCalendarIds);
    }

    /**
     * 删除消费记录信息
     *
     * @param expenseCalendarId 消费记录ID
     * @return 结果
     */
    @Override
    public int deleteSpExpenseCalendarById(Long expenseCalendarId)
    {
        return spExpenseCalendarMapper.deleteSpExpenseCalendarById(expenseCalendarId);
    }

    @Override
    public SpExpenseCalendar selectSpExpenseCalendarByExpenseCalendarOrder(String expenseCalendarOrder) {
        return spExpenseCalendarMapper.selectSpExpenseCalendarByExpenseCalendarOrder(expenseCalendarOrder);
    }

    @Override
    public List<SpExpenseCalendar> selectSpExpenseCalendarByUserId(Long userId) {
        return spExpenseCalendarMapper.selectSpExpenseCalendarByUserId(userId);
    }

    @Override
    public Integer selectSpExpenseCalendarByCount(Long videoId, int i) {
        return spExpenseCalendarMapper.selectSpExpenseCalendarByCount(videoId,i);
    }


}
