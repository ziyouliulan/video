package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVarietyVideoEvaluationMapper;
import com.ruoyi.xqsp.domain.SpVarietyVideoEvaluation;
import com.ruoyi.xqsp.service.ISpVarietyVideoEvaluationService;

/**
 * 综艺视频评论Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@Service
public class SpVarietyVideoEvaluationServiceImpl implements ISpVarietyVideoEvaluationService
{
    @Autowired
    private SpVarietyVideoEvaluationMapper spVarietyVideoEvaluationMapper;

    /**
     * 查询综艺视频评论
     *
     * @param varietyVideoEvaluationId 综艺视频评论ID
     * @return 综艺视频评论
     */
    @Override
    public SpVarietyVideoEvaluation selectSpVarietyVideoEvaluationById(Long varietyVideoEvaluationId)
    {
        return spVarietyVideoEvaluationMapper.selectSpVarietyVideoEvaluationById(varietyVideoEvaluationId);
    }

    /**
     * 查询综艺视频评论列表
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 综艺视频评论
     */
    @Override
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationList(SpVarietyVideoEvaluation spVarietyVideoEvaluation)
    {
        return spVarietyVideoEvaluationMapper.selectSpVarietyVideoEvaluationList(spVarietyVideoEvaluation);
    }

    /**
     * 查询综艺视频评论列表
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 综艺视频评论
     */
    @Override
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationListV2(Integer id)
    {
        return spVarietyVideoEvaluationMapper.selectSpVarietyVideoEvaluationListV2(id);
    }

    /**
     * 新增综艺视频评论
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 结果
     */
    @Override
    public int insertSpVarietyVideoEvaluation(SpVarietyVideoEvaluation spVarietyVideoEvaluation)
    {
        return spVarietyVideoEvaluationMapper.insertSpVarietyVideoEvaluation(spVarietyVideoEvaluation);
    }

    /**
     * 修改综艺视频评论
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 结果
     */
    @Override
    public int updateSpVarietyVideoEvaluation(SpVarietyVideoEvaluation spVarietyVideoEvaluation)
    {
        return spVarietyVideoEvaluationMapper.updateSpVarietyVideoEvaluation(spVarietyVideoEvaluation);
    }

    /**
     * 批量删除综艺视频评论
     *
     * @param varietyVideoEvaluationIds 需要删除的综艺视频评论ID
     * @return 结果
     */
    @Override
    public int deleteSpVarietyVideoEvaluationByIds(Long[] varietyVideoEvaluationIds)
    {
        return spVarietyVideoEvaluationMapper.deleteSpVarietyVideoEvaluationByIds(varietyVideoEvaluationIds);
    }

    /**
     * 删除综艺视频评论信息
     *
     * @param varietyVideoEvaluationId 综艺视频评论ID
     * @return 结果
     */
    @Override
    public int deleteSpVarietyVideoEvaluationById(Long varietyVideoEvaluationId)
    {
        return spVarietyVideoEvaluationMapper.deleteSpVarietyVideoEvaluationById(varietyVideoEvaluationId);
    }

    @Override
    public Long selectCountById(Long varietyVideoId) {
        return spVarietyVideoEvaluationMapper.selectCountById(varietyVideoId);
    }

    @Override
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationByIds(Long varietyVideoId) {
        return spVarietyVideoEvaluationMapper.selectSpVarietyVideoEvaluationByIds(varietyVideoId);
    }

    @Override
    public int updateSpVarietyVideoEvaluationLike(Long varietyVideoEvaluationId) {
        return spVarietyVideoEvaluationMapper.updateSpVarietyVideoEvaluationLike(varietyVideoEvaluationId);
    }

    @Override
    public int updateSpVarietyVideoEvaluationLike1(Long varietyVideoEvaluationId) {
        return spVarietyVideoEvaluationMapper.updateSpVarietyVideoEvaluationLike1(varietyVideoEvaluationId);
    }

    @Override
    public int deleteSpVarietyVideoEvaluationByVvId(Long varietyVideoId) {
        return spVarietyVideoEvaluationMapper.deleteSpVarietyVideoEvaluationByVvId(varietyVideoId);
    }
}
