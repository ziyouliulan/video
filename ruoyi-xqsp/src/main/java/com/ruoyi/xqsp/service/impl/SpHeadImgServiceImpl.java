package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpHeadImgMapper;
import com.ruoyi.xqsp.domain.SpHeadImg;
import com.ruoyi.xqsp.service.ISpHeadImgService;

/**
 * 用户头像Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@Service
public class SpHeadImgServiceImpl implements ISpHeadImgService 
{
    @Autowired
    private SpHeadImgMapper spHeadImgMapper;

    /**
     * 查询用户头像
     * 
     * @param headImgId 用户头像ID
     * @return 用户头像
     */
    @Override
    public SpHeadImg selectSpHeadImgById(Long headImgId)
    {
        return spHeadImgMapper.selectSpHeadImgById(headImgId);
    }

    /**
     * 查询用户头像列表
     * 
     * @param spHeadImg 用户头像
     * @return 用户头像
     */
    @Override
    public List<SpHeadImg> selectSpHeadImgList(SpHeadImg spHeadImg)
    {
        return spHeadImgMapper.selectSpHeadImgList(spHeadImg);
    }

    /**
     * 新增用户头像
     * 
     * @param spHeadImg 用户头像
     * @return 结果
     */
    @Override
    public int insertSpHeadImg(SpHeadImg spHeadImg)
    {
        return spHeadImgMapper.insertSpHeadImg(spHeadImg);
    }

    /**
     * 修改用户头像
     * 
     * @param spHeadImg 用户头像
     * @return 结果
     */
    @Override
    public int updateSpHeadImg(SpHeadImg spHeadImg)
    {
        return spHeadImgMapper.updateSpHeadImg(spHeadImg);
    }

    /**
     * 批量删除用户头像
     * 
     * @param headImgIds 需要删除的用户头像ID
     * @return 结果
     */
    @Override
    public int deleteSpHeadImgByIds(Long[] headImgIds)
    {
        return spHeadImgMapper.deleteSpHeadImgByIds(headImgIds);
    }

    /**
     * 删除用户头像信息
     * 
     * @param headImgId 用户头像ID
     * @return 结果
     */
    @Override
    public int deleteSpHeadImgById(Long headImgId)
    {
        return spHeadImgMapper.deleteSpHeadImgById(headImgId);
    }

    @Override
    public SpHeadImg selectSpHeadImg() {

        return spHeadImgMapper.selectSpHeadImg();
    }
}
