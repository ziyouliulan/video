package com.ruoyi.xqsp.service.impl;

import java.util.List;
import java.util.Set;

import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpPhoenixServiceMapper;
import com.ruoyi.xqsp.domain.SpPhoenixService;
import com.ruoyi.xqsp.service.ISpPhoenixServiceService;

/**
 * 楼凤操作Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-26
 */
@Service
public class SpPhoenixServiceServiceImpl implements ISpPhoenixServiceService 
{
    @Autowired
    private SpPhoenixServiceMapper spPhoenixServiceMapper;

    /**
     * 查询楼凤操作
     * 
     * @param phoenixId 楼凤操作ID
     * @return 楼凤操作
     */
    @Override
    public SpPhoenixService selectSpPhoenixServiceById(Long phoenixId)
    {
        return spPhoenixServiceMapper.selectSpPhoenixServiceById(phoenixId);
    }

    /**
     * 查询楼凤操作列表
     * 
     * @param spPhoenixService 楼凤操作
     * @return 楼凤操作
     */
    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceList(SpPhoenixService spPhoenixService)
    {
        return spPhoenixServiceMapper.selectSpPhoenixServiceList(spPhoenixService);
    }

    /**
     * 新增楼凤操作
     * 
     * @param spPhoenixService 楼凤操作
     * @return 结果
     */
    @Override
    public int insertSpPhoenixService(SpPhoenixService spPhoenixService)
    {
        return spPhoenixServiceMapper.insertSpPhoenixService(spPhoenixService);
    }

    /**
     * 修改楼凤操作
     * 
     * @param spPhoenixService 楼凤操作
     * @return 结果
     */
    @Override
    public int updateSpPhoenixService(SpPhoenixService spPhoenixService)
    {
        return spPhoenixServiceMapper.updateSpPhoenixService(spPhoenixService);
    }

    /**
     * 批量删除楼凤操作
     * 
     * @param phoenixIds 需要删除的楼凤操作ID
     * @return 结果
     */
    @Override
    public int deleteSpPhoenixServiceByIds(Long[] phoenixIds)
    {
        return spPhoenixServiceMapper.deleteSpPhoenixServiceByIds(phoenixIds);
    }

    /**
     * 删除楼凤操作信息
     * 
     * @param phoenixId 楼凤操作ID
     * @return 结果
     */
    @Override
    public int deleteSpPhoenixServiceById(Long phoenixId)
    {
        return spPhoenixServiceMapper.deleteSpPhoenixServiceById(phoenixId);
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceListAudit(SpPhoenixService spPhoenixService) {
        return spPhoenixServiceMapper.selectSpPhoenixServiceListAudit(spPhoenixService);
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixService() {
        return spPhoenixServiceMapper.selectSpPhoenixService();
    }

    @Override
    public int updateSpPhoenixServiceLike(Long phoenixId) {
        return spPhoenixServiceMapper.updateSpPhoenixServiceLike();
    }

    @Override
    public int updateSpPhoenixServiceLike1(Long phoenixId) {
        return spPhoenixServiceMapper.updateSpPhoenixServiceLike1(phoenixId);
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceOrderTime() {
        return spPhoenixServiceMapper.selectSpPhoenixServiceOrderTime();
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceByAddress(String area) {
        return spPhoenixServiceMapper.selectSpPhoenixServiceByAddress(area);
    }

    @Override
    public int updateSpPhoenixServiceUnlockNum(Long expenseCalendarState) {
        return spPhoenixServiceMapper.updateSpPhoenixServiceUnlockNum(expenseCalendarState);
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceByName(String phoenixName) {
        return spPhoenixServiceMapper.selectSpPhoenixServiceByName(phoenixName);
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceByAddressOrderTime(String area) {
        return spPhoenixServiceMapper.selectSpPhoenixServiceByAddressOrderTime(area);
    }

    @Override
    public List<SpPhoenixService> selectSpPhoenixServiceListByIds(Set<Long> ids1) {
        return spPhoenixServiceMapper.selectSpPhoenixServiceListByIds(ids1);
    }
}
