package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpLikeMapper;
import com.ruoyi.xqsp.domain.SpLike;
import com.ruoyi.xqsp.service.ISpLikeService;

/**
 * 用户点赞Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
@Service
public class SpLikeServiceImpl implements ISpLikeService 
{
    @Autowired
    private SpLikeMapper spLikeMapper;

    /**
     * 查询用户点赞
     * 
     * @param likeId 用户点赞ID
     * @return 用户点赞
     */
    @Override
    public SpLike selectSpLikeById(Long likeId)
    {
        return spLikeMapper.selectSpLikeById(likeId);
    }

    /**
     * 查询用户点赞列表
     * 
     * @param spLike 用户点赞
     * @return 用户点赞
     */
    @Override
    public List<SpLike> selectSpLikeList(SpLike spLike)
    {
        return spLikeMapper.selectSpLikeList(spLike);
    }

    /**
     * 新增用户点赞
     * 
     * @param spLike 用户点赞
     * @return 结果
     */
    @Override
    public int insertSpLike(SpLike spLike)
    {
        return spLikeMapper.insertSpLike(spLike);
    }

    /**
     * 修改用户点赞
     * 
     * @param spLike 用户点赞
     * @return 结果
     */
    @Override
    public int updateSpLike(SpLike spLike)
    {
        return spLikeMapper.updateSpLike(spLike);
    }

    /**
     * 批量删除用户点赞
     * 
     * @param likeIds 需要删除的用户点赞ID
     * @return 结果
     */
    @Override
    public int deleteSpLikeByIds(Long[] likeIds)
    {
        return spLikeMapper.deleteSpLikeByIds(likeIds);
    }

    /**
     * 删除用户点赞信息
     * 
     * @param likeId 用户点赞ID
     * @return 结果
     */
    @Override
    public int deleteSpLikeById(Long likeId)
    {
        return spLikeMapper.deleteSpLikeById(likeId);
    }

    @Override
    public SpLike selectSpLike(SpLike spLike) {
        return spLikeMapper.selectSpLike(spLike);
    }

    @Override
    public int deleteSpLike(SpLike spLike) {
        return spLikeMapper.deleteSpLike(spLike);
    }

    @Override
    public SpLike selectSpLikeByIds(Long userId, Long videoId) {
        return spLikeMapper.selectSpLikeByIds(userId,videoId);
    }

    @Override
    public SpLike selectSpLikeByVarietyVideoId(Long userId, Long varietyVideoId) {
        return spLikeMapper.selectSpLikeByVarietyVideoId(userId,varietyVideoId);
    }
}
