package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.VideoTagConfigs;
import com.ruoyi.xqsp.service.VideoTagConfigsService;
import com.ruoyi.xqsp.mapper.VideoTagConfigsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @author lpden
* @description 针对表【sys_video_tag_configs(视频分类)】的数据库操作Service实现
* @createDate 2022-07-09 22:08:05
*/
@Service
public class VideoTagConfigsServiceImpl extends ServiceImpl<VideoTagConfigsMapper, VideoTagConfigs>
    implements VideoTagConfigsService{

    @Autowired
    VideoTagConfigsMapper videoTagConfigsMapper;

    @Override
    public List<Map> category(Integer id) {
        return videoTagConfigsMapper.category(id);
    }
}




