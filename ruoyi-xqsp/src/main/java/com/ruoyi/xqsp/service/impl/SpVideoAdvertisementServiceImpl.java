package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVideoAdvertisementMapper;
import com.ruoyi.xqsp.domain.SpVideoAdvertisement;
import com.ruoyi.xqsp.service.ISpVideoAdvertisementService;

/**
 * 视频广告Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
@Service
public class SpVideoAdvertisementServiceImpl implements ISpVideoAdvertisementService 
{
    @Autowired
    private SpVideoAdvertisementMapper spVideoAdvertisementMapper;

    /**
     * 查询视频广告
     * 
     * @param videoAdvertisementId 视频广告ID
     * @return 视频广告
     */
    @Override
    public SpVideoAdvertisement selectSpVideoAdvertisementById(Long videoAdvertisementId)
    {
        return spVideoAdvertisementMapper.selectSpVideoAdvertisementById(videoAdvertisementId);
    }

    /**
     * 查询视频广告列表
     * 
     * @param spVideoAdvertisement 视频广告
     * @return 视频广告
     */
    @Override
    public List<SpVideoAdvertisement> selectSpVideoAdvertisementList(SpVideoAdvertisement spVideoAdvertisement)
    {
        return spVideoAdvertisementMapper.selectSpVideoAdvertisementList(spVideoAdvertisement);
    }

    /**
     * 新增视频广告
     * 
     * @param spVideoAdvertisement 视频广告
     * @return 结果
     */
    @Override
    public int insertSpVideoAdvertisement(SpVideoAdvertisement spVideoAdvertisement)
    {
        return spVideoAdvertisementMapper.insertSpVideoAdvertisement(spVideoAdvertisement);
    }

    /**
     * 修改视频广告
     * 
     * @param spVideoAdvertisement 视频广告
     * @return 结果
     */
    @Override
    public int updateSpVideoAdvertisement(SpVideoAdvertisement spVideoAdvertisement)
    {
        return spVideoAdvertisementMapper.updateSpVideoAdvertisement(spVideoAdvertisement);
    }

    /**
     * 批量删除视频广告
     * 
     * @param videoAdvertisementIds 需要删除的视频广告ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoAdvertisementByIds(Long[] videoAdvertisementIds)
    {
        return spVideoAdvertisementMapper.deleteSpVideoAdvertisementByIds(videoAdvertisementIds);
    }

    /**
     * 删除视频广告信息
     * 
     * @param videoAdvertisementId 视频广告ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoAdvertisementById(Long videoAdvertisementId)
    {
        return spVideoAdvertisementMapper.deleteSpVideoAdvertisementById(videoAdvertisementId);
    }
}
