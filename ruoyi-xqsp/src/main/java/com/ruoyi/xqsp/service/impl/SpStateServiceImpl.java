package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpStateMapper;
import com.ruoyi.xqsp.domain.SpState;
import com.ruoyi.xqsp.service.ISpStateService;

/**
 * APP状态Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-10
 */
@Service
public class SpStateServiceImpl implements ISpStateService 
{
    @Autowired
    private SpStateMapper spStateMapper;

    /**
     * 查询APP状态
     * 
     * @param stateId APP状态ID
     * @return APP状态
     */
    @Override
    public SpState selectSpStateById(Long stateId)
    {
        return spStateMapper.selectSpStateById(stateId);
    }

    /**
     * 查询APP状态列表
     * 
     * @param spState APP状态
     * @return APP状态
     */
    @Override
    public List<SpState> selectSpStateList(SpState spState)
    {
        return spStateMapper.selectSpStateList(spState);
    }

    /**
     * 新增APP状态
     * 
     * @param spState APP状态
     * @return 结果
     */
    @Override
    public int insertSpState(SpState spState)
    {
        return spStateMapper.insertSpState(spState);
    }

    /**
     * 修改APP状态
     * 
     * @param spState APP状态
     * @return 结果
     */
    @Override
    public int updateSpState(SpState spState)
    {
        return spStateMapper.updateSpState(spState);
    }

    /**
     * 批量删除APP状态
     * 
     * @param stateIds 需要删除的APP状态ID
     * @return 结果
     */
    @Override
    public int deleteSpStateByIds(Long[] stateIds)
    {
        return spStateMapper.deleteSpStateByIds(stateIds);
    }

    /**
     * 删除APP状态信息
     * 
     * @param stateId APP状态ID
     * @return 结果
     */
    @Override
    public int deleteSpStateById(Long stateId)
    {
        return spStateMapper.deleteSpStateById(stateId);
    }
}
