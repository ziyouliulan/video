package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.GoldList;
import com.ruoyi.xqsp.service.GoldListService;
import com.ruoyi.xqsp.mapper.GoldListMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_gold_list】的数据库操作Service实现
* @createDate 2022-08-05 19:00:16
*/
@Service
public class GoldListServiceImpl extends ServiceImpl<GoldListMapper, GoldList>
    implements GoldListService{

}




