package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpRechargeRecord;

/**
 * 充值管理Service接口
 *
 * @author ruoyi
 * @date 2021-05-07
 */
public interface ISpRechargeRecordService extends IService<SpRechargeRecord>
{
    /**
     * 查询充值管理
     *
     * @param rechargeRecordId 充值管理ID
     * @return 充值管理
     */
    public SpRechargeRecord selectSpRechargeRecordById(Long rechargeRecordId);

    /**
     * 查询充值管理列表
     *
     * @param spRechargeRecord 充值管理
     * @return 充值管理集合
     */
    public List<SpRechargeRecord> selectSpRechargeRecordList(SpRechargeRecord spRechargeRecord);

    /**
     * 新增充值管理
     *
     * @param spRechargeRecord 充值管理
     * @return 结果
     */
    public int insertSpRechargeRecord(SpRechargeRecord spRechargeRecord);

    /**
     * 修改充值管理
     *
     * @param spRechargeRecord 充值管理
     * @return 结果
     */
    public int updateSpRechargeRecord(SpRechargeRecord spRechargeRecord);

    /**
     * 批量删除充值管理
     *
     * @param rechargeRecordIds 需要删除的充值管理ID
     * @return 结果
     */
    public int deleteSpRechargeRecordByIds(Long[] rechargeRecordIds);

    /**
     * 删除充值管理信息
     *
     * @param rechargeRecordId 充值管理ID
     * @return 结果
     */
    public int deleteSpRechargeRecordById(Long rechargeRecordId);

    SpRechargeRecord selectSpRechargeRecordByOrder(String rechargeRecordOrder);


    /**
     * 查询自助充值及客服记录
     * @param userId
     * @return
     */
    List<SpRechargeRecord> selectSpRechargeRecordListsBySelfAndKeFu(Long userId);


    /**
     * 查询自助充值记录
     * @param userId
     * @return
     */
    List<SpRechargeRecord> selectSpRechargeRecordLists(Long userId);

    /**
     * 根据用户id查询该用户所有会员的会员记录
     * @param userId
     * @return
     */
    List<SpRechargeRecord> selectSpRechargeRecordListById(Long userId);

    /**
     * 查询用户的推广奖励
     * @param userId
     * @return
     */
    List<SpRechargeRecord> selectSpRechargeRecordListById1(Long userId);

    /**
     * 查询用户购买的会员记录
     * @param userId
     * @return
     */
    List<SpRechargeRecord> selectSpRechargeRecordListById2(Long userId);

    /**
     * 用户兑换码兑换会员记录查询
     * @param userId
     * @return
     */
    List<SpRechargeRecord> selectSpRechargeRecordListById3(Long userId);
}
