package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpMineImg;

/**
 * 我的广告Service接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface ISpMineImgService 
{
    /**
     * 查询我的广告
     * 
     * @param mineImgId 我的广告ID
     * @return 我的广告
     */
    public SpMineImg selectSpMineImgById(Long mineImgId);

    /**
     * 查询我的广告列表
     * 
     * @param spMineImg 我的广告
     * @return 我的广告集合
     */
    public List<SpMineImg> selectSpMineImgList(SpMineImg spMineImg);

    /**
     * 新增我的广告
     * 
     * @param spMineImg 我的广告
     * @return 结果
     */
    public int insertSpMineImg(SpMineImg spMineImg);

    /**
     * 修改我的广告
     * 
     * @param spMineImg 我的广告
     * @return 结果
     */
    public int updateSpMineImg(SpMineImg spMineImg);

    /**
     * 批量删除我的广告
     * 
     * @param mineImgIds 需要删除的我的广告ID
     * @return 结果
     */
    public int deleteSpMineImgByIds(Long[] mineImgIds);

    /**
     * 删除我的广告信息
     * 
     * @param mineImgId 我的广告ID
     * @return 结果
     */
    public int deleteSpMineImgById(Long mineImgId);
}
