package com.ruoyi.xqsp.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpUsersMapper;
import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.service.ISpUsersService;

/**
 * 用户表Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-26
 */
@Service
public class SpUsersServiceImpl extends ServiceImpl<SpUsersMapper,SpUsers > implements ISpUsersService
{
    @Autowired
    private SpUsersMapper spUsersMapper;

    /**
     * 查询用户表
     *
     * @param userId 用户表ID
     * @return 用户表
     */
    @Override
    public SpUsers selectSpUsersById(Long userId)
    {
        return spUsersMapper.selectSpUsersById(userId);
    }

    /**
     * 查询用户表列表
     *
     * @param spUsers 用户表
     * @return 用户表
     */
    @Override
    public List<SpUsers> selectSpUsersList(SpUsers spUsers)
    {
        return spUsersMapper.selectSpUsersList(spUsers);
    }

    /**
     * 新增用户表
     *
     * @param spUsers 用户表
     * @return 结果
     */
    @Override
    public int insertSpUsers(SpUsers spUsers)
    {
        return spUsersMapper.insertSpUsers(spUsers);
    }

    /**
     * 修改用户表
     *
     * @param spUsers 用户表
     * @return 结果
     */
    @Override
    public int updateSpUsers(SpUsers spUsers)
    {
        return spUsersMapper.updateSpUsers(spUsers);
    }

    /**
     * 批量删除用户表
     *
     * @param userIds 需要删除的用户表ID
     * @return 结果
     */
    @Override
    public int deleteSpUsersByIds(Long[] userIds)
    {
        return spUsersMapper.deleteSpUsersByIds(userIds);
    }

    /**
     * 删除用户表信息
     *
     * @param userId 用户表ID
     * @return 结果
     */
    @Override
    public int deleteSpUsersById(Long userId)
    {
        return spUsersMapper.deleteSpUsersById(userId);
    }

    @Override
    public List<SpUsers> selectSpUsersListMonth(SpUsers spUsers) {
        return spUsersMapper.selectSpUsersListMonth(spUsers);
    }

    @Override
    public SpUsers selectSpUsersByXqId(String xqid) {
        return spUsersMapper.selectSpUsersByXqId(xqid);
    }

    /**
     * 通过注册用户的邀请码查询邀请该注册用户的用户
     * @param userPromoteCode
     * @return
     */
    @Override
    public int selectSpUsersCountByXqId(String userPromoteCode) {
        return spUsersMapper.selectSpUsersCountByXqId(userPromoteCode);
    }


    @Override
    public int updateSpUsersByxqid(String userPromoteCode, int days) {
        return spUsersMapper.updateSpUsersByxqid(userPromoteCode,days);
    }

    @Override
    public List<SpUsers> selectSpUsersListByXqid(String userXqid) {
        return spUsersMapper.selectSpUsersListByXqid(userXqid);
    }

    @Override
    public SpUsers selectSpUserById(Long userId) {
        return spUsersMapper.selectSpUserById(userId);
    }

    @Override
    public int updateSpUsersById(Long userId) {
        return spUsersMapper.updateSpUsersById(userId);
    }

    @Override
    public int updateUserGlod(Long userId, BigDecimal expenseCalendarGlod) {
        return spUsersMapper.updateUserGlod(userId,expenseCalendarGlod);
    }

    @Override
    public int updatEmembersDay() {
        return spUsersMapper.updatEmembersDay();
    }

    @Override
    public int refreshUserVipType(){
        return spUsersMapper.refreshUserVipType();
    }

    @Override
    public int updateSpUsersWelfareById(Long userId) {
        return spUsersMapper.updateSpUsersWelfareById(userId);
    }

    @Override
    public int updateSpUsersTodayLongById(Long userId) {
        return spUsersMapper.updateSpUsersTodayLongById(userId);
    }

    @Override
    public int updateSpUsersTodayShortById(Long userId) {
        return spUsersMapper.updateSpUsersTodayShortById(userId);
    }

    @Override
    public int updateSpUsersBuyTypeById(Long userId) {
        return spUsersMapper.updateSpUsersBuyTypeById(userId);
    }

    @Override
    public int updateSpUsersBuyTypeById1(Long userId) {
        return spUsersMapper.updateSpUsersBuyTypeById1(userId);
    }

    @Override
    public int updatMembersDayByUserId(Long userId, Long days) {
        return spUsersMapper.updatMembersDayByUserId(userId,days);
    }

    @Override
    public int updatGlodByUserId(Long userId, Long glod) {
        return spUsersMapper.updatGlodByUserId(userId,glod);
    }

    @Override
    public int updateSpUsersWelfare(Long userId, Long carmichaelDays) {
        return spUsersMapper.updateSpUsersWelfare(userId,carmichaelDays);
    }

    @Override
    public int updateSpUsersWelfareLong(Long userId, Long carmichaelDays) {
        return spUsersMapper.updateSpUsersWelfareLong(userId,carmichaelDays);
    }

    @Override
    public int updateSpUsersMg(SpUsers spUsers) {
        return spUsersMapper.updateSpUsersMg(spUsers);
    }

    @Override
    public int updateUserLoginState(Long userId, Date date) {
        return spUsersMapper.updateUserLoginState(userId,date);
    }

    @Override
    public List<SpUsers> selectSpUsersList1() {
        return spUsersMapper.selectSpUsersList1();
    }

    @Override
    public int updateUserLoginState1(Long userId) {
        return spUsersMapper.updateUserLoginState1(userId);
    }

    @Override
    public int updateUserLoginState12(Long userId) {
        return spUsersMapper.updateUserLoginState12(userId);
    }

    @Override
    public int addTimesWatched(Long userId,Long type) {


        return 0;
    }


}
