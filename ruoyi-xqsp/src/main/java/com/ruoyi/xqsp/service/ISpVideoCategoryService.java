package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpVideoCategory;
import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;

/**
 * 分类子表Service接口
 *
 * @author ruoyi
 * @date 2021-04-25
 */
public interface ISpVideoCategoryService  extends IService<SpVideoCategory>
{
    /**
     * 查询分类子表
     *
     * @param videoCategoryId 分类子表ID
     * @return 分类子表
     */
    public SpVideoCategory selectSpVideoCategoryById(Long videoCategoryId);

    /**
     * 查询分类子表列表
     *
     * @param spVideoCategory 分类子表
     * @return 分类子表集合
     */
    public List<SpVideoCategory> selectSpVideoCategoryList(SpVideoCategory spVideoCategory);

    /**
     * 新增分类子表
     *
     * @param spVideoCategory 分类子表
     * @return 结果
     */
    public int insertSpVideoCategory(SpVideoCategory spVideoCategory);

    /**
     * 修改分类子表
     *
     * @param spVideoCategory 分类子表
     * @return 结果
     */
    public int updateSpVideoCategory(SpVideoCategory spVideoCategory);

    /**
     * 批量删除分类子表
     *
     * @param videoCategoryIds 需要删除的分类子表ID
     * @return 结果
     */
    public int deleteSpVideoCategoryByIds(Long[] videoCategoryIds);

    /**
     * 删除分类子表信息
     *
     * @param videoCategoryId 分类子表ID
     * @return 结果
     */
    public int deleteSpVideoCategoryById(Long videoCategoryId);

    /**
     * 查询精选
     * @param spVideoCategory
     * @return
     */
    List<SpVideoCategory> selectSpVideoCategorySiftList(SpVideoCategory spVideoCategory);

    /**
     * 查询标签非精选
     * @param spVideoCategory
     * @return
     */
    List<SpVideoCategory> selectSpVideoCategoryTagList(SpVideoCategory spVideoCategory);
}
