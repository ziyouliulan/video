package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpCollectMapper;
import com.ruoyi.xqsp.domain.SpCollect;
import com.ruoyi.xqsp.service.ISpCollectService;

/**
 * 用户收藏Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-08
 */
@Service
public class SpCollectServiceImpl extends ServiceImpl<SpCollectMapper,SpCollect> implements ISpCollectService
{
    @Autowired
    private SpCollectMapper spCollectMapper;

    /**
     * 查询用户收藏
     *
     * @param collectId 用户收藏ID
     * @return 用户收藏
     */
    @Override
    public SpCollect selectSpCollectById(Long collectId)
    {
        return spCollectMapper.selectSpCollectById(collectId);
    }

    /**
     * 查询用户收藏列表
     *
     * @param spCollect 用户收藏
     * @return 用户收藏
     */
    @Override
    public List<SpCollect> selectSpCollectList(SpCollect spCollect)
    {
        return spCollectMapper.selectSpCollectList(spCollect);
    }
    /**
     * 查询用户收藏列表
     *
     * @param spCollect 用户收藏
     * @return 用户收藏
     */
    @Override
    public List<SpCollect> selectSpCollectListV2(SpCollect spCollect)
    {
        return spCollectMapper.selectSpCollectListV2(spCollect);
    }

    /**
     * 新增用户收藏
     *
     * @param spCollect 用户收藏
     * @return 结果
     */
    @Override
    public int insertSpCollect(SpCollect spCollect)
    {
        return spCollectMapper.insertSpCollect(spCollect);
    }

    /**
     * 修改用户收藏
     *
     * @param spCollect 用户收藏
     * @return 结果
     */
    @Override
    public int updateSpCollect(SpCollect spCollect)
    {
        return spCollectMapper.updateSpCollect(spCollect);
    }

    /**
     * 批量删除用户收藏
     *
     * @param collectIds 需要删除的用户收藏ID
     * @return 结果
     */
    @Override
    public int deleteSpCollectByIds(Long[] collectIds)
    {
        return spCollectMapper.deleteSpCollectByIds(collectIds);
    }

    /**
     * 删除用户收藏信息
     *
     * @param collectId 用户收藏ID
     * @return 结果
     */
    @Override
    public int deleteSpCollectById(Long collectId)
    {
        return spCollectMapper.deleteSpCollectById(collectId);
    }

    @Override
    public int deleteSpCollect(SpCollect spCollect) {
        return spCollectMapper.deleteSpCollect(spCollect);
    }

    @Override
    public SpCollect selectSpCollectByIds(Long userId, Long videoId) {
        return spCollectMapper.selectSpCollectByIds(userId,videoId);
    }

    @Override
    public SpCollect selectSpCollectByVarietyVideoId(Long userId, Long varietyVideoId) {
        return spCollectMapper.selectSpCollectByVarietyVideoId(userId,varietyVideoId);
    }

    @Override
    public List<SpCollect> selectSpCollectByUserId(Long userId) {
        return spCollectMapper.selectSpCollectByUserId(userId);
    }

    @Override
    public SpCollect selectSpCollectByActorId(Long userId, Long actorId) {
        return spCollectMapper.selectSpCollectByActorId(userId,actorId);
    }

    @Override
    public List<SpCollect> selectSpCollectActorByUserId(Long userId) {
        return spCollectMapper.selectSpCollectActorByUserId(userId);
    }

    @Override
    public int deleteSpCollect1(Long actorId, long type) {
        return spCollectMapper.deleteSpCollect1(actorId,type);
    }
}
