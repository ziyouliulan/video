package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.DatingUserCollection;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_dating_user_collection】的数据库操作Service
* @createDate 2022-02-28 15:10:23
*/
public interface DatingUserCollectionService extends IService<DatingUserCollection> {

}
