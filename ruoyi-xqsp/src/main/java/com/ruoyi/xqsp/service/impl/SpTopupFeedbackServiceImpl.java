package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpTopupFeedbackMapper;
import com.ruoyi.xqsp.domain.SpTopupFeedback;
import com.ruoyi.xqsp.service.ISpTopupFeedbackService;

/**
 * 充值反馈Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
@Service
public class SpTopupFeedbackServiceImpl implements ISpTopupFeedbackService 
{
    @Autowired
    private SpTopupFeedbackMapper spTopupFeedbackMapper;

    /**
     * 查询充值反馈
     * 
     * @param topupFeedbackId 充值反馈ID
     * @return 充值反馈
     */
    @Override
    public SpTopupFeedback selectSpTopupFeedbackById(Long topupFeedbackId)
    {
        return spTopupFeedbackMapper.selectSpTopupFeedbackById(topupFeedbackId);
    }

    /**
     * 查询充值反馈列表
     * 
     * @param spTopupFeedback 充值反馈
     * @return 充值反馈
     */
    @Override
    public List<SpTopupFeedback> selectSpTopupFeedbackList(SpTopupFeedback spTopupFeedback)
    {
        return spTopupFeedbackMapper.selectSpTopupFeedbackList(spTopupFeedback);
    }

    /**
     * 新增充值反馈
     * 
     * @param spTopupFeedback 充值反馈
     * @return 结果
     */
    @Override
    public int insertSpTopupFeedback(SpTopupFeedback spTopupFeedback)
    {
        return spTopupFeedbackMapper.insertSpTopupFeedback(spTopupFeedback);
    }

    /**
     * 修改充值反馈
     * 
     * @param spTopupFeedback 充值反馈
     * @return 结果
     */
    @Override
    public int updateSpTopupFeedback(SpTopupFeedback spTopupFeedback)
    {
        return spTopupFeedbackMapper.updateSpTopupFeedback(spTopupFeedback);
    }

    /**
     * 批量删除充值反馈
     * 
     * @param topupFeedbackIds 需要删除的充值反馈ID
     * @return 结果
     */
    @Override
    public int deleteSpTopupFeedbackByIds(Long[] topupFeedbackIds)
    {
        return spTopupFeedbackMapper.deleteSpTopupFeedbackByIds(topupFeedbackIds);
    }

    /**
     * 删除充值反馈信息
     * 
     * @param topupFeedbackId 充值反馈ID
     * @return 结果
     */
    @Override
    public int deleteSpTopupFeedbackById(Long topupFeedbackId)
    {
        return spTopupFeedbackMapper.deleteSpTopupFeedbackById(topupFeedbackId);
    }

    @Override
    public List<SpTopupFeedback> selectSpTopupFeedbackByUserId(Long userId) {
        return spTopupFeedbackMapper.selectSpTopupFeedbackByUserId(userId);
    }
}
