//package com.ruoyi.xqsp.service;
//
//import com.ruoyi.xqsp.domain.ConsumeRecord;
//import com.baomidou.mybatisplus.extension.service.IService;
//
///**
//* @author lpden
//* @description 针对表【sp_consume_record(消费记录)】的数据库操作Service
//* @createDate 2022-02-26 09:52:34
//*/
//public interface ConsumeRecordService extends IService<ConsumeRecord> {
//
//}
