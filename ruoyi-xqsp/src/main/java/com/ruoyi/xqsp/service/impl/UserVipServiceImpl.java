package com.ruoyi.xqsp.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.quyang.voice.model.ConsumeRecord;
import com.quyang.voice.utils.StringUtils;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.SpConsumeRecord;
import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.domain.UserVip;
import com.ruoyi.xqsp.domain.VipCategory;
import com.ruoyi.xqsp.service.ISpUsersService;
import com.ruoyi.xqsp.service.SpConsumeRecordService;
import com.ruoyi.xqsp.service.UserVipService;
import com.ruoyi.xqsp.mapper.UserVipMapper;
import com.ruoyi.xqsp.service.VipCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
* @author lpden
* @description 针对表【sp_user_vip】的数据库操作Service实现
* @createDate 2022-02-26 09:10:19
*/
@Service
public class UserVipServiceImpl extends ServiceImpl<UserVipMapper, UserVip>
    implements UserVipService{

    @Autowired
    VipCategoryService vipCategoryService;

    @Autowired
    SpConsumeRecordService consumeRecordService;


    @Autowired
    ISpUsersService usersService;

    @Override
    public ResponseUtil openVIp(Integer userId, Integer vId) {

//        QueryWrapper<UserVip> wrapper = new QueryWrapper<>();
//        wrapper.eq("user_id",userId);
//        UserVip userVip = super.getOne(wrapper);
        UserVip userVip = super.getById(userId);

        VipCategory vipCategory = vipCategoryService.getById(vId);

        SpUsers users = usersService.getById(userId);

        if (vipCategory.getMoney().compareTo(users.getUserGlod())  > -1){
            return ResponseUtil.fail("账户余额不足");
        }

        Date date = new Date();
        //
        if (ObjectUtil.isNull(userVip) || DateUtil.compare(users.getUserMembersDay(),date) == 0){

            vipCategory.getTime();
            userVip = new UserVip();
            userVip.setName(vipCategory.getName());
            userVip.setUserId(userId);
            userVip.setStartTime(date);
            userVip.setEndTime(DateUtil.offsetMonth(date,vipCategory.getTime()));
            userVip.setStatus(vipCategory.getId());
            userVip.setDeleted(0);
            save(userVip);
        }else {
            //类型：1：月卡 2:季卡,3年卡
            if (userVip.getStatus() < vipCategory.getId()){
                userVip.setEndTime(DateUtil.offsetMonth(date,vipCategory.getTime()));
                userVip.setStatus(vipCategory.getId());
                userVip.setName(vipCategory.getName());
                updateById(userVip);
            }else {
                userVip.setEndTime(DateUtil.offsetMonth(userVip.getEndTime(),vipCategory.getTime()));
                updateById(userVip);
            }

        }
        users.setUserGlod(users.getUserGlod().subtract(vipCategory.getMoney()));
        usersService.updateById(users);

        SpConsumeRecord consumeRecord = new SpConsumeRecord();
        consumeRecord.setMoney(vipCategory.getMoney());
        consumeRecord.setTradeNo(StringUtils.generateOrderNumber());
        consumeRecord.setPayType(1);
        consumeRecord.setUserId(userId.longValue());
        consumeRecord.setCurrentBalance(users.getUserGlod());
        consumeRecord.setChangeType(2);
        consumeRecord.setType(1);
        consumeRecord.setOperationAmount(vipCategory.getMoney());
        consumeRecord.setCreateTime(new Date());
        consumeRecord.setDescs("开通会员");
        consumeRecordService.save(consumeRecord);


        return ResponseUtil.success();
    }


    @Override
    public ResponseUtil membershipRecords(Integer userId,Integer type,Integer pageNum, Integer pageSize) {

        QueryWrapper<SpConsumeRecord> wrapper = new QueryWrapper();
        wrapper.eq("user_id",userId);
        switch (type){
            case 0:
//                wrapper.eq("type",type);
                break;
            case 1:
                wrapper.in("type",1,2,3,4,5);
                break;
            case 2:
                wrapper.in("type",7,9);
                break;
            case 3:
                wrapper.in("type",10,11,12);
                break;
           case 4:
                wrapper.in("type",6);
               break;
        }
        wrapper.orderByDesc("create_time");

        PageHelper.startPage(pageNum,pageSize);
        List<SpConsumeRecord> list = consumeRecordService.list(wrapper);

        return ResponseUtil.success(PageInfo.of(list));
    }

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("date = " + date);
        DateTime dateTime = DateUtil.offsetHour(date, -1);
        System.out.println("dateTime = " + dateTime);
        int compare = DateUtil.compare(date, dateTime);
        System.out.println("compare = " + compare);


    }
}




