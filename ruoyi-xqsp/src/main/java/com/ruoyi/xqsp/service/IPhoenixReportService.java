package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.PhoenixReport;

/**
 * 楼凤举报Service接口
 * 
 * @author ruoyi
 * @date 2021-05-21
 */
public interface IPhoenixReportService 
{
    /**
     * 查询楼凤举报
     * 
     * @param reportId 楼凤举报ID
     * @return 楼凤举报
     */
    public PhoenixReport selectPhoenixReportById(Long reportId);

    /**
     * 查询楼凤举报列表
     * 
     * @param phoenixReport 楼凤举报
     * @return 楼凤举报集合
     */
    public List<PhoenixReport> selectPhoenixReportList(PhoenixReport phoenixReport);

    /**
     * 新增楼凤举报
     * 
     * @param phoenixReport 楼凤举报
     * @return 结果
     */
    public int insertPhoenixReport(PhoenixReport phoenixReport);

    /**
     * 修改楼凤举报
     * 
     * @param phoenixReport 楼凤举报
     * @return 结果
     */
    public int updatePhoenixReport(PhoenixReport phoenixReport);

    /**
     * 批量删除楼凤举报
     * 
     * @param reportIds 需要删除的楼凤举报ID
     * @return 结果
     */
    public int deletePhoenixReportByIds(Long[] reportIds);

    /**
     * 删除楼凤举报信息
     * 
     * @param reportId 楼凤举报ID
     * @return 结果
     */
    public int deletePhoenixReportById(Long reportId);

    /**
     * 根据漏风id封停改变举报表漏风状态
     * @param phoenixId
     * @param phoenixState
     * @return
     */
    int updatePhoenixReportByPhoenixId(Long phoenixId, String phoenixState);
}
