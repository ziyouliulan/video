package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.PaymentChannel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【payment_channel】的数据库操作Service
* @createDate 2022-05-26 22:45:12
*/
public interface PaymentChannelService extends IService<PaymentChannel> {

}
