package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpAdvertising;
import com.ruoyi.xqsp.mapper.SpAdvertisingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpActorMapper;
import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.service.ISpActorService;

/**
 * 演员管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-17
 */
@Service
public class SpActorServiceImpl extends ServiceImpl<SpActorMapper, SpActor> implements ISpActorService
{
    @Autowired
    private SpActorMapper spActorMapper;

    /**
     * 查询演员管理
     *
     * @param actorId 演员管理ID
     * @return 演员管理
     */
    @Override
    public SpActor selectSpActorById(Long actorId)
    {
        return spActorMapper.selectSpActorById(actorId);
    }

    /**
     * 查询演员管理列表
     *
     * @param spActor 演员管理
     * @return 演员管理
     */
    @Override
    public List<SpActor> selectSpActorList(SpActor spActor)
    {
        return spActorMapper.selectSpActorList(spActor);
    }

    /**
     * 新增演员管理
     *
     * @param spActor 演员管理
     * @return 结果
     */
    @Override
    public int insertSpActor(SpActor spActor)
    {
        return spActorMapper.insertSpActor(spActor);
    }

    /**
     * 修改演员管理
     *
     * @param spActor 演员管理
     * @return 结果
     */
    @Override
    public int updateSpActor(SpActor spActor)
    {
        return spActorMapper.updateSpActor(spActor);
    }

    /**
     * 批量删除演员管理
     *
     * @param actorIds 需要删除的演员管理ID
     * @return 结果
     */
    @Override
    public int deleteSpActorByIds(Long[] actorIds)
    {
        return spActorMapper.deleteSpActorByIds(actorIds);
    }

    /**
     * 删除演员管理信息
     *
     * @param actorId 演员管理ID
     * @return 结果
     */
    @Override
    public int deleteSpActorById(Long actorId)
    {
        return spActorMapper.deleteSpActorById(actorId);
    }

    @Override
    public List<SpActor> selectSpActorLists(SpActor spActor) {
        return spActorMapper.selectSpActorLists(spActor);
    }
}
