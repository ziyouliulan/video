package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.TemplateConfigs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author lpden
* @description 针对表【sys_template_configs(视频分类)】的数据库操作Service
* @createDate 2022-07-09 20:27:16
*/
public interface TemplateConfigsService extends IService<TemplateConfigs> {
    List<Map> category(Integer id);
}
