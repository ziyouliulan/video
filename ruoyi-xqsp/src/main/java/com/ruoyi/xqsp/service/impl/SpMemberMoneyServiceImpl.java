package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpMemberMoneyMapper;
import com.ruoyi.xqsp.domain.SpMemberMoney;
import com.ruoyi.xqsp.service.ISpMemberMoneyService;

/**
 * 会员设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
@Service
public class SpMemberMoneyServiceImpl implements ISpMemberMoneyService 
{
    @Autowired
    private SpMemberMoneyMapper spMemberMoneyMapper;

    /**
     * 查询会员设置
     * 
     * @param memberMoneyId 会员设置ID
     * @return 会员设置
     */
    @Override
    public SpMemberMoney selectSpMemberMoneyById(Long memberMoneyId)
    {
        return spMemberMoneyMapper.selectSpMemberMoneyById(memberMoneyId);
    }

    /**
     * 查询会员设置列表
     * 
     * @param spMemberMoney 会员设置
     * @return 会员设置
     */
    @Override
    public List<SpMemberMoney> selectSpMemberMoneyList(SpMemberMoney spMemberMoney)
    {
        return spMemberMoneyMapper.selectSpMemberMoneyList(spMemberMoney);
    }

    /**
     * 新增会员设置
     * 
     * @param spMemberMoney 会员设置
     * @return 结果
     */
    @Override
    public int insertSpMemberMoney(SpMemberMoney spMemberMoney)
    {
        return spMemberMoneyMapper.insertSpMemberMoney(spMemberMoney);
    }

    /**
     * 修改会员设置
     * 
     * @param spMemberMoney 会员设置
     * @return 结果
     */
    @Override
    public int updateSpMemberMoney(SpMemberMoney spMemberMoney)
    {
        return spMemberMoneyMapper.updateSpMemberMoney(spMemberMoney);
    }

    /**
     * 批量删除会员设置
     * 
     * @param memberMoneyIds 需要删除的会员设置ID
     * @return 结果
     */
    @Override
    public int deleteSpMemberMoneyByIds(Long[] memberMoneyIds)
    {
        return spMemberMoneyMapper.deleteSpMemberMoneyByIds(memberMoneyIds);
    }

    /**
     * 删除会员设置信息
     * 
     * @param memberMoneyId 会员设置ID
     * @return 结果
     */
    @Override
    public int deleteSpMemberMoneyById(Long memberMoneyId)
    {
        return spMemberMoneyMapper.deleteSpMemberMoneyById(memberMoneyId);
    }
}
