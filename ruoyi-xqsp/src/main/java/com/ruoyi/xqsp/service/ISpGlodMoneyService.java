package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpGlodMoney;

/**
 * 金币设置Service接口
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
public interface ISpGlodMoneyService 
{
    /**
     * 查询金币设置
     * 
     * @param glodMoneyId 金币设置ID
     * @return 金币设置
     */
    public SpGlodMoney selectSpGlodMoneyById(Long glodMoneyId);

    /**
     * 查询金币设置列表
     * 
     * @param spGlodMoney 金币设置
     * @return 金币设置集合
     */
    public List<SpGlodMoney> selectSpGlodMoneyList(SpGlodMoney spGlodMoney);

    /**
     * 新增金币设置
     * 
     * @param spGlodMoney 金币设置
     * @return 结果
     */
    public int insertSpGlodMoney(SpGlodMoney spGlodMoney);

    /**
     * 修改金币设置
     * 
     * @param spGlodMoney 金币设置
     * @return 结果
     */
    public int updateSpGlodMoney(SpGlodMoney spGlodMoney);

    /**
     * 批量删除金币设置
     * 
     * @param glodMoneyIds 需要删除的金币设置ID
     * @return 结果
     */
    public int deleteSpGlodMoneyByIds(Long[] glodMoneyIds);

    /**
     * 删除金币设置信息
     * 
     * @param glodMoneyId 金币设置ID
     * @return 结果
     */
    public int deleteSpGlodMoneyById(Long glodMoneyId);
}
