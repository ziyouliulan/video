package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;
import com.ruoyi.xqsp.mapper.SpVideoCategoryPatternMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVideoCategoryMapper;
import com.ruoyi.xqsp.domain.SpVideoCategory;
import com.ruoyi.xqsp.service.ISpVideoCategoryService;

/**
 * 分类子表Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-25
 */
@Service
public class SpVideoCategoryServiceImpl extends ServiceImpl<SpVideoCategoryMapper, SpVideoCategory> implements ISpVideoCategoryService
{
    @Autowired
    private SpVideoCategoryMapper spVideoCategoryMapper;

    /**
     * 查询分类子表
     *
     * @param videoCategoryId 分类子表ID
     * @return 分类子表
     */
    @Override
    public SpVideoCategory selectSpVideoCategoryById(Long videoCategoryId)
    {
        return spVideoCategoryMapper.selectSpVideoCategoryById(videoCategoryId);
    }

    /**
     * 查询分类子表列表
     *
     * @param spVideoCategory 分类子表
     * @return 分类子表
     */
    @Override
    public List<SpVideoCategory> selectSpVideoCategoryList(SpVideoCategory spVideoCategory)
    {
        return spVideoCategoryMapper.selectSpVideoCategoryList(spVideoCategory);
    }

    /**
     * 新增分类子表
     *
     * @param spVideoCategory 分类子表
     * @return 结果
     */
    @Override
    public int insertSpVideoCategory(SpVideoCategory spVideoCategory)
    {
        return spVideoCategoryMapper.insertSpVideoCategory(spVideoCategory);
    }

    /**
     * 修改分类子表
     *
     * @param spVideoCategory 分类子表
     * @return 结果
     */
    @Override
    public int updateSpVideoCategory(SpVideoCategory spVideoCategory)
    {
        return spVideoCategoryMapper.updateSpVideoCategory(spVideoCategory);
    }

    /**
     * 批量删除分类子表
     *
     * @param videoCategoryIds 需要删除的分类子表ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoCategoryByIds(Long[] videoCategoryIds)
    {
        return spVideoCategoryMapper.deleteSpVideoCategoryByIds(videoCategoryIds);
    }

    /**
     * 删除分类子表信息
     *
     * @param videoCategoryId 分类子表ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoCategoryById(Long videoCategoryId)
    {
        return spVideoCategoryMapper.deleteSpVideoCategoryById(videoCategoryId);
    }

    /**
     * 精选
     * @param spVideoCategory
     * @return
     */
    @Override
    public List<SpVideoCategory> selectSpVideoCategorySiftList(SpVideoCategory spVideoCategory) {
        return spVideoCategoryMapper.selectSpVideoCategorySiftList(spVideoCategory);
    }

    /**
     * 标签
     * @param spVideoCategory
     * @return
     */

    @Override
    public List<SpVideoCategory> selectSpVideoCategoryTagList(SpVideoCategory spVideoCategory) {
        return spVideoCategoryMapper.selectSpVideoCategoryTagList(spVideoCategory);
    }
}
