package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpAdviceFeedback;

/**
 * 意见反馈Service接口
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
public interface ISpAdviceFeedbackService  extends IService<SpAdviceFeedback>
{
    /**
     * 查询意见反馈
     * 
     * @param adviceFeedbackId 意见反馈ID
     * @return 意见反馈
     */
    public SpAdviceFeedback selectSpAdviceFeedbackById(Long adviceFeedbackId);

    /**
     * 查询意见反馈列表
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 意见反馈集合
     */
    public List<SpAdviceFeedback> selectSpAdviceFeedbackList(SpAdviceFeedback spAdviceFeedback);

    /**
     * 新增意见反馈
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 结果
     */
    public int insertSpAdviceFeedback(SpAdviceFeedback spAdviceFeedback);

    /**
     * 修改意见反馈
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 结果
     */
    public int updateSpAdviceFeedback(SpAdviceFeedback spAdviceFeedback);

    /**
     * 批量删除意见反馈
     * 
     * @param adviceFeedbackIds 需要删除的意见反馈ID
     * @return 结果
     */
    public int deleteSpAdviceFeedbackByIds(Long[] adviceFeedbackIds);

    /**
     * 删除意见反馈信息
     * 
     * @param adviceFeedbackId 意见反馈ID
     * @return 结果
     */
    public int deleteSpAdviceFeedbackById(Long adviceFeedbackId);

    /**
     * 根据用户ID查询
     * @param userId
     * @return
     */
    List<SpAdviceFeedback> selectSpAdviceFeedbackByUserId(Long userId);
}
