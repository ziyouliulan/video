package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.ScrollNotifications;
import com.ruoyi.xqsp.service.ScrollNotificationsService;
import com.ruoyi.xqsp.mapper.ScrollNotificationsMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_scroll_notifications(通知公告表)】的数据库操作Service实现
* @createDate 2022-07-25 21:06:45
*/
@Service
public class ScrollNotificationsServiceImpl extends ServiceImpl<ScrollNotificationsMapper, ScrollNotifications>
    implements ScrollNotificationsService{

}




