package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpComicBookmark;
import com.sun.jna.platform.win32.WinDef;

/**
 * 漫画书签Service接口
 * 
 * @author ruoyi
 * @date 2021-05-22
 */
public interface ISpComicBookmarkService 
{
    /**
     * 查询漫画书签
     * 
     * @param comicBookmarkId 漫画书签ID
     * @return 漫画书签
     */
    public SpComicBookmark selectSpComicBookmarkById(Long comicBookmarkId);

    /**
     * 查询漫画书签列表
     * 
     * @param spComicBookmark 漫画书签
     * @return 漫画书签集合
     */
    public List<SpComicBookmark> selectSpComicBookmarkList(SpComicBookmark spComicBookmark);

    /**
     * 新增漫画书签
     * 
     * @param spComicBookmark 漫画书签
     * @return 结果
     */
    public int insertSpComicBookmark(SpComicBookmark spComicBookmark);

    /**
     * 修改漫画书签
     * 
     * @param spComicBookmark 漫画书签
     * @return 结果
     */
    public int updateSpComicBookmark(SpComicBookmark spComicBookmark);

    /**
     * 批量删除漫画书签
     * 
     * @param comicBookmarkIds 需要删除的漫画书签ID
     * @return 结果
     */
    public int deleteSpComicBookmarkByIds(Long[] comicBookmarkIds);

    /**
     * 删除漫画书签信息
     * 
     * @param comicBookmarkId 漫画书签ID
     * @return 结果
     */
    public int deleteSpComicBookmarkById(Long comicBookmarkId);

    /**
     * 查询漫画书签的章节
     * @param spComicBookmark
     * @return
     */
    SpComicBookmark selectSpComicBookmark(SpComicBookmark spComicBookmark);

    int updateSpComicBookmark1(SpComicBookmark spComicBookmark);
}
