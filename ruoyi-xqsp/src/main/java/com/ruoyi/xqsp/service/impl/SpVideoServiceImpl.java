package com.ruoyi.xqsp.service.impl;

import java.util.HashMap;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;
import com.ruoyi.xqsp.mapper.SpVideoCategoryPatternMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVideoMapper;
import com.ruoyi.xqsp.domain.SpVideo;
import com.ruoyi.xqsp.service.ISpVideoService;

/**
 * 视频管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-23
 */
@Service
public class SpVideoServiceImpl extends ServiceImpl<SpVideoMapper, SpVideo> implements ISpVideoService
{
    @Autowired
    private SpVideoMapper spVideoMapper;

    /**
     * 查询视频管理
     *
     * @param videoId 视频管理ID
     * @return 视频管理
     */
    @Override
    public SpVideo selectSpVideoById(Long videoId)
    {
        return spVideoMapper.selectSpVideoById(videoId);
    }

    /**
     * 查询视频管理列表
     *
     * @param spVideo 视频管理
     * @return 视频管理
     */
    @Override
    public List<SpVideo> selectSpVideoList(SpVideo spVideo)
    {
        return spVideoMapper.selectSpVideoList(spVideo);
    }

    /**
     * 新增视频管理
     *
     * @param spVideo 视频管理
     * @return 结果
     */
    @Override
    public int insertSpVideo(SpVideo spVideo)
    {
        return spVideoMapper.insertSpVideo(spVideo);
    }

    /**
     * 修改视频管理
     *
     * @param spVideo 视频管理
     * @return 结果
     */
    @Override
    public int updateSpVideo(SpVideo spVideo)
    {
        return spVideoMapper.updateSpVideo(spVideo);
    }

    /**
     * 批量删除视频管理
     *
     * @param videoIds 需要删除的视频管理ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoByIds(Long[] videoIds)
    {
        return spVideoMapper.deleteSpVideoByIds(videoIds);
    }

    /**
     * 删除视频管理信息
     *
     * @param videoId 视频管理ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoById(Long videoId)
    {
        return spVideoMapper.deleteSpVideoById(videoId);
    }

    /**
     * 根据演员id获取视频信息
     * @param actorId
     * @return
     */
    @Override
    public List<SpVideo> selectSpVideoByActorId(Long actorId) {
        return spVideoMapper.selectSpVideoByActorId(actorId);
    }

    @Override
      public List<SpVideo> selectSpVideoByCategoryId(Long videoCategoryId) {
        return spVideoMapper.selectSpVideoByCategoryId(videoCategoryId);
    }

    @Override
    public int updateSpVideoLikeNumber(Long videoId) {
        return spVideoMapper.updateSpVideoLikeNumber(videoId);
    }

    @Override
    public int updateSpVideoLikeNumber1(Long videoId) {
        return spVideoMapper.updateSpVideoLikeNumber1(videoId);
    }

    @Override
    public List<SpVideo> selectSpVideoListByActorId(Long actorId) {
        return spVideoMapper.selectSpVideoListByActorId(actorId);
    }

    @Override
    public Integer selectCountByActorId(Long actorId) {
        return spVideoMapper.selectCountByActorId(actorId);
    }

    @Override
    public int updateSpVideoUnLikeNumber(Long videoId) {
        return spVideoMapper.updateSpVideoUnLikeNumber(videoId);
    }

    @Override
    public int updateSpVideoUnLikeNumber1(Long videoId) {
        return spVideoMapper.updateSpVideoUnLikeNumber1(videoId);
    }

    @Override
    public List<SpVideo> selectSpVideoByCategoryIds(Long videoCategoryId) {
        return spVideoMapper.selectSpVideoByCategoryIds(videoCategoryId);
    }

    @Override
    public int updateSpVideoCollectNumber(Long videoId) {
        return spVideoMapper.updateSpVideoCollectNumber(videoId);
    }

    @Override
    public int updateSpVideoCollectNumber1(Long videoId) {
        return spVideoMapper.updateSpVideoCollectNumber1(videoId);
    }

    @Override
    public List<SpVideo> selectSpVideoListOrderByDate(SpVideo spVideo) {
        return spVideoMapper.selectSpVideoListOrderByDate(spVideo);
    }

    @Override
    public List<SpVideo> selectSpVideoListOrderByLike(SpVideo spVideo) {
        return spVideoMapper.selectSpVideoListOrderByLike(spVideo);
    }

    @Override
    public List<SpVideo> selectSpVideoListOrderByWatch(SpVideo spVideo) {
        return spVideoMapper.selectSpVideoListOrderByWatch(spVideo);
    }

    @Override
    public List<SpVideo> selectSpVideoListOrderByCollect(SpVideo spVideo) {
        return spVideoMapper.selectSpVideoListOrderByCollect(spVideo);
    }

    @Override
    public List<SpVideo> selectSpVideoLists(SpVideo spVideo) {
        return spVideoMapper.selectSpVideoLists(spVideo);
    }

    @Override
    public int updateSpVideoWatchNumber(Long videoWatchHistoryTypeId) {
        return spVideoMapper.updateSpVideoWatchNumber(videoWatchHistoryTypeId);
    }

    @Override
    public List<SpVideo> selectSpVideoListByName(String name) {
        return spVideoMapper.selectSpVideoListByName(name);
    }

    @Override
    public List<SpVideo> selectSpAnimeVideoList(Long videoCategoryId) {
        return spVideoMapper.selectSpAnimeVideoList(videoCategoryId);
    }

    @Override
    public List<SpVideo> selectSpAnimeVideoListOrderTime(Long videoCategoryId) {
        return spVideoMapper.selectSpAnimeVideoListOrderTime(videoCategoryId);
    }

    @Override
    public List<SpVideo> selectSpAnimeVideoListOrderLike(Long videoCategoryId) {
        return spVideoMapper.selectSpAnimeVideoListOrderLike(videoCategoryId);
    }

    @Override
    public List<SpVideo> selectSpAnimeVideoListOrderWatch(Long videoCategoryId) {
        return spVideoMapper.selectSpAnimeVideoListOrderWatch(videoCategoryId);
    }

    @Override
    public List<SpVideo> selectSpAnimeVideoListByName(String videoName) {
        return spVideoMapper.selectSpAnimeVideoListByName(videoName);
    }


    public ResponseUtil likeAllName(String name, Integer pageNum, Integer pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<HashMap> hashMaps = spVideoMapper.likeAllName(name);
        return ResponseUtil.success(PageInfo.of(hashMaps));
    }
}
