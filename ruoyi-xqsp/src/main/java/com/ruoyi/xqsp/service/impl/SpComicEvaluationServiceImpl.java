package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicEvaluationMapper;
import com.ruoyi.xqsp.domain.SpComicEvaluation;
import com.ruoyi.xqsp.service.ISpComicEvaluationService;

/**
 * 漫画评价Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
@Service
public class SpComicEvaluationServiceImpl implements ISpComicEvaluationService 
{
    @Autowired
    private SpComicEvaluationMapper spComicEvaluationMapper;

    /**
     * 查询漫画评价
     * 
     * @param comicEvaluationId 漫画评价ID
     * @return 漫画评价
     */
    @Override
    public SpComicEvaluation selectSpComicEvaluationById(Long comicEvaluationId)
    {
        return spComicEvaluationMapper.selectSpComicEvaluationById(comicEvaluationId);
    }

    /**
     * 查询漫画评价列表
     * 
     * @param spComicEvaluation 漫画评价
     * @return 漫画评价
     */
    @Override
    public List<SpComicEvaluation> selectSpComicEvaluationList(SpComicEvaluation spComicEvaluation)
    {
        return spComicEvaluationMapper.selectSpComicEvaluationList(spComicEvaluation);
    }

    /**
     * 新增漫画评价
     * 
     * @param spComicEvaluation 漫画评价
     * @return 结果
     */
    @Override
    public int insertSpComicEvaluation(SpComicEvaluation spComicEvaluation)
    {
        return spComicEvaluationMapper.insertSpComicEvaluation(spComicEvaluation);
    }

    /**
     * 修改漫画评价
     * 
     * @param spComicEvaluation 漫画评价
     * @return 结果
     */
    @Override
    public int updateSpComicEvaluation(SpComicEvaluation spComicEvaluation)
    {
        return spComicEvaluationMapper.updateSpComicEvaluation(spComicEvaluation);
    }

    /**
     * 批量删除漫画评价
     * 
     * @param comicEvaluationIds 需要删除的漫画评价ID
     * @return 结果
     */
    @Override
    public int deleteSpComicEvaluationByIds(Long[] comicEvaluationIds)
    {
        return spComicEvaluationMapper.deleteSpComicEvaluationByIds(comicEvaluationIds);
    }

    /**
     * 删除漫画评价信息
     * 
     * @param comicEvaluationId 漫画评价ID
     * @return 结果
     */
    @Override
    public int deleteSpComicEvaluationById(Long comicEvaluationId)
    {
        return spComicEvaluationMapper.deleteSpComicEvaluationById(comicEvaluationId);
    }

    @Override
    public Integer selectSpComicEvaluationCountById(Long comicId) {
        return spComicEvaluationMapper.selectSpComicEvaluationCountById(comicId);
    }

    @Override
    public List<SpComicEvaluation> selectSpComicEvaluationByComicId(Long comicId) {
        return spComicEvaluationMapper.selectSpComicEvaluationByComicId(comicId);
    }

    @Override
    public int updateSpComicEvaluationLike(Long comicEvaluationId) {
        return spComicEvaluationMapper.updateSpComicEvaluationLike(comicEvaluationId);
    }

    @Override
    public int updateSpComicEvaluationLike1(Long comicEvaluationId) {
        return spComicEvaluationMapper.updateSpComicEvaluationLike1(comicEvaluationId);
    }
}
