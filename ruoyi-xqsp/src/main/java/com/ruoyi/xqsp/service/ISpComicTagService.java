package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpComicTag;
import com.ruoyi.xqsp.domain.SpVideo;

/**
 * 漫画标签Service接口
 *
 * @author ruoyi
 * @date 2021-05-24
 */
public interface ISpComicTagService extends IService<SpComicTag>
{
    /**
     * 查询漫画标签
     *
     * @param comicTagId 漫画标签ID
     * @return 漫画标签
     */
    public SpComicTag selectSpComicTagById(Long comicTagId);

    /**
     * 查询漫画标签列表
     *
     * @param spComicTag 漫画标签
     * @return 漫画标签集合
     */
    public List<SpComicTag> selectSpComicTagList(SpComicTag spComicTag);

    /**
     * 新增漫画标签
     *
     * @param spComicTag 漫画标签
     * @return 结果
     */
    public int insertSpComicTag(SpComicTag spComicTag);

    /**
     * 修改漫画标签
     *
     * @param spComicTag 漫画标签
     * @return 结果
     */
    public int updateSpComicTag(SpComicTag spComicTag);

    /**
     * 批量删除漫画标签
     *
     * @param comicTagIds 需要删除的漫画标签ID
     * @return 结果
     */
    public int deleteSpComicTagByIds(Long[] comicTagIds);

    /**
     * 删除漫画标签信息
     *
     * @param comicTagId 漫画标签ID
     * @return 结果
     */
    public int deleteSpComicTagById(Long comicTagId);
}
