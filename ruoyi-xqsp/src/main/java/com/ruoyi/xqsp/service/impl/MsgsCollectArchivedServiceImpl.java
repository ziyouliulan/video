package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.MsgsCollectArchived;
import com.ruoyi.xqsp.service.MsgsCollectArchivedService;
import com.ruoyi.xqsp.mapper.MsgsCollectArchivedMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author lpden
* @description 针对表【msgs_collect_archived(用户聊天记录表（存档）。超时后的用户聊天记录，将自动转储到本表，防止用户的消息记录表热数据过

收集用户消息的目的是有助于分析用户行为，用户消息本身对公司而言没有多大意义且违背用户隐私条款，目前先这样吧，以后或需停止收集。)】的数据库操作Service实现
* @createDate 2022-05-25 22:38:50
*/
@Service
public class MsgsCollectArchivedServiceImpl extends ServiceImpl<MsgsCollectArchivedMapper, MsgsCollectArchived>
    implements MsgsCollectArchivedService{


    @Autowired
    MsgsCollectArchivedMapper msgsCollectArchivedMapper;

    @Override
    public List<MsgsCollectArchived> listByUserId(Integer userId, Integer toUserId) {
        return null;
    }
}




