package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpRechargeRecordMapper;
import com.ruoyi.xqsp.domain.SpRechargeRecord;
import com.ruoyi.xqsp.service.ISpRechargeRecordService;

/**
 * 充值管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@Service
public class SpRechargeRecordServiceImpl extends ServiceImpl<SpRechargeRecordMapper,SpRechargeRecord> implements ISpRechargeRecordService
{
    @Autowired
    private SpRechargeRecordMapper spRechargeRecordMapper;

    /**
     * 查询充值管理
     *
     * @param rechargeRecordId 充值管理ID
     * @return 充值管理
     */
    @Override
    public SpRechargeRecord selectSpRechargeRecordById(Long rechargeRecordId)
    {
        return spRechargeRecordMapper.selectSpRechargeRecordById(rechargeRecordId);
    }

    /**
     * 查询充值管理列表
     *
     * @param spRechargeRecord 充值管理
     * @return 充值管理
     */
    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordList(SpRechargeRecord spRechargeRecord)
    {
        return spRechargeRecordMapper.selectSpRechargeRecordList(spRechargeRecord);
    }

    /**
     * 新增充值管理
     *
     * @param spRechargeRecord 充值管理
     * @return 结果
     */
    @Override
    public int insertSpRechargeRecord(SpRechargeRecord spRechargeRecord)
    {
        return spRechargeRecordMapper.insertSpRechargeRecord(spRechargeRecord);
    }

    /**
     * 修改充值管理
     *
     * @param spRechargeRecord 充值管理
     * @return 结果
     */
    @Override
    public int updateSpRechargeRecord(SpRechargeRecord spRechargeRecord)
    {
        return spRechargeRecordMapper.updateSpRechargeRecord(spRechargeRecord);
    }

    /**
     * 批量删除充值管理
     *
     * @param rechargeRecordIds 需要删除的充值管理ID
     * @return 结果
     */
    @Override
    public int deleteSpRechargeRecordByIds(Long[] rechargeRecordIds)
    {
        return spRechargeRecordMapper.deleteSpRechargeRecordByIds(rechargeRecordIds);
    }

    /**
     * 删除充值管理信息
     *
     * @param rechargeRecordId 充值管理ID
     * @return 结果
     */
    @Override
    public int deleteSpRechargeRecordById(Long rechargeRecordId)
    {
        return spRechargeRecordMapper.deleteSpRechargeRecordById(rechargeRecordId);
    }

    @Override
    public SpRechargeRecord selectSpRechargeRecordByOrder(String rechargeRecordOrder) {
        return spRechargeRecordMapper.selectSpRechargeRecordByOrder(rechargeRecordOrder);
    }

    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordListsBySelfAndKeFu(Long userId) {
        return spRechargeRecordMapper.selectSpRechargeRecordListsBySelfAndKeFu(userId);
    }

    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordLists(Long userId) {
        return spRechargeRecordMapper.selectSpRechargeRecordLists(userId);
    }

    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordListById(Long userId) {
        return spRechargeRecordMapper.selectSpRechargeRecordListById(userId);
    }

    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordListById1(Long userId) {
        return spRechargeRecordMapper.selectSpRechargeRecordListById1(userId);
    }

    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordListById2(Long userId) {
        return spRechargeRecordMapper.selectSpRechargeRecordListById2(userId);
    }

    @Override
    public List<SpRechargeRecord> selectSpRechargeRecordListById3(Long userId) {
        return spRechargeRecordMapper.selectSpRechargeRecordListById3(userId);
    }
}
