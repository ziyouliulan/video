package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_video_category_pattern】的数据库操作Service
* @createDate 2022-02-15 16:20:21
*/
public interface SpVideoCategoryPatternService extends IService<SpVideoCategoryPattern> {

}
