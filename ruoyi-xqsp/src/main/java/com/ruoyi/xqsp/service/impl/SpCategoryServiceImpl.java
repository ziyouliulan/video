package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpCategory;
import com.ruoyi.xqsp.service.SpCategoryService;
import com.ruoyi.xqsp.mapper.SpCategoryMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_category】的数据库操作Service实现
* @createDate 2022-02-16 15:29:48
*/
@Service
public class SpCategoryServiceImpl extends ServiceImpl<SpCategoryMapper, SpCategory>
    implements SpCategoryService{

}




