package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVariety;

/**
 * 综艺管理Service接口
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
public interface ISpVarietyService 
{
    /**
     * 查询综艺管理
     * 
     * @param varietyId 综艺管理ID
     * @return 综艺管理
     */
    public SpVariety selectSpVarietyById(Long varietyId);

    /**
     * 查询综艺管理列表
     * 
     * @param spVariety 综艺管理
     * @return 综艺管理集合
     */
    public List<SpVariety> selectSpVarietyList(SpVariety spVariety);

    /**
     * 新增综艺管理
     * 
     * @param spVariety 综艺管理
     * @return 结果
     */
    public int insertSpVariety(SpVariety spVariety);

    /**
     * 修改综艺管理
     * 
     * @param spVariety 综艺管理
     * @return 结果
     */
    public int updateSpVariety(SpVariety spVariety);

    /**
     * 批量删除综艺管理
     * 
     * @param varietyIds 需要删除的综艺管理ID
     * @return 结果
     */
    public int deleteSpVarietyByIds(Long[] varietyIds);

    /**
     * 删除综艺管理信息
     * 
     * @param varietyId 综艺管理ID
     * @return 结果
     */
    public int deleteSpVarietyById(Long varietyId);

    /**
     * 最多观看
     * @return
     */
    List<SpVariety> selectSpVarietySortWatch();

    /**
     * 最多点赞喜欢
     * @return
     */
    List<SpVariety> selectSpVarietySortLike();

    /**
     * 首页综艺显示
     * @param spVariety
     * @return
     */
    List<SpVariety> selectSpVarietyLists(SpVariety spVariety);
}
