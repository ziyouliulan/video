package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVersionMapper;
import com.ruoyi.xqsp.domain.SpVersion;
import com.ruoyi.xqsp.service.ISpVersionService;

/**
 * 版本设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-02
 */
@Service
public class SpVersionServiceImpl implements ISpVersionService 
{
    @Autowired
    private SpVersionMapper spVersionMapper;

    /**
     * 查询版本设置
     * 
     * @param versionId 版本设置ID
     * @return 版本设置
     */
    @Override
    public SpVersion selectSpVersionById(Long versionId)
    {
        return spVersionMapper.selectSpVersionById(versionId);
    }

    /**
     * 查询版本设置列表
     * 
     * @param spVersion 版本设置
     * @return 版本设置
     */
    @Override
    public List<SpVersion> selectSpVersionList(SpVersion spVersion)
    {
        return spVersionMapper.selectSpVersionList(spVersion);
    }

    /**
     * 新增版本设置
     * 
     * @param spVersion 版本设置
     * @return 结果
     */
    @Override
    public int insertSpVersion(SpVersion spVersion)
    {
        return spVersionMapper.insertSpVersion(spVersion);
    }

    /**
     * 修改版本设置
     * 
     * @param spVersion 版本设置
     * @return 结果
     */
    @Override
    public int updateSpVersion(SpVersion spVersion)
    {
        return spVersionMapper.updateSpVersion(spVersion);
    }

    /**
     * 批量删除版本设置
     * 
     * @param versionIds 需要删除的版本设置ID
     * @return 结果
     */
    @Override
    public int deleteSpVersionByIds(Long[] versionIds)
    {
        return spVersionMapper.deleteSpVersionByIds(versionIds);
    }

    /**
     * 删除版本设置信息
     * 
     * @param versionId 版本设置ID
     * @return 结果
     */
    @Override
    public int deleteSpVersionById(Long versionId)
    {
        return spVersionMapper.deleteSpVersionById(versionId);
    }
}
