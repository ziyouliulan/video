package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpVideo;
import com.ruoyi.xqsp.mapper.SpVideoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicTagMapper;
import com.ruoyi.xqsp.domain.SpComicTag;
import com.ruoyi.xqsp.service.ISpComicTagService;

/**
 * 漫画标签Service业务层处理
 *
 * @author ruoyi
 * @date 2021-05-24
 */
@Service
public class SpComicTagServiceImpl extends ServiceImpl<SpComicTagMapper, SpComicTag> implements ISpComicTagService
{
    @Autowired
    private SpComicTagMapper spComicTagMapper;

    /**
     * 查询漫画标签
     *
     * @param comicTagId 漫画标签ID
     * @return 漫画标签
     */
    @Override
    public SpComicTag selectSpComicTagById(Long comicTagId)
    {
        return spComicTagMapper.selectSpComicTagById(comicTagId);
    }

    /**
     * 查询漫画标签列表
     *
     * @param spComicTag 漫画标签
     * @return 漫画标签
     */
    @Override
    public List<SpComicTag> selectSpComicTagList(SpComicTag spComicTag)
    {
        return spComicTagMapper.selectSpComicTagList(spComicTag);
    }

    /**
     * 新增漫画标签
     *
     * @param spComicTag 漫画标签
     * @return 结果
     */
    @Override
    public int insertSpComicTag(SpComicTag spComicTag)
    {
        return spComicTagMapper.insertSpComicTag(spComicTag);
    }

    /**
     * 修改漫画标签
     *
     * @param spComicTag 漫画标签
     * @return 结果
     */
    @Override
    public int updateSpComicTag(SpComicTag spComicTag)
    {
        return spComicTagMapper.updateSpComicTag(spComicTag);
    }

    /**
     * 批量删除漫画标签
     *
     * @param comicTagIds 需要删除的漫画标签ID
     * @return 结果
     */
    @Override
    public int deleteSpComicTagByIds(Long[] comicTagIds)
    {
        return spComicTagMapper.deleteSpComicTagByIds(comicTagIds);
    }

    /**
     * 删除漫画标签信息
     *
     * @param comicTagId 漫画标签ID
     * @return 结果
     */
    @Override
    public int deleteSpComicTagById(Long comicTagId)
    {
        return spComicTagMapper.deleteSpComicTagById(comicTagId);
    }
}
