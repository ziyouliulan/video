package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.PaymentChannel;
import com.ruoyi.xqsp.service.PaymentChannelService;
import com.ruoyi.xqsp.mapper.PaymentChannelMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【payment_channel】的数据库操作Service实现
* @createDate 2022-05-26 22:45:12
*/
@Service
public class PaymentChannelServiceImpl extends ServiceImpl<PaymentChannelMapper, PaymentChannel>
    implements PaymentChannelService{

}




