package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpKeyMapper;
import com.ruoyi.xqsp.domain.SpKey;
import com.ruoyi.xqsp.service.ISpKeyService;

/**
 * 激活码表Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-14
 */
@Service
public class SpKeyServiceImpl implements ISpKeyService 
{
    @Autowired
    private SpKeyMapper spKeyMapper;

    /**
     * 查询激活码表
     * 
     * @param keyId 激活码表ID
     * @return 激活码表
     */
    @Override
    public SpKey selectSpKeyById(Long keyId)
    {
        return spKeyMapper.selectSpKeyById(keyId);
    }

    /**
     * 查询激活码表列表
     * 
     * @param spKey 激活码表
     * @return 激活码表
     */
    @Override
    public List<SpKey> selectSpKeyList(SpKey spKey)
    {
        return spKeyMapper.selectSpKeyList(spKey);
    }

    /**
     * 新增激活码表
     * 
     * @param spKey 激活码表
     * @return 结果
     */
    @Override
    public int insertSpKey(SpKey spKey)
    {
        return spKeyMapper.insertSpKey(spKey);
    }

    /**
     * 修改激活码表
     * 
     * @param spKey 激活码表
     * @return 结果
     */
    @Override
    public int updateSpKey(SpKey spKey)
    {
        return spKeyMapper.updateSpKey(spKey);
    }

    /**
     * 批量删除激活码表
     * 
     * @param keyIds 需要删除的激活码表ID
     * @return 结果
     */
    @Override
    public int deleteSpKeyByIds(Long[] keyIds)
    {
        return spKeyMapper.deleteSpKeyByIds(keyIds);
    }

    /**
     * 删除激活码表信息
     * 
     * @param keyId 激活码表ID
     * @return 结果
     */
    @Override
    public int deleteSpKeyById(Long keyId)
    {
        return spKeyMapper.deleteSpKeyById(keyId);
    }

    @Override
    public SpKey selectSpKeyByCode(String code) {
        return spKeyMapper.selectSpKeyByCode(code);
    }
}
