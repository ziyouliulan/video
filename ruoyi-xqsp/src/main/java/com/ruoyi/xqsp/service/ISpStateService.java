package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpState;

/**
 * APP状态Service接口
 * 
 * @author ruoyi
 * @date 2021-06-10
 */
public interface ISpStateService 
{
    /**
     * 查询APP状态
     * 
     * @param stateId APP状态ID
     * @return APP状态
     */
    public SpState selectSpStateById(Long stateId);

    /**
     * 查询APP状态列表
     * 
     * @param spState APP状态
     * @return APP状态集合
     */
    public List<SpState> selectSpStateList(SpState spState);

    /**
     * 新增APP状态
     * 
     * @param spState APP状态
     * @return 结果
     */
    public int insertSpState(SpState spState);

    /**
     * 修改APP状态
     * 
     * @param spState APP状态
     * @return 结果
     */
    public int updateSpState(SpState spState);

    /**
     * 批量删除APP状态
     * 
     * @param stateIds 需要删除的APP状态ID
     * @return 结果
     */
    public int deleteSpStateByIds(Long[] stateIds);

    /**
     * 删除APP状态信息
     * 
     * @param stateId APP状态ID
     * @return 结果
     */
    public int deleteSpStateById(Long stateId);
}
