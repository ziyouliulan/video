package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.PhoenixReportMapper;
import com.ruoyi.xqsp.domain.PhoenixReport;
import com.ruoyi.xqsp.service.IPhoenixReportService;

/**
 * 楼凤举报Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-21
 */
@Service
public class PhoenixReportServiceImpl implements IPhoenixReportService 
{
    @Autowired
    private PhoenixReportMapper phoenixReportMapper;

    /**
     * 查询楼凤举报
     * 
     * @param reportId 楼凤举报ID
     * @return 楼凤举报
     */
    @Override
    public PhoenixReport selectPhoenixReportById(Long reportId)
    {
        return phoenixReportMapper.selectPhoenixReportById(reportId);
    }

    /**
     * 查询楼凤举报列表
     * 
     * @param phoenixReport 楼凤举报
     * @return 楼凤举报
     */
    @Override
    public List<PhoenixReport> selectPhoenixReportList(PhoenixReport phoenixReport)
    {
        return phoenixReportMapper.selectPhoenixReportList(phoenixReport);
    }

    /**
     * 新增楼凤举报
     * 
     * @param phoenixReport 楼凤举报
     * @return 结果
     */
    @Override
    public int insertPhoenixReport(PhoenixReport phoenixReport)
    {
        return phoenixReportMapper.insertPhoenixReport(phoenixReport);
    }

    /**
     * 修改楼凤举报
     * 
     * @param phoenixReport 楼凤举报
     * @return 结果
     */
    @Override
    public int updatePhoenixReport(PhoenixReport phoenixReport)
    {
        return phoenixReportMapper.updatePhoenixReport(phoenixReport);
    }

    /**
     * 批量删除楼凤举报
     * 
     * @param reportIds 需要删除的楼凤举报ID
     * @return 结果
     */
    @Override
    public int deletePhoenixReportByIds(Long[] reportIds)
    {
        return phoenixReportMapper.deletePhoenixReportByIds(reportIds);
    }

    /**
     * 删除楼凤举报信息
     * 
     * @param reportId 楼凤举报ID
     * @return 结果
     */
    @Override
    public int deletePhoenixReportById(Long reportId)
    {
        return phoenixReportMapper.deletePhoenixReportById(reportId);
    }

    @Override
    public int updatePhoenixReportByPhoenixId(Long phoenixId, String phoenixState) {
        return phoenixReportMapper.updatePhoenixReportByPhoenixId(phoenixId,phoenixState);
    }
}
