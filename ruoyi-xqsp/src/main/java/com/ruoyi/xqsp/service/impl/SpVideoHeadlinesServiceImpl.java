package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpVideoHeadlines;
import com.ruoyi.xqsp.service.SpVideoHeadlinesService;
import com.ruoyi.xqsp.mapper.SpVideoHeadlinesMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_video_headlines(视频详情)】的数据库操作Service实现
* @createDate 2022-06-18 15:47:38
*/
@Service
public class SpVideoHeadlinesServiceImpl extends ServiceImpl<SpVideoHeadlinesMapper, SpVideoHeadlines>
    implements SpVideoHeadlinesService{

}




