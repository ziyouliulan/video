package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpAdviceFeedbackMapper;
import com.ruoyi.xqsp.domain.SpAdviceFeedback;
import com.ruoyi.xqsp.service.ISpAdviceFeedbackService;

/**
 * 意见反馈Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
@Service
public class SpAdviceFeedbackServiceImpl extends ServiceImpl<SpAdviceFeedbackMapper,SpAdviceFeedback> implements ISpAdviceFeedbackService
{
    @Autowired
    private SpAdviceFeedbackMapper spAdviceFeedbackMapper;

    /**
     * 查询意见反馈
     * 
     * @param adviceFeedbackId 意见反馈ID
     * @return 意见反馈
     */
    @Override
    public SpAdviceFeedback selectSpAdviceFeedbackById(Long adviceFeedbackId)
    {
        return spAdviceFeedbackMapper.selectSpAdviceFeedbackById(adviceFeedbackId);
    }

    /**
     * 查询意见反馈列表
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<SpAdviceFeedback> selectSpAdviceFeedbackList(SpAdviceFeedback spAdviceFeedback)
    {
        return spAdviceFeedbackMapper.selectSpAdviceFeedbackList(spAdviceFeedback);
    }

    /**
     * 新增意见反馈
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int insertSpAdviceFeedback(SpAdviceFeedback spAdviceFeedback)
    {
        return spAdviceFeedbackMapper.insertSpAdviceFeedback(spAdviceFeedback);
    }

    /**
     * 修改意见反馈
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int updateSpAdviceFeedback(SpAdviceFeedback spAdviceFeedback)
    {
        return spAdviceFeedbackMapper.updateSpAdviceFeedback(spAdviceFeedback);
    }

    /**
     * 批量删除意见反馈
     * 
     * @param adviceFeedbackIds 需要删除的意见反馈ID
     * @return 结果
     */
    @Override
    public int deleteSpAdviceFeedbackByIds(Long[] adviceFeedbackIds)
    {
        return spAdviceFeedbackMapper.deleteSpAdviceFeedbackByIds(adviceFeedbackIds);
    }

    /**
     * 删除意见反馈信息
     * 
     * @param adviceFeedbackId 意见反馈ID
     * @return 结果
     */
    @Override
    public int deleteSpAdviceFeedbackById(Long adviceFeedbackId)
    {
        return spAdviceFeedbackMapper.deleteSpAdviceFeedbackById(adviceFeedbackId);
    }

    @Override
    public List<SpAdviceFeedback> selectSpAdviceFeedbackByUserId(Long userId) {
        return spAdviceFeedbackMapper.selectSpAdviceFeedbackByUserId(userId);
    }
}
