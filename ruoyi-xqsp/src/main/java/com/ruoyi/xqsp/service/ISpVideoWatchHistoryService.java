package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpVideoWatchHistory;

/**
 * 视频观看历史Service接口
 *
 * @author ruoyi
 * @date 2021-05-11
 */
public interface ISpVideoWatchHistoryService extends IService<SpVideoWatchHistory>
{
    /**
     * 查询视频观看历史
     *
     * @param videoWatchHistoryId 视频观看历史ID
     * @return 视频观看历史
     */
    public SpVideoWatchHistory selectSpVideoWatchHistoryById(Long videoWatchHistoryId);

    /**
     * 查询视频观看历史列表
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 视频观看历史集合
     */
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryList(SpVideoWatchHistory spVideoWatchHistory);
    /**
     * 查询视频观看历史列表
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 视频观看历史集合
     */
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryList2(SpVideoWatchHistory spVideoWatchHistory);

    /**
     * 新增视频观看历史
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 结果
     */
    public int insertSpVideoWatchHistory(SpVideoWatchHistory spVideoWatchHistory);

    /**
     * 修改视频观看历史
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 结果
     */
    public int updateSpVideoWatchHistory(SpVideoWatchHistory spVideoWatchHistory);

    /**
     * 批量删除视频观看历史
     *
     * @param videoWatchHistoryIds 需要删除的视频观看历史ID
     * @return 结果
     */
    public int deleteSpVideoWatchHistoryByIds(Long[] videoWatchHistoryIds);

    /**
     * 删除视频观看历史信息
     *
     * @param videoWatchHistoryId 视频观看历史ID
     * @return 结果
     */
    public int deleteSpVideoWatchHistoryById(Long videoWatchHistoryId);

    List<SpVideoWatchHistory> selectSpVideoWatchHistoryByUserId(Long userId);

    List<SpVideoWatchHistory> selectSpVideoWatchHistoryByUserIdV2(Long userId);

    int deleteSpVideoWatchHistory(Long videoId, Long type1);
}
