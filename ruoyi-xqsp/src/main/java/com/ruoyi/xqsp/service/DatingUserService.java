package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.DatingUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_dating_user(用户信息表)】的数据库操作Service
* @createDate 2022-07-05 21:23:08
*/
public interface DatingUserService extends IService<DatingUser> {

    List<DatingUser> favoritesList(Integer userId);

}
