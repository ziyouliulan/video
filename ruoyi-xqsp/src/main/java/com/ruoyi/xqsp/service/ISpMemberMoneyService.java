package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpMemberMoney;

/**
 * 会员设置Service接口
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
public interface ISpMemberMoneyService 
{
    /**
     * 查询会员设置
     * 
     * @param memberMoneyId 会员设置ID
     * @return 会员设置
     */
    public SpMemberMoney selectSpMemberMoneyById(Long memberMoneyId);

    /**
     * 查询会员设置列表
     * 
     * @param spMemberMoney 会员设置
     * @return 会员设置集合
     */
    public List<SpMemberMoney> selectSpMemberMoneyList(SpMemberMoney spMemberMoney);

    /**
     * 新增会员设置
     * 
     * @param spMemberMoney 会员设置
     * @return 结果
     */
    public int insertSpMemberMoney(SpMemberMoney spMemberMoney);

    /**
     * 修改会员设置
     * 
     * @param spMemberMoney 会员设置
     * @return 结果
     */
    public int updateSpMemberMoney(SpMemberMoney spMemberMoney);

    /**
     * 批量删除会员设置
     * 
     * @param memberMoneyIds 需要删除的会员设置ID
     * @return 结果
     */
    public int deleteSpMemberMoneyByIds(Long[] memberMoneyIds);

    /**
     * 删除会员设置信息
     * 
     * @param memberMoneyId 会员设置ID
     * @return 结果
     */
    public int deleteSpMemberMoneyById(Long memberMoneyId);
}
