package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.Notice;
import com.ruoyi.xqsp.service.NoticeService;
import com.ruoyi.xqsp.mapper.NoticeMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sys_notice(通知公告表)】的数据库操作Service实现
* @createDate 2022-06-12 21:42:33
*/
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice>
    implements NoticeService{

}




