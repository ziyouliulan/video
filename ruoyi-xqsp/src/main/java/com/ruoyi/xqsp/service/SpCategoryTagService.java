package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpCategoryTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_category_tag】的数据库操作Service
* @createDate 2022-02-17 16:19:07
*/
public interface SpCategoryTagService extends IService<SpCategoryTag> {

}
