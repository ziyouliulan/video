package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.VideoTagConfigs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author lpden
* @description 针对表【sys_video_tag_configs(视频分类)】的数据库操作Service
* @createDate 2022-07-09 22:08:05
*/
public interface VideoTagConfigsService extends IService<VideoTagConfigs> {

    List<Map> category(Integer id);
}
