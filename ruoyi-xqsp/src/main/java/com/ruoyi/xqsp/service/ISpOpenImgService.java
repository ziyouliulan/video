package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpOpenImg;

/**
 * 开屏广告Service接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface ISpOpenImgService 
{
    /**
     * 查询开屏广告
     * 
     * @param openImgId 开屏广告ID
     * @return 开屏广告
     */
    public SpOpenImg selectSpOpenImgById(Long openImgId);

    /**
     * 查询开屏广告列表
     * 
     * @param spOpenImg 开屏广告
     * @return 开屏广告集合
     */
    public List<SpOpenImg> selectSpOpenImgList(SpOpenImg spOpenImg);

    /**
     * 新增开屏广告
     * 
     * @param spOpenImg 开屏广告
     * @return 结果
     */
    public int insertSpOpenImg(SpOpenImg spOpenImg);

    /**
     * 修改开屏广告
     * 
     * @param spOpenImg 开屏广告
     * @return 结果
     */
    public int updateSpOpenImg(SpOpenImg spOpenImg);

    /**
     * 批量删除开屏广告
     * 
     * @param openImgIds 需要删除的开屏广告ID
     * @return 结果
     */
    public int deleteSpOpenImgByIds(Long[] openImgIds);

    /**
     * 删除开屏广告信息
     * 
     * @param openImgId 开屏广告ID
     * @return 结果
     */
    public int deleteSpOpenImgById(Long openImgId);
}
