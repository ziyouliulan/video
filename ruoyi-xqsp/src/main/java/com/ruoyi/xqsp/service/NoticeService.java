package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sys_notice(通知公告表)】的数据库操作Service
* @createDate 2022-06-12 21:42:33
*/
public interface NoticeService extends IService<Notice> {

}
