package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpComicChapter;
import com.ruoyi.xqsp.domain.SpVideo;

/**
 * 漫画章节Service接口
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
public interface ISpComicChapterService extends IService<SpComicChapter>
{
    /**
     * 查询漫画章节
     * 
     * @param comicChapterId 漫画章节ID
     * @return 漫画章节
     */
    public SpComicChapter selectSpComicChapterById(Long comicChapterId);

    /**
     * 查询漫画章节列表
     * 
     * @param spComicChapter 漫画章节
     * @return 漫画章节集合
     */
    public List<SpComicChapter> selectSpComicChapterList(SpComicChapter spComicChapter);

    /**
     * 新增漫画章节
     * 
     * @param spComicChapter 漫画章节
     * @return 结果
     */
    public int insertSpComicChapter(SpComicChapter spComicChapter);

    /**
     * 修改漫画章节
     * 
     * @param spComicChapter 漫画章节
     * @return 结果
     */
    public int updateSpComicChapter(SpComicChapter spComicChapter);

    /**
     * 批量删除漫画章节
     * 
     * @param comicChapterIds 需要删除的漫画章节ID
     * @return 结果
     */
    public int deleteSpComicChapterByIds(Long[] comicChapterIds);

    /**
     * 删除漫画章节信息
     * 
     * @param comicChapterId 漫画章节ID
     * @return 结果
     */
    public int deleteSpComicChapterById(Long comicChapterId);

    /**
     * 根据漫画id查询章节
     * @param comicId
     * @return
     */
    List<SpComicChapter> selectSpComicChapterByComicId(Long comicId);

    /**
     * 根据漫画id查询章节数量
     * @param comicId
     * @return
     */
    Long selectSpComicChapterCountByComicId(Long comicId);


    SpComicChapter selectSpComicChapter(SpComicChapter spComicChapter);

    List<SpComicChapter> selectSpComicChapterByComicId1(SpComicChapter spComicChapter);


    /**
     * 查询后续章节
     * @Author lipeng
     * @Date 2022/5/7 23:17
     * @param SerialNumber
     * @param number
     * @return java.util.List<com.ruoyi.xqsp.domain.SpComicChapter> 
     */
    List<SpComicChapter> selectSerialNumber(Long comicId,Integer SerialNumber,Integer number);



}
