package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicBookmarkMapper;
import com.ruoyi.xqsp.domain.SpComicBookmark;
import com.ruoyi.xqsp.service.ISpComicBookmarkService;

/**
 * 漫画书签Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-22
 */
@Service
public class SpComicBookmarkServiceImpl implements ISpComicBookmarkService 
{
    @Autowired
    private SpComicBookmarkMapper spComicBookmarkMapper;

    /**
     * 查询漫画书签
     * 
     * @param comicBookmarkId 漫画书签ID
     * @return 漫画书签
     */
    @Override
    public SpComicBookmark selectSpComicBookmarkById(Long comicBookmarkId)
    {
        return spComicBookmarkMapper.selectSpComicBookmarkById(comicBookmarkId);
    }

    /**
     * 查询漫画书签列表
     * 
     * @param spComicBookmark 漫画书签
     * @return 漫画书签
     */
    @Override
    public List<SpComicBookmark> selectSpComicBookmarkList(SpComicBookmark spComicBookmark)
    {
        return spComicBookmarkMapper.selectSpComicBookmarkList(spComicBookmark);
    }

    /**
     * 新增漫画书签
     * 
     * @param spComicBookmark 漫画书签
     * @return 结果
     */
    @Override
    public int insertSpComicBookmark(SpComicBookmark spComicBookmark)
    {
        return spComicBookmarkMapper.insertSpComicBookmark(spComicBookmark);
    }

    /**
     * 修改漫画书签
     * 
     * @param spComicBookmark 漫画书签
     * @return 结果
     */
    @Override
    public int updateSpComicBookmark(SpComicBookmark spComicBookmark)
    {
        return spComicBookmarkMapper.updateSpComicBookmark(spComicBookmark);
    }

    /**
     * 批量删除漫画书签
     * 
     * @param comicBookmarkIds 需要删除的漫画书签ID
     * @return 结果
     */
    @Override
    public int deleteSpComicBookmarkByIds(Long[] comicBookmarkIds)
    {
        return spComicBookmarkMapper.deleteSpComicBookmarkByIds(comicBookmarkIds);
    }

    /**
     * 删除漫画书签信息
     * 
     * @param comicBookmarkId 漫画书签ID
     * @return 结果
     */
    @Override
    public int deleteSpComicBookmarkById(Long comicBookmarkId)
    {
        return spComicBookmarkMapper.deleteSpComicBookmarkById(comicBookmarkId);
    }

    @Override
    public SpComicBookmark selectSpComicBookmark(SpComicBookmark spComicBookmark) {
        return spComicBookmarkMapper.selectSpComicBookmark(spComicBookmark);
    }

    @Override
    public int updateSpComicBookmark1(SpComicBookmark spComicBookmark) {
        return spComicBookmarkMapper.updateSpComicBookmark1(spComicBookmark);
    }
}
