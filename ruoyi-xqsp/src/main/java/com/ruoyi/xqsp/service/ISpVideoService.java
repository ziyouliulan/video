package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.utils.status.ResponseUtil;
import com.ruoyi.xqsp.domain.SpVideo;
import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;

/**
 * 视频管理Service接口
 *
 * @author ruoyi
 * @date 2021-04-23
 */
public interface ISpVideoService extends IService<SpVideo>
{
    /**
     * 查询视频管理
     *
     * @param videoId 视频管理ID
     * @return 视频管理
     */
    public SpVideo selectSpVideoById(Long videoId);

    /**
     * 查询视频管理列表
     *
     * @param spVideo 视频管理
     * @return 视频管理集合
     */
    public List<SpVideo> selectSpVideoList(SpVideo spVideo);

    /**
     * 新增视频管理
     *
     * @param spVideo 视频管理
     * @return 结果
     */
    public int insertSpVideo(SpVideo spVideo);

    /**
     * 修改视频管理
     *
     * @param spVideo 视频管理
     * @return 结果
     */
    public int updateSpVideo(SpVideo spVideo);

    /**
     * 批量删除视频管理
     *
     * @param videoIds 需要删除的视频管理ID
     * @return 结果
     */
    public int deleteSpVideoByIds(Long[] videoIds);

    /**
     * 删除视频管理信息
     *
     * @param videoId 视频管理ID
     * @return 结果
     */
    public int deleteSpVideoById(Long videoId);

    /**
     * 根据演员id获取视频信息
     * @param actorId
     * @return
     */
    public List<SpVideo> selectSpVideoByActorId(Long actorId);

    /**
     * 根据分类查询
     * @param videoCategoryId
     * @return
     */
    public List<SpVideo> selectSpVideoByCategoryId(Long videoCategoryId);

    /**
     * 点赞人数加一
     * @param videoId
     * @return
     */
    int updateSpVideoLikeNumber(Long videoId);

    /**
     * 点赞人数减一
     * @param videoId
     * @return
     */
    int updateSpVideoLikeNumber1(Long videoId);

    /**
     * 根据演员id查询该演员的视频
     * @param actorId
     * @return
     */
    List<SpVideo> selectSpVideoListByActorId(Long actorId);

    /**
     * 根据演员id查询该演员的视频数量
     * @param actorId
     * @return
     */
    Integer selectCountByActorId(Long actorId);

    /**
     * 差评人数加一
     * @param videoId
     * @return
     */
    int updateSpVideoUnLikeNumber(Long videoId);

    /**
     * 差评人数减一
     * @param videoId
     * @return
     */
    int updateSpVideoUnLikeNumber1(Long videoId);

    /**
     * 根据id查询视频
     * @param videoCategoryId
     * @return
     */
    List<SpVideo> selectSpVideoByCategoryIds(Long videoCategoryId);

    /**
     * 收藏人数加一
     * @param videoId
     * @return
     */
    int updateSpVideoCollectNumber(Long videoId);

    int updateSpVideoCollectNumber1(Long videoId);

    /**
     * 根据时间排序
     * @param spVideo
     * @return
     */
    List<SpVideo> selectSpVideoListOrderByDate(SpVideo spVideo);

    /**
     * 根据点赞人数排序
     * @param spVideo
     * @return
     */
    List<SpVideo> selectSpVideoListOrderByLike(SpVideo spVideo);

    /**
     * 根据观看人数排序
     * @param spVideo
     * @return
     */
    List<SpVideo> selectSpVideoListOrderByWatch(SpVideo spVideo);

    /**
     * 根据收藏排序
     * @param spVideo
     * @return
     */
    List<SpVideo> selectSpVideoListOrderByCollect(SpVideo spVideo);

    /**
     * 查询所有
     * @param spVideo
     * @return
     */
    List<SpVideo> selectSpVideoLists(SpVideo spVideo);

    /**
     * 观看人数加一
     * @param videoWatchHistoryTypeId
     * @return
     */
    int updateSpVideoWatchNumber(Long videoWatchHistoryTypeId);

    /**
     * 根据名字模糊查询
     * @param name
     * @return
     */
    List<SpVideo> selectSpVideoListByName(String name);

    /**
     * 查询所有动漫
     * @param videoCategoryId
     * @return
     */
    List<SpVideo> selectSpAnimeVideoList(Long videoCategoryId);

    /**
     * 根据时间排序
     * @param videoCategoryId
     * @return
     */
    List<SpVideo> selectSpAnimeVideoListOrderTime(Long videoCategoryId);

    /**
     * 人气点赞排序
     * @param videoCategoryId
     * @return
     */
    List<SpVideo> selectSpAnimeVideoListOrderLike(Long videoCategoryId);

    /**
     * 观看次数排序
     * @param videoCategoryId
     * @return
     */
    List<SpVideo> selectSpAnimeVideoListOrderWatch(Long videoCategoryId);

    /**
     * 动漫的模糊查询
     * @param videoName
     * @return
     */
    List<SpVideo> selectSpAnimeVideoListByName(String videoName);


    public ResponseUtil likeAllName(String name, Integer pageNum, Integer pageSize);
}
