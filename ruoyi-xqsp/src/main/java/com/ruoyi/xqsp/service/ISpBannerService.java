package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpBanner;

/**
 * banner图操作Service接口
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
public interface ISpBannerService 
{
    /**
     * 查询banner图操作
     * 
     * @param bannerId banner图操作ID
     * @return banner图操作
     */
    public SpBanner selectSpBannerById(Long bannerId);

    /**
     * 查询banner图操作列表
     * 
     * @param spBanner banner图操作
     * @return banner图操作集合
     */
    public List<SpBanner> selectSpBannerList(SpBanner spBanner);

    /**
     * 新增banner图操作
     * 
     * @param spBanner banner图操作
     * @return 结果
     */
    public int insertSpBanner(SpBanner spBanner);

    /**
     * 修改banner图操作
     * 
     * @param spBanner banner图操作
     * @return 结果
     */
    public int updateSpBanner(SpBanner spBanner);

    /**
     * 批量删除banner图操作
     * 
     * @param bannerIds 需要删除的banner图操作ID
     * @return 结果
     */
    public int deleteSpBannerByIds(Long[] bannerIds);

    /**
     * 删除banner图操作信息
     * 
     * @param bannerId banner图操作ID
     * @return 结果
     */
    public int deleteSpBannerById(Long bannerId);
}
