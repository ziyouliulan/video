package com.ruoyi.xqsp.service;

import java.util.List;

import com.ruoyi.xqsp.domain.ActorMessage;
import com.ruoyi.xqsp.domain.SpActorMessage;

/**
 * 演员信息Service接口
 * 
 * @author ruoyi
 * @date 2021-04-22
 */
public interface ISpActorMessageService 
{
    /**
     * 查询演员信息
     * 
     * @param actorMessageId 演员信息ID
     * @return 演员信息
     */
    public SpActorMessage selectSpActorMessageById(Long actorMessageId);
    /**
     * 查询演员信息根据演员id
     *
     * @param actorId 演员ID
     * @return 演员信息
     */
    public SpActorMessage selectSpActorMessageByActorId(Long actorId);

    /**
     * 查询演员信息列表
     * 
     * @param ActorMessage 演员信息
     * @return 演员信息集合
     */
    public List<SpActorMessage> selectSpActorMessageList(SpActorMessage spActorMessage);

    /**
     * 查询演员信息列表加演员名字
     * @param actorMessage
     * @return
     */
    public List<ActorMessage> selectActorMessageList(SpActorMessage actorMessage);
    /**
     * 新增演员信息
     * 
     * @param spActorMessage 演员信息
     * @return 结果
     */
    public int insertSpActorMessage(SpActorMessage spActorMessage);

    /**
     * 修改演员信息
     * 
     * @param spActorMessage 演员信息
     * @return 结果
     */
    public int updateSpActorMessage(SpActorMessage spActorMessage);

    /**
     * 批量删除演员信息
     * 
     * @param actorMessageIds 需要删除的演员信息ID
     * @return 结果
     */
    public int deleteSpActorMessageByIds(Long[] actorMessageIds);

    /**
     * 删除演员信息信息
     * 
     * @param actorMessageId 演员信息ID
     * @return 结果
     */
    public int deleteSpActorMessageById(Long actorMessageId);
}
