package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpGroupMapper;
import com.ruoyi.xqsp.domain.SpGroup;
import com.ruoyi.xqsp.service.ISpGroupService;

/**
 * 车友群Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-28
 */
@Service
public class SpGroupServiceImpl implements ISpGroupService 
{
    @Autowired
    private SpGroupMapper spGroupMapper;

    /**
     * 查询车友群
     * 
     * @param groupId 车友群ID
     * @return 车友群
     */
    @Override
    public SpGroup selectSpGroupById(Long groupId)
    {
        return spGroupMapper.selectSpGroupById(groupId);
    }

    /**
     * 查询车友群列表
     * 
     * @param spGroup 车友群
     * @return 车友群
     */
    @Override
    public List<SpGroup> selectSpGroupList(SpGroup spGroup)
    {
        return spGroupMapper.selectSpGroupList(spGroup);
    }

    /**
     * 新增车友群
     * 
     * @param spGroup 车友群
     * @return 结果
     */
    @Override
    public int insertSpGroup(SpGroup spGroup)
    {
        return spGroupMapper.insertSpGroup(spGroup);
    }

    /**
     * 修改车友群
     * 
     * @param spGroup 车友群
     * @return 结果
     */
    @Override
    public int updateSpGroup(SpGroup spGroup)
    {
        return spGroupMapper.updateSpGroup(spGroup);
    }

    /**
     * 批量删除车友群
     * 
     * @param groupIds 需要删除的车友群ID
     * @return 结果
     */
    @Override
    public int deleteSpGroupByIds(Long[] groupIds)
    {
        return spGroupMapper.deleteSpGroupByIds(groupIds);
    }

    /**
     * 删除车友群信息
     * 
     * @param groupId 车友群ID
     * @return 结果
     */
    @Override
    public int deleteSpGroupById(Long groupId)
    {
        return spGroupMapper.deleteSpGroupById(groupId);
    }
}
