package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.DatingUserCollection;
import com.ruoyi.xqsp.service.DatingUserCollectionService;
import com.ruoyi.xqsp.mapper.DatingUserCollectionMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_dating_user_collection】的数据库操作Service实现
* @createDate 2022-02-28 15:10:23
*/
@Service
public class DatingUserCollectionServiceImpl extends ServiceImpl<DatingUserCollectionMapper, DatingUserCollection>
    implements DatingUserCollectionService{

}




