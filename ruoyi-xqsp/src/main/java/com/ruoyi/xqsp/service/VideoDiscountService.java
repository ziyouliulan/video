package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.VideoDiscount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【video_discount】的数据库操作Service
* @createDate 2022-06-12 19:54:12
*/
public interface VideoDiscountService extends IService<VideoDiscount> {

}
