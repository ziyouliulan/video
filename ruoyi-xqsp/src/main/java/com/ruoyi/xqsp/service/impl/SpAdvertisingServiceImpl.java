package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpAdvertisingMapper;
import com.ruoyi.xqsp.domain.SpAdvertising;
import com.ruoyi.xqsp.service.ISpAdvertisingService;

/**
 * 广告位管理Service业务层处理
 *
 * @author ruoyi
 * @date 2021-04-17
 */
@Service
public class SpAdvertisingServiceImpl extends ServiceImpl<SpAdvertisingMapper, SpAdvertising> implements ISpAdvertisingService
{
    @Autowired
    private SpAdvertisingMapper spAdvertisingMapper;

    /**
     * 查询广告位管理
     *
     * @param advertisingId 广告位管理ID
     * @return 广告位管理
     */
    @Override
    public SpAdvertising selectSpAdvertisingById(Long advertisingId)
    {
        return spAdvertisingMapper.selectSpAdvertisingById(advertisingId);
    }

    /**
     * 查询广告位管理列表
     *
     * @param spAdvertising 广告位管理
     * @return 广告位管理
     */
    @Override
    public List<SpAdvertising> selectSpAdvertisingList(SpAdvertising spAdvertising)
    {
        return spAdvertisingMapper.selectSpAdvertisingList(spAdvertising);
    }

    /**
     * 新增广告位管理
     *
     * @param spAdvertising 广告位管理
     * @return 结果
     */
    @Override
    public int insertSpAdvertising(SpAdvertising spAdvertising)
    {
        return spAdvertisingMapper.insertSpAdvertising(spAdvertising);
    }

    /**
     * 修改广告位管理
     *
     * @param spAdvertising 广告位管理
     * @return 结果
     */
    @Override
    public int updateSpAdvertising(SpAdvertising spAdvertising)
    {
        return spAdvertisingMapper.updateSpAdvertising(spAdvertising);
    }

    /**
     * 批量删除广告位管理
     *
     * @param advertisingIds 需要删除的广告位管理ID
     * @return 结果
     */
    @Override
    public int deleteSpAdvertisingByIds(Long[] advertisingIds)
    {
        return spAdvertisingMapper.deleteSpAdvertisingByIds(advertisingIds);
    }

    /**
     * 删除广告位管理信息
     *
     * @param advertisingId 广告位管理ID
     * @return 结果
     */
    @Override
    public int deleteSpAdvertisingById(Long advertisingId)
    {
        return spAdvertisingMapper.deleteSpAdvertisingById(advertisingId);
    }

    @Override
    public List<SpAdvertising> selectSpAdvertisingByCategoryId(Long categoryId) {
        return spAdvertisingMapper.selectSpAdvertisingByCategoryId(categoryId);
    }

    @Override
    public List<SpAdvertising> selectSpAdvertising() {
        return spAdvertisingMapper.selectSpAdvertising();
    }

    @Override
    public int updateSpAdvertisingCount(Long advertisingId) {
        return spAdvertisingMapper.updateSpAdvertisingCount(advertisingId);
    }

    @Override
    public List<SpAdvertising> selectSpAdvertisingPhoenix() {
        return spAdvertisingMapper.selectSpAdvertisingPhoenix();
    }

    @Override
    public List<SpAdvertising> selectSpAdvertisingByComic() {
        return spAdvertisingMapper.selectSpAdvertisingByComic();
    }

    @Override
    public List<SpAdvertising> selectSpAdvertisingAnime() {
        return spAdvertisingMapper.selectSpAdvertisingAnime();
    }
}
