package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpComicUserMapper;
import com.ruoyi.xqsp.domain.SpComicUser;
import com.ruoyi.xqsp.service.ISpComicUserService;

/**
 * 书架管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
@Service
public class SpComicUserServiceImpl implements ISpComicUserService 
{
    @Autowired
    private SpComicUserMapper spComicUserMapper;

    /**
     * 查询书架管理
     * 
     * @param comicUserId 书架管理ID
     * @return 书架管理
     */
    @Override
    public SpComicUser selectSpComicUserById(Long comicUserId)
    {
        return spComicUserMapper.selectSpComicUserById(comicUserId);
    }

    /**
     * 查询书架管理列表
     * 
     * @param spComicUser 书架管理
     * @return 书架管理
     */
    @Override
    public List<SpComicUser> selectSpComicUserList(SpComicUser spComicUser)
    {
        return spComicUserMapper.selectSpComicUserList(spComicUser);
    }

    /**
     * 新增书架管理
     * 
     * @param spComicUser 书架管理
     * @return 结果
     */
    @Override
    public int insertSpComicUser(SpComicUser spComicUser)
    {
        return spComicUserMapper.insertSpComicUser(spComicUser);
    }

    /**
     * 修改书架管理
     * 
     * @param spComicUser 书架管理
     * @return 结果
     */
    @Override
    public int updateSpComicUser(SpComicUser spComicUser)
    {
        return spComicUserMapper.updateSpComicUser(spComicUser);
    }

    /**
     * 批量删除书架管理
     * 
     * @param comicUserIds 需要删除的书架管理ID
     * @return 结果
     */
    @Override
    public int deleteSpComicUserByIds(Long[] comicUserIds)
    {
        return spComicUserMapper.deleteSpComicUserByIds(comicUserIds);
    }

    /**
     * 删除书架管理信息
     * 
     * @param comicUserId 书架管理ID
     * @return 结果
     */
    @Override
    public int deleteSpComicUserById(Long comicUserId)
    {
        return spComicUserMapper.deleteSpComicUserById(comicUserId);
    }

    @Override
    public int deleteSpComicUser(SpComicUser spComicUser) {
        return spComicUserMapper.deleteSpComicUser(spComicUser);
    }

}
