package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.VipCategory;
import com.ruoyi.xqsp.service.VipCategoryService;
import com.ruoyi.xqsp.mapper.VipCategoryMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_vip_category】的数据库操作Service实现
* @createDate 2022-02-25 16:15:06
*/
@Service
public class VipCategoryServiceImpl extends ServiceImpl<VipCategoryMapper, VipCategory>
    implements VipCategoryService{

}




