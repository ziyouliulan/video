package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpMineImgMapper;
import com.ruoyi.xqsp.domain.SpMineImg;
import com.ruoyi.xqsp.service.ISpMineImgService;

/**
 * 我的广告Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
@Service
public class SpMineImgServiceImpl implements ISpMineImgService 
{
    @Autowired
    private SpMineImgMapper spMineImgMapper;

    /**
     * 查询我的广告
     * 
     * @param mineImgId 我的广告ID
     * @return 我的广告
     */
    @Override
    public SpMineImg selectSpMineImgById(Long mineImgId)
    {
        return spMineImgMapper.selectSpMineImgById(mineImgId);
    }

    /**
     * 查询我的广告列表
     * 
     * @param spMineImg 我的广告
     * @return 我的广告
     */
    @Override
    public List<SpMineImg> selectSpMineImgList(SpMineImg spMineImg)
    {
        return spMineImgMapper.selectSpMineImgList(spMineImg);
    }

    /**
     * 新增我的广告
     * 
     * @param spMineImg 我的广告
     * @return 结果
     */
    @Override
    public int insertSpMineImg(SpMineImg spMineImg)
    {
        return spMineImgMapper.insertSpMineImg(spMineImg);
    }

    /**
     * 修改我的广告
     * 
     * @param spMineImg 我的广告
     * @return 结果
     */
    @Override
    public int updateSpMineImg(SpMineImg spMineImg)
    {
        return spMineImgMapper.updateSpMineImg(spMineImg);
    }

    /**
     * 批量删除我的广告
     * 
     * @param mineImgIds 需要删除的我的广告ID
     * @return 结果
     */
    @Override
    public int deleteSpMineImgByIds(Long[] mineImgIds)
    {
        return spMineImgMapper.deleteSpMineImgByIds(mineImgIds);
    }

    /**
     * 删除我的广告信息
     * 
     * @param mineImgId 我的广告ID
     * @return 结果
     */
    @Override
    public int deleteSpMineImgById(Long mineImgId)
    {
        return spMineImgMapper.deleteSpMineImgById(mineImgId);
    }
}
