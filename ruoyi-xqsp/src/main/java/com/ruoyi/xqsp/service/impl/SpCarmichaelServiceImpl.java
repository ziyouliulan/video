package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpCarmichaelMapper;
import com.ruoyi.xqsp.domain.SpCarmichael;
import com.ruoyi.xqsp.service.ISpCarmichaelService;

/**
 * 卡密操作Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@Service
public class SpCarmichaelServiceImpl implements ISpCarmichaelService 
{
    @Autowired
    private SpCarmichaelMapper spCarmichaelMapper;

    /**
     * 查询卡密操作
     * 
     * @param carmichaelId 卡密操作ID
     * @return 卡密操作
     */
    @Override
    public SpCarmichael selectSpCarmichaelById(Long carmichaelId)
    {
        return spCarmichaelMapper.selectSpCarmichaelById(carmichaelId);
    }

    /**
     * 查询卡密操作列表
     * 
     * @param spCarmichael 卡密操作
     * @return 卡密操作
     */
    @Override
    public List<SpCarmichael> selectSpCarmichaelList(SpCarmichael spCarmichael)
    {
        return spCarmichaelMapper.selectSpCarmichaelList(spCarmichael);
    }

    /**
     * 新增卡密操作
     * 
     * @param spCarmichael 卡密操作
     * @return 结果
     */
    @Override
    public int insertSpCarmichael(SpCarmichael spCarmichael)
    {
        return spCarmichaelMapper.insertSpCarmichael(spCarmichael);
    }

    /**
     * 修改卡密操作
     * 
     * @param spCarmichael 卡密操作
     * @return 结果
     */
    @Override
    public int updateSpCarmichael(SpCarmichael spCarmichael)
    {
        return spCarmichaelMapper.updateSpCarmichael(spCarmichael);
    }

    /**
     * 批量删除卡密操作
     * 
     * @param carmichaelIds 需要删除的卡密操作ID
     * @return 结果
     */
    @Override
    public int deleteSpCarmichaelByIds(Long[] carmichaelIds)
    {
        return spCarmichaelMapper.deleteSpCarmichaelByIds(carmichaelIds);
    }

    /**
     * 删除卡密操作信息
     * 
     * @param carmichaelId 卡密操作ID
     * @return 结果
     */
    @Override
    public int deleteSpCarmichaelById(Long carmichaelId)
    {
        return spCarmichaelMapper.deleteSpCarmichaelById(carmichaelId);
    }

    @Override
    public SpCarmichael selectSpCarmichaelByName(String code) {
        return spCarmichaelMapper.selectSpCarmichaelByName(code);
    }
}
