package com.ruoyi.xqsp.service.impl;

import java.util.List;

import com.ruoyi.xqsp.domain.SpVideo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVarietyVideoMapper;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.service.ISpVarietyVideoService;

/**
 * 综艺视频Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
@Service
public class SpVarietyVideoServiceImpl implements ISpVarietyVideoService 
{
    @Autowired
    private SpVarietyVideoMapper spVarietyVideoMapper;

    /**
     * 查询综艺视频
     * 
     * @param varietyVideoId 综艺视频ID
     * @return 综艺视频
     */
    @Override
    public SpVarietyVideo selectSpVarietyVideoById(Long varietyVideoId)
    {
        return spVarietyVideoMapper.selectSpVarietyVideoById(varietyVideoId);
    }

    /**
     * 查询综艺视频列表
     * 
     * @param spVarietyVideo 综艺视频
     * @return 综艺视频
     */
    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoList(SpVarietyVideo spVarietyVideo)
    {
        return spVarietyVideoMapper.selectSpVarietyVideoList(spVarietyVideo);
    }

    /**
     * 新增综艺视频
     * 
     * @param spVarietyVideo 综艺视频
     * @return 结果
     */
    @Override
    public int insertSpVarietyVideo(SpVarietyVideo spVarietyVideo)
    {
        return spVarietyVideoMapper.insertSpVarietyVideo(spVarietyVideo);
    }

    /**
     * 修改综艺视频
     * 
     * @param spVarietyVideo 综艺视频
     * @return 结果
     */
    @Override
    public int updateSpVarietyVideo(SpVarietyVideo spVarietyVideo)
    {
        return spVarietyVideoMapper.updateSpVarietyVideo(spVarietyVideo);
    }

    /**
     * 批量删除综艺视频
     * 
     * @param varietyVideoIds 需要删除的综艺视频ID
     * @return 结果
     */
    @Override
    public int deleteSpVarietyVideoByIds(Long[] varietyVideoIds)
    {
        return spVarietyVideoMapper.deleteSpVarietyVideoByIds(varietyVideoIds);
    }

    /**
     * 删除综艺视频信息
     * 
     * @param varietyVideoId 综艺视频ID
     * @return 结果
     */
    @Override
    public int deleteSpVarietyVideoById(Long varietyVideoId)
    {
        return spVarietyVideoMapper.deleteSpVarietyVideoById(varietyVideoId);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoByVarietyId(Long varietyId) {
        return spVarietyVideoMapper.selectSpVarietyVideoByVarietyId(varietyId);
    }

    @Override
    public Integer selectCountByVarietyId(Long varietyId) {
        return spVarietyVideoMapper.selectCountByVarietyId(varietyId);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoByVarietyIds(Long varietyId) {
        return spVarietyVideoMapper.selectSpVarietyVideoByVarietyIds(varietyId);
    }

    @Override
    public int updateVideoLikeNumber(Long varietyVideoId) {
        return spVarietyVideoMapper.updateVideoLikeNumber(varietyVideoId);
    }

    @Override
    public int updateVideoLikeNumber1(Long varietyVideoId) {
        return spVarietyVideoMapper.updateVideoLikeNumber1(varietyVideoId);
    }

    @Override
    public int updateVideoUnLikeNumber1(Long varietyVideoId) {
        return spVarietyVideoMapper.updateVideoUnLikeNumber1(varietyVideoId);
    }

    @Override
    public int updateVideoUnLikeNumber(Long varietyVideoId) {
        return spVarietyVideoMapper.updateVideoUnLikeNumber(varietyVideoId);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideo() {
        return spVarietyVideoMapper.selectSpVarietyVideo();
    }

    @Override
    public int updateVideoCollectNumber(Long varietyVideoId) {
        return spVarietyVideoMapper.updateVideoCollectNumber(varietyVideoId);
    }

    @Override
    public int updateVideoCollectNumber1(Long varietyVideoId) {
        return spVarietyVideoMapper.updateVideoCollectNumber1(varietyVideoId);
    }

    @Override
    public int updateVideoWatchNumber(Long videoWatchHistoryTypeId) {
        return spVarietyVideoMapper.updateVideoWatchNumber(videoWatchHistoryTypeId);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoLists(SpVarietyVideo spVarietyVideo) {
        return spVarietyVideoMapper.selectSpVarietyVideoLists(spVarietyVideo);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoListByDate(SpVarietyVideo spVarietyVideo) {
        return spVarietyVideoMapper.selectSpVarietyVideoListByDate(spVarietyVideo);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoListByWatch(SpVarietyVideo spVarietyVideo) {
        return spVarietyVideoMapper.selectSpVarietyVideoListByWatch(spVarietyVideo);
    }

    @Override
    public List<SpVarietyVideo> selectSpVarietyVideoListByName(String name) {
        return spVarietyVideoMapper.selectSpVarietyVideoListByName(name);
    }
}
