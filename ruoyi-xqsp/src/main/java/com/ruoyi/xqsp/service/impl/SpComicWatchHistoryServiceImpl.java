package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpComicWatchHistory;
import com.ruoyi.xqsp.service.SpComicWatchHistoryService;
import com.ruoyi.xqsp.mapper.SpComicWatchHistoryMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_comic_watch_history(视频观看记录)】的数据库操作Service实现
* @createDate 2022-07-23 17:36:46
*/
@Service
public class SpComicWatchHistoryServiceImpl extends ServiceImpl<SpComicWatchHistoryMapper, SpComicWatchHistory>
    implements SpComicWatchHistoryService{

}




