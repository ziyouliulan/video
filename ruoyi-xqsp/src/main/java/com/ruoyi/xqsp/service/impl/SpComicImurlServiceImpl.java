package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpComicImurl;
import com.ruoyi.xqsp.service.SpComicImurlService;
import com.ruoyi.xqsp.mapper.SpComicImurlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_comic_imurl】的数据库操作Service实现
* @createDate 2022-04-01 14:41:26
*/
@Service
public class SpComicImurlServiceImpl extends ServiceImpl<SpComicImurlMapper, SpComicImurl>
    implements SpComicImurlService{

    @Autowired
    SpComicImurlMapper spComicImurlMapper;

    @Override
    public List<SpComicImurl> orderList(Long comicChapterId, Long comicId) {
        return spComicImurlMapper.orderList(comicChapterId,comicId);
    }
}




