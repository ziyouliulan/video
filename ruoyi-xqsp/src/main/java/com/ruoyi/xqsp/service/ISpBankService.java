package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpBank;

/**
 * 银行卡操作Service接口
 * 
 * @author ruoyi
 * @date 2021-06-07
 */
public interface ISpBankService 
{
    /**
     * 查询银行卡操作
     * 
     * @param spBankId 银行卡操作ID
     * @return 银行卡操作
     */
    public SpBank selectSpBankById(Long spBankId);

    /**
     * 查询银行卡操作列表
     * 
     * @param spBank 银行卡操作
     * @return 银行卡操作集合
     */
    public List<SpBank> selectSpBankList(SpBank spBank);

    /**
     * 新增银行卡操作
     * 
     * @param spBank 银行卡操作
     * @return 结果
     */
    public int insertSpBank(SpBank spBank);

    /**
     * 修改银行卡操作
     * 
     * @param spBank 银行卡操作
     * @return 结果
     */
    public int updateSpBank(SpBank spBank);

    /**
     * 批量删除银行卡操作
     * 
     * @param spBankIds 需要删除的银行卡操作ID
     * @return 结果
     */
    public int deleteSpBankByIds(Long[] spBankIds);

    /**
     * 删除银行卡操作信息
     * 
     * @param spBankId 银行卡操作ID
     * @return 结果
     */
    public int deleteSpBankById(Long spBankId);
}
