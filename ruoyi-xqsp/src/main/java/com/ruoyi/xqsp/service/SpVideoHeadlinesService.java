package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpVideoHeadlines;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_video_headlines(视频详情)】的数据库操作Service
* @createDate 2022-06-18 15:47:38
*/
public interface SpVideoHeadlinesService extends IService<SpVideoHeadlines> {

}
