package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpGroup;

/**
 * 车友群Service接口
 * 
 * @author ruoyi
 * @date 2021-05-28
 */
public interface ISpGroupService 
{
    /**
     * 查询车友群
     * 
     * @param groupId 车友群ID
     * @return 车友群
     */
    public SpGroup selectSpGroupById(Long groupId);

    /**
     * 查询车友群列表
     * 
     * @param spGroup 车友群
     * @return 车友群集合
     */
    public List<SpGroup> selectSpGroupList(SpGroup spGroup);

    /**
     * 新增车友群
     * 
     * @param spGroup 车友群
     * @return 结果
     */
    public int insertSpGroup(SpGroup spGroup);

    /**
     * 修改车友群
     * 
     * @param spGroup 车友群
     * @return 结果
     */
    public int updateSpGroup(SpGroup spGroup);

    /**
     * 批量删除车友群
     * 
     * @param groupIds 需要删除的车友群ID
     * @return 结果
     */
    public int deleteSpGroupByIds(Long[] groupIds);

    /**
     * 删除车友群信息
     * 
     * @param groupId 车友群ID
     * @return 结果
     */
    public int deleteSpGroupById(Long groupId);
}
