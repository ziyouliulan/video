package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import com.sun.jna.platform.win32.WinDef;

/**
 * 楼凤评价表Service接口
 * 
 * @author ruoyi
 * @date 2021-04-25
 */
public interface ISpPhoenixEvaluationService 
{
    /**
     * 查询楼凤评价表
     * 
     * @param phoenixEvaluationId 楼凤评价表ID
     * @return 楼凤评价表
     */
    public SpPhoenixEvaluation selectSpPhoenixEvaluationById(Long phoenixEvaluationId);

    /**
     * 查询楼凤评价表列表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 楼凤评价表集合
     */
    public List<SpPhoenixEvaluation> selectSpPhoenixEvaluationList(SpPhoenixEvaluation spPhoenixEvaluation);

    /**
     * 新增楼凤评价表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 结果
     */
    public int insertSpPhoenixEvaluation(SpPhoenixEvaluation spPhoenixEvaluation);

    /**
     * 修改楼凤评价表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 结果
     */
    public int updateSpPhoenixEvaluation(SpPhoenixEvaluation spPhoenixEvaluation);

    /**
     * 批量删除楼凤评价表
     * 
     * @param phoenixEvaluationIds 需要删除的楼凤评价表ID
     * @return 结果
     */
    public int deleteSpPhoenixEvaluationByIds(Long[] phoenixEvaluationIds);

    /**
     * 删除楼凤评价表信息
     * 
     * @param phoenixEvaluationId 楼凤评价表ID
     * @return 结果
     */
    public int deleteSpPhoenixEvaluationById(Long phoenixEvaluationId);

    /**
     * 根据楼凤id查询
     * @param phoenixId
     * @return
     */
    List<SpPhoenixEvaluation> selectSpPhoenixEvaluationByPhoenixId(Long phoenixId);

    /**
     * 根据楼凤id删除楼凤评论
     * @param phoenixId
     * @return
     */
    int deleteSpPhoenixEvaluationByPhoenixId(Long phoenixId);
}
