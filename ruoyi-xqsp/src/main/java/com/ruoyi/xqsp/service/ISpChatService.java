package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpChat;

/**
 * 聊天信息Service接口
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
public interface ISpChatService 
{
    /**
     * 查询聊天信息
     * 
     * @param chatId 聊天信息ID
     * @return 聊天信息
     */
    public SpChat selectSpChatById(Long chatId);

    /**
     * 查询聊天信息列表
     * 
     * @param spChat 聊天信息
     * @return 聊天信息集合
     */
    public List<SpChat> selectSpChatList(SpChat spChat);

    /**
     * 新增聊天信息
     * 
     * @param spChat 聊天信息
     * @return 结果
     */
    public int insertSpChat(SpChat spChat);

    /**
     * 修改聊天信息
     * 
     * @param spChat 聊天信息
     * @return 结果
     */
    public int updateSpChat(SpChat spChat);

    /**
     * 批量删除聊天信息
     * 
     * @param chatIds 需要删除的聊天信息ID
     * @return 结果
     */
    public int deleteSpChatByIds(Long[] chatIds);

    /**
     * 删除聊天信息信息
     * 
     * @param chatId 聊天信息ID
     * @return 结果
     */
    public int deleteSpChatById(Long chatId);

    /**
     * 根据发送人和接收人id查询
     * @param userId
     * @param serviceId
     * @return
     */
    List<SpChat> selectSpChatLists(Long userId, Long serviceId);

    /**
     * 查询所有未读消息
     * @param spChat
     * @return
     */
    List<SpChat> selectSpChatList1(SpChat spChat);

    /**
     * 根据发送人和接收人查询
     * @param spChat
     * @return
     */
    List<SpChat> selectSpChatList2(SpChat spChat);

    /**
     * 修改恢复状态
     * @param userId
     * @return
     */

    int updateSpChatByUserId(Long userId);


    /**
     * 查询当前用户最新消息
     * @param userId
     * @return
     */
    SpChat selectSpChatByIds(Long userId);

    /**
     * 上拉数据记录查询
     * @param userId
     * @param chatNumber
     * @return
     */
    SpChat selectSpChatByIdAndNumber(Long userId, Long chatNumber);


    List<SpChat> selectSpChatList3(Long userId);
}
