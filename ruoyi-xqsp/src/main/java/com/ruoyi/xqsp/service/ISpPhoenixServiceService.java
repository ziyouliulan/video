package com.ruoyi.xqsp.service;

import java.util.List;
import java.util.Set;

import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import com.ruoyi.xqsp.domain.SpPhoenixService;

/**
 * 楼凤操作Service接口
 * 
 * @author ruoyi
 * @date 2021-04-26
 */
public interface ISpPhoenixServiceService 
{
    /**
     * 查询楼凤操作
     * 
     * @param phoenixId 楼凤操作ID
     * @return 楼凤操作
     */
    public SpPhoenixService selectSpPhoenixServiceById(Long phoenixId);

    /**
     * 查询楼凤操作列表
     * 
     * @param spPhoenixService 楼凤操作
     * @return 楼凤操作集合
     */
    public List<SpPhoenixService> selectSpPhoenixServiceList(SpPhoenixService spPhoenixService);

    /**
     * 新增楼凤操作
     * 
     * @param spPhoenixService 楼凤操作
     * @return 结果
     */
    public int insertSpPhoenixService(SpPhoenixService spPhoenixService);

    /**
     * 修改楼凤操作
     * 
     * @param spPhoenixService 楼凤操作
     * @return 结果
     */
    public int updateSpPhoenixService(SpPhoenixService spPhoenixService);

    /**
     * 批量删除楼凤操作
     * 
     * @param phoenixIds 需要删除的楼凤操作ID
     * @return 结果
     */
    public int deleteSpPhoenixServiceByIds(Long[] phoenixIds);

    /**
     * 删除楼凤操作信息
     * 
     * @param phoenixId 楼凤操作ID
     * @return 结果
     */
    public int deleteSpPhoenixServiceById(Long phoenixId);

    /**
     * 楼凤审核查询
     * @param spPhoenixService
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceListAudit(SpPhoenixService spPhoenixService);

    /**
     * 前台楼凤首页
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixService();

    /**
     * 楼凤收藏人数加一
     * @param phoenixId
     * @return
     */
    int updateSpPhoenixServiceLike(Long phoenixId);

    /**
     * 楼凤收藏人数减一
     * @param phoenixId
     * @return
     */
    int updateSpPhoenixServiceLike1(Long phoenixId);

    /**
     * 根据时间排序
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceOrderTime();

    /**
     * 根据地址查询
     * @param area
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceByAddress(String area);

    /**
     * 解锁人数加一
     * @param expenseCalendarState
     * @return
     */
    int updateSpPhoenixServiceUnlockNum(Long expenseCalendarState);

    /**
     * 根据名字模糊查询
     * @param phoenixName
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceByName(String phoenixName);

    /**
     * 根据时间排序的地区查询
     * @param area
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceByAddressOrderTime(String area);

    /**
     * 查询楼凤
     * @param ids1
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceListByIds(Set<Long> ids1);
}
