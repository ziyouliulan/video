package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpComicImurl;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_comic_imurl】的数据库操作Service
* @createDate 2022-04-01 14:41:26
*/
public interface SpComicImurlService extends IService<SpComicImurl> {


    List<SpComicImurl>  orderList(Long comicChapterId,  Long comicId);

}
