package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpGlodMoneyMapper;
import com.ruoyi.xqsp.domain.SpGlodMoney;
import com.ruoyi.xqsp.service.ISpGlodMoneyService;

/**
 * 金币设置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
@Service
public class SpGlodMoneyServiceImpl implements ISpGlodMoneyService 
{
    @Autowired
    private SpGlodMoneyMapper spGlodMoneyMapper;

    /**
     * 查询金币设置
     * 
     * @param glodMoneyId 金币设置ID
     * @return 金币设置
     */
    @Override
    public SpGlodMoney selectSpGlodMoneyById(Long glodMoneyId)
    {
        return spGlodMoneyMapper.selectSpGlodMoneyById(glodMoneyId);
    }

    /**
     * 查询金币设置列表
     * 
     * @param spGlodMoney 金币设置
     * @return 金币设置
     */
    @Override
    public List<SpGlodMoney> selectSpGlodMoneyList(SpGlodMoney spGlodMoney)
    {
        return spGlodMoneyMapper.selectSpGlodMoneyList(spGlodMoney);
    }

    /**
     * 新增金币设置
     * 
     * @param spGlodMoney 金币设置
     * @return 结果
     */
    @Override
    public int insertSpGlodMoney(SpGlodMoney spGlodMoney)
    {
        return spGlodMoneyMapper.insertSpGlodMoney(spGlodMoney);
    }

    /**
     * 修改金币设置
     * 
     * @param spGlodMoney 金币设置
     * @return 结果
     */
    @Override
    public int updateSpGlodMoney(SpGlodMoney spGlodMoney)
    {
        return spGlodMoneyMapper.updateSpGlodMoney(spGlodMoney);
    }

    /**
     * 批量删除金币设置
     * 
     * @param glodMoneyIds 需要删除的金币设置ID
     * @return 结果
     */
    @Override
    public int deleteSpGlodMoneyByIds(Long[] glodMoneyIds)
    {
        return spGlodMoneyMapper.deleteSpGlodMoneyByIds(glodMoneyIds);
    }

    /**
     * 删除金币设置信息
     * 
     * @param glodMoneyId 金币设置ID
     * @return 结果
     */
    @Override
    public int deleteSpGlodMoneyById(Long glodMoneyId)
    {
        return spGlodMoneyMapper.deleteSpGlodMoneyById(glodMoneyId);
    }
}
