package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpAdvertising;

/**
 * 广告位管理Service接口
 *
 * @author ruoyi
 * @date 2021-04-17
 */
public interface ISpAdvertisingService  extends IService<SpAdvertising>
{
    /**
     * 查询广告位管理
     *
     * @param advertisingId 广告位管理ID
     * @return 广告位管理
     */
    public SpAdvertising selectSpAdvertisingById(Long advertisingId);

    /**
     * 查询广告位管理列表
     *
     * @param spAdvertising 广告位管理
     * @return 广告位管理集合
     */
    public List<SpAdvertising> selectSpAdvertisingList(SpAdvertising spAdvertising);

    /**
     * 新增广告位管理
     *
     * @param spAdvertising 广告位管理
     * @return 结果
     */
    public int insertSpAdvertising(SpAdvertising spAdvertising);

    /**
     * 修改广告位管理
     *
     * @param spAdvertising 广告位管理
     * @return 结果
     */
    public int updateSpAdvertising(SpAdvertising spAdvertising);

    /**
     * 批量删除广告位管理
     *
     * @param advertisingIds 需要删除的广告位管理ID
     * @return 结果
     */
    public int deleteSpAdvertisingByIds(Long[] advertisingIds);

    /**
     * 删除广告位管理信息
     *
     * @param advertisingId 广告位管理ID
     * @return 结果
     */
    public int deleteSpAdvertisingById(Long advertisingId);

    /**
     * 根据分类查询
     * @param categoryId
     * @return
     */
    List<SpAdvertising> selectSpAdvertisingByCategoryId(Long categoryId);

    /**
     * 前台精选查询
     * @return
     */
    List<SpAdvertising> selectSpAdvertising();

    int updateSpAdvertisingCount(Long advertisingId);

    /**
     * 楼凤的banner图
     * @return
     */
    List<SpAdvertising> selectSpAdvertisingPhoenix();

    /**
     * 漫画的banner图
     * @return
     */
    List<SpAdvertising> selectSpAdvertisingByComic();

    /**
     * 动漫页banner图
     * @return
     */
    List<SpAdvertising> selectSpAdvertisingAnime();
}
