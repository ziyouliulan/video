package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVideoCacheMapper;
import com.ruoyi.xqsp.domain.SpVideoCache;
import com.ruoyi.xqsp.service.ISpVideoCacheService;

/**
 * 视频缓存Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
@Service
public class SpVideoCacheServiceImpl implements ISpVideoCacheService 
{
    @Autowired
    private SpVideoCacheMapper spVideoCacheMapper;

    /**
     * 查询视频缓存
     * 
     * @param videoCacheId 视频缓存ID
     * @return 视频缓存
     */
    @Override
    public SpVideoCache selectSpVideoCacheById(Long videoCacheId)
    {
        return spVideoCacheMapper.selectSpVideoCacheById(videoCacheId);
    }

    /**
     * 查询视频缓存列表
     * 
     * @param spVideoCache 视频缓存
     * @return 视频缓存
     */
    @Override
    public List<SpVideoCache> selectSpVideoCacheList(SpVideoCache spVideoCache)
    {
        return spVideoCacheMapper.selectSpVideoCacheList(spVideoCache);
    }

    /**
     * 新增视频缓存
     * 
     * @param spVideoCache 视频缓存
     * @return 结果
     */
    @Override
    public int insertSpVideoCache(SpVideoCache spVideoCache)
    {
        return spVideoCacheMapper.insertSpVideoCache(spVideoCache);
    }

    /**
     * 修改视频缓存
     * 
     * @param spVideoCache 视频缓存
     * @return 结果
     */
    @Override
    public int updateSpVideoCache(SpVideoCache spVideoCache)
    {
        return spVideoCacheMapper.updateSpVideoCache(spVideoCache);
    }

    /**
     * 批量删除视频缓存
     * 
     * @param videoCacheIds 需要删除的视频缓存ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoCacheByIds(Long[] videoCacheIds)
    {
        return spVideoCacheMapper.deleteSpVideoCacheByIds(videoCacheIds);
    }

    /**
     * 删除视频缓存信息
     * 
     * @param videoCacheId 视频缓存ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoCacheById(Long videoCacheId)
    {
        return spVideoCacheMapper.deleteSpVideoCacheById(videoCacheId);
    }

    @Override
    public List<SpVideoCache> selectSpVideoCacheByUserId(Long userId) {
        return spVideoCacheMapper.selectSpVideoCacheByUserId(userId);
    }

    @Override
    public int updateSpVideoCacheUrl(Long videoCacheId, String videoCacheUrl) {
        return spVideoCacheMapper.updateSpVideoCacheUrl(videoCacheId,videoCacheUrl);
    }

    @Override
    public int deleteSpVideoCacheBy(Long videoId, Long type1) {
        return spVideoCacheMapper.deleteSpVideoCacheBy(videoId,type1);
    }
}
