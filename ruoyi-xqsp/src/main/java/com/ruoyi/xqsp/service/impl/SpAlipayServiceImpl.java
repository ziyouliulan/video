package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpAlipayMapper;
import com.ruoyi.xqsp.domain.SpAlipay;
import com.ruoyi.xqsp.service.ISpAlipayService;

/**
 * 支付宝支付Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@Service
public class SpAlipayServiceImpl implements ISpAlipayService 
{
    @Autowired
    private SpAlipayMapper spAlipayMapper;

    /**
     * 查询支付宝支付
     * 
     * @param alipayId 支付宝支付ID
     * @return 支付宝支付
     */
    @Override
    public SpAlipay selectSpAlipayById(Long alipayId)
    {
        return spAlipayMapper.selectSpAlipayById(alipayId);
    }

    /**
     * 查询支付宝支付列表
     * 
     * @param spAlipay 支付宝支付
     * @return 支付宝支付
     */
    @Override
    public List<SpAlipay> selectSpAlipayList(SpAlipay spAlipay)
    {
        return spAlipayMapper.selectSpAlipayList(spAlipay);
    }

    /**
     * 新增支付宝支付
     * 
     * @param spAlipay 支付宝支付
     * @return 结果
     */
    @Override
    public int insertSpAlipay(SpAlipay spAlipay)
    {
        return spAlipayMapper.insertSpAlipay(spAlipay);
    }

    /**
     * 修改支付宝支付
     * 
     * @param spAlipay 支付宝支付
     * @return 结果
     */
    @Override
    public int updateSpAlipay(SpAlipay spAlipay)
    {
        return spAlipayMapper.updateSpAlipay(spAlipay);
    }

    /**
     * 批量删除支付宝支付
     * 
     * @param alipayIds 需要删除的支付宝支付ID
     * @return 结果
     */
    @Override
    public int deleteSpAlipayByIds(Long[] alipayIds)
    {
        return spAlipayMapper.deleteSpAlipayByIds(alipayIds);
    }

    /**
     * 删除支付宝支付信息
     * 
     * @param alipayId 支付宝支付ID
     * @return 结果
     */
    @Override
    public int deleteSpAlipayById(Long alipayId)
    {
        return spAlipayMapper.deleteSpAlipayById(alipayId);
    }
}
