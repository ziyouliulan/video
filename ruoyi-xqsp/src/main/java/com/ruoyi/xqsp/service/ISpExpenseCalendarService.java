package com.ruoyi.xqsp.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.xqsp.domain.SpExpenseCalendar;

/**
 * 消费记录Service接口
 *
 * @author ruoyi
 * @date 2021-05-08
 */
public interface ISpExpenseCalendarService  extends IService<SpExpenseCalendar>
{
    /**
     * 查询消费记录
     *
     * @param expenseCalendarId 消费记录ID
     * @return 消费记录
     */
    public SpExpenseCalendar selectSpExpenseCalendarById(Long expenseCalendarId);

    /**
     * 查询消费记录列表
     *
     * @param spExpenseCalendar 消费记录
     * @return 消费记录集合
     */
    public List<SpExpenseCalendar> selectSpExpenseCalendarList(SpExpenseCalendar spExpenseCalendar);

    /**
     * 新增消费记录
     *
     * @param spExpenseCalendar 消费记录
     * @return 结果
     */
    public int insertSpExpenseCalendar(SpExpenseCalendar spExpenseCalendar);

    /**
     * 修改消费记录
     *
     * @param spExpenseCalendar 消费记录
     * @return 结果
     */
    public int updateSpExpenseCalendar(SpExpenseCalendar spExpenseCalendar);

    /**
     * 批量删除消费记录
     *
     * @param expenseCalendarIds 需要删除的消费记录ID
     * @return 结果
     */
    public int deleteSpExpenseCalendarByIds(Long[] expenseCalendarIds);

    /**
     * 删除消费记录信息
     *
     * @param expenseCalendarId 消费记录ID
     * @return 结果
     */
    public int deleteSpExpenseCalendarById(Long expenseCalendarId);

    SpExpenseCalendar selectSpExpenseCalendarByExpenseCalendarOrder(String expenseCalendarOrder);

    /**
     * 根据用户id查询消费记录
     * @param userId
     * @return
     */

    List<SpExpenseCalendar> selectSpExpenseCalendarByUserId(Long userId);

    Integer selectSpExpenseCalendarByCount(Long videoId, int i);
}
