package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.SpComicWatchHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_comic_watch_history(视频观看记录)】的数据库操作Service
* @createDate 2022-07-23 17:36:46
*/
public interface SpComicWatchHistoryService extends IService<SpComicWatchHistory> {

}
