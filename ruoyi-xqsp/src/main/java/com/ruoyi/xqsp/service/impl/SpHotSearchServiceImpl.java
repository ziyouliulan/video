package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.SpHotSearch;
import com.ruoyi.xqsp.service.SpHotSearchService;
import com.ruoyi.xqsp.mapper.SpHotSearchMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_hot_search】的数据库操作Service实现
* @createDate 2022-06-18 16:21:38
*/
@Service
public class SpHotSearchServiceImpl extends ServiceImpl<SpHotSearchMapper, SpHotSearch>
    implements SpHotSearchService{

}




