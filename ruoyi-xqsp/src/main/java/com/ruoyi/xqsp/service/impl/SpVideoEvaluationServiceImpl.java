package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpVideoEvaluationMapper;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;
import com.ruoyi.xqsp.service.ISpVideoEvaluationService;

/**
 * 视频评价Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-24
 */
@Service
public class SpVideoEvaluationServiceImpl implements ISpVideoEvaluationService 
{
    @Autowired
    private SpVideoEvaluationMapper spVideoEvaluationMapper;

    /**
     * 查询视频评价
     * 
     * @param videoEvaluationId 视频评价ID
     * @return 视频评价
     */
    @Override
    public SpVideoEvaluation selectSpVideoEvaluationById(Long videoEvaluationId)
    {
        return spVideoEvaluationMapper.selectSpVideoEvaluationById(videoEvaluationId);
    }

    /**
     * 查询视频评价列表
     * 
     * @param spVideoEvaluation 视频评价
     * @return 视频评价
     */
    @Override
    public List<SpVideoEvaluation> selectSpVideoEvaluationList(SpVideoEvaluation spVideoEvaluation)
    {
        return spVideoEvaluationMapper.selectSpVideoEvaluationList(spVideoEvaluation);
    }

    /**
     * 新增视频评价
     * 
     * @param spVideoEvaluation 视频评价
     * @return 结果
     */
    @Override
    public int insertSpVideoEvaluation(SpVideoEvaluation spVideoEvaluation)
    {
        return spVideoEvaluationMapper.insertSpVideoEvaluation(spVideoEvaluation);
    }

    /**
     * 修改视频评价
     * 
     * @param spVideoEvaluation 视频评价
     * @return 结果
     */
    @Override
    public int updateSpVideoEvaluation(SpVideoEvaluation spVideoEvaluation)
    {
        return spVideoEvaluationMapper.updateSpVideoEvaluation(spVideoEvaluation);
    }

    /**
     * 批量删除视频评价
     * 
     * @param videoEvaluationIds 需要删除的视频评价ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoEvaluationByIds(Long[] videoEvaluationIds)
    {
        return spVideoEvaluationMapper.deleteSpVideoEvaluationByIds(videoEvaluationIds);
    }

    /**
     * 删除视频评价信息
     * 
     * @param videoEvaluationId 视频评价ID
     * @return 结果
     */
    @Override
    public int deleteSpVideoEvaluationById(Long videoEvaluationId)
    {
        return spVideoEvaluationMapper.deleteSpVideoEvaluationById(videoEvaluationId);
    }

    @Override
    public List<SpVideoEvaluation> selectSpVideoEvaluationListByVideoId(Long videoId) {
        return spVideoEvaluationMapper.selectSpVideoEvaluationListByVideoId(videoId);
    }

    @Override
    public int updateSpVideoEvaluationLike(Long videoEvaluationId) {
        return spVideoEvaluationMapper.updateSpVideoEvaluationLike(videoEvaluationId);
    }

    @Override
    public int updateSpVideoEvaluationLike1(Long videoEvaluationId) {
        return spVideoEvaluationMapper.updateSpVideoEvaluationLike1(videoEvaluationId);
    }

    @Override
    public Long selectCountById(Long videoId) {
        return spVideoEvaluationMapper.selectCountById(videoId);
    }

    @Override
    public int deleteSpVideoEvaluationByVideoId(Long videoId) {
        return spVideoEvaluationMapper.deleteSpVideoEvaluationByVideoId(videoId);
    }
}
