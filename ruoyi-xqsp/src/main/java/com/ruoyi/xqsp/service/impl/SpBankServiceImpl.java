package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpBankMapper;
import com.ruoyi.xqsp.domain.SpBank;
import com.ruoyi.xqsp.service.ISpBankService;

/**
 * 银行卡操作Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-07
 */
@Service
public class SpBankServiceImpl implements ISpBankService 
{
    @Autowired
    private SpBankMapper spBankMapper;

    /**
     * 查询银行卡操作
     * 
     * @param spBankId 银行卡操作ID
     * @return 银行卡操作
     */
    @Override
    public SpBank selectSpBankById(Long spBankId)
    {
        return spBankMapper.selectSpBankById(spBankId);
    }

    /**
     * 查询银行卡操作列表
     * 
     * @param spBank 银行卡操作
     * @return 银行卡操作
     */
    @Override
    public List<SpBank> selectSpBankList(SpBank spBank)
    {
        return spBankMapper.selectSpBankList(spBank);
    }

    /**
     * 新增银行卡操作
     * 
     * @param spBank 银行卡操作
     * @return 结果
     */
    @Override
    public int insertSpBank(SpBank spBank)
    {
        return spBankMapper.insertSpBank(spBank);
    }

    /**
     * 修改银行卡操作
     * 
     * @param spBank 银行卡操作
     * @return 结果
     */
    @Override
    public int updateSpBank(SpBank spBank)
    {
        return spBankMapper.updateSpBank(spBank);
    }

    /**
     * 批量删除银行卡操作
     * 
     * @param spBankIds 需要删除的银行卡操作ID
     * @return 结果
     */
    @Override
    public int deleteSpBankByIds(Long[] spBankIds)
    {
        return spBankMapper.deleteSpBankByIds(spBankIds);
    }

    /**
     * 删除银行卡操作信息
     * 
     * @param spBankId 银行卡操作ID
     * @return 结果
     */
    @Override
    public int deleteSpBankById(Long spBankId)
    {
        return spBankMapper.deleteSpBankById(spBankId);
    }
}
