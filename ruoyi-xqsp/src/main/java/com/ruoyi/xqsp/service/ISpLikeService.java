package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpLike;

/**
 * 用户点赞Service接口
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
public interface ISpLikeService 
{
    /**
     * 查询用户点赞
     * 
     * @param likeId 用户点赞ID
     * @return 用户点赞
     */
    public SpLike selectSpLikeById(Long likeId);

    /**
     * 查询用户点赞列表
     * 
     * @param spLike 用户点赞
     * @return 用户点赞集合
     */
    public List<SpLike> selectSpLikeList(SpLike spLike);

    /**
     * 新增用户点赞
     * 
     * @param spLike 用户点赞
     * @return 结果
     */
    public int insertSpLike(SpLike spLike);

    /**
     * 修改用户点赞
     * 
     * @param spLike 用户点赞
     * @return 结果
     */
    public int updateSpLike(SpLike spLike);

    /**
     * 批量删除用户点赞
     * 
     * @param likeIds 需要删除的用户点赞ID
     * @return 结果
     */
    public int deleteSpLikeByIds(Long[] likeIds);

    /**
     * 删除用户点赞信息
     * 
     * @param likeId 用户点赞ID
     * @return 结果
     */
    public int deleteSpLikeById(Long likeId);

    /**
     * 根据相关id查询是否已点赞
     * @param spLike
     * @return
     */
    public SpLike selectSpLike(SpLike spLike);

    int deleteSpLike(SpLike spLike);

    /**
     * 查询该用户是否点赞该视频
     * @param userId
     * @param videoId
     * @return
     */
    SpLike selectSpLikeByIds(Long userId, Long videoId);

    /**
     * 查询该用户是否点赞该综艺视频
     * @param userId
     * @param varietyVideoId
     * @return
     */
    SpLike selectSpLikeByVarietyVideoId(Long userId, Long varietyVideoId);
}
