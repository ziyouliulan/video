package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.UrlFilter;
import com.ruoyi.xqsp.service.UrlFilterService;
import com.ruoyi.xqsp.mapper.UrlFilterMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【sp_url_filter】的数据库操作Service实现
* @createDate 2022-08-17 03:29:14
*/
@Service
public class UrlFilterServiceImpl extends ServiceImpl<UrlFilterMapper, UrlFilter>
    implements UrlFilterService{

}




