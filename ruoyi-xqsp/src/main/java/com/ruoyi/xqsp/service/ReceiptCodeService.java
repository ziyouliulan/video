package com.ruoyi.xqsp.service;

import com.ruoyi.xqsp.domain.ReceiptCode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【sp_receipt_code】的数据库操作Service
* @createDate 2022-08-05 19:55:54
*/
public interface ReceiptCodeService extends IService<ReceiptCode> {

}
