package com.ruoyi.xqsp.service;

import java.util.List;
import com.ruoyi.xqsp.domain.SpHeadImg;

/**
 * 用户头像Service接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface ISpHeadImgService 
{
    /**
     * 查询用户头像
     * 
     * @param headImgId 用户头像ID
     * @return 用户头像
     */
    public SpHeadImg selectSpHeadImgById(Long headImgId);

    /**
     * 查询用户头像列表
     * 
     * @param spHeadImg 用户头像
     * @return 用户头像集合
     */
    public List<SpHeadImg> selectSpHeadImgList(SpHeadImg spHeadImg);

    /**
     * 新增用户头像
     * 
     * @param spHeadImg 用户头像
     * @return 结果
     */
    public int insertSpHeadImg(SpHeadImg spHeadImg);

    /**
     * 修改用户头像
     * 
     * @param spHeadImg 用户头像
     * @return 结果
     */
    public int updateSpHeadImg(SpHeadImg spHeadImg);

    /**
     * 批量删除用户头像
     * 
     * @param headImgIds 需要删除的用户头像ID
     * @return 结果
     */
    public int deleteSpHeadImgByIds(Long[] headImgIds);

    /**
     * 删除用户头像信息
     * 
     * @param headImgId 用户头像ID
     * @return 结果
     */
    public int deleteSpHeadImgById(Long headImgId);

    SpHeadImg selectSpHeadImg();
}
