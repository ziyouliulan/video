package com.ruoyi.xqsp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.xqsp.mapper.SpChatMapper;
import com.ruoyi.xqsp.domain.SpChat;
import com.ruoyi.xqsp.service.ISpChatService;

/**
 * 聊天信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
@Service
public class SpChatServiceImpl implements ISpChatService 
{
    @Autowired
    private SpChatMapper spChatMapper;

    /**
     * 查询聊天信息
     * 
     * @param chatId 聊天信息ID
     * @return 聊天信息
     */
    @Override
    public SpChat selectSpChatById(Long chatId)
    {
        return spChatMapper.selectSpChatById(chatId);
    }

    /**
     * 查询聊天信息列表
     * 
     * @param spChat 聊天信息
     * @return 聊天信息
     */
    @Override
    public List<SpChat> selectSpChatList(SpChat spChat)
    {
        return spChatMapper.selectSpChatList(spChat);
    }

    /**
     * 新增聊天信息
     * 
     * @param spChat 聊天信息
     * @return 结果
     */
    @Override
    public int insertSpChat(SpChat spChat)
    {
        return spChatMapper.insertSpChat(spChat);
    }

    /**
     * 修改聊天信息
     * 
     * @param spChat 聊天信息
     * @return 结果
     */
    @Override
    public int updateSpChat(SpChat spChat)
    {
        return spChatMapper.updateSpChat(spChat);
    }

    /**
     * 批量删除聊天信息
     * 
     * @param chatIds 需要删除的聊天信息ID
     * @return 结果
     */
    @Override
    public int deleteSpChatByIds(Long[] chatIds)
    {
        return spChatMapper.deleteSpChatByIds(chatIds);
    }

    /**
     * 删除聊天信息信息
     * 
     * @param chatId 聊天信息ID
     * @return 结果
     */
    @Override
    public int deleteSpChatById(Long chatId)
    {
        return spChatMapper.deleteSpChatById(chatId);
    }

    @Override
    public List<SpChat> selectSpChatLists(Long userId, Long serviceId) {
        return spChatMapper.selectSpChatLists(userId,serviceId);
    }

    @Override
    public List<SpChat> selectSpChatList1(SpChat spChat) {
        return spChatMapper.selectSpChatList1(spChat);
    }

    @Override
    public List<SpChat> selectSpChatList2(SpChat spChat) {
        return spChatMapper.selectSpChatList2(spChat);
    }

    @Override
    public int updateSpChatByUserId(Long userId) {
        return spChatMapper.updateSpChatByUserId(userId);
    }

    @Override
    public SpChat selectSpChatByIds(Long userId) {
        return spChatMapper.selectSpChatByIds(userId);
    }

    @Override
    public SpChat selectSpChatByIdAndNumber(Long userId, Long chatNumber) {
        return spChatMapper.selectSpChatByIdAndNumber(userId,chatNumber);
    }

    @Override
    public List<SpChat> selectSpChatList3(Long userId) {
        return spChatMapper.selectSpChatList3(userId);
    }


}
