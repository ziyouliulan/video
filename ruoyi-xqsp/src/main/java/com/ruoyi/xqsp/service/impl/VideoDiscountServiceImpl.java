package com.ruoyi.xqsp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.xqsp.domain.VideoDiscount;
import com.ruoyi.xqsp.service.VideoDiscountService;
import com.ruoyi.xqsp.mapper.VideoDiscountMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【video_discount】的数据库操作Service实现
* @createDate 2022-06-12 19:54:12
*/
@Service
public class VideoDiscountServiceImpl extends ServiceImpl<VideoDiscountMapper, VideoDiscount>
    implements VideoDiscountService{

}




