package com.ruoyi.xqsp.config;


import com.ruoyi.xqsp.domain.SpAlipay;
import com.ruoyi.xqsp.service.ISpAlipayService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author houyu
 * @createTime 2019/3/19 23:39
 */
public class AlipayConfig {

    @Autowired
    private ISpAlipayService spAlipayService;
    private static SpAlipay spAlipay;

    {
        spAlipay = spAlipayService.selectSpAlipayById(1l);
    }


    // 商户appid
    //public static String APPID = "2021002140617891";
    public static String APPID = spAlipay.getAppid();
    // 应用私钥 pkcs8格式的
    public static String RSA_PRIVATE_KEY = spAlipay.getRsaPrivateKey();
    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://crm.quanshikeji.com:8080/dashi/alipay/notify";
    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    public static String return_url = "http://crm.quanshikeji.com:8080/dashi/alipay/notify";
    // 请求网关地址,沙箱是:https://openapi.alipaydev.com/gateway.do
    public static String URL = "https://openapi.alipay.com/gateway.do";
    // 编码
    public static String CHARSET = "UTF-8";
    // 返回格式
    public static String FORMAT = "json";
    //应用公钥
    public static String ALIPAY_PUBLIC_KEY = spAlipay.getAlipayPublicKey();
    // RSA2
    public static String SIGNTYPE = "RSA2";

    // 支付宝公钥(在应用中可以获取)
    public static String ZFB_PUBLIC_KEY =spAlipay.getZfbPublicKey();
}

