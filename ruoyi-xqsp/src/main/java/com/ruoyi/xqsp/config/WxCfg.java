package com.ruoyi.xqsp.config;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * 微信参数配置中心
 */

@Data
@Component
public class WxCfg {

    private String appType;//App支付交易类型
    private String appNotifyUrl;//App回调地址
    private String ip;//服务器ip
}
