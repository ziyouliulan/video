package com.ruoyi.xqsp.utils;

import java.util.List;
import java.util.Map;

public interface IRedisClient {

	void set(String key, String value);

	Boolean setnx(String key,String value);

	void setTimeout(String key, String value, int seconds);

	void append(String key, String value);

	void setExpired(String key, int seconds);

	List<String> mget(String... keys);

	void del(String... keys);

	String get(String key);

	String getKey(String pattern);

	String[] getKeys(String pattern);

	void delKeys(String pattern);

	Long incr(String key);

	void increaseBy(String key, String value);

	void decr(String key);

	void decrBy(String key, String value);

	// hash
	Map<String, String> hGetAll(String key);

	String hGet(String key, String field);

	// 获取指定的值
	List<String> hMGet(String key, String... fields);

	void hMSet(String key, Map<String, String> map);

	// 为key中的域 field 的值加上增量 increment
	void hIncrease(String key, String field, String value);

	// 为key中的域 field 的值加上增量 increment
	long hIncrBy(String key, String field, String value);

	// 删除指定的值
	void hDel(String key, String... fields);

	boolean hexists(String key, String field);

	// set
	boolean exists(String key);

	// 添加
	void sSet(String key, String member);

	void sSetWithTimeout(String key, String member, int second);

	// 删除
	void sDel(String key, String member);

	// // 判断value是否在列表中
	Boolean sIsMember(String key, String member);

	// 获取所有加入的value
	String[] sMembers(String key);

	// 返回集合的元素个数
	long scard(String key);

	// list
	// 添加数据
	void rpush(String key, String value);

	// 列表出栈
	String lpop(String key);

	// 数组长度
	long llen(String key);

	// 整个列表值
	List<String> lrange(String key, String start, String end);

	// 升序
	List<String> sort(String key);

	List<String> sortDesc(String key);

	// 删除区间以外的数据
	String ltrim(String key, String start, String end);

	// 删除列表指定下标的值
	long lrem(String key, String count, String value);

}