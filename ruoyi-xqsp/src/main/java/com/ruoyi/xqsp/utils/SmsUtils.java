package com.ruoyi.xqsp.utils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.ruoyi.xqsp.domain.vo.Sms;

import javax.swing.text.html.FormSubmitEvent;

public class SmsUtils {
    /**
     * 发送短信
     * @param signName
     * @param phoneNumbers
     * @param templateCode
     *     signName1: quanshikeji
     *     templateCode1: SMS_140060111
     *
     *     signName2: quanshikeji
     *     templateCode2: SMS_140060111
     *
     *     signName3: quanshikeji
     *     templateCode3: SMS_140060111
     */
    public static void sendSms(String signName, String phoneNumbers, String templateCode, String templateParam)  {
        DefaultProfile profile = DefaultProfile.getProfile(Sms.regionId, Sms.accessKeyId, Sms.accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", Sms.regionId);
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", templateParam);
        CommonResponse response = null;
        try {
            response = client.getCommonResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        String data = response.getData();
        JSONObject dataJson = JSONUtil.parseObj(data);
        if(!"OK".equals(dataJson.get("Code"))){

            throw new RuntimeException("发送短信失败：" + dataJson.get("Message"));
        }
    }
}
