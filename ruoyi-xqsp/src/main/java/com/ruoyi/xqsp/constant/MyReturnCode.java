package com.ruoyi.xqsp.constant;

public enum MyReturnCode {

    ERR_70000(70000, "系统错误，请稍候再试"){},//其它错误
    ERR_70001(70001, "验证码发送过频繁，请稍后再试"){},
    ERR_70002(70002, "验证码不合法"){},
    ERR_70003(70003, "验证码过期"){},
    ERR_70004(70004, "验证码有误"){},
    ERR_70005(70005, "账号不存在"){},
    ;

    MyReturnCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MyReturnCode{" + "code='" + code + '\'' + "msg='" + msg + '\'' + '}';
    }

}
