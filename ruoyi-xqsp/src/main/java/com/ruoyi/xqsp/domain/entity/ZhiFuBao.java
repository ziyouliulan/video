package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

@Data
public class ZhiFuBao {
    private Long userId;
    private String pay_productname;//商品名称
    private int channelid;//类型1 wx/2 zfb
    /**
     * rechargeRecordType充值类型（1.vip月卡，2.VIP季卡，3.VIP年卡，7.金币充值 8.半年卡）
     * type  1会员充值，2金币充值
     * moneyType 套餐类型
     */
    private int rechargeRecordType;
    private int type;
    private int moneyType;

}
