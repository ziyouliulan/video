package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 我的广告对象 sp_mine_img
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public class SpMineImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long mineImgId;

    /** 地址 */
    @Excel(name = "地址")
    private String mineImgUrl;

    /** 链接 */
    @Excel(name = "链接")
    private String mineImgLink;

    public void setMineImgId(Long mineImgId) 
    {
        this.mineImgId = mineImgId;
    }

    public Long getMineImgId() 
    {
        return mineImgId;
    }
    public void setMineImgUrl(String mineImgUrl) 
    {
        this.mineImgUrl = mineImgUrl;
    }

    public String getMineImgUrl() 
    {
        return mineImgUrl;
    }
    public void setMineImgLink(String mineImgLink) 
    {
        this.mineImgLink = mineImgLink;
    }

    public String getMineImgLink() 
    {
        return mineImgLink;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("mineImgId", getMineImgId())
            .append("mineImgUrl", getMineImgUrl())
            .append("mineImgLink", getMineImgLink())
            .toString();
    }
}
