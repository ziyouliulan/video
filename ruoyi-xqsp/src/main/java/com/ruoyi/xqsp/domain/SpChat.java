package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 聊天信息对象 sp_chat
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
public class SpChat extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long chatId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 客服id */
    @Excel(name = "客服id")
    private Long serviceId;

    /** 客服回复内容 */
    @Excel(name = "客服回复内容")
    private String chatContent;

    /** 聊天时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "聊天时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date chatTime;

    /** 聊天类型（0 文字，1 图片） */
    @Excel(name = "聊天类型", readConverterExp = "0=,文=字，1,图=片")
    private Integer chatType;

    /** 查看状态（0未读 1已读） */
    @Excel(name = "查看状态", readConverterExp = "0=未读,1=已读")
    private Integer chatState;

    /** 聊天条数 */
    @Excel(name = "聊天条数")
    private Long chatNumber;

    public void setChatId(Long chatId)
    {
        this.chatId = chatId;
    }

    public Long getChatId()
    {
        return chatId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setServiceId(Long serviceId)
    {
        this.serviceId = serviceId;
    }

    public Long getServiceId()
    {
        return serviceId;
    }
    public void setChatContent(String chatContent)
    {
        this.chatContent = chatContent;
    }

    public String getChatContent()
    {
        return chatContent;
    }
    public void setChatTime(Date chatTime)
    {
        this.chatTime = chatTime;
    }

    public Date getChatTime()
    {
        return chatTime;
    }
    public void setChatType(Integer chatType)
    {
        this.chatType = chatType;
    }

    public Integer getChatType()
    {
        return chatType;
    }
    public void setChatState(Integer chatState)
    {
        this.chatState = chatState;
    }

    public Integer getChatState()
    {
        return chatState;
    }
    public void setChatNumber(Long chatNumber)
    {
        this.chatNumber = chatNumber;
    }

    public Long getChatNumber()
    {
        return chatNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("chatId", getChatId())
                .append("userId", getUserId())
                .append("serviceId", getServiceId())
                .append("chatContent", getChatContent())
                .append("chatTime", getChatTime())
                .append("chatType", getChatType())
                .append("chatState", getChatState())
                .append("chatNumber", getChatNumber())
                .toString();
    }
    private Integer pageNums;
    private Integer pageSize;

    public Integer getPageNums() {
        return pageNums;
    }

    public void setPageNums(Integer pageNums) {
        this.pageNums = pageNums;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
