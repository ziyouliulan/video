package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

@Data
public class DeleteCollects {
    private Long[] collectIds;//收藏删除
    private Long[] videoWatchHistoryIds;//观看历史删除
    private String type;//观看历史删除
    private Long[] videoCacheIds;//缓存删除

    private Integer userId;

}
