package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVideo;
import lombok.Data;

import java.util.List;

/**
 * 综艺视频详情
 */
@Data
public class HomeVarietyVideoAll {
    private Long userId;// 用户id
    private Long varietyVideoId;  //综艺视频id
    private SpVarietyVideo spVarietyVideo;// 视频信息
    private List<SpVarietyVideo> spVarietyVideoList; //猜你喜欢（同一分类下的视频随意拿六个）

    private SpUsers spUsers;//用户信息
    private Long spLike;//点赞情况（已点，未点）
    private Long spUnlike;//差评情况
    private Long spCollect; //用户收藏
    private Long spUnlock;//金币解锁
    private Long videoEvaluationCounts;// 视频评论的数量

}
