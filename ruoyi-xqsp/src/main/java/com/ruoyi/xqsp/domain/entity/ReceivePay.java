package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 支付接受第三方传回来的值
 */
@Data
public class ReceivePay {
    String memberid;//商户编号
    String orderid;//订单号
    String amount;//订单金额
    String transaction_id;//交易流水号
    String datetime;//交易时间
    String returncode;//交易状态
    String attach;//扩展返回
    String sign;//签名
}
