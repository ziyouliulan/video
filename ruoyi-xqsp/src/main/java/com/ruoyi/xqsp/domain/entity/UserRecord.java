package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpRechargeRecord;
import lombok.Data;

/**
 * 充值记录表
 */
@Data
public class UserRecord {
    private SpRechargeRecord spRechargeRecord;
    private String userName;
}
