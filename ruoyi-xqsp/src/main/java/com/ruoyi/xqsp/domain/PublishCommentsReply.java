//
//package com.ruoyi.xqsp.domain;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * @Description:TODO(动态评论回复表实体类)
// *
// * @version: V1.0
// * @author: liuhaotian
// *
// */
//@Data
//@ApiModel("TPublishCommentsReply")
//@TableName(value = "t_publish_comments_reply")
//@AllArgsConstructor
//@NoArgsConstructor
//public class PublishCommentsReply implements Serializable {
//
//	private static final long serialVersionUID = 1599714656802L;
//
//    @TableId(value = "id", type = IdType.AUTO)
//    @ApiModelProperty(name = "id", value = "", hidden = true)
//	private Long id;
//
//    @ApiModelProperty(name = "commentsId", value = "评论id或者是回复id")
//	private Long commentsId;
//
//    @ApiModelProperty(name = "uid", value = "用户ID")
//	private Long uid;
//
//    @ApiModelProperty(name = "touid", value = "目标用户id 给谁回复")
//	private Long touid;
//
//	@ApiModelProperty(name = "isCommentUser", value = "是否是该动态评论层主  0：是 1：不是")
//	private Integer isCommentUser;
//
//    @ApiModelProperty(name = "uname", value = "用户名")
//	private String uname;
//
//    @ApiModelProperty(name = "touname", value = "目标用户名")
//	private String touname;
//
//    @ApiModelProperty(name = "replyComment", value = "回复内容")
//	private String replyComment;
//
//    @ApiModelProperty(name = "replyCommentPic", value = "回复图片")
//	private String replyCommentPic;
//
//    @ApiModelProperty(name = "replyCommentPicName", value = "回复图片名字")
//	private String replyCommentPicName;
//
//	@DateTimeFormat(pattern = "MM-dd HH:mm")
//	@JsonFormat(pattern="MM-dd HH:mm",timezone = "GMT+8")
//    @ApiModelProperty(name = "createDate", value = "回复时间")
//	private Date createDate;
//
//
//}
