package com.ruoyi.xqsp.domain;

import lombok.Data;

@Data
public class SpGetComicChapterInfo {
    private Long userId;
    private Long comicId;
    private Integer pageNum;//分页页数
    private Integer pageSize;//分页条数
}
