package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpExpenseCalendar;
import lombok.Data;

/**
 * 用户消费表
 */
@Data
public class UserExpenseCalendar {

    private SpExpenseCalendar spExpenseCalendar;
    private String userName;
}
