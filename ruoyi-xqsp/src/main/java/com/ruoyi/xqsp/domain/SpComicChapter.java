package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.poi.ss.formula.functions.Count;

/**
 * 漫画章节对象 sp_comic_chapter
 *
 * @author ruoyi
 * @date 2021-05-06
 */
@Data
public class SpComicChapter extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 章节编号
     */
    @TableId(type = IdType.AUTO)
    private Long comicChapterId;

    /**
     * 漫画id
     */
    @Excel(name = " 漫画id")
    private Long comicId;

    /**
     * 章节
     */
    @Excel(name = "章节")
    private Long comicChapterDirectory;

    /**
     * 章节名
     */
    @Excel(name = "章节名")
    private String comicChapterName;



    private Integer comicIsFree;
    /**
     * 章节内容
     */
    @Excel(name = "章节内容")
    private String comicChapterImg;



    private Integer serialNumber;
    /**
     * 是否解锁
     */
    @TableField(exist = false)
    private boolean bUnlock;

    public boolean getBUnlock() {
        return bUnlock;
    }

    public void setBUnlock(Boolean bUnlock) {
        this.bUnlock = bUnlock;
    }

    public void setComicChapterId(Long comicChapterId) {
        this.comicChapterId = comicChapterId;
    }

    public Long getComicChapterId() {
        return comicChapterId;
    }

    public void setComicId(Long comicId) {
        this.comicId = comicId;
    }

    public Long getComicId() {
        return comicId;
    }

    public void setComicChapterDirectory(Long comicChapterDirectory) {
        this.comicChapterDirectory = comicChapterDirectory;
    }

    public Long getComicChapterDirectory() {
        return comicChapterDirectory;
    }

    public void setComicChapterName(String comicChapterName) {
        this.comicChapterName = comicChapterName;
    }

    public String getComicChapterName() {
        return comicChapterName;
    }

    public void setComicChapterImg(String comicChapterImg) {
        this.comicChapterImg = comicChapterImg;
    }

    public String getComicChapterImg() {
        return comicChapterImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("comicChapterId", getComicChapterId())
                .append("comicId", getComicId())
                .append("comicChapterDirectory", getComicChapterDirectory())
                .append("comicChapterName", getComicChapterName())
                .append("comicChapterImg", getComicChapterImg())
                .toString();
    }

    @TableField(exist = false)
    private Long Count;

    public Long getCount() {
        return Count;
    }

    public void setCount(Long count) {
        Count = count;
    }
}
