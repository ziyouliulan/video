package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.domain.SpVideo;
import lombok.Data;

import java.util.List;

/**
 * 演员和视频
 */
@Data
public class VideoActor {

    private SpActor spActor;
    private List<SpVideo>spVideoList;
    private Integer counts;
}
