package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 点赞情况
 */
@Data
public class UserLike {

    private Long userId;// 用户id
    private Long videoEvaluationId;//视频评论id
    private Long videoId;  //视频id
    private Long varietyVideoId;//综艺视频id
    private Long actorId;//演员id
    private  Long varietyVideoEvaluationId;//综艺品论id
    private Long phoenixId;//楼凤id

    private Long comicEvaluationId; //漫画评论id
}
