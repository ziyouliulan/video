package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 综艺管理对象 sp_variety
 *
 * @author ruoyi
 * @date 2021-05-07
 */
public class SpVariety extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 综艺id
     */
    private Long varietyId;

    /**
     * 综艺名称
     */
    @Excel(name = "综艺名称")
    private String varietyName;

    /**
     * 综艺头像
     */
    @Excel(name = "综艺头像")
    private String varietyHeadImg;

    /**
     * 综艺背景
     */
    @Excel(name = "综艺背景")
    private String varietyBg;

    /**
     * 综艺介绍
     */
    @Excel(name = "综艺介绍")
    private String varietyIntroduce;

    public void setVarietyBg(String varietyBg) {
        this.varietyBg = varietyBg;
    }

    public String getVarietyBg() {
        return varietyBg;
    }

    public void setVarietyId(Long varietyId) {
        this.varietyId = varietyId;
    }

    public Long getVarietyId() {
        return varietyId;
    }

    public void setVarietyName(String varietyName) {
        this.varietyName = varietyName;
    }

    public String getVarietyName() {
        return varietyName;
    }

    public void setVarietyHeadImg(String varietyHeadImg) {
        this.varietyHeadImg = varietyHeadImg;
    }

    public String getVarietyHeadImg() {
        return varietyHeadImg;
    }

    public void setVarietyIntroduce(String varietyIntroduce) {
        this.varietyIntroduce = varietyIntroduce;
    }

    public String getVarietyIntroduce() {
        return varietyIntroduce;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("varietyId", getVarietyId())
                .append("varietyName", getVarietyName())
                .append("varietyHeadImg", getVarietyHeadImg())
                .append("varietyBg", getVarietyBg())
                .append("varietyIntroduce", getVarietyIntroduce())
                .toString();
    }
}
