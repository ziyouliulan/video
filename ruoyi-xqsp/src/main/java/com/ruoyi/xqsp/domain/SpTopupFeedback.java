package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 充值反馈对象 sp_topup_feedback
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
public class SpTopupFeedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  编号 */
    private Long topupFeedbackId;

    /** 反馈遇见的问题（1.自主支付，2.客服支付） */
    @Excel(name = "反馈遇见的问题", readConverterExp = "1=.自主支付，2.客服支付")
    private Integer topupFeedbackIssue;

    /** 订单号 */
    @Excel(name = "订单号")
    private String topupFeedbackOrder;

    /** 反馈描述 */
    @Excel(name = "反馈描述")
    private String topupFeedbackContent;

    /** 充值截图 */
    @Excel(name = "充值截图")
    private String topupFeedbackImg;

    /** 反馈时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "反馈时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date topupFeedbackTime;

    /** 是否阅读（管理员是否查阅0未查阅 1已查阅） */
    @Excel(name = "是否阅读", readConverterExp = "管=理员是否查阅0未查阅,1=已查阅")
    private Integer topupFeedbackWatch;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    public void setTopupFeedbackId(Long topupFeedbackId)
    {
        this.topupFeedbackId = topupFeedbackId;
    }

    public Long getTopupFeedbackId()
    {
        return topupFeedbackId;
    }
    public void setTopupFeedbackIssue(Integer topupFeedbackIssue)
    {
        this.topupFeedbackIssue = topupFeedbackIssue;
    }

    public Integer getTopupFeedbackIssue()
    {
        return topupFeedbackIssue;
    }
    public void setTopupFeedbackOrder(String topupFeedbackOrder)
    {
        this.topupFeedbackOrder = topupFeedbackOrder;
    }

    public String getTopupFeedbackOrder()
    {
        return topupFeedbackOrder;
    }
    public void setTopupFeedbackContent(String topupFeedbackContent)
    {
        this.topupFeedbackContent = topupFeedbackContent;
    }

    public String getTopupFeedbackContent()
    {
        return topupFeedbackContent;
    }
    public void setTopupFeedbackImg(String topupFeedbackImg)
    {
        this.topupFeedbackImg = topupFeedbackImg;
    }

    public String getTopupFeedbackImg()
    {
        return topupFeedbackImg;
    }
    public void setTopupFeedbackTime(Date topupFeedbackTime)
    {
        this.topupFeedbackTime = topupFeedbackTime;
    }

    public Date getTopupFeedbackTime()
    {
        return topupFeedbackTime;
    }
    public void setTopupFeedbackWatch(Integer topupFeedbackWatch)
    {
        this.topupFeedbackWatch = topupFeedbackWatch;
    }

    public Integer getTopupFeedbackWatch()
    {
        return topupFeedbackWatch;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("topupFeedbackId", getTopupFeedbackId())
                .append("topupFeedbackIssue", getTopupFeedbackIssue())
                .append("topupFeedbackOrder", getTopupFeedbackOrder())
                .append("topupFeedbackContent", getTopupFeedbackContent())
                .append("topupFeedbackImg", getTopupFeedbackImg())
                .append("topupFeedbackTime", getTopupFeedbackTime())
                .append("topupFeedbackWatch", getTopupFeedbackWatch())
                .append("userId", getUserId())
                .toString();
    }
}
