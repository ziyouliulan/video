package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import com.ruoyi.xqsp.domain.SpUsers;
import lombok.Data;

/**
 * 用户楼凤评论
 */
@Data
public class UserPhoenixEvaluation {
    private SpPhoenixEvaluation spPhoenixEvaluation;
    private String userPhone;
    private Long userHead = 1L;//用户头像
}
