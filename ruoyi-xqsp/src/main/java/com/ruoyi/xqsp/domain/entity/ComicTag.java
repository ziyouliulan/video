package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpComic;
import lombok.Data;

import java.util.List;

/**
 * 漫画标签
 */
@Data
public class ComicTag {
    private List<Long> value1;
    private SpComic spComic;
}
