package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @TableName sp_category_tag
 */
@TableName(value ="sp_category_tag")
@Data
public class SpCategoryTag implements Serializable {
    /**
     * 标签编号
     */
    @TableId(type = IdType.AUTO)
    private Long categoryTagId;

    /**
     * 值
     */
    private String categoryTagValue;

    /**
     * 标签名字
     */
    private String categoryTagName;

    /**
     *
     */
    private Integer status;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 是否显示
     */
    private Boolean isShow;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
