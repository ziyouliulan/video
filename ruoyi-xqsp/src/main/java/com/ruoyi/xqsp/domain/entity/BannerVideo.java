package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpAdvertising;
import com.ruoyi.xqsp.domain.SpBanner;
import lombok.Data;

/**
 * banner图和视频
 */
@Data
public class BannerVideo {
    private SpAdvertising spAdvertising;
    private String videoName;
    private String categoryName;

}
