package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import lombok.Data;

/**
 * 综艺视频
 */
@Data
public class VarietyVideo {

    private SpVarietyVideo spVarietyVideo;
    private String varietyName;

}
