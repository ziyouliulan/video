package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import lombok.Data;

import java.util.List;

/**
 * 前台综艺模块
 */
@Data
public class HomeVarietyVideo {
    private SpVariety spVariety; //综艺
    private List<SpVarietyVideo>varietyVideoList; //视频
    private Integer counts;//部数多少部
}
