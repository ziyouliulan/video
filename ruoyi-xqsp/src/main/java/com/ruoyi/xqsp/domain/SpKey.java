package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 激活码表对象 sp_key
 * 
 * @author ruoyi
 * @date 2021-05-14
 */
public class SpKey extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long keyId;

    /** 激活码 */
    @Excel(name = "激活码")
    private String keyActivationCode;

    /** 激活天数 */
    @Excel(name = "激活天数")
    private Long keyDays;

    public void setKeyId(Long keyId) 
    {
        this.keyId = keyId;
    }

    public Long getKeyId() 
    {
        return keyId;
    }
    public void setKeyActivationCode(String keyActivationCode) 
    {
        this.keyActivationCode = keyActivationCode;
    }

    public String getKeyActivationCode() 
    {
        return keyActivationCode;
    }
    public void setKeyDays(Long keyDays) 
    {
        this.keyDays = keyDays;
    }

    public Long getKeyDays() 
    {
        return keyDays;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("keyId", getKeyId())
            .append("keyActivationCode", getKeyActivationCode())
            .append("keyDays", getKeyDays())
            .toString();
    }
}
