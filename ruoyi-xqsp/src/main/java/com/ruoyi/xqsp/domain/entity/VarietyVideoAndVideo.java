package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVideo;
import lombok.Data;

import java.util.List;

/**
 * 综艺视频和视频
 */
@Data
public class VarietyVideoAndVideo {
    private SpVarietyVideo spVarietyVideo;
    private SpVideo spVideo;
    private SpActor spActor;
    private SpVariety spVariety;
    private Long type;//收藏类型 1微视频 2位综艺视频
    private Long ID;
    private String cacheUrl;//缓存地址
}
