package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVideo;
import lombok.Data;

/**
 * 前台首页视频和综艺视频的模糊查询
 */
@Data
public class InquireVideo {
    private  Long type;
    private SpVideo spVideo;

    private SpVarietyVideo spVarietyVideo;

}
