package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 视频详情
 * @TableName sp_video_headlines
 */
@TableName(value ="sp_video_headlines")
@Data
public class SpVideoHeadlines implements Serializable {
    /**
     * 视频编号
     */
    @TableId(type = IdType.AUTO)
    private Long videoId;

    /**
     * 视频名称
     */
    private String videoName;

    /**
     * 视频封面
     */
    private String videoCoverImg;

    /**
     * 演员id
     */
    private Long actorId;

    /**
     * 视频介绍
     */
    private String videoIntroduce;

    /**
     * 观看人数
     */
    private Long videoWatchNumber;

    /**
     * 点赞人数
     */
    private Long videoLikeNumber;

    /**
     * 付费设置(0免费，1金币付费，2会员免费)
     */
    private Integer videoUnlock;

    /**
     * 视频分类id
     */
    private Long videoCategoryId;

    /**
     * 是否上架（0上架  1下架）
     */
    private String videoPutaway;

    /**
     * 清晰度设置（0标清 2高清）
     */
    private String videoDefinition;

    /**
     * 解锁所需金币数
     */
    private Long videoUnlockGlod;

    /**
     * 上传时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date videoTime;

    /**
     * 视频地址
     */
    private String videoUrl;

    /**
     * 差评人数
     */
    private Integer videoUnlikeNumber;

    /**
     * 收藏人数
     */
    private Integer videoCollectNumber;

    /**
     * 视频作者
     */
    private String videoAuthor;

    /**
     * 视频类型（1.视频，2.动漫）
     */
    private Integer videoType;

    /**
     * 视频预览
     */
    private String videoUrlPreview;

    /**
     * 视频时长
     */
    private String videoDuration;

    /**
     * 置顶
     */
    private Integer isTop;

    /**
     * 视频解锁人数
     */
    private Integer unlockCount;

    /**
     * 置顶时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date isTopDate;

    /**
     * 评分
     */
    private Double videoScore;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SpVideoHeadlines other = (SpVideoHeadlines) that;
        return (this.getVideoId() == null ? other.getVideoId() == null : this.getVideoId().equals(other.getVideoId()))
            && (this.getVideoName() == null ? other.getVideoName() == null : this.getVideoName().equals(other.getVideoName()))
            && (this.getVideoCoverImg() == null ? other.getVideoCoverImg() == null : this.getVideoCoverImg().equals(other.getVideoCoverImg()))
            && (this.getActorId() == null ? other.getActorId() == null : this.getActorId().equals(other.getActorId()))
            && (this.getVideoIntroduce() == null ? other.getVideoIntroduce() == null : this.getVideoIntroduce().equals(other.getVideoIntroduce()))
            && (this.getVideoWatchNumber() == null ? other.getVideoWatchNumber() == null : this.getVideoWatchNumber().equals(other.getVideoWatchNumber()))
            && (this.getVideoLikeNumber() == null ? other.getVideoLikeNumber() == null : this.getVideoLikeNumber().equals(other.getVideoLikeNumber()))
            && (this.getVideoUnlock() == null ? other.getVideoUnlock() == null : this.getVideoUnlock().equals(other.getVideoUnlock()))
            && (this.getVideoCategoryId() == null ? other.getVideoCategoryId() == null : this.getVideoCategoryId().equals(other.getVideoCategoryId()))
            && (this.getVideoPutaway() == null ? other.getVideoPutaway() == null : this.getVideoPutaway().equals(other.getVideoPutaway()))
            && (this.getVideoDefinition() == null ? other.getVideoDefinition() == null : this.getVideoDefinition().equals(other.getVideoDefinition()))
            && (this.getVideoUnlockGlod() == null ? other.getVideoUnlockGlod() == null : this.getVideoUnlockGlod().equals(other.getVideoUnlockGlod()))
            && (this.getVideoTime() == null ? other.getVideoTime() == null : this.getVideoTime().equals(other.getVideoTime()))
            && (this.getVideoUrl() == null ? other.getVideoUrl() == null : this.getVideoUrl().equals(other.getVideoUrl()))
            && (this.getVideoUnlikeNumber() == null ? other.getVideoUnlikeNumber() == null : this.getVideoUnlikeNumber().equals(other.getVideoUnlikeNumber()))
            && (this.getVideoCollectNumber() == null ? other.getVideoCollectNumber() == null : this.getVideoCollectNumber().equals(other.getVideoCollectNumber()))
            && (this.getVideoAuthor() == null ? other.getVideoAuthor() == null : this.getVideoAuthor().equals(other.getVideoAuthor()))
            && (this.getVideoType() == null ? other.getVideoType() == null : this.getVideoType().equals(other.getVideoType()))
            && (this.getVideoUrlPreview() == null ? other.getVideoUrlPreview() == null : this.getVideoUrlPreview().equals(other.getVideoUrlPreview()))
            && (this.getVideoDuration() == null ? other.getVideoDuration() == null : this.getVideoDuration().equals(other.getVideoDuration()))
            && (this.getIsTop() == null ? other.getIsTop() == null : this.getIsTop().equals(other.getIsTop()))
            && (this.getUnlockCount() == null ? other.getUnlockCount() == null : this.getUnlockCount().equals(other.getUnlockCount()))
            && (this.getIsTopDate() == null ? other.getIsTopDate() == null : this.getIsTopDate().equals(other.getIsTopDate()))
            && (this.getVideoScore() == null ? other.getVideoScore() == null : this.getVideoScore().equals(other.getVideoScore()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getVideoId() == null) ? 0 : getVideoId().hashCode());
        result = prime * result + ((getVideoName() == null) ? 0 : getVideoName().hashCode());
        result = prime * result + ((getVideoCoverImg() == null) ? 0 : getVideoCoverImg().hashCode());
        result = prime * result + ((getActorId() == null) ? 0 : getActorId().hashCode());
        result = prime * result + ((getVideoIntroduce() == null) ? 0 : getVideoIntroduce().hashCode());
        result = prime * result + ((getVideoWatchNumber() == null) ? 0 : getVideoWatchNumber().hashCode());
        result = prime * result + ((getVideoLikeNumber() == null) ? 0 : getVideoLikeNumber().hashCode());
        result = prime * result + ((getVideoUnlock() == null) ? 0 : getVideoUnlock().hashCode());
        result = prime * result + ((getVideoCategoryId() == null) ? 0 : getVideoCategoryId().hashCode());
        result = prime * result + ((getVideoPutaway() == null) ? 0 : getVideoPutaway().hashCode());
        result = prime * result + ((getVideoDefinition() == null) ? 0 : getVideoDefinition().hashCode());
        result = prime * result + ((getVideoUnlockGlod() == null) ? 0 : getVideoUnlockGlod().hashCode());
        result = prime * result + ((getVideoTime() == null) ? 0 : getVideoTime().hashCode());
        result = prime * result + ((getVideoUrl() == null) ? 0 : getVideoUrl().hashCode());
        result = prime * result + ((getVideoUnlikeNumber() == null) ? 0 : getVideoUnlikeNumber().hashCode());
        result = prime * result + ((getVideoCollectNumber() == null) ? 0 : getVideoCollectNumber().hashCode());
        result = prime * result + ((getVideoAuthor() == null) ? 0 : getVideoAuthor().hashCode());
        result = prime * result + ((getVideoType() == null) ? 0 : getVideoType().hashCode());
        result = prime * result + ((getVideoUrlPreview() == null) ? 0 : getVideoUrlPreview().hashCode());
        result = prime * result + ((getVideoDuration() == null) ? 0 : getVideoDuration().hashCode());
        result = prime * result + ((getIsTop() == null) ? 0 : getIsTop().hashCode());
        result = prime * result + ((getUnlockCount() == null) ? 0 : getUnlockCount().hashCode());
        result = prime * result + ((getIsTopDate() == null) ? 0 : getIsTopDate().hashCode());
        result = prime * result + ((getVideoScore() == null) ? 0 : getVideoScore().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", videoId=").append(videoId);
        sb.append(", videoName=").append(videoName);
        sb.append(", videoCoverImg=").append(videoCoverImg);
        sb.append(", actorId=").append(actorId);
        sb.append(", videoIntroduce=").append(videoIntroduce);
        sb.append(", videoWatchNumber=").append(videoWatchNumber);
        sb.append(", videoLikeNumber=").append(videoLikeNumber);
        sb.append(", videoUnlock=").append(videoUnlock);
        sb.append(", videoCategoryId=").append(videoCategoryId);
        sb.append(", videoPutaway=").append(videoPutaway);
        sb.append(", videoDefinition=").append(videoDefinition);
        sb.append(", videoUnlockGlod=").append(videoUnlockGlod);
        sb.append(", videoTime=").append(videoTime);
        sb.append(", videoUrl=").append(videoUrl);
        sb.append(", videoUnlikeNumber=").append(videoUnlikeNumber);
        sb.append(", videoCollectNumber=").append(videoCollectNumber);
        sb.append(", videoAuthor=").append(videoAuthor);
        sb.append(", videoType=").append(videoType);
        sb.append(", videoUrlPreview=").append(videoUrlPreview);
        sb.append(", videoDuration=").append(videoDuration);
        sb.append(", isTop=").append(isTop);
        sb.append(", unlockCount=").append(unlockCount);
        sb.append(", isTopDate=").append(isTopDate);
        sb.append(", videoScore=").append(videoScore);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
