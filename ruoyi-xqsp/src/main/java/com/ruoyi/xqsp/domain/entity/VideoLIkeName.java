package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVideo;
import lombok.Data;

import java.util.List;

/**
 * 首页的摸糊查询
 */
@Data
public class VideoLIkeName {
    private Object spVideoList;
    private List<SpVarietyVideo>spVarietyVideoLists;
}
