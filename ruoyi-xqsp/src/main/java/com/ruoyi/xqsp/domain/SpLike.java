package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户点赞对象 sp_like
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
public class SpLike extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long likeId;

    /** 用户编号 */
    @Excel(name = "用户编号")
    private Long userId;

    /** 点赞类型id（视频，视频评论，等） */
    @Excel(name = "点赞类型id", readConverterExp = "视=频，视频评论，等")
    private Long likeTypeId;

    /** 点赞类型（视频0，视频评论1，漫画2，漫画评论3，楼凤4，楼凤评论5，综艺6，综艺评论7） */
    @Excel(name = "点赞类型", readConverterExp = "视=频0，视频评论1，漫画2，漫画评论3，楼凤4，楼凤评论5，综艺6，综艺评论7")
    private Long likeType;

    public void setLikeId(Long likeId) 
    {
        this.likeId = likeId;
    }

    public Long getLikeId() 
    {
        return likeId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setLikeTypeId(Long likeTypeId) 
    {
        this.likeTypeId = likeTypeId;
    }

    public Long getLikeTypeId() 
    {
        return likeTypeId;
    }
    public void setLikeType(Long likeType) 
    {
        this.likeType = likeType;
    }

    public Long getLikeType() 
    {
        return likeType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("likeId", getLikeId())
            .append("userId", getUserId())
            .append("likeTypeId", getLikeTypeId())
            .append("likeType", getLikeType())
            .toString();
    }
}
