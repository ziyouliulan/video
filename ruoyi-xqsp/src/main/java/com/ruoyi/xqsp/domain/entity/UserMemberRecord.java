package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 用户会员记录
 */
@Data
public class UserMemberRecord {
    private Long userId;
    private Integer type;
    private Integer pageNums;
    private Integer pageSize;
}
