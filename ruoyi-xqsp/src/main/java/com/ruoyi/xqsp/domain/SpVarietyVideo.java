package com.ruoyi.xqsp.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 综艺视频对象 sp_variety_video
 *
 * @author ruoyi
 * @date 2021-05-07
 */
public class SpVarietyVideo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 综艺视频编号
     */
    private Long varietyVideoId;

    /**
     * 综艺id（综艺表）
     */
    @Excel(name = "综艺id", readConverterExp = "综=艺表")
    private Long varietyId;

    /**
     * 综艺视频地址
     */
    @Excel(name = "综艺视频地址")
    private String varietyVideoUrl;

    /**
     * 综艺视频观看次数
     */
    @Excel(name = "综艺视频观看次数")
    private Long varietyVideoWatch;

    /**
     * 综艺视频点赞次数
     */
    @Excel(name = "综艺视频点赞次数")
    private Long varietyVideoLikeNumber;

    /**
     * 综艺视频解锁设置
     */
    @Excel(name = "综艺视频解锁设置")
    private Integer varietyVideoUnlock;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date varietyVideoTime;

    /**
     * 是否上架
     */
    @Excel(name = "是否上架")
    private Integer varietyVideoPutaway;

    /**
     * 解锁视频所需金币数
     */
    @Excel(name = "解锁视频所需金币数")
    private Integer varietyVideoUnlockGold;
    /**
     * 综艺视频差评次数
     */
    @Excel(name = "综艺视频差评次数")
    private Long varietyVideoUnlikeNumber;

    /**
     * 收藏人数
     */
    @Excel(name = "收藏人数")
    private Long varietyVideoCollectNumber;

    /**
     * 综艺视频名字
     */
    @Excel(name = "综艺视频名字")
    private String varietyVideoName;

    /**
     * 综艺视频封面
     */
    @Excel(name = "综艺视频封面")
    private String varietyVideoCoverImg;

    /**
     * 清晰度设置
     */
    @Excel(name = "清晰度设置")
    private String varietyVideoDefinition;
    /**
     * 视频预览
     */
    @Excel(name = "视频预览")
    private String varietyVideoUrlPreview;

    /**
     * 视频时长
     */
    @Excel(name = "视频时长")
    private String varietyVideoDuration;

    /**
     * 解锁人数
     */
    @Excel(name = "解锁人数")
    private Integer unlockCount = 0;  //视频金币解锁人数

    public Integer getUnlockCount() {
        return unlockCount;
    }

    public void setUnlockCount(Integer unlockCount) {
        this.unlockCount = unlockCount;
    }

    public String getVarietyVideoDuration() {
        return varietyVideoDuration;
    }

    public void setVarietyVideoDuration(String varietyVideoDuration) {
        this.varietyVideoDuration = varietyVideoDuration;
    }

    public String getVarietyVideoUrlPreview() {
        return varietyVideoUrlPreview;
    }

    public void setVarietyVideoUrlPreview(String varietyVideoUrlPreview) {
        this.varietyVideoUrlPreview = varietyVideoUrlPreview;
    }

    public void setVarietyVideoId(Long varietyVideoId) {
        this.varietyVideoId = varietyVideoId;
    }

    public Long getVarietyVideoId() {
        return varietyVideoId;
    }

    public void setVarietyId(Long varietyId) {
        this.varietyId = varietyId;
    }

    public Long getVarietyId() {
        return varietyId;
    }

    public void setVarietyVideoUrl(String varietyVideoUrl) {
        this.varietyVideoUrl = varietyVideoUrl;
    }

    public String getVarietyVideoUrl() {
        return varietyVideoUrl;
    }

    public void setVarietyVideoWatch(Long varietyVideoWatch) {
        this.varietyVideoWatch = varietyVideoWatch;
    }

    public Long getVarietyVideoWatch() {
        return varietyVideoWatch;
    }

    public void setVarietyVideoLikeNumber(Long varietyVideoLikeNumber) {
        this.varietyVideoLikeNumber = varietyVideoLikeNumber;
    }

    public Long getVarietyVideoLikeNumber() {
        return varietyVideoLikeNumber;
    }

    public void setVarietyVideoUnlock(Integer varietyVideoUnlock) {
        this.varietyVideoUnlock = varietyVideoUnlock;
    }

    public Integer getVarietyVideoUnlock() {
        return varietyVideoUnlock;
    }

    public void setVarietyVideoTime(Date varietyVideoTime) {
        this.varietyVideoTime = varietyVideoTime;
    }

    public Date getVarietyVideoTime() {
        return varietyVideoTime;
    }

    public void setVarietyVideoPutaway(Integer varietyVideoPutaway) {
        this.varietyVideoPutaway = varietyVideoPutaway;
    }

    public Integer getVarietyVideoPutaway() {
        return varietyVideoPutaway;
    }

    public void setVarietyVideoUnlockGold(Integer varietyVideoUnlockGold) {
        this.varietyVideoUnlockGold = varietyVideoUnlockGold;
    }

    public Integer getVarietyVideoUnlockGold() {
        return varietyVideoUnlockGold;
    }

    public void setVarietyVideoUnlikeNumber(Long varietyVideoUnlikeNumber) {
        this.varietyVideoUnlikeNumber = varietyVideoUnlikeNumber;
    }

    public Long getVarietyVideoUnlikeNumber() {
        return varietyVideoUnlikeNumber;
    }

    public void setVarietyVideoCollectNumber(Long varietyVideoCollectNumber) {
        this.varietyVideoCollectNumber = varietyVideoCollectNumber;
    }

    public Long getVarietyVideoCollectNumber() {
        return varietyVideoCollectNumber;
    }

    public void setVarietyVideoName(String varietyVideoName) {
        this.varietyVideoName = varietyVideoName;
    }

    public String getVarietyVideoName() {
        return varietyVideoName;
    }

    public void setVarietyVideoCoverImg(String varietyVideoCoverImg) {
        this.varietyVideoCoverImg = varietyVideoCoverImg;
    }

    public String getVarietyVideoCoverImg() {
        return varietyVideoCoverImg;
    }

    public void setVarietyVideoDefinition(String varietyVideoDefinition) {
        this.varietyVideoDefinition = varietyVideoDefinition;
    }

    public String getVarietyVideoDefinition() {
        return varietyVideoDefinition;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("varietyVideoId", getVarietyVideoId())
                .append("varietyId", getVarietyId())
                .append("varietyVideoUrl", getVarietyVideoUrl())
                .append("varietyVideoWatch", getVarietyVideoWatch())
                .append("varietyVideoLikeNumber", getVarietyVideoLikeNumber())
                .append("varietyVideoUnlock", getVarietyVideoUnlock())
                .append("varietyVideoTime", getVarietyVideoTime())
                .append("varietyVideoPutaway", getVarietyVideoPutaway())
                .append("varietyVideoUnlockGold", getVarietyVideoUnlockGold())
                .append("varietyVideoUnlikeNumber", getVarietyVideoUnlikeNumber())
                .append("varietyVideoCollectNumber", getVarietyVideoCollectNumber())
                .append("varietyVideoName", getVarietyVideoName())
                .append("varietyVideoCoverImg", getVarietyVideoCoverImg())
                .append("varietyVideoDefinition", getVarietyVideoDefinition())
                .toString();
    }

}
