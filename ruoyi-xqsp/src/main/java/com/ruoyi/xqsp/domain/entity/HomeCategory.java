package com.ruoyi.xqsp.domain.entity;


import com.ruoyi.xqsp.domain.*;
import lombok.Data;

import java.util.List;

/**
 * 首页分类
 */
@Data
public class HomeCategory {


    private SpVideoCategory spVideoCategory;
    private List<SpVideo> spVideo;
    private List<SpAdvertising>spAdvertisingList;

    //综艺模块
    private List<SpVariety> spVariety; //综艺
    private List<HomeVarietyVideo> homeVarietyVideo;//综艺下的视频

    //演员模块
    private List<SpActor>spActors;// 演员
    private List<VideoActor>videoActorList;//演员视频


}
