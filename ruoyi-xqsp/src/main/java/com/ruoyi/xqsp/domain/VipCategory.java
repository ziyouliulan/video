package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalSerializer;
import lombok.Data;

/**
 *
 * @TableName sp_vip_category
 */
@TableName(value ="sp_vip_category")
@Data
public class VipCategory implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 金额
     */
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal money;
    /**
     * 金额
     */
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal favorable;

    /**
     * 多少个月
     */
    private Integer time;

    /**
     * 是否删除
     */
    private Integer deleted;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 更新时间
     */
    private Integer type;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
