package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车友群对象 sp_group
 * 
 * @author ruoyi
 * @date 2021-05-28
 */
public class SpGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  编号 */
    private Long groupId;

    /** 内容 */
    @Excel(name = "内容")
    private String groupContent;

    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setGroupContent(String groupContent) 
    {
        this.groupContent = groupContent;
    }

    public String getGroupContent() 
    {
        return groupContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("groupId", getGroupId())
            .append("groupContent", getGroupContent())
            .toString();
    }
}
