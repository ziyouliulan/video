package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 聊天历史
 */
@Data
public class ChatHistory {
    private Long userId;
    private Long chatNumber;
}
