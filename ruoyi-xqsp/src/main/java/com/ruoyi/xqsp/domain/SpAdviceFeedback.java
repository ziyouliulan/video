package com.ruoyi.xqsp.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 意见反馈对象 sp_advice_feedback
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
public class SpAdviceFeedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    @TableId(type = IdType.AUTO)
    private Long adviceFeedbackId;

    /**  用户id */
    @Excel(name = " 用户id")
    private Long userId;

    /** 反馈遇见问题(1.无法播放，2内容不符，3字幕声音，4侵权投诉，5播放卡顿，6收益提现，7其他) */
    @Excel(name = "反馈遇见问题(1.无法播放，2内容不符，3字幕声音，4侵权投诉，5播放卡顿，7其他)")
    private Long adviceFeedbackIssue;

    /** 反馈内容 */
    @Excel(name = "反馈内容")
    private String adviceFeedbackContent;

    /** 反馈图片 */
    @Excel(name = "反馈图片")
    private String adviceFeedbackImg;

    /** 反馈机型和手机系统版本 */
    @Excel(name = "反馈机型和手机系统版本")
    private String adviceFeedbackPhone;

    /** 软件名称 */
    @Excel(name = "软件名称")
    private String adviceFeedbackSoftware;

    /** 网络类型（1.WI-FI  2.4g网络3.5g网络 4.其他） */
    @Excel(name = "网络类型", readConverterExp = "1=.WI-FI,2=.4g网络3.5g网络,4=.其他")
    private Long adviceFeedbackNetwork;

    /** 通讯类型（1.中国移动 2.中国联通 3.中国电信 4.其他） */
    @Excel(name = "通讯类型", readConverterExp = "1=.中国移动,2=.中国联通,3=.中国电信,4=.其他")
    private Long adviceFeedbackCommunicate;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String adviceFeedbackMailbox;

    /** 反馈时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "反馈时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date adviceFeedbackTime;

    /** 是否阅读（管理员是否查阅0未阅读 1已阅读） */
    @Excel(name = "是否阅读", readConverterExp = "管=理员是否查阅0未阅读,1=已阅读")
    private Integer adviceFeedbackWatch;

    public void setAdviceFeedbackId(Long adviceFeedbackId) 
    {
        this.adviceFeedbackId = adviceFeedbackId;
    }

    public Long getAdviceFeedbackId() 
    {
        return adviceFeedbackId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAdviceFeedbackIssue(Long adviceFeedbackIssue) 
    {
        this.adviceFeedbackIssue = adviceFeedbackIssue;
    }

    public Long getAdviceFeedbackIssue() 
    {
        return adviceFeedbackIssue;
    }
    public void setAdviceFeedbackContent(String adviceFeedbackContent) 
    {
        this.adviceFeedbackContent = adviceFeedbackContent;
    }

    public String getAdviceFeedbackContent() 
    {
        return adviceFeedbackContent;
    }
    public void setAdviceFeedbackImg(String adviceFeedbackImg) 
    {
        this.adviceFeedbackImg = adviceFeedbackImg;
    }

    public String getAdviceFeedbackImg() 
    {
        return adviceFeedbackImg;
    }
    public void setAdviceFeedbackPhone(String adviceFeedbackPhone) 
    {
        this.adviceFeedbackPhone = adviceFeedbackPhone;
    }

    public String getAdviceFeedbackPhone() 
    {
        return adviceFeedbackPhone;
    }
    public void setAdviceFeedbackSoftware(String adviceFeedbackSoftware) 
    {
        this.adviceFeedbackSoftware = adviceFeedbackSoftware;
    }

    public String getAdviceFeedbackSoftware() 
    {
        return adviceFeedbackSoftware;
    }
    public void setAdviceFeedbackNetwork(Long adviceFeedbackNetwork) 
    {
        this.adviceFeedbackNetwork = adviceFeedbackNetwork;
    }

    public Long getAdviceFeedbackNetwork() 
    {
        return adviceFeedbackNetwork;
    }
    public void setAdviceFeedbackCommunicate(Long adviceFeedbackCommunicate) 
    {
        this.adviceFeedbackCommunicate = adviceFeedbackCommunicate;
    }

    public Long getAdviceFeedbackCommunicate() 
    {
        return adviceFeedbackCommunicate;
    }
    public void setAdviceFeedbackMailbox(String adviceFeedbackMailbox) 
    {
        this.adviceFeedbackMailbox = adviceFeedbackMailbox;
    }

    public String getAdviceFeedbackMailbox() 
    {
        return adviceFeedbackMailbox;
    }
    public void setAdviceFeedbackTime(Date adviceFeedbackTime) 
    {
        this.adviceFeedbackTime = adviceFeedbackTime;
    }

    public Date getAdviceFeedbackTime() 
    {
        return adviceFeedbackTime;
    }
    public void setAdviceFeedbackWatch(Integer adviceFeedbackWatch) 
    {
        this.adviceFeedbackWatch = adviceFeedbackWatch;
    }

    public Integer getAdviceFeedbackWatch() 
    {
        return adviceFeedbackWatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("adviceFeedbackId", getAdviceFeedbackId())
            .append("userId", getUserId())
            .append("adviceFeedbackIssue", getAdviceFeedbackIssue())
            .append("adviceFeedbackContent", getAdviceFeedbackContent())
            .append("adviceFeedbackImg", getAdviceFeedbackImg())
            .append("adviceFeedbackPhone", getAdviceFeedbackPhone())
            .append("adviceFeedbackSoftware", getAdviceFeedbackSoftware())
            .append("adviceFeedbackNetwork", getAdviceFeedbackNetwork())
            .append("adviceFeedbackCommunicate", getAdviceFeedbackCommunicate())
            .append("adviceFeedbackMailbox", getAdviceFeedbackMailbox())
            .append("adviceFeedbackTime", getAdviceFeedbackTime())
            .append("adviceFeedbackWatch", getAdviceFeedbackWatch())
            .toString();
    }
}
