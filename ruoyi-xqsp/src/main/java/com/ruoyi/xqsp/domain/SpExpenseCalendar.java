package com.ruoyi.xqsp.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 消费记录对象 sp_expense_calendar
 *
 * @author ruoyi
 * @date 2021-05-08
 */
public class SpExpenseCalendar extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long expenseCalendarId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 订单号 */
    @Excel(name = "订单号")
    private String expenseCalendarOrder;

    /** 状态 */
    @Excel(name = "类型id")
    private Long expenseCalendarState;

    /** 消费金币数 */
    @Excel(name = "消费金币数")
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal expenseCalendarGlod;

    /** 消费时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "消费时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date expenseCalendarTime;

    /** 消费途径（1.视频，2.漫画，3.楼凤，4.综艺） */
    @Excel(name = "消费途径", readConverterExp = "1=.视频，2.漫画，3.楼凤，4.综艺")
    private Integer expenseCalendarWay;

    public void setExpenseCalendarId(Long expenseCalendarId)
    {
        this.expenseCalendarId = expenseCalendarId;
    }

    public Long getExpenseCalendarId()
    {
        return expenseCalendarId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setExpenseCalendarOrder(String expenseCalendarOrder)
    {
        this.expenseCalendarOrder = expenseCalendarOrder;
    }

    public String getExpenseCalendarOrder()
    {
        return expenseCalendarOrder;
    }
    public void setExpenseCalendarState(Long expenseCalendarState)
    {
        this.expenseCalendarState = expenseCalendarState;
    }

    public Long getExpenseCalendarState()
    {
        return expenseCalendarState;
    }
    public void setExpenseCalendarGlod(BigDecimal expenseCalendarGlod)
    {
        this.expenseCalendarGlod = expenseCalendarGlod;
    }

    public BigDecimal getExpenseCalendarGlod()
    {
        return expenseCalendarGlod;
    }
    public void setExpenseCalendarTime(Date expenseCalendarTime)
    {
        this.expenseCalendarTime = expenseCalendarTime;
    }

    public Date getExpenseCalendarTime()
    {
        return expenseCalendarTime;
    }
    public void setExpenseCalendarWay(Integer expenseCalendarWay)
    {
        this.expenseCalendarWay = expenseCalendarWay;
    }

    public Integer getExpenseCalendarWay()
    {
        return expenseCalendarWay;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("expenseCalendarId", getExpenseCalendarId())
            .append("userId", getUserId())
            .append("expenseCalendarOrder", getExpenseCalendarOrder())
            .append("expenseCalendarState", getExpenseCalendarState())
            .append("expenseCalendarGlod", getExpenseCalendarGlod())
            .append("expenseCalendarTime", getExpenseCalendarTime())
            .append("expenseCalendarWay", getExpenseCalendarWay())
            .toString();
    }
    private Long chapterCounts; //章节

    public Long getChapterCounts() {
        return chapterCounts;
    }

    public void setChapterCounts(Long chapterCounts) {
        this.chapterCounts = chapterCounts;
    }
    private Long chapterDirectory;//第几章

    public Long getChapterDirectory() {
        return chapterDirectory;
    }

    public void setChapterDirectory(Long chapterDirectory) {
        this.chapterDirectory = chapterDirectory;
    }

}
