//
//package com.ruoyi.xqsp.domain;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * @Description:TODO(世界动态点赞实体类)
// *
// * @version: V1.0
// * @author: wlx
// *
// */
//@Data
//@ApiModel("TPublishsLike")
//@TableName(value = "t_publishs_like")
//@AllArgsConstructor
//@NoArgsConstructor
//public class PublishsLike implements Serializable {
//
//	private static final long serialVersionUID = 1606801212699L;
//
//    @TableId(value = "id", type = IdType.AUTO)
//    @ApiModelProperty(name = "id", value = "id", hidden = true)
//	private Long id;
//
//    @ApiModelProperty(name = "publishId", value = "动态id")
//	private Long publishId;
//
//    @ApiModelProperty(name = "likedPostId", value = "点赞的用户id")
//	private Long likedPostId;
//
//    @ApiModelProperty(name = "status", value = "点赞状态，0取消，1点赞")
//	private Integer status;
//
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//    @ApiModelProperty(name = "createTime", value = "更新时间")
//	private Date createTime;
//
//
//}
