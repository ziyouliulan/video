package com.ruoyi.xqsp.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分类子表对象 sp_video_category
 *
 * @author ruoyi
 * @date 2021-04-29
 */
@Data
public class SpVideoCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 视频分类id */
    private Long videoCategoryId;

    /** 视频分类父id */
    @Excel(name = "视频分类父id")
    private Long videoCategoryParentId;

    /** 视频分类名称 */
    @Excel(name = "视频分类名称")
    private String videoCategoryName;

    /** 分类权重（用于排序） */
    @Excel(name = "分类权重", readConverterExp = "用=于排序")
    private Long videoCategoryWeight;

    /** 分类状态 （用于屏蔽分类 0显示  1不显示） */
    @Excel(name = "分类状态 ", readConverterExp = "用=于屏蔽分类,0=显示,1=不显示")
    private Long videoCategoryState;

    /** 模式选择(0 模式1 ，1 模式2 ) */
    @Excel(name = "模式选择(0 模式1 ，1 模式2 )")
    private Long videoCategoryPattern;

    private Boolean selected;

    public void setVideoCategoryId(Long videoCategoryId)
    {
        this.videoCategoryId = videoCategoryId;
    }

    public Long getVideoCategoryId()
    {
        return videoCategoryId;
    }
    public void setVideoCategoryParentId(Long videoCategoryParentId)
    {
        this.videoCategoryParentId = videoCategoryParentId;
    }

    public Long getVideoCategoryParentId()
    {
        return videoCategoryParentId;
    }
    public void setVideoCategoryName(String videoCategoryName)
    {
        this.videoCategoryName = videoCategoryName;
    }

    public String getVideoCategoryName()
    {
        return videoCategoryName;
    }
    public void setVideoCategoryWeight(Long videoCategoryWeight)
    {
        this.videoCategoryWeight = videoCategoryWeight;
    }

    public Long getVideoCategoryWeight()
    {
        return videoCategoryWeight;
    }
    public void setVideoCategoryState(Long videoCategoryState)
    {
        this.videoCategoryState = videoCategoryState;
    }

    public Long getVideoCategoryState()
    {
        return videoCategoryState;
    }
    public void setVideoCategoryPattern(Long videoCategoryPattern)
    {
        this.videoCategoryPattern = videoCategoryPattern;
    }

    public Long getVideoCategoryPattern()
    {
        return videoCategoryPattern;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("videoCategoryId", getVideoCategoryId())
                .append("videoCategoryParentId", getVideoCategoryParentId())
                .append("videoCategoryName", getVideoCategoryName())
                .append("videoCategoryWeight", getVideoCategoryWeight())
                .append("videoCategoryState", getVideoCategoryState())
                .append("videoCategoryPattern", getVideoCategoryPattern())
                .toString();
    }
}
