package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户头像对象 sp_head_img
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public class SpHeadImg
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long headImgId;

    /** 头像地址 */
    @Excel(name = "头像地址")
    private String headImgUrl;

    public void setHeadImgId(Long headImgId) 
    {
        this.headImgId = headImgId;
    }

    public Long getHeadImgId() 
    {
        return headImgId;
    }
    public void setHeadImgUrl(String headImgUrl) 
    {
        this.headImgUrl = headImgUrl;
    }

    public String getHeadImgUrl() 
    {
        return headImgUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("headImgId", getHeadImgId())
            .append("headImgUrl", getHeadImgUrl())
            .toString();
    }
}
