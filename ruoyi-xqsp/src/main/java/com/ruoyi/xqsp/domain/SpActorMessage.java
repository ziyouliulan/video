package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 演员信息对象 sp_actor_message
 *
 * @author ruoyi
 * @date 2021-04-22
 */
public class SpActorMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long actorMessageId;

    /** 演员id */
    @Excel(name = "演员id")
    private Long actorId;

    /** 感情状态 */
    @Excel(name = "感情状态")
    private String actorMessageEmotionalState;

    /** 喜欢 */
    @Excel(name = "喜欢")
    private String actorMessageLove;

    /** 性别 */
    @Excel(name = "性别")
    private String actorMessageSex;

    /** 生日 */
//    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date actorMessageBirth;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long actorMessageAge;

    /** 种族 */
    @Excel(name = "种族")
    private String actorMessageRace;

    /** 发色 */
    @Excel(name = "发色")
    private String actorMessageHairColor;

    /** 假奶子 */
    @Excel(name = "假奶子")
    private String actorMessageFakeMilk;

    /** 三围 */
    @Excel(name = "三围")
    private String actorMessageSanwei;

    /** 身高 */
    @Excel(name = "身高")
    private String actorMessageStature;

    /** 体重 */
    @Excel(name = "体重")
    private String actorMessageWeight;

    /** 城市和国家 */
    @Excel(name = "城市和国家")
    private String actorMessageSite;

    public void setActorMessageId(Long actorMessageId)
    {
        this.actorMessageId = actorMessageId;
    }

    public Long getActorMessageId()
    {
        return actorMessageId;
    }
    public void setActorId(Long actorId)
    {
        this.actorId = actorId;
    }

    public Long getActorId()
    {
        return actorId;
    }
    public void setActorMessageEmotionalState(String actorMessageEmotionalState)
    {
        this.actorMessageEmotionalState = actorMessageEmotionalState;
    }

    public String getActorMessageEmotionalState()
    {
        return actorMessageEmotionalState;
    }
    public void setActorMessageLove(String actorMessageLove)
    {
        this.actorMessageLove = actorMessageLove;
    }

    public String getActorMessageLove()
    {
        return actorMessageLove;
    }
    public void setActorMessageSex(String actorMessageSex)
    {
        this.actorMessageSex = actorMessageSex;
    }

    public String getActorMessageSex()
    {
        return actorMessageSex;
    }
    public void setActorMessageBirth(Date actorMessageBirth)
    {
        this.actorMessageBirth = actorMessageBirth;
    }

    public Date getActorMessageBirth()
    {
        return actorMessageBirth;
    }
    public void setActorMessageAge(Long actorMessageAge)
    {
        this.actorMessageAge = actorMessageAge;
    }

    public Long getActorMessageAge()
    {
        return actorMessageAge;
    }
    public void setActorMessageRace(String actorMessageRace)
    {
        this.actorMessageRace = actorMessageRace;
    }

    public String getActorMessageRace()
    {
        return actorMessageRace;
    }
    public void setActorMessageHairColor(String actorMessageHairColor)
    {
        this.actorMessageHairColor = actorMessageHairColor;
    }

    public String getActorMessageHairColor()
    {
        return actorMessageHairColor;
    }
    public void setActorMessageFakeMilk(String actorMessageFakeMilk)
    {
        this.actorMessageFakeMilk = actorMessageFakeMilk;
    }

    public String getActorMessageFakeMilk()
    {
        return actorMessageFakeMilk;
    }
    public void setActorMessageSanwei(String actorMessageSanwei)
    {
        this.actorMessageSanwei = actorMessageSanwei;
    }

    public String getActorMessageSanwei()
    {
        return actorMessageSanwei;
    }
    public void setActorMessageStature(String actorMessageStature)
    {
        this.actorMessageStature = actorMessageStature;
    }

    public String getActorMessageStature()
    {
        return actorMessageStature;
    }
    public void setActorMessageWeight(String actorMessageWeight)
    {
        this.actorMessageWeight = actorMessageWeight;
    }

    public String getActorMessageWeight()
    {
        return actorMessageWeight;
    }
    public void setActorMessageSite(String actorMessageSite)
    {
        this.actorMessageSite = actorMessageSite;
    }

    public String getActorMessageSite()
    {
        return actorMessageSite;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("actorMessageId", getActorMessageId())
            .append("actorId", getActorId())
            .append("actorMessageEmotionalState", getActorMessageEmotionalState())
            .append("actorMessageLove", getActorMessageLove())
            .append("actorMessageSex", getActorMessageSex())
            .append("actorMessageBirth", getActorMessageBirth())
            .append("actorMessageAge", getActorMessageAge())
            .append("actorMessageRace", getActorMessageRace())
            .append("actorMessageHairColor", getActorMessageHairColor())
            .append("actorMessageFakeMilk", getActorMessageFakeMilk())
            .append("actorMessageSanwei", getActorMessageSanwei())
            .append("actorMessageStature", getActorMessageStature())
            .append("actorMessageWeight", getActorMessageWeight())
            .append("actorMessageSite", getActorMessageSite())
            .toString();
    }
}
