package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @TableName user_phone
 */
@TableName(value ="user_phone")
@Data
public class UserPhone implements Serializable {
    /**
     * 用户id
     */
    @TableId
    private Integer userUid;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户名称
     */
    private String username;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
