package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpChat;
import lombok.Data;

/**
 * 聊天信息
 */
@Data
public class UserChat {

    private String userName;
    private SpChat spChat;
}
