package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.PhoenixReport;
import lombok.Data;

@Data
public class UserPhoenixReport {
    private PhoenixReport phoenixReport;
    private String userName;
    private String phoenixName;
}
