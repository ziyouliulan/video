package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName sp_video_category_pattern
 */
@TableName(value ="sp_video_category_pattern")
@Data
public class SpVideoCategoryPattern implements Serializable {
    /**
     * 视频分类id
     */
    @TableId(type = IdType.AUTO)
    private Long videoCategoryId;

    /**
     * 判断位置(0 精选 ，1 标题)
     */
    private Long videoCategoryParentId;

    /**
     * 视频分类名称
     */
    private String videoCategoryName;

    /**
     * 分类权重（用于排序）
     */
    private Integer videoCategoryWeight;

    /**
     * 分类状态 （用于屏蔽分类 0显示  1不显示）
     */
    private Integer videoCategoryState;

    /**
     * 模式选择(0 模式1 ，1 模式2 2:演员综艺) 3:4种视频  4:漫画格式
     */
    private Integer videoCategoryUrl;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}