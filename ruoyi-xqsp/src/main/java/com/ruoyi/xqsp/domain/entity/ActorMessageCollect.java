package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

@Data
public class ActorMessageCollect {
    private Long userId;
    private Long actorId;
}
