package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 漫画标签对象 sp_comic_tag
 * 
 * @author ruoyi
 * @date 2021-05-24
 */
public class SpComicTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标签编号 */
    private Long comicTagId;

    /** 标签名字 */
    @Excel(name = "标签名字")
    private String comicTagName;

    public void setComicTagId(Long comicTagId) 
    {
        this.comicTagId = comicTagId;
    }

    public Long getComicTagId() 
    {
        return comicTagId;
    }
    public void setComicTagName(String comicTagName) 
    {
        this.comicTagName = comicTagName;
    }

    public String getComicTagName() 
    {
        return comicTagName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("comicTagId", getComicTagId())
            .append("comicTagName", getComicTagName())
            .toString();
    }
}
