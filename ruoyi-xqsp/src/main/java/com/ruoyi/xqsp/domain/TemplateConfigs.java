package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 视频分类
 * @TableName sys_template_configs
 */
@TableName(value ="sys_template_configs")
@Data
public class TemplateConfigs implements Serializable {
    /**
     * 分类id
     */
    @TableId(type = IdType.AUTO)
    private Long deptId;

    /**
     * 分类上级id
     */
    private Long parentId;

    /**
     * 分类列表
     */
    private String ancestors;

    /**
     * 分类名称
     */
    private String deptName;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 分类值
     */
    private String categoryTagValue;

    /**
     * 分类状态（0正常 1停用）
     */
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 
     */
    private String type;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}