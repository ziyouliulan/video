package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * APP状态对象 sp_state
 * 
 * @author ruoyi
 * @date 2021-06-10
 */
public class SpState extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long stateId;

    /** 状态值（0，正常，1下架） */
    @Excel(name = "状态值", readConverterExp = "0=，正常，1下架")
    private Integer stateValue;

    public void setStateId(Long stateId) 
    {
        this.stateId = stateId;
    }

    public Long getStateId() 
    {
        return stateId;
    }
    public void setStateValue(Integer stateValue) 
    {
        this.stateValue = stateValue;
    }

    public Integer getStateValue() 
    {
        return stateValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("stateId", getStateId())
            .append("stateValue", getStateValue())
            .toString();
    }
}
