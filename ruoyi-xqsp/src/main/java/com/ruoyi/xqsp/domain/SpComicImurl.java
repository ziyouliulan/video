package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * 
 * @TableName sp_comic_imurl
 */
@TableName(value ="sp_comic_imurl")
public class SpComicImurl implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 章节编号
     */
    private Long comicChapterId;

    /**
     *  漫画id
     */
    private Long comicId;

    /**
     * 章节目录
     */
    private Integer comicChapterDirectory;

    /**
     * 图片链接
     */
    private String comicImurl;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    public Integer getId() {
        return id;
    }

    /**
     * id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 章节编号
     */
    public Long getComicChapterId() {
        return comicChapterId;
    }

    /**
     * 章节编号
     */
    public void setComicChapterId(Long comicChapterId) {
        this.comicChapterId = comicChapterId;
    }

    /**
     *  漫画id
     */
    public Long getComicId() {
        return comicId;
    }

    /**
     *  漫画id
     */
    public void setComicId(Long comicId) {
        this.comicId = comicId;
    }

    /**
     * 章节目录
     */
    public Integer getComicChapterDirectory() {
        return comicChapterDirectory;
    }

    /**
     * 章节目录
     */
    public void setComicChapterDirectory(Integer comicChapterDirectory) {
        this.comicChapterDirectory = comicChapterDirectory;
    }

    /**
     * 图片链接
     */
    public String getComicImurl() {
        return comicImurl;
    }

    /**
     * 图片链接
     */
    public void setComicImurl(String comicImurl) {
        this.comicImurl = comicImurl;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SpComicImurl other = (SpComicImurl) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getComicChapterId() == null ? other.getComicChapterId() == null : this.getComicChapterId().equals(other.getComicChapterId()))
            && (this.getComicId() == null ? other.getComicId() == null : this.getComicId().equals(other.getComicId()))
            && (this.getComicChapterDirectory() == null ? other.getComicChapterDirectory() == null : this.getComicChapterDirectory().equals(other.getComicChapterDirectory()))
            && (this.getComicImurl() == null ? other.getComicImurl() == null : this.getComicImurl().equals(other.getComicImurl()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getComicChapterId() == null) ? 0 : getComicChapterId().hashCode());
        result = prime * result + ((getComicId() == null) ? 0 : getComicId().hashCode());
        result = prime * result + ((getComicChapterDirectory() == null) ? 0 : getComicChapterDirectory().hashCode());
        result = prime * result + ((getComicImurl() == null) ? 0 : getComicImurl().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", comicChapterId=").append(comicChapterId);
        sb.append(", comicId=").append(comicId);
        sb.append(", comicChapterDirectory=").append(comicChapterDirectory);
        sb.append(", comicImurl=").append(comicImurl);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}