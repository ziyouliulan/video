package com.ruoyi.xqsp.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 漫画评价对象 sp_comic_evaluation
 *
 * @author ruoyi
 * @date 2021-04-30
 */
@Data
public class SpComicEvaluation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 漫画评价编号 */
    private Long comicEvaluationId;

    /** 漫画id */
    @Excel(name = "漫画id")
    private Long comicId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 评价内容 */
    @Excel(name = "评价内容")
    private String comicEvaluationContent;
    /** 评价内容 */
    @Excel(name = "评价内容")
    @TableField(exist = false)
    private String userImg;

    /** 点赞数量 */
    @Excel(name = "点赞数量")
    private Long comicEvaluationLike;

    /**  评价时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = " 评价时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date comicEvaluationTime;

    public void setComicEvaluationId(Long comicEvaluationId)
    {
        this.comicEvaluationId = comicEvaluationId;
    }

    public Long getComicEvaluationId()
    {
        return comicEvaluationId;
    }
    public void setComicId(Long comicId)
    {
        this.comicId = comicId;
    }

    public Long getComicId()
    {
        return comicId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setComicEvaluationContent(String comicEvaluationContent)
    {
        this.comicEvaluationContent = comicEvaluationContent;
    }

    public String getComicEvaluationContent()
    {
        return comicEvaluationContent;
    }
    public void setComicEvaluationTime(Date comicEvaluationTime)
    {
        this.comicEvaluationTime = comicEvaluationTime;
    }

    public Date getComicEvaluationTime()
    {
        return comicEvaluationTime;
    }

    public void setComicEvaluationLike(Long comicEvaluationLike)
    {
        this.comicEvaluationLike = comicEvaluationLike;
    }

    public Long getComicEvaluationLike()
    {
        return comicEvaluationLike;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("comicEvaluationId", getComicEvaluationId())
                .append("comicId", getComicId())
                .append("userId", getUserId())
                .append("comicEvaluationContent", getComicEvaluationContent())
                .append("comicEvaluationTime", getComicEvaluationTime())
                .append("comicEvaluationLike", getComicEvaluationLike())
                .toString();
    }

    private Long evaluationLike;

    public Long getEvaluationLike() {
        return evaluationLike;
    }

    public void setEvaluationLike(Long evaluationLike) {
        this.evaluationLike = evaluationLike;
    }

    @TableField(exist = false)
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    private Long userHead;

    public Long getUserHead() {
        return userHead;
    }

    public void setUserHead(Long userHead) {
        this.userHead = userHead;
    }
}
