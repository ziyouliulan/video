//
//package com.ruoyi.xqsp.domain;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * @Description:TODO(世界动态实体类)
// *
// * @version: V1.0
// * @author: wlx
// *
// */
//@Data
//@ApiModel("TPublishs")
//@TableName(value = "t_publishs")
//@AllArgsConstructor
//@NoArgsConstructor
//public class Publishs implements Serializable {
//
//	private static final long serialVersionUID = 1606794166973L;
//
//    @TableId(value = "id", type = IdType.AUTO)
//    @ApiModelProperty(name = "id", value = "id", hidden = true)
//	private Long id;
//
//    @ApiModelProperty(name = "content", value = "动态内容")
//	private String content;
//
//    @ApiModelProperty(name = "userId", value = "发布的用户ID")
//	private Long userId;
//
//    @ApiModelProperty(name = "type", value = "动态类型（1  文字 + 图片   2 文字 + 视频  3文字）")
//	private Integer type;
//
//    @ApiModelProperty(name = "picUrl", value = "图片url，多张已逗号分隔")
//	private String picUrl;
//
//    @ApiModelProperty(name = "videoUrl", value = "视频url")
//	private String videoUrl;
//
//    @ApiModelProperty(name = "videoCoverUrl", value = "视频封面url")
//	private String videoCoverUrl;
//
//    @ApiModelProperty(name = "regionCode", value = "地区编码")
//	private String regionCode;
//
//    @ApiModelProperty(name = "regionName", value = "地区名称")
//	private String regionName;
//
//
//	@ApiModelProperty(name = "region", value = "状态")
//	private Integer noSeeStatus;
//
//
//	@ApiModelProperty(name = "goodsId", value = "商品id")
//	private Integer goodsId;
//
////
////	@ApiModelProperty(name = "goodsId", value = "商品id")
////	private Integer goodsName;
////
////
////	@ApiModelProperty(name = "goodsId", value = "商品id")
////	private Integer shopId;
////
//
//
//
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//    @ApiModelProperty(name = "createTime", value = "发布时间")
//	private Date createTime;
//
//
//}
