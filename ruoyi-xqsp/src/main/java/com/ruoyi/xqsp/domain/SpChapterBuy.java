package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 章节购买对象 sp_chapter_buy
 * 
 * @author ruoyi
 * @date 2021-05-22
 */
public class SpChapterBuy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long chapterBuyId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 章节目录 */
    @Excel(name = "章节目录")
    private Long comicChapterDirectory;

    /** 漫画id */
    @Excel(name = "漫画id")
    private Long comicId;

    public void setChapterBuyId(Long chapterBuyId) 
    {
        this.chapterBuyId = chapterBuyId;
    }

    public Long getChapterBuyId() 
    {
        return chapterBuyId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setComicChapterDirectory(Long comicChapterDirectory) 
    {
        this.comicChapterDirectory = comicChapterDirectory;
    }

    public Long getComicChapterDirectory() 
    {
        return comicChapterDirectory;
    }
    public void setComicId(Long comicId) 
    {
        this.comicId = comicId;
    }

    public Long getComicId() 
    {
        return comicId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chapterBuyId", getChapterBuyId())
            .append("userId", getUserId())
            .append("comicChapterDirectory", getComicChapterDirectory())
            .append("comicId", getComicId())
            .toString();
    }
}
