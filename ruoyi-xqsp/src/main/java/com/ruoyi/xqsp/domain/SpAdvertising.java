package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 广告位管理对象 sp_advertising
 * 
 * @author ruoyi
 * @date 2021-04-17
 */
public class SpAdvertising extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 广告编号 */
    private Long advertisingId;

    /** 广告名字 */
    @Excel(name = "广告名字")
    private String advertisingName;

    /** 点击次数 */
    @Excel(name = "点击次数")
    private Long advertisingClick;

    /** 广告图片 */
    @Excel(name = "广告图片")
    private String advertisingImg;

    /**  广告链接 */
    @Excel(name = " 广告链接")
    private String advertisingLink;

    /** 广告状态 （是否显示0.显示 1不显示） */
    @Excel(name = "广告状态 ", readConverterExp = "是=否显示0.显示,1=不显示")
    private Integer advertisingState;

    /** 链接id（视频，或综艺视频） */
    @Excel(name = "链接id", readConverterExp = "视=频，或综艺视频")
    private Long advertisingTypeId;

    /** (类型：1.banner图 2.外部广告 3.视频广告 4.综艺视频广告) */
    @Excel(name = "(类型：1.banner图 2.外部广告 3.视频广告 4.综艺视频广告)")
    private Integer advertisingType;

    /** 视频分类id */
    @Excel(name = "视频分类id")
    private Long videoCategoryId;

    /** 精选页banner图（0.是 ，1.否） */
    @Excel(name = "精选页banner图", readConverterExp = "0=.是,，=1.否")
    private Integer advertisingHome;

    /** banner图类型（1首页分类2楼凤3漫画4动漫） */
    @Excel(name = "banner图类型", readConverterExp = "1=首页分类2楼凤3漫画4动漫")
    private Integer bannerType;

    public void setAdvertisingId(Long advertisingId)
    {
        this.advertisingId = advertisingId;
    }

    public Long getAdvertisingId()
    {
        return advertisingId;
    }
    public void setAdvertisingName(String advertisingName)
    {
        this.advertisingName = advertisingName;
    }

    public String getAdvertisingName()
    {
        return advertisingName;
    }
    public void setAdvertisingClick(Long advertisingClick)
    {
        this.advertisingClick = advertisingClick;
    }

    public Long getAdvertisingClick()
    {
        return advertisingClick;
    }
    public void setAdvertisingImg(String advertisingImg)
    {
        this.advertisingImg = advertisingImg;
    }

    public String getAdvertisingImg()
    {
        return advertisingImg;
    }
    public void setAdvertisingLink(String advertisingLink)
    {
        this.advertisingLink = advertisingLink;
    }

    public String getAdvertisingLink()
    {
        return advertisingLink;
    }
    public void setAdvertisingState(Integer advertisingState)
    {
        this.advertisingState = advertisingState;
    }

    public Integer getAdvertisingState()
    {
        return advertisingState;
    }
    public void setAdvertisingTypeId(Long advertisingTypeId)
    {
        this.advertisingTypeId = advertisingTypeId;
    }

    public Long getAdvertisingTypeId()
    {
        return advertisingTypeId;
    }
    public void setAdvertisingType(Integer advertisingType)
    {
        this.advertisingType = advertisingType;
    }

    public Integer getAdvertisingType()
    {
        return advertisingType;
    }
    public void setVideoCategoryId(Long videoCategoryId)
    {
        this.videoCategoryId = videoCategoryId;
    }

    public Long getVideoCategoryId()
    {
        return videoCategoryId;
    }
    public void setAdvertisingHome(Integer advertisingHome)
    {
        this.advertisingHome = advertisingHome;
    }

    public Integer getAdvertisingHome()
    {
        return advertisingHome;
    }
    public void setBannerType(Integer bannerType)
    {
        this.bannerType = bannerType;
    }

    public Integer getBannerType()
    {
        return bannerType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("advertisingId", getAdvertisingId())
                .append("advertisingName", getAdvertisingName())
                .append("advertisingClick", getAdvertisingClick())
                .append("advertisingImg", getAdvertisingImg())
                .append("advertisingLink", getAdvertisingLink())
                .append("advertisingState", getAdvertisingState())
                .append("advertisingTypeId", getAdvertisingTypeId())
                .append("advertisingType", getAdvertisingType())
                .append("videoCategoryId", getVideoCategoryId())
                .append("advertisingHome", getAdvertisingHome())
                .append("bannerType", getBannerType())
                .toString();
    }
}
