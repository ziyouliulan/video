package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalV2Serializer;
import com.quyang.voice.utils.CustomerDoubleV2Serialize;
import lombok.Data;

/**
 *
 * @TableName sp_gold_list
 */
@TableName(value ="sp_gold_list")
@Data
public class GoldList implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     *
     */
    private String name;

    /**
     * 人民币
     */
    @JsonSerialize(using = BigDecimalV2Serializer.class)
    private BigDecimal money;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 金币
     */
    private Long gold;

    /**
     * 是否显示
     */
    private Integer isShow;

    /**
     * 是否删除
     */
    private Integer isDel;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
