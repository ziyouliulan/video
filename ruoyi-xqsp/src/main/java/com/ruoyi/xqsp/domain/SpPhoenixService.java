package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 楼凤操作对象 sp_phoenix_service
 * 
 * @author ruoyi
 * @date 2021-04-26
 */
public class SpPhoenixService extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 楼凤id */
    private Long phoenixId;

    /** 楼凤名 */
    @Excel(name = "楼凤名")
    private String phoenixName;

    /** 标题信息 */
    @Excel(name = "标题信息")
    private String phoenixTitle;

    /** 楼凤类型 */
    @Excel(name = "楼凤类型")
    private String phoenixType;

    /** 楼凤数量 */
    @Excel(name = "楼凤数量")
    private String phoenixNumber;

    /** 楼凤年龄 */
    @Excel(name = "楼凤年龄")
    private String phoenixAge;

    /** 楼凤职务 */
    @Excel(name = "楼凤职务")
    private String phoenixWork;

    /** 服务项目 */
    @Excel(name = "服务项目")
    private String phoenixServicesAvailable;

    /** 楼凤价格 */
    @Excel(name = "楼凤价格")
    private String phoenixPrice;

    /** 楼凤服务时间 */
    @Excel(name = "楼凤服务时间")
    private String phoenixServiceTime;

    /** 楼凤地址 */
    @Excel(name = "楼凤地址")
    private String phoenixAddress;

    /** 楼凤详情介绍 */
    @Excel(name = "楼凤详情介绍")
    private String phoenixDetails;

    /**  楼凤评分 */
    @Excel(name = " 楼凤评分")
    private Long phoenixScore;

    /** 楼凤观看人数 */
    @Excel(name = "楼凤观看人数")
    private Long phoenixWatchNumber;

    /** 楼凤点赞人数 */
    @Excel(name = "楼凤点赞人数")
    private Long phoenixLikeNumber;

    /** 解锁金币数 */
    @Excel(name = "解锁金币数")
    private Long phoenixUnlockGold;

    /** 楼凤上传图片 */
    @Excel(name = "楼凤上传图片")
    private String phoenixImg;

    /** 楼凤状态(0正常，1锁定) */
    @Excel(name = "楼凤状态(0正常，1锁定)")
    private String phoenixState;

    /** 楼凤审核（0 未同意，1已同意） */
    @Excel(name = "楼凤审核", readConverterExp = "0=,未=同意，1已同意")
    private String phoenixAudit;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date phoenixTime;

    /** 楼风解锁人数 */
    @Excel(name = "楼风解锁人数")
    private Long phoenixUnlockNumber;
    /** 楼凤联系方式 */
    @Excel(name = "楼凤联系方式")
    private String phoenixPhone;

    /** 楼凤详细地址 */
    @Excel(name = "楼凤详细地址")
    private String phoenixDetailedAddress;

    public void setPhoenixId(Long phoenixId)
    {
        this.phoenixId = phoenixId;
    }

    public Long getPhoenixId()
    {
        return phoenixId;
    }
    public void setPhoenixName(String phoenixName)
    {
        this.phoenixName = phoenixName;
    }

    public String getPhoenixName()
    {
        return phoenixName;
    }
    public void setPhoenixTitle(String phoenixTitle)
    {
        this.phoenixTitle = phoenixTitle;
    }

    public String getPhoenixTitle()
    {
        return phoenixTitle;
    }
    public void setPhoenixType(String phoenixType)
    {
        this.phoenixType = phoenixType;
    }

    public String getPhoenixType()
    {
        return phoenixType;
    }
    public void setPhoenixNumber(String phoenixNumber)
    {
        this.phoenixNumber = phoenixNumber;
    }

    public String getPhoenixNumber()
    {
        return phoenixNumber;
    }
    public void setPhoenixAge(String phoenixAge)
    {
        this.phoenixAge = phoenixAge;
    }

    public String getPhoenixAge()
    {
        return phoenixAge;
    }
    public void setPhoenixWork(String phoenixWork)
    {
        this.phoenixWork = phoenixWork;
    }

    public String getPhoenixWork()
    {
        return phoenixWork;
    }
    public void setPhoenixServicesAvailable(String phoenixServicesAvailable)
    {
        this.phoenixServicesAvailable = phoenixServicesAvailable;
    }

    public String getPhoenixServicesAvailable()
    {
        return phoenixServicesAvailable;
    }
    public void setPhoenixPrice(String phoenixPrice)
    {
        this.phoenixPrice = phoenixPrice;
    }

    public String getPhoenixPrice()
    {
        return phoenixPrice;
    }
    public void setPhoenixServiceTime(String phoenixServiceTime)
    {
        this.phoenixServiceTime = phoenixServiceTime;
    }

    public String getPhoenixServiceTime()
    {
        return phoenixServiceTime;
    }
    public void setPhoenixAddress(String phoenixAddress)
    {
        this.phoenixAddress = phoenixAddress;
    }

    public String getPhoenixAddress()
    {
        return phoenixAddress;
    }
    public void setPhoenixDetails(String phoenixDetails)
    {
        this.phoenixDetails = phoenixDetails;
    }

    public String getPhoenixDetails()
    {
        return phoenixDetails;
    }
    public void setPhoenixScore(Long phoenixScore)
    {
        this.phoenixScore = phoenixScore;
    }

    public Long getPhoenixScore()
    {
        return phoenixScore;
    }
    public void setPhoenixWatchNumber(Long phoenixWatchNumber)
    {
        this.phoenixWatchNumber = phoenixWatchNumber;
    }

    public Long getPhoenixWatchNumber()
    {
        return phoenixWatchNumber;
    }
    public void setPhoenixLikeNumber(Long phoenixLikeNumber)
    {
        this.phoenixLikeNumber = phoenixLikeNumber;
    }

    public Long getPhoenixLikeNumber()
    {
        return phoenixLikeNumber;
    }
    public void setPhoenixUnlockGold(Long phoenixUnlockGold)
    {
        this.phoenixUnlockGold = phoenixUnlockGold;
    }

    public Long getPhoenixUnlockGold()
    {
        return phoenixUnlockGold;
    }
    public void setPhoenixImg(String phoenixImg)
    {
        this.phoenixImg = phoenixImg;
    }

    public String getPhoenixImg()
    {
        return phoenixImg;
    }
    public void setPhoenixState(String phoenixState)
    {
        this.phoenixState = phoenixState;
    }

    public String getPhoenixState()
    {
        return phoenixState;
    }
    public void setPhoenixAudit(String phoenixAudit)
    {
        this.phoenixAudit = phoenixAudit;
    }

    public String getPhoenixAudit()
    {
        return phoenixAudit;
    }
    public void setPhoenixTime(Date phoenixTime)
    {
        this.phoenixTime = phoenixTime;
    }

    public Date getPhoenixTime()
    {
        return phoenixTime;
    }
    public void setPhoenixUnlockNumber(Long phoenixUnlockNumber)
    {
        this.phoenixUnlockNumber = phoenixUnlockNumber;
    }

    public Long getPhoenixUnlockNumber()
    {
        return phoenixUnlockNumber;
    }
    public void setPhoenixPhone(String phoenixPhone)
    {
        this.phoenixPhone = phoenixPhone;
    }

    public String getPhoenixPhone()
    {
        return phoenixPhone;
    }
    public void setPhoenixDetailedAddress(String phoenixDetailedAddress)
    {
        this.phoenixDetailedAddress = phoenixDetailedAddress;
    }

    public String getPhoenixDetailedAddress()
    {
        return phoenixDetailedAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("phoenixId", getPhoenixId())
                .append("phoenixName", getPhoenixName())
                .append("phoenixTitle", getPhoenixTitle())
                .append("phoenixType", getPhoenixType())
                .append("phoenixNumber", getPhoenixNumber())
                .append("phoenixAge", getPhoenixAge())
                .append("phoenixWork", getPhoenixWork())
                .append("phoenixServicesAvailable", getPhoenixServicesAvailable())
                .append("phoenixPrice", getPhoenixPrice())
                .append("phoenixServiceTime", getPhoenixServiceTime())
                .append("phoenixAddress", getPhoenixAddress())
                .append("phoenixDetails", getPhoenixDetails())
                .append("phoenixScore", getPhoenixScore())
                .append("phoenixWatchNumber", getPhoenixWatchNumber())
                .append("phoenixLikeNumber", getPhoenixLikeNumber())
                .append("phoenixUnlockGold", getPhoenixUnlockGold())
                .append("phoenixImg", getPhoenixImg())
                .append("phoenixState", getPhoenixState())
                .append("phoenixAudit", getPhoenixAudit())
                .append("phoenixTime", getPhoenixTime())
                .append("phoenixUnlockNumber", getPhoenixUnlockNumber())
                .append("phoenixPhone", getPhoenixPhone())
                .append("phoenixDetailedAddress", getPhoenixDetailedAddress())
                .toString();
    }
}
