package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 前台充值记录查询
 */
@Data
public class RechargeRecord {
    private Long userId;
    private Long rechargeRecordWay;//充值类型  0全部（1自主2客服）
    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数
}
