package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 系统通知表对象 sp_inform
 * 
 * @author ruoyi
 * @date 2021-04-21
 */
public class SpInform extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long informId;

    /** 系统通知内容 */
    @Excel(name = "系统通知内容")
    private String informContent;

    /** 通知时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "通知时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date informTime;

    public void setInformId(Long informId) 
    {
        this.informId = informId;
    }

    public Long getInformId() 
    {
        return informId;
    }
    public void setInformContent(String informContent) 
    {
        this.informContent = informContent;
    }

    public String getInformContent() 
    {
        return informContent;
    }
    public void setInformTime(Date informTime) 
    {
        this.informTime = informTime;
    }

    public Date getInformTime() 
    {
        return informTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("informId", getInformId())
            .append("informContent", getInformContent())
            .append("informTime", getInformTime())
            .toString();
    }
}
