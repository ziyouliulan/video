package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 视频评价对象 sp_video_evaluation
 * 
 * @author ruoyi
 * @date 2021-04-24
 */
public class SpVideoEvaluation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  视频评价id */
    private Long videoEvaluationId;

    /** 视频id（视频表） */
    @Excel(name = "视频id", readConverterExp = "视=频表")
    private Long videoId;

    /** 用户id(用户表) */
    @Excel(name = "用户id(用户表)")
    private Long userId;

    /** 评价内容 */
    @Excel(name = "评价内容")
    private String videoEvaluationContent;

    /** 评价时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "评价时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date videoEvaluationTime;


    /** 点赞数量 */
    @Excel(name = "点赞数量")
    private Long videoEvaluationLike;

    public void setVideoEvaluationId(Long videoEvaluationId) 
    {
        this.videoEvaluationId = videoEvaluationId;
    }

    public Long getVideoEvaluationId() 
    {
        return videoEvaluationId;
    }
    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setVideoEvaluationContent(String videoEvaluationContent) 
    {
        this.videoEvaluationContent = videoEvaluationContent;
    }

    public String getVideoEvaluationContent() 
    {
        return videoEvaluationContent;
    }
    public void setVideoEvaluationTime(Date videoEvaluationTime) 
    {
        this.videoEvaluationTime = videoEvaluationTime;
    }

    public Date getVideoEvaluationTime() 
    {
        return videoEvaluationTime;
    }
    public void setVideoEvaluationLike(Long videoEvaluationLike)
    {
        this.videoEvaluationLike = videoEvaluationLike;
    }

    public Long getVideoEvaluationLike()
    {
        return videoEvaluationLike;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("videoEvaluationId", getVideoEvaluationId())
                .append("videoId", getVideoId())
                .append("userId", getUserId())
                .append("videoEvaluationContent", getVideoEvaluationContent())
                .append("videoEvaluationTime", getVideoEvaluationTime())
                .append("videoEvaluationLike", getVideoEvaluationLike())
                .toString();
    }
}
