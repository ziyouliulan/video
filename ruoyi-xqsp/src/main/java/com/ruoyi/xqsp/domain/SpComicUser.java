package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 书架管理对象 sp_comic_user
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
public class SpComicUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long comicUserId;

    /** 漫画id */
    @Excel(name = "漫画id")
    private Long comicId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 加入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "加入时间", width = 30, dateFormat = "yyyy-MM-dd ")
    private Date comicUserTime;

    public void setComicUserId(Long comicUserId) 
    {
        this.comicUserId = comicUserId;
    }

    public Long getComicUserId() 
    {
        return comicUserId;
    }
    public void setComicId(Long comicId) 
    {
        this.comicId = comicId;
    }

    public Long getComicId() 
    {
        return comicId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setComicUserTime(Date comicUserTime) 
    {
        this.comicUserTime = comicUserTime;
    }

    public Date getComicUserTime() 
    {
        return comicUserTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("comicUserId", getComicUserId())
            .append("comicId", getComicId())
            .append("userId", getUserId())
            .append("comicUserTime", getComicUserTime())
            .toString();
    }

    private SpComic spComic;

    public SpComic getSpComic() {
        return spComic;
    }

    public void setSpComic(SpComic spComic) {
        this.spComic = spComic;
    }
}
