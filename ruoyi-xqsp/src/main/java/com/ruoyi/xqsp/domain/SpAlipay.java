package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 支付宝支付对象 sp_alipay
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public class SpAlipay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long alipayId;

    /** 商户appid */
    @Excel(name = "商户appid")
    private String appid;

    /** 应用私钥 */
    @Excel(name = "应用私钥")
    private String rsaPrivateKey;

    /** 应用公钥 */
    @Excel(name = "应用公钥")
    private String alipayPublicKey;

    /** 支付宝公钥 */
    @Excel(name = "支付宝公钥")
    private String zfbPublicKey;

    public void setAlipayId(Long alipayId) 
    {
        this.alipayId = alipayId;
    }

    public Long getAlipayId() 
    {
        return alipayId;
    }
    public void setAppid(String appid) 
    {
        this.appid = appid;
    }

    public String getAppid() 
    {
        return appid;
    }
    public void setRsaPrivateKey(String rsaPrivateKey) 
    {
        this.rsaPrivateKey = rsaPrivateKey;
    }

    public String getRsaPrivateKey() 
    {
        return rsaPrivateKey;
    }
    public void setAlipayPublicKey(String alipayPublicKey) 
    {
        this.alipayPublicKey = alipayPublicKey;
    }

    public String getAlipayPublicKey() 
    {
        return alipayPublicKey;
    }
    public void setZfbPublicKey(String zfbPublicKey) 
    {
        this.zfbPublicKey = zfbPublicKey;
    }

    public String getZfbPublicKey() 
    {
        return zfbPublicKey;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("alipayId", getAlipayId())
            .append("appid", getAppid())
            .append("rsaPrivateKey", getRsaPrivateKey())
            .append("alipayPublicKey", getAlipayPublicKey())
            .append("zfbPublicKey", getZfbPublicKey())
            .toString();
    }
}
