package com.ruoyi.xqsp.domain.entity;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.xqsp.domain.SpUsers;
import lombok.Data;

import java.util.Date;

@Data
public class User extends SpUsers {


//        private static final long serialVersionUID = 1L;
//
//        /**  用户id */
//        private Long userId;
//
//        /** 用户名称 */
//        @Excel(name = "用户名称")
//        private String userName;
//
//        /** 用户密码 */
//        @Excel(name = "用户密码")
//        private String userPassword;
//
//        /** 用户手机序列号（唯一） */
//        @Excel(name = "用户手机序列号", readConverterExp = "唯=一")
//        private String userApi;
//
//        /** 用户手机号 */
//        @Excel(name = "用户手机号")
//        private String userPhone;
//
//        /** 用户权限 */
//        @Excel(name = "用户权限")
//        private String userPermissions;
//
//        /** 用户设备 */
//        @Excel(name = "用户设备")
//        private String userEquipment;
//
//        /** 上级用户 */
//        @Excel(name = "上级用户")
//        private String userSuperior;
//
//        /** 下级用户 */
//        @Excel(name = "下级用户")
//        private String userUnder;
//
//        /** 推广码 */
//        @Excel(name = "推广码")
//        private String userPromoteCode;
//
//        /** 推广人数 */
//        @Excel(name = "推广人数")
//        private Long userPromotePeople;
//
//        /** 金币数 */
//        @Excel(name = "金币数")
//        private Long userGlod;
//
//        /** 用户状态(用来封停用户) */
//        @Excel(name = "用户状态(用来封停用户)")
//        private String userState;
//
//        /** 用户评论（用来禁言） */
//        @Excel(name = "用户评论", readConverterExp = "用=来禁言")
//        private String userComments;
//
//        /** 星球ID */
//        @Excel(name = "星球ID")
//        private String userXqid;
//
//        /**  用户头像 */
//        @Excel(name = " 用户头像")
//        private Long userImg;
//
//        /** 剩余会员天数 */
//        @Excel(name = "剩余会员天数")
//        private Long userMembersDay;
//
//        /** 今天观看次数 */
//        @Excel(name = "今天观看次数")
//        private Long userToday;
//
//        /** 福利观看次数 */
//
//        private Long userWelfare;
//
//        /** 卡密 */
//
//        private String userCarmichael;
//
//        /** 创建时间 */
//
//        private Date userTime;
//
//        /** 漫画自动购买类型（0手动，1自动） */
//          private Integer userBuyType;
//


    private Long userMoney = 0L;//用户充值金额

    private Integer userVipType = 0;//用户的vip类型 0:普通(月卡,季卡) 1:半年卡 2:年卡

}
