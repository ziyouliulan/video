package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.domain.SpVarietyVideoEvaluation;
import lombok.Data;

/**
 * 综艺视频的点赞情况
 */
@Data
public class VarietyVideoEvaluationLike {
    private SpVarietyVideoEvaluation spVarietyVideoEvaluation;
    private Integer like;
    private Long userId;// 用户id
    private Long varietyVideoId;  //综艺视频id
    private String spUsers;//用户信息
    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数

}
