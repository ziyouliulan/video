package com.ruoyi.xqsp.domain;

import lombok.Data;

@Data
public class ActorVideo {
    private SpVideo spVideo;
    private String actorName;
    private String videoCategoryName;
}
