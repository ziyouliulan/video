package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 漫画标签中间表对象 sp_comic_and_tag
 * 
 * @author ruoyi
 * @date 2021-05-24
 */
public class SpComicAndTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long comicAndTagId;

    /** 漫画id */
    @Excel(name = "漫画id")
    private Long comicId;

    /** 标签编号 */
    @Excel(name = "标签编号")
    private Long comicTagId;

    public void setComicAndTagId(Long comicAndTagId) 
    {
        this.comicAndTagId = comicAndTagId;
    }

    public Long getComicAndTagId() 
    {
        return comicAndTagId;
    }
    public void setComicId(Long comicId) 
    {
        this.comicId = comicId;
    }

    public Long getComicId() 
    {
        return comicId;
    }
    public void setComicTagId(Long comicTagId) 
    {
        this.comicTagId = comicTagId;
    }

    public Long getComicTagId() 
    {
        return comicTagId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("comicAndTagId", getComicAndTagId())
            .append("comicId", getComicId())
            .append("comicTagId", getComicTagId())
            .toString();
    }
    private Long[] value1;

    public Long[] getValue1() {
        return value1;
    }

    public void setValue1(Long[] value1) {
        this.value1 = value1;
    }
}
