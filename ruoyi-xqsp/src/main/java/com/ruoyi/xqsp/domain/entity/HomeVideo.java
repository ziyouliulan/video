package com.ruoyi.xqsp.domain.entity;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalSerializer;
import com.ruoyi.xqsp.domain.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 前台视频详情
 */
@Data
public class HomeVideo {
    private Long userId;// 用户id
    private Long videoId;  //视频id
    private SpVideo spVideo;// 视频信息
    private List<SpVideo> spVideoList; //猜你喜欢（同一分类下的视频随意拿六个）
    private SpUsers spUsers;//用户信息
    private Long spLike;//点赞情况（已点，未点）
    private Long spUnlike;//差评情况
    private Long spCollect; //用户收藏
    private Long spUnlock;//金币解锁状态
    private Long videoEvaluationCounts;// 视频评论的数量
    private Integer unlockCount;  //视频金币解锁人数
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal favorableRate;  //视频金币解锁人数

}
