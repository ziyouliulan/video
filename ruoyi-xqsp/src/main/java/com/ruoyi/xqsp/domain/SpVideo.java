package com.ruoyi.xqsp.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalSerializer;
import com.quyang.voice.utils.CustomerDoubleSerialize;
import com.quyang.voice.utils.CustomerDoubleV2Serialize;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 视频管理对象 sp_video
 *
 * @author ruoyi
 * @date 2021-04-23
 */
@TableName(value ="sp_video")
@Data
public class SpVideo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 视频编号
     */
    @TableId(type = IdType.AUTO)
    private Long videoId;

    /**
     * 视频名称
     */
    @Excel(name = "视频名称")
    private String videoName;

    /**
     * 视频封面
     */
    @Excel(name = "视频封面")
    private String videoCoverImg;

    /**
     * 演员id
     */
    @Excel(name = "演员id")
    private Long actorId;

    /**
     * 视频介绍
     */
    @Excel(name = "视频介绍")
    private String videoIntroduce;

    /**
     * 观看人数
     */
    @Excel(name = "观看人数")
    private Long videoWatchNumber;

    /**
     * 点赞人数
     */
    @Excel(name = "点赞人数")
    private Long videoLikeNumber;

    /**
     * 付费设置
     */
    @Excel(name = "付费设置")
    private Long videoUnlock;

    /**
     * 视频分类id
     */
    @Excel(name = "视频分类id")
    private Long videoCategoryId;

    /**
     * 是否上架
     */
    @Excel(name = "是否上架")
    private String videoPutaway;

    /**
     * 清晰度设置
     */
    @Excel(name = "清晰度设置")
    private String videoDefinition;

    /**
     * 解锁所需金币数
     */
    @Excel(name = "解锁所需金币数")
    private BigDecimal videoUnlockGlod;

    /**
     * 上传时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "上传时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date videoTime;

    /**
     * 视频地址
     */
    @Excel(name = "视频地址")
    private String videoUrl;
    /**
     * 差评人数
     */
    @Excel(name = "差评人数")
    private Long videoUnlikeNumber;

    /**
     * 收藏人数
     */
    @Excel(name = "收藏人数")
    private Long videoCollectNumber;

    /**
     * 视频作者
     */
    @Excel(name = "视频作者")
    private String videoAuthor;

    /**
     * 视频类型（1.视频，2.动漫）
     */
    @Excel(name = "视频类型", readConverterExp = "1=.视频，2.动漫")
    private Integer videoType;
    /**
     * 视频预览
     */
    @Excel(name = "视频预览")
    private String videoUrlPreview;
    /**
     * 视频时长
     */
    @Excel(name = "视频时长")
    private String videoDuration;
    /**
     * 视频时长
     */
    @Excel(name = "评分")
    @JsonSerialize(using = CustomerDoubleV2Serialize.class)
    private Double videoScore;

    private Boolean isTop;
    /**
     * 置顶时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "置顶时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date isTopDate;


    /**
     * 解锁人数
     */
    @Excel(name = "解锁人数")
    private Integer unlockCount = 0;  //视频金币解锁人数

    public Integer getUnlockCount() {
        return unlockCount;
    }

    public void setUnlockCount(Integer unlockCount) {
        this.unlockCount = unlockCount;
    }


    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoUrlPreview() {
        return videoUrlPreview;
    }

    public void setVideoUrlPreview(String videoUrlPreview) {
        this.videoUrlPreview = videoUrlPreview;
    }

    public String getVideoAuthor() {
        return videoAuthor;
    }


    public Integer getVideoType() {
        return videoType;
    }

    public void setVideoType(Integer videoType) {
        this.videoType = videoType;
    }

    public void setVideoAuthor(String videoAuthor) {
        this.videoAuthor = videoAuthor;
    }

    public void setVideoId(Long videoId) {
        this.videoId = videoId;
    }

    public Long getVideoId() {
        return videoId;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoCoverImg(String videoCoverImg) {
        this.videoCoverImg = videoCoverImg;
    }

    public String getVideoCoverImg() {
        return videoCoverImg;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public Long getActorId() {
        return actorId;
    }

    public void setVideoIntroduce(String videoIntroduce) {
        this.videoIntroduce = videoIntroduce;
    }

    public String getVideoIntroduce() {
        return videoIntroduce;
    }

    public void setVideoWatchNumber(Long videoWatchNumber) {
        this.videoWatchNumber = videoWatchNumber;
    }

    public Long getVideoWatchNumber() {
        return videoWatchNumber;
    }

    public void setVideoLikeNumber(Long videoLikeNumber) {
        this.videoLikeNumber = videoLikeNumber;
    }

    public Long getVideoLikeNumber() {
        return videoLikeNumber;
    }

    public void setVideoUnlock(Long videoUnlock) {
        this.videoUnlock = videoUnlock;
    }

    public Long getVideoUnlock() {
        return videoUnlock;
    }

    public void setVideoCategoryId(Long videoCategoryId) {
        this.videoCategoryId = videoCategoryId;
    }

    public Long getVideoCategoryId() {
        return videoCategoryId;
    }

    public void setVideoPutaway(String videoPutaway) {
        this.videoPutaway = videoPutaway;
    }

    public String getVideoPutaway() {
        return videoPutaway;
    }

    public void setVideoDefinition(String videoDefinition) {
        this.videoDefinition = videoDefinition;
    }

    public String getVideoDefinition() {
        return videoDefinition;
    }

    public void setVideoUnlockGlod(BigDecimal videoUnlockGlod) {
        this.videoUnlockGlod = videoUnlockGlod;
    }

    public BigDecimal getVideoUnlockGlod() {
        return videoUnlockGlod;
    }

    public void setVideoTime(Date videoTime) {
        this.videoTime = videoTime;
    }

    public Date getVideoTime() {
        return videoTime;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUnlikeNumber(Long videoUnlikeNumber) {
        this.videoUnlikeNumber = videoUnlikeNumber;
    }

    public Long getVideoUnlikeNumber() {
        return videoUnlikeNumber;
    }

    public void setVideoCollectNumber(Long videoCollectNumber) {
        this.videoCollectNumber = videoCollectNumber;
    }

    public Long getVideoCollectNumber() {
        return videoCollectNumber;
    }

    @Override
    public String toString() {
        return "SpVideo{" +
                "videoId=" + videoId +
                ", videoName='" + videoName + '\'' +
                ", videoCoverImg='" + videoCoverImg + '\'' +
                ", actorId=" + actorId +
                ", videoIntroduce='" + videoIntroduce + '\'' +
                ", videoWatchNumber=" + videoWatchNumber +
                ", videoLikeNumber=" + videoLikeNumber +
                ", videoUnlock=" + videoUnlock +
                ", videoCategoryId=" + videoCategoryId +
                ", videoPutaway='" + videoPutaway + '\'' +
                ", videoDefinition='" + videoDefinition + '\'' +
                ", videoUnlockGlod=" + videoUnlockGlod +
                ", videoTime=" + videoTime +
                ", videoUrl='" + videoUrl + '\'' +
                ", videoUnlikeNumber=" + videoUnlikeNumber +
                ", videoCollectNumber=" + videoCollectNumber +
                ", videoAuthor='" + videoAuthor + '\'' +
                ", videoType=" + videoType +
                '}';
    }

}
