package com.ruoyi.xqsp.domain;

import lombok.Data;



@Data
public class UserLoginReq {
    private String userPhone;// 手机号
    private String userName;// 手机号
    private String userPassword;//密码
    private  String verificationCode;//验证码
    private String userApi;//手机序列号
    private String userPromoteCode;//邀请码（也就是邀请用户那个用户的xqid）
    private String userXqid;  //用户的星球id（唯一）
    private String userEquipment;  //用户的星球id（唯一）

}
