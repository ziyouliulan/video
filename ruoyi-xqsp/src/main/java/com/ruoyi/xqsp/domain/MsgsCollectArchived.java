package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 用户聊天记录表（存档）。超时后的用户聊天记录，将自动转储到本表，防止用户的消息记录表热数据过

收集用户消息的目的是有助于分析用户行为，用户消息本身对公司而言没有多大意义且违背用户隐私条款，目前先这样吧，以后或需停止收集。
 * @TableName msgs_collect_archived
 */
@TableName(value ="msgs_collect_archived")
@Data
public class MsgsCollectArchived implements Serializable {
    /**
     * 记录ID
     */
    @TableId(type = IdType.AUTO)
    private Integer collectId;

    /**
     * 发送人的ID
     */
    private String srcUid;

    /**
     * 接收人的ID
     */
    private String destUid;

    /**
     * 聊天模式类型：
0 正式一对一聊天；
1 临时一对一聊天；
2 群聊或世界频道聊天（当为本类型时，group_id字段不可为空，且当group_id=-1时表示群聊！）
 注：当聊天模式为群聊时，dest_uid字段值为消息发生的群组id哦。
     */
    private Integer chatType;

    /**
     * 聊天消息类型：
0 普通文本消息
1 图片消息
2 语音留言（就是类似于微信的对讲机功能）
3 赠送的礼品
4 索取礼品消息
5 文件消息
6 短视频消息
7 名片
8 位置消息
10 发红包消息（wlx新增）
11 撤回消息 (wlx新增)
12 禁言 (wlx新增)
13 抢红包 (wlx新增)
90 系统消息或提示信息
     */
    private Integer msgType;

    /**
     * 消息内容：
1）当消息类型是文本消息时，本字段存放的是文本消息内容。
2）当消息类型是图片消息时，本字段存放的是暂存于服务端的图片原文件（非缩略图）的文件名。
3）当消息类型是语音留言消息时，本字段存放的是暂存于服务端的语音文件的文件名。
     */
    private String msgContent;

    /**
     * 发生时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date msgTime;

    /**
     * 发生时间戳
     */
    private Long msgTime2;

    /**
     * 消息发送时的在线人数：
数据可助于分析消息推送时的服务端压力等。
     */
    private Integer onlineCount;

    /**
     * 消息指纹码(唯一id)
     */
    private String fingerprint;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        MsgsCollectArchived other = (MsgsCollectArchived) that;
        return (this.getCollectId() == null ? other.getCollectId() == null : this.getCollectId().equals(other.getCollectId()))
            && (this.getSrcUid() == null ? other.getSrcUid() == null : this.getSrcUid().equals(other.getSrcUid()))
            && (this.getDestUid() == null ? other.getDestUid() == null : this.getDestUid().equals(other.getDestUid()))
            && (this.getChatType() == null ? other.getChatType() == null : this.getChatType().equals(other.getChatType()))
            && (this.getMsgType() == null ? other.getMsgType() == null : this.getMsgType().equals(other.getMsgType()))
            && (this.getMsgContent() == null ? other.getMsgContent() == null : this.getMsgContent().equals(other.getMsgContent()))
            && (this.getMsgTime() == null ? other.getMsgTime() == null : this.getMsgTime().equals(other.getMsgTime()))
            && (this.getMsgTime2() == null ? other.getMsgTime2() == null : this.getMsgTime2().equals(other.getMsgTime2()))
            && (this.getOnlineCount() == null ? other.getOnlineCount() == null : this.getOnlineCount().equals(other.getOnlineCount()))
            && (this.getFingerprint() == null ? other.getFingerprint() == null : this.getFingerprint().equals(other.getFingerprint()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCollectId() == null) ? 0 : getCollectId().hashCode());
        result = prime * result + ((getSrcUid() == null) ? 0 : getSrcUid().hashCode());
        result = prime * result + ((getDestUid() == null) ? 0 : getDestUid().hashCode());
        result = prime * result + ((getChatType() == null) ? 0 : getChatType().hashCode());
        result = prime * result + ((getMsgType() == null) ? 0 : getMsgType().hashCode());
        result = prime * result + ((getMsgContent() == null) ? 0 : getMsgContent().hashCode());
        result = prime * result + ((getMsgTime() == null) ? 0 : getMsgTime().hashCode());
        result = prime * result + ((getMsgTime2() == null) ? 0 : getMsgTime2().hashCode());
        result = prime * result + ((getOnlineCount() == null) ? 0 : getOnlineCount().hashCode());
        result = prime * result + ((getFingerprint() == null) ? 0 : getFingerprint().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", collectId=").append(collectId);
        sb.append(", srcUid=").append(srcUid);
        sb.append(", destUid=").append(destUid);
        sb.append(", chatType=").append(chatType);
        sb.append(", msgType=").append(msgType);
        sb.append(", msgContent=").append(msgContent);
        sb.append(", msgTime=").append(msgTime);
        sb.append(", msgTime2=").append(msgTime2);
        sb.append(", onlineCount=").append(onlineCount);
        sb.append(", fingerprint=").append(fingerprint);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
