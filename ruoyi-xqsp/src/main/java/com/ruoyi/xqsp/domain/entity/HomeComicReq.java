package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpComic;
import lombok.Data;

import java.util.List;

/**
 * 漫画详情
 */
@Data
public class HomeComicReq {
    private Long userId;
    private Long comicId;
    private Long comicLike;//喜欢状态
    private Long comicCollect;//收藏状态
    private List<SpComic>spComicList;//同类漫画推荐
    private SpComic spComic;
    private List<String>comicTagName;//漫画标签
    private Long comicChapter;//漫画章节
    private String latestChapter;//漫画章节

}
