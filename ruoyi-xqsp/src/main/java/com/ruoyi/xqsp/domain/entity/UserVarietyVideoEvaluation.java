package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpVarietyVideoEvaluation;
import lombok.Data;

/**
 * 综艺视频评论和用户
 */
@Data
public class UserVarietyVideoEvaluation {
    private SpVarietyVideoEvaluation spVarietyVideoEvaluation;
    private String varietyVideoName;
    private String userName;

}
