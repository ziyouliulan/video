package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpComicUser;
import lombok.Data;

/**
 * 漫画书架
 */
@Data
public class UserComic {

    private SpComicUser spComicUser;
    private String userPhone;
    private String comicName;
}
