package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpVariety;
import com.ruoyi.xqsp.domain.SpVarietyVideo;
import lombok.Data;

import java.util.List;

/**
 * 综艺和综艺视频
 */
@Data
public class VarietyAndVarietyVideo {
    private SpVariety spVariety;
    private List<SpVarietyVideo>spVarietyVideoList;
}
