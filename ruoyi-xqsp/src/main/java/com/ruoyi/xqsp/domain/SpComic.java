package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalSerializer;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 漫画操作对象 sp_comic
 *
 * @author ruoyi
 * @date 2021-04-28
 */
@Data
public class SpComic extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 漫画id */
    @TableId(type = IdType.AUTO)
    private Long comicId;

    /** 漫画名字 */
    @Excel(name = "漫画名字")
    private String comicName;

    /** 漫画封面 */
    @Excel(name = "漫画封面")
    private String comicCoverImg;

    /** 漫画介绍 */
    @Excel(name = "漫画介绍")
    private String comicIntroduce;

    /** 漫画作者 */
    @Excel(name = "漫画作者")
    private String comicAuthor;

    /** 漫画分类 */
    @Excel(name = "漫画分类")
    private String comicCategory;

    /** 漫画分类 */
    @Excel(name = "漫画分类")
    private Integer comicCategoryId;

    /** 漫画观看人数 */
    @Excel(name = "漫画观看人数")
    private Long comicWatchNumber;

    /** 喜欢人数 */
    @Excel(name = "喜欢人数")
    private Double comicLikeNumber;

    /** 付费设置 */
    @Excel(name = "付费设置")
    private Long comicUnlock;

    /** 是否置顶 */
    @Excel(name = "是否置顶")
    private String comicTop;


    /**  解锁所需金币数 */
    @Excel(name = " 解锁所需金币数")

    private BigDecimal comicUnlockGlod;

    /** 最新章节(上传) */
    @Excel(name = "最新章节(上传)")
    private String comicNewChapter;

    /** 排序 */
    @Excel(name = "排序")
    private Long comicSort;

    /** 状态(0 上架， 1 下架) */
    @Excel(name = "状态(0 上架， 1 下架)")
    private Long comicState;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date comicTime;

    /** 漫画创作状态（1新作，2连载中，3已完结） */
    @Excel(name = "漫画创作状态", readConverterExp = "1=新作，2连载中，3已完结")
    private Long comicStateType;

    @TableField(exist = false)
    private Integer comicIsFree;

    public void setComicId(Long comicId)
    {
        this.comicId = comicId;
    }

    public Long getComicId()
    {
        return comicId;
    }
    public void setComicName(String comicName)
    {
        this.comicName = comicName;
    }

    public String getComicName()
    {
        return comicName;
    }
    public void setComicCoverImg(String comicCoverImg)
    {
        this.comicCoverImg = comicCoverImg;
    }

    public String getComicCoverImg()
    {
        return comicCoverImg;
    }
    public void setComicIntroduce(String comicIntroduce)
    {
        this.comicIntroduce = comicIntroduce;
    }

    public String getComicIntroduce()
    {
        return comicIntroduce;
    }
    public void setComicAuthor(String comicAuthor)
    {
        this.comicAuthor = comicAuthor;
    }

    public String getComicAuthor()
    {
        return comicAuthor;
    }
    public void setComicCategory(String comicCategory)
    {
        this.comicCategory = comicCategory;
    }

    public String getComicCategory()
    {
        return comicCategory;
    }
    public void setComicWatchNumber(Long comicWatchNumber)
    {
        this.comicWatchNumber = comicWatchNumber;
    }

    public Long getComicWatchNumber()
    {
        return comicWatchNumber;
    }
    public void setComicLikeNumber(Double comicLikeNumber)
    {
        this.comicLikeNumber = comicLikeNumber;
    }

    public Double getComicLikeNumber()
    {
        return comicLikeNumber;
    }
    public void setComicUnlock(Long comicUnlock)
    {
        this.comicUnlock = comicUnlock;
    }

    public Long getComicUnlock()
    {
        return comicUnlock;
    }
    public void setComicTop(String comicTop)
    {
        this.comicTop = comicTop;
    }

    public String getComicTop()
    {
        return comicTop;
    }
    public void setComicUnlockGlod(BigDecimal comicUnlockGlod)
    {
        this.comicUnlockGlod = comicUnlockGlod;
    }

    @JsonSerialize(using = BigDecimalSerializer.class)
    public BigDecimal getComicUnlockGlod()
    {
        return comicUnlockGlod;
    }
    public void setComicNewChapter(String comicNewChapter)
    {
        this.comicNewChapter = comicNewChapter;
    }

    public String getComicNewChapter()
    {
        return comicNewChapter;
    }
    public void setComicSort(Long comicSort)
    {
        this.comicSort = comicSort;
    }

    public Long getComicSort()
    {
        return comicSort;
    }
    public void setComicState(Long comicState)
    {
        this.comicState = comicState;
    }

    public Long getComicState()
    {
        return comicState;
    }
    public void setComicTime(Date comicTime)
    {
        this.comicTime = comicTime;
    }

    public Date getComicTime()
    {
        return comicTime;
    }
    public void setComicStateType(Long comicStateType)
    {
        this.comicStateType = comicStateType;
    }

    public Long getComicStateType()
    {
        return comicStateType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("comicId", getComicId())
                .append("comicName", getComicName())
                .append("comicCoverImg", getComicCoverImg())
                .append("comicIntroduce", getComicIntroduce())
                .append("comicAuthor", getComicAuthor())
                .append("comicCategory", getComicCategory())
                .append("comicWatchNumber", getComicWatchNumber())
                .append("comicLikeNumber", getComicLikeNumber())
                .append("comicUnlock", getComicUnlock())
                .append("comicTop", getComicTop())
                .append("comicUnlockGlod", getComicUnlockGlod())
                .append("comicNewChapter", getComicNewChapter())
                .append("comicSort", getComicSort())
                .append("comicState", getComicState())
                .append("comicTime", getComicTime())
                .append("comicStateType", getComicStateType())
                .toString();
    }

    @TableField(exist = false)
    private Long chapterCount;//章节数量

    public Long getChapterCount() {
        return chapterCount;
    }

    public void setChapterCount(Long chapterCount) {
        this.chapterCount = chapterCount;
    }
    @TableField(exist = false)
    private Integer evaluationCount;//评论数量

    public Integer getEvaluationCount() {
        return evaluationCount;
    }

    public void setEvaluationCount(Integer evaluationCount) {
        this.evaluationCount = evaluationCount;
    }
    @TableField(exist = false)
    private List<String>comicTagName;//漫画标签

    public List<String> getComicTagName() {
        return comicTagName;
    }

    public void setComicTagName(List<String> comicTagName) {
        this.comicTagName = comicTagName;
    }
}
