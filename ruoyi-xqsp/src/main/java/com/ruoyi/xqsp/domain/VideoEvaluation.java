package com.ruoyi.xqsp.domain;

import lombok.Data;

@Data
public class VideoEvaluation {
    private SpVideoEvaluation spVideoEvaluation;
    private String userPhone;
    private String videoName;
}
