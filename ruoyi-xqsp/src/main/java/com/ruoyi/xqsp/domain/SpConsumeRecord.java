package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 消费记录
 * @TableName sp_consume_record
 */
@TableName(value ="sp_consume_record")
@Data
public class SpConsumeRecord implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 交易单号
     */
    private String tradeNo;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 邀请用户的手机号码
     */
    private String toUserPhone;

    /**
     * 邀请用户的名称
     */
    private String toUserName;


    /**
     * 邀请人
     */
    private Long toUserId;

    /**
     * 金额
     */
    private BigDecimal money;

    /**
     * 类型（1.vip月卡，2.VIP季卡，3.VIP年卡，4.活动赠送，5.推广奖励，6.兑换码，7.金币充值8.半年卡9.客服充值 ,10视频购买 11漫画购买）
     */
    private Integer type;

    /**
     * 消费备注
     */
    private String descs;

    /**
     * 1：客服充值
     */
    private Integer payType;

    /**
     *  1 收入  2支出
     */
    private Integer changeType;

    /**
     * 当前余额
     */
    private BigDecimal currentBalance;

    /**
     * 实际操作金额
     */
    private BigDecimal operationAmount;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 到账时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 会员日期
     */
    private Integer userMembersDay;


    @TableField(exist = false)
    private SpVideo spVideo;

    /**
     * 1：天数 2 月
     */
    private Integer memberType;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SpConsumeRecord other = (SpConsumeRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTradeNo() == null ? other.getTradeNo() == null : this.getTradeNo().equals(other.getTradeNo()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getToUserPhone() == null ? other.getToUserPhone() == null : this.getToUserPhone().equals(other.getToUserPhone()))
            && (this.getToUserName() == null ? other.getToUserName() == null : this.getToUserName().equals(other.getToUserName()))
            && (this.getToUserId() == null ? other.getToUserId() == null : this.getToUserId().equals(other.getToUserId()))
            && (this.getMoney() == null ? other.getMoney() == null : this.getMoney().equals(other.getMoney()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getDescs() == null ? other.getDescs() == null : this.getDescs().equals(other.getDescs()))
            && (this.getPayType() == null ? other.getPayType() == null : this.getPayType().equals(other.getPayType()))
            && (this.getChangeType() == null ? other.getChangeType() == null : this.getChangeType().equals(other.getChangeType()))
            && (this.getCurrentBalance() == null ? other.getCurrentBalance() == null : this.getCurrentBalance().equals(other.getCurrentBalance()))
            && (this.getOperationAmount() == null ? other.getOperationAmount() == null : this.getOperationAmount().equals(other.getOperationAmount()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getRemarks() == null ? other.getRemarks() == null : this.getRemarks().equals(other.getRemarks()))
            && (this.getUserMembersDay() == null ? other.getUserMembersDay() == null : this.getUserMembersDay().equals(other.getUserMembersDay()))
            && (this.getMemberType() == null ? other.getMemberType() == null : this.getMemberType().equals(other.getMemberType()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTradeNo() == null) ? 0 : getTradeNo().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getToUserPhone() == null) ? 0 : getToUserPhone().hashCode());
        result = prime * result + ((getToUserName() == null) ? 0 : getToUserName().hashCode());
        result = prime * result + ((getToUserId() == null) ? 0 : getToUserId().hashCode());
        result = prime * result + ((getMoney() == null) ? 0 : getMoney().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getDescs() == null) ? 0 : getDescs().hashCode());
        result = prime * result + ((getPayType() == null) ? 0 : getPayType().hashCode());
        result = prime * result + ((getChangeType() == null) ? 0 : getChangeType().hashCode());
        result = prime * result + ((getCurrentBalance() == null) ? 0 : getCurrentBalance().hashCode());
        result = prime * result + ((getOperationAmount() == null) ? 0 : getOperationAmount().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getRemarks() == null) ? 0 : getRemarks().hashCode());
        result = prime * result + ((getUserMembersDay() == null) ? 0 : getUserMembersDay().hashCode());
        result = prime * result + ((getMemberType() == null) ? 0 : getMemberType().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", tradeNo=").append(tradeNo);
        sb.append(", userId=").append(userId);
        sb.append(", toUserPhone=").append(toUserPhone);
        sb.append(", toUserName=").append(toUserName);
        sb.append(", toUserId=").append(toUserId);
        sb.append(", money=").append(money);
        sb.append(", type=").append(type);
        sb.append(", descs=").append(descs);
        sb.append(", payType=").append(payType);
        sb.append(", changeType=").append(changeType);
        sb.append(", currentBalance=").append(currentBalance);
        sb.append(", operationAmount=").append(operationAmount);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", remarks=").append(remarks);
        sb.append(", userMembersDay=").append(userMembersDay);
        sb.append(", memberType=").append(memberType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
