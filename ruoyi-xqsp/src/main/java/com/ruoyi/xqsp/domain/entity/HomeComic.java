package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 漫画首页
 */
@Data
public class HomeComic {
    private Long userId;
    private Long type;//排序类型
    private Long comicUnlock;//收费类型
    private Integer tag;//标签类型
    private String name;//标签类型
    private Integer pageNum;//分页页数
    private Integer pageSize;//分页条数
    private Long comicStateType;//漫画创造状态



    private Long comicId;
    private Long comicChapterId;//漫画章节
    private Long comicChapterEndId;//漫画章节
    private Integer buyType;//购买类型（1自动，0手动）
}
