package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 微信支付对象 sp_wxpay
 * 
 * @author ruoyi
 * @date 2021-06-01
 */
public class SpWxpay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long wxpayId;

    /** 商户appid */
    @Excel(name = "商户appid")
    private String appId;

    /** 商户秘钥 */
    @Excel(name = "商户秘钥")
    private String spKey;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchId;

    public void setWxpayId(Long wxpayId) 
    {
        this.wxpayId = wxpayId;
    }

    public Long getWxpayId() 
    {
        return wxpayId;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setSpKey(String spKey) 
    {
        this.spKey = spKey;
    }

    public String getSpKey() 
    {
        return spKey;
    }
    public void setMchId(String mchId) 
    {
        this.mchId = mchId;
    }

    public String getMchId() 
    {
        return mchId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("wxpayId", getWxpayId())
            .append("appId", getAppId())
            .append("spKey", getSpKey())
            .append("mchId", getMchId())
            .toString();
    }
}
