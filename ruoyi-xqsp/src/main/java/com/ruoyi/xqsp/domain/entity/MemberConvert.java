package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 *会员兑换
 */
@Data
public class MemberConvert {

    private Long type;//1 推广奖励 2兑换码 3卡密
    private String code;//xqid/t兑换码/卡密
    private Long userId;
}
