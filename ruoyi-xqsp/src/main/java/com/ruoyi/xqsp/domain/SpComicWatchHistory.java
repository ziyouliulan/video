package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 视频观看记录
 * @TableName sp_comic_watch_history
 */
@TableName(value ="sp_comic_watch_history")
@Data
public class SpComicWatchHistory implements Serializable {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long comicWatchHistoryId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 漫画id
     */
    private Long comicWatchHistoryTypeId;

    /**
     * 漫画章节id
     */
    private Long comicWatchHistoryChapterId;

    @TableField(exist = false)
    private SpComic comic;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date comicWatchHistoryTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
