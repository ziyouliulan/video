package com.ruoyi.xqsp.domain;

import lombok.Data;

@Data
public class CategoryParent {
    private SpVideoCategory spVideoCategory;
    private String videoCategoryParentName;
}
