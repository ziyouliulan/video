package com.ruoyi.xqsp.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.quyang.voice.utils.BigDecimalSerializer;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户表对象 sp_users
 *
 * @author ruoyi
 * @date 2021-04-26
 */
@Data
public class SpUsers
{
    private static final long serialVersionUID = 1L;

    /**  用户id */

    @TableId(type = IdType.AUTO)
    private Long userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 用户名称 */
    @Excel(name = "用户昵称")
    private String userNickname;

    /** 用户密码 */
    @Excel(name = "用户密码")
    private String userPassword;

    /** 用户手机序列号（唯一） */
    @Excel(name = "用户手机序列号", readConverterExp = "唯=一")
    private String userApi;

    /** 用户手机号 */
    @Excel(name = "用户手机号")
    private String userPhone;

    /** 用户权限 */
    @Excel(name = "用户权限")
    private String userPermissions;

    /** 用户设备 */
    @Excel(name = "用户设备")
    private String userEquipment;

    /** 上级用户 */
    @Excel(name = "上级用户")
    private String userSuperior;

    /** 下级用户 */
    @Excel(name = "下级用户")
    private String userUnder;

    /** 推广码 */
    @Excel(name = "推广码")
    private String userPromoteCode;

    /** 推广人数 */
    @Excel(name = "推广人数")
    private Long userPromotePeople;

    /** 金币数 */
    @Excel(name = "金币数")
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal userGlod;

    /** 用户状态(用来封停用户) */
    @Excel(name = "用户状态(用来封停用户)")
    private String userState;

    /** 用户评论（用来禁言） */
    @Excel(name = "用户评论", readConverterExp = "用=来禁言")
    private String userComments;

    /** 星球ID */
    @Excel(name = "星球ID")
    private String userXqid;

    /**  用户头像 */
    @Excel(name = " 用户头像")
    private String userImg;

    /** 剩余会员天数 */
//    @Excel(name = "剩余会员天数")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date userMembersDay;

    /** 今天观看次数 */
    @Excel(name = "短视频今天观看次数")
    private Long userToday;

    /** 福利观看次数 */
    @Excel(name = "短视频福利观看次数")
    private Long userWelfare;

    /** 今天观看次数 */
    @Excel(name = "短视频今天观看次数")
    private Long userTodayShort;

    /** 福利观看次数 */
    @Excel(name = "短视频福利观看次数")
    private Long userWelfareShort;

    /** 今天观看次数 */
    @Excel(name = "长视频今天观看次数")
    private Long userTodayLong;

    /** 福利观看次数 */
    @Excel(name = "长视频福利观看次数")
    private Long userWelfareLong;

    /** 卡密 */
    @Excel(name = "卡密")
    private String userCarmichael;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date userTime;
    /** 登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date userLoginTime;

    /** 登录状态（0下线，1上线） */
    @Excel(name = "登录状态", readConverterExp = "0=下线，1上线")
    private Integer userLoginState;

    /** 漫画自动购买类型（0手动，1自动） */
    @Excel(name = "漫画自动购买类型", readConverterExp = "0=手动，1自动")
    private Integer userBuyType;

    /** 用户的vip类型 */
    @Excel(name = "0:普通(月卡,季卡) 1:半年卡 2:年卡")
    private Integer userVipType;


    /** 用户的vip类型 */
    @TableField(exist = false)
    private String token;

    /** 用户的vip类型 */
    @TableField(exist = false)
    private Boolean isVip = false;

    /** 用户的vip类型 */
    @TableField(exist = false)
    private Boolean isUnlock = false;

    public Integer getUserVipType() {
        return userVipType;
    }

    public void setUserVipType(Integer userVipType) {
        this.userVipType = userVipType;
    }


    public Integer getUserBuyType() {
        return userBuyType;
    }

    public void setUserBuyType(Integer userBuyType) {
        this.userBuyType = userBuyType;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;
    }

    public String getUserPassword()
    {
        return userPassword;
    }
    public void setUserApi(String userApi)
    {
        this.userApi = userApi;
    }

    public String getUserApi()
    {
        return userApi;
    }
    public void setUserPhone(String userPhone)
    {
        this.userPhone = userPhone;
    }

    public String getUserPhone()
    {
        return userPhone;
    }
    public void setUserPermissions(String userPermissions)
    {
        this.userPermissions = userPermissions;
    }

    public String getUserPermissions()
    {
        return userPermissions;
    }
    public void setUserEquipment(String userEquipment)
    {
        this.userEquipment = userEquipment;
    }

    public String getUserEquipment()
    {
        return userEquipment;
    }
    public void setUserSuperior(String userSuperior)
    {
        this.userSuperior = userSuperior;
    }

    public String getUserSuperior()
    {
        return userSuperior;
    }
    public void setUserUnder(String userUnder)
    {
        this.userUnder = userUnder;
    }

    public String getUserUnder()
    {
        return userUnder;
    }
    public void setUserPromoteCode(String userPromoteCode)
    {
        this.userPromoteCode = userPromoteCode;
    }

    public String getUserPromoteCode()
    {
        return userPromoteCode;
    }
    public void setUserPromotePeople(Long userPromotePeople)
    {
        this.userPromotePeople = userPromotePeople;
    }

    public Long getUserPromotePeople()
    {
        return userPromotePeople;
    }
    public void setUserGlod(BigDecimal userGlod)
    {
        this.userGlod = userGlod;
    }

    public BigDecimal getUserGlod()
    {
        return userGlod;
    }
    public void setUserState(String userState)
    {
        this.userState = userState;
    }

    public String getUserState()
    {
        return userState;
    }
    public void setUserComments(String userComments)
    {
        this.userComments = userComments;
    }

    public String getUserComments()
    {
        return userComments;
    }
    public void setUserXqid(String userXqid)
    {
        this.userXqid = userXqid;
    }

    public String getUserXqid()
    {
        return userXqid;
    }
    public void setUserCarmichael(String userCarmichael)
    {
        this.userCarmichael = userCarmichael;
    }

    public String getUserCarmichael()
    {
        return userCarmichael;
    }
    public void setUserTime(Date userTime)
    {
        this.userTime = userTime;
    }

    public Date getUserTime()
    {
        return userTime;
    }

    public Date getUserLoginTime() {
        return userLoginTime;
    }

    public void setUserLoginTime(Date userLoginTime) {
        this.userLoginTime = userLoginTime;
    }

    public Integer getUserLoginState() {
        return userLoginState;
    }

    public void setUserLoginState(Integer userLoginState) {
        this.userLoginState = userLoginState;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("userPassword", getUserPassword())
            .append("userApi", getUserApi())
            .append("userPhone", getUserPhone())
            .append("userPermissions", getUserPermissions())
            .append("userEquipment", getUserEquipment())
            .append("userSuperior", getUserSuperior())
            .append("userUnder", getUserUnder())
            .append("userPromoteCode", getUserPromoteCode())
            .append("userPromotePeople", getUserPromotePeople())
            .append("userGlod", getUserGlod())
            .append("userState", getUserState())
            .append("userComments", getUserComments())
            .append("userXqid", getUserXqid())
            .append("userImg", getUserImg())
            .append("userMembersDay", getUserMembersDay())
            .append("userCarmichael", getUserCarmichael())
            .append("userTime", getUserTime())
                .append("userBuyType", getUserBuyType())
            .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpUsers spUsers = (SpUsers) o;
        return Objects.equals(userId, spUsers.userId) &&
                Objects.equals(userName, spUsers.userName) &&
                Objects.equals(userPassword, spUsers.userPassword) &&
                Objects.equals(userApi, spUsers.userApi) &&
                Objects.equals(userPhone, spUsers.userPhone) &&
                Objects.equals(userPermissions, spUsers.userPermissions) &&
                Objects.equals(userEquipment, spUsers.userEquipment) &&
                Objects.equals(userSuperior, spUsers.userSuperior) &&
                Objects.equals(userUnder, spUsers.userUnder) &&
                Objects.equals(userPromoteCode, spUsers.userPromoteCode) &&
                Objects.equals(userPromotePeople, spUsers.userPromotePeople) &&
                Objects.equals(userGlod, spUsers.userGlod) &&
                Objects.equals(userState, spUsers.userState) &&
                Objects.equals(userComments, spUsers.userComments) &&
                Objects.equals(userXqid, spUsers.userXqid) &&
                Objects.equals(userImg, spUsers.userImg) &&
                Objects.equals(userMembersDay, spUsers.userMembersDay) &&
                Objects.equals(userCarmichael, spUsers.userCarmichael) &&
                Objects.equals(userTime, spUsers.userTime) &&
                 Objects.equals(userBuyType, spUsers.userBuyType)&&
                Objects.equals(userLoginTime, spUsers.userLoginTime) &&
                Objects.equals(userLoginState, spUsers.userLoginState);

    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userName, userPassword, userApi, userPhone, userPermissions, userEquipment, userSuperior, userUnder, userPromoteCode, userPromotePeople, userGlod, userState, userComments, userXqid, userImg, userMembersDay,  userCarmichael, userTime,userBuyType,userLoginTime,userLoginState);
    }



}
