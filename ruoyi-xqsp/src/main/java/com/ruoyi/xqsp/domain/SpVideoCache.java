package com.ruoyi.xqsp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 视频缓存对象 sp_video_cache
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
public class SpVideoCache extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long videoCacheId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 缓存视频类型id（视频id，综艺视频id） */
    @Excel(name = "缓存视频类型id", readConverterExp = "视=频id，综艺视频id")
    private Long videoCacheTypeId;

    /** 缓存视频类型(1.视频 2.综艺视频) */
    @Excel(name = "缓存视频类型(1.视频 2.综艺视频)")
    private Integer videoCacheType;

    /**  时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = " 时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date videoCacheTime;

    /** 缓存地址 */
    @Excel(name = "缓存地址")
    private String videoCacheUrl;

    public void setVideoCacheId(Long videoCacheId)
    {
        this.videoCacheId = videoCacheId;
    }

    public Long getVideoCacheId()
    {
        return videoCacheId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setVideoCacheTypeId(Long videoCacheTypeId)
    {
        this.videoCacheTypeId = videoCacheTypeId;
    }

    public Long getVideoCacheTypeId()
    {
        return videoCacheTypeId;
    }
    public void setVideoCacheType(Integer videoCacheType)
    {
        this.videoCacheType = videoCacheType;
    }

    public Integer getVideoCacheType()
    {
        return videoCacheType;
    }
    public void setVideoCacheTime(Date videoCacheTime)
    {
        this.videoCacheTime = videoCacheTime;
    }

    public Date getVideoCacheTime()
    {
        return videoCacheTime;
    }
    public void setVideoCacheUrl(String videoCacheUrl)
    {
        this.videoCacheUrl = videoCacheUrl;
    }

    public String getVideoCacheUrl()
    {
        return videoCacheUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("videoCacheId", getVideoCacheId())
                .append("userId", getUserId())
                .append("videoCacheTypeId", getVideoCacheTypeId())
                .append("videoCacheType", getVideoCacheType())
                .append("videoCacheTime", getVideoCacheTime())
                .append("videoCacheUrl", getVideoCacheUrl())
                .toString();
    }
}
