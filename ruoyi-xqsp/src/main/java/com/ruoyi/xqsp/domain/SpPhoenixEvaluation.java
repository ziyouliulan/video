package com.ruoyi.xqsp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 楼凤评价表对象 sp_phoenix_evaluation
 * 
 * @author ruoyi
 * @date 2021-04-25
 */
public class SpPhoenixEvaluation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评价编号 */
    private Long phoenixEvaluationId;

    /** 楼凤id */
    @Excel(name = "楼凤id")
    private Long phoenixId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 评价内容 */
    @Excel(name = "评价内容")
    private String phoenixEvaluationContent;

    /** 评价时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "评价时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date phoenixEvaluationTime;

    /** 点赞数量 */
    @Excel(name = "点赞数量")
    private Long phoenixEvaluationLike;


    public void setPhoenixEvaluationId(Long phoenixEvaluationId) 
    {
        this.phoenixEvaluationId = phoenixEvaluationId;
    }

    public Long getPhoenixEvaluationId() 
    {
        return phoenixEvaluationId;
    }
    public void setPhoenixId(Long phoenixId) 
    {
        this.phoenixId = phoenixId;
    }

    public Long getPhoenixId() 
    {
        return phoenixId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPhoenixEvaluationContent(String phoenixEvaluationContent) 
    {
        this.phoenixEvaluationContent = phoenixEvaluationContent;
    }

    public String getPhoenixEvaluationContent() 
    {
        return phoenixEvaluationContent;
    }
    public void setPhoenixEvaluationTime(Date phoenixEvaluationTime) 
    {
        this.phoenixEvaluationTime = phoenixEvaluationTime;
    }

    public Date getPhoenixEvaluationTime() 
    {
        return phoenixEvaluationTime;
    }

    public void setPhoenixEvaluationLike(Long phoenixEvaluationLike)
    {
        this.phoenixEvaluationLike = phoenixEvaluationLike;
    }

    public Long getPhoenixEvaluationLike()
    {
        return phoenixEvaluationLike;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("phoenixEvaluationId", getPhoenixEvaluationId())
            .append("phoenixId", getPhoenixId())
            .append("userId", getUserId())
            .append("phoenixEvaluationContent", getPhoenixEvaluationContent())
            .append("phoenixEvaluationTime", getPhoenixEvaluationTime())
            .append("phoenixEvaluationLike", getPhoenixEvaluationLike())
            .toString();
    }
}
