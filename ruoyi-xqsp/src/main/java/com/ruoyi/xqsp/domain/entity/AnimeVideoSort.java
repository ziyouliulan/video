package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 动漫首页排序
 */
@Data
public class AnimeVideoSort {

    private Integer sort;//排序类型
    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数
}
