package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 银行卡操作对象 sp_bank
 * 
 * @author ruoyi
 * @date 2021-06-07
 */
public class SpBank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long spBankId;

    /** 持卡人 */
    @Excel(name = "持卡人")
    private String spBankPeople;

    /** 卡号 */
    @Excel(name = "卡号")
    private Long spBankCard;

    /** 银行名字 */
    @Excel(name = "银行名字")
    private String spBankName;

    public void setSpBankId(Long spBankId) 
    {
        this.spBankId = spBankId;
    }

    public Long getSpBankId() 
    {
        return spBankId;
    }
    public void setSpBankPeople(String spBankPeople) 
    {
        this.spBankPeople = spBankPeople;
    }

    public String getSpBankPeople() 
    {
        return spBankPeople;
    }
    public void setSpBankCard(Long spBankCard) 
    {
        this.spBankCard = spBankCard;
    }

    public Long getSpBankCard() 
    {
        return spBankCard;
    }
    public void setSpBankName(String spBankName) 
    {
        this.spBankName = spBankName;
    }

    public String getSpBankName() 
    {
        return spBankName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("spBankId", getSpBankId())
            .append("spBankPeople", getSpBankPeople())
            .append("spBankCard", getSpBankCard())
            .append("spBankName", getSpBankName())
            .toString();
    }
}
