package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @TableName sp_url_filter
 */
@TableName(value ="sp_url_filter")
@Data
public class UrlFilter implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * url
     */
    private String urlPath;

    /**
     * 时间
     */
    private String time;

    /**
     * 请求token
     */
    private String token;

    /**
     * ip地址
     */
    private String ip;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
