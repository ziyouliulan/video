package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 漫画书签对象 sp_comic_bookmark
 * 
 * @author ruoyi
 * @date 2021-05-22
 */
public class SpComicBookmark extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 漫画书签id、 */
    private Long comicBookmarkId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 漫画id */
    @Excel(name = "漫画id")
    private Long comicId;

    /** 章节目录 */
    @Excel(name = "章节目录")
    private Long comicChapterDirectory;

    public void setComicBookmarkId(Long comicBookmarkId) 
    {
        this.comicBookmarkId = comicBookmarkId;
    }

    public Long getComicBookmarkId() 
    {
        return comicBookmarkId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setComicId(Long comicId) 
    {
        this.comicId = comicId;
    }

    public Long getComicId() 
    {
        return comicId;
    }
    public void setComicChapterDirectory(Long comicChapterDirectory) 
    {
        this.comicChapterDirectory = comicChapterDirectory;
    }

    public Long getComicChapterDirectory() 
    {
        return comicChapterDirectory;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("comicBookmarkId", getComicBookmarkId())
            .append("userId", getUserId())
            .append("comicId", getComicId())
            .append("comicChapterDirectory", getComicChapterDirectory())
            .toString();
    }
}
