package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *
 * @TableName sp_dating_user_comment
 */
@TableName(value ="sp_dating_user_comment")
@Data
public class DatingUserComment implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @NotNull
    private Long userId;
    /**
     * 用户ID
     */

    @TableField(exist = false)
    private String userNickname;
    /**
     * 用户ID
     */

    @TableField(exist = false)
    private String userImg;

    /**
     * 被评论的用户id
     */
    @NotNull
    private Long datingUseId;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 推荐1是
     */
    private Integer recommend;

    /**
     * 是否删除
     */
    private Integer isDel;

    /**
     * 评论内容
     */
    @NotNull
    private String content;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
