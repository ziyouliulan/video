package com.ruoyi.xqsp.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 综艺视频评论对象 sp_variety_video_evaluation
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@Data
public class SpVarietyVideoEvaluation implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long varietyVideoEvaluationId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户id */
    @Excel(name = "用户id")
    @TableField(exist = false)
    private String userNickname;

    /** 用户id */
    @Excel(name = "用户id")
    @TableField(exist = false)
    private String userImg;

    /** 综艺视频编号 */
    @Excel(name = "综艺视频编号")
    private Long varietyVideoId;

    /** 评价内容 */
    @Excel(name = "评价内容")
    private String varietyVideoEvaluationContent;

    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date varietyVideoEvaluationTime;

    /** 点赞 */
    @Excel(name = "点赞")
    private Long varietyVideoEvaluationLike;

    public void setVarietyVideoEvaluationId(Long varietyVideoEvaluationId)
    {
        this.varietyVideoEvaluationId = varietyVideoEvaluationId;
    }

    public Long getVarietyVideoEvaluationId()
    {
        return varietyVideoEvaluationId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setVarietyVideoId(Long varietyVideoId)
    {
        this.varietyVideoId = varietyVideoId;
    }

    public Long getVarietyVideoId()
    {
        return varietyVideoId;
    }
    public void setVarietyVideoEvaluationContent(String varietyVideoEvaluationContent)
    {
        this.varietyVideoEvaluationContent = varietyVideoEvaluationContent;
    }

    public String getVarietyVideoEvaluationContent()
    {
        return varietyVideoEvaluationContent;
    }
    public void setVarietyVideoEvaluationTime(Date varietyVideoEvaluationTime)
    {
        this.varietyVideoEvaluationTime = varietyVideoEvaluationTime;
    }

    public Date getVarietyVideoEvaluationTime()
    {
        return varietyVideoEvaluationTime;
    }
    public void setVarietyVideoEvaluationLike(Long varietyVideoEvaluationLike)
    {
        this.varietyVideoEvaluationLike = varietyVideoEvaluationLike;
    }

    public Long getVarietyVideoEvaluationLike()
    {
        return varietyVideoEvaluationLike;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("varietyVideoEvaluationId", getVarietyVideoEvaluationId())
            .append("userId", getUserId())
            .append("varietyVideoId", getVarietyVideoId())
            .append("varietyVideoEvaluationContent", getVarietyVideoEvaluationContent())
            .append("varietyVideoEvaluationTime", getVarietyVideoEvaluationTime())
            .append("varietyVideoEvaluationLike", getVarietyVideoEvaluationLike())
            .toString();
    }
}
