//package com.ruoyi.xqsp.domain;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import java.io.Serializable;
//import java.time.LocalDateTime;
//import lombok.Data;
//
///**
// * 消费记录
// * @TableName sp_consume_record
// */
//@TableName(value ="sp_consume_record")
//@Data
//public class ConsumeRecord implements Serializable {
//    /**
//     *
//     */
//    @TableId(value = "id", type = IdType.AUTO)
//    private Long id;
//
//    /**
//     * 交易单号
//     */
//    @TableField(value = "trade_no")
//    private String tradeNo;
//
//    /**
//     * 用户ID
//     */
//    @TableField(value = "user_id")
//    private Long userId;
//
//    /**
//     *  对方用户Id：
// 接受转账时 为 转账人的ID，
// 发送转账时 为 接受放的 ID
//     */
//    @TableField(value = "to_user_id")
//    private Long toUserId;
//
//    /**
//     * 金额
//     */
//    @TableField(value = "money")
//    private Double money;
//
//    /**
//     * 类型：
//1:用户充值, 2:用户提现, 3:后台充值, 4:发红包, 5:领取红包,6:红包退款  7:转账   8:接受转账   9:转账退回,10:预约订单,11预约订单退回 12：会员开通 13按药开方 14购买课程  15群升级  16 线下活动 17推荐分成
//20人民币购买商品  21积分购买商品  22 出售人民币商品 23 出售积分商品 24 商品推荐分成 25 平台抽成 26获得积分分成，27推荐分成提现金额 ，28平台回报,
//29:二维码转账 30二维码收款，31：人民币商品退款,32:积分商品退款，33：出售人民币商品退款，34：出售的积分商品退款，35推荐分成回退，36平台抽成回退，37获得的积分回退，38平台回报回退, 39:商家提现
//40，积分出售（记录积分）  41 积分出售 （记录余额）42，积分购买（记录积分） 43，积分购买(记录余额)
//44，商家洗车分成 45 开通会员积分获得 46 开通会员推荐分成 47，商家钱包提现到余额转出，48，商家钱包提现到余额接收，49 推荐钱包提现到余额转出 50 推荐钱包提现到余额接收,  51: 联盟分成 52 联盟分成回退
//53    二维码平台抽成  54 二维码平台回报 ，55二维码收款
//     */
//    @TableField(value = "type")
//    private Integer type;
//
//    /**
//     * 消费备注
//     */
//    @TableField(value = "descs")
//    private String descs;
//
//    /**
//     *  1：支付宝支付 , 2：微信支付, 3：余额支付, 4:系统支付，5，推荐钱包  6商家钱包 7 积分钱包
//     */
//    @TableField(value = "pay_type")
//    private Integer payType;
//
//    /**
//     * 0：创建  1：支付完成  2：交易完成  -1：交易关闭 3提现成功，4提现失败
//     */
//    @TableField(value = "status")
//    private Integer status;
//
//    /**
//     * 类型：
//1:用户充值, 2:用户提现, 3:后台充值, 4:发红包, 5:领取红包,6:红包退款  7:转账   8:接受转账   9:转账退回,10:预约订单,11预约订单退回 12：会员开通 13按药开方 14购买课程  15群升级  16 线下活动 17推荐分成
//20人民币购买商品  21积分购买商品  22 出售人民币商品 23 出售积分商品 24 商品推荐分成 25 平台抽成 26获得积分分成，27推荐分成提现金额 ，28平台回报,
//29:二维码转账 30二维码收款，31：人民币商品退款,32:积分商品退款，33：出售人民币商品退款，34：出售的积分商品退款，35推荐分成回退，36平台抽成回退，37获得的积分回退，38平台回报回退, 39:商家提现
//40，积分出售（记录积分）  41 积分出售 （记录余额）42，积分购买（记录积分） 43，积分购买(记录余额)
//44，商家洗车分成 45 开通会员积分获得 46 开通会员推荐分成 47，商家钱包提现到余额转出，48，商家钱包提现到余额接收，49 推荐钱包提现到余额转出 50 推荐钱包提现到余额接收,  51: 联盟分成
//
//     */
//    @TableField(value = "red_packet_id")
//    private String redPacketId;
//
//    /**
//     *  1 收入  2支出
//     */
//    @TableField(value = "change_type")
//    private Integer changeType;
//
//    /**
//     * 超过一定金额需要后台审核：1.审核成功 ，-1.审核失败
//     */
//    @TableField(value = "manual_pay_status")
//    private Integer manualPayStatus;
//
//    /**
//     * 手续费
//     */
//    @TableField(value = "service_charge")
//    private Double serviceCharge;
//
//    /**
//     * 当前余额
//     */
//    @TableField(value = "current_balance")
//    private Double currentBalance;
//
//    /**
//     * 实际操作金额
//     */
//    @TableField(value = "operation_amount")
//    private Double operationAmount;
//
//    /**
//     * 时间
//     */
//    @TableField(value = "time")
//    private LocalDateTime time;
//
//    /**
//     * 到账时间
//     */
//    @TableField(value = "timestamp")
//    private LocalDateTime timestamp;
//
//    /**
//     * 商品购买优惠
//     */
//    @TableField(value = "shop_service_charge")
//    private Double shopServiceCharge;
//
//    /**
//     * 积分获得率
//     */
//    @TableField(value = "integral_service_charge")
//    private Double integralServiceCharge;
//
//    /**
//     * 推荐抽成
//     */
//    @TableField(value = "recommend_service_charge")
//    private Double recommendServiceCharge;
//
//    /**
//     * 平台回报
//     */
//    @TableField(value = "system_service_charge")
//    private Double systemServiceCharge;
//
//    @TableField(exist = false)
//    private static final long serialVersionUID = 1L;
//}