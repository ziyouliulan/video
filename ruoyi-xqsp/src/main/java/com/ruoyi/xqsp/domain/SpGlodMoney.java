package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 金币设置对象 sp_glod_money
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
public class SpGlodMoney extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long glodMoneyId;

    /** 金币数量 */
    @Excel(name = "金币数量")
    private Long glodMoneyGlod;

    /** 金钱数量 */
    @Excel(name = "金钱数量")
    private Long glodMoneyNumber;

    public void setGlodMoneyId(Long glodMoneyId) 
    {
        this.glodMoneyId = glodMoneyId;
    }

    public Long getGlodMoneyId() 
    {
        return glodMoneyId;
    }
    public void setGlodMoneyGlod(Long glodMoneyGlod) 
    {
        this.glodMoneyGlod = glodMoneyGlod;
    }

    public Long getGlodMoneyGlod() 
    {
        return glodMoneyGlod;
    }
    public void setGlodMoneyNumber(Long glodMoneyNumber) 
    {
        this.glodMoneyNumber = glodMoneyNumber;
    }

    public Long getGlodMoneyNumber() 
    {
        return glodMoneyNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("glodMoneyId", getGlodMoneyId())
            .append("glodMoneyGlod", getGlodMoneyGlod())
            .append("glodMoneyNumber", getGlodMoneyNumber())
            .toString();
    }
}
