//
//package com.ruoyi.xqsp.domain.vo;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * @Description:TODO(世界动态评论回复实体类)
// *
// * @version: V1.0
// * @author: wlx
// *
// */
//@Data
//@ApiModel("TPublishCommentsReply")
//@TableName(value = "t_publish_comments_reply")
//@AllArgsConstructor
//@NoArgsConstructor
//public class PublishCommentsReplyVo implements Serializable {
//
//	private static final long serialVersionUID = 1606802330155L;
//
//    @TableId(value = "id", type = IdType.AUTO)
//    @ApiModelProperty(name = "id", value = "", hidden = true)
//	private Long id;
//
//    @ApiModelProperty(name = "commentsId", value = "评论id")
//	private Long commentsId;
//
//    @ApiModelProperty(name = "uid", value = "回复用户ID")
//	private Long uid;
//
//	@ApiModelProperty(name = "uidNickname", value = "回复用户名")
//	private String uidNickname;
//
//	@ApiModelProperty(name = "uidAvatarUrl", value = "回复用户头像")
//	private String uidAvatarUrl;
//
//    @ApiModelProperty(name = "touid", value = "被回复用户id")
//	private Long touid;
//
//	@ApiModelProperty(name = "nickname", value = "被回复用户名")
//	private String touidNickname;
//
//	@ApiModelProperty(name = "avatarUrl", value = "被回复用户头像")
//	private String touidAvatarUrl;
//
//    @ApiModelProperty(name = "replyComment", value = "回复内容")
//	private String replyComment;
//
//    @ApiModelProperty(name = "replyCommentPic", value = "回复图片")
//	private String replyCommentPic;
//
//    @ApiModelProperty(name = "replyCommentPicName", value = "回复图片名字")
//	private String replyCommentPicName;
//
//    @ApiModelProperty(name = "isCommentUid", value = "回复用户是否是作者  0：是 1：不是")
//	private Integer isCommentUid;
//
//	@ApiModelProperty(name = "isCommentTouid", value = "被回复用户是否是作者  0：是 1：不是")
//	private Integer isCommentTouid;
//
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//    @ApiModelProperty(name = "createDate", value = "回复时间")
//	private Date createDate;
//
//
//}
