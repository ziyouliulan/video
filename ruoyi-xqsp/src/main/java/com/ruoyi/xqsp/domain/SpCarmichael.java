package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 卡密操作对象 sp_carmichael
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public class SpCarmichael extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long carmichaelId;

    /** 卡密 */
    @Excel(name = "卡密")
    private String carmichaelName;

    /** 兑换次数 */
    @Excel(name = "兑换次数")
    private Long carmichaelDays;

    public void setCarmichaelId(Long carmichaelId) 
    {
        this.carmichaelId = carmichaelId;
    }

    public Long getCarmichaelId() 
    {
        return carmichaelId;
    }
    public void setCarmichaelName(String carmichaelName) 
    {
        this.carmichaelName = carmichaelName;
    }

    public String getCarmichaelName() 
    {
        return carmichaelName;
    }
    public void setCarmichaelDays(Long carmichaelDays) 
    {
        this.carmichaelDays = carmichaelDays;
    }

    public Long getCarmichaelDays() 
    {
        return carmichaelDays;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("carmichaelId", getCarmichaelId())
            .append("carmichaelName", getCarmichaelName())
            .append("carmichaelDays", getCarmichaelDays())
            .toString();
    }
}
