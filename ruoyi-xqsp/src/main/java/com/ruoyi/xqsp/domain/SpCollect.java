package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 用户收藏对象 sp_collect
 *
 * @author ruoyi
 * @date 2021-05-08
 */
@Data
public class SpCollect extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    @TableId(type = IdType.AUTO)
    private Long collectId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 收藏类型id（视频，视频评论，等） */
    @Excel(name = "收藏类型id", readConverterExp = "视=频，视频评论，等")
    private Long collectTypeId;

    /** 收藏类型（视频0，漫画1，楼凤2，综艺3，演员4） */
    @Excel(name = "收藏类型", readConverterExp = "收藏类型（1长视频，2漫画，3演员）")
    private Long collectType;
    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date collectTime;

    @TableField(exist = false)
    private SpVideo spVideo;


    @TableField(exist = false)
    private SpComic spComic;

    public void setCollectId(Long collectId)
    {
        this.collectId = collectId;
    }

    public Long getCollectId()
    {
        return collectId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setCollectTypeId(Long collectTypeId)
    {
        this.collectTypeId = collectTypeId;
    }

    public Long getCollectTypeId()
    {
        return collectTypeId;
    }
    public void setCollectType(Long collectType)
    {
        this.collectType = collectType;
    }

    public Long getCollectType()
    {
        return collectType;
    }
    public void setCollectTime(Date collectTime)
    {
        this.collectTime = collectTime;
    }

    public Date getCollectTime()
    {
        return collectTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("collectId", getCollectId())
                .append("userId", getUserId())
                .append("collectTypeId", getCollectTypeId())
                .append("collectType", getCollectType())
                .append("collectTime", getCollectTime())
                .toString();
    }
}
