package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpComicEvaluation;
import lombok.Data;

/**
 * 漫画用户评论
 */
@Data
public class ComicUserEvaluation {
    private SpComicEvaluation spComicEvaluation;
    private  String userPhone;
    private String comicName;
}
