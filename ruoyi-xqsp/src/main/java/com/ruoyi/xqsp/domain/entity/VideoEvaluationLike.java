package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpUsers;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * 视频评论点赞情况
 */
@Data
public class VideoEvaluationLike {
    private SpVideoEvaluation spVideoEvaluation;
    private Integer like;

    private Long userId;// 用户id
    private Long videoId;  //视频id
    private String spUsers;//用户信息
    private Long userHead = 1L;//用户头像

    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数
}
