package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 免费章节数对象 sp_comic_free_chapter
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public class SpComicFreeChapter
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long comicFreeChapter;

    /** 免费章节数 */
    @Excel(name = "免费章节数")
    private Long comicFreeChapterNumber;

    public void setComicFreeChapter(Long comicFreeChapter) 
    {
        this.comicFreeChapter = comicFreeChapter;
    }

    public Long getComicFreeChapter() 
    {
        return comicFreeChapter;
    }
    public void setComicFreeChapterNumber(Long comicFreeChapterNumber) 
    {
        this.comicFreeChapterNumber = comicFreeChapterNumber;
    }

    public Long getComicFreeChapterNumber() 
    {
        return comicFreeChapterNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("comicFreeChapter", getComicFreeChapter())
            .append("comicFreeChapterNumber", getComicFreeChapterNumber())
            .toString();
    }
}
