//
//package com.ruoyi.xqsp.domain;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * @Description:TODO(动态点赞实体类)
// *
// * @version: V1.0
// * @author: liuhaotian
// *
// */
//@Data
//@ApiModel("TPublishComments")
//@TableName(value = "t_publish_comments")
//@AllArgsConstructor
//@NoArgsConstructor
//public class PublishComments implements Serializable {
//
//	private static final long serialVersionUID = 1599710372539L;
//
//    @TableId(value = "id", type = IdType.AUTO)
//    @ApiModelProperty(name = "id", value = "", hidden = true)
//	private Long id;
//
//    @ApiModelProperty(name = "publishId", value = "动态id")
//	private Long publishId;
//
//    @ApiModelProperty(name = "userId", value = "评论用户id")
//	private Long userId;
//
//    @ApiModelProperty(name = "userName", value = "评论用户名")
//	private String userName;
//
//	@ApiModelProperty(name = "userAvatar", value = "评论用户头像")
//	private String userAvatar;
//
//    @ApiModelProperty(name = "comment", value = "评论内容")
//	private String comment;
//
//    @ApiModelProperty(name = "commentPic", value = "评论图片url")
//	private String commentPic;
//
//    @ApiModelProperty(name = "commentPicName", value = "评论图片文件名")
//	private String commentPicName;
//
//	@DateTimeFormat(pattern = "MM-dd HH:mm")
//	@JsonFormat(pattern="MM-dd HH:mm",timezone = "GMT+8")
//    @ApiModelProperty(name = "createTime", value = "评论时间")
//	private Date createTime;
//
//
//}
