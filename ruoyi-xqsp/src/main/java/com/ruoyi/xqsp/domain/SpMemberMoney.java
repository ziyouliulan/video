package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员设置对象 sp_member_money
 * 
 * @author ruoyi
 * @date 2021-06-03
 */
public class SpMemberMoney extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long memberMoneyId;

    /** 类型 */
    @Excel(name = "类型")
    private String memberMoneyType;

    /** 金钱数量 */
    @Excel(name = "金钱数量")
    private Long memberMoneyNumber;

    public void setMemberMoneyId(Long memberMoneyId) 
    {
        this.memberMoneyId = memberMoneyId;
    }

    public Long getMemberMoneyId() 
    {
        return memberMoneyId;
    }
    public void setMemberMoneyType(String memberMoneyType) 
    {
        this.memberMoneyType = memberMoneyType;
    }

    public String getMemberMoneyType() 
    {
        return memberMoneyType;
    }
    public void setMemberMoneyNumber(Long memberMoneyNumber) 
    {
        this.memberMoneyNumber = memberMoneyNumber;
    }

    public Long getMemberMoneyNumber() 
    {
        return memberMoneyNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("memberMoneyId", getMemberMoneyId())
            .append("memberMoneyType", getMemberMoneyType())
            .append("memberMoneyNumber", getMemberMoneyNumber())
            .toString();
    }
}
