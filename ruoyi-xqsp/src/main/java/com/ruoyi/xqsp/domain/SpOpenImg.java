package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 开屏广告对象 sp_open_img
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public class SpOpenImg
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long openImgId;

    /** 广告地址 */
    @Excel(name = "广告地址")
    private String openImgUrl;

    /** 广告链接 */
    @Excel(name = "广告链接")
    private String openImgLink;

    public void setOpenImgId(Long openImgId) 
    {
        this.openImgId = openImgId;
    }

    public Long getOpenImgId() 
    {
        return openImgId;
    }
    public void setOpenImgUrl(String openImgUrl) 
    {
        this.openImgUrl = openImgUrl;
    }

    public String getOpenImgUrl() 
    {
        return openImgUrl;
    }
    public void setOpenImgLink(String openImgLink) 
    {
        this.openImgLink = openImgLink;
    }

    public String getOpenImgLink() 
    {
        return openImgLink;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("openImgId", getOpenImgId())
            .append("openImgUrl", getOpenImgUrl())
            .append("openImgLink", getOpenImgLink())
            .toString();
    }
}
