package com.ruoyi.xqsp.domain;

import lombok.Data;

@Data
public class PhoenixEvaluation {
    private SpPhoenixEvaluation spPhoenixEvaluation;
    private String userName;
    private String phoenixName;
}
