package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.xqsp.domain.entity.VarietyVideoAndVideo;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 视频观看历史对象 sp_video_watch_history
 *
 * @author ruoyi
 * @date 2021-05-11
 */
@Data
public class SpVideoWatchHistory
{
    private static final long serialVersionUID = 1L;

//    /** 编号 */
    @TableId(type = IdType.AUTO)
    private Long videoWatchHistoryId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 观看历史类型id（视频id，综艺视频id） */
    @Excel(name = "观看历史类型id", readConverterExp = "视频id，综艺视频id")
    private Long videoWatchHistoryTypeId;

    /** 观看历史类型(1.长视频 2.短视频) */
    @Excel(name = "观看历史类型(1.长视频 2.短视频)")
    private Integer videoWatchHistoryType;



    private Boolean isVip;
    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date videoWatchHistoryTime;

    @TableField(exist = false)
    private VarietyVideoAndVideo varietyVideoAndVideo;
//
//    public void setVideoWatchHistoryId(Long videoWatchHistoryId)
//    {
//        this.videoWatchHistoryId = videoWatchHistoryId;
//    }
//
//    public Long getVideoWatchHistoryId()
//    {
//        return videoWatchHistoryId;
//    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setVideoWatchHistoryTypeId(Long videoWatchHistoryTypeId)
    {
        this.videoWatchHistoryTypeId = videoWatchHistoryTypeId;
    }

    public Long getVideoWatchHistoryTypeId()
    {
        return videoWatchHistoryTypeId;
    }
    public void setVideoWatchHistoryType(Integer videoWatchHistoryType)
    {
        this.videoWatchHistoryType = videoWatchHistoryType;
    }

    public Integer getVideoWatchHistoryType()
    {
        return videoWatchHistoryType;
    }
    public void setVideoWatchHistoryTime(Date videoWatchHistoryTime)
    {
        this.videoWatchHistoryTime = videoWatchHistoryTime;
    }

    public Date getVideoWatchHistoryTime()
    {
        return videoWatchHistoryTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
//                .append("videoWatchHistoryId", getVideoWatchHistoryId())
                .append("userId", getUserId())
                .append("videoWatchHistoryTypeId", getVideoWatchHistoryTypeId())
                .append("videoWatchHistoryType", getVideoWatchHistoryType())
                .append("videoWatchHistoryTime", getVideoWatchHistoryTime())
                .toString();
    }










}
