package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 差评对象 sp_unlike
 * 
 * @author ruoyi
 * @date 2021-05-08
 */
public class SpUnlike extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  编号 */
    private Long unlikeId;

    /** 用户 */
    @Excel(name = "用户")
    private Long userId;

    /** 差评类型id（视频和综艺视频） */
    @Excel(name = "差评类型id", readConverterExp = "视=频和综艺视频")
    private Long unlikeTypeId;

    /** 差评类型（1.视频和2.综艺视频） */
    @Excel(name = "差评类型", readConverterExp = "1=.视频和2.综艺视频")
    private Long unlikeType;

    public void setUnlikeId(Long unlikeId) 
    {
        this.unlikeId = unlikeId;
    }

    public Long getUnlikeId() 
    {
        return unlikeId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUnlikeTypeId(Long unlikeTypeId) 
    {
        this.unlikeTypeId = unlikeTypeId;
    }

    public Long getUnlikeTypeId() 
    {
        return unlikeTypeId;
    }
    public void setUnlikeType(Long unlikeType) 
    {
        this.unlikeType = unlikeType;
    }

    public Long getUnlikeType() 
    {
        return unlikeType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("unlikeId", getUnlikeId())
            .append("userId", getUserId())
            .append("unlikeTypeId", getUnlikeTypeId())
            .append("unlikeType", getUnlikeType())
            .toString();
    }
}
