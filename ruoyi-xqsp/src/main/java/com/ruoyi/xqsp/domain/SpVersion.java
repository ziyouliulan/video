package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 版本设置对象 sp_version
 * 
 * @author ruoyi
 * @date 2021-06-02
 */
public class SpVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long versionId;

    /** 官网 */
    @Excel(name = "官网")
    private String versionOfficialWebsite;

    /** 安卓 */
    @Excel(name = "安卓")
    private String versionAndroid;

    /** 苹果 */
    @Excel(name = "苹果")
    private String versionApple;

    /** 版本号 */
    @Excel(name = "版本号")
    private String versionNumber;

    /** 版本说明 */
    @Excel(name = "版本说明")
    private String versionIntroduce;

    public void setVersionId(Long versionId) 
    {
        this.versionId = versionId;
    }

    public Long getVersionId() 
    {
        return versionId;
    }
    public void setVersionOfficialWebsite(String versionOfficialWebsite) 
    {
        this.versionOfficialWebsite = versionOfficialWebsite;
    }

    public String getVersionOfficialWebsite() 
    {
        return versionOfficialWebsite;
    }
    public void setVersionAndroid(String versionAndroid) 
    {
        this.versionAndroid = versionAndroid;
    }

    public String getVersionAndroid() 
    {
        return versionAndroid;
    }
    public void setVersionApple(String versionApple) 
    {
        this.versionApple = versionApple;
    }

    public String getVersionApple() 
    {
        return versionApple;
    }
    public void setVersionNumber(String versionNumber) 
    {
        this.versionNumber = versionNumber;
    }

    public String getVersionNumber() 
    {
        return versionNumber;
    }
    public void setVersionIntroduce(String versionIntroduce) 
    {
        this.versionIntroduce = versionIntroduce;
    }

    public String getVersionIntroduce() 
    {
        return versionIntroduce;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("versionId", getVersionId())
            .append("versionOfficialWebsite", getVersionOfficialWebsite())
            .append("versionAndroid", getVersionAndroid())
            .append("versionApple", getVersionApple())
            .append("versionNumber", getVersionNumber())
            .append("versionIntroduce", getVersionIntroduce())
            .toString();
    }
}
