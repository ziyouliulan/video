package com.ruoyi.xqsp.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 充值管理对象 sp_recharge_record
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@Data
public class SpRechargeRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    @TableId(type = IdType.AUTO)
    private Long rechargeRecordId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 充值类型（1.vip月卡，2.VIP季卡，3.VIP年卡，4.活动赠送，5.推广奖励，6.兑换码，7.金币充值，8半年卡） */
    @Excel(name = "充值类型", readConverterExp = "1=.vip月卡，2.VIP季卡，3.VIP年卡，4.活动赠送，5.推广奖励，6.兑换码，7.金币充值，8半年卡")
    private Integer rechargeRecordType;

    /** 订单号 */
    @Excel(name = "订单号")
    private String rechargeRecordOrder;

    /** 充值状态 */
    @Excel(name = "充值状态")
    private Integer rechargeRecordState;

    /** 充值金额 */
    @Excel(name = "充值金额")
    private Long rechargeRecordMoney;
    /** 充值金额 */
    @Excel(name = "充值金额")
    private Long rechargeRecordNumber;

    /** 充值时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "充值时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date rechargeRecordTime;

    public void setRechargeRecordId(Long rechargeRecordId)
    {
        this.rechargeRecordId = rechargeRecordId;
    }

    public Long getRechargeRecordId()
    {
        return rechargeRecordId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setRechargeRecordType(Integer rechargeRecordType)
    {
        this.rechargeRecordType = rechargeRecordType;
    }

    public Integer getRechargeRecordType()
    {
        return rechargeRecordType;
    }
    public void setRechargeRecordOrder(String rechargeRecordOrder)
    {
        this.rechargeRecordOrder = rechargeRecordOrder;
    }

    public String getRechargeRecordOrder()
    {
        return rechargeRecordOrder;
    }
    public void setRechargeRecordState(Integer rechargeRecordState)
    {
        this.rechargeRecordState = rechargeRecordState;
    }

    public Integer getRechargeRecordState()
    {
        return rechargeRecordState;
    }
    public void setRechargeRecordMoney(Long rechargeRecordMoney)
    {
        this.rechargeRecordMoney = rechargeRecordMoney;
    }

    public Long getRechargeRecordMoney()
    {
        return rechargeRecordMoney;
    }
    public void setRechargeRecordTime(Date rechargeRecordTime)
    {
        this.rechargeRecordTime = rechargeRecordTime;
    }

    public Date getRechargeRecordTime()
    {
        return rechargeRecordTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rechargeRecordId", getRechargeRecordId())
            .append("userId", getUserId())
            .append("rechargeRecordType", getRechargeRecordType())
            .append("rechargeRecordOrder", getRechargeRecordOrder())
            .append("rechargeRecordState", getRechargeRecordState())
            .append("rechargeRecordMoney", getRechargeRecordMoney())
            .append("rechargeRecordTime", getRechargeRecordTime())
            .toString();
    }

    @TableField(exist = false)
    private Long type;//类型1，会员 2，金币
    @TableField(exist = false)
    private Long moneyType;//充值套餐类型

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(Long moneyType) {
        this.moneyType = moneyType;
    }
}
