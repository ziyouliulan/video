package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * banner图操作对象 sp_banner
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
public class SpBanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long bannerId;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String bannerImg;

    /** banner图状态（是否显示0.显示 1不显示） */
    @Excel(name = "banner图状态", readConverterExp = "是=否显示0.显示,1=不显示")
    private Integer bannerState;

    /** 视频id */
    @Excel(name = "视频id")
    private Long videoId;

    public void setBannerId(Long bannerId) 
    {
        this.bannerId = bannerId;
    }

    public Long getBannerId() 
    {
        return bannerId;
    }
    public void setBannerImg(String bannerImg) 
    {
        this.bannerImg = bannerImg;
    }

    public String getBannerImg() 
    {
        return bannerImg;
    }
    public void setBannerState(Integer bannerState) 
    {
        this.bannerState = bannerState;
    }

    public Integer getBannerState() 
    {
        return bannerState;
    }
    public void setVideoId(Long videoId) 
    {
        this.videoId = videoId;
    }

    public Long getVideoId() 
    {
        return videoId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bannerId", getBannerId())
            .append("bannerImg", getBannerImg())
            .append("bannerState", getBannerState())
            .append("videoId", getVideoId())
            .toString();
    }
}
