package com.ruoyi.xqsp.domain.entity;

import io.swagger.annotations.ApiImplicitParam;
import lombok.Data;

@Data
public class VideoSort {

    private Integer sort;
    private Long videoCategoryId;
    private Long varietyId;
    private Long actorId;
    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数
    private String videoName;


}
