package com.ruoyi.xqsp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActorMessage {
   private SpActorMessage spActorMessage;
   private String actorName;
}
