package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 视频广告对象 sp_video_advertisement
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public class SpVideoAdvertisement
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long videoAdvertisementId;

    /** 广告视频地址 */
    @Excel(name = "广告视频地址")
    private String videoAdvertisementUrl;

    /** 广告视频链接 */
    @Excel(name = "广告视频链接")
    private String videoAdvertisementLink;

    public void setVideoAdvertisementId(Long videoAdvertisementId) 
    {
        this.videoAdvertisementId = videoAdvertisementId;
    }

    public Long getVideoAdvertisementId() 
    {
        return videoAdvertisementId;
    }
    public void setVideoAdvertisementUrl(String videoAdvertisementUrl) 
    {
        this.videoAdvertisementUrl = videoAdvertisementUrl;
    }

    public String getVideoAdvertisementUrl() 
    {
        return videoAdvertisementUrl;
    }
    public void setVideoAdvertisementLink(String videoAdvertisementLink) 
    {
        this.videoAdvertisementLink = videoAdvertisementLink;
    }

    public String getVideoAdvertisementLink() 
    {
        return videoAdvertisementLink;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("videoAdvertisementId", getVideoAdvertisementId())
            .append("videoAdvertisementUrl", getVideoAdvertisementUrl())
            .append("videoAdvertisementLink", getVideoAdvertisementLink())
            .toString();
    }
}
