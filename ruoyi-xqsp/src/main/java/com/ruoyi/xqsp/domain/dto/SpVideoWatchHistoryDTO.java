package com.ruoyi.xqsp.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;

import java.util.Date;

public class SpVideoWatchHistoryDTO {


    /** 编号 */
    private Long videoWatchHistoryId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 观看历史类型id（视频id，综艺视频id） */
    @Excel(name = "观看历史类型id", readConverterExp = "视=频id，综艺视频id")
    private Long videoWatchHistoryTypeId;

    /** 观看历史类型(1.视频 2.综艺视频) */
    @Excel(name = "观看历史类型(1.视频 2.综艺视频)")
    private Integer videoWatchHistoryType;
    /** 时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date videoWatchHistoryTime;

}
