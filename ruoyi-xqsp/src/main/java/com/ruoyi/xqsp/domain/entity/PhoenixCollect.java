package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpPhoenixService;
import lombok.Data;

import java.util.List;

/**
 * 楼凤收藏
 */
@Data
public class PhoenixCollect {
    private Long userId;
    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数
    private SpPhoenixService spPhoenixService;
}
