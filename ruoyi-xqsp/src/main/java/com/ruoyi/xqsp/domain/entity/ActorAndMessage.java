package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.domain.SpActorMessage;
import lombok.Data;

/**
 * 演员和演员信息
 */
@Data
public class ActorAndMessage {
    private SpActorMessage spActorMessage;
    private SpActor spActor;
    private Long actorCollect;
}
