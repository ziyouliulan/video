package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.time.LocalDateTime;

/**
 * 演员管理对象 sp_actor
 *
 * @author ruoyi
 * @date 2021-04-17
 */
@Data
public class SpActor  {
    private static final long serialVersionUID = 1L;

    /**
     * 演员id
     */
    private Long actorId;

    /**
     * 演员名称
     */
    @Excel(name = "演员名称")
    private String actorName;

    /**
     * 演员头像
     */
    @Excel(name = "演员头像")
    private String actorHeadImg;

    /**
     * 演员封面
     */
    @Excel(name = "演员封面")
    private String actorCoverImg;

    /**
     * 演员背景
     */
    @Excel(name = "演员背景")
    private String actorBg;

    /**
     * 演员简介
     */
    @Excel(name = "演员简介")
    private String actorIntroduce;

    /**
     * 演员点击次数
     */
    @Excel(name = "演员点击次数")
    private Long actorClick;


    @TableField(exist = false)
    private Integer videoWatchNumber;



    /*
    推荐
     */
    private Boolean isTop;

    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    /**
     * 演员标签
     */
    @Excel(name = "演员标签")
    private String actorTag;

    public void setActorBg(String actorBg) {
        this.actorBg = actorBg;
    }

    public String getActorBg() {
        return actorBg;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public Long getActorId() {
        return actorId;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorHeadImg(String actorHeadImg) {
        this.actorHeadImg = actorHeadImg;
    }

    public String getActorHeadImg() {
        return actorHeadImg;
    }

    public void setActorCoverImg(String actorCoverImg) {
        this.actorCoverImg = actorCoverImg;
    }

    public String getActorCoverImg() {
        return actorCoverImg;
    }

    public void setActorIntroduce(String actorIntroduce) {
        this.actorIntroduce = actorIntroduce;
    }

    public String getActorIntroduce() {
        return actorIntroduce;
    }

    public void setActorClick(Long actorClick) {
        this.actorClick = actorClick;
    }

    public Long getActorClick() {
        return actorClick;
    }

    public void setActorTag(String actorTag) {
        this.actorTag = actorTag;
    }

    public String getActorTag() {
        return actorTag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("actorId", getActorId())
                .append("actorName", getActorName())
                .append("actorHeadImg", getActorHeadImg())
                .append("actorCoverImg", getActorCoverImg())
                .append("actorBg", getActorBg())
                .append("actorIntroduce", getActorIntroduce())
                .append("actorClick", getActorClick())
                .append("actorTag", getActorTag())
                .toString();
    }
}
