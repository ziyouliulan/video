package com.ruoyi.xqsp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 楼凤举报对象 phoenix_report
 * 
 * @author ruoyi
 * @date 2021-05-21
 */
public class PhoenixReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 举报id */
    private Long reportId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 楼凤id */
    @Excel(name = "楼凤id")
    private Long phoenixId;

    /** 楼凤状态 */
    @Excel(name = "楼凤状态")
    private Long phoenixState;

    /** 举报内容 */
    @Excel(name = "举报内容")
    private String reportContent;

    public void setReportId(Long reportId) 
    {
        this.reportId = reportId;
    }

    public Long getReportId() 
    {
        return reportId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPhoenixId(Long phoenixId) 
    {
        this.phoenixId = phoenixId;
    }

    public Long getPhoenixId() 
    {
        return phoenixId;
    }
    public void setPhoenixState(Long phoenixState) 
    {
        this.phoenixState = phoenixState;
    }

    public Long getPhoenixState() 
    {
        return phoenixState;
    }
    public void setReportContent(String reportContent) 
    {
        this.reportContent = reportContent;
    }

    public String getReportContent() 
    {
        return reportContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("reportId", getReportId())
            .append("userId", getUserId())
            .append("phoenixId", getPhoenixId())
            .append("phoenixState", getPhoenixState())
            .append("reportContent", getReportContent())
            .toString();
    }
}
