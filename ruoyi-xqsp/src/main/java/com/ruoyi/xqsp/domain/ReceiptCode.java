package com.ruoyi.xqsp.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * @TableName sp_receipt_code
 */
@TableName(value ="sp_receipt_code")
@Data
public class ReceiptCode implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 是否显示
     */
    private Integer isShow;

    /**
     * 收款码
     */
    private String receiptCode;

    /**
     * 收款人姓名
     */
    private String payeeName;

    /**
     * 收款人 账号
     */
    private String payeeAccount;

    /**
     * 示例
     */
    private String demonstrate;

    /**
     * 开户行
     */
    private String bank;


    private String status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
