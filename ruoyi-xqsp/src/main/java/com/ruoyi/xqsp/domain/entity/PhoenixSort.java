package com.ruoyi.xqsp.domain.entity;

import lombok.Data;

/**
 * 楼凤排序
 */
@Data
public class PhoenixSort {
    private Integer type;
    private String area;
    private Integer pageNums;//分页页数
    private Integer pageSize;//分页条数
}
