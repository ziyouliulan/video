package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpAdviceFeedback;
import lombok.Data;

/**
 * 用户反馈信息
 */
@Data
public class UserFeedback {

    private SpAdviceFeedback spAdviceFeedback;
    private String userName;
}
