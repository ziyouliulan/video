package com.ruoyi.xqsp.domain.entity;

import com.ruoyi.xqsp.domain.SpPhoenixService;
import lombok.Data;

@Data
public class PhoenixCalendar {
    private Long userId;
    private Long phoenixId;
    private Long unlock;
    private Long collect;
    private SpPhoenixService spPhoenixService;
}
