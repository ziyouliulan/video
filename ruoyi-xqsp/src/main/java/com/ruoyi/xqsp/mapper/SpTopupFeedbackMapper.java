package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpTopupFeedback;

/**
 * 充值反馈Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
public interface SpTopupFeedbackMapper 
{
    /**
     * 查询充值反馈
     * 
     * @param topupFeedbackId 充值反馈ID
     * @return 充值反馈
     */
    public SpTopupFeedback selectSpTopupFeedbackById(Long topupFeedbackId);

    /**
     * 查询充值反馈列表
     * 
     * @param spTopupFeedback 充值反馈
     * @return 充值反馈集合
     */
    public List<SpTopupFeedback> selectSpTopupFeedbackList(SpTopupFeedback spTopupFeedback);

    /**
     * 新增充值反馈
     * 
     * @param spTopupFeedback 充值反馈
     * @return 结果
     */
    public int insertSpTopupFeedback(SpTopupFeedback spTopupFeedback);

    /**
     * 修改充值反馈
     * 
     * @param spTopupFeedback 充值反馈
     * @return 结果
     */
    public int updateSpTopupFeedback(SpTopupFeedback spTopupFeedback);

    /**
     * 删除充值反馈
     * 
     * @param topupFeedbackId 充值反馈ID
     * @return 结果
     */
    public int deleteSpTopupFeedbackById(Long topupFeedbackId);

    /**
     * 批量删除充值反馈
     * 
     * @param topupFeedbackIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpTopupFeedbackByIds(Long[] topupFeedbackIds);

    List<SpTopupFeedback> selectSpTopupFeedbackByUserId(Long userId);
}
