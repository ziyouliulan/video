package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpActor;
import com.ruoyi.xqsp.domain.SpAdvertising;

/**
 * 演员管理Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-17
 */
public interface SpActorMapper  extends BaseMapper<SpActor>
{
    /**
     * 查询演员管理
     *
     * @param actorId 演员管理ID
     * @return 演员管理
     */
    public SpActor selectSpActorById(Long actorId);

    /**
     * 查询演员管理列表
     *
     * @param spActor 演员管理
     * @return 演员管理集合
     */
    public List<SpActor> selectSpActorList(SpActor spActor);






    /**
     * 新增演员管理
     *
     * @param spActor 演员管理
     * @return 结果
     */
    public int insertSpActor(SpActor spActor);

    /**
     * 修改演员管理
     *
     * @param spActor 演员管理
     * @return 结果
     */
    public int updateSpActor(SpActor spActor);

    /**
     * 删除演员管理
     *
     * @param actorId 演员管理ID
     * @return 结果
     */
    public int deleteSpActorById(Long actorId);

    /**
     * 批量删除演员管理
     *
     * @param actorIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpActorByIds(Long[] actorIds);

    List<SpActor> selectSpActorLists(SpActor spActor);
}
