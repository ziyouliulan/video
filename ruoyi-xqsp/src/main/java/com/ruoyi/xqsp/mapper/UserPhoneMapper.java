package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.UserPhone;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【user_phone】的数据库操作Mapper
* @createDate 2022-07-26 20:34:55
* @Entity com.ruoyi.xqsp.domain.UserPhone
*/
public interface UserPhoneMapper extends BaseMapper<UserPhone> {

}




