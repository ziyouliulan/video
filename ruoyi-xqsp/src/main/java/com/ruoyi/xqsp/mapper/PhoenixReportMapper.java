package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.PhoenixReport;
import org.apache.ibatis.annotations.Param;

/**
 * 楼凤举报Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-21
 */
public interface PhoenixReportMapper 
{
    /**
     * 查询楼凤举报
     * 
     * @param reportId 楼凤举报ID
     * @return 楼凤举报
     */
    public PhoenixReport selectPhoenixReportById(Long reportId);

    /**
     * 查询楼凤举报列表
     * 
     * @param phoenixReport 楼凤举报
     * @return 楼凤举报集合
     */
    public List<PhoenixReport> selectPhoenixReportList(PhoenixReport phoenixReport);

    /**
     * 新增楼凤举报
     * 
     * @param phoenixReport 楼凤举报
     * @return 结果
     */
    public int insertPhoenixReport(PhoenixReport phoenixReport);

    /**
     * 修改楼凤举报
     * 
     * @param phoenixReport 楼凤举报
     * @return 结果
     */
    public int updatePhoenixReport(PhoenixReport phoenixReport);

    /**
     * 删除楼凤举报
     * 
     * @param reportId 楼凤举报ID
     * @return 结果
     */
    public int deletePhoenixReportById(Long reportId);

    /**
     * 批量删除楼凤举报
     * 
     * @param reportIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePhoenixReportByIds(Long[] reportIds);

    int updatePhoenixReportByPhoenixId(@Param("phoenixId") Long phoenixId, @Param("phoenixState") String phoenixState);
}
