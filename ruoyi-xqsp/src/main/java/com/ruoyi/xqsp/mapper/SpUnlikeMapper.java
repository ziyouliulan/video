package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpUnlike;
import org.apache.ibatis.annotations.Param;

/**
 * 差评Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-08
 */
public interface SpUnlikeMapper 
{
    /**
     * 查询差评
     * 
     * @param unlikeId 差评ID
     * @return 差评
     */
    public SpUnlike selectSpUnlikeById(Long unlikeId);

    /**
     * 查询差评列表
     * 
     * @param spUnlike 差评
     * @return 差评集合
     */
    public List<SpUnlike> selectSpUnlikeList(SpUnlike spUnlike);

    /**
     * 新增差评
     * 
     * @param spUnlike 差评
     * @return 结果
     */
    public int insertSpUnlike(SpUnlike spUnlike);

    /**
     * 修改差评
     * 
     * @param spUnlike 差评
     * @return 结果
     */
    public int updateSpUnlike(SpUnlike spUnlike);

    /**
     * 删除差评
     * 
     * @param unlikeId 差评ID
     * @return 结果
     */
    public int deleteSpUnlikeById(Long unlikeId);

    /**
     * 批量删除差评
     * 
     * @param unlikeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpUnlikeByIds(Long[] unlikeIds);

    int deleteSpUnlike(SpUnlike spUnlike);

    SpUnlike selectSpUnlikeByIds(@Param("userId") Long userId, @Param("videoId") Long videoId);

    SpUnlike selectSpUnlikeByVarietyVideoId(@Param("userId") Long userId, @Param("varietyVideoId") Long varietyVideoId);

    int deleteVarietyVideoUnlike(SpUnlike spUnlike);
}
