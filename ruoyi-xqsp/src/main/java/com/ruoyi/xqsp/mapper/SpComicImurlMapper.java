package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpComicImurl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_comic_imurl】的数据库操作Mapper
* @createDate 2022-04-01 14:41:26
* @Entity com.ruoyi.xqsp.domain.SpComicImurl
*/
@Mapper
public interface SpComicImurlMapper extends BaseMapper<SpComicImurl> {

    List<SpComicImurl> orderList(@Param("comicChapterId") Long comicChapterId,@Param("comicId") Long comicId);

}




