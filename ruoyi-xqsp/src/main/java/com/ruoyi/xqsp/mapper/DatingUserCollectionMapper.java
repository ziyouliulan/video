package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.DatingUserCollection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_dating_user_collection】的数据库操作Mapper
* @createDate 2022-02-28 15:10:23
* @Entity com.ruoyi.xqsp.domain.DatingUserCollection
*/
public interface DatingUserCollectionMapper extends BaseMapper<DatingUserCollection> {

}




