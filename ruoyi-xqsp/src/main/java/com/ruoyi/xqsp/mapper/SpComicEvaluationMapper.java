package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpComicEvaluation;

/**
 * 漫画评价Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-30
 */
public interface SpComicEvaluationMapper 
{
    /**
     * 查询漫画评价
     * 
     * @param comicEvaluationId 漫画评价ID
     * @return 漫画评价
     */
    public SpComicEvaluation selectSpComicEvaluationById(Long comicEvaluationId);

    /**
     * 查询漫画评价列表
     * 
     * @param spComicEvaluation 漫画评价
     * @return 漫画评价集合
     */
    public List<SpComicEvaluation> selectSpComicEvaluationList(SpComicEvaluation spComicEvaluation);

    /**
     * 新增漫画评价
     * 
     * @param spComicEvaluation 漫画评价
     * @return 结果
     */
    public int insertSpComicEvaluation(SpComicEvaluation spComicEvaluation);

    /**
     * 修改漫画评价
     * 
     * @param spComicEvaluation 漫画评价
     * @return 结果
     */
    public int updateSpComicEvaluation(SpComicEvaluation spComicEvaluation);

    /**
     * 删除漫画评价
     * 
     * @param comicEvaluationId 漫画评价ID
     * @return 结果
     */
    public int deleteSpComicEvaluationById(Long comicEvaluationId);

    /**
     * 批量删除漫画评价
     * 
     * @param comicEvaluationIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpComicEvaluationByIds(Long[] comicEvaluationIds);

    Integer selectSpComicEvaluationCountById(Long comicId);

    List<SpComicEvaluation> selectSpComicEvaluationByComicId(Long comicId);

    int updateSpComicEvaluationLike(Long comicEvaluationId);

    int updateSpComicEvaluationLike1(Long comicEvaluationId);
}
