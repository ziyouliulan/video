package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.PaymentChannel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【payment_channel】的数据库操作Mapper
* @createDate 2022-05-26 22:45:12
* @Entity com.ruoyi.xqsp.domain.PaymentChannel
*/
public interface PaymentChannelMapper extends BaseMapper<PaymentChannel> {

}




