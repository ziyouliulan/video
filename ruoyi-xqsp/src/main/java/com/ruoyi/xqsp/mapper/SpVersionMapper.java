package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVersion;

/**
 * 版本设置Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-02
 */
public interface SpVersionMapper 
{
    /**
     * 查询版本设置
     * 
     * @param versionId 版本设置ID
     * @return 版本设置
     */
    public SpVersion selectSpVersionById(Long versionId);

    /**
     * 查询版本设置列表
     * 
     * @param spVersion 版本设置
     * @return 版本设置集合
     */
    public List<SpVersion> selectSpVersionList(SpVersion spVersion);

    /**
     * 新增版本设置
     * 
     * @param spVersion 版本设置
     * @return 结果
     */
    public int insertSpVersion(SpVersion spVersion);

    /**
     * 修改版本设置
     * 
     * @param spVersion 版本设置
     * @return 结果
     */
    public int updateSpVersion(SpVersion spVersion);

    /**
     * 删除版本设置
     * 
     * @param versionId 版本设置ID
     * @return 结果
     */
    public int deleteSpVersionById(Long versionId);

    /**
     * 批量删除版本设置
     * 
     * @param versionIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVersionByIds(Long[] versionIds);
}
