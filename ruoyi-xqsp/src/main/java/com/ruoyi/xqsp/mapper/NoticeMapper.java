package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sys_notice(通知公告表)】的数据库操作Mapper
* @createDate 2022-06-12 21:42:33
* @Entity com.ruoyi.xqsp.domain.Notice
*/
public interface NoticeMapper extends BaseMapper<Notice> {

}




