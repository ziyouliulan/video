package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpHotSearch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_hot_search】的数据库操作Mapper
* @createDate 2022-06-18 16:21:38
* @Entity com.ruoyi.xqsp.domain.SpHotSearch
*/
public interface SpHotSearchMapper extends BaseMapper<SpHotSearch> {

}




