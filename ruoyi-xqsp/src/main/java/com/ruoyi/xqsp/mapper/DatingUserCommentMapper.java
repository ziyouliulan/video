package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.DatingUserComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_dating_user_comment】的数据库操作Mapper
* @createDate 2022-02-23 17:25:50
* @Entity com.ruoyi.xqsp.domain.DatingUserComment
*/
@Mapper
public interface DatingUserCommentMapper extends BaseMapper<DatingUserComment> {

    public List<DatingUserComment> byIdList(Long id);


}




