package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpLike;
import org.apache.ibatis.annotations.Param;

/**
 * 用户点赞Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
public interface SpLikeMapper 
{
    /**
     * 查询用户点赞
     * 
     * @param likeId 用户点赞ID
     * @return 用户点赞
     */
    public SpLike selectSpLikeById(Long likeId);

    /**
     * 查询用户点赞列表
     * 
     * @param spLike 用户点赞
     * @return 用户点赞集合
     */
    public List<SpLike> selectSpLikeList(SpLike spLike);

    /**
     * 新增用户点赞
     * 
     * @param spLike 用户点赞
     * @return 结果
     */
    public int insertSpLike(SpLike spLike);

    /**
     * 修改用户点赞
     * 
     * @param spLike 用户点赞
     * @return 结果
     */
    public int updateSpLike(SpLike spLike);

    /**
     * 删除用户点赞
     * 
     * @param likeId 用户点赞ID
     * @return 结果
     */
    public int deleteSpLikeById(Long likeId);

    /**
     * 批量删除用户点赞
     * 
     * @param likeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpLikeByIds(Long[] likeIds);

    SpLike selectSpLike(SpLike spLike);

    int deleteSpLike(SpLike spLike);

    SpLike selectSpLikeByIds(@Param("userId") Long userId,@Param("videoId") Long videoId);

    SpLike selectSpLikeByVarietyVideoId(@Param("userId") Long userId, @Param("varietyVideoId") Long varietyVideoId);
}
