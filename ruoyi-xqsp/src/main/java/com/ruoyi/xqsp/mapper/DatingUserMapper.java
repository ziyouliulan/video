package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.DatingUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author lpden
* @description 针对表【sp_dating_user(用户信息表)】的数据库操作Mapper
* @createDate 2022-07-04 21:31:09
* @Entity com.ruoyi.xqsp.domain.DatingUser
*/
@Mapper
public interface DatingUserMapper extends BaseMapper<DatingUser> {

    public List<DatingUser> favoritesList(Integer userId);

}




