package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpCarmichael;

/**
 * 卡密操作Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public interface SpCarmichaelMapper 
{
    /**
     * 查询卡密操作
     * 
     * @param carmichaelId 卡密操作ID
     * @return 卡密操作
     */
    public SpCarmichael selectSpCarmichaelById(Long carmichaelId);

    /**
     * 查询卡密操作列表
     * 
     * @param spCarmichael 卡密操作
     * @return 卡密操作集合
     */
    public List<SpCarmichael> selectSpCarmichaelList(SpCarmichael spCarmichael);

    /**
     * 新增卡密操作
     * 
     * @param spCarmichael 卡密操作
     * @return 结果
     */
    public int insertSpCarmichael(SpCarmichael spCarmichael);

    /**
     * 修改卡密操作
     * 
     * @param spCarmichael 卡密操作
     * @return 结果
     */
    public int updateSpCarmichael(SpCarmichael spCarmichael);

    /**
     * 删除卡密操作
     * 
     * @param carmichaelId 卡密操作ID
     * @return 结果
     */
    public int deleteSpCarmichaelById(Long carmichaelId);

    /**
     * 批量删除卡密操作
     * 
     * @param carmichaelIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpCarmichaelByIds(Long[] carmichaelIds);

    SpCarmichael selectSpCarmichaelByName(String code);
}
