package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpKey;

/**
 * 激活码表Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-14
 */
public interface SpKeyMapper 
{
    /**
     * 查询激活码表
     * 
     * @param keyId 激活码表ID
     * @return 激活码表
     */
    public SpKey selectSpKeyById(Long keyId);

    /**
     * 查询激活码表列表
     * 
     * @param spKey 激活码表
     * @return 激活码表集合
     */
    public List<SpKey> selectSpKeyList(SpKey spKey);

    /**
     * 新增激活码表
     * 
     * @param spKey 激活码表
     * @return 结果
     */
    public int insertSpKey(SpKey spKey);

    /**
     * 修改激活码表
     * 
     * @param spKey 激活码表
     * @return 结果
     */
    public int updateSpKey(SpKey spKey);

    /**
     * 删除激活码表
     * 
     * @param keyId 激活码表ID
     * @return 结果
     */
    public int deleteSpKeyById(Long keyId);

    /**
     * 批量删除激活码表
     * 
     * @param keyIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpKeyByIds(Long[] keyIds);

    SpKey selectSpKeyByCode(String code);
}
