package com.ruoyi.xqsp.mapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpUsers;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户表Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-26
 */
@Mapper
public interface SpUsersMapper extends BaseMapper<SpUsers>
{
    /**
     * 查询用户表
     *
     * @param userId 用户表ID
     * @return 用户表
     */
    public SpUsers selectSpUsersById(Long userId);

    /**
     * 查询用户表列表
     *
     * @param spUsers 用户表
     * @return 用户表集合
     */
    public List<SpUsers> selectSpUsersList(SpUsers spUsers);

    /**
     * 新增用户表
     *
     * @param spUsers 用户表
     * @return 结果
     */
    public int insertSpUsers(SpUsers spUsers);

    /**
     * 修改用户表
     *
     * @param spUsers 用户表
     * @return 结果
     */
    public int updateSpUsers(SpUsers spUsers);

    /**
     * 删除用户表
     *
     * @param userId 用户表ID
     * @return 结果
     */
    public int deleteSpUsersById(Long userId);

    /**
     * 批量删除用户表
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpUsersByIds(Long[] userIds);

    /**
     * \获取月新增
     * @param spUsers
     * @return
     */
    List<SpUsers> selectSpUsersListMonth(SpUsers spUsers);

    /**
     * 判断xqid是否已存在
     * @param xqid
     * @return
     */
    SpUsers selectSpUsersByXqId(String xqid);

    /**
     * 通过注册用户的邀请码查询邀请该注册用户的用户
     * @param userPromoteCode
     * @return
     */
    int selectSpUsersCountByXqId(String userPromoteCode);

    /**
     * 根据xqid修改会员天数
     * @param userPromoteCode
     * @param days
     * @return
     */
    int updateSpUsersByxqid(String userPromoteCode, int days);

    List<SpUsers> selectSpUsersListByXqid(String userXqid);

    SpUsers selectSpUserById(Long userId);

    int updateSpUsersById(Long userId);

    int updateUserGlod(@Param("userId") Long userId, @Param("expenseCalendarGlod") BigDecimal expenseCalendarGlod);

    int updatEmembersDay();

    int refreshUserVipType();

    int updateSpUsersWelfareById(Long userId);

    int updateSpUsersTodayLongById(Long userId);

    int updateSpUsersTodayShortById(Long userId);

    int updateSpUsersBuyTypeById(Long userId);

    int updateSpUsersBuyTypeById1(Long userId);

    int updatMembersDayByUserId(@Param("userId") Long userId, @Param("days") Long days);

    int updatGlodByUserId(@Param("userId") Long userId,@Param("glod") Long glod);

    int updateSpUsersWelfare(@Param("userId") Long userId, @Param("carmichaelDays") Long carmichaelDays);

    int updateSpUsersWelfareLong(@Param("userId") Long userId, @Param("carmichaelDays") Long carmichaelDays);

    int updateSpUsersMg(SpUsers spUsers);

    int updateUserLoginState(@Param("userId") Long userId,@Param("date") Date date);

    List<SpUsers> selectSpUsersList1();

    int updateUserLoginState1(Long userId);

    int updateUserLoginState12(Long userId);
}
