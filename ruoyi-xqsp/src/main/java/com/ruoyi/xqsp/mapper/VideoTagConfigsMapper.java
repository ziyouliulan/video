package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.VideoTagConfigs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author lpden
* @description 针对表【sys_video_tag_configs(视频分类)】的数据库操作Mapper
* @createDate 2022-07-09 22:08:05
* @Entity com.ruoyi.xqsp.domain.VideoTagConfigs
*/
@Mapper
public interface VideoTagConfigsMapper extends BaseMapper<VideoTagConfigs> {

    List<Map> category(@Param("id") Integer id);
}




