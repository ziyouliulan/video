package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;

/**
 * 视频评价Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-24
 */
public interface SpVideoEvaluationMapper 
{
    /**
     * 查询视频评价
     * 
     * @param videoEvaluationId 视频评价ID
     * @return 视频评价
     */
    public SpVideoEvaluation selectSpVideoEvaluationById(Long videoEvaluationId);

    /**
     * 查询视频评价列表
     * 
     * @param spVideoEvaluation 视频评价
     * @return 视频评价集合
     */
    public List<SpVideoEvaluation> selectSpVideoEvaluationList(SpVideoEvaluation spVideoEvaluation);

    /**
     * 新增视频评价
     * 
     * @param spVideoEvaluation 视频评价
     * @return 结果
     */
    public int insertSpVideoEvaluation(SpVideoEvaluation spVideoEvaluation);

    /**
     * 修改视频评价
     * 
     * @param spVideoEvaluation 视频评价
     * @return 结果
     */
    public int updateSpVideoEvaluation(SpVideoEvaluation spVideoEvaluation);

    /**
     * 删除视频评价
     * 
     * @param videoEvaluationId 视频评价ID
     * @return 结果
     */
    public int deleteSpVideoEvaluationById(Long videoEvaluationId);

    /**
     * 批量删除视频评价
     * 
     * @param videoEvaluationIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVideoEvaluationByIds(Long[] videoEvaluationIds);

    /**
     * 根据视频id查询评价
     * @param videoId
     * @return
     */
    List<SpVideoEvaluation> selectSpVideoEvaluationListByVideoId(Long videoId);

    int updateSpVideoEvaluationLike(Long videoEvaluationId);

    int updateSpVideoEvaluationLike1(Long videoEvaluationId);

    Long selectCountById(Long videoId);

    int deleteSpVideoEvaluationByVideoId(Long videoId);
}
