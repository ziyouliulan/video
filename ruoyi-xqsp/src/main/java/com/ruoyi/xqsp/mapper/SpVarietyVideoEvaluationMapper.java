package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.ruoyi.xqsp.domain.SpVarietyVideo;
import com.ruoyi.xqsp.domain.SpVarietyVideoEvaluation;
import com.ruoyi.xqsp.domain.SpVideoEvaluation;

/**
 * 综艺视频评论Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-07
 */
public interface SpVarietyVideoEvaluationMapper
{
    /**
     * 查询综艺视频评论
     *
     * @param varietyVideoEvaluationId 综艺视频评论ID
     * @return 综艺视频评论
     */
    public SpVarietyVideoEvaluation selectSpVarietyVideoEvaluationById(Long varietyVideoEvaluationId);

    /**
     * 查询综艺视频评论列表
     *
     * @param id 综艺视频评论
     * @return 综艺视频评论集合
     */
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationList(SpVarietyVideoEvaluation  spVarietyVideoEvaluation);

    /**
     * 查询综艺视频评论列表
     *
     * @param id 综艺视频评论
     * @return 综艺视频评论集合
     */
    public List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationListV2(Integer  id);

    /**
     * 新增综艺视频评论
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 结果
     */
    public int insertSpVarietyVideoEvaluation(SpVarietyVideoEvaluation spVarietyVideoEvaluation);

    /**
     * 修改综艺视频评论
     *
     * @param spVarietyVideoEvaluation 综艺视频评论
     * @return 结果
     */
    public int updateSpVarietyVideoEvaluation(SpVarietyVideoEvaluation spVarietyVideoEvaluation);

    /**
     * 删除综艺视频评论
     *
     * @param varietyVideoEvaluationId 综艺视频评论ID
     * @return 结果
     */
    public int deleteSpVarietyVideoEvaluationById(Long varietyVideoEvaluationId);

    /**
     * 批量删除综艺视频评论
     *
     * @param varietyVideoEvaluationIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVarietyVideoEvaluationByIds(Long[] varietyVideoEvaluationIds);

    Long selectCountById(Long varietyVideoId);

    List<SpVarietyVideoEvaluation> selectSpVarietyVideoEvaluationByIds(Long varietyVideoId);

    int updateSpVarietyVideoEvaluationLike(Long varietyVideoEvaluationId);

    int updateSpVarietyVideoEvaluationLike1(Long varietyVideoEvaluationId);

    int deleteSpVarietyVideoEvaluationByVvId(Long varietyVideoId);
}



