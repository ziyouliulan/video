package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpHeadImg;

/**
 * 用户头像Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface SpHeadImgMapper 
{
    /**
     * 查询用户头像
     * 
     * @param headImgId 用户头像ID
     * @return 用户头像
     */
    public SpHeadImg selectSpHeadImgById(Long headImgId);

    /**
     * 查询用户头像列表
     * 
     * @param spHeadImg 用户头像
     * @return 用户头像集合
     */
    public List<SpHeadImg> selectSpHeadImgList(SpHeadImg spHeadImg);

    /**
     * 新增用户头像
     * 
     * @param spHeadImg 用户头像
     * @return 结果
     */
    public int insertSpHeadImg(SpHeadImg spHeadImg);

    /**
     * 修改用户头像
     * 
     * @param spHeadImg 用户头像
     * @return 结果
     */
    public int updateSpHeadImg(SpHeadImg spHeadImg);

    /**
     * 删除用户头像
     * 
     * @param headImgId 用户头像ID
     * @return 结果
     */
    public int deleteSpHeadImgById(Long headImgId);

    /**
     * 批量删除用户头像
     * 
     * @param headImgIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpHeadImgByIds(Long[] headImgIds);

    SpHeadImg selectSpHeadImg();

}
