package com.ruoyi.xqsp.mapper;

import java.util.List;
import java.util.Set;

import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;
import com.ruoyi.xqsp.domain.SpPhoenixService;

/**
 * 楼凤操作Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-26
 */
public interface SpPhoenixServiceMapper 
{
    /**
     * 查询楼凤操作
     * 
     * @param phoenixId 楼凤操作ID
     * @return 楼凤操作
     */
    public SpPhoenixService selectSpPhoenixServiceById(Long phoenixId);

    /**
     * 查询楼凤操作列表
     * 
     * @param spPhoenixService 楼凤操作
     * @return 楼凤操作集合
     */
    public List<SpPhoenixService> selectSpPhoenixServiceList(SpPhoenixService spPhoenixService);

    /**
     * 新增楼凤操作
     * 
     * @param spPhoenixService 楼凤操作
     * @return 结果
     */
    public int insertSpPhoenixService(SpPhoenixService spPhoenixService);

    /**
     * 修改楼凤操作
     * 
     * @param spPhoenixService 楼凤操作
     * @return 结果
     */
    public int updateSpPhoenixService(SpPhoenixService spPhoenixService);

    /**
     * 删除楼凤操作
     * 
     * @param phoenixId 楼凤操作ID
     * @return 结果
     */
    public int deleteSpPhoenixServiceById(Long phoenixId);

    /**
     * 批量删除楼凤操作
     * 
     * @param phoenixIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpPhoenixServiceByIds(Long[] phoenixIds);

    /**
     * 审核
     * @param spPhoenixService
     * @return
     */
    List<SpPhoenixService> selectSpPhoenixServiceListAudit(SpPhoenixService spPhoenixService);

    List<SpPhoenixService> selectSpPhoenixService();

    int updateSpPhoenixServiceLike();

    int updateSpPhoenixServiceLike1(Long phoenixId);

    List<SpPhoenixService> selectSpPhoenixServiceOrderTime();

    List<SpPhoenixService> selectSpPhoenixServiceByAddress(String area);

    int updateSpPhoenixServiceUnlockNum(Long expenseCalendarState);

    List<SpPhoenixService> selectSpPhoenixServiceByName(String phoenixName);

    List<SpPhoenixService> selectSpPhoenixServiceByAddressOrderTime(String area);

    List<SpPhoenixService> selectSpPhoenixServiceListByIds(Set<Long> ids1);
}
