package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVarietyVideo;


/**
 * 综艺视频Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
public interface SpVarietyVideoMapper 
{
    /**
     * 查询综艺视频
     * 
     * @param varietyVideoId 综艺视频ID
     * @return 综艺视频
     */
    public SpVarietyVideo selectSpVarietyVideoById(Long varietyVideoId);

    /**
     * 查询综艺视频列表
     * 
     * @param spVarietyVideo 综艺视频
     * @return 综艺视频集合
     */
    public List<SpVarietyVideo> selectSpVarietyVideoList(SpVarietyVideo spVarietyVideo);

    /**
     * 新增综艺视频
     * 
     * @param spVarietyVideo 综艺视频
     * @return 结果
     */
    public int insertSpVarietyVideo(SpVarietyVideo spVarietyVideo);

    /**
     * 修改综艺视频
     * 
     * @param spVarietyVideo 综艺视频
     * @return 结果
     */
    public int updateSpVarietyVideo(SpVarietyVideo spVarietyVideo);

    /**
     * 删除综艺视频
     * 
     * @param varietyVideoId 综艺视频ID
     * @return 结果
     */
    public int deleteSpVarietyVideoById(Long varietyVideoId);

    /**
     * 批量删除综艺视频
     * 
     * @param varietyVideoIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVarietyVideoByIds(Long[] varietyVideoIds);

    List<SpVarietyVideo> selectSpVarietyVideoByVarietyId(Long varietyId);

    Integer selectCountByVarietyId(Long varietyId);

    List<SpVarietyVideo> selectSpVarietyVideoByVarietyIds(Long varietyId);

    int updateVideoLikeNumber(Long varietyVideoId);

    int updateVideoLikeNumber1(Long varietyVideoId);

    int updateVideoUnLikeNumber1(Long varietyVideoId);

    int updateVideoUnLikeNumber(Long varietyVideoId);

    List<SpVarietyVideo> selectSpVarietyVideo();

    int updateVideoCollectNumber(Long varietyVideoId);

    int updateVideoCollectNumber1(Long varietyVideoId);

    int updateVideoWatchNumber(Long videoWatchHistoryTypeId);

    List<SpVarietyVideo> selectSpVarietyVideoLists(SpVarietyVideo spVarietyVideo);

    List<SpVarietyVideo> selectSpVarietyVideoListByDate(SpVarietyVideo spVarietyVideo);

    List<SpVarietyVideo> selectSpVarietyVideoListByWatch(SpVarietyVideo spVarietyVideo);

    List<SpVarietyVideo> selectSpVarietyVideoListByName(String name);
}
