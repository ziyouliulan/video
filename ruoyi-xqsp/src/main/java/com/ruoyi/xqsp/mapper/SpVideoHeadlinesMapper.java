package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpVideoHeadlines;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_video_headlines(视频详情)】的数据库操作Mapper
* @createDate 2022-06-18 15:47:38
* @Entity com.ruoyi.xqsp.domain.SpVideoHeadlines
*/
public interface SpVideoHeadlinesMapper extends BaseMapper<SpVideoHeadlines> {

}




