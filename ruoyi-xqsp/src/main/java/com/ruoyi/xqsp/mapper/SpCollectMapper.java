package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpCollect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户收藏Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-08
 */
@Mapper
public interface SpCollectMapper extends BaseMapper<SpCollect> {
    /**
     * 查询用户收藏
     *
     * @param collectId 用户收藏ID
     * @return 用户收藏
     */
    public SpCollect selectSpCollectById(Long collectId);

    /**
     * 查询用户收藏列表
     *
     * @param spCollect 用户收藏
     * @return 用户收藏集合
     */
    public List<SpCollect> selectSpCollectList(SpCollect spCollect);

    /**
     * 查询用户收藏列表
     *
     * @param spCollect 用户收藏
     * @return 用户收藏集合
     */
    public List<SpCollect> selectSpCollectListV2(SpCollect spCollect);

    /**
     * 新增用户收藏
     *
     * @param spCollect 用户收藏
     * @return 结果
     */
    public int insertSpCollect(SpCollect spCollect);

    /**
     * 修改用户收藏
     *
     * @param spCollect 用户收藏
     * @return 结果
     */
    public int updateSpCollect(SpCollect spCollect);

    /**
     * 删除用户收藏
     *
     * @param collectId 用户收藏ID
     * @return 结果
     */
    public int deleteSpCollectById(Long collectId);

    /**
     * 批量删除用户收藏
     *
     * @param collectIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpCollectByIds(Long[] collectIds);

    int deleteSpCollect(SpCollect spCollect);

    SpCollect selectSpCollectByIds(@Param("userId") Long userId, @Param("videoId") Long videoId);

    SpCollect selectSpCollectByVarietyVideoId(@Param("userId") Long userId, @Param("varietyVideoId") Long varietyVideoId);

    List<SpCollect> selectSpCollectByUserId(Long userId);

    SpCollect selectSpCollectByActorId(@Param("userId") Long userId,@Param("actorId") Long actorId);

    List<SpCollect> selectSpCollectActorByUserId(Long userId);

    int deleteSpCollect1(@Param("actorId") Long actorId,@Param("type") long type);
}
