package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpAdvertising;

/**
 * 广告位管理Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-17
 */
public interface SpAdvertisingMapper  extends BaseMapper<SpAdvertising>
{
    /**
     * 查询广告位管理
     *
     * @param advertisingId 广告位管理ID
     * @return 广告位管理
     */
    public SpAdvertising selectSpAdvertisingById(Long advertisingId);

    /**
     * 查询广告位管理列表
     *
     * @param spAdvertising 广告位管理
     * @return 广告位管理集合
     */
    public List<SpAdvertising> selectSpAdvertisingList(SpAdvertising spAdvertising);

    /**
     * 新增广告位管理
     *
     * @param spAdvertising 广告位管理
     * @return 结果
     */
    public int insertSpAdvertising(SpAdvertising spAdvertising);

    /**
     * 修改广告位管理
     *
     * @param spAdvertising 广告位管理
     * @return 结果
     */
    public int updateSpAdvertising(SpAdvertising spAdvertising);

    /**
     * 删除广告位管理
     *
     * @param advertisingId 广告位管理ID
     * @return 结果
     */
    public int deleteSpAdvertisingById(Long advertisingId);

    /**
     * 批量删除广告位管理
     *
     * @param advertisingIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpAdvertisingByIds(Long[] advertisingIds);

    List<SpAdvertising> selectSpAdvertisingByCategoryId(Long categoryId);

    List<SpAdvertising> selectSpAdvertising();

    int updateSpAdvertisingCount(Long advertisingId);

    List<SpAdvertising> selectSpAdvertisingPhoenix();

    List<SpAdvertising> selectSpAdvertisingByComic();

    List<SpAdvertising> selectSpAdvertisingAnime();
}
