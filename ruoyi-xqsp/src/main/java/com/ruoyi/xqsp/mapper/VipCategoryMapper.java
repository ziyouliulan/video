package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.VipCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_vip_category】的数据库操作Mapper
* @createDate 2022-02-25 16:15:06
* @Entity com.ruoyi.xqsp.domain.VipCategory
*/
public interface VipCategoryMapper extends BaseMapper<VipCategory> {

}




