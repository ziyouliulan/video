package com.ruoyi.xqsp.mapper;

import java.util.HashMap;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpVideo;
import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;
import org.apache.ibatis.annotations.Mapper;

/**
 * 视频管理Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-23
 */
@Mapper
public interface SpVideoMapper extends BaseMapper<SpVideo>
{
    /**
     * 查询视频管理
     *
     * @param videoId 视频管理ID
     * @return 视频管理
     */
    public SpVideo selectSpVideoById(Long videoId);

    /**
     * 查询视频管理列表
     *
     * @param spVideo 视频管理
     * @return 视频管理集合
     */
    public List<SpVideo> selectSpVideoList(SpVideo spVideo);

    /**
     * 新增视频管理
     *
     * @param spVideo 视频管理
     * @return 结果
     */
    public int insertSpVideo(SpVideo spVideo);

    /**
     * 修改视频管理
     *
     * @param spVideo 视频管理
     * @return 结果
     */
    public int updateSpVideo(SpVideo spVideo);

    /**
     * 删除视频管理
     *
     * @param videoId 视频管理ID
     * @return 结果
     */
    public int deleteSpVideoById(Long videoId);

    /**
     * 批量删除视频管理
     *
     * @param videoIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVideoByIds(Long[] videoIds);

    /**
     * 根据演员id获取视频信息
     * @param actorId
     * @return
     */
    public List<SpVideo> selectSpVideoByActorId(Long actorId);

    /**
     * 根据分类id查询
     * @param videoCategoryId
     * @return
     */
    public List<SpVideo> selectSpVideoByCategoryId(Long videoCategoryId);

    int updateSpVideoLikeNumber(Long videoId);

    int updateSpVideoLikeNumber1(Long videoId);

    List<SpVideo> selectSpVideoListByActorId(Long actorId);

    Integer selectCountByActorId(Long actorId);

    int updateSpVideoUnLikeNumber(Long videoId);

    int updateSpVideoUnLikeNumber1(Long videoId);

    List<SpVideo> selectSpVideoByCategoryIds(Long videoCategoryId);

    int updateSpVideoCollectNumber(Long videoId);

    int updateSpVideoCollectNumber1(Long videoId);

    List<SpVideo> selectSpVideoListOrderByDate(SpVideo spVideo);

    List<SpVideo> selectSpVideoListOrderByLike(SpVideo spVideo);

    List<SpVideo> selectSpVideoListOrderByWatch(SpVideo spVideo);

    List<SpVideo> selectSpVideoListOrderByCollect(SpVideo spVideo);

    List<SpVideo> selectSpVideoLists(SpVideo spVideo);

    int updateSpVideoWatchNumber(Long videoWatchHistoryTypeId);

    List<SpVideo> selectSpVideoListByName(String name);

    List<SpVideo> selectSpAnimeVideoList(Long videoCategoryId);

    List<SpVideo> selectSpAnimeVideoListOrderTime(Long videoCategoryId);

    List<SpVideo> selectSpAnimeVideoListOrderLike(Long videoCategoryId);

    List<SpVideo> selectSpAnimeVideoListOrderWatch(Long videoCategoryId);

    List<SpVideo> selectSpAnimeVideoListByName(String videoName);


    List<HashMap> likeAllName(String name);
}
