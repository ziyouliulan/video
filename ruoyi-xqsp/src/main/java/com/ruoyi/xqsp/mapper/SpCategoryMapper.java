package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_category】的数据库操作Mapper
* @createDate 2022-02-16 15:29:48
* @Entity com.ruoyi.xqsp.domain.SpCategory
*/
public interface SpCategoryMapper extends BaseMapper<SpCategory> {

}




