package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVideoCache;
import org.apache.ibatis.annotations.Param;

/**
 * 视频缓存Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-11
 */
public interface SpVideoCacheMapper 
{
    /**
     * 查询视频缓存
     * 
     * @param videoCacheId 视频缓存ID
     * @return 视频缓存
     */
    public SpVideoCache selectSpVideoCacheById(Long videoCacheId);

    /**
     * 查询视频缓存列表
     * 
     * @param spVideoCache 视频缓存
     * @return 视频缓存集合
     */
    public List<SpVideoCache> selectSpVideoCacheList(SpVideoCache spVideoCache);

    /**
     * 新增视频缓存
     * 
     * @param spVideoCache 视频缓存
     * @return 结果
     */
    public int insertSpVideoCache(SpVideoCache spVideoCache);

    /**
     * 修改视频缓存
     * 
     * @param spVideoCache 视频缓存
     * @return 结果
     */
    public int updateSpVideoCache(SpVideoCache spVideoCache);

    /**
     * 删除视频缓存
     * 
     * @param videoCacheId 视频缓存ID
     * @return 结果
     */
    public int deleteSpVideoCacheById(Long videoCacheId);

    /**
     * 批量删除视频缓存
     * 
     * @param videoCacheIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVideoCacheByIds(Long[] videoCacheIds);

    List<SpVideoCache> selectSpVideoCacheByUserId(Long userId);

    int updateSpVideoCacheUrl(@Param("videoCacheId") Long videoCacheId, @Param("videoCacheUrl") String videoCacheUrl);

    int deleteSpVideoCacheBy(@Param("videoId") Long videoId,@Param("type1") Long type1);
}
