package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpVideoWatchHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 视频观看历史Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-11
 */
@Mapper
public interface SpVideoWatchHistoryMapper extends BaseMapper<SpVideoWatchHistory> {
    /**
     * 查询视频观看历史
     *
     * @param videoWatchHistoryId 视频观看历史ID
     * @return 视频观看历史
     */
    public SpVideoWatchHistory selectSpVideoWatchHistoryById(Long videoWatchHistoryId);

    /**
     * 查询视频观看历史列表
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 视频观看历史集合
     */
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryList(SpVideoWatchHistory spVideoWatchHistory);
    /**
     * 查询视频观看历史列表
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 视频观看历史集合
     */
    public List<SpVideoWatchHistory> selectSpVideoWatchHistoryList2(SpVideoWatchHistory spVideoWatchHistory);

    /**
     * 新增视频观看历史
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 结果
     */
    public int insertSpVideoWatchHistory(SpVideoWatchHistory spVideoWatchHistory);

    /**
     * 修改视频观看历史
     *
     * @param spVideoWatchHistory 视频观看历史
     * @return 结果
     */
    public int updateSpVideoWatchHistory(SpVideoWatchHistory spVideoWatchHistory);

    /**
     * 删除视频观看历史
     *
     * @param videoWatchHistoryId 视频观看历史ID
     * @return 结果
     */
    public int deleteSpVideoWatchHistoryById(Long videoWatchHistoryId);

    /**
     * 批量删除视频观看历史
     *
     * @param videoWatchHistoryIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVideoWatchHistoryByIds(Long[] videoWatchHistoryIds);

    List<SpVideoWatchHistory> selectSpVideoWatchHistoryByUserId(Long userId);

    List<SpVideoWatchHistory> selectSpVideoWatchHistoryByUserIdV2(Long userId);

    int deleteSpVideoWatchHistory(@Param("videoId") Long videoId,@Param("type1") Long type1);
}
