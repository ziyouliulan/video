package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.TemplateConfigs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author lpden
* @description 针对表【sys_template_configs(视频分类)】的数据库操作Mapper
* @createDate 2022-07-09 20:27:16
* @Entity com.ruoyi.xqsp.domain.TemplateConfigs
*/

@Mapper
public interface TemplateConfigsMapper extends BaseMapper<TemplateConfigs> {

    List<Map> category(@Param("id") Integer id);
}




