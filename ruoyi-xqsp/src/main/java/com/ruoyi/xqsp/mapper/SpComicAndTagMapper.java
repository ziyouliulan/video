package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpComicAndTag;

/**
 * 漫画标签中间表Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-24
 */
public interface SpComicAndTagMapper 
{
    /**
     * 查询漫画标签中间表
     * 
     * @param comicAndTagId 漫画标签中间表ID
     * @return 漫画标签中间表
     */
    public SpComicAndTag selectSpComicAndTagById(Long comicAndTagId);

    /**
     * 查询漫画标签中间表列表
     * 
     * @param spComicAndTag 漫画标签中间表
     * @return 漫画标签中间表集合
     */
    public List<SpComicAndTag> selectSpComicAndTagList(SpComicAndTag spComicAndTag);

    /**
     * 新增漫画标签中间表
     * 
     * @param spComicAndTag 漫画标签中间表
     * @return 结果
     */
    public int insertSpComicAndTag(SpComicAndTag spComicAndTag);

    /**
     * 修改漫画标签中间表
     * 
     * @param spComicAndTag 漫画标签中间表
     * @return 结果
     */
    public int updateSpComicAndTag(SpComicAndTag spComicAndTag);

    /**
     * 删除漫画标签中间表
     * 
     * @param comicAndTagId 漫画标签中间表ID
     * @return 结果
     */
    public int deleteSpComicAndTagById(Long comicAndTagId);

    /**
     * 批量删除漫画标签中间表
     * 
     * @param comicAndTagIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpComicAndTagByIds(Long[] comicAndTagIds);

    int deleteSpComicAndTagByComicId(Long comicId);

    List<SpComicAndTag> selectSpComicAndTagByComicId(Long comicId);
}
