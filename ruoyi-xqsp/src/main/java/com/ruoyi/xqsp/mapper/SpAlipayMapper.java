package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpAlipay;

/**
 * 支付宝支付Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public interface SpAlipayMapper 
{
    /**
     * 查询支付宝支付
     * 
     * @param alipayId 支付宝支付ID
     * @return 支付宝支付
     */
    public SpAlipay selectSpAlipayById(Long alipayId);

    /**
     * 查询支付宝支付列表
     * 
     * @param spAlipay 支付宝支付
     * @return 支付宝支付集合
     */
    public List<SpAlipay> selectSpAlipayList(SpAlipay spAlipay);

    /**
     * 新增支付宝支付
     * 
     * @param spAlipay 支付宝支付
     * @return 结果
     */
    public int insertSpAlipay(SpAlipay spAlipay);

    /**
     * 修改支付宝支付
     * 
     * @param spAlipay 支付宝支付
     * @return 结果
     */
    public int updateSpAlipay(SpAlipay spAlipay);

    /**
     * 删除支付宝支付
     * 
     * @param alipayId 支付宝支付ID
     * @return 结果
     */
    public int deleteSpAlipayById(Long alipayId);

    /**
     * 批量删除支付宝支付
     * 
     * @param alipayIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpAlipayByIds(Long[] alipayIds);
}
