package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpExpenseCalendar;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 消费记录Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-08
 */
@Mapper
public interface SpExpenseCalendarMapper extends BaseMapper<SpExpenseCalendar> {
    /**
     * 查询消费记录
     *
     * @param expenseCalendarId 消费记录ID
     * @return 消费记录
     */
    public SpExpenseCalendar selectSpExpenseCalendarById(Long expenseCalendarId);

    /**
     * 查询消费记录列表
     *
     * @param spExpenseCalendar 消费记录
     * @return 消费记录集合
     */
    public List<SpExpenseCalendar> selectSpExpenseCalendarList(SpExpenseCalendar spExpenseCalendar);

    /**
     * 新增消费记录
     *
     * @param spExpenseCalendar 消费记录
     * @return 结果
     */
    public int insertSpExpenseCalendar(SpExpenseCalendar spExpenseCalendar);

    /**
     * 修改消费记录
     *
     * @param spExpenseCalendar 消费记录
     * @return 结果
     */
    public int updateSpExpenseCalendar(SpExpenseCalendar spExpenseCalendar);

    /**
     * 删除消费记录
     *
     * @param expenseCalendarId 消费记录ID
     * @return 结果
     */
    public int deleteSpExpenseCalendarById(Long expenseCalendarId);

    /**
     * 批量删除消费记录
     *
     * @param expenseCalendarIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpExpenseCalendarByIds(Long[] expenseCalendarIds);

    SpExpenseCalendar selectSpExpenseCalendarByExpenseCalendarOrder(String expenseCalendarOrder);


    List<SpExpenseCalendar> selectSpExpenseCalendarByUserId(Long userId);

    Integer selectSpExpenseCalendarByCount(@Param("videoId") Long videoId,@Param("i") int i);
}
