package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.UrlFilter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_url_filter】的数据库操作Mapper
* @createDate 2022-08-17 03:29:14
* @Entity com.ruoyi.xqsp.domain.UrlFilter
*/
public interface UrlFilterMapper extends BaseMapper<UrlFilter> {

}




