package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpVariety;

/**
 * 综艺管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-07
 */
public interface SpVarietyMapper 
{
    /**
     * 查询综艺管理
     * 
     * @param varietyId 综艺管理ID
     * @return 综艺管理
     */
    public SpVariety selectSpVarietyById(Long varietyId);

    /**
     * 查询综艺管理列表
     * 
     * @param spVariety 综艺管理
     * @return 综艺管理集合
     */
    public List<SpVariety> selectSpVarietyList(SpVariety spVariety);

    /**
     * 新增综艺管理
     * 
     * @param spVariety 综艺管理
     * @return 结果
     */
    public int insertSpVariety(SpVariety spVariety);

    /**
     * 修改综艺管理
     * 
     * @param spVariety 综艺管理
     * @return 结果
     */
    public int updateSpVariety(SpVariety spVariety);

    /**
     * 删除综艺管理
     * 
     * @param varietyId 综艺管理ID
     * @return 结果
     */
    public int deleteSpVarietyById(Long varietyId);

    /**
     * 批量删除综艺管理
     * 
     * @param varietyIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpVarietyByIds(Long[] varietyIds);

    List<SpVariety> selectSpVarietySortWatch();

    List<SpVariety> selectSpVarietySortLike();

    List<SpVariety> selectSpVarietyLists(SpVariety spVariety);
}
