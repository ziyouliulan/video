package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpOpenImg;

/**
 * 开屏广告Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface SpOpenImgMapper 
{
    /**
     * 查询开屏广告
     * 
     * @param openImgId 开屏广告ID
     * @return 开屏广告
     */
    public SpOpenImg selectSpOpenImgById(Long openImgId);

    /**
     * 查询开屏广告列表
     * 
     * @param spOpenImg 开屏广告
     * @return 开屏广告集合
     */
    public List<SpOpenImg> selectSpOpenImgList(SpOpenImg spOpenImg);

    /**
     * 新增开屏广告
     * 
     * @param spOpenImg 开屏广告
     * @return 结果
     */
    public int insertSpOpenImg(SpOpenImg spOpenImg);

    /**
     * 修改开屏广告
     * 
     * @param spOpenImg 开屏广告
     * @return 结果
     */
    public int updateSpOpenImg(SpOpenImg spOpenImg);

    /**
     * 删除开屏广告
     * 
     * @param openImgId 开屏广告ID
     * @return 结果
     */
    public int deleteSpOpenImgById(Long openImgId);

    /**
     * 批量删除开屏广告
     * 
     * @param openImgIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpOpenImgByIds(Long[] openImgIds);
}
