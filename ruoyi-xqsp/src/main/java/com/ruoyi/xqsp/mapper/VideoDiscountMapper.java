package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.VideoDiscount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【video_discount】的数据库操作Mapper
* @createDate 2022-06-12 19:54:11
* @Entity com.ruoyi.xqsp.domain.VideoDiscount
*/
public interface VideoDiscountMapper extends BaseMapper<VideoDiscount> {

}




