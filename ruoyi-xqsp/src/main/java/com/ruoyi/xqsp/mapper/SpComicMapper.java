package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpCategoryTag;
import com.ruoyi.xqsp.domain.SpComic;

/**
 * 漫画操作Mapper接口
 *
 * @author ruoyi
 * @date 2021-04-28
 */
public interface SpComicMapper  extends BaseMapper<SpComic>
{
    /**
     * 查询漫画操作
     *
     * @param comicId 漫画操作ID
     * @return 漫画操作
     */
    public SpComic selectSpComicById(Long comicId);

    /**
     * 查询漫画操作列表
     *
     * @param spComic 漫画操作
     * @return 漫画操作集合
     */
    public List<SpComic> selectSpComicList(SpComic spComic);

    /**
     * 新增漫画操作
     *
     * @param spComic 漫画操作
     * @return 结果
     */
    public int insertSpComic(SpComic spComic);

    /**
     * 修改漫画操作
     *
     * @param spComic 漫画操作
     * @return 结果
     */
    public int updateSpComic(SpComic spComic);

    /**
     * 删除漫画操作
     *
     * @param comicId 漫画操作ID
     * @return 结果
     */
    public int deleteSpComicById(Long comicId);

    /**
     * 批量删除漫画操作
     *
     * @param comicIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpComicByIds(Long[] comicIds);

    SpComic selectSpComicById1(Long comicId);

    List<SpComic> selectSpComicByCategory(String comicCategory);

    List<SpComic> selectSpComicListOrderTime(SpComic spComic);

    List<SpComic> selectSpComicListOrderWatch(SpComic spComic);

    int updateSpComicByComicId(Long comicId);

    int updateSpComicByComicId1(Long comicId);

    int updateWatchByComicId(Long comicId);
}
