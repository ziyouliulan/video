package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.ToolLocalStorage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【tool_local_storage(本地存储)】的数据库操作Mapper
* @createDate 2022-08-09 23:30:50
* @Entity com.ruoyi.xqsp.domain.ToolLocalStorage
*/
public interface ToolLocalStorageMapper extends BaseMapper<ToolLocalStorage> {

}




