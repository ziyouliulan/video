package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpCategoryTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_category_tag】的数据库操作Mapper
* @createDate 2022-02-17 16:19:07
* @Entity com.ruoyi.xqsp.domain.SpCategoryTag
*/
public interface SpCategoryTagMapper extends BaseMapper<SpCategoryTag> {

}




