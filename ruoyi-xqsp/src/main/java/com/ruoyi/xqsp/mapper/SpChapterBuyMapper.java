package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpChapterBuy;
import org.apache.ibatis.annotations.Mapper;

/**
 * 章节购买Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-22
 */
@Mapper
public interface SpChapterBuyMapper extends BaseMapper<SpChapterBuy> {
    /**
     * 查询章节购买
     *
     * @param chapterBuyId 章节购买ID
     * @return 章节购买
     */
    public SpChapterBuy selectSpChapterBuyById(Long chapterBuyId);

    /**
     * 查询章节购买列表
     *
     * @param spChapterBuy 章节购买
     * @return 章节购买集合
     */
    public List<SpChapterBuy> selectSpChapterBuyList(SpChapterBuy spChapterBuy);

    /**
     * 新增章节购买
     *
     * @param spChapterBuy 章节购买
     * @return 结果
     */
    public int insertSpChapterBuy(SpChapterBuy spChapterBuy);

    /**
     * 修改章节购买
     *
     * @param spChapterBuy 章节购买
     * @return 结果
     */
    public int updateSpChapterBuy(SpChapterBuy spChapterBuy);

    /**
     * 删除章节购买
     *
     * @param chapterBuyId 章节购买ID
     * @return 结果
     */
    public int deleteSpChapterBuyById(Long chapterBuyId);

    /**
     * 批量删除章节购买
     *
     * @param chapterBuyIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpChapterBuyByIds(Long[] chapterBuyIds);

    Long selectSpChapterBuyCount(SpChapterBuy spChapterBuy);
}
