package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.ReceiptCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_receipt_code】的数据库操作Mapper
* @createDate 2022-08-05 19:55:54
* @Entity com.ruoyi.xqsp.domain.ReceiptCode
*/
public interface ReceiptCodeMapper extends BaseMapper<ReceiptCode> {

}




