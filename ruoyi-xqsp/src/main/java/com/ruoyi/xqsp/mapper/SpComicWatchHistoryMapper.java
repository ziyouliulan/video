package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpComicWatchHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_comic_watch_history(视频观看记录)】的数据库操作Mapper
* @createDate 2022-07-23 17:36:46
* @Entity com.ruoyi.xqsp.domain.SpComicWatchHistory
*/
public interface SpComicWatchHistoryMapper extends BaseMapper<SpComicWatchHistory> {

}




