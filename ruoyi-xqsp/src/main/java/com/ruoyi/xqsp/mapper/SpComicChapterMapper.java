package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.ComicDiscount;
import com.ruoyi.xqsp.domain.SpComicChapter;
import org.apache.ibatis.annotations.Mapper;

/**
 * 漫画章节Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-06
 */
@Mapper
public interface SpComicChapterMapper extends BaseMapper<SpComicChapter>
{
    /**
     * 查询漫画章节
     * 
     * @param comicChapterId 漫画章节ID
     * @return 漫画章节
     */
    public SpComicChapter selectSpComicChapterById(Long comicChapterId);

    /**
     * 查询漫画章节列表
     * 
     * @param spComicChapter 漫画章节
     * @return 漫画章节集合
     */
    public List<SpComicChapter> selectSpComicChapterList(SpComicChapter spComicChapter);

    /**
     * 新增漫画章节
     * 
     * @param spComicChapter 漫画章节
     * @return 结果
     */
    public int insertSpComicChapter(SpComicChapter spComicChapter);

    /**
     * 修改漫画章节
     * 
     * @param spComicChapter 漫画章节
     * @return 结果
     */
    public int updateSpComicChapter(SpComicChapter spComicChapter);

    /**
     * 删除漫画章节
     * 
     * @param comicChapterId 漫画章节ID
     * @return 结果
     */
    public int deleteSpComicChapterById(Long comicChapterId);

    /**
     * 批量删除漫画章节
     * 
     * @param comicChapterIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpComicChapterByIds(Long[] comicChapterIds);

    /**
     * 根据漫画id查询章节
     * @param comicId
     * @return
     */
    List<SpComicChapter> selectSpComicChapterByComicId(Long comicId);

    Long selectSpComicChapterCountByComicId(Long comicId);


    SpComicChapter selectSpComicChapter(SpComicChapter spComicChapter);

    List<SpComicChapter> selectSpComicChapterByComicId1(SpComicChapter spComicChapter);


}
