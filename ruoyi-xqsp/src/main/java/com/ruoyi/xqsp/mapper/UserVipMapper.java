package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.UserVip;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_user_vip】的数据库操作Mapper
* @createDate 2022-02-26 09:10:19
* @Entity com.ruoyi.xqsp.domain.UserVip
*/
public interface UserVipMapper extends BaseMapper<UserVip> {

}




