package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpComicFreeChapter;

/**
 * 免费章节数Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-15
 */
public interface SpComicFreeChapterMapper 
{
    /**
     * 查询免费章节数
     * 
     * @param comicFreeChapter 免费章节数ID
     * @return 免费章节数
     */
    public SpComicFreeChapter selectSpComicFreeChapterById(Long comicFreeChapter);

    /**
     * 查询免费章节数列表
     * 
     * @param spComicFreeChapter 免费章节数
     * @return 免费章节数集合
     */
    public List<SpComicFreeChapter> selectSpComicFreeChapterList(SpComicFreeChapter spComicFreeChapter);

    /**
     * 新增免费章节数
     * 
     * @param spComicFreeChapter 免费章节数
     * @return 结果
     */
    public int insertSpComicFreeChapter(SpComicFreeChapter spComicFreeChapter);

    /**
     * 修改免费章节数
     * 
     * @param spComicFreeChapter 免费章节数
     * @return 结果
     */
    public int updateSpComicFreeChapter(SpComicFreeChapter spComicFreeChapter);

    /**
     * 删除免费章节数
     * 
     * @param comicFreeChapter 免费章节数ID
     * @return 结果
     */
    public int deleteSpComicFreeChapterById(Long comicFreeChapter);

    /**
     * 批量删除免费章节数
     * 
     * @param comicFreeChapters 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpComicFreeChapterByIds(Long[] comicFreeChapters);
}
