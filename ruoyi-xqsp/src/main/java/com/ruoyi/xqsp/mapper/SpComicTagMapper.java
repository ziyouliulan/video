package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpComicTag;
import com.ruoyi.xqsp.domain.SpVideo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 漫画标签Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-24
 */
@Mapper
public interface SpComicTagMapper extends BaseMapper<SpComicTag> {
    /**
     * 查询漫画标签
     *
     * @param comicTagId 漫画标签ID
     * @return 漫画标签
     */
    public SpComicTag selectSpComicTagById(Long comicTagId);

    /**
     * 查询漫画标签列表
     *
     * @param spComicTag 漫画标签
     * @return 漫画标签集合
     */
    public List<SpComicTag> selectSpComicTagList(SpComicTag spComicTag);

    /**
     * 新增漫画标签
     *
     * @param spComicTag 漫画标签
     * @return 结果
     */
    public int insertSpComicTag(SpComicTag spComicTag);

    /**
     * 修改漫画标签
     *
     * @param spComicTag 漫画标签
     * @return 结果
     */
    public int updateSpComicTag(SpComicTag spComicTag);

    /**
     * 删除漫画标签
     *
     * @param comicTagId 漫画标签ID
     * @return 结果
     */
    public int deleteSpComicTagById(Long comicTagId);

    /**
     * 批量删除漫画标签
     *
     * @param comicTagIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpComicTagByIds(Long[] comicTagIds);
}
