package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.GoldList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_gold_list】的数据库操作Mapper
* @createDate 2022-08-05 19:00:16
* @Entity com.ruoyi.xqsp.domain.GoldList
*/
public interface GoldListMapper extends BaseMapper<GoldList> {

}




