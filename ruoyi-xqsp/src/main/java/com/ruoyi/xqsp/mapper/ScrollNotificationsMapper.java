package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.ScrollNotifications;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_scroll_notifications(通知公告表)】的数据库操作Mapper
* @createDate 2022-07-25 21:06:45
* @Entity com.ruoyi.xqsp.domain.ScrollNotifications
*/
public interface ScrollNotificationsMapper extends BaseMapper<ScrollNotifications> {

}




