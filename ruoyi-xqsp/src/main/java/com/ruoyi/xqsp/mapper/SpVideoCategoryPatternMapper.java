package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpVideoCategoryPattern;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_video_category_pattern】的数据库操作Mapper
* @createDate 2022-02-15 16:20:21
* @Entity com.ruoyi.xqsp.domain.SpVideoCategoryPattern
*/
public interface SpVideoCategoryPatternMapper extends BaseMapper<SpVideoCategoryPattern> {

}




