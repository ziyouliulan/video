package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpAdviceFeedback;
import org.apache.ibatis.annotations.Mapper;

/**
 * 意见反馈Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
@Mapper
public interface SpAdviceFeedbackMapper extends BaseMapper<SpAdviceFeedback> {
    /**
     * 查询意见反馈
     * 
     * @param adviceFeedbackId 意见反馈ID
     * @return 意见反馈
     */
    public SpAdviceFeedback selectSpAdviceFeedbackById(Long adviceFeedbackId);

    /**
     * 查询意见反馈列表
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 意见反馈集合
     */
    public List<SpAdviceFeedback> selectSpAdviceFeedbackList(SpAdviceFeedback spAdviceFeedback);

    /**
     * 新增意见反馈
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 结果
     */
    public int insertSpAdviceFeedback(SpAdviceFeedback spAdviceFeedback);

    /**
     * 修改意见反馈
     * 
     * @param spAdviceFeedback 意见反馈
     * @return 结果
     */
    public int updateSpAdviceFeedback(SpAdviceFeedback spAdviceFeedback);

    /**
     * 删除意见反馈
     * 
     * @param adviceFeedbackId 意见反馈ID
     * @return 结果
     */
    public int deleteSpAdviceFeedbackById(Long adviceFeedbackId);

    /**
     * 批量删除意见反馈
     * 
     * @param adviceFeedbackIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpAdviceFeedbackByIds(Long[] adviceFeedbackIds);

    List<SpAdviceFeedback> selectSpAdviceFeedbackByUserId(Long userId);
}
