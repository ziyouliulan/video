package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.SpConsumeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_consume_record(消费记录)】的数据库操作Mapper
* @createDate 2022-05-23 21:51:34
* @Entity com.ruoyi.xqsp.domain.SpConsumeRecord
*/
public interface SpConsumeRecordMapper extends BaseMapper<SpConsumeRecord> {

}




