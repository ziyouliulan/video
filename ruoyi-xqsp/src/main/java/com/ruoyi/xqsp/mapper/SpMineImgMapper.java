package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpMineImg;

/**
 * 我的广告Mapper接口
 * 
 * @author ruoyi
 * @date 2021-07-14
 */
public interface SpMineImgMapper 
{
    /**
     * 查询我的广告
     * 
     * @param mineImgId 我的广告ID
     * @return 我的广告
     */
    public SpMineImg selectSpMineImgById(Long mineImgId);

    /**
     * 查询我的广告列表
     * 
     * @param spMineImg 我的广告
     * @return 我的广告集合
     */
    public List<SpMineImg> selectSpMineImgList(SpMineImg spMineImg);

    /**
     * 新增我的广告
     * 
     * @param spMineImg 我的广告
     * @return 结果
     */
    public int insertSpMineImg(SpMineImg spMineImg);

    /**
     * 修改我的广告
     * 
     * @param spMineImg 我的广告
     * @return 结果
     */
    public int updateSpMineImg(SpMineImg spMineImg);

    /**
     * 删除我的广告
     * 
     * @param mineImgId 我的广告ID
     * @return 结果
     */
    public int deleteSpMineImgById(Long mineImgId);

    /**
     * 批量删除我的广告
     * 
     * @param mineImgIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpMineImgByIds(Long[] mineImgIds);
}
