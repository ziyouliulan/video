package com.ruoyi.xqsp.mapper;

import com.ruoyi.xqsp.domain.City;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【sp_city】的数据库操作Mapper
* @createDate 2022-07-05 21:19:26
* @Entity com.ruoyi.xqsp.domain.City
*/
public interface CityMapper extends BaseMapper<City> {

}




