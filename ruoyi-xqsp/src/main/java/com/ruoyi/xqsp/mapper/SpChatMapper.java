package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpChat;
import org.apache.ibatis.annotations.Param;

/**
 * 聊天信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-10
 */
public interface SpChatMapper 
{
    /**
     * 查询聊天信息
     * 
     * @param chatId 聊天信息ID
     * @return 聊天信息
     */
    public SpChat selectSpChatById(Long chatId);

    /**
     * 查询聊天信息列表
     * 
     * @param spChat 聊天信息
     * @return 聊天信息集合
     */
    public List<SpChat> selectSpChatList(SpChat spChat);

    /**
     * 新增聊天信息
     * 
     * @param spChat 聊天信息
     * @return 结果
     */
    public int insertSpChat(SpChat spChat);

    /**
     * 修改聊天信息
     * 
     * @param spChat 聊天信息
     * @return 结果
     */
    public int updateSpChat(SpChat spChat);

    /**
     * 删除聊天信息
     * 
     * @param chatId 聊天信息ID
     * @return 结果
     */
    public int deleteSpChatById(Long chatId);

    /**
     * 批量删除聊天信息
     * 
     * @param chatIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpChatByIds(Long[] chatIds);

    List<SpChat> selectSpChatLists(@Param("userId") Long userId, @Param("serviceId") Long serviceId);

    List<SpChat> selectSpChatList1(SpChat spChat);

    List<SpChat> selectSpChatList2(SpChat spChat);


    int updateSpChatByUserId(Long userId);


    SpChat selectSpChatByIds(Long userId);

    SpChat selectSpChatByIdAndNumber( @Param("userId") Long userId,@Param("chatNumber") Long chatNumber);

    List<SpChat> selectSpChatList3(Long userId);
}
