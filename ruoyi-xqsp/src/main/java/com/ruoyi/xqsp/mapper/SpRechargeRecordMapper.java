package com.ruoyi.xqsp.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.xqsp.domain.SpRechargeRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 充值管理Mapper接口
 *
 * @author ruoyi
 * @date 2021-05-07
 */
@Mapper
public interface SpRechargeRecordMapper extends BaseMapper<SpRechargeRecord> {
    /**
     * 查询充值管理
     *
     * @param rechargeRecordId 充值管理ID
     * @return 充值管理
     */
    public SpRechargeRecord selectSpRechargeRecordById(Long rechargeRecordId);

    /**
     * 查询充值管理列表
     *
     * @param spRechargeRecord 充值管理
     * @return 充值管理集合
     */
    public List<SpRechargeRecord> selectSpRechargeRecordList(SpRechargeRecord spRechargeRecord);

    /**
     * 新增充值管理
     *
     * @param spRechargeRecord 充值管理
     * @return 结果
     */
    public int insertSpRechargeRecord(SpRechargeRecord spRechargeRecord);

    /**
     * 修改充值管理
     *
     * @param spRechargeRecord 充值管理
     * @return 结果
     */
    public int updateSpRechargeRecord(SpRechargeRecord spRechargeRecord);

    /**
     * 删除充值管理
     *
     * @param rechargeRecordId 充值管理ID
     * @return 结果
     */
    public int deleteSpRechargeRecordById(Long rechargeRecordId);

    /**
     * 批量删除充值管理
     *
     * @param rechargeRecordIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpRechargeRecordByIds(Long[] rechargeRecordIds);

    SpRechargeRecord selectSpRechargeRecordByOrder(String rechargeRecordOrder);

    List<SpRechargeRecord> selectSpRechargeRecordLists(Long userId);

    List<SpRechargeRecord> selectSpRechargeRecordListById(Long userId);

    List<SpRechargeRecord> selectSpRechargeRecordListById1(Long userId);

    List<SpRechargeRecord> selectSpRechargeRecordListById2(Long userId);

    List<SpRechargeRecord> selectSpRechargeRecordListById3(Long userId);

    List<SpRechargeRecord> selectSpRechargeRecordListsBySelfAndKeFu(Long userId);
}
