package com.ruoyi.xqsp.mapper;

import java.util.List;
import com.ruoyi.xqsp.domain.SpPhoenixEvaluation;

/**
 * 楼凤评价表Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-25
 */
public interface SpPhoenixEvaluationMapper 
{
    /**
     * 查询楼凤评价表
     * 
     * @param phoenixEvaluationId 楼凤评价表ID
     * @return 楼凤评价表
     */
    public SpPhoenixEvaluation selectSpPhoenixEvaluationById(Long phoenixEvaluationId);

    /**
     * 查询楼凤评价表列表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 楼凤评价表集合
     */
    public List<SpPhoenixEvaluation> selectSpPhoenixEvaluationList(SpPhoenixEvaluation spPhoenixEvaluation);

    /**
     * 新增楼凤评价表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 结果
     */
    public int insertSpPhoenixEvaluation(SpPhoenixEvaluation spPhoenixEvaluation);

    /**
     * 修改楼凤评价表
     * 
     * @param spPhoenixEvaluation 楼凤评价表
     * @return 结果
     */
    public int updateSpPhoenixEvaluation(SpPhoenixEvaluation spPhoenixEvaluation);

    /**
     * 删除楼凤评价表
     * 
     * @param phoenixEvaluationId 楼凤评价表ID
     * @return 结果
     */
    public int deleteSpPhoenixEvaluationById(Long phoenixEvaluationId);

    /**
     * 批量删除楼凤评价表
     * 
     * @param phoenixEvaluationIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSpPhoenixEvaluationByIds(Long[] phoenixEvaluationIds);

    /**
     * 根据楼分id查询
     * @param phoenixId
     * @return
     */
    List<SpPhoenixEvaluation> selectSpPhoenixEvaluationByPhoneixId(Long phoenixId);

    int deleteSpPhoenixEvaluationByPhoenixId(Long phoenixId);
}
