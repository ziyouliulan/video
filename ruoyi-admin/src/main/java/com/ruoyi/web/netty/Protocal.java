package com.ruoyi.web.netty;

import lombok.Data;

@Data
public class Protocal {

    //消息类型 1文字 2图片
    protected int msgType = 0;
    //0 正常消息 1登录  2退出
    protected int type ;
    //消息内容
    protected String dataContent = null;
    //消息发送者
    protected String from = "-1";
    //指定用户
    protected String to = "-1";



}
