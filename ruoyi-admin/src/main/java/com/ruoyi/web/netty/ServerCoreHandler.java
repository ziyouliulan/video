package com.ruoyi.web.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.stereotype.Component;

@Component
public class ServerCoreHandler {


    public void messageReceived(ChannelHandlerContext handlerContext)
    {
        String message = "我是服务器，你好呀";
        handlerContext.writeAndFlush(new TextWebSocketFrame("hello"));
    }
}
