package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.system.domain.vo.TestVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestLinkController extends BaseController{
    private static final Logger log = LoggerFactory.getLogger(TestLinkController.class);
    @GetMapping("/getTest")
    public AjaxResult getTest()
    {
        return AjaxResult.success("test http link ok");
    }
    @PostMapping("/postTest")
    public AjaxResult postTest(@RequestBody TestVo testVo)
    {
        log.info(testVo.getAge()+"");
        log.info(testVo.getName());
        return AjaxResult.success("test http link ok");
    }
}
