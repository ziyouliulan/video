package com.ruoyi.common.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LpStringUtil {

    public static String generateOrderNumber(){

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String format = LocalDateTime.now().format(dateTimeFormatter);

        String numeric = RandomStringUtils.randomNumeric(8);


        return format +  numeric;

    }

}

