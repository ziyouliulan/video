# From java image, version : 8
FROM java:8

# 挂载app目录
VOLUME /app

# COPY or ADD to image
COPY ./ruoyi-admin/target/ruoyi-admin.jar app.jar

COPY ./docker/application.yml  application.yml

COPY ./docker/application-druid.yml  application-druid.yml

RUN bash -c "touch /app.jar"
EXPOSE 8080
ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005", "-jar", "app.jar","--spring.config.location=application.yml,application-druid.yml"]
