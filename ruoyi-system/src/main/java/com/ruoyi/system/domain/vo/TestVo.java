package com.ruoyi.system.domain.vo;

import lombok.Data;

@Data
public class TestVo {
    private String name;
    private Integer age;
}
