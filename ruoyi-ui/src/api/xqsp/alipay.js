import request from '@/utils/request'

// 查询支付宝支付列表
export function listAlipay(query) {
  return request({
    url: '/xqsp/alipay/list',
    method: 'get',
    params: query
  })
}

// 查询支付宝支付详细
export function getAlipay(alipayId) {
  return request({
    url: '/xqsp/alipay/' + alipayId,
    method: 'get'
  })
}

// 新增支付宝支付
export function addAlipay(data) {
  return request({
    url: '/xqsp/alipay',
    method: 'post',
    data: data
  })
}

// 修改支付宝支付
export function updateAlipay(data) {
  return request({
    url: '/xqsp/alipay',
    method: 'put',
    data: data
  })
}

// 删除支付宝支付
export function delAlipay(alipayId) {
  return request({
    url: '/xqsp/alipay/' + alipayId,
    method: 'delete'
  })
}

// 导出支付宝支付
export function exportAlipay(query) {
  return request({
    url: '/xqsp/alipay/export',
    method: 'get',
    params: query
  })
}