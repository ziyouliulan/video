import request from '@/utils/request'

// 查询APP状态列表
export function listState(query) {
  return request({
    url: '/xqsp/state/list',
    method: 'get',
    params: query
  })
}

// 查询APP状态详细
export function getState(stateId) {
  return request({
    url: '/xqsp/state/' + stateId,
    method: 'get'
  })
}

// 新增APP状态
export function addState(data) {
  return request({
    url: '/xqsp/state',
    method: 'post',
    data: data
  })
}

// 修改APP状态
export function updateState(data) {
  return request({
    url: '/xqsp/state',
    method: 'put',
    data: data
  })
}

// 修改APP状态
export function changeUserStatus(stateId,stateValue) {
  const data={
    stateId,stateValue
  }
  return request({
    url: '/xqsp/state',
    method: 'put',
    data: data
  })
}

// 删除APP状态
export function delState(stateId) {
  return request({
    url: '/xqsp/state/' + stateId,
    method: 'delete'
  })
}

// 导出APP状态
export function exportState(query) {
  return request({
    url: '/xqsp/state/export',
    method: 'get',
    params: query
  })
}
