import request from '@/utils/request'

// 查询开屏广告列表
export function listOpen_img(query) {
  return request({
    url: '/xqsp/open_img/list',
    method: 'get',
    params: query
  })
}

// 查询开屏广告详细
export function getOpen_img(openImgId) {
  return request({
    url: '/xqsp/open_img/' + openImgId,
    method: 'get'
  })
}

// 新增开屏广告
export function addOpen_img(data) {
  return request({
    url: '/xqsp/open_img',
    method: 'post',
    data: data
  })
}

// 修改开屏广告
export function updateOpen_img(data) {
  return request({
    url: '/xqsp/open_img',
    method: 'put',
    data: data
  })
}

// 删除开屏广告
export function delOpen_img(openImgId) {
  return request({
    url: '/xqsp/open_img/' + openImgId,
    method: 'delete'
  })
}

// 导出开屏广告
export function exportOpen_img(query) {
  return request({
    url: '/xqsp/open_img/export',
    method: 'get',
    params: query
  })
}