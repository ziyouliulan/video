import request from '@/utils/request'

// 查询卡密操作列表
export function listCarmichael(query) {
  return request({
    url: '/xqsp/carmichael/list',
    method: 'get',
    params: query
  })
}

// 查询卡密操作详细
export function getCarmichael(carmichaelId) {
  return request({
    url: '/xqsp/carmichael/' + carmichaelId,
    method: 'get'
  })
}

// 新增卡密操作
export function addCarmichael(data) {
  return request({
    url: '/xqsp/carmichael',
    method: 'post',
    data: data
  })
}

// 修改卡密操作
export function updateCarmichael(data) {
  return request({
    url: '/xqsp/carmichael',
    method: 'put',
    data: data
  })
}

// 删除卡密操作
export function delCarmichael(carmichaelId) {
  return request({
    url: '/xqsp/carmichael/' + carmichaelId,
    method: 'delete'
  })
}

// 导出卡密操作
export function exportCarmichael(query) {
  return request({
    url: '/xqsp/carmichael/export',
    method: 'get',
    params: query
  })
}