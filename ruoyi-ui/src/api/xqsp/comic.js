import request from '@/utils/request'

// 查询漫画操作列表
export function listComic(query) {
  return request({
    url: '/xqsp/comic/list',
    method: 'get',
    params: query
  })
}

// 查询漫画操作详细
export function getComic(comicId) {
  return request({
    url: '/xqsp/comic/' + comicId,
    method: 'get'
  })
}

// 新增漫画操作
export function addComic(data) {
  return request({
    url: '/xqsp/comic',
    method: 'post',
    data: data
  })
}

// 修改漫画操作
export function updateComic(data) {
  return request({
    url: '/xqsp/comic',
    method: 'put',
    data: data
  })
}

// 修改漫画状态操作
export function changeUserStatus(comicId, comicState) {
  const data={
    comicId, comicState
  }
  return request({
    url: '/xqsp/comic',
    method: 'put',
    data: data
  })
}

// 修改漫画置顶操作
export function changeComicTop(comicId, comicTop) {
  const data={
    comicId, comicTop
  }
  return request({
    url: '/xqsp/comic',
    method: 'put',
    data: data
  })
}

// 修改漫画置顶后排序操作
export function changeComicSort(comicId, comicSort) {
  const data={
    comicId, comicSort
  }
  return request({
    url: '/xqsp/comic',
    method: 'put',
    data: data
  })
}



// 删除漫画操作
export function delComic(comicId) {
  return request({
    url: '/xqsp/comic/' + comicId,
    method: 'delete'
  })
}

// 导出漫画操作
export function exportComic(query) {
  return request({
    url: '/xqsp/comic/export',
    method: 'get',
    params: query
  })
}
