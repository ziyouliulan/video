import request from '@/utils/request'

// 查询漫画标签中间表列表
export function listComicandtag(query) {
  return request({
    url: '/xqsp/comicandtag/list',
    method: 'get',
    params: query
  })
}

// 查询漫画标签中间表详细
export function getComicandtag(comicAndTagId) {
  return request({
    url: '/xqsp/comicandtag/' + comicAndTagId,
    method: 'get'
  })
}
export function getComictag(comicId){
  return request({
    url:'/xqsp/comicandtag/comicId/'+comicId,
    method:'get'
  })
}

// 新增漫画标签中间表
export function addComicandtag(value1,comicId) {

  const data={
    value1,comicId
  }
  return request({
    url: '/xqsp/comicandtag',
    method: 'post',
    data: data
  })
}

// 修改漫画标签中间表
export function updateComicandtag(value1,comicId) {
  const data={
    value1,comicId
  }
  return request({
    url: '/xqsp/comicandtag',
    method: 'put',
    data: data
  })
}

// 删除漫画标签中间表
export function delComicandtag(comicAndTagId) {
  return request({
    url: '/xqsp/comicandtag/' + comicAndTagId,
    method: 'delete'
  })
}

// 导出漫画标签中间表
export function exportComicandtag(query) {
  return request({
    url: '/xqsp/comicandtag/export',
    method: 'get',
    params: query
  })
}
