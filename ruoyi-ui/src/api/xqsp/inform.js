import request from '@/utils/request'

// 查询系统通知表列表
export function listInform(query) {
  return request({
    url: '/xqsp/inform/list',
    method: 'get',
    params: query
  })
}

// 查询系统通知表详细
export function getInform(informId) {
  return request({
    url: '/xqsp/inform/' + informId,
    method: 'get'
  })
}

// 新增系统通知表
export function addInform(data) {
  return request({
    url: '/xqsp/inform',
    method: 'post',
    data: data
  })
}

// 修改系统通知表
export function updateInform(data) {
  return request({
    url: '/xqsp/inform',
    method: 'put',
    data: data
  })
}

// 删除系统通知表
export function delInform(informId) {
  return request({
    url: '/xqsp/inform/' + informId,
    method: 'delete'
  })
}

// 导出系统通知表
export function exportInform(query) {
  return request({
    url: '/xqsp/inform/export',
    method: 'get',
    params: query
  })
}