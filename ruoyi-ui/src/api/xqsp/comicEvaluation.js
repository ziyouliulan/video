import request from '@/utils/request'

// 查询漫画评价列表
export function listComicEvaluation(query) {
  return request({
    url: '/xqsp/comicEvaluation/list',
    method: 'get',
    params: query
  })
}

// 查询漫画评价详细
export function getComicEvaluation(comicEvaluationId) {
  return request({
    url: '/xqsp/comicEvaluation/' + comicEvaluationId,
    method: 'get'
  })
}

// 新增漫画评价
export function addComicEvaluation(data) {
  return request({
    url: '/xqsp/comicEvaluation',
    method: 'post',
    data: data
  })
}

// 修改漫画评价
export function updateComicEvaluation(data) {
  return request({
    url: '/xqsp/comicEvaluation',
    method: 'put',
    data: data
  })
}

// 删除漫画评价
export function delComicEvaluation(comicEvaluationId) {
  return request({
    url: '/xqsp/comicEvaluation/' + comicEvaluationId,
    method: 'delete'
  })
}

// 导出漫画评价
export function exportComicEvaluation(query) {
  return request({
    url: '/xqsp/comicEvaluation/export',
    method: 'get',
    params: query
  })
}