import request from '@/utils/request'

// 查询意见反馈列表
export function listFeedback(query) {
  return request({
    url: '/xqsp/feedback/list',
    method: 'get',
    params: query
  })
}

// 查询意见反馈详细
export function getFeedback(adviceFeedbackId) {
  return request({
    url: '/xqsp/feedback/' + adviceFeedbackId,
    method: 'get'
  })
}

// 新增意见反馈
export function addFeedback(data) {
  return request({
    url: '/xqsp/feedback',
    method: 'post',
    data: data
  })
}

// 修改意见反馈
export function updateFeedback(data) {
  return request({
    url: '/xqsp/feedback',
    method: 'put',
    data: data
  })
}
export function changeUserStatus(adviceFeedbackId,adviceFeedbackWatch) {
  const data={
    adviceFeedbackId,adviceFeedbackWatch
  }
  return request({
    url: '/xqsp/feedback',
    method: 'put',
    data: data
  })

}
// 删除意见反馈
export function delFeedback(adviceFeedbackId) {
  return request({
    url: '/xqsp/feedback/' + adviceFeedbackId,
    method: 'delete'
  })
}

// 导出意见反馈
export function exportFeedback(query) {
  return request({
    url: '/xqsp/feedback/export',
    method: 'get',
    params: query
  })
}
