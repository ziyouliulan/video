import request from '@/utils/request'

// 查询版本设置列表
export function listVersion(query) {
  return request({
    url: '/xqsp/version/list',
    method: 'get',
    params: query
  })
}

// 查询版本设置详细
export function getVersion(versionId) {
  return request({
    url: '/xqsp/version/' + versionId,
    method: 'get'
  })
}

// 新增版本设置
export function addVersion(data) {
  return request({
    url: '/xqsp/version',
    method: 'post',
    data: data
  })
}

// 修改版本设置
export function updateVersion(data) {
  return request({
    url: '/xqsp/version',
    method: 'put',
    data: data
  })
}

// 删除版本设置
export function delVersion(versionId) {
  return request({
    url: '/xqsp/version/' + versionId,
    method: 'delete'
  })
}

// 导出版本设置
export function exportVersion(query) {
  return request({
    url: '/xqsp/version/export',
    method: 'get',
    params: query
  })
}