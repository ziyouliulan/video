import request from '@/utils/request'

// 查询激活码表列表
export function listKey(query) {
  return request({
    url: '/xqsp/key/list',
    method: 'get',
    params: query
  })
}

// 查询激活码表详细
export function getKey(keyId) {
  return request({
    url: '/xqsp/key/' + keyId,
    method: 'get'
  })
}

// 新增激活码表
export function addKey(data) {
  return request({
    url: '/xqsp/key',
    method: 'post',
    data: data
  })
}

// 修改激活码表
export function updateKey(data) {
  return request({
    url: '/xqsp/key',
    method: 'put',
    data: data
  })
}

// 删除激活码表
export function delKey(keyId) {
  return request({
    url: '/xqsp/key/' + keyId,
    method: 'delete'
  })
}

// 导出激活码表
export function exportKey(query) {
  return request({
    url: '/xqsp/key/export',
    method: 'get',
    params: query
  })
}