import request from '@/utils/request'

// 查询银行卡操作列表
export function listBank(query) {
  return request({
    url: '/xqsp/bank/list',
    method: 'get',
    params: query
  })
}

// 查询银行卡操作详细
export function getBank(spBankId) {
  return request({
    url: '/xqsp/bank/' + spBankId,
    method: 'get'
  })
}

// 新增银行卡操作
export function addBank(data) {
  return request({
    url: '/xqsp/bank',
    method: 'post',
    data: data
  })
}

// 修改银行卡操作
export function updateBank(data) {
  return request({
    url: '/xqsp/bank',
    method: 'put',
    data: data
  })
}

// 删除银行卡操作
export function delBank(spBankId) {
  return request({
    url: '/xqsp/bank/' + spBankId,
    method: 'delete'
  })
}

// 导出银行卡操作
export function exportBank(query) {
  return request({
    url: '/xqsp/bank/export',
    method: 'get',
    params: query
  })
}