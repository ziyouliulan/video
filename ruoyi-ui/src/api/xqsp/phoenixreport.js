import request from '@/utils/request'

// 查询楼凤举报列表
export function listPhoenixreport(query) {
  return request({
    url: '/xqsp/phoenixreport/list',
    method: 'get',
    params: query
  })
}

// 查询楼凤举报详细
export function getPhoenixreport(reportId) {
  return request({
    url: '/xqsp/phoenixreport/' + reportId,
    method: 'get'
  })
}

// 新增楼凤举报
export function addPhoenixreport(data) {
  return request({
    url: '/xqsp/phoenixreport',
    method: 'post',
    data: data
  })
}

// 修改楼凤举报
export function updatePhoenixreport(data) {
  return request({
    url: '/xqsp/phoenixreport',
    method: 'put',
    data: data
  })
}

// 删除楼凤举报
export function delPhoenixreport(reportId) {
  return request({
    url: '/xqsp/phoenixreport/' + reportId,
    method: 'delete'
  })
}

// 导出楼凤举报
export function exportPhoenixreport(query) {
  return request({
    url: '/xqsp/phoenixreport/export',
    method: 'get',
    params: query
  })
}