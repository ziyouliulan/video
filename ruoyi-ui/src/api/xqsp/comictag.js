import request from '@/utils/request'

// 查询漫画标签列表
export function listComictag(query) {
  return request({
    url: '/xqsp/comictag/list',
    method: 'get',
    params: query
  })
}

// 查询漫画标签详细
export function getComictag(comicTagId) {
  return request({
    url: '/xqsp/comictag/' + comicTagId,
    method: 'get'
  })
}

// 新增漫画标签
export function addComictag(data) {
  return request({
    url: '/xqsp/comictag',
    method: 'post',
    data: data
  })
}

// 修改漫画标签
export function updateComictag(data) {
  return request({
    url: '/xqsp/comictag',
    method: 'put',
    data: data
  })
}

// 删除漫画标签
export function delComictag(comicTagId) {
  return request({
    url: '/xqsp/comictag/' + comicTagId,
    method: 'delete'
  })
}

// 导出漫画标签
export function exportComictag(query) {
  return request({
    url: '/xqsp/comictag/export',
    method: 'get',
    params: query
  })
}