import request from '@/utils/request'

// 查询漫画章节列表
export function listComicChapter(query) {
  return request({
    url: '/xqsp/comicChapter/list',
    method: 'get',
    params: query
  })
}

// 查询漫画章节详细
export function getComicChapter(comicChapterId) {
  return request({
    url: '/xqsp/comicChapter/' + comicChapterId,
    method: 'get'
  })
}

 //根据漫画id查询章节
export function listComicChapterByComicId(query) {

  return request({
    url:'/xqsp/comicChapter/comicId',
    method:'get',
    params: query
  })
}
// 新增漫画章节
export function addComicChapter(data) {
  return request({
    url: '/xqsp/comicChapter',
    method: 'post',
    data: data
  })
}

// 修改漫画章节
export function updateComicChapter(data) {
  return request({
    url: '/xqsp/comicChapter',
    method: 'put',
    data: data
  })
}

// 删除漫画章节
export function delComicChapter(comicChapterId) {
  return request({
    url: '/xqsp/comicChapter/' + comicChapterId,
    method: 'delete'
  })
}

// 导出漫画章节
export function exportComicChapter(query) {
  return request({
    url: '/xqsp/comicChapter/export',
    method: 'get',
    params: query
  })
}
