import request from '@/utils/request'

// 查询综艺视频列表
export function listVarietyVideo(query) {
  return request({
    url: '/xqsp/varietyVideo/list',
    method: 'get',
    params: query
  })
}

// 查询综艺视频列表根据综艺id
export function getVideoByVarietyId(query) {
  return request({
    url: '/xqsp/varietyVideo/varietyId/'+query.varietyId,
    method: 'get',
    params: query
  })
}

// 查询综艺视频详细
export function getVarietyVideo(varietyVideoId) {
  return request({
    url: '/xqsp/varietyVideo/' + varietyVideoId,
    method: 'get'
  })
}

// 新增综艺视频
export function addVarietyVideo(data) {
  return request({
    url: '/xqsp/varietyVideo',
    method: 'post',
    data: data
  })
}

// 修改综艺视频
export function updateVarietyVideo(data) {
  return request({
    url: '/xqsp/varietyVideo',
    method: 'put',
    data: data
  })
}
export function changeUserStatus(varietyVideoId,varietyVideoPutaway) {
  const data={
    varietyVideoId,varietyVideoPutaway
  }
  return request({
    url: '/xqsp/varietyVideo',
    method: 'put',
    data: data
  })
}

// 删除综艺视频
export function delVarietyVideo(varietyVideoId) {
  return request({
    url: '/xqsp/varietyVideo/' + varietyVideoId,
    method: 'delete'
  })
}

// 导出综艺视频
export function exportVarietyVideo(query) {
  return request({
    url: '/xqsp/varietyVideo/export',
    method: 'get',
    params: query
  })
}
