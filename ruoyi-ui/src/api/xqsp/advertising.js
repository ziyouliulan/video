import request from '@/utils/request'

// 查询广告位管理列表
export function listAdvertising(query) {
  return request({
    url: '/xqsp/advertising/list',
    method: 'get',
    params: query
  })
}

// 查询广告位管理详细
export function getAdvertising(advertisingId) {
  return request({
    url: '/xqsp/advertising/' + advertisingId,
    method: 'get'
  })
}

// 新增广告位管理
export function addAdvertising(data) {
  return request({
    url: '/xqsp/advertising',
    method: 'post',
    data: data
  })
}

// 修改广告位管理
export function updateAdvertising(data) {
  return request({
    url: '/xqsp/advertising',
    method: 'put',
    data: data
  })
}

// 修改广告位状态
export function changeBannerStatus(advertisingId, advertisingState) {
  const data={
    advertisingId, advertisingState
  }
  return request({
    url: '/xqsp/advertising',
    method: 'put',
    data: data
  })
}

// 修改是否精选展示
export function changeBannerHome(advertisingId, advertisingHome) {
  const data={
    advertisingId, advertisingHome
  }
  return request({
    url: '/xqsp/advertising',
    method: 'put',
    data: data
  })
}
// 删除广告位管理
export function delAdvertising(advertisingId) {
  return request({
    url: '/xqsp/advertising/' + advertisingId,
    method: 'delete'
  })
}

// 导出广告位管理
export function exportAdvertising(query) {
  return request({
    url: '/xqsp/advertising/export',
    method: 'get',
    params: query
  })
}
