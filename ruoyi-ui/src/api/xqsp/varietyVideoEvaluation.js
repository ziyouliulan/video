import request from '@/utils/request'

// 查询综艺视频评论列表
export function listVarietyVideoEvaluation(query) {
  return request({
    url: '/xqsp/varietyVideoEvaluation/list',
    method: 'get',
    params: query
  })
}

// 查询综艺视频评论详细
export function getVarietyVideoEvaluation(varietyVideoEvaluationId) {
  return request({
    url: '/xqsp/varietyVideoEvaluation/' + varietyVideoEvaluationId,
    method: 'get'
  })
}

// 新增综艺视频评论
export function addVarietyVideoEvaluation(data) {
  return request({
    url: '/xqsp/varietyVideoEvaluation',
    method: 'post',
    data: data
  })
}

// 修改综艺视频评论
export function updateVarietyVideoEvaluation(data) {
  return request({
    url: '/xqsp/varietyVideoEvaluation',
    method: 'put',
    data: data
  })
}

// 删除综艺视频评论
export function delVarietyVideoEvaluation(varietyVideoEvaluationId) {
  return request({
    url: '/xqsp/varietyVideoEvaluation/' + varietyVideoEvaluationId,
    method: 'delete'
  })
}

// 导出综艺视频评论
export function exportVarietyVideoEvaluation(query) {
  return request({
    url: '/xqsp/varietyVideoEvaluation/export',
    method: 'get',
    params: query
  })
}