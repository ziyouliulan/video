import request from '@/utils/request'

// 查询金币设置列表
export function listGlodmoney(query) {
  return request({
    url: '/xqsp/glodmoney/list',
    method: 'get',
    params: query
  })
}

// 查询金币设置详细
export function getGlodmoney(glodMoneyId) {
  return request({
    url: '/xqsp/glodmoney/' + glodMoneyId,
    method: 'get'
  })
}

// 新增金币设置
export function addGlodmoney(data) {
  return request({
    url: '/xqsp/glodmoney',
    method: 'post',
    data: data
  })
}

// 修改金币设置
export function updateGlodmoney(data) {
  return request({
    url: '/xqsp/glodmoney',
    method: 'put',
    data: data
  })
}

// 删除金币设置
export function delGlodmoney(glodMoneyId) {
  return request({
    url: '/xqsp/glodmoney/' + glodMoneyId,
    method: 'delete'
  })
}

// 导出金币设置
export function exportGlodmoney(query) {
  return request({
    url: '/xqsp/glodmoney/export',
    method: 'get',
    params: query
  })
}