import request from '@/utils/request'

// 查询视频管理列表
export function listVideo(query) {
  return request({
    url: '/xqsp/video/list',
    method: 'get',
    params: query
  })
}

// 查询视频管理详细
export function getVideo(videoId) {
  return request({
    url: '/xqsp/video/' + videoId,
    method: 'get'
  })
}

// 查询视频管理详细
export function getVideoByActorId(query) {
  return request({
    url: '/xqsp/video/actorId/' + query.actorId,
    method: 'get',
    params: query
  })
}

// 新增视频管理
export function addVideo(data) {
  return request({
    url: '/xqsp/video',
    method: 'post',
    data: data
  })
}

// 修改视频管理
export function updateVideo(data) {
  return request({
    url: '/xqsp/video',
    method: 'put',
    data: data
  })
}
// 修改视频管理
export function changeUserStatus(videoId, videoPutaway) {
  const data={
    videoId, videoPutaway
  }
  return request({
    url: '/xqsp/video',
    method: 'put',
    data: data
  })
}

// 删除视频管理
export function delVideo(videoId) {
  return request({
    url: '/xqsp/video/' + videoId,
    method: 'delete'
  })
}

// 导出视频管理
export function exportVideo(query) {
  return request({
    url: '/xqsp/video/export',
    method: 'get',
    params: query
  })
}

//上传视频   /common/upload

export  function uploadVideo(query){
  return request({
    url:'/common/upload',
    method:'post',
    data:query
  })
}


//动漫


// 查询动漫视频管理列表
export function listVideo1(query) {
  return request({
    url: '/xqsp/video/list1',
    method: 'get',
    params: query
  })
}
// 新增动漫视频管理
export function addVideo1(data) {
  return request({
    url: '/xqsp/video/add1',
    method: 'post',
    data: data
  })
}
