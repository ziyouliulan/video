import request from '@/utils/request'

// 查询演员信息列表
export function listMessage(query) {
  return request({
    url: '/xqsp/message/list',
    method: 'get',
    params: query
  })
}

// 查询演员信息详细
export function getMessage(actorMessageId) {
  return request({
    url: '/xqsp/message/' + actorMessageId,
    method: 'get'
  })
}



// 查询演员信息详细
export function getMessages(actorId) {
  return request({
    url: '/xqsp/message/actorId/' + actorId,
    method: 'get'
  })
}

// 新增演员信息
export function addMessage(data) {
  return request({
    url: '/xqsp/message',
    method: 'post',
    data: data
  })
}

// 修改演员信息
export function updateMessage(data) {
  return request({
    url: '/xqsp/message',
    method: 'put',
    data: data
  })
}

// 删除演员信息
export function delMessage(actorMessageId) {
  return request({
    url: '/xqsp/message/' + actorMessageId,
    method: 'delete'
  })
}

// 导出演员信息
export function exportMessage(query) {
  return request({
    url: '/xqsp/message/export',
    method: 'get',
    params: query
  })
}
