import request from '@/utils/request'

// 查询楼凤评价表列表
export function listPhoenixevaluation(query) {
  return request({
    url: '/xqsp/phoenixevaluation/list',
    method: 'get',
    params: query
  })
}

// 查询楼凤评价表详细
export function getPhoenixevaluation(phoenixEvaluationId) {
  return request({
    url: '/xqsp/phoenixevaluation/' + phoenixEvaluationId,
    method: 'get'
  })
}

// 查询楼凤评价表详细
export function getEvaluationByPhoenixId(query) {
  return request({
    url: '/xqsp/phoenixevaluation/phoenixId/' + query.phoenixId,
    method: 'get',
    params: query
  })
}

// 新增楼凤评价表
export function addPhoenixevaluation(data) {
  return request({
    url: '/xqsp/phoenixevaluation',
    method: 'post',
    data: data
  })
}

// 修改楼凤评价表
export function updatePhoenixevaluation(data) {
  return request({
    url: '/xqsp/phoenixevaluation',
    method: 'put',
    data: data
  })
}

// 删除楼凤评价表
export function delPhoenixevaluation(phoenixEvaluationId) {
  return request({
    url: '/xqsp/phoenixevaluation/' + phoenixEvaluationId,
    method: 'delete'
  })
}

// 导出楼凤评价表
export function exportPhoenixevaluation(query) {
  return request({
    url: '/xqsp/phoenixevaluation/export',
    method: 'get',
    params: query
  })
}
