import request from '@/utils/request'

// 查询视频评价列表
export function listEvaluation(query) {
  return request({
    url: '/xqsp/evaluation/list',
    method: 'get',
    params: query
  })
}

// 查询视频评价详细
export function getEvaluation(videoEvaluationId) {
  return request({
    url: '/xqsp/evaluation/' + videoEvaluationId,
    method: 'get'
  })
}

// 根据视频id查询视频评价详细
export function getEvaluationByVideoId(query) {

  return request({
    url: '/xqsp/evaluation/videoId/' + query.videoId,
    method: 'get',
    params: query
  })
}

// 新增视频评价
export function addEvaluation(data) {
  return request({
    url: '/xqsp/evaluation',
    method: 'post',
    data: data
  })
}

// 修改视频评价
export function updateEvaluation(data) {
  return request({
    url: '/xqsp/evaluation',
    method: 'put',
    data: data
  })
}

// 删除视频评价
export function delEvaluation(videoEvaluationId) {
  return request({
    url: '/xqsp/evaluation/' + videoEvaluationId,
    method: 'delete'
  })
}

// 导出视频评价
export function exportEvaluation(query) {
  return request({
    url: '/xqsp/evaluation/export',
    method: 'get',
    params: query
  })
}
