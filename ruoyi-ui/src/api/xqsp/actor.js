import request from '@/utils/request'

// 查询演员管理列表
export function listActor(query) {
  return request({
    url: '/xqsp/actor/list',
    method: 'get',
    params: query
  })
}

// 查询演员管理详细
export function getActor(actorId) {
  return request({
    url: '/xqsp/actor/' + actorId,
    method: 'get'
  })
}

// 新增演员管理
export function addActor(data) {
  return request({
    url: '/xqsp/actor',
    method: 'post',
    data: data
  })
}

// 修改演员管理
export function updateActor(data) {
  return request({
    url: '/xqsp/actor',
    method: 'put',
    data: data
  })
}

// 删除演员管理
export function delActor(actorId) {
  return request({
    url: '/xqsp/actor/' + actorId,
    method: 'delete'
  })
}

// 导出演员管理
export function exportActor(query) {
  return request({
    url: '/xqsp/actor/export',
    method: 'get',
    params: query
  })
}

