import request from '@/utils/request'

// 查询免费章节数列表
export function listComic_free_chapter(query) {
  return request({
    url: '/xqsp/comic_free_chapter/list',
    method: 'get',
    params: query
  })
}

// 查询免费章节数详细
export function getComic_free_chapter(comicFreeChapter) {
  return request({
    url: '/xqsp/comic_free_chapter/' + comicFreeChapter,
    method: 'get'
  })
}

// 新增免费章节数
export function addComic_free_chapter(data) {
  return request({
    url: '/xqsp/comic_free_chapter',
    method: 'post',
    data: data
  })
}

// 修改免费章节数
export function updateComic_free_chapter(data) {
  return request({
    url: '/xqsp/comic_free_chapter',
    method: 'put',
    data: data
  })
}

// 删除免费章节数
export function delComic_free_chapter(comicFreeChapter) {
  return request({
    url: '/xqsp/comic_free_chapter/' + comicFreeChapter,
    method: 'delete'
  })
}

// 导出免费章节数
export function exportComic_free_chapter(query) {
  return request({
    url: '/xqsp/comic_free_chapter/export',
    method: 'get',
    params: query
  })
}