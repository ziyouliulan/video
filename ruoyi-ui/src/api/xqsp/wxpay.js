import request from '@/utils/request'

// 查询微信支付列表
export function listWxpay(query) {
  return request({
    url: '/xqsp/wxpay/list',
    method: 'get',
    params: query
  })
}

// 查询微信支付详细
export function getWxpay(wxpayId) {
  return request({
    url: '/xqsp/wxpay/' + wxpayId,
    method: 'get'
  })
}

// 新增微信支付
export function addWxpay(data) {
  return request({
    url: '/xqsp/wxpay',
    method: 'post',
    data: data
  })
}

// 修改微信支付
export function updateWxpay(data) {
  return request({
    url: '/xqsp/wxpay',
    method: 'put',
    data: data
  })
}

// 删除微信支付
export function delWxpay(wxpayId) {
  return request({
    url: '/xqsp/wxpay/' + wxpayId,
    method: 'delete'
  })
}

// 导出微信支付
export function exportWxpay(query) {
  return request({
    url: '/xqsp/wxpay/export',
    method: 'get',
    params: query
  })
}