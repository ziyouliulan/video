import request from '@/utils/request'

// 查询banner图操作列表
export function listBanner(query) {
  return request({
    url: '/xqsp/banner/list',
    method: 'get',
    params: query
  })
}

// 查询banner图操作详细
export function getBanner(bannerId) {
  return request({
    url: '/xqsp/banner/' + bannerId,
    method: 'get'
  })
}

// 新增banner图操作
export function addBanner(data) {
  return request({
    url: '/xqsp/banner',
    method: 'post',
    data: data
  })
}

// 修改banner图操作
export function updateBanner(data) {
  return request({
    url: '/xqsp/banner',
    method: 'put',
    data: data
  })
}

export function changeBannerStatus(bannerId,bannerState) {

  const data={
    bannerId,bannerState
  }
  return request({
    url: '/xqsp/banner',
    method: 'put',
    data: data
  })
}
// 删除banner图操作
export function delBanner(bannerId) {
  return request({
    url: '/xqsp/banner/' + bannerId,
    method: 'delete'
  })
}

// 导出banner图操作
export function exportBanner(query) {
  return request({
    url: '/xqsp/banner/export',
    method: 'get',
    params: query
  })
}
