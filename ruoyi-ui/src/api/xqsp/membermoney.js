import request from '@/utils/request'

// 查询会员设置列表
export function listMembermoney(query) {
  return request({
    url: '/xqsp/membermoney/list',
    method: 'get',
    params: query
  })
}

// 查询会员设置详细
export function getMembermoney(memberMoneyId) {
  return request({
    url: '/xqsp/membermoney/' + memberMoneyId,
    method: 'get'
  })
}

// 新增会员设置
export function addMembermoney(data) {
  return request({
    url: '/xqsp/membermoney',
    method: 'post',
    data: data
  })
}

// 修改会员设置
export function updateMembermoney(data) {
  return request({
    url: '/xqsp/membermoney',
    method: 'put',
    data: data
  })
}

// 删除会员设置
export function delMembermoney(memberMoneyId) {
  return request({
    url: '/xqsp/membermoney/' + memberMoneyId,
    method: 'delete'
  })
}

// 导出会员设置
export function exportMembermoney(query) {
  return request({
    url: '/xqsp/membermoney/export',
    method: 'get',
    params: query
  })
}