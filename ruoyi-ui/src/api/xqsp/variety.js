import request from '@/utils/request'

// 查询综艺管理列表
export function listVariety(query) {
  return request({
    url: '/xqsp/variety/list',
    method: 'get',
    params: query
  })
}

// 查询综艺管理详细
export function getVariety(varietyId) {
  return request({
    url: '/xqsp/variety/' + varietyId,
    method: 'get'
  })
}

// 新增综艺管理
export function addVariety(data) {
  return request({
    url: '/xqsp/variety',
    method: 'post',
    data: data
  })
}

// 修改综艺管理
export function updateVariety(data) {
  return request({
    url: '/xqsp/variety',
    method: 'put',
    data: data
  })
}

// 删除综艺管理
export function delVariety(varietyId) {
  return request({
    url: '/xqsp/variety/' + varietyId,
    method: 'delete'
  })
}

// 导出综艺管理
export function exportVariety(query) {
  return request({
    url: '/xqsp/variety/export',
    method: 'get',
    params: query
  })
}