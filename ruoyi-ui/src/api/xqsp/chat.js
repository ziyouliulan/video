import request from '@/utils/request'

// 查询聊天信息列表
export function listChat(query) {
  return request({
    url: '/xqsp/chat/list',
    method: 'get',
    params: query
  })
}

// 查询聊天信息列表
export function listChat1(query) {
  return request({
    url: '/xqsp/chat/list1',
    method: 'get',
    params: query
  })
}
// 查询聊天信息列表
export function listChat2(query) {
  return request({
    url: '/xqsp/chat/list2',
    method: 'get',
    params: query
  })
}

// 查询聊天信息详细
export function getChat(chatId) {
  return request({
    url: '/xqsp/chat/' + chatId,
    method: 'get'
  })
}

// 新增聊天信息
export function addChat(data) {
  return request({
    url: '/xqsp/chat',
    method: 'post',
    data: data
  })
}

// 修改聊天信息
export function updateChat(data) {
  return request({
    url: '/xqsp/chat',
    method: 'put',
    data: data
  })
}

// 删除聊天信息
export function delChat(chatId) {
  return request({
    url: '/xqsp/chat/' + chatId,
    method: 'delete'
  })
}

// 导出聊天信息
export function exportChat(query) {
  return request({
    url: '/xqsp/chat/export',
    method: 'get',
    params: query
  })
}
