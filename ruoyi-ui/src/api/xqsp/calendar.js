import request from '@/utils/request'

// 查询消费记录列表
export function listCalendar(query) {
  return request({
    url: '/xqsp/calendar/list',
    method: 'get',
    params: query
  })
}

// 查询消费记录详细
export function getCalendar(expenseCalendarId) {
  return request({
    url: '/xqsp/calendar/' + expenseCalendarId,
    method: 'get'
  })
}

// 新增消费记录
export function addCalendar(data) {
  return request({
    url: '/xqsp/calendar',
    method: 'post',
    data: data
  })
}

// 修改消费记录
export function updateCalendar(data) {
  return request({
    url: '/xqsp/calendar',
    method: 'put',
    data: data
  })
}

// 删除消费记录
export function delCalendar(expenseCalendarId) {
  return request({
    url: '/xqsp/calendar/' + expenseCalendarId,
    method: 'delete'
  })
}

// 导出消费记录
export function exportCalendar(query) {
  return request({
    url: '/xqsp/calendar/export',
    method: 'get',
    params: query
  })
}