import request from '@/utils/request'

// 查询用户头像列表
export function listHead_img(query) {
  return request({
    url: '/xqsp/head_img/list',
    method: 'get',
    params: query
  })
}

// 查询用户头像详细
export function getHead_img(headImgId) {
  return request({
    url: '/xqsp/head_img/' + headImgId,
    method: 'get'
  })
}

// 新增用户头像
export function addHead_img(data) {
  return request({
    url: '/xqsp/head_img',
    method: 'post',
    data: data
  })
}

// 修改用户头像
export function updateHead_img(data) {
  return request({
    url: '/xqsp/head_img',
    method: 'put',
    data: data
  })
}

// 删除用户头像
export function delHead_img(headImgId) {
  return request({
    url: '/xqsp/head_img/' + headImgId,
    method: 'delete'
  })
}

// 导出用户头像
export function exportHead_img(query) {
  return request({
    url: '/xqsp/head_img/export',
    method: 'get',
    params: query
  })
}