import request from '@/utils/request'

// 查询车友群列表
export function listGroup(query) {
  return request({
    url: '/xqsp/group/list',
    method: 'get',
    params: query
  })
}

// 查询车友群详细
export function getGroup(groupId) {
  return request({
    url: '/xqsp/group/' + groupId,
    method: 'get'
  })
}

// 新增车友群
export function addGroup(data) {
  return request({
    url: '/xqsp/group',
    method: 'post',
    data: data
  })
}

// 修改车友群
export function updateGroup(data) {
  return request({
    url: '/xqsp/group',
    method: 'put',
    data: data
  })
}

// 删除车友群
export function delGroup(groupId) {
  return request({
    url: '/xqsp/group/' + groupId,
    method: 'delete'
  })
}

// 导出车友群
export function exportGroup(query) {
  return request({
    url: '/xqsp/group/export',
    method: 'get',
    params: query
  })
}