import request from '@/utils/request'

// 查询用户表列表
export function listUsers(query) {
  return request({
    url: '/xqsp/users/list',
    method: 'get',
    params: query
  })
}

// 查询日新增数量
export function listUserCountDay(query) {
  return request({
    url: '/xqsp/users/list/userCountDay',
    method: 'get',
    params: query
  })
}

// 查询月新增数量
export function listUserCountMonth(query) {
  return request({
    url: '/xqsp/users/list/userCountMonth',
    method: 'get',
    params: query
  })
}

// 查询日活数量
export function listUserOnlineCountDay(query) {
  return request({
    url: '/xqsp/users/list/userOnlineCountDay',
    method: 'get',
    params: query
  })
}
// 查询月活数量
export function listUserOnlineCountMonth(query) {
  return request({
    url: '/xqsp/users/list/userOnlineCountMonth',
    method: 'get',
    params: query
  })
}

// 查询在线数量
export function listUserOnlineCount() {
  return request({
    url: '/xqsp/users/list/userOnlineCount',
    method: 'get',
  })
}


// 查询用户表详细
export function getUsers(userId) {
  return request({
    url: '/xqsp/users/' + userId,
    method: 'get'
  })
}
//根据xqid查询下级用户
export function getLookList(query){
  return request({
    url:'/xqsp/users/userXqid/'+query.userXqid,
    method:'get',
    params: query
  })
}
// 新增用户表
export function addUsers(data) {
  return request({
    url: '/xqsp/users',
    method: 'post',
    data: data
  })
}

// 修改用户表
export function updateUsers(data) {
  return request({
    url: '/xqsp/users',
    method: 'put',
    data: data
  })
}

// 会员充值
export function updateUsersMg(data) {
  return request({
    url: '/xqsp/users/updateUsersMg',
    method: 'put',
    data: data
  })
}
// 金币充值
export function updateUsersGlod(data) {
  return request({
    url: '/xqsp/users/updateUsersGlod',
    method: 'put',
    data: data
  })
}
// 修改用户表状态
export function updateUserState(userId, userState) {
  const data={
    userId, userState
  }
  return request({
    url: '/xqsp/users',
    method: 'put',
    data: data
  })
}

// 修改用户表评论权限
export function updateUserComments(userId, userComments) {
  const data={
    userId, userComments
  }
  return request({
    url: '/xqsp/users',
    method: 'put',
    data: data
  })
}

// 删除用户表
export function delUsers(userId) {
  return request({
    url: '/xqsp/users/' + userId,
    method: 'delete'
  })
}

// 导出用户表
export function exportUsers(query) {
  return request({
    url: '/xqsp/users/export',
    method: 'get',
    params: query
  })
}
