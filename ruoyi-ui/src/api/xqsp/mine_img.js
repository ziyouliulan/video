import request from '@/utils/request'

// 查询我的广告列表
export function listMine_img(query) {
  return request({
    url: '/xqsp/mine_img/list',
    method: 'get',
    params: query
  })
}

// 查询我的广告详细
export function getMine_img(mineImgId) {
  return request({
    url: '/xqsp/mine_img/' + mineImgId,
    method: 'get'
  })
}

// 新增我的广告
export function addMine_img(data) {
  return request({
    url: '/xqsp/mine_img',
    method: 'post',
    data: data
  })
}

// 修改我的广告
export function updateMine_img(data) {
  return request({
    url: '/xqsp/mine_img',
    method: 'put',
    data: data
  })
}

// 删除我的广告
export function delMine_img(mineImgId) {
  return request({
    url: '/xqsp/mine_img/' + mineImgId,
    method: 'delete'
  })
}

// 导出我的广告
export function exportMine_img(query) {
  return request({
    url: '/xqsp/mine_img/export',
    method: 'get',
    params: query
  })
}