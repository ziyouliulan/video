import request from '@/utils/request'

// 查询楼凤已同意列表
export function listService(query) {
  return request({
    url: '/xqsp/service/list',
    method: 'get',
    params: query
  })
}

// 查询楼凤审核列表
export function listServiceAudit(query) {
  return request({
    url: '/xqsp/service/list/audit',
    method: 'get',
    params: query
  })
}

// 查询楼凤操作详细
export function getService(phoenixId) {
  return request({
    url: '/xqsp/service/' + phoenixId,
    method: 'get'
  })
}

// 新增楼凤操作
export function addService(data) {
  return request({
    url: '/xqsp/service',
    method: 'post',
    data: data
  })
}

// 修改楼凤操作
export function updateService(phoenixId,phoenixAudit) {
  const data={
    phoenixId,phoenixAudit
  }
  return request({
    url: '/xqsp/service',
    method: 'put',
    data: data
  })
}
// 修改楼凤操作
export function updateServices(data) {
  return request({
    url: '/xqsp/service',
    method: 'put',
    data: data
  })
}
// 修改楼凤状态操作
export function updateServiceState(phoenixId, phoenixState) {

  const data={
    phoenixId, phoenixState
  }
  return request({
    url: '/xqsp/service',
    method: 'put',
    data: data
  })
}
// 是否同意申请
export function updateServiceAudit(phoenixId, phoenixAudit) {

  const data={
    phoenixId, phoenixAudit
  }
  return request({
    url: '/xqsp/service',
    method: 'put',
    data: data
  })
}

// 删除楼凤操作
export function delService(phoenixId) {
  return request({
    url: '/xqsp/service/' + phoenixId,
    method: 'delete'
  })
}

// 导出楼凤操作
export function exportService(query) {
  return request({
    url: '/xqsp/service/export',
    method: 'get',
    params: query
  })
}
