import request from '@/utils/request'

// 查询充值反馈列表
export function listTopupfeedback(query) {
  return request({
    url: '/xqsp/topupfeedback/list',
    method: 'get',
    params: query
  })
}

// 查询充值反馈详细
export function getTopupfeedback(topupFeedbackId) {
  return request({
    url: '/xqsp/topupfeedback/' + topupFeedbackId,
    method: 'get'
  })
}

// 新增充值反馈
export function addTopupfeedback(data) {
  return request({
    url: '/xqsp/topupfeedback',
    method: 'post',
    data: data
  })
}

// 修改充值反馈
export function updateTopupfeedback(data) {
  return request({
    url: '/xqsp/topupfeedback',
    method: 'put',
    data: data
  })
}

// 修改查看状态
export function changeUserStatus(topupFeedbackId,topupFeedbackWatch) {
  const data={
    topupFeedbackId,topupFeedbackWatch
  }
  return request({
    url: '/xqsp/topupfeedback',
    method: 'put',
    data: data
  })
}

// 删除充值反馈
export function delTopupfeedback(topupFeedbackId) {
  return request({
    url: '/xqsp/topupfeedback/' + topupFeedbackId,
    method: 'delete'
  })
}

// 导出充值反馈
export function exportTopupfeedback(query) {
  return request({
    url: '/xqsp/topupfeedback/export',
    method: 'get',
    params: query
  })
}
