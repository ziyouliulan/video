import request from '@/utils/request'

// 查询分类子表列表
export function listCategory(query) {
  return request({
    url: '/xqsp/category/list',
    method: 'get',
    params: query
  })
}

// 查询分类子表详细
export function getCategory(videoCategoryId) {
  return request({
    url: '/xqsp/category/' + videoCategoryId,
    method: 'get'
  })
}

// 新增分类子表
export function addCategory(data) {
  return request({
    url: '/xqsp/category',
    method: 'post',
    data: data
  })
}

// 修改分类子表
export function updateCategory(data) {
  return request({
    url: '/xqsp/category',
    method: 'put',
    data: data
  })
}
// 修改分类子表状态
export function updateCategoryState(videoCategoryId,videoCategoryState) {
  const data={
    videoCategoryId,
    videoCategoryState
  }
  return request({
    url: '/xqsp/category',
    method: 'put',
    data: data
  })
}
// 修改分类子表状态
export function updateCategoryParent(videoCategoryId,videoCategoryParentId) {
  const data={
    videoCategoryId,
    videoCategoryParentId
  }
  return request({
    url: '/xqsp/category',
    method: 'put',
    data: data
  })
}

// 删除分类子表
export function delCategory(videoCategoryId) {
  return request({
    url: '/xqsp/category/' + videoCategoryId,
    method: 'delete'
  })
}

// 导出分类子表
export function exportCategory(query) {
  return request({
    url: '/xqsp/category/export',
    method: 'get',
    params: query
  })
}
