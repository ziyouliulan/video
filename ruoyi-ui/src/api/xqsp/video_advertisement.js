import request from '@/utils/request'

// 查询视频广告列表
export function listVideo_advertisement(query) {
  return request({
    url: '/xqsp/video_advertisement/list',
    method: 'get',
    params: query
  })
}

// 查询视频广告详细
export function getVideo_advertisement(videoAdvertisementId) {
  return request({
    url: '/xqsp/video_advertisement/' + videoAdvertisementId,
    method: 'get'
  })
}

// 新增视频广告
export function addVideo_advertisement(data) {
  return request({
    url: '/xqsp/video_advertisement',
    method: 'post',
    data: data
  })
}

// 修改视频广告
export function updateVideo_advertisement(data) {
  return request({
    url: '/xqsp/video_advertisement',
    method: 'put',
    data: data
  })
}

// 删除视频广告
export function delVideo_advertisement(videoAdvertisementId) {
  return request({
    url: '/xqsp/video_advertisement/' + videoAdvertisementId,
    method: 'delete'
  })
}

// 导出视频广告
export function exportVideo_advertisement(query) {
  return request({
    url: '/xqsp/video_advertisement/export',
    method: 'get',
    params: query
  })
}