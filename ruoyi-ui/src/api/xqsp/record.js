import request from '@/utils/request'

// 查询充值管理列表
export function listRecord(query) {
  return request({
    url: '/xqsp/record/list',
    method: 'get',
    params: query
  })
}

// 查询充值管理详细
export function getRecord(rechargeRecordId) {
  return request({
    url: '/xqsp/record/' + rechargeRecordId,
    method: 'get'
  })
}

// 新增充值管理
export function addRecord(data) {
  return request({
    url: '/xqsp/record',
    method: 'post',
    data: data
  })
}

// 修改充值管理
export function updateRecord(data) {
  return request({
    url: '/xqsp/record',
    method: 'put',
    data: data
  })
}

// 删除充值管理
export function delRecord(rechargeRecordId) {
  return request({
    url: '/xqsp/record/' + rechargeRecordId,
    method: 'delete'
  })
}

// 导出充值管理
export function exportRecord(query) {
  return request({
    url: '/xqsp/record/export',
    method: 'get',
    params: query
  })
}