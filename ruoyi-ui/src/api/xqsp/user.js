import request from '@/utils/request'

// 查询书架管理列表
export function listUser(query) {
  return request({
    url: '/xqsp/user/list',
    method: 'get',
    params: query
  })
}

// 查询书架管理详细
export function getUser(comicUserId) {
  return request({
    url: '/xqsp/user/' + comicUserId,
    method: 'get'
  })
}

// 新增书架管理
export function addUser(data) {
  return request({
    url: '/xqsp/user',
    method: 'post',
    data: data
  })
}

// 修改书架管理
export function updateUser(data) {
  return request({
    url: '/xqsp/user',
    method: 'put',
    data: data
  })
}

// 删除书架管理
export function delUser(comicUserId) {
  return request({
    url: '/xqsp/user/' + comicUserId,
    method: 'delete'
  })
}

// 导出书架管理
export function exportUser(query) {
  return request({
    url: '/xqsp/user/export',
    method: 'get',
    params: query
  })
}